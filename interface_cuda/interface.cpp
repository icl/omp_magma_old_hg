/*
    -- MAGMA (version 2.0) --
       Univ. of Tennessee, Knoxville
       Univ. of California, Berkeley
       Univ. of Colorado, Denver
       @date
 
       @author Mark Gates
*/

#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include <map>

#if __cplusplus >= 201103  // C++11 standard
#include <mutex>
#endif

#if defined(_OPENMP)
#include <omp.h>
#endif

#if defined(MAGMA_WITH_MKL)
#include <mkl_service.h>
#endif

#if defined(MAGMA_WITH_ACML)
#include <acml.h>
#endif

//#include <cuda_runtime.h>
//#include <cublas_v2.h>

// defining MAGMA_LAPACK_H is a hack to NOT include magma_lapack.h
// via common_magma.h here, since it conflicts with acml.h and we don't
// need lapack here, but we want acml.h for the acmlversion() function.
#define MAGMA_LAPACK_H

#ifndef MAGMA_NO_V1
#include "magma.h"
#endif
#include "magma_internal.h"

#include "error.h"


#ifdef DEBUG_MEMORY
// defined in alloc.cpp
extern std::map< void*, size_t > g_pointers_dev;
extern std::map< void*, size_t > g_pointers_cpu;
extern std::map< void*, size_t > g_pointers_pin;
#endif


// ------------------------------------------------------------
// constants

// bit flags
enum {
    own_none     = 0x0000,
    own_stream   = 0x0001,
    own_cublas   = 0x0002,
    own_cusparse = 0x0004,
    own_opencl   = 0x0008
};


// ------------------------------------------------------------
// globals
#if __cplusplus >= 201103  // C++11 standard
    static std::mutex g_mutex;
#else
    // without C++11, wrap pthread mutex
    class PthreadMutex {
    public:
        PthreadMutex()
        {
            int err = pthread_mutex_init( &mutex, NULL );
            if ( err ) {
                fprintf( stderr, "pthread_mutex_init failed: %d\n", err );
            }
        }
        
        ~PthreadMutex()
        {
            int err = pthread_mutex_destroy( &mutex );
            if ( err ) {
                fprintf( stderr, "pthread_mutex_destroy failed: %d\n", err );
            }
        }
        
        void lock()
        {
            int err = pthread_mutex_lock( &mutex );
            if ( err ) {
                fprintf( stderr, "pthread_mutex_lock failed: %d\n", err );
            }
        }
        
        void unlock()
        {
            int err = pthread_mutex_unlock( &mutex );
            if ( err ) {
                fprintf( stderr, "pthread_mutex_unlock failed: %d\n", err );
            }
        }
        
    private:
        pthread_mutex_t mutex;
    };
    
    static PthreadMutex g_mutex;
#endif

// count of (init - finalize) calls
static int g_init = 0;

#ifndef MAGMA_NO_V1
    static magma_queue_t* g_null_queues = NULL;

    #ifdef HAVE_PTHREAD_KEY
    static pthread_key_t g_magma_queue_key;
    #else
    static magma_queue_t g_magma_queue = NULL;
    #endif
#endif // MAGMA_NO_V1


// --------------------
// subset of the CUDA device properties, set by magma_init()
struct magma_device_info
{
    size_t memory;
    magma_int_t cuda_arch;
};

static int g_magma_devices_cnt = 0;
static struct magma_device_info* g_magma_devices = NULL;


// ========================================
// initialization

/**
    Initializes the MAGMA library.
    Caches information about available CUDA devices.
    When renumbering CUDA devices, call cudaSetValidDevices before calling magma_init.
    When setting CUDA device flags, call cudaSetDeviceFlags before calling magma_init.
    
    Every magma_init call must be paired with a magma_finalize call.
    Only one thread needs to call magma_init and magma_finalize,
    but every thread may call it. If n threads call magma_init,
    the n-th call to magma_finalize will release resources.
    
    @see magma_finalize
    
    @ingroup magma_init
*/
extern "C"
magma_int_t magma_init()
{
    magma_int_t info = 0;
    
    return info;
}


// --------------------
#ifdef DEBUG_MEMORY
extern "C"
void magma_warn_leaks( const std::map< void*, size_t >& pointers, const char* type )
{
    if ( pointers.size() > 0 ) {
        fprintf( stderr, "Warning: MAGMA detected memory leak of %lld %s pointers:\n",
                 pointers.size(), type );
        std::map< void*, size_t >::const_iterator iter;
        for( iter = pointers.begin(); iter != pointers.end(); ++iter ) {
            fprintf( stderr, "    pointer %p, size %lu\n", iter->first, iter->second );
        }
    }
}
#endif


// --------------------
/**
    Frees information used by the MAGMA library.
    @ingroup magma_init
*/
extern "C"
magma_int_t magma_finalize()
{
    magma_int_t info = 0;
    
    return info;
}

// --------------------
/**
    Print the available GPU devices. Used in testing.
    @ingroup magma_init
*/
extern "C"
void magma_print_environment()
{
  ;
}


// ========================================
// device support
// ---------------------------------------------
// Returns CUDA architecture capability for the current device.
// This requires magma_init to be called first (to cache the information).
// Version is integer xyz, where x is major, y is minor, and z is micro,
// the same as __CUDA_ARCH__. Thus for architecture 1.3 it returns 130.
extern "C"
magma_int_t magma_getdevice_arch()
{
    return 0;
}


// --------------------
extern "C"
void magma_getdevices(
    magma_device_t* devices,
    magma_int_t  size,
    magma_int_t* numPtr )
{
    int z=0;
}

// --------------------
extern "C"
void magma_getdevice( magma_device_t* device )
{
    int z=0;
}

// --------------------
extern "C"
void magma_setdevice( magma_device_t device )
{
    int z=0;
}

// --------------------
/// This functionality does not exist in OpenCL, so it is deprecated for CUDA, too.
/// @deprecated
extern "C"
void magma_device_sync()
{
    int z=0;
}


// ========================================
// queue support
extern "C"
magma_int_t
magma_queue_get_device( magma_queue_t queue )
{
    return queue->device();
}

extern "C"
cudaStream_t
magma_queue_get_cuda_stream( magma_queue_t queue )
{
    return 0;
}

extern "C"
cublasHandle_t
magma_queue_get_cublas_handle( magma_queue_t queue )
{
    return 0;
}

extern "C"
cusparseHandle_t
magma_queue_get_cusparse_handle( magma_queue_t queue )
{
    return 0;
}


#ifndef MAGMA_NO_V1
// --------------------
extern "C"
magma_int_t magmablasSetKernelStream( magma_queue_t queue )
{
    magma_int_t info = 0;
    #ifdef HAVE_PTHREAD_KEY
    info = pthread_setspecific( g_magma_queue_key, queue );
    #else
    g_magma_queue = queue;
    #endif
    return info;
}


// --------------------
extern "C"
magma_int_t magmablasGetKernelStream( magma_queue_t *queue_ptr )
{
    #ifdef HAVE_PTHREAD_KEY
    *queue_ptr = (magma_queue_t) pthread_getspecific( g_magma_queue_key );
    #else
    *queue_ptr = g_magma_queue;
    #endif
    return 0;
}


// --------------------
extern "C"
magma_queue_t magmablasGetQueue()
{
    magma_queue_t queue;
    #ifdef HAVE_PTHREAD_KEY
    queue = (magma_queue_t) pthread_getspecific( g_magma_queue_key );
    #else
    queue = g_magma_queue;
    #endif
    if ( queue == NULL ) {
        magma_device_t dev;
        magma_getdevice( &dev );
        if ( dev >= g_magma_devices_cnt || g_null_queues == NULL ) {
            fprintf( stderr, "Error: %s requires magma_init() to be called first for MAGMA v1 compatability.\n",
                     __func__ );
            return NULL;
        }
        // create queue w/ NULL stream first time that NULL queue is used
        if ( g_null_queues[dev] == NULL ) {
            magma_queue_create_from_cuda( dev, NULL, NULL, NULL, &g_null_queues[dev] );
            printf( "dev %lld create queue %p\n", (long long) dev, (void*) g_null_queues[dev] );
            assert( g_null_queues[dev] != NULL );
        }
        queue = g_null_queues[dev];
    }
    assert( queue != NULL );
    return queue;
}
#endif // MAGMA_NO_V1


// --------------------
extern "C"
void magma_queue_create_internal(
    magma_queue_t* queue_ptr,
    const char* func, const char* file, int line )
{
    int device;
}

// --------------------
extern "C"
void magma_queue_create_v2_internal(
    magma_device_t device, magma_queue_t* queue_ptr,
    const char* func, const char* file, int line )
{
    magma_queue_t queue;
}

// --------------------
extern "C"
void magma_queue_create_from_cuda_internal(
    magma_device_t   device,
    cudaStream_t     stream,
    cublasHandle_t   cublas_handle,
    cusparseHandle_t cusparse_handle,
    magma_queue_t*   queue_ptr,
    const char* func, const char* file, int line )
{
    int queue;
  
}

// --------------------
extern "C"
void magma_queue_destroy_internal(
    magma_queue_t queue,
    const char* func, const char* file, int line )
{
   int t=0;
}

// --------------------
extern "C"
void magma_queue_sync_internal(
    magma_queue_t queue,
    const char* func, const char* file, int line )
{
    ;
}


// --------------------
// TODO: do set device based on queue? and restore device?
extern "C" size_t
magma_queue_mem_size( magma_queue_t queue )
{

    return 0;
}


// ========================================
// event support
// --------------------
extern "C"
void magma_event_create( magma_event_t* event )
{
    int t=0;
}

// --------------------
extern "C"
void magma_event_destroy( magma_event_t event )
{
    int t=0;
}

// --------------------
extern "C"
void magma_event_record( magma_event_t event, magma_queue_t queue )
{
    int t=0;
}

// --------------------
// blocks CPU until event occurs
extern "C"
void magma_event_sync( magma_event_t event )
{
    int t=0;
}

// --------------------
// blocks queue (but not CPU) until event occurs
extern "C"
void magma_queue_wait_event( magma_queue_t queue, magma_event_t event )
{
    int t=0;
}

