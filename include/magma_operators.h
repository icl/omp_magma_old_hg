/*
    -- MAGMA (version 2.0) --
       Univ. of Tennessee, Knoxville
       Univ. of California, Berkeley
       Univ. of Colorado, Denver
       @date
 
       @author Mathieu Faverge
       @author Mark Gates
*/

#ifndef MAGMA_OPERATORS_H
#define MAGMA_OPERATORS_H

#ifdef __cplusplus

// __host__ and __device__ are defined in CUDA headers.
#include "magma_types.h"

/* names to match C++ std complex functions */
 static inline double real(const magmaDoubleComplex &x) { return MAGMA_Z_REAL(x); }
 static inline float  real(const magmaFloatComplex  &x) { return MAGMA_C_REAL(x); }
 static inline double real(const double             &x) { return x; }
 static inline float  real(const float              &x) { return x; }

 static inline double imag(const magmaDoubleComplex &x) { return MAGMA_Z_IMAG(x); }
 static inline float  imag(const magmaFloatComplex  &x) { return MAGMA_C_IMAG(x); }
 static inline double imag(const double        & /*x*/) { return 0.; }
 static inline float  imag(const float         & /*x*/) { return 0.; }

 static inline double             conj(const double             &x) { return x; }
 static inline float              conj(const float              &x) { return x; }

 static inline double fabs(const magmaDoubleComplex &x) { return MAGMA_Z_ABS(x); }
 static inline float  fabs(const magmaFloatComplex  &x) { return MAGMA_C_ABS(x); }
// static inline float  fabs(const float              &x) { return MAGMA_S_ABS(x); }  // conflicts with std::fabs in .cu files
// already have fabs( double ) in math.h

 static inline double abs1(const magmaDoubleComplex &x) { return MAGMA_Z_ABS1(x); }
 static inline float  abs1(const magmaFloatComplex  &x) { return MAGMA_C_ABS1(x); }
 static inline double abs1(const double             &x) { return MAGMA_D_ABS1(x); }
 static inline float  abs1(const float              &x) { return MAGMA_S_ABS1(x); }


/*************************************************************
 *              magmaDoubleComplex
 */

// ---------- negate
/* static inline magmaDoubleComplex
operator - (const magmaDoubleComplex &a)
{
    return MAGMA_Z_MAKE( -real(a),
                         -imag(a) );
}


// ---------- add
 static inline magmaDoubleComplex
operator + (const magmaDoubleComplex a, const magmaDoubleComplex b)
{
    return MAGMA_Z_MAKE( real(a) + real(b),
                         imag(a) + imag(b) );
}

 static inline magmaDoubleComplex
operator + (const magmaDoubleComplex a, const double s)
{
    return MAGMA_Z_MAKE( real(a) + s,
                         imag(a) );
}

 static inline magmaDoubleComplex
operator + (const double s, const magmaDoubleComplex b)
{
    return MAGMA_Z_MAKE( s + real(b),
                             imag(b) );
}

 static inline magmaDoubleComplex&
operator += (magmaDoubleComplex &a, const magmaDoubleComplex b)
{
    a = MAGMA_Z_MAKE( real(a) + real(b),
                      imag(a) + imag(b) );
    return a;
}

 static inline magmaDoubleComplex&
operator += (magmaDoubleComplex &a, const double s)
{
    a = MAGMA_Z_MAKE( real(a) + s,
                      imag(a) );
    return a;
}

*/
// ---------- subtract
 /*static inline magmaDoubleComplex
operator - (const magmaDoubleComplex a, const magmaDoubleComplex b)
{
    return MAGMA_Z_MAKE( real(a) - real(b),
                         imag(a) - imag(b) );
}

 static inline magmaDoubleComplex
operator - (const magmaDoubleComplex a, const double s)
{
    return MAGMA_Z_MAKE( real(a) - s,
                         imag(a) );
}

 static inline magmaDoubleComplex
operator - (const double s, const magmaDoubleComplex b)
{
    return MAGMA_Z_MAKE( s - real(b),
                           - imag(b) );
}

 static inline magmaDoubleComplex&
operator -= (magmaDoubleComplex &a, const magmaDoubleComplex b)
{
    a = MAGMA_Z_MAKE( real(a) - real(b),
                      imag(a) - imag(b) );
    return a;
}

 static inline magmaDoubleComplex&
operator -= (magmaDoubleComplex &a, const double s)
{
    a = MAGMA_Z_MAKE( real(a) - s,
                      imag(a) );
    return a;
}

*/
// ---------- multiply
/* static inline magmaDoubleComplex
operator * (const magmaDoubleComplex a, const magmaDoubleComplex b)
{
    return MAGMA_Z_MAKE( real(a)*real(b) - imag(a)*imag(b),
                         imag(a)*real(b) + real(a)*imag(b) );
}

 static inline magmaDoubleComplex
operator * (const magmaDoubleComplex a, const double s)
{
    return MAGMA_Z_MAKE( real(a)*s,
                         imag(a)*s );
}

 static inline magmaDoubleComplex
operator * (const double s, const magmaDoubleComplex a)
{
    return MAGMA_Z_MAKE( real(a)*s,
                         imag(a)*s );
}

 static inline magmaDoubleComplex&
operator *= (magmaDoubleComplex &a, const magmaDoubleComplex b)
{
    a = MAGMA_Z_MAKE( real(a)*real(b) - imag(a)*imag(b),
                      imag(a)*real(b) + real(a)*imag(b) );
    return a;
}

 static inline magmaDoubleComplex&
operator *= (magmaDoubleComplex &a, const double s)
{
    a = MAGMA_Z_MAKE( real(a)*s,
                      imag(a)*s );
    return a;
}
*/

// ---------- divide
/* From LAPACK DLADIV
 * Performs complex division in real arithmetic, avoiding unnecessary overflow.
 *
 *             a + i*b
 *  p + i*q = ---------
 *             c + i*d
 */
/* static inline magmaDoubleComplex
operator / (const magmaDoubleComplex x, const magmaDoubleComplex y)
{
    double a = real(x);
    double b = imag(x);
    double c = real(y);
    double d = imag(y);
    double e, f, p, q;
    if ( fabs( d ) < fabs( c ) ) {
        e = d / c;
        f = c + d*e;
        p = ( a + b*e ) / f;
        q = ( b - a*e ) / f;
    }
    else {
        e = c / d;
        f = d + c*e;
        p = (  b + a*e ) / f;
        q = ( -a + b*e ) / f;
    }
    return MAGMA_Z_MAKE( p, q );
}

 static inline magmaDoubleComplex
operator / (const magmaDoubleComplex a, const double s)
{
    return MAGMA_Z_MAKE( real(a)/s,
                         imag(a)/s );
}
*/
 static inline magmaDoubleComplex
operator / (const double a, const magmaDoubleComplex y)
{
    double c = real(y);
    double d = imag(y);
    double e, f, p, q;
    if ( fabs( d ) < fabs( c ) ) {
        e = d / c;
        f = c + d*e;
        p =  a   / f;
        q = -a*e / f;
    }
    else {
        e = c / d;
        f = d + c*e;
        p =  a*e / f;
        q = -a   / f;
    }
    return MAGMA_Z_MAKE( p, q );
}

 static inline magmaDoubleComplex&
operator /= (magmaDoubleComplex &a, const double s)
{
    a = MAGMA_Z_MAKE( real(a)/s,
                      imag(a)/s );
    return a;
}


// ---------- equality
/* static inline bool
operator == (const magmaDoubleComplex a, const magmaDoubleComplex b)
{
    return ( real(a) == real(b) &&
             imag(a) == imag(b) );
}

 static inline bool
operator == (const magmaDoubleComplex a, const double s)
{
    return ( real(a) == s &&
             imag(a) == 0. );
}

 static inline bool
operator == (const double s, const magmaDoubleComplex a)
{
    return ( real(a) == s &&
             imag(a) == 0. );
}
*/

// ---------- not equality

 static inline bool
operator != (const magmaDoubleComplex a, const double s)
{
    return ! (a == s);
}

 static inline bool
operator != (const double s, const magmaDoubleComplex a)
{
    return ! (a == s);
}


/*************************************************************
 *              magmaFloatComplex
 */
/*
// ---------- negate
 static inline magmaFloatComplex
operator - (const magmaFloatComplex &a)
{
    return MAGMA_C_MAKE( -real(a),
                         -imag(a) );
}


// ---------- add
 static inline magmaFloatComplex
operator + (const magmaFloatComplex a, const magmaFloatComplex b)
{
    return MAGMA_C_MAKE( real(a) + real(b),
                         imag(a) + imag(b) );
}

 static inline magmaFloatComplex
operator + (const magmaFloatComplex a, const float s)
{
    return MAGMA_C_MAKE( real(a) + s,
                         imag(a) );
}

 static inline magmaFloatComplex
operator + (const float s, const magmaFloatComplex b)
{
    return MAGMA_C_MAKE( s + real(b),
                             imag(b) );
}

 static inline magmaFloatComplex&
operator += (magmaFloatComplex &a, const magmaFloatComplex b)
{
    a = MAGMA_C_MAKE( real(a) + real(b),
                      imag(a) + imag(b) );
    return a;
}

 static inline magmaFloatComplex&
operator += (magmaFloatComplex &a, const float s)
{
    a = MAGMA_C_MAKE( real(a) + s,
                      imag(a) );
    return a;
}


// ---------- subtract
 static inline magmaFloatComplex
operator - (const magmaFloatComplex a, const magmaFloatComplex b)
{
    return MAGMA_C_MAKE( real(a) - real(b),
                         imag(a) - imag(b) );
}

 static inline magmaFloatComplex
operator - (const magmaFloatComplex a, const float s)
{
    return MAGMA_C_MAKE( real(a) - s,
                         imag(a) );
}

 static inline magmaFloatComplex
operator - (const float s, const magmaFloatComplex b)
{
    return MAGMA_C_MAKE( s - real(b),
                           - imag(b) );
}

 static inline magmaFloatComplex&
operator -= (magmaFloatComplex &a, const magmaFloatComplex b)
{
    a = MAGMA_C_MAKE( real(a) - real(b),
                      imag(a) - imag(b) );
    return a;
}

 static inline magmaFloatComplex&
operator -= (magmaFloatComplex &a, const float s)
{
    a = MAGMA_C_MAKE( real(a) - s,
                      imag(a) );
    return a;
}


// ---------- multiply
 static inline magmaFloatComplex
operator * (const magmaFloatComplex a, const magmaFloatComplex b)
{
    return MAGMA_C_MAKE( real(a)*real(b) - imag(a)*imag(b),
                         imag(a)*real(b) + real(a)*imag(b) );
}

 static inline magmaFloatComplex
operator * (const magmaFloatComplex a, const float s)
{
    return MAGMA_C_MAKE( real(a)*s,
                         imag(a)*s );
}

 static inline magmaFloatComplex
operator * (const float s, const magmaFloatComplex a)
{
    return MAGMA_C_MAKE( real(a)*s,
                         imag(a)*s );
}

 static inline magmaFloatComplex&
operator *= (magmaFloatComplex &a, const magmaFloatComplex b)
{
    a = MAGMA_C_MAKE( real(a)*real(b) - imag(a)*imag(b),
                      imag(a)*real(b) + real(a)*imag(b) );
    return a;
}

 static inline magmaFloatComplex&
operator *= (magmaFloatComplex &a, const float s)
{
    a = MAGMA_C_MAKE( real(a)*s,
                      imag(a)*s );
    return a;
}

*/
// ---------- divide
/* From LAPACK DLADIV
 * Performs complex division in real arithmetic, avoiding unnecessary overflow.
 *
 *             a + i*b
 *  p + i*q = ---------
 *             c + i*d
 */
 /*static inline magmaFloatComplex
operator / (const magmaFloatComplex x, const magmaFloatComplex y)
{
    float a = real(x);
    float b = imag(x);
    float c = real(y);
    float d = imag(y);
    float e, f, p, q;
    if ( fabs( d ) < fabs( c ) ) {
        e = d / c;
        f = c + d*e;
        p = ( a + b*e ) / f;
        q = ( b - a*e ) / f;
    }
    else {
        e = c / d;
        f = d + c*e;
        p = (  b + a*e ) / f;
        q = ( -a + b*e ) / f;
    }
    return MAGMA_C_MAKE( p, q );
}

 static inline magmaFloatComplex
operator / (const magmaFloatComplex a, const float s)
{
    return MAGMA_C_MAKE( real(a)/s,
                         imag(a)/s );
}

 static inline magmaFloatComplex
operator / (const float a, const magmaFloatComplex y)
{
    float c = real(y);
    float d = imag(y);
    float e, f, p, q;
    if ( fabs( d ) < fabs( c ) ) {
        e = d / c;
        f = c + d*e;
        p =  a   / f;
        q = -a*e / f;
    }
    else {
        e = c / d;
        f = d + c*e;
        p =  a*e / f;
        q = -a   / f;
    }
    return MAGMA_C_MAKE( p, q );
}

 static inline magmaFloatComplex&
operator /= (magmaFloatComplex &a, const float s)
{
    a = MAGMA_C_MAKE( real(a)/s,
                      imag(a)/s );
    return a;
}


// ---------- equality
 static inline bool
operator == (const magmaFloatComplex a, const magmaFloatComplex b)
{
    return ( real(a) == real(b) &&
             imag(a) == imag(b) );
}

 static inline bool
operator == (const magmaFloatComplex a, const float s)
{
    return ( real(a) == s &&
             imag(a) == 0. );
}

 static inline bool
operator == (const float s, const magmaFloatComplex a)
{
    return ( real(a) == s &&
             imag(a) == 0. );
}


// ---------- not equality

 static inline bool
operator != (const magmaFloatComplex a, const float s)
{
    return ! (a == s);
}

 static inline bool
operator != (const float s, const magmaFloatComplex a)
{
    return ! (a == s);
}
*/
#endif /* __cplusplus */

#endif /* MAGMA_OPERATORS_H */
