/*
    -- MAGMA (version 2.0) --
       Univ. of Tennessee, Knoxville
       Univ. of California, Berkeley
       Univ. of Colorado, Denver
       @date

       @precisions normal z -> s d c
       @author Hartwig Anzt
       @author Stephen Wood

*/
#include "magmasparse_internal.h"

/*******************************************************************************
    Purpose
    -------

    Computes matrix-vector product C = A*B on the CPU using OpenMP.

    Arguments
    ---------

    @param[in]
    A           magma_z_matrix
                input matrix A

    @param[in]
    B           magma_z_matrix
                input vector B

    @param[out]
    C           magma_z_matrix
                output vector C

    @param[in]
    num_tds     magma_int_t
                number of OpenMP threads

    @param[in]
    queue       magma_queue_t
                Queue to execute in.

    @ingroup magmasparse_zaux
*******************************************************************************/

extern "C" magma_int_t
magma_zgemvomp(
    magma_z_matrix A, magma_z_matrix B, magma_z_matrix *C, magma_int_t num_tds,
    magma_queue_t queue )
{
    magma_int_t info = 0;
    
    magmaDoubleComplex c_zero = MAGMA_Z_MAKE(0.0, 0.0);
    
    int size = A.num_rows;
    
    #pragma omp parallel for num_threads( num_tds )
    for( int i=0; i<size; i++){
        magmaDoubleComplex tmp = c_zero;
        #pragma omp simd
        for( int j=0; j<size; j++){
            tmp += A.val[size*i+j] * B.val[j];           
        }
        C->val[i] = tmp;
    }
    
    return info;
}



