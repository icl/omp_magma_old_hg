/*
    -- MAGMA (version 2.0) --
       Univ. of Tennessee, Knoxville
       Univ. of California, Berkeley
       Univ. of Colorado, Denver
       @date

       @precisions normal z -> c d s
       @author Hartwig Anzt
       @suthor Stephen Wood

*/
#include "magmasparse_internal.h"

#define PRECISION_z

/*******************************************************************************
    Purpose
    -------

    Mergels multiple operations into one kernel:

    dt = drs - dr

    Arguments
    ---------

    @param[in]
    num_rows    magma_int_t
                dimension m
                
    @param[in]
    num_cols    magma_int_t
                dimension n

    @param[in]
    drs         magmaDoubleComplex_ptr 
                vector

    @param[in]
    dr          magmaDoubleComplex_ptr 
                vector

    @param[in,out]
    dt          magmaDoubleComplex_ptr 
                vector

    @param[in]
    queue       magma_queue_t
                Queue to execute in.

    @ingroup magmasparse_zgegpuk
*******************************************************************************/

extern "C" 
magma_int_t
magma_zidr_smoothing_1(  
    magma_int_t num_rows, 
    magma_int_t num_cols, 
    magmaDoubleComplex_ptr drs,
    magmaDoubleComplex_ptr dr, 
    magmaDoubleComplex_ptr dt, 
    magma_queue_t queue )
{
	
    for( magma_int_t j=0; j<num_cols; j++ ){
        #pragma omp parallel for simd
        for( magma_int_t i=0; i<num_rows; i++ ) {
            dt[ i+j*num_rows ] =  drs[ i+j*num_rows ] - dr[ i+j*num_rows ];
        }
    }
    
    return MAGMA_SUCCESS;
}

/*******************************************************************************
    Purpose
    -------

    Mergels multiple operations into one kernel:

    dxs = dxs - gamma*(dxs-dx)

    Arguments
    ---------

    @param[in]
    num_rows    magma_int_t
                dimension m
                
    @param[in]
    num_cols    magma_int_t
                dimension n
                
    @param[in]
    omega       magmaDoubleComplex
                scalar
                
    @param[in]
    dx          magmaDoubleComplex_ptr 
                vector

    @param[in,out]
    dxs         magmaDoubleComplex_ptr 
                vector

    @param[in]
    queue       magma_queue_t
                Queue to execute in.

    @ingroup magmasparse_zgegpuk
*******************************************************************************/

extern "C" 
magma_int_t
magma_zidr_smoothing_2(  
    magma_int_t num_rows, 
    magma_int_t num_cols, 
    magmaDoubleComplex omega,
    magmaDoubleComplex_ptr dx,
    magmaDoubleComplex_ptr dxs, 
    magma_queue_t queue )
{
	
    for( magma_int_t j=0; j<num_cols; j++ ){
        #pragma omp parallel for simd
        for( magma_int_t i=0; i<num_rows; i++ ) {
            dxs[ i+j*num_rows ] = dxs[ i+j*num_rows ] + omega * dxs[ i+j*num_rows ]
                    - omega * dx[ i+j*num_rows ];
        }
    }
    
    return MAGMA_SUCCESS;
}
