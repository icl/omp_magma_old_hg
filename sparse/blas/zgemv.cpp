/*
    -- MAGMA (version 2.0) --
       Univ. of Tennessee, Knoxville
       Univ. of California, Berkeley
       Univ. of Colorado, Denver
       @date

       @precisions normal z -> c d s
       @author Stephen Wood

*/
#include "magmasparse_internal.h"

/*******************************************************************************
    Purpose
    -------
    
    ZGEMV  performs one of the matrix-vector operations

    y := alpha*A*x + beta*y,   or   y := alpha*A^T*x + beta*y,   or

    y := alpha*A^H*x + beta*y,

    where alpha and beta are scalars, x and y are vectors and A is an
    m by n matrix.
    
    Arguments
    ---------
    @param[in]
    transA     	transA is CHARACTER*1
           	   	On entry, TRANS specifies the operation to be performed as
           	   	follows:

           	   	transA = 'N' or 'n'   y := alpha*A*x + beta*y.

           	   	transA = 'T' or 't'   y := alpha*A^T*x + beta*y.

           	   	transA = 'C' or 'c'   y := alpha*A^H*x + beta*y.

    @param[in]
    m 	       	magma_int_t
                number of rows of the matrix A.

    @param[in]
    n 	       	magma_int_t
                number of columns of the matrix A.                
                
    @param[in]
    alpha       magmaDoubleComplex
                scalar.
                
    @param[in]
    dA          magmaDoubleComplex_ptr
                input matrix dA.
                
    @param[in]
    ldda        magma_int_t
                the increment for the elements of dx.     
                
    @param[in]
    dx          magmaDoubleComplex_ptr
                input vector dx.            
                
    @param[in]
    incx        magma_int_t
                the increment for the elements of dx.
                
    @param[in]
    beta        magmaDoubleComplex
                scalar.            
                
    @param[in,out]
    dy          magmaDoubleComplex_ptr
                input vector dy.            
                
    @param[in]
    incy        magma_int_t
                the increment for the elements of dy.        
                
    @param[in]
    queue       magma_queue_t
                Queue to execute in.

    @ingroup magmasparse_zblas
*******************************************************************************/

extern "C" 
void
magmablas_zgemv_q(
    magma_trans_t transA, magma_int_t m, magma_int_t n, 
    magmaDoubleComplex alpha,
    magmaDoubleComplex_const_ptr dA, magma_int_t ldda,
    magmaDoubleComplex_const_ptr dx, magma_int_t incx, magmaDoubleComplex beta,
    magmaDoubleComplex_ptr dy, magma_int_t incy, magma_queue_t queue )
{
    blasf77_zgemv( lapack_trans_const(transA), &m, &n, &alpha, dA, &ldda, dx, 
    	&incx, &beta, dy, &incy);    
}
