/*
    -- MAGMA (version 2.0) --
       Univ. of Tennessee, Knoxville
       Univ. of California, Berkeley
       Univ. of Colorado, Denver
       @date

       @precisions normal z -> c d s
       @author Stephen Wood

*/
#include "magmasparse_internal.h"

/*******************************************************************************
    Purpose
    -------
    
    This routine computes a dot product of a conjugated vector, dx, with
    another vector, dy, on the CPU.
    
    Arguments
    ---------
    @param[in]
    n           magma_int_t
                the number of elements in vectors x and y.
                
    @param[in]
    dx          magmaDoubleComplex_const_ptr
                input vector dx.
                
    @param[in]
    incx        magma_int_t
                the increment for the elements of dx.
                
    @param[in,out]
    dy          magmaDoubleComplex_const_ptr
                input/output vector dy.
                
    @param[in]
    incy        magma_int_t
                the increment for the elements of dy.
                
    @param[in]
    queue       magma_queue_t
                Queue to execute in.

    @ingroup magmasparse_zblas
*******************************************************************************/

extern "C" 
magmaDoubleComplex
magma_zdotc_q(
    magma_int_t n,
    magmaDoubleComplex_const_ptr dx, magma_int_t incx,
    magmaDoubleComplex_const_ptr dy, magma_int_t incy,
    magma_queue_t queue )
{                    
	magmaDoubleComplex result;
    //blasf77_zdotc_sub(&n, dx, &incx, dy, &incy, &result); 
    // A squential implementation for now
    result = magma_cblas_zdotc(n, dx, incx, dy, incy);
    
    return result;
}
