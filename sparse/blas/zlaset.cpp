/*
    -- MAGMA (version 2.0) --
       Univ. of Tennessee, Knoxville
       Univ. of California, Berkeley
       Univ. of Colorado, Denver
       @date

       @precisions normal z -> c d s
       @author Stephen Wood

*/
#include "magmasparse_internal.h"

/*******************************************************************************
    Purpose
    -------
    
    This routine initializes a 2-D array A to beta on the diagonal and
    alpha on the offdiagonals on the CPU.
    
    Arguments
    ---------
    @param[in]
    uplo        magma_uplo_t
                Specifies the part of the matrix dA to be set.
                	= 'U':      Upper triangular part is set. The lower triangle
                	            is unchanged.
                	= 'L':      Lower triangular part is set. The upper triangle
                	            is unchanged.
                Otherwise:  All of the matrix dA is set.
    
    @param[in]
    m           magma_int_t
                the number of rows in matrix dA.

    @param[in]
    n           magma_int_t
                the number of columns in matrix dA.
                
    @param[in]
    alpha       magmaDoubleComplex
                scalar multiplier.
                
    @param[in]
    beta        magmaDoubleComplex
                scalar multiplier.
                
    @param[in]
    dA          magmaDoubleComplex_ptr
                input matrix dA.
                
    @param[in]
    lda         magma_int_t
                The leading dimension of the array dA.  
                lda >= max(1,m)
                
    @param[in]
    queue       magma_queue_t
                Queue to execute in.

    @ingroup magmasparse_zblas
*******************************************************************************/

extern "C" 
void
magmablas_zlaset_q(
    magma_uplo_t uplo, magma_int_t m, magma_int_t n,
    magmaDoubleComplex alpha, magmaDoubleComplex beta,
    magmaDoubleComplex_ptr dA, magma_int_t ldda, 
    magma_queue_t queue )
{
	
    if ( uplo == MagmaUpper ) {
    	for ( magma_int_t j=1; j<n; j++ ) {
    		for ( magma_int_t i=0; i<min(j-1, m); i++ ) {
    			dA[ i+j*n ] = alpha;
    		}
    	}
    	for ( magma_int_t i=0; i<min(m, n); i++ ) {
    		dA[ i+i*n ] = beta;
    	}
    }
    else if ( uplo == MagmaLower ) {
    	for ( magma_int_t j=0; j<min(m, n); j++ ) {
    		for ( magma_int_t i=j+1; i<m; i++ ) {
    			dA[ i+j*n ] = alpha;
    		}
    	}
    	for ( magma_int_t i=0; i<min(m, n); i++ ) {
    		dA[ i+i*n ] = beta;
    	}
    }
    else {
    	for ( magma_int_t j=0; j< n; j++ ) {
    		for ( magma_int_t i=0; i<m; i++ ) {
    			dA[ i+j*n ] = alpha;
    		}
    	}
    	for ( magma_int_t i=0; i<min(m, n); i++ ) {
    		dA[ i+i*n ] = beta;
    	}
    }
    
}
