/*
    -- MAGMA (version 2.0) --
       Univ. of Tennessee, Knoxville
       Univ. of California, Berkeley
       Univ. of Colorado, Denver
       @date

       @precisions normal z -> c d s
       @author Stephen Wood

*/
#include "magmasparse_internal.h"

/*******************************************************************************
    Purpose
    -------
    
    This routine computes the product of a vector, dx, by a scalar, alpha, 
    on the CPU.
    
    Arguments
    ---------
    @param[in]
    n           magma_int_t
                the number of elements in vectors dx.

    @param[in]
    alpha       magmaDoubleComplex
                scalar multiplier.
                
    @param[in]
    dx          magmaDoubleComplex_ptr
                input vector dx.
                
    @param[in]
    incx        magma_int_t
                the increment for the elements of dx.
                
    @param[in]
    queue       magma_queue_t
                Queue to execute in.

    @ingroup magmasparse_zblas
*******************************************************************************/

extern "C" 
void
magma_zscal_q(
    magma_int_t n,
    magmaDoubleComplex alpha,
    magmaDoubleComplex_ptr dx, magma_int_t incx,
    magma_queue_t queue )
{
    blasf77_zscal(&n, &alpha, dx, &incx); 
}
