/*
    -- MAGMA (version 2.0) --
       Univ. of Tennessee, Knoxville
       Univ. of California, Berkeley
       Univ. of Colorado, Denver
       @date

       @precisions normal z -> c d s
       @author Stephen Wood

*/
#include "magmasparse_internal.h"

/*******************************************************************************
    Purpose
    -------
    
    This routine computes dy = alpha *  dx + dy on the CPU.
    
    Arguments
    ---------
    @param[in]
    n           magma_int_t
                the number of elements in vectors dx and dy.

    @param[in]
    alpha       magmaDoubleComplex
                scalar multiplier.
                
    @param[in]
    dx          magmaDoubleComplex_const_ptr
                input vector dx.
                
    @param[in]
    incx        magma_int_t
                the increment for the elements of dx.
                
    @param[in,out]
    dy          magmaDoubleComplex_ptr
                input/output vector dy.
                
    @param[in]
    incy        magma_int_t
                the increment for the elements of dy.
                
    @param[in]
    queue       magma_queue_t
                Queue to execute in.

    @ingroup magmasparse_zblas
*******************************************************************************/

extern "C" 
void
magma_zaxpy_q(
    magma_int_t n,
    magmaDoubleComplex alpha,
    magmaDoubleComplex_const_ptr dx, magma_int_t incx,
    magmaDoubleComplex_ptr       dy, magma_int_t incy,
    magma_queue_t queue )
{                    
    blasf77_zaxpy(&n, &alpha, dx, &incx, dy, &incy); 
}
