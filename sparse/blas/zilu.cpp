/*
    -- MAGMA (version 2.0) --
       Univ. of Tennessee, Knoxville
       Univ. of California, Berkeley
       Univ. of Colorado, Denver
       @date

       @author Hartwig Anzt
       @author Stephen Wood

       @precisions normal z -> s d c
*/
#include "magmasparse_internal.h"
#ifdef MAGMA_WITH_MKL
	#define MKL_Complex8  magmaFloatComplex
	#define MKL_Complex16 magmaDoubleComplex
    #include <mkl_spblas.h>
    #include <mkl_rci.h>
#endif

#define PRECISION_z


/*******************************************************************************
    Purpose
    -------

    Prepares the ILU preconditioner via the MKL iLU for double precision only.
    
    CAUTION: The MKL routine supports only one-based indexing of the array 
    parameters.

    Arguments
    ---------

    @param[in]
    A           magma_z_matrix
                input matrix A

    @param[in,out]
    precond     magma_z_preconditioner*
                preconditioner parameters
    @param[in]
    queue       magma_queue_t
                Queue to execute in.

    @ingroup magmasparse_zgepr
*******************************************************************************/

extern "C" magma_int_t
magma_zcumilusetup(
    magma_z_matrix A,
    magma_z_preconditioner *precond,
    magma_queue_t queue )
{
	magma_int_t info = 0;
 
    // copy matrix into preconditioner parameter
    magma_z_matrix hA={Magma_CSR}, hACSR={Magma_CSR};
    magma_z_matrix hL={Magma_CSR}, hU={Magma_CSR};
    
#if defined PRECISION_d  
    //magmaDoubleComplex one = MAGMA_Z_MAKE(1.0, 0.0);
    magmaDoubleComplex zero = MAGMA_Z_MAKE(0.0, 0.0);
    int n = A.num_rows;
    int ipar[128];
    magmaDoubleComplex dpar[128];
    
    // copy matrix into preconditioner parameter
    CHECK( magma_zmtransfer( A, &hA, A.memory_location, Magma_CPU, queue ));
    CHECK( magma_zmconvert( hA, &hACSR, hA.storage_type, Magma_CSR, queue ));
    
    // in case using fill-in
    if( precond->levels > 0 ){
        magma_z_matrix hAL={Magma_CSR}, hAUt={Magma_CSR};
        CHECK( magma_zsymbilu( &hACSR, precond->levels, &hAL, &hAUt,  queue ));
        magma_zmfree(&hAL, queue);
        magma_zmfree(&hAUt, queue);
    }

    CHECK( magma_zmtransfer(hACSR, &(precond->M), Magma_CPU, Magma_DEV, queue ));

    magma_zmfree( &hA, queue );
    magma_zmfree( &hACSR, queue );

	for (magma_int_t i=0; i<128; i++ ) {
		ipar[i] = 0;
		dpar[i] = zero;
	}
	ipar[0] = n;			// problem size (number of rows)
	ipar[1] = 6;			// error log control 6: to screan
	ipar[2] = 1;			// RCI stage
	ipar[3] = 0;			// iteration count
	ipar[4] = (int) min(150, n); // maximum number of iterations
	ipar[5] = 1; 			// error output switch
	ipar[6] = 1;			// warning output switch
	ipar[7] = 0; 			// dfgmres iteration stopping test switch
	ipar[8] = 1;			// dfgmres residual stopping test switch
	ipar[9] = 1;			// user defined stopping test switch
	ipar[10] = 0; 			// preconditioned dfgmres switch
	ipar[11] = 0;			// zero norm check switch
	ipar[15] = ipar[4];	    // default non-restart GMRES
	ipar[30] = 1;  			// 0: stop if diagonal entry < dpar[30], 1: replace with dpar[31]

	dpar[30] = 1.0e-16;		// small value
	dpar[31] = 1.0e-10;		// value that replaces diagonal elements < dpar[31]
	
	// increment to create one-based indexing of the array parameters
	#pragma omp parallel for
	for (magma_int_t i=0; i<precond->M.nnz; i++) {
		precond->M.dcol[i] += 1;
	}
	#pragma omp parallel for
	for (magma_int_t i=0; i<precond->M.num_rows+1; i++) {
		precond->M.drow[i] += 1;	
	}
	
	//call dcsrilu0(n, a, ia, ja, bilu0, ipar, dpar, ierr)
	dcsrilu0( (int*) &n, A.dval, precond->M.drow, precond->M.dcol, precond->M.dval, 
		ipar, dpar, (int*) &info );
	
	// decrement to create zero-based indexing of the array parameters
	#pragma omp parallel for
	for (magma_int_t i=0; i<precond->M.nnz; i++) {
		precond->M.dcol[i] -= 1;
	}
	#pragma omp parallel for
	for (magma_int_t i=0; i<precond->M.num_rows+1; i++) {
		precond->M.drow[i] -= 1;
	}
	
#else
	info = MAGMA_ERR_NOT_SUPPORTED;
	goto cleanup;
#endif

	CHECK( magma_zmtransfer( precond->M, &hA, Magma_DEV, Magma_CPU, queue ));
    hL.diagorder_type = Magma_UNITY;
    CHECK( magma_zmconvert( hA, &hL , Magma_CSR, Magma_CSRL, queue ));
    hU.diagorder_type = Magma_VALUE;
    CHECK( magma_zmconvert( hA, &hU , Magma_CSR, Magma_CSRU, queue ));
    CHECK( magma_zmtransfer( hL, &(precond->L), Magma_CPU, Magma_DEV, queue ));
    CHECK( magma_zmtransfer( hU, &(precond->U), Magma_CPU, Magma_DEV, queue ));
    
    if( precond->trisolver == 0 || precond->trisolver == Magma_CUSOLVE ){
		// increment to create one-based indexing of the array parameters
		#pragma omp parallel for
		for (magma_int_t i=0; i<precond->L.nnz; i++) {
			precond->L.dcol[i] += 1;
		}
		#pragma omp parallel for
		for (magma_int_t i=0; i<precond->L.num_rows+1; i++) {
			precond->L.drow[i] += 1;	
		}
		#pragma omp parallel for
		for (magma_int_t i=0; i<precond->U.nnz; i++) {
			precond->U.dcol[i] += 1;
		}
		#pragma omp parallel for
		for (magma_int_t i=0; i<precond->U.num_rows+1; i++) {
			precond->U.drow[i] += 1;	
		}
	}
    
    if( precond->trisolver != 0 && precond->trisolver != Magma_CUSOLVE ){
        //prepare for iterative solves
        
        // extract the diagonal of L into precond->d
        CHECK( magma_zjacobisetup_diagscal( precond->L, &precond->d, queue ));
        // precond->d.memory_location = Magma_DEV;
        CHECK( magma_zvinit( &precond->work1, Magma_DEV, hA.num_rows, 1, MAGMA_Z_ZERO, queue ));
        
        // extract the diagonal of U into precond->d2
        CHECK( magma_zjacobisetup_diagscal( precond->U, &precond->d2, queue ));
        // precond->d2.memory_location = Magma_DEV;
        CHECK( magma_zvinit( &precond->work2, Magma_DEV, hA.num_rows, 1, MAGMA_Z_ZERO, queue ));
    }
        
    
cleanup:
    magma_zmfree( &hA, queue );
    magma_zmfree( &hACSR, queue );
    magma_zmfree(&hA, queue );
    magma_zmfree(&hL, queue );
    magma_zmfree(&hU, queue );

    return info;
}


/*******************************************************************************
    Purpose
    -------

    Prepares the ILU transpose preconditioner via the MKL iLIU.

    Arguments
    ---------

    @param[in]
    A           magma_z_matrix
                input matrix A

    @param[in,out]
    precond     magma_z_preconditioner*
                preconditioner parameters
    @param[in]
    queue       magma_queue_t
                Queue to execute in.

    @ingroup magmasparse_zgepr
*******************************************************************************/

extern "C" magma_int_t
magma_zcumilusetup_transpose(
    magma_z_matrix A,
    magma_z_preconditioner *precond,
    magma_queue_t queue )
{
    magma_int_t info = 0;
    magma_z_matrix Ah1={Magma_CSR}, Ah2={Magma_CSR};
    
    if( precond->trisolver == 0 || precond->trisolver == Magma_CUSOLVE ){
		// increment to create one-based indexing of the array parameters
		#pragma omp parallel for
		for (magma_int_t i=0; i<precond->L.nnz; i++) {
			precond->L.dcol[i] -= 1;
		}
		#pragma omp parallel for
		for (magma_int_t i=0; i<precond->L.num_rows+1; i++) {
			precond->L.drow[i] -= 1;	
		}
		#pragma omp parallel for
		for (magma_int_t i=0; i<precond->U.nnz; i++) {
			precond->U.dcol[i] -= 1;
		}
		#pragma omp parallel for
		for (magma_int_t i=0; i<precond->U.num_rows+1; i++) {
			precond->U.drow[i] -= 1;	
		}
	}
    
    // transpose the matrix
    CHECK( magma_zmtransfer( precond->L, &Ah1, Magma_DEV, Magma_CPU, queue ));
    magma_zmconvert( Ah1, &Ah2, A.storage_type, Magma_CSR, queue );
    magma_zmfree(&Ah1, queue );
    magma_zmtransposeconjugate( Ah2, &Ah1, queue );
    magma_zmfree(&Ah2, queue );
    Ah2.blocksize = A.blocksize;
    Ah2.alignment = A.alignment;
    magma_zmconvert( Ah1, &Ah2, Magma_CSR, A.storage_type, queue );
    magma_zmfree(&Ah1, queue );
    magma_zmtransfer( Ah2, &(precond->LT), Magma_CPU, Magma_DEV, queue );
    magma_zmfree(&Ah2, queue );
    
    magma_zmtransfer( precond->U, &Ah1, Magma_DEV, Magma_CPU, queue );
    magma_zmconvert( Ah1, &Ah2, A.storage_type, Magma_CSR, queue );
    magma_zmfree(&Ah1, queue );
    magma_zmtransposeconjugate( Ah2, &Ah1, queue );
    magma_zmfree(&Ah2, queue );
    Ah2.blocksize = A.blocksize;
    Ah2.alignment = A.alignment;
    magma_zmconvert( Ah1, &Ah2, Magma_CSR, A.storage_type, queue );
    magma_zmfree(&Ah1, queue );
    magma_zmtransfer( Ah2, &(precond->UT), Magma_CPU, Magma_DEV, queue );
    magma_zmfree(&Ah2, queue );
    
    if( precond->trisolver == 0 || precond->trisolver == Magma_CUSOLVE ){
		// increment to create one-based indexing of the array parameters
		#pragma omp parallel for
		for (magma_int_t i=0; i<precond->L.nnz; i++) {
			precond->L.dcol[i] += 1;
		}
		#pragma omp parallel for
		for (magma_int_t i=0; i<precond->L.num_rows+1; i++) {
			precond->L.drow[i] += 1;	
		}
		#pragma omp parallel for
		for (magma_int_t i=0; i<precond->U.nnz; i++) {
			precond->U.dcol[i] += 1;
		}
		#pragma omp parallel for
		for (magma_int_t i=0; i<precond->U.num_rows+1; i++) {
			precond->U.drow[i] += 1;	
		}
        // increment to create one-based indexing of the array parameters
		#pragma omp parallel for
		for (magma_int_t i=0; i<precond->LT.nnz; i++) {
			precond->LT.dcol[i] += 1;
		}
		#pragma omp parallel for
		for (magma_int_t i=0; i<precond->LT.num_rows+1; i++) {
			precond->LT.drow[i] += 1;	
		}
		#pragma omp parallel for
		for (magma_int_t i=0; i<precond->UT.nnz; i++) {
			precond->UT.dcol[i] += 1;
		}
		#pragma omp parallel for
		for (magma_int_t i=0; i<precond->UT.num_rows+1; i++) {
			precond->UT.drow[i] += 1;	
		}
	}

cleanup:
    magma_zmfree(&Ah1, queue );
    magma_zmfree(&Ah2, queue );

    return info;
}



/*******************************************************************************
    Purpose
    -------

    Prepares the ILU triangular solves via MKL using an ILU factorization
    matrix stored either in precond->M or on the device as
    precond->L and precond->U.

    Arguments
    ---------

    @param[in,out]
    precond     magma_z_preconditioner*
                preconditioner parameters
    @param[in]
    queue       magma_queue_t
                Queue to execute in.

    @ingroup magmasparse_zgepr
*******************************************************************************/


extern "C" magma_int_t
magma_zcumilugeneratesolverinfo(
    magma_z_preconditioner *precond,
    magma_queue_t queue )
{
	printf("\nmagma_zcumilugeneratesolverinfo did nothing.\n");
    return MAGMA_SUCCESS;
}


/*******************************************************************************
    Purpose
    -------

    Performs the left triangular solves using the ILU preconditioner.
		L*x = b

    Arguments
    ---------

    @param[in]
    b           magma_z_matrix
                RHS

    @param[in,out]
    x           magma_z_matrix*
                vector to precondition

    @param[in,out]
    precond     magma_z_preconditioner*
                preconditioner parameters
    @param[in]
    queue       magma_queue_t
                Queue to execute in.

    @ingroup magmasparse_zgepr
*******************************************************************************/

extern "C" magma_int_t
magma_zapplycumilu_l(
    magma_z_matrix b,
    magma_z_matrix *x,
    magma_z_preconditioner *precond,
    magma_queue_t queue )
{
	
	magma_uplo_t uplo = MagmaLower;
    magma_trans_t transa = MagmaNoTrans;
    magma_diag_t diag = MagmaNonUnit;
    
    //call mkl_zcsrtrsv(uplo, transa, diag, m, a, ia, ja, x, y)
    // solves A*y = x in mkl's notation
    mkl_zcsrtrsv( lapack_uplo_const(uplo), lapack_trans_const(transa),
    	lapack_diag_const(diag), &precond->L.num_rows, precond->L.dval, 
    	precond->L.drow, precond->L.dcol, b.dval, x->dval );
    
    return MAGMA_SUCCESS;
}



/*******************************************************************************
    Purpose
    -------

    Performs the left triangular solves using the transpose ILU preconditioner.
    	L'*x = b
    	
    Arguments
    ---------

    @param[in]
    b           magma_z_matrix
                RHS

    @param[in,out]
    x           magma_z_matrix*
                vector to precondition

    @param[in,out]
    precond     magma_z_preconditioner*
                preconditioner parameters
    @param[in]
    queue       magma_queue_t
                Queue to execute in.

    @ingroup magmasparse_zgepr
*******************************************************************************/
   
extern "C" magma_int_t
magma_zapplycumilu_l_transpose(
    magma_z_matrix b,
    magma_z_matrix *x,
    magma_z_preconditioner *precond,
    magma_queue_t queue )
{
	
	magma_uplo_t uplo = MagmaLower;
    magma_trans_t transa = MagmaTrans;
    magma_diag_t diag = MagmaNonUnit;
    
    //call mkl_zcsrtrsv(uplo, transa, diag, m, a, ia, ja, x, y)
    // solves A'*y = x in mkl's notation
    mkl_zcsrtrsv( lapack_uplo_const(uplo), lapack_trans_const(transa),
    	lapack_diag_const(diag), &precond->L.num_rows, precond->L.dval, 
    	precond->L.drow, precond->L.dcol, b.dval, x->dval );
    
	return MAGMA_SUCCESS;
	
}


/*******************************************************************************
    Purpose
    -------

    Performs the right triangular solves using the ILU preconditioner.
    	U*x = b
    	
    Arguments
    ---------

    @param[in]
    b           magma_z_matrix
                RHS

    @param[in,out]
    x           magma_z_matrix*
                vector to precondition

    @param[in,out]
    precond     magma_z_preconditioner*
                preconditioner parameters
    @param[in]
    queue       magma_queue_t
                Queue to execute in.

    @ingroup magmasparse_zgepr
*******************************************************************************/

extern "C" magma_int_t
magma_zapplycumilu_r(
    magma_z_matrix b,
    magma_z_matrix *x,
    magma_z_preconditioner *precond,
    magma_queue_t queue )
{
	magma_uplo_t uplo = MagmaUpper;
    magma_trans_t transa = MagmaNoTrans;
    magma_diag_t diag = MagmaNonUnit;
    
    //call mkl_zcsrtrsv(uplo, transa, diag, m, a, ia, ja, x, y)
    // solves A'*y = x in mkl's notation
    mkl_zcsrtrsv( lapack_uplo_const(uplo), lapack_trans_const(transa),
    	lapack_diag_const(diag), &precond->U.num_rows, precond->U.dval, 
    	precond->U.drow, precond->U.dcol, b.dval, x->dval );
    
	return MAGMA_SUCCESS;
}


/*******************************************************************************
    Purpose
    -------

    Performs the right triangular solves using the transpose ILU preconditioner.
    	U'*x = b
    	
    Arguments
    ---------

    @param[in]
    b           magma_z_matrix
                RHS

    @param[in,out]
    x           magma_z_matrix*
                vector to precondition

    @param[in,out]
    precond     magma_z_preconditioner*
                preconditioner parameters
    @param[in]
    queue       magma_queue_t
                Queue to execute in.

    @ingroup magmasparse_zgepr
*******************************************************************************/

extern "C" magma_int_t
magma_zapplycumilu_r_transpose(
    magma_z_matrix b,
    magma_z_matrix *x,
    magma_z_preconditioner *precond,
    magma_queue_t queue )
{
	magma_uplo_t uplo = MagmaUpper;
    magma_trans_t transa = MagmaTrans;
    magma_diag_t diag = MagmaNonUnit;
    
    //call mkl_zcsrtrsv(uplo, transa, diag, m, a, ia, ja, x, y)
    // solves A'*y = x in mkl's notation
    mkl_zcsrtrsv( lapack_uplo_const(uplo), lapack_trans_const(transa),
    	lapack_diag_const(diag), &precond->U.num_rows, precond->U.dval, 
    	precond->U.drow, precond->U.dcol, b.dval, x->dval );
    
	return MAGMA_SUCCESS;
}




/*******************************************************************************
    Purpose
    -------

    Prepares the IC preconditioner via cuSPARSE.

    Arguments
    ---------

    @param[in]
    A           magma_z_matrix
                input matrix A

    @param[in,out]
    precond     magma_z_preconditioner*
                preconditioner parameters
    @param[in]
    queue       magma_queue_t
                Queue to execute in.

    @ingroup magmasparse_zhepr
*******************************************************************************/

extern "C" magma_int_t
magma_zcumiccsetup(
    magma_z_matrix A,
    magma_z_preconditioner *precond,
    magma_queue_t queue )
{
    magma_int_t info = 0;
    
    // MKL has no Incomplete Cholesky
    // Idea: generate an ILU and then symmetrize the incomplete factorization
    
    // make sure we have zero-based pointers/col-indices
    magma_solver_type tmpsolver = precond->trisolver;
    precond->trisolver = Magma_ISAI;
    
    magmaDoubleComplex *diags;
    CHECK( magma_zmalloc_cpu( &diags, A.num_rows ) );
    
    // generate ILU factors
    info = magma_zcumilusetup( A, precond, queue );
    
    
    // extract the diagonal and take the square root
    #pragma omp parallel for
    for (magma_int_t i=0; i<precond->U.num_rows; i++) {
        diags[i] = magma_zsqrt( precond->U.dval[ precond->U.drow[i] ] );    
    }
    
    // now symmetrize
    // scale all U elements by dividing with sq_diag
    #pragma omp parallel for
    for (magma_int_t i=0; i<precond->L.nnz; i++) {
        precond->L.dval[ i ] = precond->L.dval[ i ] * diags[ precond->L.dcol[ i ] ];
    }
    magma_zmfree( &precond->U, queue );
    magma_zmtranspose( precond->L, &precond->U, queue );
    
    precond->trisolver = tmpsolver;
    
    // now we need to get back to 1-indexing in case of CUSOLVE
    if( precond->trisolver == 0 || precond->trisolver == Magma_CUSOLVE ){
		// increment to create one-based indexing of the array parameters
		#pragma omp parallel for
		for (magma_int_t i=0; i<precond->L.nnz; i++) {
			precond->L.dcol[i] += 1;
		}
		#pragma omp parallel for
		for (magma_int_t i=0; i<precond->L.num_rows+1; i++) {
			precond->L.drow[i] += 1;	
		}
		#pragma omp parallel for
		for (magma_int_t i=0; i<precond->U.nnz; i++) {
			precond->U.dcol[i] += 1;
		}
		#pragma omp parallel for
		for (magma_int_t i=0; i<precond->U.num_rows+1; i++) {
			precond->U.drow[i] += 1;	
		}
	}
	
cleanup:
    magma_free_cpu( diags );
    return info; 
}

/*******************************************************************************
    Purpose
    -------

    Prepares the IC preconditioner solverinfo via cuSPARSE for a triangular
    matrix present on the device in precond->M.

    Arguments
    ---------
    
    @param[in,out]
    precond     magma_z_preconditioner*
                preconditioner parameters
    @param[in]
    queue       magma_queue_t
                Queue to execute in.

    @ingroup magmasparse_zhepr
*******************************************************************************/

extern "C" magma_int_t
magma_zcumicgeneratesolverinfo(
    magma_z_preconditioner *precond,
    magma_queue_t queue )
{
    return MAGMA_ERR_NOT_SUPPORTED; 
}



/*******************************************************************************
    Purpose
    -------

    Performs the left triangular solves using the ICC preconditioner.

    Arguments
    ---------

    @param[in]
    b           magma_z_matrix
                RHS

    @param[in,out]
    x           magma_z_matrix*
                vector to precondition

    @param[in,out]
    precond     magma_z_preconditioner*
                preconditioner parameters
    @param[in]
    queue       magma_queue_t
                Queue to execute in.

    @ingroup magmasparse_zhepr
*******************************************************************************/

extern "C" magma_int_t
magma_zapplycumicc_l(
    magma_z_matrix b,
    magma_z_matrix *x,
    magma_z_preconditioner *precond,
    magma_queue_t queue )
{
	magma_uplo_t uplo = MagmaLower;
    magma_trans_t transa = MagmaNoTrans;
    magma_diag_t diag = MagmaNonUnit;
    
    //call mkl_zcsrtrsv(uplo, transa, diag, m, a, ia, ja, x, y)
    // solves A*y = x in mkl's notation
    mkl_zcsrtrsv( lapack_uplo_const(uplo), lapack_trans_const(transa),
    	lapack_diag_const(diag), &precond->L.num_rows, precond->L.dval, 
    	precond->L.drow, precond->L.dcol, b.dval, x->dval );
    
    return MAGMA_SUCCESS;
}




/*******************************************************************************
    Purpose
    -------

    Performs the right triangular solves using the ICC preconditioner.

    Arguments
    ---------

    @param[in]
    b           magma_z_matrix
                RHS

    @param[in,out]
    x           magma_z_matrix*
                vector to precondition

    @param[in,out]
    precond     magma_z_preconditioner*
                preconditioner parameters
    @param[in]
    queue       magma_queue_t
                Queue to execute in.

    @ingroup magmasparse_zhepr
*******************************************************************************/

extern "C" magma_int_t
magma_zapplycumicc_r(
    magma_z_matrix b,
    magma_z_matrix *x,
    magma_z_preconditioner *precond,
    magma_queue_t queue )
{
	magma_uplo_t uplo = MagmaUpper;
    magma_trans_t transa = MagmaNoTrans;
    magma_diag_t diag = MagmaNonUnit;
    
    //call mkl_zcsrtrsv(uplo, transa, diag, m, a, ia, ja, x, y)
    // solves A'*y = x in mkl's notation
    mkl_zcsrtrsv( lapack_uplo_const(uplo), lapack_trans_const(transa),
    	lapack_diag_const(diag), &precond->U.num_rows, precond->U.dval, 
    	precond->U.drow, precond->U.dcol, b.dval, x->dval );
    
	return MAGMA_SUCCESS;
}