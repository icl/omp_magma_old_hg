/*
    -- MAGMA (version 2.0) --
       Univ. of Tennessee, Knoxville
       Univ. of California, Berkeley
       Univ. of Colorado, Denver
       @date

       @precisions normal z -> c d s
       @author Hartwig Anzt
       @author Stephen Wood
*/

#include "magmasparse_internal.h"

#define PRECISION_z

/*******************************************************************************
    Purpose
    -------
    
    This routine iteratively computes an incomplete LU factorization.
    The idea is according to Edmond Chow's presentation at SIAM 2014.
    This routine was used in the ISC 2015 paper:
    E. Chow et al.: 'Study of an Asynchronous Iterative Algorithm
                     for Computing Incomplete Factorizations on GPUs'
 
    The input format of the matrix is Magma_CSRCOO for the upper and lower 
    triangular parts. Note however, that we flip col and rowidx for the 
    U-part.
    Every component of L and U is handled by one thread. 

    Arguments
    ---------

    @param[in]
    A           magma_z_matrix
                input matrix A determing initial guess & processing order

    @param[in,out]
    L           magma_z_matrix
                input/output matrix L containing the ILU approximation

    @param[in,out]
    U           magma_z_matrix
                input/output matrix U containing the ILU approximation
                              
    @param[in]
    queue       magma_queue_t
                Queue to execute in.
                
    @ingroup magmasparse_zgegpuk
*******************************************************************************/

extern "C" magma_int_t
magma_zparilu_csr( 
    magma_z_matrix A,
    magma_z_matrix L,
    magma_z_matrix U,
    magma_queue_t queue )
{
    int i, j;

    magmaDoubleComplex zero = MAGMA_Z_MAKE(0.0, 0.0);
    magmaDoubleComplex s, sp;
    int il, iu, jl, ju;
    
    sp = zero;
    
    #pragma omp parallel for private(i, j, il, iu, jl, ju, s, sp)
    for (int k=0; k<A.nnz; k++ ) {
        i = A.drowidx[k];
        j = A.dcol[k];
        s = A.dval[k];

        il = L.drow[i];
        iu = U.drow[j];

        while (il < L.drow[i+1] && iu < U.drow[j+1])
        {
            sp = zero;
            jl = L.dcol[il];
            ju = U.dcol[iu];

            // avoid branching
            sp = ( jl == ju ) ? L.dval[il] * U.dval[iu] : sp;
            s = ( jl == ju ) ? s-sp : s;
            il = ( jl <= ju ) ? il+1 : il;
            iu = ( jl >= ju ) ? iu+1 : iu;
        }
        // undo the last operation (it must be the last)
        s += sp;
        
        if ( i > j )      // modify l entry
            L.dval[il-1] =  s / U.dval[U.drow[j+1]-1];
        else {            // modify u entry
            U.dval[iu-1] = s;
        }
    }

    return MAGMA_SUCCESS;
}
