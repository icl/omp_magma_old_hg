/*
    -- MAGMA (version 1.1) --
       Univ. of Tennessee, Knoxville
       Univ. of California, Berkeley
       Univ. of Colorado, Denver
       @date

       @precisions normal z -> c d s
       @author Hartwig Anzt
       @author Stephen Wood

*/
#include "common_magmasparse.h"

#define BLOCK_SIZE 512


#define PRECISION_z

/*******************************************************************************
    Purpose
    -------

    Prepares the Jacobi Iteration according to
       x^(k+1) = D^(-1) * b - D^(-1) * (L+U) * x^k
       x^(k+1) =      c     -       M        * x^k.

    Returns the vector c. It calls a GPU kernel

    Arguments
    ---------

    @param[in]
    num_rows    magma_int_t
                number of rows
                
    @param[in]
    b           magma_z_matrix
                RHS b

    @param[in]
    d           magma_z_matrix
                vector with diagonal entries

    @param[out]
    c           magma_z_matrix*
                c = D^(-1) * b

    @param[out]
    x           magma_z_matrix*
                iteration vector
    @param[in]
    queue       magma_queue_t
                Queue to execute in.

    @ingroup magmasparse_zgegpuk
*******************************************************************************/

extern "C" magma_int_t
magma_zjacobisetup_vector_gpu(
    magma_int_t num_rows, 
    magma_z_matrix b, 
    magma_z_matrix d, 
    magma_z_matrix c,
    magma_z_matrix *x,
    magma_queue_t queue )
{
    int num_vecs = b.num_rows / num_rows;
    
    #pragma omp parallel for
    for ( int row=0; row<num_rows; row++ ){
        for( int i=0; i<num_vecs; i++ ){
            c.dval[row+i*num_rows] = b.dval[row+i*num_rows] / d.dval[row];
            x->val[row+i*num_rows] = c.dval[row+i*num_rows];
        }
    }            

    return MAGMA_SUCCESS;
}


/*******************************************************************************
    Purpose
    -------

    Prepares the Jacobi Iteration according to
       x^(k+1) = D^(-1) * b - D^(-1) * (L+U) * x^k
       x^(k+1) =      c     -       M        * x^k.

    Returns the vector c. It calls a GPU kernel

    Arguments
    ---------

    @param[in]
    num_rows    magma_int_t
                number of rows
                
    @param[in]
    b           magma_z_matrix
                RHS b

    @param[in]
    d           magma_z_matrix
                vector with diagonal entries

    @param[out]
    c           magma_z_matrix*
                c = D^(-1) * b
    @param[in]
    queue       magma_queue_t
                Queue to execute in.

    @ingroup magmasparse_z
*******************************************************************************/

extern "C" magma_int_t
magma_zjacobi_diagscal(
    magma_int_t num_rows, 
    magma_z_matrix d, 
    magma_z_matrix b, 
    magma_z_matrix *c,
    magma_queue_t queue )
{
    int num_vecs = b.num_rows*b.num_cols/num_rows;
    
    #pragma omp parallel for
    for ( int row=0; row<num_rows; row++ ){
        for( int i=0; i<num_vecs; i++)
            c->val[row+i*num_rows] = b.dval[row+i*num_rows] * d.dval[row];
    }
    
    return MAGMA_SUCCESS;
}


/*******************************************************************************
    Purpose
    -------

    Updates the iteration vector x for the Jacobi iteration
    according to
        x=x+d.*(b-t)
    where d is the diagonal of the system matrix A and t=Ax.

    Arguments
    ---------
                
    @param[in]
    t           magma_z_matrix
                t = A*x
                
    @param[in]
    b           magma_z_matrix
                RHS b
                
    @param[in]
    d           magma_z_matrix
                vector with diagonal entries

    @param[out]
    x           magma_z_matrix*
                iteration vector
    @param[in]
    queue       magma_queue_t
                Queue to execute in.

    @ingroup magmasparse_z
*******************************************************************************/

extern "C" magma_int_t
magma_zjacobiupdate(
    magma_z_matrix t, 
    magma_z_matrix b, 
    magma_z_matrix d, 
    magma_z_matrix *x,
    magma_queue_t queue )
{
    #pragma omp parallel for
    for ( int row=0; row<t.num_rows; row++ ){
        for( int i=0; i<t.num_cols; i++)
            x->val[row+i*t.num_rows] += 
        		(b.dval[row+i*t.num_rows]-t.dval[row+i*t.num_rows]) * d.dval[row];
    }
    
    return MAGMA_SUCCESS;
}


/*******************************************************************************
    Purpose
    -------

    Updates the iteration vector x for the Jacobi iteration
    according to
        x=x+d.*(b-Ax)


    Arguments
    ---------

    @param[in]
    maxiter     magma_int_t
                number of Jacobi iterations   
                
    @param[in]
    A           magma_z_matrix
                system matrix
                
    @param[in]
    t           magma_z_matrix
                workspace
                
    @param[in]
    b           magma_z_matrix
                RHS b
                
    @param[in]
    d           magma_z_matrix
                vector with diagonal entries

    @param[out]
    x           magma_z_matrix*
                iteration vector
    @param[in]
    queue       magma_queue_t
                Queue to execute in.

    @ingroup magmasparse_z
*******************************************************************************/

extern "C" magma_int_t
magma_zjacobispmvupdate(
    magma_int_t maxiter,
    magma_z_matrix A,
    magma_z_matrix t, 
    magma_z_matrix b, 
    magma_z_matrix d, 
    magma_z_matrix *x,
    magma_queue_t queue )
{
	
    
    for( magma_int_t it=0; it<maxiter; it++ ) {
        #pragma omp parallel for
		for ( int row=0; row<t.num_rows; row++ ){
			magmaDoubleComplex dot = MAGMA_Z_ZERO;
			int start = A.drow[ row ];
			int end = A.drow[ row+1 ];
			for( int i=0; i<t.num_cols; i++ ){
				for( int j=start; j<end; j++ ){
					dot += A.dval[ j ] * x->val[ A.dcol[j]+i*t.num_rows ];
				}
				x->val[row+i*t.num_rows] += (b.dval[row+i*t.num_rows]-dot) * d.dval[row];
			}
		}
    }

    return MAGMA_SUCCESS;
}


/*******************************************************************************
    Purpose
    -------

    Updates the iteration vector x for the Jacobi iteration
    according to
        x=x+d.*(b-Ax)
    This kernel processes the thread blocks in reversed order.

    Arguments
    ---------

    @param[in]
    maxiter     magma_int_t
                number of Jacobi iterations   
                
    @param[in]
    A           magma_z_matrix
                system matrix
                
    @param[in]
    t           magma_z_matrix
                workspace
                
    @param[in]
    b           magma_z_matrix
                RHS b
                
    @param[in]
    d           magma_z_matrix
                vector with diagonal entries

    @param[out]
    x           magma_z_matrix*
                iteration vector
    @param[in]
    queue       magma_queue_t
                Queue to execute in.

    @ingroup magmasparse_z
*******************************************************************************/

extern "C" magma_int_t
magma_zjacobispmvupdate_bw(
    magma_int_t maxiter,
    magma_z_matrix A,
    magma_z_matrix t, 
    magma_z_matrix b, 
    magma_z_matrix d, 
    magma_z_matrix *x,
    magma_queue_t queue )
{

	#pragma omp parallel for
    for( magma_int_t it=0; it<maxiter; it++ ) {
        #pragma omp parallel for
		for ( int row=t.num_rows-1; row>-1; row-- ){
			magmaDoubleComplex dot = MAGMA_Z_ZERO;
			int start = A.drow[ row ];
			int end = A.drow[ row+1 ];
			for( int i=0; i<t.num_cols; i++){
				for( int j=start; j<end; j++){
					dot += A.dval[ j ] * x->val[ A.dcol[j]+i*t.num_rows ];
				}
				x->val[row+i*t.num_rows] += (b.dval[row+i*t.num_rows]-dot) * d.dval[row];
			}
		}
    }

    return MAGMA_SUCCESS;
}


/*******************************************************************************
    Purpose
    -------

    Updates the iteration vector x for the Jacobi iteration
    according to
        x=x+d.*(b-Ax)
        
    This kernel allows for overlapping domains: the indices-array contains
    the locations that are updated. Locations may be repeated to simulate
    overlapping domains.


    Arguments
    ---------

    @param[in]
    maxiter     magma_int_t
                number of Jacobi iterations
                
    @param[in]
    num_updates magma_int_t
                number of updates - length of the indices array
                    
    @param[in]
    indices     magma_index_t*
                indices, which entries of x to update
                
    @param[in]
    A           magma_z_matrix
                system matrix
                
    @param[in]
    t           magma_z_matrix
                workspace
                
    @param[in]
    b           magma_z_matrix
                RHS b
                
    @param[in]
    d           magma_z_matrix
                vector with diagonal entries
   
    @param[in]
    tmp         magma_z_matrix
                workspace

    @param[out]
    x           magma_z_matrix*
                iteration vector
    @param[in]
    queue       magma_queue_t
                Queue to execute in.

    @ingroup magmasparse_z
*******************************************************************************/

extern "C" magma_int_t
magma_zjacobispmvupdateselect(
    magma_int_t maxiter,
    magma_int_t num_updates,
    magma_index_t *indices,
    magma_z_matrix A,
    magma_z_matrix t, 
    magma_z_matrix b, 
    magma_z_matrix d, 
    magma_z_matrix tmp, 
    magma_z_matrix *x,
    magma_queue_t queue )
{

    for( magma_int_t it=0; it<maxiter; it++ ) {
        #pragma omp parallel for
        for ( int idx=0; idx<num_updates; idx++ ){
        	int row = indices[ idx ];
			printf(" ");    // why???
			magmaDoubleComplex dot = MAGMA_Z_ZERO;
			int start = A.drow[ row ];
			int end = A.drow[ row+1 ];
			for( int i=0; i<t.num_cols; i++){
				for( int j=start; j<end; j++){
					dot += A.dval[ j ] * x->val[ A.dcol[j]+i*t.num_rows ];
				}
				x->val[row+i*t.num_rows] = x->val[row+i*t.num_rows] + (b.dval[row+i*t.num_rows]-dot) * d.dval[row];
	
			}
		}
        
    }
    
    return MAGMA_SUCCESS;
}

/*******************************************************************************
    Purpose
    -------

    Computes the contraction coefficients c_i:
    
    c_i = z_i^{k-1} / z_i^{k} 
        
        = | x_i^{k-1} - x_i^{k-2} | / |  x_i^{k} - x_i^{k-1} |

    Arguments
    ---------

    @param[in]
    xkm2        magma_z_matrix
                vector x^{k-2}
                
    @param[in]
    xkm1        magma_z_matrix
                vector x^{k-2}
                
    @param[in]
    xk          magma_z_matrix
                vector x^{k-2}
   
    @param[out]
    z           magma_z_matrix*
                ratio
                
    @param[out]
    c           magma_z_matrix*
                contraction coefficients
                
    @param[in]
    queue       magma_queue_t
                Queue to execute in.

    @ingroup magmasparse_z
*******************************************************************************/

extern "C" magma_int_t
magma_zftjacobicontractions(
    magma_z_matrix xkm2,
    magma_z_matrix xkm1, 
    magma_z_matrix xk, 
    magma_z_matrix *z,
    magma_z_matrix *c,
    magma_queue_t queue )
{
    
    #pragma omp parallel for
    for ( int idx=0; idx<xkm2.num_rows; idx++ ){
        z->dval[idx] = MAGMA_Z_MAKE( MAGMA_Z_ABS( xkm1.dval[idx] - xk.dval[idx] ), 0.0);
        c->dval[ idx ] = MAGMA_Z_MAKE(
            MAGMA_Z_ABS( xkm2.dval[idx] - xkm1.dval[idx] ) 
                / MAGMA_Z_ABS( xkm1.dval[idx] - xk.dval[idx] ) 
                , 0.0 );
    }
    
    return MAGMA_SUCCESS;
}


/*******************************************************************************
    Purpose
    -------

    Checks the Jacobi updates according to the condition in the ScaLA'15 paper.

    Arguments
    ---------
    
    @param[in]
    delta       double
                threshold

    @param[in,out]
    xold        magma_z_matrix*
                vector xold
                
    @param[in,out]
    xnew        magma_z_matrix*
                vector xnew
                
    @param[in,out]
    zprev       magma_z_matrix*
                vector z = | x_k-1 - x_k |
   
    @param[in]
    c           magma_z_matrix
                contraction coefficients
                
    @param[in,out]
    flag_t      magma_int_t
                threshold condition
                
    @param[in,out]
    flag_fp     magma_int_t
                false positive condition
                
    @param[in]
    queue       magma_queue_t
                Queue to execute in.

    @ingroup magmasparse_z
*******************************************************************************/

extern "C" magma_int_t
magma_zftjacobiupdatecheck(
    double delta,
    magma_z_matrix *xold,
    magma_z_matrix *xnew, 
    magma_z_matrix *zprev, 
    magma_z_matrix c,
    magma_int_t *flag_t,
    magma_int_t *flag_fp,
    magma_queue_t queue )
{
    
    #pragma omp parallel for
    for ( int idx=0; idx<xold->num_rows; idx++ ){
        double t1 = delta * MAGMA_Z_ABS(c.dval[idx]);
        double  vkv = 1.0;
        for( magma_int_t i=0; i<min( flag_fp[idx], 100 ); i++){
            vkv = vkv*2;
        }
        magmaDoubleComplex xold_l = xold->dval[idx];
        magmaDoubleComplex xnew_l = xnew->dval[idx];
        magmaDoubleComplex znew = MAGMA_Z_MAKE(
                        max( MAGMA_Z_ABS( xold_l - xnew_l), 1e-15), 0.0 );
                        
        magmaDoubleComplex znr = zprev->dval[idx] / znew; 
        double t2 = MAGMA_Z_ABS( znr - c.dval[idx] );
        
        //% evaluate fp-cond
        magma_int_t fpcond = 0;
        if( MAGMA_Z_ABS(znr)>vkv ){
            fpcond = 1;
        }
        
        // % combine t-cond and fp-cond + flag_t == 1
        magma_int_t cond = 0;
        if( t2<t1 || (flag_t[idx]>0 && fpcond > 0 ) ){
            cond = 1;
        }
        flag_fp[idx] = flag_fp[idx]+1;
        if( fpcond>0 ){
            flag_fp[idx] = 0;
        }
        if( cond > 0 ){
            flag_t[idx] = 0;
            zprev->dval[idx] = znew;
            xold->dval[idx] = xnew_l;
        } else {
            flag_t[idx] = 1;
            xnew->dval[idx] = xold_l;
        }
    }
    
    return MAGMA_SUCCESS;
}
