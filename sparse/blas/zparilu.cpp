/*
    -- MAGMA (version 2.0) --
       Univ. of Tennessee, Knoxville
       Univ. of California, Berkeley
       Univ. of Colorado, Denver
       @date

       @author Hartwig Anzt

       @precisions normal z -> s d c
*/
#include "magmasparse_internal.h"

#include "magmasparse_internal.h"
#ifdef _OPENMP
#include <omp.h>
#endif

#define PRECISION_z


/**
    Purpose
    -------

    Prepares the Parallel Incomplete LU preconditioner. 
    
    This function requires OpenMP, and is only available if OpenMP is activated. 

    Arguments
    ---------

    @param[in]
    A           magma_z_matrix
                input matrix A
                
    @param[in]
    b           magma_z_matrix
                input RHS b

    @param[in,out]
    precond     magma_z_preconditioner*
                preconditioner parameters
                
    @param[in]
    queue       magma_queue_t
                Queue to execute in.

    @ingroup magmasparse_zgepr
    ********************************************************************/
extern "C"
magma_int_t
magma_zparilusetup(
    magma_z_matrix A,
    magma_z_matrix b,
    magma_z_preconditioner *precond,
    magma_queue_t queue )
{
    magma_int_t info = 0;
    real_Double_t start, end;
                    
#ifdef _OPENMP
    int tid = omp_get_thread_num();

    magma_z_matrix hA={Magma_CSR}, hAT={Magma_CSR}, hL={Magma_CSR}, hU={Magma_CSR}, 
                    L={Magma_CSR}, U={Magma_CSR};
    
    CHECK( magma_zmtransfer( A, &hA, A.memory_location, Magma_CPU, queue ));
    
    // in case using fill-in
    if( precond->levels > 0 ){
        CHECK( magma_zsymbilu( &hA, precond->levels, &hL, &hU , queue ));
    }
    // need only lower triangular
    magma_zmfree(&hU, queue );
    magma_zmfree(&hL, queue );
    magma_zmconvert( hA, &hL, Magma_CSR, Magma_CSRL, queue );
    magma_zmconvert( hL, &L, Magma_CSR, Magma_CSRLIST, queue );
    
    magma_zmtranspose(hA, &hAT, queue );
    magma_zmconvert( hAT, &hU, Magma_CSR, Magma_CSRL, queue );
    magma_zmconvert( hU, &U, Magma_CSR, Magma_CSRLIST, queue );
    
    start = magma_sync_wtime( queue );
    //##########################################################################
    for( magma_int_t iters =0; iters<precond->sweeps; iters++ ) {
        magma_zparilut_sweep_list( &hA, &L, &U, queue );
    }
    end = magma_sync_wtime( queue );
    
    precond->setuptime = end-start;
    
    magma_zmconvert( L, &hL, Magma_CSRLIST, Magma_CSR, queue );
    magma_zmconvert( U, &hU, Magma_CSRLIST, Magma_CSR, queue );
    magma_zmtranspose(hU, &hAT, queue );
    
    CHECK( magma_zmtransfer( hL, &precond->L, Magma_CPU, Magma_DEV , queue ));
    CHECK( magma_zmtransfer( hAT, &precond->U, Magma_CPU, Magma_DEV , queue )); 
    
    if( precond->trisolver == 0 || precond->trisolver == Magma_CUSOLVE ){
		// increment to create one-based indexing of the array parameters
		#pragma omp parallel for
		for (magma_int_t i=0; i<precond->L.nnz; i++) {
			precond->L.dcol[i] += 1;
		}
		#pragma omp parallel for
		for (magma_int_t i=0; i<precond->L.num_rows+1; i++) {
			precond->L.drow[i] += 1;	
		}
		#pragma omp parallel for
		for (magma_int_t i=0; i<precond->U.nnz; i++) {
			precond->U.dcol[i] += 1;
		}
		#pragma omp parallel for
		for (magma_int_t i=0; i<precond->U.num_rows+1; i++) {
			precond->U.drow[i] += 1;	
		}
	}
	
	
    if( precond->trisolver != 0 && precond->trisolver != Magma_CUSOLVE ){
        //prepare for iterative solves
        
        // extract the diagonal of L into precond->d
        CHECK( magma_zjacobisetup_diagscal( precond->L, &precond->d, queue ));
        // precond->d.memory_location = Magma_DEV;
        CHECK( magma_zvinit( &precond->work1, Magma_DEV, hA.num_rows, 1, MAGMA_Z_ZERO, queue ));
        
        // extract the diagonal of U into precond->d2
        CHECK( magma_zjacobisetup_diagscal( precond->U, &precond->d2, queue ));
        // precond->d2.memory_location = Magma_DEV;
        CHECK( magma_zvinit( &precond->work2, Magma_DEV, hA.num_rows, 1, MAGMA_Z_ZERO, queue ));
    }
    
    if( precond->trisolver == Magma_JACOBI && precond->pattern == 1 ){
        // dirty workaround for Jacobi trisolves....
        magma_zmfree( &hL, queue );
        magma_zmfree( &hU, queue );
        CHECK( magma_zmtransfer( precond->U, &hU, Magma_DEV, Magma_CPU , queue )); 
        CHECK( magma_zmtransfer( precond->L, &hL, Magma_DEV, Magma_CPU , queue )); 
        
        magma_zmfree( &hAT, queue );
        hAT.diagorder_type = Magma_VALUE;
        CHECK( magma_zmconvert( hL, &hAT , Magma_CSR, Magma_CSRU, queue ));
        #pragma omp parallel for
		for (magma_int_t i=0; i<hAT.nnz; i++) {
			hAT.val[i] = MAGMA_Z_ONE/hAT.val[i];	
		}
		CHECK( magma_zmtransfer( hAT, &(precond->LD), Magma_CPU, Magma_DEV, queue ));
		
        magma_zmfree( &hAT, queue );
        hAT.diagorder_type = Magma_VALUE;
        CHECK( magma_zmconvert( hU, &hAT , Magma_CSR, Magma_CSRL, queue ));
        #pragma omp parallel for
		for (magma_int_t i=0; i<hAT.nnz; i++) {
			hAT.val[i] = MAGMA_Z_ONE/hAT.val[i];	
		}
		CHECK( magma_zmtransfer( hAT, &(precond->UD), Magma_CPU, Magma_DEV, queue ));
    }
    
cleanup:
    magma_zmfree( &hA, queue );
    magma_zmfree( &hAT, queue );
    magma_zmfree( &hL, queue );
    magma_zmfree( &L, queue ); 
    magma_zmfree( &hU, queue );
    magma_zmfree( &U, queue );
    
#endif
    return info;
}
    

/**
    Purpose
    -------

    Updates an existing preconditioner via additional iterative ILU sweeps for
    previous factorization initial guess (PFIG).
    See  Anzt et al., Parallel Computing, 2015.

    Arguments
    ---------
    
    @param[in]
    A           magma_z_matrix
                input matrix A, current target system

    @param[in]
    precond     magma_z_preconditioner*
                preconditioner parameters

    @param[in]
    updates     magma_int_t 
                number of updates
    
    @param[in]
    queue       magma_queue_t
                Queue to execute in.
                
    @ingroup magmasparse_zhepr
    ********************************************************************/
extern "C"
magma_int_t
magma_zpariluupdate(
    magma_z_matrix A,
    magma_z_preconditioner *precond,
    magma_int_t updates,
    magma_queue_t queue )
{
    magma_int_t info = 0;

    magma_z_matrix hALt={Magma_CSR};
    magma_z_matrix d_h={Magma_CSR};
    
    magma_z_matrix hL={Magma_CSR}, hU={Magma_CSR},
    hAcopy={Magma_CSR}, hAL={Magma_CSR}, hAU={Magma_CSR}, hAUt={Magma_CSR},
    hUT={Magma_CSR}, hAtmp={Magma_CSR},
    dL={Magma_CSR}, dU={Magma_CSR};

    if ( updates > 0 ){
        CHECK( magma_zmtransfer( precond->M, &hAcopy, Magma_DEV, Magma_CPU , queue ));
        // in case using fill-in
        CHECK( magma_zsymbilu( &hAcopy, precond->levels, &hAL, &hAUt,  queue ));
        // add a unit diagonal to L for the algorithm
        CHECK( magma_zmLdiagadd( &hAL , queue ));
        // transpose U for the algorithm
        //CHECK( magma_z_cucsrtranspose(  hAUt, &hAU , queue ));
        CHECK( magma_zmtranspose(  hAUt, &hAU , queue ));
        // transfer the factor L and U
        CHECK( magma_zmtransfer( hAL, &dL, Magma_CPU, Magma_DEV , queue ));
        CHECK( magma_zmtransfer( hAU, &dU, Magma_CPU, Magma_DEV , queue ));
        magma_zmfree(&hAL, queue );
        magma_zmfree(&hAU, queue );
        magma_zmfree(&hAUt, queue );
        magma_zmfree(&precond->M, queue );
        magma_zmfree(&hAcopy, queue );
        
        // copy original matrix as CSRCOO to device
        for(int i=0; i<updates; i++){
            CHECK( magma_zparilu_csr( A, dL, dU, queue ));
        }
        CHECK( magma_zmtransfer( dL, &hL, Magma_DEV, Magma_CPU , queue ));
        CHECK( magma_zmtransfer( dU, &hU, Magma_DEV, Magma_CPU , queue ));
        //CHECK( magma_z_cucsrtranspose(  hU, &hUT , queue ));
        CHECK( magma_zmtranspose(  hU, &hUT , queue ));
        magma_zmfree(&dL, queue );
        magma_zmfree(&dU, queue );
        magma_zmfree(&hU, queue );
        CHECK( magma_zmlumerge( hL, hUT, &hAtmp, queue ));
        // for CUSPARSE
        CHECK( magma_zmtransfer( hAtmp, &precond->M, Magma_CPU, Magma_DEV , queue ));
        
        magma_zmfree(&hL, queue );
        magma_zmfree(&hUT, queue );
        hAL.diagorder_type = Magma_UNITY;
        CHECK( magma_zmconvert(hAtmp, &hAL, Magma_CSR, Magma_CSRL, queue ));
        hAL.storage_type = Magma_CSR;
        CHECK( magma_zmconvert(hAtmp, &hAU, Magma_CSR, Magma_CSRU, queue ));
        hAU.storage_type = Magma_CSR;
        
        magma_zmfree(&hAtmp, queue );
        CHECK( magma_zmtransfer( hAL, &precond->L, Magma_CPU, Magma_DEV , queue ));
        CHECK( magma_zmtransfer( hAU, &precond->U, Magma_CPU, Magma_DEV , queue ));
        magma_zmfree(&hAL, queue );
        magma_zmfree(&hAU, queue );
    
        magma_zmfree( &precond->d , queue );
        magma_zmfree( &precond->d2 , queue );
        
        CHECK( magma_zjacobisetup_diagscal( precond->L, &precond->d, queue ));
        CHECK( magma_zjacobisetup_diagscal( precond->U, &precond->d2, queue ));
    }

cleanup:
    magma_zmfree(&d_h, queue );
    magma_zmfree(&hALt, queue );
    
    return info;
}

