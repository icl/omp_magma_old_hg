/*
    -- MAGMA (version 2.0) --
       Univ. of Tennessee, Knoxville
       Univ. of California, Berkeley
       Univ. of Colorado, Denver
       @date

       @precisions normal z -> c d s
       @author Stephen Wood
       @author Hartwig Anzt
*/

#include "magmasparse_internal.h"
#ifdef MAGMA_WITH_MKL
	#define MKL_INT magma_int_t
	#define MKL_Complex8  magmaFloatComplex
	#define MKL_Complex16 magmaDoubleComplex
    #include <mkl_spblas.h>
    #include <mkl_rci.h>
    #include <mkl_blas.h>
#endif

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

#define PRECISION_z

/*******************************************************************************
    Purpose
    -------
    
    This routine iteratively computes an incomplete LU factorization.
    The idea is according to Edmond Chow's presentation at SIAM 2014.
    This routine was used in the ISC 2015 paper:
    E. Chow et al.: 'Study of an Asynchronous Iterative Algorithm
                     for Computing Incomplete Factorizations on GPUs'
 
    The input format of the matrix is Magma_CSR for the upper and lower 
    triangular parts. Note however, that we transposed the U-part to 
    achieve CSC.
    
    Square tiles of L and U are updated by individual threads through zgemm 
    operations.  Temporary rectangular dense matrices are formed from 
    subsets of elements in L and U for the zgemm calls.
    
    This is a basic implementation test that prioritizes compatibility with 
    preexisting routines over performance.

    Arguments
    ---------

    @param[in]
    A           magma_z_matrix
                input matrix A determing initial guess & processing order

    @param[in,out]
    L           magma_z_matrix
                input/output matrix L containing the ILU approximation

    @param[in,out]
    U           magma_z_matrix
                input/output matrix U containing the ILU approximation
                              
    @param[in]
    queue       magma_queue_t
                Queue to execute in.
                
    @ingroup magmasparse_zgegpuk
*******************************************************************************/

extern "C" magma_int_t
magma_zparilu_csr_bdense_zgemm( 
    magma_z_matrix *A,
    magma_z_matrix *L,
    magma_z_matrix *U,
    magma_queue_t queue )
{
	magma_int_t info = 0;
#if defined PRECISION_d || defined PRECISION_s
	int i, j;
	int ti, tj, ii, jj;
	
    // TODO: lookup tile size for architecture, number of available threads, 
    // matrix size, and matrix diameter
	int tile = 10;
	int span = A->num_cols;
	magmaDoubleComplex cone = MAGMA_Z_ONE;
	magmaDoubleComplex czero = MAGMA_Z_ZERO;
	
	magma_z_matrix hAc={Magma_CSRCOO}, hLc={Magma_CSR}, hUc={Magma_CSR};
	magma_z_matrix hA={Magma_CSR}, hL={Magma_CSR}, hU={Magma_CSR}, hUT={Magma_CSR};
	magmaDoubleComplex *d1 = NULL;
	
    CHECK( magma_z_mtransfer( *A, &hAc, Magma_DEV, Magma_CPU, queue ) );
    CHECK( magma_z_mtransfer( *L, &hL, Magma_DEV, Magma_CPU, queue ) );
	
    // U and hU are stored in trasposed CSR, effectively in CSC format
    CHECK( magma_z_mtransfer( *U, &hU, Magma_DEV, Magma_CPU, queue ) ); 
    CHECK( magma_zmconvert( hAc, &hA, Magma_CSRCOO, Magma_CSR, queue ) );
    
    hLc.diagorder_type = Magma_UNITY;
    CHECK( magma_zmconvert( hA, &hLc, Magma_CSR, Magma_CSRL, queue ) );
    CHECK( magma_zmconvert( hA, &hUT, Magma_CSR, Magma_CSRU, queue ) );
    magma_zmtranspose(  hUT, &hUc, queue );
    
    magma_zdiameter( &hA, queue );
    magma_zdiameter( &hL, queue );
	magma_zdiameter( &hU, queue );
    // workspace for diagonal
    CHECK( magma_zmalloc( &d1, hA.num_rows ));
    
    // move major diagonal in separate vector   
    // set 0 on major diagonal of L and U
    #pragma omp parallel private(i)
    {
      #pragma omp for schedule(static,1) nowait
      for (i=0; i<hA.num_rows; i++) {
        d1[i] = hU.val[hU.row[i+1]-1];
        hL.val[hL.row[i+1]-1] = czero;
        hU.val[hU.row[i+1]-1] = czero;
      }
    }
    // TODO : unnecessary, temporary L and U blocks do not use the major diagonals
            
	#pragma omp parallel private(ti, tj, i, j, ii, jj) firstprivate(span, tile)
    {
       #pragma omp for schedule(static,1) collapse(2) nowait
       for (ti=0; ti<hA.num_rows; ti += tile) {
         for (tj=0; tj<hA.num_cols; tj += tile) { 
           // TODO : check if this is an active tile 
           // TODO : iterate over a list of active tiles
           	 
           // upper tiles and tiles along major diagonal
           if (ti<=tj) {
           	 magma_int_t maxcol = MIN(ti + tile, hA.num_rows);  
           	 magma_int_t mincol = MAX(0, ti - hA.diameter);
             span = MIN( MIN(ti+tile, hA.diameter+tile), hA.num_rows );
             magmaDoubleComplex Ctile2[sizeof(magmaDoubleComplex) * tile*tile];
             #pragma omp simd
             for( i=0; i<(tile*tile); i++) {
               Ctile2[i] = MAGMA_Z_MAKE(0., 0.);
             }
             magmaDoubleComplex Lblock[sizeof(magmaDoubleComplex) * tile*span];
             // fill Lblock from CSR in row major ordering 
             #pragma omp simd
             for( i=0; i<(tile*span); i++) {
               Lblock[i] = MAGMA_Z_MAKE(0., 0.);
             }
             for(i=ti; i < MIN(ti + tile, hL.num_rows); i++ ) {
               #pragma omp simd
               for(j=hL.row[i]; j < hL.row[i+1]-1; j++ ) {
               	 if (mincol <= hL.col[j] && hL.col[j] < maxcol ) {   
                   Lblock[(i-ti) * (span) + (hL.col[j] - mincol) ] = hL.val[ j ];
                 }
               } 
             }  
             
             magmaDoubleComplex Ublock[sizeof(magmaDoubleComplex) * span*tile];
             // fill Ublock from CSC in column major ordering
             #pragma omp simd
             for( i=0; i<(span*tile); i++) {
               Ublock[i] = MAGMA_Z_MAKE(0., 0.);
             }
             for(i=tj; i < MIN(tj + tile, hU.num_rows); i++ ) {
               #pragma omp simd
               for(j=hU.row[i]; j < hU.row[i+1]-1; j++ ) {
               	 if (mincol <= hU.col[j] && hU.col[j] < maxcol ) {
                   Ublock[(i-tj) * (span) + (hU.col[j] - mincol) ] = hU.val[ j ];
                 }
               }
             }
            
             // calculate update
             zgemm( lapack_trans_const(MagmaTrans), 
           	   lapack_trans_const(MagmaNoTrans),
           	   (const int*) &tile, (const int*) &tile, (const int*) &span,
               &cone, Lblock, (const int*) &span, 
               Ublock, (const int*) &span, 
               &czero, Ctile2, (const int*) &tile ); 
             
             // begin updating values of U
             for(i=tj; i < MIN(tj+tile, hU.num_rows); i++ ) {
               #pragma omp simd
               for(j=hUc.row[i]; j < hUc.row[i+1]-1; j++ ) {
                 if (hU.col[j] >= ti && hU.col[j] < maxcol ) {  
                   ii = i - tj;
                   jj = hU.col[j] - ti;
                   hU.val[ j ] = ( hUc.val[ j ] - Ctile2[ ii * tile + jj ] );  
                 }
               }
             }
             if (ti==tj) {
               maxcol = MIN(tj + tile, hA.num_rows);	 
               // begin updating major diagonal values of U
               #pragma omp simd
               for(i=tj; i < MIN(tj+tile, hU.num_rows); i++ ) {
                 j = hUc.row[i+1]-1;
                 ii = i - tj;
                 jj = hU.col[j] - ti; 
                 // TODO : unnecessary, update hU.val[ j ] directly, 
                 // temporary L and U blocks do not use the major diagonals
                 d1[ i ] = ( hUc.val[ j ] - Ctile2[ ii * tile + jj ] );  
               }	 
               // begin updating values of L
               for(i=ti; i < MIN(ti+tile, hL.num_rows); i++ ) {
                 #pragma omp simd
                 for(j=hLc.row[i]; j < (hLc.row[i+1]-1); j++ ) {
                   if (hL.col[j] >= tj && hL.col[j] < maxcol ) { 
                     ii = i - ti;
                     jj = hL.col[j] - tj;
                     hL.val[ j ] = ( (hLc.val[ j ] - Ctile2[ ii + tile * jj ])/d1[ hL.col[j] ] );
                   }
                 }
               }
             }
             // done updating values for tiles in the upper triangle and on the major diagonal
           }
           // strictly lower tiles	 
           else {
           	 magma_int_t maxcol = MIN(tj + tile, hA.num_rows);	 
             magma_int_t mincol = MAX(0, tj - hA.diameter);
           	 // use dgemm to calculate sums for entire tile  
             span = MIN( MIN(tj+tile, hA.diameter+tile), hA.num_rows);
             magmaDoubleComplex Ctile[sizeof(magmaDoubleComplex) * tile*tile];
             #pragma omp simd
             for( i=0; i<(tile*tile); i++) {
               Ctile[i] = MAGMA_Z_MAKE(0., 0.);
             }
             magmaDoubleComplex Lblock[sizeof(magmaDoubleComplex) * tile*span];
             // fill Lblock from CSR in row major ordering  
             #pragma omp simd
             for( i=0; i<(tile*span); i++) {
               Lblock[i] = MAGMA_Z_MAKE(0., 0.);
             }
             for(i=ti; i < MIN(ti + tile, hL.num_rows); i++ ) {
               #pragma omp simd
               for(j=hL.row[i]; j < hL.row[i+1]-1; j++ ) {
               	 if (mincol <= hL.col[j] && hL.col[j] < maxcol ) {   
                   Lblock[ (i-ti) * (span) + (hL.col[j] - mincol) ] = hL.val[ j ];
                 }
               } 
             }
                
             magmaDoubleComplex Ublock[sizeof(magmaDoubleComplex) * span*tile];
             // fill Ublock from CSC in column major ordering
             #pragma omp simd
             for( i=0; i<(span*tile); i++) {
               Ublock[i] = MAGMA_Z_MAKE(0., 0.);
             }
             for(i=tj; i < MIN(tj + tile, hU.num_rows); i++ ) {
               #pragma omp simd
               for(j=hU.row[i]; j < hU.row[i+1]-1; j++ ) {
               	 if (mincol <= hU.col[j] && hU.col[j] < maxcol ) {
                   Ublock[ (i-tj) * (span) + (hU.col[j] - mincol) ] = hU.val[ j ];
                 }
               }
             }
             
             // calculate update
             zgemm( lapack_trans_const(MagmaTrans), 
           	   lapack_trans_const(MagmaNoTrans),
           	   (const int*) &tile, (const int*) &tile, (const int*) &span,
               &cone, Lblock, (const int*) &span, 
               Ublock, (const int*) &span, 
               &czero, Ctile, (const int*) &tile ); 
             
             // begin updating values of L
             for(i=ti; i < MIN(ti + tile, hL.num_rows); i++ ) {
               #pragma omp simd
               for(j=hLc.row[i]; j < (hLc.row[i+1]-1); j++ ) {
                 if (hLc.col[j] >= tj && hL.col[j] < maxcol ) {
                   ii = i - ti;
                   jj = hL.col[j] - tj;
                   hL.val[ j ] = ( (hLc.val[ j ] - Ctile[ ii + tile * jj ])/d1[ hL.col[j] ] );
                 }
               }
             }
             // done updating values for strictly lower tiles
           }
           
         }
       }
       
    }
     
    // update diagonal values of L and U
    #pragma omp parallel private(i)
    {
      #pragma omp for schedule(static,1) nowait
      for (i=0; i<hA.num_rows; i++) {
        hL.val[hL.row[i+1]-1] = cone;
        hU.val[hU.row[i+1]-1] = d1[i];
      }
    }
    // TODO : unnecessary, temporary L and U blocks do not use major the diagonals
    
    CHECK( magma_z_mtransfer( hL, L, Magma_CPU, Magma_DEV, queue ) );
    CHECK( magma_z_mtransfer( hU, U, Magma_CPU, Magma_DEV, queue ) );
	
    magma_z_mfree( &hA, queue );
    magma_z_mfree( &hLc, queue );
    magma_z_mfree( &hUc, queue );
    magma_z_mfree( &hA, queue );
    magma_z_mfree( &hL, queue );
    magma_z_mfree( &hU, queue );
    magma_z_mfree( &hUT, queue );
    magma_free( d1 );

#else
	info = MAGMA_ERR_NOT_SUPPORTED;
	goto cleanup;
#endif	
    
cleanup:
	return info;
}
