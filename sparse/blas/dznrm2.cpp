/*
    -- MAGMA (version 2.0) --
       Univ. of Tennessee, Knoxville
       Univ. of California, Berkeley
       Univ. of Colorado, Denver
       @date

       @precisions normal z -> c d s
       @author Stephen Wood

*/
#include "magmasparse_internal.h"

/*******************************************************************************
    Purpose
    -------
    
    This routine computes the Euclidean norm of a vector, dx, on the CPU.
    
    Arguments
    ---------
    @param[in]
    n           magma_int_t
                the number of elements in vectors dx.
                
    @param[in]
    dx          magmaDoubleComplex_const_ptr
                input vector dx.
                
    @param[in]
    incx        magma_int_t
                the increment for the elements of dx.
                
    @param[in]
    queue       magma_queue_t
                Queue to execute in.

    @ingroup magmasparse_zblas
*******************************************************************************/

extern "C" 
double
magma_dznrm2_q(
    magma_int_t n,
    magmaDoubleComplex_const_ptr dx, magma_int_t incx,
    magma_queue_t queue )
{
	double result;
    //double cblas_dznrm2 (const MKL_INT n, const void *x, const MKL_INT incx);
    //result = blasf77_dznrm2(&n, dx, &incx); 
    
    // A sequential implementation for now
    result = magma_cblas_dznrm2(n, dx, incx); 
    
    //return MAGMA_SUCCESS;
    return result;
}
