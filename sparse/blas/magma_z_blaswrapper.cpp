/*
    -- MAGMA (version 2.0) --
       Univ. of Tennessee, Knoxville
       Univ. of California, Berkeley
       Univ. of Colorado, Denver
       @date

       @precisions normal z -> c d s
       @author Hartwig Anzt
       @author Stephen Wood

*/
#include "magmasparse_internal.h"

#ifdef MAGMA_WITH_MKL
	#define MKL_Complex8  magmaFloatComplex
	#define MKL_Complex16 magmaDoubleComplex
    #include <mkl_spblas.h>

    #define CALL_AND_CHECK_STATUS(function, error_message) do { 	\
    	if(function != SPARSE_STATUS_SUCCESS)             			\
    	{                                                 			\    	
    	printf(error_message); fflush(0);                 			\    	
    	status = 1;                                       			\    	
    	goto cleanup;                                 	  			\    	
    	}                                                 			\
    } while(0)
    
#endif


/*******************************************************************************
    Purpose
    -------

    For a given input sparse matrix A and vectors x, y and scalars alpha, beta
    the wrapper determines the suitable SpMV computing
              y = alpha * A * x + beta * y.
    Arguments
    ---------

    @param[in]
    alpha       magmaDoubleComplex
                scalar alpha

    @param[in]
    A           magma_z_matrix
                sparse matrix A

    @param[in]
    x           magma_z_matrix
                input vector x
                
    @param[in]
    beta        magmaDoubleComplex
                scalar beta
    @param[out]
    y           magma_z_matrix
                output vector y
    @param[in]
    queue       magma_queue_t
                Queue to execute in.

    @ingroup magmasparse_zblas
*******************************************************************************/

extern "C" magma_int_t
magma_z_spmv(
    magmaDoubleComplex alpha,
    magma_z_matrix A,
    magma_z_matrix x,
    magmaDoubleComplex beta,
    magma_z_matrix y,
    magma_queue_t queue )
{
    magma_int_t info = 0;

    // make sure RHS is a dense matrix
    if ( x.storage_type != Magma_DENSE ) {
         printf("error: only dense vectors are supported for SpMV.\n");
         info = MAGMA_ERR_NOT_SUPPORTED;
         goto cleanup;
    }

    if ( A.memory_location != x.memory_location ||
                            x.memory_location != y.memory_location ) {
        printf("error: linear algebra objects are not located in same memory!\n");
        printf("memory locations are: %d   %d   %d\n",
                        A.memory_location, x.memory_location, y.memory_location );
        info = MAGMA_ERR_INVALID_PTR;
        goto cleanup;
    }

    // DEV case
    if ( A.memory_location == Magma_DEV ) {
        if ( A.num_cols == x.num_rows && x.num_cols == 1 ) {
             if ( A.storage_type == Magma_CSR || A.storage_type == Magma_CUCSR
                            || A.storage_type == Magma_CSRL
                            || A.storage_type == Magma_CSRU ) {
                mkl_zcsrmv( "N", &A.num_rows, &A.num_cols,
                            &alpha, "GFNC", A.dval,
                            A.dcol, A.drow, A.drow+1,
                            x.dval, &beta, y.dval );
             }
             else {
                 printf("error: format not supported.\n");
                 info = MAGMA_ERR_NOT_SUPPORTED; 
             }
        }
    }
    // CPU case
    else if( A.memory_location == Magma_CPU ){
        if ( A.num_cols == x.num_rows && x.num_cols == 1 ) {
             if ( A.storage_type == Magma_CSR || A.storage_type == Magma_CUCSR
                            || A.storage_type == Magma_CSRL
                            || A.storage_type == Magma_CSRU ) {
                mkl_zcsrmv( "N", &A.num_rows, &A.num_cols,
                            &alpha, "GFNC", A.dval,
                            A.dcol, A.drow, A.drow+1,
                            x.dval, &beta, y.dval );
             }
             else {
                 printf("error: format not supported.\n");
                 info = MAGMA_ERR_NOT_SUPPORTED; 
             }
        }
    }

cleanup:
    
    return info;
}





/*******************************************************************************
    Purpose
    -------

    For a given input sparse matrices A and B, and scalar alpha
    the wrapper determines the suitable SpMM computing
              C = alpha * A * B.
    Arguments
    ---------

    @param[in]
    alpha       magmaDoubleComplex
                scalar alpha

    @param[in]
    A           magma_z_matrix
                sparse matrix A
                
    @param[in]
    B           magma_z_matrix
                sparse matrix B
                
    @param[out]
    C           magma_z_matrix
                sparse matrix C
                
    @param[in]
    queue       magma_queue_t
                Queue to execute in.

    @ingroup magmasparse_zblas
*******************************************************************************/

extern "C" magma_int_t
magma_z_spmm(
    magmaDoubleComplex alpha,
    magma_z_matrix A,
    magma_z_matrix B,
    magma_z_matrix *C,
    magma_queue_t queue )
{
	magma_int_t info = 0;
	int status = 0;
	sparse_matrix_t csrA = NULL, csrB = NULL, csrC = NULL;
	magmaDoubleComplex *values_C = NULL;
	int *pointerB_C = NULL, *pointerE_C = NULL, *columns_C = NULL;
	int rows_C, cols_C, nnz_C;
	sparse_index_base_t indexing = SPARSE_INDEX_BASE_ZERO;
	int ii = 0;
	int coltmp;
	magmaDoubleComplex valtmp;
	
	// scale A matrix by alpha
	#pragma omp parallel 
	#pragma omp for nowait
	for ( int i=0; i<A.nnz; i++ ) {
		A.dval[i] *= alpha;
	}
	
	// create MKL sparse matrix handles	
	CALL_AND_CHECK_STATUS( mkl_sparse_z_create_csr( &csrA, indexing, 
		A.num_rows, A.num_cols, A.drow, 
		A.drow+1, A.dcol, A.dval ), 
		"Error after MKL_SPARSE_D_CREATE_CSR, csrA\n");
	
	CALL_AND_CHECK_STATUS( mkl_sparse_z_create_csr( &csrB, indexing, 
		B.num_rows, B.num_cols, B.drow, 
		B.drow+1, B.dcol, B.dval ), 
		"Error after MKL_SPARSE_D_CREATE_CSR, csrB\n");
    
    if ( A.memory_location != B.memory_location ) {
        printf("error: linear algebra objects are not located in same memory!\n");
        printf("memory locations are: %d   %d\n",
                        A.memory_location, B.memory_location );
        info = MAGMA_ERR_INVALID_PTR;
        goto cleanup;
    }

    // DEV case
    if ( A.memory_location == Magma_DEV ) {
        if ( A.num_cols == B.num_rows ) {
            if ( A.storage_type == Magma_CSR  ||
                 A.storage_type == Magma_CSRL ||
                 A.storage_type == Magma_CSRU ||
                 A.storage_type == Magma_CSRCOO ) {
               	CALL_AND_CHECK_STATUS( 
                	mkl_sparse_spmm( SPARSE_OPERATION_NON_TRANSPOSE, 
                		csrA, csrB, &csrC ), 
                		"Error after MKL_SPARSE_SPMM\n");
               	info = MAGMA_SUCCESS;
            }
            else {
                printf("error: format not supported.\n");
                info = MAGMA_ERR_NOT_SUPPORTED;
            }
        }
    }
    // CPU case
    else {
        if ( A.num_cols == B.num_rows ) {
            if ( A.storage_type == Magma_CSR  ||
                 A.storage_type == Magma_CSRL ||
                 A.storage_type == Magma_CSRU ||
                 A.storage_type == Magma_CSRCOO ) {
               	CALL_AND_CHECK_STATUS( 
                	mkl_sparse_spmm( SPARSE_OPERATION_NON_TRANSPOSE, 
                		csrA, csrB, &csrC ), 
                		"Error after MKL_SPARSE_SPMM\n");
                info = MAGMA_SUCCESS;
            }
            else {
                printf("error: format not supported.\n");
                info = MAGMA_ERR_NOT_SUPPORTED;
            }
        }
    }
     
    CALL_AND_CHECK_STATUS( mkl_sparse_z_export_csr( csrC, &indexing, 
    	&rows_C, &cols_C,
    	&pointerB_C, &pointerE_C, &columns_C, &values_C ),
    	"Error after MKL_SPARSE_D_EXPORT_CSR\n");
    
    // ensure column indices are in ascending order in every row
    //printf( "\n RESULTANT MATRIX C:\nrow# : (value, column) (value, column)\n" );
    ii = 0;
	for( int i = 0; i < rows_C; i++ ) {
		//printf("row#%d:", i); fflush(0);
		for( int j = pointerB_C[i]; j < pointerE_C[i]; j++ ) {
			//printf(" (%e, %6d)", values_C[ii], columns_C[ii] ); fflush(0);
			if ( j+1 < pointerE_C[i] && columns_C[ii] > columns_C[ii+1]) {
				//printf("\nSWAP!!!\n");	
				valtmp = values_C[ii];
				values_C[ii] = values_C[ii+1];
				values_C[ii+1] = valtmp;
				coltmp = columns_C[ii];
				columns_C[ii] = columns_C[ii+1];
				columns_C[ii+1] = coltmp;
			}
			ii++;
		}
		//printf( "\n" );
	}
	//printf( "_____________________________________________________________________  \n" );
	
    nnz_C = pointerE_C[ rows_C-1 ];

    // fill in information for C
    C->storage_type = A.storage_type;
    C->memory_location = A.memory_location;
    C->sym = A.sym;
    C->diagorder_type = A.diagorder_type;
    C->fill_mode = A.fill_mode;
    C->num_rows = rows_C;
    C->num_cols = cols_C;
    C->nnz = nnz_C; C->true_nnz = nnz_C;
    // memory allocation
    CHECK( magma_zmalloc( &C->dval, nnz_C ));
    CHECK( magma_index_malloc( &C->drow, rows_C + 1 ));
    CHECK( magma_index_malloc( &C->dcol, nnz_C ));
    // data transfer
    magma_zsetvector( nnz_C, values_C, 1, C->dval, 1, queue );
    magma_index_setvector( rows_C, pointerB_C, 1, C->drow, 1, queue );
    magma_index_setvector( 1, &pointerE_C[rows_C-1], 1, &C->drow[rows_C], 1, queue );
    magma_index_setvector( nnz_C, columns_C, 1, C->dcol, 1, queue );

cleanup:
    return info;
}
