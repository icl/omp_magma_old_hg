/*
    -- MAGMA (version 1.1) --
       Univ. of Tennessee, Knoxville
       Univ. of California, Berkeley
       Univ. of Colorado, Denver
       @date
       
       @author Hartwig Anzt

       @precisions normal z -> c d s

*/
#include "magmasparse_internal.h"

#define PRECISION_z
#define COMPLEX
#define BLOCKSIZE 32
#define WARP_SIZE 32
#define WRP 32
#define WRQ 4




const int MaxBlockSize = 32;


template <int block_size>
void
magma_zlowerisai_regs_inv_kernel(
magma_int_t z,
int N,
magma_int_t num_rows,
const magma_index_t * __restrict__ Arow,
const magma_index_t * __restrict__ Acol,
const magmaDoubleComplex * __restrict__ Aval,
magma_index_t *Mrow,
magma_index_t *Mcol,
magmaDoubleComplex *Mval )
{
#if defined( REAL )

    int row = z;
    

    // only if within the size
    int mstart = Mrow[ row ];
    int mlim = Mrow[ row + 1 ];
    N = mlim-mstart;
    // magmaDoubleComplex *rB;
    // magma_zmalloc( &rB, N );
    magmaDoubleComplex rB[ 32 ];      // registers for trsv
    //magmaDoubleComplex dA[ N*N ];  // registers for trisystem
    magmaDoubleComplex rA;

    // set dA to 0
    // #pragma unroll
    // #pragma omp parallel for
    //for( int j = 0; j < N*N; j++ ){
    //    dA[ j ] = MAGMA_Z_ZERO;
    //}
        // we know how RHS looks like
    #pragma unroll
    for( int tid=0; tid<32; tid++){
        rB[tid] = ( tid == 0 ) ? MAGMA_Z_ONE : MAGMA_Z_ZERO;
    }
    // #pragma omp parallel for
    for( int tid=0; tid<N; tid++){
        // generate the triangular systems
        int t = Mcol[ mstart + tid ];
        int k = Arow[ t ];
        int alim = Arow[ t+1 ];
        int l = mstart;
        int idx = 0;
        while( k < alim && l < mlim  ){ // stop once this column is done
            int mcol =  Mcol[ l ];
            int acol = Acol[k];
            if( mcol == acol ){ //match
                // dA[ idx+tid*N ] = Aval[ k ];
                if((idx)%N == tid){
                    rB[tid] /= Aval[ k ];
                } else if(tid > idx ){
                    rB[tid] -= rB[idx] * Aval[ k ];
                }
                
                k++;
                l++;
                idx++;
            } else if( acol < mcol ){// need to check next element
                k++;
            } else { // element does not exist, i.e. l < LC.col[k]
                l++; // check next elment in the sparsity pattern
                idx++; // leave this element equal zero
            }
        }
        Mval[ mstart + tid ] = rB[tid];
    }

  // // second: solve the triangular systems - in registers
  // for( int tid=0; tid<N; tid++){
  //         // Triangular solve in regs.
  //     //#pragma unroll
  //     for (int k = 0; k < N; k++)
  //     {
  //         rA = dA[ k+tid*N ];
  //         if (k%N == tid)
  //             rB[tid] /= rA;
  //         magmaDoubleComplex top = rB[k];
  //         if ( tid > k)
  //             rB[tid] -= (top*rA);
  //     }
  // }
    // Drop B to dev memory - in ISAI preconditioner M
    // #pragma omp parallel for
    // for( int tid=0; tid<N; tid++){
    //     Mval[ mstart + tid ] = rB[tid];
    // }
    
#endif

}


template <int block_size>
 void
magma_zlowerisai_regs_inv_select(
magma_int_t z,
int N,
magma_int_t num_rows,
const magma_index_t * __restrict__ Arow,
const magma_index_t * __restrict__ Acol,
const magmaDoubleComplex * __restrict__ Aval,
magma_index_t *Mrow,
magma_index_t *Mcol,
magmaDoubleComplex *Mval )
{
    magma_zlowerisai_regs_inv_kernel<block_size>(
                z, N, num_rows, Arow, Acol, Aval, Mrow, Mcol, Mval);
}


void
magma_zlowerisai_regs_inv_switch(
magma_int_t z,
magma_int_t num_rows,
const magma_index_t * __restrict__ Arow,
const magma_index_t * __restrict__ Acol,
const magmaDoubleComplex * __restrict__ Aval,
magma_index_t *Mrow,
magma_index_t *Mcol,
magmaDoubleComplex *Mval )
{
    int row = z;
    if( row < num_rows ){
        int N = Mrow[ row+1 ] - Mrow[ row ];
        magma_zlowerisai_regs_inv_select<MaxBlockSize>(
                z, N, num_rows, Arow, Acol, Aval, Mrow, Mcol, Mval);
    }
}

template <int block_size>
void
magma_zupperisai_regs_inv_kernel(
magma_int_t z,
int N,
magma_int_t num_rows,
const magma_index_t * __restrict__ Arow,
const magma_index_t * __restrict__ Acol,
const magmaDoubleComplex * __restrict__ Aval,
magma_index_t *Mrow,
magma_index_t *Mcol,
magmaDoubleComplex *Mval )
{
#if defined( REAL )

    int row = z;

    // only if within the size
    int mstart = Mrow[ row ];
    int mlim = Mrow[ row ]-1;
    N = Mrow[ row+1 ] - Mrow[ row ];
    
    magmaDoubleComplex rB[ 32 ];      // registers for trsv
    // magmaDoubleComplex *rB;
    // magma_zmalloc( &rB, N );
    // magmaDoubleComplex dA[ N*N ];  // registers for trisystem
    // magmaDoubleComplex rA;

    // set dA to 0
    // #pragma unroll
    // #pragma omp parallel for
    // for( int j = 0; j < N*N; j++ ){
    //     dA[ j ] = MAGMA_Z_ZERO;
    // }
        // we know how RHS looks like
    #pragma unroll
    for( int tid=0; tid<32; tid++){
        rB[tid] = ( tid == N-1 ) ? MAGMA_Z_ONE : MAGMA_Z_ZERO;
    }
    // #pragma omp parallel for
    for( int tid=N-1; tid>-1; tid--){
        // generate the triangular systems
        int t = Mcol[ mstart + tid ];
        int k = Arow[ t+1 ] - 1;
        int alim = Arow[ t ]-1;
        int l = Mrow[ row+1 ]-1;
        int idx = N-1;
        while( k > alim && l > mlim  ){ // stop once this column is done
            int mcol =  Mcol[ l ];
            int acol = Acol[k];
            if( mcol == acol ){ //match
                //dA[ idx+tid*N ] = Aval[ k ];
                if((idx)%N == tid){
                    rB[tid] /= Aval[ k ];
                } else if(tid < idx ){
                    rB[tid] -= rB[idx] * Aval[ k ];
                }
                k--;
                l--;
                idx--;
            } else if( acol > mcol ){// need to check next element
                k--;
            } else { // element does not exist, i.e. l < LC.col[k]
                l--; // check next elment in the sparsity pattern
                idx--; // leave this element equal zero
            }
        }
        Mval[ mstart + tid ] = rB[tid];
    }
    
    
//   for( int tid=0; tid<N; tid++){
//       rB[tid] = ( tid == N-1 ) ? MAGMA_Z_ONE : MAGMA_Z_ZERO;
//   }
//   // second: solve the triangular systems - in registers
//   for( int tid=N-1; tid>-1; tid--){
//           // Triangular solve in regs.
//       //#pragma unroll
//       for (int k = N-1; k >-1; k--)
//       {
//           rA = dA[ k+tid*N ];
//           if (k%N == tid)
//               rB[tid] /= rA;
//           magmaDoubleComplex bottom = rB[k];
//           if ( tid < k)
//               ;//rB[tid] -= (bottom*rA);
//       }
//   }
    
    // Drop B to dev memory - in ISAI preconditioner M
    // #pragma omp parallel for
    //for( int tid=0; tid<N; tid++){
    //    Mval[ mstart + tid ] = rB[tid];
    //}
    
#endif

}

template <int block_size>
void
magma_zupperisai_regs_inv_select(
magma_int_t z,
int N,
magma_int_t num_rows,
const magma_index_t * __restrict__ Arow,
const magma_index_t * __restrict__ Acol,
const magmaDoubleComplex * __restrict__ Aval,
magma_index_t *Mrow,
magma_index_t *Mcol,
magmaDoubleComplex *Mval )
{
    magma_zupperisai_regs_inv_kernel<block_size>(
                z, N, num_rows, Arow, Acol, Aval, Mrow, Mcol, Mval);
}

void
magma_zupperisai_regs_inv_switch(
magma_int_t z,
magma_int_t num_rows,
const magma_index_t * __restrict__ Arow,
const magma_index_t * __restrict__ Acol,
const magmaDoubleComplex * __restrict__ Aval,
magma_index_t *Mrow,
magma_index_t *Mcol,
magmaDoubleComplex *Mval )
{
    int row = z;
    if( row < num_rows ){
        int N = Mrow[ row+1 ] - Mrow[ row ];
        magma_zupperisai_regs_inv_select<MaxBlockSize>(
                z, N, num_rows, Arow, Acol, Aval, Mrow, Mcol, Mval);
    }
}



/**
    Purpose
    -------
    This routine is designet to combine all kernels into one.

    Arguments
    ---------


    @param[in]
    uplotype    magma_uplo_t
                lower or upper triangular

    @param[in]
    transtype   magma_trans_t
                possibility for transposed matrix

    @param[in]
    diagtype    magma_diag_t
                unit diagonal or not

    @param[in]
    L           magma_z_matrix
                triangular factor for which the ISAI matrix is computed.
                Col-Major CSR storage.

    @param[in,out]
    M           magma_z_matrix*
                SPAI preconditioner CSR col-major

    @param[out]
    sizes       magma_int_t*
                Number of Elements that are replaced.

    @param[out]
    locations   magma_int_t*
                Array indicating the locations.

    @param[out]
    trisystems  magmaDoubleComplex*
                trisystems

    @param[out]
    rhs         magmaDoubleComplex*
                right-hand sides

    @param[in]
    queue       magma_queue_t
                Queue to execute in.

    @ingroup magmasparse_zaux
    ********************************************************************/

extern "C" magma_int_t
magma_zisai_generator_regs(
    magma_uplo_t uplotype,
    magma_trans_t transtype,
    magma_diag_t diagtype,
    magma_z_matrix L,
    magma_z_matrix *M,
    magma_index_t *sizes,
    magma_index_t *locations,
    magmaDoubleComplex *trisystems,
    magmaDoubleComplex *rhs,
    magma_queue_t queue )
{
    magma_int_t info = 0;

    if (uplotype == MagmaLower) {
        #pragma omp parallel 
        #pragma omp for nowait
        for(int z=0; z<L.num_rows; z++){
            //magma_zlowerisai_regs_inv_switch(
            //    z,
            //    L.num_rows,
            //    L.row,
            //    L.col,
            //    L.val,
            //    M->row,
            //    M->col,
            //    M->val );
            magma_zlowerisai_regs_inv_kernel<32>(
                z,
                z,
                L.num_rows,
                L.row,
                L.col,
                L.val,
                M->row,
                M->col,
                M->val );
        }
    }
    else {
        #pragma omp parallel 
        #pragma omp for nowait
        for(int z=0; z<L.num_rows; z++){
            //magma_zupperisai_regs_inv_switch(
            //    z,
            //    L.num_rows,
            //    L.row,
            //    L.col,
            //    L.val,
            //    M->row,
            //    M->col,
            //    M->val );
            magma_zupperisai_regs_inv_kernel<32>(
                z,
                z,
                L.num_rows,
                L.row,
                L.col,
                L.val,
                M->row,
                M->col,
                M->val );
        }
    }
    
    return info;
}

