/*
    -- MAGMA (version 2.0) --
       Univ. of Tennessee, Knoxville
       Univ. of California, Berkeley
       Univ. of Colorado, Denver
       @date

       @precisions normal z -> s d c
       @author Hartwig Anzt
       @author Mark Gates
       @author Stephen Wood

*/
#include "magmasparse_internal.h"

#ifdef MAGMA_WITH_MKL
    #define MKL_Complex8  magmaFloatComplex
    #define MKL_Complex16 magmaDoubleComplex
    #include <mkl_spblas.h>
    #include <mkl_trans.h>
#endif

/*******************************************************************************
    Purpose
    -------
    Transposes a matrix stored in CSR format on the CPU host.


    Arguments
    ---------
    @param[in]
    n_rows      magma_int_t
                number of rows in input matrix

    @param[in]
    n_cols      magma_int_t
                number of columns in input matrix

    @param[in]
    nnz         magma_int_t
                number of nonzeros in input matrix

    @param[in]
    values      magmaDoubleComplex*
                value array of input matrix

    @param[in]
    rowptr      magma_index_t*
                row pointer of input matrix

    @param[in]
    colind      magma_index_t*
                column indices of input matrix

    @param[in]
    new_n_rows  magma_index_t*
                number of rows in transposed matrix

    @param[in]
    new_n_cols  magma_index_t*
                number of columns in transposed matrix

    @param[in]
    new_nnz     magma_index_t*
                number of nonzeros in transposed matrix

    @param[in]
    new_values  magmaDoubleComplex**
                value array of transposed matrix

    @param[in]
    new_rowptr  magma_index_t**
                row pointer of transposed matrix

    @param[in]
    new_colind  magma_index_t**
                column indices of transposed matrix

    @param[in]
    queue       magma_queue_t
                Queue to execute in.

    @ingroup magmasparse_zaux
*******************************************************************************/

extern "C" magma_int_t
z_transpose_csr(
    magma_z_matrix A,
    magma_z_matrix *B,
    magma_queue_t queue )
{
    magma_int_t info = 0;
    magma_int_t workaround = 0;
    
    // this workaround should resolve the problem with the 1 indexing in case of MKL
    // we check whether first element in A.rowptr is 1
    if( A.memory_location == Magma_CPU ){
        if( A.row[0] == 1 ){
            // increment to create one-based indexing of the array parameters
            #pragma omp parallel 
            {
            #pragma omp for simd nowait
            for (magma_int_t i=0; i<A.nnz; i++) {
                A.col[i] -= 1;
            }
            #pragma omp for simd nowait
            for (magma_int_t i=0; i<A.num_rows+1; i++) {
                A.row[i] -= 1;	
            }
            }
            workaround = 1;       
        }
        CHECK( magma_zmtransfer( A, B, A.memory_location, A.memory_location, queue) );
    
        // easier to keep names straight if we convert CSR to CSC,
        // which is the same as tranposing CSR.
        // magmaDoubleComplex *csc_values=NULL;
        // magma_index_t *csc_colptr=NULL, *csc_rowind=NULL;
        
        // i, j are actual row & col indices (0 <= i < nrows, 0 <= j < ncols).
        // k is index into col and values (0 <= k < nnz).
        magma_int_t i, j, k, total;
        
        // CHECK( magma_zmalloc_cpu( &csc_values, nnz ) );
        // CHECK( magma_index_malloc_cpu( &csc_colptr, n_cols + 1 ) );
        // CHECK( magma_index_malloc_cpu( &csc_rowind, nnz ) );
        
        // example matrix
        // [ x x 0 x ]
        // [ x 0 x x ]
        // [ x x 0 0 ]
        // rowptr = [ 0 3 6, 8 ]
        // colind = [ 0 1 3; 0 2 3; 0 1 ]
        
        // sum up nnz in each original column
        // colptr = [ 3 2 1 2, X ]
        #pragma omp parallel private (j) 
        #pragma omp for simd nowait
        for( j=0; j < B->num_rows+1; j++ ) {
            B->row[ j ] = 0;
        }
        for( k=0; k < A.nnz; k++ ) {
            B->row[ A.col[k]+1 ]++;
        }
        
        // running sum to convert to new colptr
        // colptr = [ 0 3 5 6, 8 ]
        total = 0;
        for( j=0; j < A.num_rows+1; j++ ) {
            total = total + B->row[ j ];
            B->row[ j ] = total;
        }
        assert( total == A.nnz );
        
        // copy row indices and values
        // this increments colptr until it effectively shifts left one
        // colptr = [ 3 5 6 8, 8 ]
        // rowind = [ 0 1 2; 0 2; 1; 0 1 ]
        for( i=0; i < A.num_rows; i++ ) {
            for( k=A.row[i]; k < A.row[i+1]; k++ ) {
                j = A.col[k];
                B->col[ B->row[ j ] ] = i;
                B->val[ B->row[ j ] ] = A.val[k];
                B->row[ j ]++;
            }
        }
        assert( B->row[ B->num_rows-1 ] == A.nnz );
        
        // shift colptr right one
        // colptr = [ 0 3 5 6, 8 ]
        for( j=B->num_rows-1; j > 0; j-- ) {
            B->row[j] = B->row[j-1];
        }
        B->row[0] = 0;

        if( workaround == 1 ){
            // increment to create one-based indexing of the array parameters
            #pragma omp parallel 
            {
            #pragma omp for simd nowait
            for (magma_int_t it=0; it<B->nnz; it++) {
                A.col[it] += 1;
                B->col[it] += 1;
            }
            #pragma omp for simd nowait
            for (magma_int_t it=0; it<B->num_rows+1; it++) {
                A.row[it] += 1;	
                B->row[it] += 1;	
            }
            }
        }
    } else if( A.memory_location == Magma_DEV ){
        if( A.drow[0] == 1 ){
            // increment to create one-based indexing of the array parameters
            #pragma omp parallel 
            {
            #pragma omp for simd nowait
            for (magma_int_t i=0; i<A.nnz; i++) {
                A.dcol[i] -= 1;
            }
            #pragma omp for simd nowait
            for (magma_int_t i=0; i<A.num_rows+1; i++) {
                A.drow[i] -= 1;	
            }
            }
            workaround = 1;       
        }
        CHECK( magma_zmtransfer( A, B, A.memory_location, A.memory_location, queue) );
    
        // easier to keep names straight if we convert CSR to CSC,
        // which is the same as tranposing CSR.
        // magmaDoubleComplex *csc_values=NULL;
        // magma_index_t *csc_colptr=NULL, *csc_rowind=NULL;
        
        // i, j are actual row & col indices (0 <= i < nrows, 0 <= j < ncols).
        // k is index into col and values (0 <= k < nnz).
        magma_int_t i, j, k, total;
        
        // CHECK( magma_zmalloc_cpu( &csc_values, nnz ) );
        // CHECK( magma_index_malloc_cpu( &csc_colptr, n_cols + 1 ) );
        // CHECK( magma_index_malloc_cpu( &csc_rowind, nnz ) );
        
        // example matrix
        // [ x x 0 x ]
        // [ x 0 x x ]
        // [ x x 0 0 ]
        // rowptr = [ 0 3 6, 8 ]
        // colind = [ 0 1 3; 0 2 3; 0 1 ]
        
        // sum up nnz in each original column
        // colptr = [ 3 2 1 2, X ]
        #pragma omp parallel private (j) 
        #pragma omp for simd nowait
        for( j=0; j < B->num_rows+1; j++ ) {
            B->drow[ j ] = 0;
        }
        for( k=0; k < A.nnz; k++ ) {
            B->drow[ A.dcol[k]+1 ]++;
        }
        
        // running sum to convert to new colptr
        // colptr = [ 0 3 5 6, 8 ]
        total = 0;
        for( j=0; j < A.num_rows+1; j++ ) {
            total = total + B->drow[ j ];
            B->drow[ j ] = total;
        }
        assert( total == A.nnz );
        
        // copy row indices and values
        // this increments colptr until it effectively shifts left one
        // colptr = [ 3 5 6 8, 8 ]
        // rowind = [ 0 1 2; 0 2; 1; 0 1 ]
        for( i=0; i < A.num_rows; i++ ) {
            for( k=A.drow[i]; k < A.drow[i+1]; k++ ) {
                j = A.dcol[k];
                B->dcol[ B->drow[ j ] ] = i;
                B->dval[ B->drow[ j ] ] = A.dval[k];
                B->drow[ j ]++;
            }
        }
        assert( B->drow[ B->num_rows-1 ] == A.nnz );
        
        // shift colptr right one
        // colptr = [ 0 3 5 6, 8 ]
        for( j=B->num_rows-1; j > 0; j-- ) {
            B->drow[j] = B->drow[j-1];
        }
        B->drow[0] = 0;

        if( workaround == 1 ){
            // increment to create one-based indexing of the array parameters
            #pragma omp parallel 
            {
            #pragma omp for simd nowait
            for (magma_int_t it=0; it<B->nnz; it++) {
                A.dcol[it] += 1;
                B->dcol[it] += 1;
            }
            #pragma omp for simd nowait
            for (magma_int_t it=0; it<B->num_rows+1; it++) {
                A.drow[it] += 1;	
                B->drow[it] += 1;	
            }
            }
        }
    }
    
cleanup:
    return info;
}


/*******************************************************************************
    Purpose
    -------

    Interface to cuSPARSE transpose.

    Arguments
    ---------

    @param[in]
    A           magma_z_matrix
                input matrix (CSR)

    @param[out]
    B           magma_z_matrix*
                output matrix (CSR)
    @param[in]
    queue       magma_queue_t
                Queue to execute in.

    @ingroup magmasparse_zaux
*******************************************************************************/
    
    
extern "C" magma_int_t
magma_zmtranspose(
    magma_z_matrix A, magma_z_matrix *B,
    magma_queue_t queue )
{
    magma_int_t info = 0;
    
    CHECK( z_transpose_csr( A, B, queue ) );
    // CHECK( magma_z_cucsrtranspose( A, B, queue ));
    
cleanup:
    return info;
}

/*******************************************************************************
    Purpose
    -------

    This function forms the transpose conjugate of a matrix on the CPU. 
    For a real-value matrix, the output is the transpose.


    Arguments
    ---------

    @param[in]
    A           magma_z_matrix
                input matrix (CSR)

    @param[out]
    B           magma_z_matrix*
                output matrix (CSR)
    @param[in]
    queue       magma_queue_t
                Queue to execute in.

    @ingroup magmasparse_zaux
*******************************************************************************/

extern "C" 
magma_int_t
    magma_zmtransposeconjugate(
    magma_z_matrix A,
    magma_z_matrix *B,
    magma_queue_t queue )
{
	magma_int_t info = 0;
    
    CHECK( z_transpose_csr( A, B, queue ) );
    
#if defined(PRECISION_z) || defined(PRECISION_c)
    if( A.memory_location == Magma_CPU ){
        #pragma omp parallel 
        #pragma omp for simd nowait
        for( magma_int_t i=0; i<A.nnz; i++ ){
            B->val[i] = MAGMA_Z_CONJ( B->val[i] );
        }
    } else if( A.memory_location == Magma_DEV ){
        #pragma omp parallel 
        #pragma omp for simd nowait
        for( magma_int_t i=0; i<A.nnz; i++ ){
            B->dval[i] = MAGMA_Z_CONJ( B->dval[i] );
        } 
    }
    
#endif
    
cleanup:
    return info;
    
}