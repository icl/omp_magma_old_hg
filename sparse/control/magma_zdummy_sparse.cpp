/*
    -- MAGMA (version 2.0) --
       Univ. of Tennessee, Knoxville
       Univ. of California, Berkeley
       Univ. of Colorado, Denver
       @date

       @author Hartwig Anzt

       @precisions normal z -> s d c
*/

// just a dummy file containing undefined functions

#include "magmasparse_internal.h"

// ISAI preconditioner
/*
magma_int_t
magma_zmprepare_batched(
    magma_uplo_t uplotype,
    magma_trans_t transtype,
    magma_diag_t diagtype,
    magma_z_matrix L,
    magma_z_matrix LC,
    magma_index_t *sizes,
    magma_index_t *locations,
    magmaDoubleComplex *trisystems,
    magmaDoubleComplex *rhs,    
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zmtrisolve_batched(
    magma_uplo_t uplotype,
    magma_trans_t transtype,
    magma_diag_t diagtype,
    magma_z_matrix L,
    magma_z_matrix LC,
    magma_index_t *sizes,
    magma_index_t *locations,
    magmaDoubleComplex *trisystems,
    magmaDoubleComplex *rhs,    
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zmbackinsert_batched(
    magma_uplo_t uplotype,
    magma_trans_t transtype,
    magma_diag_t diagtype,
    magma_z_matrix *M,
    magma_index_t *sizes,
    magma_index_t *locations,
    magmaDoubleComplex *trisystems,
    magmaDoubleComplex *rhs,    
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}
*/
magma_int_t
magma_zmprepare_batched_gpu(
    magma_uplo_t uplotype,
    magma_trans_t transtype,
    magma_diag_t diagtype,
    magma_z_matrix L,
    magma_z_matrix LC,
    magma_index_t *sizes,
    magma_index_t *locations,
    magmaDoubleComplex *trisystems,
    magmaDoubleComplex *rhs,    
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zmtrisolve_batched_gpu(
    magma_uplo_t uplotype,
    magma_trans_t transtype,
    magma_diag_t diagtype,
    magma_z_matrix L,
    magma_z_matrix LC,
    magma_index_t *sizes,
    magma_index_t *locations,
    magmaDoubleComplex *trisystems,
    magmaDoubleComplex *rhs,    
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zmbackinsert_batched_gpu(
    magma_uplo_t uplotype,
    magma_trans_t transtype,
    magma_diag_t diagtype,
    magma_z_matrix *M,
    magma_index_t *sizes,
    magma_index_t *locations,
    magmaDoubleComplex *trisystems,
    magmaDoubleComplex *rhs,    
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}
/*
magma_int_t
magma_ziluisaisetup(
    magma_z_matrix A,
    magma_z_matrix b,
    magma_z_preconditioner *precond,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_ziluisaisetup_t(
    magma_z_matrix A,
    magma_z_matrix b,
    magma_z_preconditioner *precond,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}
*/

magma_int_t
magma_zmisai_blockstruct_gpu(
    magma_int_t n,
    magma_int_t bs,
    magma_int_t offs,
    magma_uplo_t uplotype,
    magma_z_matrix *A,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zgeisai_maxblock(
    magma_z_matrix L,
    magma_z_matrix *MT,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zisaigenerator_32_gpu(
    magma_uplo_t uplotype,
    magma_trans_t transtype,
    magma_diag_t diagtype,
    magma_z_matrix L,
    magma_z_matrix *M,
    magma_index_t *sizes,
    magma_index_t *locations,
    magmaDoubleComplex *trisystems,
    magmaDoubleComplex *rhs,    
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zisaigenerator_16_gpu(
    magma_uplo_t uplotype,
    magma_trans_t transtype,
    magma_diag_t diagtype,
    magma_z_matrix L,
    magma_z_matrix *M,
    magma_index_t *sizes,
    magma_index_t *locations,
    magmaDoubleComplex *trisystems,
    magmaDoubleComplex *rhs,    
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zisaigenerator_8_gpu(
    magma_uplo_t uplotype,
    magma_trans_t transtype,
    magma_diag_t diagtype,
    magma_z_matrix L,
    magma_z_matrix *M,
    magma_index_t *sizes,
    magma_index_t *locations,
    magmaDoubleComplex *trisystems,
    magmaDoubleComplex *rhs,    
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

// #endif
/* ////////////////////////////////////////////////////////////////////////////
 -- MAGMA_SPARSE function definitions / Data on CPU
*/




/* ////////////////////////////////////////////////////////////////////////////
 -- MAGMA_SPARSE function definitions / Data on CPU / Multi-GPU
*/

/* ////////////////////////////////////////////////////////////////////////////
 -- MAGMA_SPARSE solvers (Data on GPU)
*/


magma_int_t 
magma_zcg(
    magma_z_matrix A, magma_z_matrix b, 
    magma_z_matrix *x, magma_z_solver_par *solver_par,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

/*
magma_int_t 
magma_zcg_res(
    magma_z_matrix A, magma_z_matrix b, 
    magma_z_matrix *x, magma_z_solver_par *solver_par,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}
*/


magma_int_t 
magma_zcg_merge(
    magma_z_matrix A, magma_z_matrix b, 
    magma_z_matrix *x, magma_z_solver_par *solver_par,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}


magma_int_t
magma_zpcg_merge(
    magma_z_matrix A, magma_z_matrix b, magma_z_matrix *x,
    magma_z_solver_par *solver_par,
    magma_z_preconditioner *precond_par,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

/*
magma_int_t
magma_zcgs(
    magma_z_matrix A, magma_z_matrix b, magma_z_matrix *x,
    magma_z_solver_par *solver_par,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zcgs_merge(
    magma_z_matrix A, magma_z_matrix b, magma_z_matrix *x,
    magma_z_solver_par *solver_par,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}
*/

/*
magma_int_t
magma_zpcgs(
    magma_z_matrix A, magma_z_matrix b, magma_z_matrix *x,
    magma_z_solver_par *solver_par,
    magma_z_preconditioner *precond_par,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}
*/
/*
magma_int_t
magma_zpcgs_merge(
    magma_z_matrix A, magma_z_matrix b, magma_z_matrix *x,
    magma_z_solver_par *solver_par,
    magma_z_preconditioner *precond_par,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}
*/
/*
magma_int_t
magma_zqmr(
    magma_z_matrix A, magma_z_matrix b, magma_z_matrix *x,
    magma_z_solver_par *solver_par,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}
*/
/*
magma_int_t
magma_zpqmr(
    magma_z_matrix A, magma_z_matrix b, magma_z_matrix *x,
    magma_z_solver_par *solver_par,
    magma_z_preconditioner *precond_par,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}
*/
/*
magma_int_t
magma_zqmr_merge(
    magma_z_matrix A, magma_z_matrix b, magma_z_matrix *x,
    magma_z_solver_par *solver_par,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}
*/
/*
magma_int_t
magma_zpqmr_merge(
    magma_z_matrix A, magma_z_matrix b, magma_z_matrix *x,
    magma_z_solver_par *solver_par,
    magma_z_preconditioner *precond_par,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}
*/
/*
magma_int_t
magma_ztfqmr(
    magma_z_matrix A, magma_z_matrix b, magma_z_matrix *x,
    magma_z_solver_par *solver_par,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}
*/

magma_int_t
magma_ztfqmr_unrolled(
    magma_z_matrix A, magma_z_matrix b, magma_z_matrix *x,
    magma_z_solver_par *solver_par,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

/*
magma_int_t
magma_ztfqmr_merge(
    magma_z_matrix A, magma_z_matrix b, magma_z_matrix *x,
    magma_z_solver_par *solver_par,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}
*/
/*
magma_int_t
magma_zptfqmr(
    magma_z_matrix A, magma_z_matrix b, magma_z_matrix *x,
    magma_z_solver_par *solver_par,
    magma_z_preconditioner *precond_par,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}
*/
/*
magma_int_t
magma_zptfqmr_merge(
    magma_z_matrix A, magma_z_matrix b, magma_z_matrix *x,
    magma_z_solver_par *solver_par,
    magma_z_preconditioner *precond_par,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}
*/
/*
magma_int_t
magma_zbicgstab(
    magma_z_matrix A, magma_z_matrix b, magma_z_matrix *x, 
    magma_z_solver_par *solver_par,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}
*/

/*
magma_int_t
magma_zbicg(
    magma_z_matrix A, magma_z_matrix b, magma_z_matrix *x,
    magma_z_solver_par *solver_par,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}
*/

/*
magma_int_t
magma_zbicgstab_merge(
    magma_z_matrix A, magma_z_matrix b, 
    magma_z_matrix *x, magma_z_solver_par *solver_par,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}
*/

magma_int_t
magma_zbicgstab_merge2(
    magma_z_matrix A, magma_z_matrix b, 
    magma_z_matrix *x, magma_z_solver_par *solver_par,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zbicgstab_merge3(
    magma_z_matrix A, magma_z_matrix b, 
    magma_z_matrix *x, magma_z_solver_par *solver_par,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

/*
magma_int_t
magma_zpcg(
    magma_z_matrix A, magma_z_matrix b, 
    magma_z_matrix *x, magma_z_solver_par *solver_par, 
    magma_z_preconditioner *precond_par,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}
*/

magma_int_t
magma_zbpcg(
    magma_z_matrix A, magma_z_matrix b, 
    magma_z_matrix *x, magma_z_solver_par *solver_par, 
    magma_z_preconditioner *precond_par,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

/*
magma_int_t
magma_zpbicg(
    magma_z_matrix A, magma_z_matrix b, 
    magma_z_matrix *x, magma_z_solver_par *solver_par, 
    magma_z_preconditioner *precond_par,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}
*/

magma_int_t
magma_zpbicgstab(
    magma_z_matrix A, magma_z_matrix b, 
    magma_z_matrix *x, magma_z_solver_par *solver_par, 
    magma_z_preconditioner *precond_par,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

/*
magma_int_t
magma_zpbicgstab_merge(
    magma_z_matrix A, magma_z_matrix b, 
    magma_z_matrix *x, magma_z_solver_par *solver_par, 
    magma_z_preconditioner *precond_par,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}
*/

/*
magma_int_t
magma_zfgmres(
    magma_z_matrix A, magma_z_matrix b, 
    magma_z_matrix *x, magma_z_solver_par *solver_par, 
    magma_z_preconditioner *precond_par,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}
*/

magma_int_t
magma_zbfgmres(
    magma_z_matrix A, magma_z_matrix b, 
    magma_z_matrix *x, magma_z_solver_par *solver_par, 
    magma_z_preconditioner *precond_par,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

/*
magma_int_t
magma_zidr(
    magma_z_matrix A, magma_z_matrix b, 
    magma_z_matrix *x, magma_z_solver_par *solver_par,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}
*/
/*
magma_int_t
magma_zidr_merge(
    magma_z_matrix A, magma_z_matrix b, 
    magma_z_matrix *x, magma_z_solver_par *solver_par,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}
*/

magma_int_t
magma_zidr_strms(
    magma_z_matrix A, magma_z_matrix b, 
    magma_z_matrix *x, magma_z_solver_par *solver_par,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zpidr(
    magma_z_matrix A, magma_z_matrix b,
    magma_z_matrix *x, magma_z_solver_par *solver_par,
    magma_z_preconditioner *precond_par,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

/*
magma_int_t
magma_zpidr_merge(
    magma_z_matrix A, magma_z_matrix b,
    magma_z_matrix *x, magma_z_solver_par *solver_par,
    magma_z_preconditioner *precond_par,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}
*/

magma_int_t
magma_zpidr_strms(
    magma_z_matrix A, magma_z_matrix b,
    magma_z_matrix *x, magma_z_solver_par *solver_par,
    magma_z_preconditioner *precond_par,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zbombard(
    magma_z_matrix A, magma_z_matrix b, 
    magma_z_matrix *x, magma_z_solver_par *solver_par,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zbombard_merge(
    magma_z_matrix A, magma_z_matrix b, 
    magma_z_matrix *x, magma_z_solver_par *solver_par,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

/*
magma_int_t
magma_zjacobi(
    magma_z_matrix A, magma_z_matrix b, 
    magma_z_matrix *x, magma_z_solver_par *solver_par,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}
*/

magma_int_t
magma_zjacobidomainoverlap(
    magma_z_matrix A, 
    magma_z_matrix b, 
    magma_z_matrix *x,  
    magma_z_solver_par *solver_par,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zbaiter(
    magma_z_matrix A, magma_z_matrix b, 
    magma_z_matrix *x, magma_z_solver_par *solver_par,
    magma_z_preconditioner *precond_par,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zbaiter_overlap(
    magma_z_matrix A, magma_z_matrix b,
    magma_z_matrix *x, magma_z_solver_par *solver_par,
    magma_z_preconditioner *precond_par,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

/*
magma_int_t
magma_zftjacobicontractions(
    magma_z_matrix xkm2,
    magma_z_matrix xkm1, 
    magma_z_matrix xk, 
    magma_z_matrix *z,
    magma_z_matrix *c,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}
*/
/*
magma_int_t
magma_zftjacobiupdatecheck(
    double delta,
    magma_z_matrix *xold,
    magma_z_matrix *xnew, 
    magma_z_matrix *zprev, 
    magma_z_matrix c,
    magma_int_t *flag_t,
    magma_int_t *flag_fp,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}
*/

magma_int_t
magma_ziterref(
    magma_z_matrix A, magma_z_matrix b, 
    magma_z_matrix *x, magma_z_solver_par *solver_par, 
    magma_z_preconditioner *precond_par,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zbcsrlu(
    magma_z_matrix A, magma_z_matrix b, 
    magma_z_matrix *x, magma_z_solver_par *solver_par,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}


magma_int_t
magma_zbcsrlutrf(
    magma_z_matrix A, 
    magma_z_matrix *M,
    magma_int_t *ipiv, 
    magma_int_t version,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zbcsrlusv(
    magma_z_matrix A, magma_z_matrix b, 
    magma_z_matrix *x, magma_z_solver_par *solver_par, 
    magma_int_t *ipiv,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}



magma_int_t
magma_zilucg(
    magma_z_matrix A, magma_z_matrix b, 
    magma_z_matrix *x, magma_z_solver_par *solver_par,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zilugmres(
    magma_z_matrix A, magma_z_matrix b, 
    magma_z_matrix *x, magma_z_solver_par *solver_par,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

} 


magma_int_t
magma_zlobpcg_shift(
    magma_int_t num_rows,
    magma_int_t num_vecs, 
    magma_int_t shift,
    magmaDoubleComplex_ptr x,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zlobpcg_res(
    magma_int_t num_rows,
    magma_int_t num_vecs, 
    double *evalues, 
    magmaDoubleComplex_ptr X,
    magmaDoubleComplex_ptr R, 
    double *res,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zlobpcg_maxpy(
    magma_int_t num_rows,
    magma_int_t num_vecs, 
    magmaDoubleComplex_ptr X,
    magmaDoubleComplex_ptr Y,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}



/*/////////////////////////////////////////////////////////////////////////////
 -- MAGMA_SPARSE eigensolvers (Data on GPU)
*/
magma_int_t
magma_zlobpcg(
    magma_z_matrix A, 
    magma_z_solver_par *solver_par,
    magma_z_preconditioner *precond_par, 
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

/*/////////////////////////////////////////////////////////////////////////////
 -- MAGMA_SPARSE LSQR (Data on GPU)
*/
magma_int_t
magma_zlsqr(
    magma_z_matrix A, magma_z_matrix b, magma_z_matrix *x,
    magma_z_solver_par *solver_par,
    magma_z_preconditioner *precond_par,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}
/*
magma_int_t
magma_zresidual(
    magma_z_matrix A, 
    magma_z_matrix b, 
    magma_z_matrix x, 
    double *res,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zresidualvec(
    magma_z_matrix A,
    magma_z_matrix b,
    magma_z_matrix x,
    magma_z_matrix *r,
    double *res,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}


magma_int_t
magma_zresidual_slice(
    magma_int_t start,
    magma_int_t end,
    magma_z_matrix A, 
    magma_z_matrix b, 
    magma_z_matrix x,
    double *res,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}
*/

/*/////////////////////////////////////////////////////////////////////////////
 -- MAGMA_SPARSE preconditioners (Data on GPU)
*/
/*
magma_int_t
magma_zjacobisetup(
    magma_z_matrix A, 
    magma_z_matrix b, 
    magma_z_matrix *M, 
    magma_z_matrix *c,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zjacobisetup_matrix(
    magma_z_matrix A, 
    magma_z_matrix *M, 
    magma_z_matrix *d,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zjacobisetup_vector(
    magma_z_matrix b, 
    magma_z_matrix d, 
    magma_z_matrix *c,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zjacobiiter(
    magma_z_matrix M, 
    magma_z_matrix c, 
    magma_z_matrix *x, 
    magma_z_solver_par *solver_par,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zjacobiiter_precond( 
    magma_z_matrix M, 
    magma_z_matrix *x, 
    magma_z_solver_par *solver_par, 
    magma_z_preconditioner *precond,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zjacobiiter_sys(
    magma_z_matrix A, 
    magma_z_matrix b, 
    magma_z_matrix d, 
    magma_z_matrix t, 
    magma_z_matrix *x,  
    magma_z_solver_par *solver_par,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}
*/

magma_int_t
magma_zftjacobi(
    magma_z_matrix A, 
    magma_z_matrix b, 
    magma_z_matrix *x,  
    magma_z_solver_par *solver_par,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}


// CUSPARSE preconditioner
/*
magma_int_t
magma_zcuilusetup(
    magma_z_matrix A, magma_z_preconditioner *precond,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}
*/
/*
magma_int_t
magma_zcumilusetup_transpose(
    magma_z_matrix A, magma_z_preconditioner *precond,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}
*/

magma_int_t
magma_zapplycuilu_l(
    magma_z_matrix b, magma_z_matrix *x, 
    magma_z_preconditioner *precond,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zapplycuilu_r(
    magma_z_matrix b, magma_z_matrix *x, 
    magma_z_preconditioner *precond,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zcuiccsetup(
    magma_z_matrix A, magma_z_preconditioner *precond,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zapplycuicc_l(
    magma_z_matrix b, magma_z_matrix *x, 
    magma_z_preconditioner *precond,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zapplycuicc_r(
    magma_z_matrix b, magma_z_matrix *x, 
    magma_z_preconditioner *precond,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

/*
magma_int_t
magma_zcumilusetup(
    magma_z_matrix A, 
    magma_z_preconditioner *precond,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}
*/

magma_int_t
magma_zcustomilusetup(
    magma_z_matrix A,
    magma_z_matrix b,
    magma_z_preconditioner *precond,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zcustomicsetup(
    magma_z_matrix A,
    magma_z_matrix b,
    magma_z_preconditioner *precond,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

/*
magma_int_t
magma_zcumilugeneratesolverinfo(
    magma_z_preconditioner *precond,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}
*/
/*
magma_int_t
magma_zapplycumilu_l(
    magma_z_matrix b, 
    magma_z_matrix *x, 
    magma_z_preconditioner *precond,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zapplycumilu_r(
    magma_z_matrix b, 
    magma_z_matrix *x, 
    magma_z_preconditioner *precond,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zapplycumilu_l_transpose(
    magma_z_matrix b, magma_z_matrix *x, 
    magma_z_preconditioner *precond,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zapplycumilu_r_transpose(
    magma_z_matrix b, magma_z_matrix *x, 
    magma_z_preconditioner *precond,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}
*/

/*
magma_int_t
magma_zcumiccsetup(
    magma_z_matrix A, 
    magma_z_preconditioner *precond,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}
*/
/*
magma_int_t
magma_zcumicgeneratesolverinfo(
    magma_z_preconditioner *precond,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}
*/
/*
magma_int_t
magma_zapplycumicc_l(
    magma_z_matrix b, 
    magma_z_matrix *x, 
    magma_z_preconditioner *precond,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}
*/
/*
magma_int_t
magma_zapplycumicc_r(
    magma_z_matrix b, 
    magma_z_matrix *x, 
    magma_z_preconditioner *precond,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}
*/

// block-asynchronous iteration

magma_int_t
magma_zbajac_csr(
    magma_int_t localiters,
    magma_z_matrix D,
    magma_z_matrix R,
    magma_z_matrix b,
    magma_z_matrix *x,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zbajac_csr_overlap(
    magma_int_t localiters,
    magma_int_t matrices,
    magma_int_t overlap,
    magma_z_matrix *D,
    magma_z_matrix *R,
    magma_z_matrix b,
    magma_z_matrix *x,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}


magma_int_t
magma_zcuspmm(
    magma_z_matrix A, 
    magma_z_matrix B, 
    magma_z_matrix *AB,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}
/*
magma_int_t
magma_z_spmm(
    magmaDoubleComplex alpha, 
    magma_z_matrix A,
    magma_z_matrix B,
    magma_z_matrix *C,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}
*/

magma_int_t
magma_zcuspaxpy(
    magmaDoubleComplex_ptr alpha, magma_z_matrix A, 
    magmaDoubleComplex_ptr beta, magma_z_matrix B, 
    magma_z_matrix *AB,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}


magma_int_t
magma_z_initP2P(
    magma_int_t *bandwidth_benchmark,
    magma_int_t *num_gpus,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zcompact(
    magma_int_t m, magma_int_t n,
    magmaDoubleComplex_ptr dA, magma_int_t ldda,
    double *dnorms, double tol, 
    magma_int_t *activeMask, magma_int_t *cBlockSize,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zcompactActive(
    magma_int_t m, magma_int_t n,
    magmaDoubleComplex_ptr dA, magma_int_t ldda, 
    magma_int_t *active,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}


magma_int_t
magma_zdiagcheck(
    magma_z_matrix dA,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}


/*/////////////////////////////////////////////////////////////////////////////
 -- MAGMA_SPARSE wrappers to dense MAGMA
*/
/*
magma_int_t
magma_zqr(
    magma_int_t m, 
    magma_int_t n, 
    magma_z_matrix A, 
    magma_int_t lda, 
    magma_z_matrix *Q, 
    magma_z_matrix *R, 
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}
*/

/* ////////////////////////////////////////////////////////////////////////////
 -- MAGMA_SPARSE BLAS function definitions
*/

magma_int_t
magma_zgeaxpy(
    magmaDoubleComplex alpha,
    magma_z_matrix X,
    magmaDoubleComplex beta,
    magma_z_matrix *Y,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zgecsrreimsplit(
    magma_z_matrix A,
    magma_z_matrix *ReA,
    magma_z_matrix *ImA,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zgedensereimsplit(
    magma_z_matrix A,
    magma_z_matrix *ReA,
    magma_z_matrix *ImA,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t 
magma_zgecsrmv(
    magma_trans_t transA,
    magma_int_t m, magma_int_t n,
    magmaDoubleComplex alpha,
    magmaDoubleComplex_ptr dval,
    magmaIndex_ptr drowptr,
    magmaIndex_ptr dcolind,
    magmaDoubleComplex_ptr dx,
    magmaDoubleComplex beta,
    magmaDoubleComplex_ptr dy,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t 
magma_zgecsrmv_shift(
    magma_trans_t transA,
    magma_int_t m, magma_int_t n,
    magmaDoubleComplex alpha,
    magmaDoubleComplex lambda,
    magmaDoubleComplex_ptr dval,
    magmaIndex_ptr drowptr,
    magmaIndex_ptr dcolind,
    magmaDoubleComplex_ptr dx,
    magmaDoubleComplex beta,
    magma_int_t offset,
    magma_int_t blocksize,
    magmaIndex_ptr dadd_rows,
    magmaDoubleComplex_ptr dy,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t 
magma_zmgecsrmv(
    magma_trans_t transA,
    magma_int_t m, magma_int_t n,
    magma_int_t num_vecs,
    magmaDoubleComplex alpha,
    magmaDoubleComplex_ptr dval,
    magmaIndex_ptr drowptr,
    magmaIndex_ptr dcolind,
    magmaDoubleComplex_ptr dx,
    magmaDoubleComplex beta,
    magmaDoubleComplex_ptr dy,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t 
magma_zgeellmv(
    magma_trans_t transA,
    magma_int_t m, magma_int_t n,
    magma_int_t nnz_per_row,
    magmaDoubleComplex alpha,
    magmaDoubleComplex_ptr dval,
    magmaIndex_ptr dcolind,
    magmaDoubleComplex_ptr dx,
    magmaDoubleComplex beta,
    magmaDoubleComplex_ptr dy,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t 
magma_zgeellmv_shift(
    magma_trans_t transA,
    magma_int_t m, magma_int_t n,
    magma_int_t nnz_per_row,
    magmaDoubleComplex alpha,
    magmaDoubleComplex lambda,
    magmaDoubleComplex_ptr dval,
    magmaIndex_ptr dcolind,
    magmaDoubleComplex_ptr dx,
    magmaDoubleComplex beta,
    magma_int_t offset,
    magma_int_t blocksize,
    magmaIndex_ptr dadd_rows,
    magmaDoubleComplex_ptr dy,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}


magma_int_t 
magma_zmgeellmv(
    magma_trans_t transA,
    magma_int_t m, magma_int_t n,
    magma_int_t num_vecs,
    magma_int_t nnz_per_row,
    magmaDoubleComplex alpha,
    magmaDoubleComplex_ptr dval,
    magmaIndex_ptr dcolind,
    magmaDoubleComplex_ptr dx,
    magmaDoubleComplex beta,
    magmaDoubleComplex_ptr dy,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}


magma_int_t 
magma_zgeelltmv(
    magma_trans_t transA,
    magma_int_t m, magma_int_t n,
    magma_int_t nnz_per_row,
    magmaDoubleComplex alpha,
    magmaDoubleComplex_ptr dval,
    magmaIndex_ptr dcolind,
    magmaDoubleComplex_ptr dx,
    magmaDoubleComplex beta,
    magmaDoubleComplex_ptr dy,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t 
magma_zgeelltmv_shift(
    magma_trans_t transA,
    magma_int_t m, magma_int_t n,
    magma_int_t nnz_per_row,
    magmaDoubleComplex alpha,
    magmaDoubleComplex lambda,
    magmaDoubleComplex_ptr dval,
    magmaIndex_ptr dcolind,
    magmaDoubleComplex_ptr dx,
    magmaDoubleComplex beta,
    magma_int_t offset,
    magma_int_t blocksize,
    magmaIndex_ptr dadd_rows,
    magmaDoubleComplex_ptr dy,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}


magma_int_t 
magma_zmgeelltmv(
    magma_trans_t transA,
    magma_int_t m, magma_int_t n,
    magma_int_t num_vecs,
    magma_int_t nnz_per_row,
    magmaDoubleComplex alpha,
    magmaDoubleComplex_ptr dval,
    magmaIndex_ptr dcolind,
    magmaDoubleComplex_ptr dx,
    magmaDoubleComplex beta,
    magmaDoubleComplex_ptr dy,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t 
magma_zgeellrtmv(
    magma_trans_t transA,
    magma_int_t m, magma_int_t n,
    magma_int_t nnz_per_row,
    magmaDoubleComplex alpha,
    magmaDoubleComplex_ptr dval,
    magmaIndex_ptr dcolind,
    magmaIndex_ptr drowlength,
    magmaDoubleComplex_ptr dx,
    magmaDoubleComplex beta,
    magmaDoubleComplex_ptr dy,
    magma_int_t num_threads,
    magma_int_t threads_per_row,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t 
magma_zgesellcmv(
    magma_trans_t transA,
    magma_int_t m, magma_int_t n,
    magma_int_t blocksize,
    magma_int_t slices,
    magma_int_t alignment,
    magmaDoubleComplex alpha,
    magmaDoubleComplex_ptr dval,
    magmaIndex_ptr dcolind,
    magmaIndex_ptr drowptr,
    magmaDoubleComplex_ptr dx,
    magmaDoubleComplex beta,
    magmaDoubleComplex_ptr dy,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zgesellpmv(
    magma_trans_t transA,
    magma_int_t m, magma_int_t n,
    magma_int_t blocksize,
    magma_int_t slices,
    magma_int_t alignment,
    magmaDoubleComplex alpha,
    magmaDoubleComplex_ptr dval,
    magmaIndex_ptr dcolind,
    magmaIndex_ptr drowptr,
    magmaDoubleComplex_ptr dx,
    magmaDoubleComplex beta,
    magmaDoubleComplex_ptr dy,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zmgesellpmv(
    magma_trans_t transA,
    magma_int_t m, magma_int_t n,
    magma_int_t num_vecs,
    magma_int_t blocksize,
    magma_int_t slices,
    magma_int_t alignment,
    magmaDoubleComplex alpha,
    magmaDoubleComplex_ptr dval,
    magmaIndex_ptr dcolind,
    magmaIndex_ptr drowptr,
    magmaDoubleComplex_ptr dx,
    magmaDoubleComplex beta,
    magmaDoubleComplex_ptr dy,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zmgesellpmv_blocked(
    magma_trans_t transA,
    magma_int_t m, magma_int_t n,
    magma_int_t num_vecs,
    magma_int_t blocksize,
    magma_int_t slices,
    magma_int_t alignment,
    magmaDoubleComplex alpha,
    magmaDoubleComplex_ptr dval,
    magmaIndex_ptr dcolind,
    magmaIndex_ptr drowptr,
    magmaDoubleComplex_ptr dx,
    magmaDoubleComplex beta,
    magmaDoubleComplex_ptr dy,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}


magma_int_t
magma_zmergedgs(
    magma_int_t n, 
    magma_int_t ldh,
    magma_int_t k, 
    magmaDoubleComplex_ptr dv, 
    magmaDoubleComplex_ptr dr,
    magmaDoubleComplex_ptr dskp,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zcopyscale(    
    magma_int_t n, 
    magma_int_t k,
    magmaDoubleComplex_ptr dr, 
    magmaDoubleComplex_ptr dv,
    magmaDoubleComplex_ptr dskp,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_dznrm2scale(    
    magma_int_t m, 
    magmaDoubleComplex_ptr dr,    
    magma_int_t lddr, 
    magmaDoubleComplex *drnorm,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

/*
magma_int_t
magma_zjacobisetup_vector_gpu(
    magma_int_t num_rows, 
    magma_z_matrix b, 
    magma_z_matrix d, 
    magma_z_matrix c,
    magma_z_matrix *x,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}
*/
/*
magma_int_t
magma_zjacobi_diagscal(    
    magma_int_t num_rows, 
    magma_z_matrix d, 
    magma_z_matrix b, 
    magma_z_matrix *c,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}
*/
/*
magma_int_t
magma_zjacobiupdate(
    magma_z_matrix t, 
    magma_z_matrix b, 
    magma_z_matrix d, 
    magma_z_matrix *x,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}
*/
/*
magma_int_t
magma_zjacobispmvupdate(
    magma_int_t maxiter,
    magma_z_matrix A, 
    magma_z_matrix t, 
    magma_z_matrix b, 
    magma_z_matrix d, 
    magma_z_matrix *x,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}
*/
/*
magma_int_t
magma_zjacobispmvupdate_bw(
    magma_int_t maxiter,
    magma_z_matrix A, 
    magma_z_matrix t, 
    magma_z_matrix b, 
    magma_z_matrix d, 
    magma_z_matrix *x,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}
*/
/*
magma_int_t
magma_zjacobispmvupdateselect(
    magma_int_t maxiter,
    magma_int_t num_updates,
    magma_index_t *indices,
    magma_z_matrix A,
    magma_z_matrix t, 
    magma_z_matrix b, 
    magma_z_matrix d, 
    magma_z_matrix tmp, 
    magma_z_matrix *x,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}
*/
/*
magma_int_t
magma_zjacobisetup_diagscal(
    magma_z_matrix A, magma_z_matrix *d,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}
*/

//##################   kernel fusion for Krylov methods

magma_int_t
magma_zmergeblockkrylov(  
    magma_int_t num_rows, 
    magma_int_t num_cols, 
    magmaDoubleComplex_ptr alpha, 
    magmaDoubleComplex_ptr p,
    magmaDoubleComplex_ptr x,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zbicgmerge1(    
    magma_int_t n, 
    magmaDoubleComplex_ptr dskp,
    magmaDoubleComplex_ptr dv, 
    magmaDoubleComplex_ptr dr, 
    magmaDoubleComplex_ptr dp,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}


magma_int_t
magma_zbicgmerge2(
    magma_int_t n, 
    magmaDoubleComplex_ptr dskp, 
    magmaDoubleComplex_ptr dr,
    magmaDoubleComplex_ptr dv, 
    magmaDoubleComplex_ptr ds,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zbicgmerge3(
    magma_int_t n, 
    magmaDoubleComplex_ptr dskp, 
    magmaDoubleComplex_ptr dp,
    magmaDoubleComplex_ptr ds,
    magmaDoubleComplex_ptr dt,
    magmaDoubleComplex_ptr dx, 
    magmaDoubleComplex_ptr dr,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zbicgmerge4(
    magma_int_t type, 
    magmaDoubleComplex_ptr dskp,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

/*
magma_int_t
magma_zidr_smoothing_1(  
    magma_int_t num_rows, 
    magma_int_t num_cols, 
    magmaDoubleComplex_ptr drs,
    magmaDoubleComplex_ptr dr, 
    magmaDoubleComplex_ptr dt, 
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}
*/
/*
magma_int_t
magma_zidr_smoothing_2(  
    magma_int_t num_rows, 
    magma_int_t num_cols, 
    magmaDoubleComplex omega,
    magmaDoubleComplex_ptr dx,
    magmaDoubleComplex_ptr dxs, 
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}
*/
/*
magma_int_t
magma_zcgs_1(  
    magma_int_t num_rows, 
    magma_int_t num_cols, 
    magmaDoubleComplex beta,
    magmaDoubleComplex_ptr r,
    magmaDoubleComplex_ptr q, 
    magmaDoubleComplex_ptr u,
    magmaDoubleComplex_ptr p,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zcgs_2(  
    magma_int_t num_rows, 
    magma_int_t num_cols, 
    magmaDoubleComplex_ptr r,
    magmaDoubleComplex_ptr u,
    magmaDoubleComplex_ptr p, 
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zcgs_3(  
    magma_int_t num_rows, 
    magma_int_t num_cols, 
    magmaDoubleComplex alpha,
    magmaDoubleComplex_ptr v_hat,
    magmaDoubleComplex_ptr u, 
    magmaDoubleComplex_ptr q,
    magmaDoubleComplex_ptr t, 
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zcgs_4(  
    magma_int_t num_rows, 
    magma_int_t num_cols, 
    magmaDoubleComplex alpha,
    magmaDoubleComplex_ptr u_hat,
    magmaDoubleComplex_ptr t,
    magmaDoubleComplex_ptr x, 
    magmaDoubleComplex_ptr r,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}
*/
/*
magma_int_t
magma_zqmr_1(  
    magma_int_t num_rows, 
    magma_int_t num_cols, 
    magmaDoubleComplex rho,
    magmaDoubleComplex psi,
    magmaDoubleComplex_ptr y, 
    magmaDoubleComplex_ptr z,
    magmaDoubleComplex_ptr v,
    magmaDoubleComplex_ptr w,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}


magma_int_t
magma_zqmr_2(  
    magma_int_t num_rows, 
    magma_int_t num_cols, 
    magmaDoubleComplex pde,
    magmaDoubleComplex rde,
    magmaDoubleComplex_ptr y,
    magmaDoubleComplex_ptr z,
    magmaDoubleComplex_ptr p, 
    magmaDoubleComplex_ptr q, 
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zqmr_3(  
    magma_int_t num_rows, 
    magma_int_t num_cols, 
    magmaDoubleComplex beta,
    magmaDoubleComplex_ptr pt,
    magmaDoubleComplex_ptr v,
    magmaDoubleComplex_ptr y,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zqmr_4(  
    magma_int_t num_rows, 
    magma_int_t num_cols, 
    magmaDoubleComplex eta,
    magmaDoubleComplex_ptr p,
    magmaDoubleComplex_ptr pt,
    magmaDoubleComplex_ptr d, 
    magmaDoubleComplex_ptr s, 
    magmaDoubleComplex_ptr x, 
    magmaDoubleComplex_ptr r, 
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zqmr_5(  
    magma_int_t num_rows, 
    magma_int_t num_cols, 
    magmaDoubleComplex eta,
    magmaDoubleComplex pds,
    magmaDoubleComplex_ptr p,
    magmaDoubleComplex_ptr pt,
    magmaDoubleComplex_ptr d, 
    magmaDoubleComplex_ptr s, 
    magmaDoubleComplex_ptr x, 
    magmaDoubleComplex_ptr r, 
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zqmr_6(  
    magma_int_t num_rows, 
    magma_int_t num_cols, 
    magmaDoubleComplex beta,
    magmaDoubleComplex rho,
    magmaDoubleComplex psi,
    magmaDoubleComplex_ptr y, 
    magmaDoubleComplex_ptr z,
    magmaDoubleComplex_ptr v,
    magmaDoubleComplex_ptr w,
    magmaDoubleComplex_ptr wt,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zqmr_7(  
    magma_int_t num_rows, 
    magma_int_t num_cols, 
    magmaDoubleComplex beta,
    magmaDoubleComplex_ptr pt,
    magmaDoubleComplex_ptr v,
    magmaDoubleComplex_ptr vt,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zqmr_8(  
    magma_int_t num_rows, 
    magma_int_t num_cols, 
    magmaDoubleComplex rho,
    magmaDoubleComplex psi,
    magmaDoubleComplex_ptr vt,
    magmaDoubleComplex_ptr wt,
    magmaDoubleComplex_ptr y, 
    magmaDoubleComplex_ptr z,
    magmaDoubleComplex_ptr v,
    magmaDoubleComplex_ptr w,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}
*/

/*
magma_int_t
magma_zbicgstab_1(  
    magma_int_t num_rows, 
    magma_int_t num_cols, 
    magmaDoubleComplex beta,
    magmaDoubleComplex omega,
    magmaDoubleComplex_ptr r, 
    magmaDoubleComplex_ptr v,
    magmaDoubleComplex_ptr p,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zbicgstab_2(  
    magma_int_t num_rows, 
    magma_int_t num_cols, 
    magmaDoubleComplex alpha,
    magmaDoubleComplex_ptr r,
    magmaDoubleComplex_ptr v,
    magmaDoubleComplex_ptr s, 
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zbicgstab_3(  
    magma_int_t num_rows, 
    magma_int_t num_cols, 
    magmaDoubleComplex alpha,
    magmaDoubleComplex omega,
    magmaDoubleComplex_ptr p,
    magmaDoubleComplex_ptr s,
    magmaDoubleComplex_ptr t,
    magmaDoubleComplex_ptr x,
    magmaDoubleComplex_ptr r,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zbicgstab_4(  
    magma_int_t num_rows, 
    magma_int_t num_cols, 
    magmaDoubleComplex alpha,
    magmaDoubleComplex omega,
    magmaDoubleComplex_ptr y,
    magmaDoubleComplex_ptr z,
    magmaDoubleComplex_ptr s,
    magmaDoubleComplex_ptr t,
    magmaDoubleComplex_ptr x,
    magmaDoubleComplex_ptr r,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}
*/

/*
magma_int_t
magma_ztfqmr_1(  
    magma_int_t num_rows, 
    magma_int_t num_cols, 
    magmaDoubleComplex alpha,
    magmaDoubleComplex sigma,
    magmaDoubleComplex_ptr v, 
    magmaDoubleComplex_ptr Au,
    magmaDoubleComplex_ptr u_m,
    magmaDoubleComplex_ptr pu_m,
    magmaDoubleComplex_ptr u_mp1,
    magmaDoubleComplex_ptr w, 
    magmaDoubleComplex_ptr d,
    magmaDoubleComplex_ptr Ad,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_ztfqmr_2(  
    magma_int_t num_rows, 
    magma_int_t num_cols, 
    magmaDoubleComplex eta,
    magmaDoubleComplex_ptr d,
    magmaDoubleComplex_ptr Ad,
    magmaDoubleComplex_ptr x, 
    magmaDoubleComplex_ptr r, 
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_ztfqmr_3(  
    magma_int_t num_rows, 
    magma_int_t num_cols, 
    magmaDoubleComplex beta,
    magmaDoubleComplex_ptr w,
    magmaDoubleComplex_ptr u_m,
    magmaDoubleComplex_ptr u_mp1, 
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_ztfqmr_4(  
    magma_int_t num_rows, 
    magma_int_t num_cols, 
    magmaDoubleComplex beta,
    magmaDoubleComplex_ptr Au_new,
    magmaDoubleComplex_ptr v,
    magmaDoubleComplex_ptr Au, 
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_ztfqmr_5(  
    magma_int_t num_rows, 
    magma_int_t num_cols, 
    magmaDoubleComplex alpha,
    magmaDoubleComplex sigma,
    magmaDoubleComplex_ptr v, 
    magmaDoubleComplex_ptr Au,
    magmaDoubleComplex_ptr u_mp1,
    magmaDoubleComplex_ptr w, 
    magmaDoubleComplex_ptr d,
    magmaDoubleComplex_ptr Ad,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}
*/

magma_int_t
magma_zcgmerge_spmv1( 
    magma_z_matrix A,
    magmaDoubleComplex_ptr d1,
    magmaDoubleComplex_ptr d2,
    magmaDoubleComplex_ptr dd,
    magmaDoubleComplex_ptr dz,
    magmaDoubleComplex_ptr dskp,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zcgmerge_xrbeta( 
    magma_int_t n,
    magmaDoubleComplex_ptr d1,
    magmaDoubleComplex_ptr d2,
    magmaDoubleComplex_ptr dx,
    magmaDoubleComplex_ptr dr,
    magmaDoubleComplex_ptr dd,
    magmaDoubleComplex_ptr dz, 
    magmaDoubleComplex_ptr dskp,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}


magma_int_t
magma_zpcgmerge_xrbeta1(
    magma_int_t n,
    magmaDoubleComplex_ptr dx,
    magmaDoubleComplex_ptr dr,
    magmaDoubleComplex_ptr dd,
    magmaDoubleComplex_ptr dz, 
    magmaDoubleComplex_ptr skp,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zpcgmerge_xrbeta2(
    magma_int_t n,
    magmaDoubleComplex_ptr d1,
    magmaDoubleComplex_ptr d2,
    magmaDoubleComplex_ptr dh,
    magmaDoubleComplex_ptr dr, 
    magmaDoubleComplex_ptr dd, 
    magmaDoubleComplex_ptr skp,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zjcgmerge_xrbeta(
    magma_int_t n,
    magmaDoubleComplex_ptr d1,
    magmaDoubleComplex_ptr d2,
    magmaDoubleComplex_ptr diag,
    magmaDoubleComplex_ptr dx,
    magmaDoubleComplex_ptr dr,
    magmaDoubleComplex_ptr dd,
    magmaDoubleComplex_ptr dz,
    magmaDoubleComplex_ptr dh, 
    magmaDoubleComplex_ptr skp,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zmdotc_shfl(
    magma_int_t n, 
    magma_int_t k, 
    magmaDoubleComplex_ptr dv, 
    magmaDoubleComplex_ptr dr,
    magmaDoubleComplex_ptr dd1,
    magmaDoubleComplex_ptr dd2,
    magmaDoubleComplex_ptr dskp,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zmdotc(
    magma_int_t n, 
    magma_int_t k, 
    magmaDoubleComplex_ptr dv, 
    magmaDoubleComplex_ptr dr,
    magmaDoubleComplex_ptr dd1,
    magmaDoubleComplex_ptr dd2,
    magmaDoubleComplex_ptr dskp,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

/*
magma_int_t
magma_zgemvmdot_shfl(
    magma_int_t n, 
    magma_int_t k, 
    magmaDoubleComplex_ptr dv, 
    magmaDoubleComplex_ptr dr,
    magmaDoubleComplex_ptr dd1,
    magmaDoubleComplex_ptr dd2,
    magmaDoubleComplex_ptr dskp,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}
*/


magma_int_t
magma_zgemvmdot(
    magma_int_t n, 
    magma_int_t k, 
    magmaDoubleComplex_ptr dv, 
    magmaDoubleComplex_ptr dr,
    magmaDoubleComplex_ptr dd1,
    magmaDoubleComplex_ptr dd2,
    magmaDoubleComplex_ptr dskp,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}


magma_int_t
magma_zmdotc1(
    magma_int_t n,  
    magmaDoubleComplex_ptr v0, 
    magmaDoubleComplex_ptr w0,
    magmaDoubleComplex_ptr d1,
    magmaDoubleComplex_ptr d2,
    magmaDoubleComplex_ptr skp,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zmdotc2(
    magma_int_t n,  
    magmaDoubleComplex_ptr v0, 
    magmaDoubleComplex_ptr w0,
    magmaDoubleComplex_ptr v1, 
    magmaDoubleComplex_ptr w1,
    magmaDoubleComplex_ptr d1,
    magmaDoubleComplex_ptr d2,
    magmaDoubleComplex_ptr skp,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zmdotc3(
    magma_int_t n,  
    magmaDoubleComplex_ptr v0, 
    magmaDoubleComplex_ptr w0,
    magmaDoubleComplex_ptr v1, 
    magmaDoubleComplex_ptr w1,
    magmaDoubleComplex_ptr v2, 
    magmaDoubleComplex_ptr w2,
    magmaDoubleComplex_ptr d1,
    magmaDoubleComplex_ptr d2,
    magmaDoubleComplex_ptr skp,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zmdotc4(
    magma_int_t n,  
    magmaDoubleComplex_ptr v0, 
    magmaDoubleComplex_ptr w0,
    magmaDoubleComplex_ptr v1, 
    magmaDoubleComplex_ptr w1,
    magmaDoubleComplex_ptr v2, 
    magmaDoubleComplex_ptr w2,
    magmaDoubleComplex_ptr v3, 
    magmaDoubleComplex_ptr w3,
    magmaDoubleComplex_ptr d1,
    magmaDoubleComplex_ptr d2,
    magmaDoubleComplex_ptr skp,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zbicgmerge_spmv1( 
    magma_z_matrix A,
    magmaDoubleComplex_ptr dd1,
    magmaDoubleComplex_ptr dd2,
    magmaDoubleComplex_ptr dp,
    magmaDoubleComplex_ptr dr,
    magmaDoubleComplex_ptr dv,
    magmaDoubleComplex_ptr dskp,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zbicgmerge_spmv2( 
    magma_z_matrix A,
    magmaDoubleComplex_ptr dd1,
    magmaDoubleComplex_ptr dd2,
    magmaDoubleComplex_ptr ds,
    magmaDoubleComplex_ptr dt,
    magmaDoubleComplex_ptr dskp,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zbicgmerge_xrbeta( 
    magma_int_t n,
    magmaDoubleComplex_ptr dd1,
    magmaDoubleComplex_ptr dd2,
    magmaDoubleComplex_ptr drr,
    magmaDoubleComplex_ptr dr,
    magmaDoubleComplex_ptr dp,
    magmaDoubleComplex_ptr ds,
    magmaDoubleComplex_ptr dt,
    magmaDoubleComplex_ptr dx, 
    magmaDoubleComplex_ptr dskp,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zbcsrswp(
    magma_int_t n,
    magma_int_t size_b, 
    magma_int_t *ipiv,
    magmaDoubleComplex_ptr dx,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zbcsrtrsv(
    magma_uplo_t uplo,
    magma_int_t r_blocks,
    magma_int_t c_blocks,
    magma_int_t size_b, 
    magmaDoubleComplex_ptr dA,
    magma_index_t *blockinfo, 
    magmaDoubleComplex_ptr dx,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zbcsrvalcpy(
    magma_int_t size_b, 
    magma_int_t num_blocks, 
    magma_int_t num_zero_blocks, 
    magmaDoubleComplex_ptr *dAval, 
    magmaDoubleComplex_ptr *dBval,
    magmaDoubleComplex_ptr *dBval2,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zbcsrluegemm(
    magma_int_t size_b, 
    magma_int_t num_block_rows,
    magma_int_t kblocks,
    magmaDoubleComplex_ptr *dA, 
    magmaDoubleComplex_ptr *dB, 
    magmaDoubleComplex_ptr *dC,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zbcsrlupivloc(
    magma_int_t size_b, 
    magma_int_t kblocks,
    magmaDoubleComplex_ptr *dA, 
    magma_int_t *ipiv,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zbcsrblockinfo5(
    magma_int_t lustep,
    magma_int_t num_blocks, 
    magma_int_t c_blocks, 
    magma_int_t size_b,
    magma_index_t *blockinfo,
    magmaDoubleComplex_ptr dval,
    magmaDoubleComplex_ptr *AII,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}


magma_int_t
magma_ztestasync( 
    magma_z_matrix A,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zmcsrcompressor_gpu( 
    magma_z_matrix *A,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}


magma_int_t 
magma_z_cucsrtranspose( 
    magma_z_matrix A, 
    magma_z_matrix *B,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zitericsetup( 
    magma_z_matrix A, 
    magma_z_matrix b, 
    magma_z_preconditioner *precond,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zitericupdate( 
    magma_z_matrix A, 
    magma_z_preconditioner *precond, 
    magma_int_t updates,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zapplyiteric_l( 
    magma_z_matrix b, 
    magma_z_matrix *x, 
    magma_z_preconditioner *precond,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

magma_int_t
magma_zapplyiteric_r( 
    magma_z_matrix b, 
    magma_z_matrix *x, 
    magma_z_preconditioner *precond,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

/*
magma_int_t
magma_zparilu_csr( 
    magma_z_matrix A,
    magma_z_matrix L,
    magma_z_matrix U,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}
*/
/*
magma_int_t
magma_zpariluupdate(
    magma_z_matrix A,
    magma_z_preconditioner *precond,
    magma_int_t updates,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}
*/

magma_int_t
magma_ziteric_csr( 
    magma_z_matrix A,
    magma_z_matrix A_CSR,
    magma_queue_t queue ){

    return MAGMA_SUCCESS;

}

 