/*
    -- MAGMA (version 1.1) --
       Univ. of Tennessee, Knoxville
       Univ. of California, Berkeley
       Univ. of Colorado, Denver
       @date

       @precisions normal z -> c d s
       @author Hartwig Anzt
       @author Stephen Wood
*/

// includes, system
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

// includes, project
#include "magma_v2.h"
#include "magmasparse.h"
#include "testings.h"

#ifdef HAVE_PAPI
#include "papi.h"
#define PAPI

//#define MEASURE_EACH
#define MEASURE_GROUP

// For now use the define macro to select hardware for PAPI to monitor
//#define ARCH_WESTMERE
//#define ARCH_GAINESTOWN
#define ARCH_HASWELL
//#define ARCH_KNL
//#define ARCH_KNL_POWER
//#define ARCH_K20M
//#define ARCH_K40C

#if defined ARCH_WESTMERE
  #define NUM_EVENTS 5
#elif defined ARCH_GAINESTOWN
  #define NUM_EVENTS 4
#elif defined ARCH_HASWELL
  #define NUM_EVENTS 4
#elif defined ARCH_KNL
  #define NUM_EVENTS 4
#elif defined ARCH_KNL_POWER
  //#define NUM_EVENTS 6
  #define NUM_EVENTS 2
#elif defined ARCH_K20M
  #define NUM_EVENTS 1  
#elif defined ARCH_K40C
  #define NUM_EVENTS 1
#endif

#endif

/* ////////////////////////////////////////////////////////////////////////////
   -- testing any solver
*/
int main(  int argc, char** argv )
{
    magma_int_t info = 0;
    TESTING_CHECK( magma_init() );
    magma_print_environment();

    magma_zopts zopts;
    magma_queue_t queue=NULL;
    magma_queue_create( 0, &queue );
    
    // chronometry
    real_Double_t tempo1, tempo2, runtL, runtU, eL=0.0, eU=0.0;
    magma_int_t *power = NULL;
    
    magmaDoubleComplex one = MAGMA_Z_MAKE(1.0, 0.0);
    magmaDoubleComplex zero = MAGMA_Z_MAKE(0.0, 0.0);
    magma_z_matrix A={Magma_CSR}, B={Magma_CSR}, B_d={Magma_CSR};
    magma_z_matrix x={Magma_CSR}, b={Magma_CSR};
    
    magmaDoubleComplex *trisystems_d = NULL; 
    magmaDoubleComplex *rhs_d = NULL; 
    magma_index_t *sizes_d = NULL, *locations_d = NULL; 
    magma_index_t *sizes_h = NULL; 
    magma_int_t maxsize, nnzloc, nnzL=0, nnzU=0;
    int warpsize=32;
    int offset = 0; // can be changed to better match the matrix structure
    magma_z_matrix LT={Magma_CSR}, MT={Magma_CSR}, QT={Magma_CSR};
    magma_int_t z;
    
    int i=1;
    
#ifdef PAPI
    int ii, retval;
    int EventSet = PAPI_NULL;
    long long values[NUM_EVENTS];
    /* REPLACE THE EVENT NAME 'PAPI_FP_OPS' WITH A CUDA EVENT 
       FOR THE CUDA DEVICE YOU ARE RUNNING ON.
       RUN papi_native_avail to get a list of CUDA events that are 
       supported on your machine */
#if defined ARCH_GAINESTOWN || defined ARCH_WESTMERE       
    char *EventName[] = { "PAPI_L1_DCM", "PAPI_L1_ICM", "PAPI_FP_OPS", 
      "PAPI_FP_INS", "PAPI_TOT_INS" }; 
    // cache metrics
    long long L_L1_dcm=0.0, L_L1_icm=0.0, U_L1_dcm=0.0, U_L1_icm=0.0;
    long long L1_dcm=0, L1_icm=0;
    // cycle metrics
    long long L_tot_ins=0, L_fp_ops=0, L_fp_ins=0, U_tot_ins=0, U_fp_ops=0, U_fp_ins=0;
    
#elif defined ARCH_HASWELL      
    char *EventName[] = { "PAPI_L2_DCM", "PAPI_L2_ICM", "PAPI_L2_DCA", "PAPI_L2_ICA" }; 
    // cache metrics
    real_Double_t L_L2_dcmr=0.0, L_L2_icmr=0.0, U_L2_dcmr=0.0, U_L2_icmr=0.0;
    long long L2_dcm=0, L2_icm=0, L2_dca=0, L2_ica=0;
    
#elif defined ARCH_KNL
    char *EventName[] = { "PAPI_L1_DCM", "PAPI_L1_ICM", "PAPI_TOT_INS", "PAPI_REF_CYC" };
    // cache metrics
    real_Double_t L_L1_dcm=0.0, L_L1_icm=0.0, U_L1_dcm=0.0, U_L1_icm=0.0;
    magma_int_t L1_dcm=0, L1_icm=0;
    // cycle metrics
    real_Double_t L_rsr=0.0, U_rsr=0.0;
    long long L_res_stl=0, L_tot_ins=0, U_res_stl=0, U_tot_ins=0;
    
#elif defined ARCH_KNL_POWER
    //char *EventName[] = { "powercap:::ENERGY_UJ:ZONE0", "powercap:::MAX_ENERGY_RANGE_UJ:ZONE0", 
    //  "powercap:::MAX_POWER_A_UW:ZONE0", "powercap:::ENERGY_UJ:ZONE0_SUBZONE1",
    //  "powercap:::MAX_ENERGY_RANGE_UJ:ZONE0_SUBZONE1", "powercap:::MAX_POWER_A_UW:ZONE0_SUBZONE1" };
    char *EventName[] = { "powercap:::ENERGY_UJ:ZONE0", "powercap:::ENERGY_UJ:ZONE0_SUBZONE1" }; 
    long long L_energy[2], U_energy[2];
    L_energy[0] = 0;
    L_energy[1] = 0;
    U_energy[0] = 0;
    U_energy[1] = 0;

#elif defined ARCH_K20M
    char *EventName[] = { "nvml:::Tesla_K20m:power" };
    
#elif defined ARCH_K40C
    char *EventName[] = { "nvml:::Tesla_K40c:power" };
    
#endif

    int events[NUM_EVENTS];
    int eventCount = 0;
    
    /* PAPI Initialization */
    retval = PAPI_library_init( PAPI_VER_CURRENT );
    if( retval != PAPI_VER_CURRENT )
        fprintf( stderr, "PAPI_library_init failed\n" );
    
    printf( "%% PAPI_VERSION     : %4d %6d %7d\n",
            PAPI_VERSION_MAJOR( PAPI_VERSION ),
            PAPI_VERSION_MINOR( PAPI_VERSION ),
            PAPI_VERSION_REVISION( PAPI_VERSION ) );
    
    /* convert PAPI native events to PAPI code */
    for( ii = 0; ii < NUM_EVENTS; ii++ ){
        retval = PAPI_event_name_to_code( EventName[ii], &events[ii] );
        if( retval != PAPI_OK ) {
            fprintf( stderr, "PAPI_event_name_to_code failed\n" );
            continue;
        }
        eventCount++;
            printf( "%% Name %s --- Code: %#x\n", EventName[ii], events[ii] );
    }

    /* if we did not find any valid events, just report test failed. */
    if (eventCount == 0) {
        printf( "Test FAILED: no valid events found.\n");
        return 1;
    }
    
    retval = PAPI_create_eventset( &EventSet );
    if( retval != PAPI_OK )
        fprintf( stderr, "PAPI_create_eventset failed\n" );
    
    retval = PAPI_add_events( EventSet, events, eventCount );
    if( retval != PAPI_OK )
        fprintf( stderr, "PAPI_add_events failed\n" );
      
    printf("eventCount = %d\n", eventCount);   
#endif 
   
 
    TESTING_CHECK( magma_zparse_opts( argc, argv, &zopts, &i, queue ));
    B.blocksize = zopts.blocksize;
    B.alignment = zopts.alignment;

    TESTING_CHECK( magma_zsolverinfo_init( &zopts.solver_par, &zopts.precond_par, queue ));

    while( i < argc ) {
        if ( strcmp("LAPLACE2D", argv[i]) == 0 && i+1 < argc ) {   // Laplace test
            i++;
            magma_int_t laplace_size = atoi( argv[i] );
            TESTING_CHECK( magma_zm_5stencil(  laplace_size, &A, queue ));
        } else {                        // file-matrix test
            TESTING_CHECK( magma_z_csr_mtx( &A,  argv[i], queue ));
        }
        
         
        TESTING_CHECK( magma_index_malloc( &sizes_d, A.num_rows ) );
        TESTING_CHECK( magma_index_malloc_cpu( &sizes_h, A.num_rows+1 ) );
        TESTING_CHECK( magma_index_malloc( &locations_d, A.num_rows*warpsize ) );
        TESTING_CHECK( magma_zmalloc( &trisystems_d, min(320000,A.num_rows) *warpsize*warpsize ) ); // fixed size - go recursive
        TESTING_CHECK( magma_zmalloc( &rhs_d, A.num_rows*warpsize ) );
        
        for( magma_int_t j=0; j<A.num_rows; j++ ){
                maxsize = sizes_h[j] = 0;
        }
        // selects GPU kernels
        //zopts.solver_par.version = 1;
        
        TESTING_CHECK( magma_zmconvert( A, &B, Magma_CSR, zopts.output_format, queue ));
        
        printf( "\n%% matrix info: %d-by-%d with %d nonzeros\n\n",
                            int(A.num_rows), int(A.num_cols), int(A.nnz) );
        
        printf("matrixinfo = [ \n");
        printf("%%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz\n");
        printf("%%======================================================================"
                            "======%%\n");
        printf("  %8d  %8d      %10d             %4d        %10d\n",
            int(B.num_rows), int(B.num_cols), int(B.true_nnz), int(B.true_nnz/B.num_rows), int(B.nnz) );
        printf("%%======================================================================"
        "======%%\n");
        printf("];\n");

        TESTING_CHECK( magma_zmtransfer( B, &B_d, Magma_CPU, Magma_DEV, queue ));
        
        // vectors and initial guess
        TESTING_CHECK( magma_zvinit( &b, Magma_DEV, A.num_rows, 1, one, queue ));
        //magma_zvinit( &x, Magma_DEV, A.num_cols, 1, one, queue );
        //magma_z_spmv( one, B_d, x, zero, b, queue );                 //  b = A x
        //magma_zmfree(&x, queue );
        TESTING_CHECK( magma_zvinit( &x, Magma_DEV, A.num_cols, 1, zero, queue ));
        
        
        TESTING_CHECK( magma_imalloc_cpu( &power, zopts.precond_par.maxiter ) );
        
        
        
    // here the ISAI code
            // ILU setup
    TESTING_CHECK( magma_zcumilusetup( B_d, &zopts.precond_par, queue ) );
    
    // we need this in any case
    TESTING_CHECK( magma_zmtranspose( zopts.precond_par.L, &LT, queue ) );
    
    
    
    // SPAI for L 
    if( zopts.precond_par.pattern <= 0 ){ // block diagonal structure
        if( zopts.precond_par.pattern == 0 ){
            zopts.precond_par.pattern = -1;    
        }
        // magma_zmisai_blockstruct_gpu( A.num_rows, -precond->pattern, offset, MagmaLower, &QT, queue );
        // magma_z_mvisu(QT, queue );
        // printf("done here\n");
        magma_zmisai_blockstruct( A.num_rows, -zopts.precond_par.pattern, offset, MagmaLower, &MT, queue );
        TESTING_CHECK( magma_z_mtransfer( MT, &QT, Magma_CPU, Magma_DEV, queue ) );
        magma_zmfree( &MT, queue );
        TESTING_CHECK( magma_zmtranspose( QT, &MT, queue ) );
        magma_zmfree( &QT, queue );
    } else {
        if( zopts.precond_par.pattern == 100 ){
            TESTING_CHECK( magma_zgeisai_maxblock( LT, &MT, queue ) );
        } else {
            // pattern L^x
            TESTING_CHECK( magma_z_mtransfer( LT, &MT, Magma_DEV, Magma_DEV, queue ) );
            if( zopts.precond_par.pattern > 0 ){
                z = 1;
                while( z<zopts.precond_par.pattern ){
                    TESTING_CHECK( magma_z_spmm( MAGMA_Z_ONE, LT, MT, &QT, queue ) );
                    magma_zmfree( &MT, queue );
                    TESTING_CHECK( magma_z_mtransfer( QT, &MT, Magma_DEV, Magma_DEV, queue ) );
                    magma_zmfree( &QT, queue );
                    z++;
                }
            }
        }
    }
    magma_index_getvector( A.num_rows+1, MT.drow, 1, sizes_h, 1, queue );
    maxsize = 0;
    for( magma_int_t j=0; j<A.num_rows; j++ ){
        nnzloc = sizes_h[j+1]-sizes_h[j];
        nnzL+= nnzloc;
        if( nnzloc > maxsize ){
            maxsize = sizes_h[j+1]-sizes_h[j];
        }
        if( maxsize > warpsize ){
            printf("%%   error for ISAI: size of system %d is too large by %d\n", j, maxsize-32); 
            break;
        }
    }
    printf("%% nnz in L-ISAI: %d\n", nnzL); 
    // this can be modified to the thread-block-size
    if( maxsize > warpsize ){
       info = -(maxsize - warpsize);     
       goto cleanup;
    }
    
    // do twice - first time for warmup
    for(int warmup = 0; warmup < 2; warmup++ ){
        eL = 0.0;
    
    //--------------START TIME---------------
    // chronometry
    tempo1 = magma_sync_wtime( queue );

#ifdef MEASURE_EACH    
    for( int iter=0; iter<zopts.precond_par.maxiter; iter++){
#endif

#ifdef PAPI
        retval = PAPI_start( EventSet );
        if( retval != PAPI_OK )
            fprintf( stderr, "PAPI_start failed\n" );
#endif

#ifdef MEASURE_GROUP
    for( int iter=0; iter<zopts.precond_par.maxiter; iter++){
#endif      
        if( zopts.solver_par.version == 1 ){//printf("%% fallback version.\n");
            if( maxsize <= 8 ){
                TESTING_CHECK( magma_zisaigenerator_8_gpu( MagmaLower, MagmaNoTrans, MagmaNonUnit, 
                            LT, &MT, sizes_d, locations_d, trisystems_d, rhs_d, queue ) );
            } else if( maxsize <= 16 ){
                TESTING_CHECK( magma_zisaigenerator_16_gpu( MagmaLower, MagmaNoTrans, MagmaNonUnit, 
                            LT, &MT, sizes_d, locations_d, trisystems_d, rhs_d, queue ) );
            } else {
                TESTING_CHECK( magma_zisaigenerator_32_gpu( MagmaLower, MagmaNoTrans, MagmaNonUnit, 
                            LT, &MT, sizes_d, locations_d, trisystems_d, rhs_d, queue ) );
            }
        } else {
            TESTING_CHECK( magma_zisai_generator_regs( MagmaLower, MagmaNoTrans, MagmaNonUnit, 
                            zopts.precond_par.L, &MT, sizes_d, locations_d, trisystems_d, rhs_d, queue ) );
        }
#ifdef MEASURE_GROUP        
    }  
#endif 

#ifdef PAPI
        retval = PAPI_stop( EventSet, values );
        if( retval != PAPI_OK )
                fprintf( stderr, "PAPI_stop failed\n" );

#if defined ARCH_WESTMERE || defined ARCH_GAINESTOWN 
        L_L1_dcm += values[0];
        L_L1_icm += values[1];
        L_fp_ops += values[2];
        L_fp_ins += values[3];
        L_tot_ins += values[4];

#elif defined ARCH_HASWELL        
        L2_dcm += values[0];
        L2_icm += values[1];
        L2_dca += values[2];
        L2_ica += values[3];

#elif defined ARCH_KNL
        L1_dcm += values[0];
        L1_icm += values[1];
        L_tot_ins += values[2];
        
#elif defined ARCH_KNL_POWER      
        for ( ii = 0; ii < eventCount; ii++ ){
          L_energy[ii] += values[ii]; 
        }
        
#elif defined ARCH_K20M || defined ARCH_K40C 
        for ( ii = 0; ii < eventCount; ii++ ){
             power[i] = values[ii];
             eL = eL + (real_Double_t) power[i];
          printf("\tevent[%d] = %lld\n", ii, values[ii]);
        }      
        
#endif

        for ( ii = 0; ii < eventCount; ii++ ){
          printf("%s = %lld, ", EventName[ii], values[ii]);
        }
        printf("\n");

#endif

#ifdef MEASURE_EACH
    }
#endif

    //--------------STOP TIME----------------
    tempo2 = magma_sync_wtime( queue );
    runtL = (real_Double_t) tempo2-tempo1;
    eL = eL * runtL/1000;
#ifdef ARCH_HASWELL     
    L_L2_dcmr = (real_Double_t) L2_dcm / (real_Double_t) (L2_dca+L2_ica);
    L_L2_icmr = (real_Double_t) L2_icm / (real_Double_t) (L2_dca+L2_ica);
    
    L2_dcm=0;
    L2_icm=0;
    L2_dca=0;
    L2_ica=0;
#elif defined ARCH_WESTMERE || defined ARCH_GAINESTOWN 
 

#elif defined ARCH_KNL
    L_L1_dcm=L1_dcm;
    L_L1_icm=L1_icm;
    L1_dcm=0;
    L1_icm=0;
#elif defined ARCH_KNL_POWER     

    
#endif    
    printf("%% ISAI generation L:%.4e\n", runtL  );
    
    }// end warmup

    TESTING_CHECK( magma_zmtranspose( MT, &zopts.precond_par.LD, queue ) );
    magma_zmfree( &LT, queue );
    magma_zmfree( &MT, queue );
    // magma_z_mvisu(zopts.precond_par.LD, queue);
    
    // free a few things to save memory
    magma_zmfree( &zopts.precond_par.LD, queue );
    magma_zmfree( &zopts.precond_par.L, queue );
    magma_zmfree(&B_d, queue );
    
    
    
    //#####################################
    
    
   
   // we need this in any case
   TESTING_CHECK( magma_zmtranspose( zopts.precond_par.U, &LT, queue ) );
   
    
    // SPAI for U
    if( zopts.precond_par.pattern <= 0 ){ // block diagonal structure
        
        if( zopts.precond_par.pattern == 0 ){
            zopts.precond_par.pattern = -1;    
        }
        magma_zmisai_blockstruct( A.num_rows, -zopts.precond_par.pattern, offset, MagmaUpper, &MT, queue );
        TESTING_CHECK( magma_z_mtransfer( MT, &QT, Magma_CPU, Magma_DEV, queue ) );
        magma_zmfree( &MT, queue );
        TESTING_CHECK( magma_zmtranspose( QT, &MT, queue ) );
        magma_zmfree( &QT, queue );
        
    } else {
        if( zopts.precond_par.pattern == 100 ){
            TESTING_CHECK( magma_zgeisai_maxblock( LT, &MT, queue ) );
        } else {
        // pattern U^x
            TESTING_CHECK( magma_z_mtransfer( LT, &MT, Magma_DEV, Magma_DEV, queue ) );
            if( zopts.precond_par.pattern > 0 ){
                z = 1;
                while( z<zopts.precond_par.pattern ){
                    TESTING_CHECK( magma_z_spmm( MAGMA_Z_ONE, LT, MT, &QT, queue ) );
                    magma_zmfree( &MT, queue );
                    TESTING_CHECK( magma_z_mtransfer( QT, &MT, Magma_DEV, Magma_DEV, queue ) );
                    magma_zmfree( &QT, queue );
                    z++;
                }
            }
        }
    }
    magma_index_getvector( A.num_rows+1, MT.drow, 1, sizes_h, 1, queue );
    maxsize = 0;
    for( magma_int_t j=0; j<A.num_rows; j++ ){
        nnzloc = sizes_h[j+1]-sizes_h[j];
        nnzU+= nnzloc;
        if( nnzloc > maxsize ){
            maxsize = sizes_h[j+1]-sizes_h[j];
        }
        if( maxsize > warpsize ){
            printf("%%   error for ISAI: size of system %d is too large by %d\n", j, maxsize-32); 
            break;
        }
    }
    printf("%% nnz in U-ISAI: %d\n", nnzU); 
    // this can be modified to the thread-block-size
    if( maxsize > warpsize ){
       info = -(maxsize - warpsize);     
       goto cleanup;
    }
    
    // do twice - first time for warmup
    for(int warmup = 0; warmup < 2; warmup++ ){
        eU = 0.0;
        
    //--------------START TIME---------------
    // chronometry
    tempo1 = magma_sync_wtime( queue );
#ifdef MEASURE_EACH  
    for( int iter=0; iter<zopts.precond_par.maxiter; iter++){
#endif
  
#ifdef PAPI
        retval = PAPI_start( EventSet );
        if( retval != PAPI_OK )
            fprintf( stderr, "PAPI_start failed\n" );
#endif

#ifdef MEASURE_GROUP  
    for( int iter=0; iter<zopts.precond_par.maxiter; iter++){
#endif

        if( zopts.solver_par.version == 1 ){// printf("%% fallback version.\n");
            if( maxsize <= 8 ){
                TESTING_CHECK( magma_zisaigenerator_8_gpu( MagmaUpper, MagmaNoTrans, MagmaNonUnit, 
                            LT, &MT, sizes_d, locations_d, trisystems_d, rhs_d, queue ) );
            } else if( maxsize <= 16 ){
                TESTING_CHECK( magma_zisaigenerator_16_gpu( MagmaUpper, MagmaNoTrans, MagmaNonUnit, 
                            LT, &MT, sizes_d, locations_d, trisystems_d, rhs_d, queue ) );
            } else {
                TESTING_CHECK( magma_zisaigenerator_32_gpu( MagmaUpper, MagmaNoTrans, MagmaNonUnit, 
                            LT, &MT, sizes_d, locations_d, trisystems_d, rhs_d, queue ) );
            }
        } else {
        
            TESTING_CHECK( magma_zisai_generator_regs( MagmaUpper, MagmaNoTrans, MagmaNonUnit, 
                            zopts.precond_par.U, &MT, sizes_d, locations_d, trisystems_d, rhs_d, queue ) );
        }
        
#ifdef MEASURE_GROUP
    }
#endif        
        
#ifdef PAPI
        retval = PAPI_stop( EventSet, values );
        if( retval != PAPI_OK )
                fprintf( stderr, "PAPI_stop failed\n" );
              
#if defined ARCH_WESTMERE || defined ARCH_GAINESTOWN 
        U_L1_dcm += values[0];
        U_L1_icm += values[1];
        U_fp_ops += values[2];
        U_fp_ins += values[3];
        U_tot_ins += values[4];


#elif defined ARCH_HASWELL        
        L2_dcm += values[0];
        L2_icm += values[1];
        L2_dca += values[2];
        L2_ica += values[3];

#elif defined ARCH_KNL
        L1_dcm += values[0];
        L1_icm += values[1];
        L_tot_ins += values[2];

#elif defined ARCH_KNL_POWER    
        for ( ii = 0; ii < eventCount; ii++ ){
          U_energy[ii] += values[ii]; 
        }
        
#elif defined ARCH_K20M || defined ARCH_K40C 
        for( ii = 0; ii < eventCount; ii++ ){
             power[i] = values[ii];
             eL = eL + (real_Double_t) power[i];
          printf("\tevent[%d] = %lld\n", ii, values[ii]);
        }      
        
#endif
        for ( ii = 0; ii < eventCount; ii++ ){
          printf("%s = %lld, ", EventName[ii], values[ii]);
        }
        printf("\n");        
        
#endif

#ifdef MEASURE_EACH
    }
#endif

    //--------------STOP TIME----------------
    tempo2 = magma_sync_wtime( queue );
    runtU = (real_Double_t) tempo2-tempo1;
    printf("%% ISAI generation U:%.4e\n", runtU  );
    eU = eU * runtU/1000;
#ifdef ARCH_HASWELL    
    U_L2_dcmr = (real_Double_t) L2_dcm / (real_Double_t) (L2_dca+L2_ica);
    U_L2_icmr = (real_Double_t) L2_icm / (real_Double_t) (L2_dca+L2_ica);
    
    L2_dcm=0;
    L2_icm=0;
    L2_dca=0;
    L2_ica=0;
#elif defined ARCH_WESTMERE || defined ARCH_GAINESTOWN 
    

#elif defined ARCH_KNL
    U_L1_dcm=L1_dcm;
    U_L1_icm=L1_icm;
    L1_dcm=0;
    L1_icm=0;

#elif defined ARCH_KNL_POWER         

#endif    
    TESTING_CHECK( magma_zmtranspose( MT, &zopts.precond_par.UD, queue ) );
    // magma_z_mvisu(zopts.precond_par.UD, queue);

    } //end warmup
    //end ISAI code
        
    int num_threads = 0;
    #pragma omp parallel 
    {
      num_threads = omp_get_num_threads();
    }
        
        
        printf("isai_runtime = [\n");
        printf("%%   L\t\t  U\t\t  num_threads\n");        
        printf("  %.4e  %.4e  %d\n",
           runtL, runtU, num_threads );
        printf("];\n\n");
        fflush(stdout);
#if defined ARCH_WESTMERE || defined ARCH_GAINESTOWN 
        printf("isai_L1_Data_Cache_misses = [\n");
        printf("%%   L\t\t  U\n");        
        printf("  %lld  %lld\n",
           L_L1_dcm, U_L1_dcm );
        printf("];\n\n");
        printf("isai_L1_Instuction_Cache_misses = [\n");
        printf("%%   L\t\t  U\n");        
        printf("  %lld  %lld\n",
           L_L1_icm, U_L1_icm );
        printf("];\n\n");
        printf("isai_FP_OPS = [\n");
        printf("%%   L\t\t  U\n");        
        printf("  %lld  %lld\n",
           L_fp_ops, U_fp_ops );
        printf("];\n\n");
        printf("isai_FP_INS = [\n");
        printf("%%   L\t\t  U\n");        
        printf("  %lld  %lld\n",
           L_fp_ins, U_fp_ins );
        printf("];\n\n");
        printf("isai_TOT_INS = [\n");
        printf("%%   L\t\t  U\n");        
        printf("  %lld  %lld\n",
           L_tot_ins, U_tot_ins );
        printf("];\n\n");
         
#elif defined ARCH_HASWELL        
        printf("isai_L2_Data_Cache_miss_rate = [\n");
        printf("%%   L\t\t  U\n");        
        printf("  %.4e  %.4e\n",
           L_L2_dcmr, U_L2_dcmr );
        printf("];\n\n");
        printf("isai_L2_Instuction_Cache_miss_rate = [\n");
        printf("%%   L\t\t  U\n");        
        printf("  %.4e  %.4e\n",
           L_L2_icmr, U_L2_icmr );
        printf("];\n\n");
//#elif defined ARCH_GAINESTOWN   

#elif defined ARCH_KNL
        printf("isai_L1_Data_Cache_misses = [\n");
        printf("%%   L\t\t  U\n");        
        printf("  %lld  %lld\n",
           L_L1_dcm, U_L1_dcm );
        printf("];\n\n");
        printf("isai_L1_Instuction_Cache_misses = [\n");
        printf("%%   L\t\t  U\n");        
        printf("  %lld  %lld\n",
           L_L1_icm, U_L1_icm );
        printf("];\n\n");
#elif defined ARCH_KNL_POWER             
        printf("isai_energy = [\n");
        printf("%%   L\t\t  U\n");   
        for ( ii = 0; ii < eventCount; ii++ ){
          printf("  %lld  %lld\n",
             L_energy[ii], U_energy[ii] );
        }
        printf("];\n\n");
#elif defined ARCH_K20M || defined ARCH_K40C
        printf("isai_energy = [\n");
        printf("%%   L\t\t  U\n");        
        printf("  %.4e  %.4e\n",
           eL, eU );
        printf("];\n\n");
#endif        
        fflush(stdout);
        magma_zmfree(&B_d, queue );
        magma_zmfree(&B, queue );
        magma_zmfree(&A, queue );
        magma_zmfree(&x, queue );
        magma_zmfree(&b, queue );
        i++;
    }

cleanup:
    magma_zmfree(&B_d, queue );
    magma_zmfree(&B, queue );
    magma_zmfree(&A, queue );
    magma_zmfree(&x, queue );
    magma_zmfree(&b, queue );
    magma_zsolverinfo_free( &zopts.solver_par, &zopts.precond_par, queue );
    // ISAI allocations
    magma_free( sizes_d );
    magma_free_cpu( sizes_h );
    magma_free_cpu( power );
    magma_free( locations_d );
    magma_free( trisystems_d );
    magma_free( rhs_d );
    magma_zmfree( &LT, queue );
    magma_zmfree( &MT, queue );
    magma_zmfree( &QT, queue );
    
    magma_queue_destroy( queue );
    TESTING_CHECK( magma_finalize() );
    return info;
}
