% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_206 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	819204	813424	141586	141586	9.49e-03	4.05e-02	7.97e-03	1.84e-02	4.74e-04	0.00e+00	2.65e-03	5.46e-04	4.04e-04	1.13e-02	1.99e-03	9.38e-02		9.38e-02
1	809043	820808	129014	134794	9.51e-03	3.63e-02	3.80e-03	5.00e-03	7.63e-04	0.00e+00	3.26e-03	4.73e-04	4.18e-04	7.56e-03	2.28e-03	6.94e-02		1.63e-01
2	814893	815207	139981	128216	9.57e-03	5.44e-02	4.04e-03	4.10e-03	3.19e-04	0.00e+00	3.70e-03	5.74e-04	4.48e-04	4.41e-03	2.27e-03	8.39e-02		2.47e-01
3	801709	807179	134937	134623	9.54e-03	5.60e-02	4.74e-03	3.58e-03	3.40e-04	0.00e+00	4.11e-03	7.29e-04	4.66e-04	4.40e-03	2.18e-03	8.61e-02		3.33e-01
4	806193	803308	148927	143457	9.44e-03	5.61e-02	4.22e-03	2.30e-03	4.46e-04	0.00e+00	4.27e-03	6.13e-04	4.99e-04	5.56e-03	2.09e-03	8.55e-02		4.19e-01
5	811846	807702	145248	148133	9.41e-03	5.10e-02	4.18e-03	2.56e-03	1.02e-03	0.00e+00	4.36e-03	7.08e-04	4.50e-04	4.81e-03	2.11e-03	8.06e-02		4.99e-01
6	811309	808806	140401	144545	9.51e-03	5.61e-02	4.27e-03	2.85e-03	5.12e-04	0.00e+00	4.33e-03	6.87e-04	4.73e-04	4.86e-03	2.15e-03	8.57e-02		5.85e-01
7	809793	811376	141744	144247	9.49e-03	5.27e-02	4.32e-03	2.58e-03	4.51e-04	0.00e+00	4.17e-03	7.18e-04	4.45e-04	4.36e-03	2.17e-03	8.14e-02		6.66e-01
8	811647	813324	144066	142483	9.50e-03	5.65e-02	4.30e-03	3.57e-03	1.21e-03	0.00e+00	4.37e-03	7.61e-04	4.50e-04	4.45e-03	2.18e-03	8.73e-02		7.54e-01
9	815136	812960	143018	141341	9.56e-03	5.51e-02	4.36e-03	2.64e-03	1.97e-03	0.00e+00	4.33e-03	6.58e-04	4.45e-04	4.37e-03	2.20e-03	8.57e-02		8.39e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.5848 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.5848 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.584818  0.000000
];

