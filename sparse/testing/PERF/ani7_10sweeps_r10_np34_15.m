% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_34 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	809241	806666	141586	141586	3.32e-03	3.74e-02	6.98e-03	6.79e-03	6.78e-04	0.00e+00	7.00e-03	3.41e-04	7.67e-04	5.87e-03	4.06e-03	7.32e-02		7.32e-02
1	812273	802750	138977	141552	3.30e-03	4.43e-02	6.11e-03	2.69e-03	8.09e-04	0.00e+00	8.79e-03	2.17e-04	7.97e-04	4.79e-03	5.26e-03	7.71e-02		1.50e-01
2	811533	809817	136751	146274	3.35e-03	6.94e-02	7.99e-03	2.17e-03	6.45e-04	0.00e+00	1.05e-02	3.47e-04	8.92e-04	4.18e-03	5.78e-03	1.05e-01		2.56e-01
3	815345	801238	138297	140013	3.36e-03	1.04e-01	9.08e-03	2.41e-03	8.91e-04	0.00e+00	1.23e-02	2.77e-04	1.04e-03	4.15e-03	5.45e-03	1.42e-01		3.98e-01
4	805039	813859	135291	149398	3.33e-03	9.78e-02	9.19e-03	2.54e-03	1.34e-03	0.00e+00	1.31e-02	2.32e-04	1.11e-03	4.66e-03	6.19e-03	1.40e-01		5.38e-01
5	810674	813073	146402	137582	4.94e-03	9.56e-02	1.23e-02	3.28e-03	1.65e-03	0.00e+00	1.31e-02	3.65e-04	1.06e-03	4.49e-03	6.35e-03	1.43e-01		6.81e-01
6	812682	810718	141573	139174	4.99e-03	9.72e-02	1.21e-02	2.52e-03	1.19e-03	0.00e+00	1.29e-02	2.90e-04	1.04e-03	4.33e-03	5.42e-03	1.42e-01		8.23e-01
7	807620	813761	140371	142335	3.36e-03	9.74e-02	9.48e-03	2.28e-03	1.17e-03	0.00e+00	1.32e-02	3.18e-04	1.05e-03	4.22e-03	5.46e-03	1.38e-01		9.61e-01
8	811348	811228	146239	140098	3.38e-03	1.00e-01	9.40e-03	2.12e-03	1.51e-03	0.00e+00	1.30e-02	2.98e-04	1.05e-03	4.24e-03	5.38e-03	1.41e-01		1.10e+00
9	814894	813127	143317	143437	3.40e-03	1.00e-01	9.49e-03	2.34e-03	1.68e-03	0.00e+00	1.33e-02	2.88e-04	1.07e-03	4.16e-03	5.50e-03	1.41e-01		1.24e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.6214 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.6214 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.621398  0.000000
];

