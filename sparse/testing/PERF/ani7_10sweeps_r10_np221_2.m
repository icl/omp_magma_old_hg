% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_221 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	824029	804384	141586	141586	1.02e-02	4.17e-02	9.05e-03	1.92e-02	7.68e-04	0.00e+00	2.80e-03	4.77e-04	4.02e-04	1.19e-02	1.77e-03	9.83e-02		9.83e-02
1	800017	803256	124189	143834	1.11e-02	3.52e-02	3.54e-03	4.23e-03	7.95e-04	0.00e+00	3.55e-03	4.14e-04	3.85e-04	6.50e-03	2.04e-03	6.77e-02		1.66e-01
2	809013	806745	149007	145768	1.11e-02	5.16e-02	3.98e-03	2.95e-03	2.91e-04	0.00e+00	3.68e-03	5.35e-04	4.25e-04	4.34e-03	2.25e-03	8.12e-02		2.47e-01
3	809305	797544	140817	143085	1.11e-02	5.29e-02	4.58e-03	2.26e-03	4.47e-04	0.00e+00	3.96e-03	5.87e-04	4.59e-04	4.23e-03	2.23e-03	8.28e-02		3.30e-01
4	796315	808741	141331	153092	1.10e-02	5.53e-02	4.58e-03	3.98e-03	5.13e-04	0.00e+00	4.09e-03	7.15e-04	4.46e-04	5.19e-03	2.26e-03	8.80e-02		4.18e-01
5	814442	811563	155126	142700	1.11e-02	7.79e-02	4.34e-03	5.72e-03	1.34e-03	0.00e+00	3.97e-03	6.07e-04	4.32e-04	4.80e-03	2.09e-03	1.12e-01		5.30e-01
6	810546	811625	137805	140684	1.01e-02	5.61e-02	4.63e-03	3.93e-03	4.04e-04	0.00e+00	4.03e-03	5.74e-04	4.20e-04	4.95e-03	2.04e-03	8.72e-02		6.18e-01
7	810360	809740	142507	141428	1.03e-02	5.06e-02	4.53e-03	2.21e-03	3.87e-04	0.00e+00	3.99e-03	6.00e-04	4.27e-04	4.34e-03	2.05e-03	7.94e-02		6.97e-01
8	814276	812926	143499	144119	1.01e-02	5.53e-02	4.51e-03	2.76e-03	1.56e-03	0.00e+00	3.95e-03	7.37e-04	4.22e-04	4.56e-03	2.08e-03	8.60e-02		7.83e-01
9	814051	815106	140389	141739	1.02e-02	5.14e-02	4.56e-03	2.62e-03	1.65e-03	0.00e+00	4.16e-03	6.93e-04	4.52e-04	4.28e-03	2.08e-03	8.20e-02		8.65e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.5996 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.5996 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.599612  0.000000
];

