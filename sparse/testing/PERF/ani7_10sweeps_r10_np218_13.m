% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_218 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	818541	803741	141586	141586	9.96e-03	4.12e-02	9.06e-03	1.88e-02	2.01e-03	0.00e+00	2.48e-03	4.81e-04	3.72e-04	1.13e-02	1.82e-03	9.74e-02		9.74e-02
1	813190	794641	129677	144477	9.87e-03	3.58e-02	3.59e-03	4.33e-03	1.87e-03	0.00e+00	3.09e-03	4.05e-04	3.81e-04	6.56e-03	2.02e-03	6.79e-02		1.65e-01
2	811321	811985	135834	154383	9.75e-03	5.27e-02	3.78e-03	2.84e-03	1.34e-03	0.00e+00	3.48e-03	7.49e-04	3.89e-04	4.32e-03	2.09e-03	8.14e-02		2.47e-01
3	813084	816209	138509	137845	9.93e-03	5.36e-02	4.45e-03	3.30e-03	3.87e-04	0.00e+00	3.75e-03	5.03e-04	4.40e-04	4.26e-03	2.12e-03	8.28e-02		3.30e-01
4	809583	816008	137552	134427	9.99e-03	5.83e-02	4.41e-03	1.70e-03	4.87e-04	0.00e+00	3.98e-03	8.09e-04	4.51e-04	4.26e-03	2.09e-03	8.65e-02		4.16e-01
5	809006	815482	141858	135433	1.00e-02	5.08e-02	4.63e-03	4.11e-03	4.51e-04	0.00e+00	4.14e-03	9.30e-04	4.58e-04	4.71e-03	2.09e-03	8.23e-02		4.98e-01
6	804380	813582	143241	136765	9.98e-03	5.59e-02	4.57e-03	2.42e-03	3.87e-04	0.00e+00	3.90e-03	1.07e-03	4.58e-04	4.36e-03	2.06e-03	8.52e-02		5.83e-01
7	811214	812644	148673	139471	1.01e-02	5.01e-02	4.42e-03	3.23e-03	3.95e-04	0.00e+00	3.98e-03	8.55e-04	4.61e-04	4.46e-03	2.09e-03	8.01e-02		6.64e-01
8	812499	812924	142645	141215	9.95e-03	5.59e-02	4.62e-03	2.35e-03	1.21e-03	0.00e+00	4.03e-03	5.62e-04	4.31e-04	4.45e-03	2.12e-03	8.56e-02		7.49e-01
9	813765	813413	142166	141741	9.96e-03	5.09e-02	4.59e-03	3.89e-03	1.53e-03	0.00e+00	4.23e-03	7.25e-04	4.34e-04	4.30e-03	2.12e-03	8.27e-02		8.32e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.5790 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.5790 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.579014  0.000000
];

