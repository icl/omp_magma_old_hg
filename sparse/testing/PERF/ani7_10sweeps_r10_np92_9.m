% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_92 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	825299	822849	141586	141586	5.43e-03	2.99e-02	6.23e-03	9.57e-03	1.75e-03	0.00e+00	2.93e-03	3.61e-04	4.48e-04	7.09e-03	2.47e-03	6.62e-02		6.62e-02
1	820717	785643	122919	125369	5.57e-03	3.02e-02	4.38e-03	3.04e-03	1.57e-03	0.00e+00	3.49e-03	2.59e-04	5.24e-04	4.42e-03	2.53e-03	5.60e-02		1.22e-01
2	791616	810229	128307	163381	5.26e-03	4.65e-02	4.64e-03	1.91e-03	3.52e-04	0.00e+00	4.13e-03	3.31e-04	4.73e-04	3.36e-03	2.72e-03	6.97e-02		1.92e-01
3	815626	806766	158214	139601	5.45e-03	4.97e-02	5.02e-03	1.85e-03	4.36e-04	0.00e+00	4.63e-03	3.12e-04	5.28e-04	3.21e-03	2.92e-03	7.41e-02		2.66e-01
4	807896	810672	135010	143870	5.43e-03	5.26e-02	5.59e-03	1.89e-03	5.37e-04	0.00e+00	4.92e-03	3.67e-04	5.45e-04	3.35e-03	2.86e-03	7.81e-02		3.44e-01
5	809918	809444	143545	140769	5.44e-03	4.82e-02	5.75e-03	2.01e-03	5.32e-04	0.00e+00	4.98e-03	3.85e-04	5.37e-04	3.60e-03	2.91e-03	7.44e-02		4.18e-01
6	804842	811688	142329	142803	5.46e-03	5.18e-02	5.75e-03	2.42e-03	5.24e-04	0.00e+00	4.98e-03	4.84e-04	5.19e-04	3.47e-03	2.89e-03	7.83e-02		4.97e-01
7	813157	810056	148211	141365	5.45e-03	4.93e-02	5.76e-03	2.80e-03	5.46e-04	0.00e+00	4.90e-03	3.72e-04	5.26e-04	3.15e-03	2.93e-03	7.58e-02		5.73e-01
8	811695	815829	140702	143803	5.42e-03	5.27e-02	5.92e-03	1.63e-03	5.01e-04	0.00e+00	5.00e-03	3.72e-04	5.26e-04	3.51e-03	2.93e-03	7.85e-02		6.51e-01
9	812870	814351	142970	138836	5.49e-03	5.16e-02	5.98e-03	1.92e-03	1.05e-03	0.00e+00	4.95e-03	5.30e-04	5.01e-04	3.25e-03	2.96e-03	7.82e-02		7.29e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.1988 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.1988 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.198794  0.000000
];

