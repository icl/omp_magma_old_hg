% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_78 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	810041	804309	141586	141586	5.15e-03	3.04e-02	6.82e-03	9.22e-03	1.36e-03	0.00e+00	3.36e-03	3.18e-04	5.62e-04	6.35e-03	2.64e-03	6.62e-02		6.62e-02
1	808458	789905	138177	143909	5.13e-03	2.83e-02	4.82e-03	3.92e-03	1.08e-03	0.00e+00	3.82e-03	2.84e-04	5.38e-04	4.06e-03	2.66e-03	5.46e-02		1.21e-01
2	799402	802334	140566	159119	5.02e-03	4.85e-02	6.10e-03	2.50e-03	6.95e-04	0.00e+00	4.68e-03	3.22e-04	4.92e-04	3.49e-03	2.75e-03	7.46e-02		1.95e-01
3	816077	813548	150428	147496	5.14e-03	5.45e-02	5.54e-03	2.35e-03	4.51e-04	0.00e+00	5.30e-03	5.85e-04	5.77e-04	3.47e-03	2.98e-03	8.09e-02		2.76e-01
4	802695	810480	134559	137088	5.19e-03	5.73e-02	6.12e-03	1.85e-03	5.87e-04	0.00e+00	5.88e-03	3.87e-04	6.13e-04	3.37e-03	2.97e-03	8.43e-02		3.60e-01
5	807281	809164	148746	140961	5.17e-03	4.96e-02	6.24e-03	1.99e-03	6.10e-04	0.00e+00	5.87e-03	4.05e-04	5.89e-04	3.70e-03	2.99e-03	7.72e-02		4.38e-01
6	809740	810161	144966	143083	5.15e-03	5.29e-02	6.25e-03	2.18e-03	6.94e-04	0.00e+00	5.85e-03	5.49e-04	5.83e-04	3.50e-03	2.98e-03	8.06e-02		5.18e-01
7	814514	811606	143313	142892	5.16e-03	5.16e-02	6.31e-03	2.27e-03	6.33e-04	0.00e+00	5.83e-03	4.90e-04	5.96e-04	3.43e-03	3.01e-03	7.94e-02		5.98e-01
8	812197	816521	139345	142253	5.16e-03	5.48e-02	6.42e-03	2.07e-03	5.64e-04	0.00e+00	5.73e-03	3.88e-04	6.10e-04	3.47e-03	3.01e-03	8.22e-02		6.80e-01
9	815933	812777	142468	138144	5.20e-03	5.34e-02	6.48e-03	2.12e-03	1.17e-03	0.00e+00	5.76e-03	4.79e-04	5.97e-04	3.45e-03	2.99e-03	8.16e-02		7.61e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.2354 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.2354 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.235379  0.000000
];

