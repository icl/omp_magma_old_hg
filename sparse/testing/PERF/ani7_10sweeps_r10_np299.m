% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_299 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	816499	798632	141586	141586	1.57e-02	7.13e-02	1.78e-02	2.05e-02	2.41e-03	0.00e+00	3.04e-03	8.01e-04	1.59e-03	2.27e-02	2.42e-03	1.58e-01		1.58e-01
1	800409	804361	131719	149586	1.44e-02	4.80e-02	4.12e-03	5.19e-03	2.13e-03	0.00e+00	3.36e-03	6.66e-04	1.62e-03	1.27e-02	2.53e-03	9.47e-02		2.53e-01
2	806137	803197	148615	144663	1.51e-02	6.30e-02	4.41e-03	4.83e-03	1.54e-03	0.00e+00	3.40e-03	8.70e-04	1.66e-03	3.95e-03	2.72e-03	1.01e-01		3.54e-01
3	792993	813696	143693	146633	1.47e-02	5.61e-02	4.78e-03	5.91e-03	1.19e-03	0.00e+00	3.62e-03	8.63e-04	1.69e-03	3.72e-03	2.61e-03	9.52e-02		4.50e-01
4	807441	806102	157643	136940	1.46e-02	6.79e-02	4.72e-03	5.53e-03	1.21e-03	0.00e+00	3.92e-03	8.00e-04	1.65e-03	4.33e-03	2.63e-03	1.07e-01		5.57e-01
5	808408	813262	144000	145339	1.45e-02	6.05e-02	4.82e-03	5.18e-03	1.14e-03	0.00e+00	3.62e-03	7.58e-04	1.65e-03	4.39e-03	2.67e-03	9.92e-02		6.56e-01
6	813609	812233	143839	138985	1.46e-02	7.21e-02	4.97e-03	5.03e-03	1.21e-03	0.00e+00	3.61e-03	9.28e-04	1.72e-03	3.82e-03	2.61e-03	1.11e-01		7.67e-01
7	813657	811694	139444	140820	1.57e-02	5.90e-02	5.01e-03	5.19e-03	1.15e-03	0.00e+00	3.69e-03	7.95e-04	1.67e-03	3.85e-03	2.64e-03	9.87e-02		8.65e-01
8	810319	815998	140202	142165	1.51e-02	7.18e-02	4.97e-03	4.74e-03	2.32e-03	0.00e+00	3.76e-03	8.99e-04	1.62e-03	3.79e-03	2.69e-03	1.12e-01		9.77e-01
9	815882	816577	144346	138667	1.48e-02	5.92e-02	5.11e-03	4.69e-03	2.59e-03	0.00e+00	3.82e-03	8.49e-04	1.62e-03	3.69e-03	2.71e-03	9.90e-02		1.08e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.6083 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.6083 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.608319  0.000000
];

