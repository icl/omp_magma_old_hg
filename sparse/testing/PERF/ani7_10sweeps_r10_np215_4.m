% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_215 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	815731	802917	141586	141586	9.95e-03	4.13e-02	8.84e-03	1.88e-02	9.61e-04	0.00e+00	2.51e-03	5.34e-04	3.83e-04	1.20e-02	1.86e-03	9.72e-02		9.72e-02
1	805980	798390	132487	145301	9.83e-03	3.50e-02	3.56e-03	6.57e-03	9.91e-04	0.00e+00	3.10e-03	4.43e-04	3.58e-04	6.62e-03	2.06e-03	6.85e-02		1.66e-01
2	807283	823270	143044	150634	9.74e-03	5.32e-02	3.80e-03	2.09e-03	2.87e-04	0.00e+00	3.47e-03	4.38e-04	3.88e-04	4.27e-03	2.19e-03	7.99e-02		2.46e-01
3	805809	821180	142547	126560	1.01e-02	5.52e-02	4.67e-03	2.28e-03	5.70e-04	0.00e+00	3.90e-03	5.51e-04	4.31e-04	4.33e-03	2.14e-03	8.41e-02		3.30e-01
4	805025	807774	144827	129456	1.29e-02	5.80e-02	4.44e-03	2.23e-03	3.35e-04	0.00e+00	3.92e-03	5.31e-04	4.55e-04	5.56e-03	2.08e-03	9.05e-02		4.20e-01
5	812473	807106	146416	143667	9.84e-03	5.08e-02	4.56e-03	2.40e-03	1.36e-03	0.00e+00	4.10e-03	5.78e-04	4.61e-04	4.70e-03	2.13e-03	8.09e-02		5.01e-01
6	813011	810414	139774	145141	9.86e-03	5.62e-02	4.63e-03	2.00e-03	4.22e-04	0.00e+00	4.11e-03	7.53e-04	4.29e-04	4.93e-03	2.11e-03	8.55e-02		5.87e-01
7	812092	811913	140042	142639	9.93e-03	5.18e-02	4.71e-03	2.56e-03	5.53e-04	0.00e+00	4.23e-03	7.71e-04	4.34e-04	4.38e-03	2.12e-03	8.14e-02		6.68e-01
8	811715	812485	141767	141946	9.91e-03	5.63e-02	4.67e-03	2.00e-03	1.44e-03	0.00e+00	4.18e-03	6.37e-04	4.31e-04	4.39e-03	2.12e-03	8.60e-02		7.54e-01
9	813107	812845	142950	142180	9.95e-03	5.16e-02	4.58e-03	1.93e-03	1.20e-03	0.00e+00	4.13e-03	7.16e-04	4.64e-04	4.26e-03	2.14e-03	8.10e-02		8.35e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.5844 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.5844 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.584373  0.000000
];

