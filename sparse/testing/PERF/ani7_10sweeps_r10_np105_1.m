% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_105 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	813317	806083	141586	141586	5.69e-03	2.91e-02	5.91e-03	1.00e-02	1.74e-03	0.00e+00	2.55e-03	3.83e-04	4.05e-04	7.08e-03	2.10e-03	6.50e-02		6.50e-02
1	804634	807867	134901	142135	5.69e-03	2.80e-02	3.71e-03	4.70e-03	1.55e-03	0.00e+00	3.09e-03	2.81e-04	4.86e-04	4.56e-03	2.43e-03	5.45e-02		1.19e-01
2	811529	813298	144390	141157	5.67e-03	4.44e-02	4.31e-03	2.19e-03	3.53e-04	0.00e+00	3.59e-03	3.42e-04	4.27e-04	3.21e-03	2.53e-03	6.71e-02		1.87e-01
3	802025	791253	138301	136532	5.71e-03	4.90e-02	4.73e-03	1.67e-03	4.23e-04	0.00e+00	4.08e-03	4.57e-04	5.19e-04	3.06e-03	2.36e-03	7.20e-02		2.59e-01
4	807834	815689	148611	159383	5.57e-03	4.41e-02	4.74e-03	1.96e-03	5.20e-04	0.00e+00	4.41e-03	4.26e-04	4.43e-04	3.34e-03	2.52e-03	6.80e-02		3.27e-01
5	810318	809724	143607	135752	5.72e-03	4.54e-02	5.11e-03	2.32e-03	5.20e-04	0.00e+00	5.41e-03	5.49e-04	5.06e-04	3.62e-03	2.54e-03	7.17e-02		3.98e-01
6	808522	810027	141929	142523	6.47e-03	5.14e-02	5.21e-03	1.65e-03	4.67e-04	0.00e+00	5.68e-03	4.81e-04	5.50e-04	3.31e-03	2.72e-03	7.79e-02		4.76e-01
7	811689	812513	144531	143026	5.91e-03	4.56e-02	5.16e-03	1.84e-03	4.02e-04	0.00e+00	4.32e-03	4.36e-04	4.54e-04	3.13e-03	2.57e-03	6.99e-02		5.46e-01
8	811674	807896	142170	141346	5.69e-03	4.83e-02	5.26e-03	1.82e-03	4.76e-04	0.00e+00	4.39e-03	5.04e-04	4.71e-04	3.27e-03	2.53e-03	7.27e-02		6.19e-01
9	812969	812914	142991	146769	5.70e-03	4.52e-02	5.16e-03	1.72e-03	1.12e-03	0.00e+00	4.47e-03	4.15e-04	4.63e-04	3.24e-03	2.59e-03	7.01e-02		6.89e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.1739 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.1739 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.173898  0.000000
];

