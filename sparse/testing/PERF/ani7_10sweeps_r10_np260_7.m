% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_260 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	823386	804367	141586	141586	1.13e-02	4.53e-02	9.32e-03	2.08e-02	5.99e-04	0.00e+00	2.62e-03	6.57e-04	3.32e-04	1.48e-02	1.56e-03	1.07e-01		1.07e-01
1	810092	809698	124832	143851	1.13e-02	3.60e-02	3.11e-03	4.92e-03	4.53e-04	0.00e+00	2.74e-03	4.89e-04	3.51e-04	8.44e-03	1.83e-03	6.96e-02		1.77e-01
2	809325	801257	138932	139326	1.13e-02	5.09e-02	3.58e-03	3.48e-03	2.76e-04	0.00e+00	3.26e-03	5.30e-04	3.85e-04	4.27e-03	2.01e-03	8.00e-02		2.57e-01
3	817191	810275	140505	148573	1.12e-02	5.09e-02	4.05e-03	3.16e-03	3.08e-04	0.00e+00	3.49e-03	6.63e-04	4.37e-04	4.20e-03	2.10e-03	8.05e-02		3.37e-01
4	808094	813493	133445	140361	1.14e-02	5.73e-02	4.36e-03	2.57e-03	3.96e-04	0.00e+00	3.67e-03	6.26e-04	4.40e-04	5.26e-03	2.08e-03	8.81e-02		4.26e-01
5	809041	813847	143347	137948	1.14e-02	5.37e-02	4.21e-03	3.40e-03	1.28e-03	0.00e+00	3.78e-03	7.51e-04	4.14e-04	5.01e-03	2.04e-03	8.61e-02		5.12e-01
6	809363	810284	143206	138400	1.14e-02	5.80e-02	4.27e-03	1.76e-03	3.88e-04	0.00e+00	3.61e-03	6.92e-04	4.18e-04	4.93e-03	2.06e-03	8.75e-02		5.99e-01
7	812831	810124	143690	142769	1.14e-02	5.30e-02	4.26e-03	2.99e-03	3.87e-04	0.00e+00	4.00e-03	7.86e-04	4.05e-04	4.30e-03	2.09e-03	8.36e-02		6.83e-01
8	814279	810906	141028	143735	1.14e-02	5.85e-02	4.36e-03	2.09e-03	1.09e-03	0.00e+00	3.95e-03	7.62e-04	4.13e-04	4.31e-03	2.12e-03	8.89e-02		7.72e-01
9	814967	814393	140386	143759	1.14e-02	5.37e-02	4.34e-03	1.51e-03	1.77e-03	0.00e+00	4.02e-03	7.37e-04	4.75e-04	4.14e-03	2.14e-03	8.42e-02		8.56e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.5974 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.5974 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.597374  0.000000
];

