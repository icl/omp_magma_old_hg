% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_151 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	814806	817363	141586	141586	7.74e-03	3.62e-02	7.66e-03	1.44e-02	5.19e-04	0.00e+00	2.55e-03	4.24e-04	3.96e-04	9.13e-03	2.13e-03	8.11e-02		8.11e-02
1	796790	801541	133412	130855	7.84e-03	2.96e-02	4.05e-03	2.72e-03	6.59e-04	0.00e+00	3.26e-03	3.66e-04	4.11e-04	5.51e-03	2.16e-03	5.65e-02		1.38e-01
2	811032	814852	152234	147483	7.64e-03	4.62e-02	4.26e-03	2.01e-03	2.72e-04	0.00e+00	3.40e-03	4.06e-04	4.04e-04	4.03e-03	2.26e-03	7.09e-02		2.09e-01
3	808120	804684	138798	134978	7.75e-03	4.97e-02	4.60e-03	3.64e-03	3.87e-04	0.00e+00	3.97e-03	6.05e-04	4.80e-04	3.94e-03	2.28e-03	7.73e-02		2.86e-01
4	817070	812000	142516	145952	9.12e-03	4.98e-02	4.89e-03	2.91e-03	4.14e-04	0.00e+00	4.17e-03	5.88e-04	4.58e-04	4.74e-03	2.40e-03	7.95e-02		3.65e-01
5	806189	809534	134371	139441	7.72e-03	4.79e-02	5.20e-03	3.25e-03	1.02e-03	0.00e+00	4.30e-03	4.05e-04	4.95e-04	4.44e-03	2.54e-03	7.73e-02		4.43e-01
6	808863	807419	146058	142713	8.61e-03	5.08e-02	5.23e-03	3.12e-03	4.25e-04	0.00e+00	4.28e-03	5.65e-04	4.60e-04	4.27e-03	2.48e-03	8.02e-02		5.23e-01
7	812116	815247	144190	145634	8.56e-03	4.74e-02	5.28e-03	3.94e-03	4.21e-04	0.00e+00	4.26e-03	5.63e-04	4.59e-04	4.13e-03	2.56e-03	7.76e-02		6.00e-01
8	810446	812491	141743	138612	8.68e-03	5.16e-02	5.39e-03	2.21e-03	1.17e-03	0.00e+00	4.04e-03	6.54e-04	4.53e-04	4.14e-03	2.56e-03	8.09e-02		6.81e-01
9	810957	811920	144219	142174	8.66e-03	5.73e-02	5.11e-03	1.83e-03	1.01e-03	0.00e+00	4.25e-03	6.95e-04	4.52e-04	3.96e-03	2.35e-03	8.56e-02		7.67e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.3977 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.3977 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.397700  0.000000
];

