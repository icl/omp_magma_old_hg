% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_46 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	804758	803023	141586	141586	3.28e-03	3.10e-02	5.54e-03	8.29e-03	1.16e-03	0.00e+00	5.26e-03	2.54e-04	5.91e-04	5.96e-03	3.00e-03	6.44e-02		6.44e-02
1	796869	798553	143460	145195	3.24e-03	3.58e-02	4.49e-03	2.00e-03	1.07e-03	0.00e+00	6.52e-03	2.49e-04	6.08e-04	4.48e-03	3.95e-03	6.24e-02		1.27e-01
2	809267	814503	152155	150471	3.29e-03	5.71e-02	5.81e-03	1.70e-03	5.46e-04	0.00e+00	7.63e-03	3.33e-04	6.90e-04	3.63e-03	4.41e-03	8.52e-02		2.12e-01
3	819531	807779	140563	135327	3.36e-03	8.19e-02	6.98e-03	2.66e-03	6.19e-04	0.00e+00	9.00e-03	3.64e-04	7.55e-04	3.64e-03	4.12e-03	1.13e-01		3.25e-01
4	809028	806765	131105	142857	3.33e-03	8.19e-02	7.00e-03	1.73e-03	1.11e-03	0.00e+00	9.87e-03	3.96e-04	8.29e-04	3.76e-03	3.98e-03	1.14e-01		4.39e-01
5	813270	812363	142413	144676	3.28e-03	7.39e-02	6.90e-03	1.89e-03	9.62e-04	0.00e+00	9.85e-03	3.57e-04	8.25e-04	4.12e-03	4.01e-03	1.06e-01		5.45e-01
6	810407	812039	138977	139884	3.34e-03	7.84e-02	7.03e-03	1.73e-03	8.83e-04	0.00e+00	9.82e-03	3.20e-04	8.23e-04	3.70e-03	4.01e-03	1.10e-01		6.55e-01
7	811501	810073	142646	141014	3.33e-03	7.65e-02	7.02e-03	1.81e-03	1.11e-03	0.00e+00	9.97e-03	3.44e-04	7.89e-04	3.61e-03	4.01e-03	1.09e-01		7.64e-01
8	813063	811816	142358	143786	3.28e-03	7.80e-02	7.02e-03	1.70e-03	8.71e-04	0.00e+00	9.72e-03	3.68e-04	8.04e-04	3.73e-03	4.04e-03	1.10e-01		8.73e-01
9	810492	811856	141602	142849	3.32e-03	7.84e-02	7.10e-03	1.81e-03	1.64e-03	0.00e+00	9.91e-03	3.15e-04	8.27e-04	3.63e-03	4.07e-03	1.11e-01		9.84e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.3500 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.3500 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.349967  0.000000
];

