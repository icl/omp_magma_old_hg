% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_49 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	809176	810811	141586	141586	3.23e-03	3.01e-02	5.29e-03	6.73e-03	4.88e-04	0.00e+00	4.91e-03	2.63e-04	5.68e-04	5.41e-03	2.87e-03	5.98e-02		5.98e-02
1	813138	796725	139042	137407	3.25e-03	3.45e-02	4.30e-03	2.16e-03	6.74e-04	0.00e+00	5.99e-03	2.15e-04	6.13e-04	4.17e-03	3.81e-03	5.97e-02		1.20e-01
2	811893	807134	135886	152299	3.24e-03	5.57e-02	5.56e-03	2.49e-03	4.46e-04	0.00e+00	7.34e-03	3.04e-04	6.46e-04	3.57e-03	4.08e-03	8.34e-02		2.03e-01
3	805473	785251	137937	142696	3.28e-03	7.67e-02	6.43e-03	2.15e-03	6.25e-04	0.00e+00	8.29e-03	2.94e-04	7.66e-04	3.52e-03	3.65e-03	1.06e-01		3.09e-01
4	818448	813794	145163	165385	3.19e-03	6.81e-02	6.22e-03	1.96e-03	9.48e-04	0.00e+00	8.99e-03	2.18e-04	7.84e-04	3.97e-03	3.82e-03	9.82e-02		4.07e-01
5	808937	812872	132993	137647	3.32e-03	7.54e-02	6.67e-03	1.44e-03	1.02e-03	0.00e+00	8.96e-03	3.42e-04	7.66e-04	3.84e-03	3.80e-03	1.06e-01		5.12e-01
6	810264	810565	143310	139375	3.32e-03	7.45e-02	6.57e-03	1.73e-03	8.74e-04	0.00e+00	9.38e-03	3.50e-04	7.81e-04	3.81e-03	3.77e-03	1.05e-01		6.17e-01
7	809223	811858	142789	142488	3.30e-03	7.39e-02	6.59e-03	2.16e-03	7.46e-04	0.00e+00	8.98e-03	2.75e-04	7.15e-04	3.64e-03	3.81e-03	1.04e-01		7.22e-01
8	811292	811994	144636	142001	3.31e-03	7.53e-02	6.61e-03	2.50e-03	1.23e-03	0.00e+00	9.04e-03	3.91e-04	7.58e-04	3.70e-03	3.83e-03	1.07e-01		8.28e-01
9	807093	813761	143373	142671	3.33e-03	7.49e-02	6.65e-03	1.95e-03	9.93e-04	0.00e+00	9.11e-03	3.69e-04	7.59e-04	3.51e-03	3.83e-03	1.05e-01		9.34e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.3053 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.3053 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.305309  0.000000
];

