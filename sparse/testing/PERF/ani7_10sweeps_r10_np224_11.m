% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_224 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	824022	809245	141586	141586	1.02e-02	4.33e-02	8.99e-03	1.97e-02	1.11e-03	0.00e+00	2.40e-03	4.92e-04	3.69e-04	1.32e-02	1.77e-03	1.01e-01		1.01e-01
1	800960	811145	124196	138973	1.02e-02	3.51e-02	3.52e-03	4.84e-03	1.95e-03	0.00e+00	3.12e-03	3.80e-04	3.91e-04	7.55e-03	1.97e-03	6.90e-02		1.70e-01
2	796355	806228	148064	137879	1.02e-02	5.19e-02	3.73e-03	3.59e-03	4.53e-04	0.00e+00	3.40e-03	4.91e-04	4.00e-04	4.33e-03	1.99e-03	8.04e-02		2.51e-01
3	806721	804679	153475	143602	1.01e-02	5.16e-02	3.86e-03	4.72e-03	3.42e-04	0.00e+00	3.75e-03	6.06e-04	4.24e-04	4.36e-03	2.01e-03	8.18e-02		3.33e-01
4	813058	800708	143915	145957	1.01e-02	5.57e-02	5.14e-03	3.80e-03	4.88e-04	0.00e+00	4.00e-03	6.11e-04	4.63e-04	4.70e-03	1.98e-03	8.70e-02		4.20e-01
5	807824	809227	138383	150733	1.00e-02	5.04e-02	4.41e-03	3.94e-03	5.93e-04	0.00e+00	3.98e-03	6.03e-04	4.26e-04	5.09e-03	2.02e-03	8.15e-02		5.01e-01
6	814162	811754	144423	143020	1.02e-02	5.57e-02	4.67e-03	3.23e-03	4.86e-04	0.00e+00	4.49e-03	5.53e-04	4.27e-04	4.78e-03	2.41e-03	8.69e-02		5.88e-01
7	806202	814196	138891	141299	1.14e-02	5.18e-02	4.50e-03	1.84e-03	4.45e-04	0.00e+00	4.08e-03	6.23e-04	4.58e-04	4.21e-03	2.03e-03	8.14e-02		6.69e-01
8	813926	812577	147657	139663	1.02e-02	5.59e-02	4.35e-03	3.79e-03	4.98e-04	0.00e+00	3.98e-03	7.71e-04	4.44e-04	4.42e-03	2.03e-03	8.63e-02		7.56e-01
9	813639	811735	140739	142088	1.02e-02	5.21e-02	4.53e-03	4.43e-03	1.43e-03	0.00e+00	4.01e-03	7.44e-04	4.16e-04	4.36e-03	2.03e-03	8.43e-02		8.40e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.5833 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.5833 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.583316  0.000000
];

