% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_12 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	819706	808798	141586	141586	4.05e-03	9.08e-02	1.90e-02	8.64e-03	1.49e-03	0.00e+00	2.05e-02	2.88e-04	2.09e-03	9.67e-03	1.22e-02	1.69e-01		1.69e-01
1	822846	814767	128512	139420	4.03e-03	1.14e-01	1.86e-02	4.98e-03	1.78e-03	0.00e+00	2.57e-02	2.07e-04	2.29e-03	9.07e-03	1.58e-02	1.97e-01		3.65e-01
2	818812	811698	126178	134257	4.17e-03	1.87e-01	2.44e-02	4.53e-03	1.76e-03	0.00e+00	3.10e-02	2.76e-04	2.43e-03	8.46e-03	1.72e-02	2.81e-01		6.47e-01
3	817477	800952	131018	138132	4.12e-03	2.78e-01	2.69e-02	4.40e-03	2.53e-03	0.00e+00	3.56e-02	4.76e-04	2.85e-03	8.29e-03	1.60e-02	3.79e-01		1.03e+00
4	815645	813510	133159	149684	4.09e-03	2.52e-01	2.73e-02	4.98e-03	3.12e-03	0.00e+00	3.83e-02	3.50e-04	2.93e-03	8.66e-03	1.56e-02	3.58e-01		1.38e+00
5	804635	813614	135796	137931	4.14e-03	2.52e-01	2.78e-02	5.12e-03	3.28e-03	0.00e+00	3.82e-02	4.63e-04	2.76e-03	8.67e-03	1.55e-02	3.58e-01		1.74e+00
6	809674	811660	147612	138633	4.15e-03	2.51e-01	2.75e-02	5.38e-03	3.22e-03	0.00e+00	3.87e-02	3.66e-04	2.88e-03	8.29e-03	1.55e-02	3.57e-01		2.10e+00
7	813371	810536	143379	141393	4.14e-03	2.54e-01	2.78e-02	5.40e-03	3.06e-03	0.00e+00	3.86e-02	2.95e-04	2.84e-03	8.34e-03	1.56e-02	3.60e-01		2.46e+00
8	813962	812112	140488	143323	4.11e-03	2.58e-01	2.81e-02	5.61e-03	3.11e-03	0.00e+00	3.64e-02	4.16e-04	2.74e-03	8.43e-03	1.57e-02	3.63e-01		2.82e+00
9	812656	812829	140703	142553	4.16e-03	2.57e-01	2.82e-02	5.00e-03	3.32e-03	0.00e+00	3.88e-02	3.48e-04	2.85e-03	8.26e-03	1.56e-02	3.64e-01		3.18e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 3.6275 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 3.6275 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  3.627525  0.000000
];

