% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_144 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	813737	804683	141586	141586	7.51e-03	3.40e-02	7.03e-03	1.42e-02	7.60e-04	0.00e+00	2.69e-03	3.98e-04	4.09e-04	9.48e-03	2.16e-03	7.87e-02		7.87e-02
1	796103	797184	134481	143535	7.46e-03	2.94e-02	4.13e-03	2.67e-03	2.86e-03	0.00e+00	3.08e-03	3.75e-04	4.18e-04	5.87e-03	2.23e-03	5.85e-02		1.37e-01
2	807828	800915	152921	151840	7.34e-03	4.58e-02	4.30e-03	3.82e-03	2.77e-04	0.00e+00	3.61e-03	5.47e-04	4.23e-04	4.09e-03	2.23e-03	7.25e-02		2.10e-01
3	791950	810313	142002	148915	7.42e-03	4.93e-02	4.69e-03	3.36e-03	3.87e-04	0.00e+00	4.26e-03	5.82e-04	4.99e-04	4.00e-03	2.29e-03	7.68e-02		2.86e-01
4	809522	810123	158686	140323	7.48e-03	4.96e-02	5.01e-03	1.81e-03	4.48e-04	0.00e+00	4.23e-03	4.44e-04	5.08e-04	4.23e-03	2.40e-03	7.61e-02		3.63e-01
5	810561	806255	141919	141318	7.44e-03	4.68e-02	5.32e-03	1.89e-03	5.81e-04	0.00e+00	4.29e-03	5.61e-04	4.71e-04	4.66e-03	2.42e-03	7.44e-02		4.37e-01
6	812227	808237	141686	145992	7.48e-03	5.03e-02	5.35e-03	2.41e-03	5.40e-04	0.00e+00	4.45e-03	5.13e-04	4.94e-04	4.28e-03	2.39e-03	7.82e-02		5.15e-01
7	812766	812299	140826	144816	7.47e-03	4.77e-02	5.42e-03	1.77e-03	5.73e-04	0.00e+00	4.55e-03	7.51e-04	4.78e-04	3.99e-03	2.47e-03	7.52e-02		5.90e-01
8	811399	811461	141093	141560	7.46e-03	5.10e-02	5.43e-03	1.79e-03	6.34e-04	0.00e+00	4.39e-03	7.05e-04	5.05e-04	4.16e-03	2.41e-03	7.85e-02		6.69e-01
9	809748	811106	143266	143204	7.53e-03	4.81e-02	5.33e-03	1.58e-03	9.62e-04	0.00e+00	4.35e-03	7.11e-04	4.66e-04	4.00e-03	2.41e-03	7.55e-02		7.44e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.3751 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.3751 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.375113  0.000000
];

