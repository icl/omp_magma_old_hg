% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_103 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	807935	811079	141586	141586	6.74e-03	3.87e-02	9.84e-03	1.28e-02	1.25e-03	0.00e+00	3.62e-03	3.46e-04	4.93e-04	1.15e-02	2.19e-03	8.75e-02		8.75e-02
1	809890	807296	140283	137139	7.33e-03	3.61e-02	4.52e-03	2.07e-03	9.47e-04	0.00e+00	4.33e-03	3.15e-04	4.89e-04	6.06e-03	2.95e-03	6.51e-02		1.53e-01
2	811668	807974	139134	141728	6.86e-03	6.07e-02	5.33e-03	1.90e-03	6.07e-04	0.00e+00	5.15e-03	4.39e-04	5.69e-04	3.73e-03	3.17e-03	8.85e-02		2.41e-01
3	806983	807847	138162	141856	7.06e-03	6.39e-02	5.56e-03	1.76e-03	5.08e-04	0.00e+00	5.76e-03	4.11e-04	6.77e-04	3.30e-03	3.07e-03	9.20e-02		3.33e-01
4	805976	807021	143653	142789	6.63e-03	6.44e-02	5.64e-03	1.79e-03	6.28e-04	0.00e+00	6.20e-03	4.06e-04	6.76e-04	3.29e-03	2.83e-03	9.25e-02		4.26e-01
5	810658	809337	145465	144420	7.82e-03	5.76e-02	5.44e-03	2.78e-03	7.27e-04	0.00e+00	6.29e-03	5.41e-04	6.49e-04	3.90e-03	2.96e-03	8.88e-02		5.14e-01
6	807444	814538	141589	142910	7.12e-03	6.30e-02	5.47e-03	2.36e-03	6.80e-04	0.00e+00	6.38e-03	4.54e-04	6.44e-04	3.40e-03	2.93e-03	9.25e-02		6.07e-01
7	812396	810793	145609	138515	6.93e-03	5.99e-02	5.75e-03	1.77e-03	6.45e-04	0.00e+00	6.27e-03	4.25e-04	6.55e-04	3.75e-03	3.01e-03	8.91e-02		6.96e-01
8	813473	815808	141463	143066	6.73e-03	6.61e-02	5.73e-03	1.69e-03	1.28e-03	0.00e+00	6.39e-03	4.72e-04	6.47e-04	3.52e-03	3.06e-03	9.56e-02		7.92e-01
9	814430	812302	141192	138857	6.93e-03	6.27e-02	5.78e-03	1.52e-03	1.15e-03	0.00e+00	6.02e-03	3.83e-04	6.33e-04	3.36e-03	2.96e-03	9.15e-02		8.83e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.4125 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.4125 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.412520  0.000000
];

