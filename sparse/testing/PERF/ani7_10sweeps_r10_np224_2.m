% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_224 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	823524	808771	141586	141586	1.02e-02	4.18e-02	8.98e-03	2.13e-02	1.50e-03	0.00e+00	2.44e-03	4.53e-04	4.06e-04	1.22e-02	1.80e-03	1.01e-01		1.01e-01
1	802131	805866	124694	139447	1.02e-02	3.51e-02	3.55e-03	7.52e-03	1.76e-03	0.00e+00	3.07e-03	4.11e-04	3.90e-04	6.43e-03	1.97e-03	7.04e-02		1.72e-01
2	803910	804397	146893	143158	1.01e-02	5.23e-02	3.68e-03	3.62e-03	7.80e-04	0.00e+00	3.35e-03	5.22e-04	4.00e-04	4.21e-03	2.00e-03	8.09e-02		2.52e-01
3	811771	808852	145920	145433	1.01e-02	5.19e-02	3.91e-03	3.83e-03	3.18e-04	0.00e+00	3.70e-03	7.15e-04	4.19e-04	4.29e-03	2.02e-03	8.11e-02		3.34e-01
4	803262	807035	138865	141784	1.01e-02	5.81e-02	4.23e-03	2.59e-03	4.13e-04	0.00e+00	3.90e-03	8.19e-04	4.44e-04	4.34e-03	1.94e-03	8.69e-02		4.21e-01
5	813550	809748	148179	144406	1.01e-02	4.98e-02	4.35e-03	2.65e-03	3.82e-04	0.00e+00	3.90e-03	8.24e-04	4.22e-04	4.78e-03	2.04e-03	7.92e-02		5.00e-01
6	809023	809306	138697	142499	1.02e-02	5.69e-02	4.44e-03	5.42e-03	3.89e-04	0.00e+00	3.98e-03	5.63e-04	4.32e-04	4.40e-03	2.02e-03	8.87e-02		5.88e-01
7	811673	813199	144030	143747	1.01e-02	5.10e-02	4.39e-03	1.94e-03	4.33e-04	0.00e+00	3.88e-03	6.67e-04	4.48e-04	4.55e-03	2.05e-03	7.95e-02		6.68e-01
8	811329	811958	142186	140660	1.02e-02	5.72e-02	4.47e-03	3.43e-03	1.45e-03	0.00e+00	4.01e-03	5.50e-04	4.25e-04	4.14e-03	2.03e-03	8.79e-02		7.56e-01
9	816493	815972	143336	142707	1.02e-02	5.15e-02	4.51e-03	2.60e-03	1.33e-03	0.00e+00	4.01e-03	5.96e-04	4.55e-04	4.24e-03	2.06e-03	8.14e-02		8.37e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.5861 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.5861 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.586097  0.000000
];

