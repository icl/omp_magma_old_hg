% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_64 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	820610	802426	141586	141586	3.22e-03	2.62e-02	4.65e-03	6.78e-03	6.87e-04	0.00e+00	3.82e-03	2.67e-04	4.53e-04	5.88e-03	2.24e-03	5.42e-02		5.42e-02
1	803694	809466	127608	145792	3.21e-03	3.03e-02	3.34e-03	2.70e-03	8.99e-04	0.00e+00	4.61e-03	2.42e-04	4.74e-04	3.88e-03	3.03e-03	5.27e-02		1.07e-01
2	794767	809020	145330	139558	3.26e-03	5.01e-02	4.34e-03	1.91e-03	5.80e-04	0.00e+00	5.64e-03	2.96e-04	5.68e-04	3.49e-03	3.11e-03	7.33e-02		1.80e-01
3	808962	811988	155063	140810	3.29e-03	6.10e-02	4.74e-03	2.30e-03	5.58e-04	0.00e+00	6.61e-03	3.05e-04	6.46e-04	3.21e-03	3.09e-03	8.57e-02		2.66e-01
4	800467	811584	141674	138648	3.28e-03	6.27e-02	5.40e-03	1.46e-03	8.43e-04	0.00e+00	7.01e-03	3.62e-04	6.97e-04	3.18e-03	2.90e-03	8.79e-02		3.54e-01
5	811671	813659	150974	139857	3.27e-03	5.76e-02	4.99e-03	2.13e-03	6.77e-04	0.00e+00	6.86e-03	4.23e-04	6.55e-04	3.45e-03	2.98e-03	8.30e-02		4.37e-01
6	813285	809842	140576	138588	3.27e-03	6.11e-02	5.09e-03	1.55e-03	6.54e-04	0.00e+00	7.01e-03	2.87e-04	6.55e-04	3.24e-03	2.99e-03	8.59e-02		5.23e-01
7	807634	810304	139768	143211	3.26e-03	6.05e-02	5.09e-03	1.76e-03	6.60e-04	0.00e+00	6.97e-03	4.53e-04	6.45e-04	3.46e-03	2.97e-03	8.57e-02		6.08e-01
8	813871	813040	146225	143555	3.29e-03	6.08e-02	5.08e-03	1.80e-03	1.09e-03	0.00e+00	7.08e-03	2.89e-04	6.67e-04	3.21e-03	2.99e-03	8.63e-02		6.95e-01
9	813221	813198	140794	141625	3.30e-03	6.10e-02	5.16e-03	1.86e-03	1.18e-03	0.00e+00	7.07e-03	2.89e-04	6.52e-04	3.20e-03	3.00e-03	8.68e-02		7.82e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.1520 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.1520 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.151980  0.000000
];

