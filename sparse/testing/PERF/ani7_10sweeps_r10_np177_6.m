% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_177 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	822476	801951	141586	141586	8.42e-03	3.59e-02	7.47e-03	1.56e-02	8.47e-04	0.00e+00	2.21e-03	4.16e-04	3.95e-04	1.05e-02	1.93e-03	8.36e-02		8.36e-02
1	795233	813478	125742	146267	8.30e-03	3.07e-02	3.56e-03	6.24e-03	7.84e-04	0.00e+00	2.56e-03	4.18e-04	3.84e-04	5.97e-03	2.06e-03	6.09e-02		1.45e-01
2	802810	829487	153791	135546	8.45e-03	4.53e-02	4.00e-03	3.07e-03	3.28e-04	0.00e+00	2.94e-03	6.35e-04	3.81e-04	3.85e-03	2.28e-03	7.12e-02		2.16e-01
3	805861	806426	147020	120343	8.56e-03	4.72e-02	4.32e-03	2.55e-03	4.36e-04	0.00e+00	3.32e-03	5.55e-04	4.31e-04	3.87e-03	2.29e-03	7.36e-02		2.89e-01
4	811620	816960	144775	144210	8.37e-03	4.81e-02	4.53e-03	2.21e-03	3.68e-04	0.00e+00	3.55e-03	5.95e-04	4.04e-04	4.12e-03	2.30e-03	7.46e-02		3.64e-01
5	810588	807606	139821	134481	8.41e-03	4.63e-02	4.85e-03	2.01e-03	9.98e-04	0.00e+00	3.57e-03	6.94e-04	4.06e-04	4.28e-03	2.29e-03	7.39e-02		4.38e-01
6	812142	811335	141659	144641	8.35e-03	4.93e-02	4.74e-03	2.50e-03	3.99e-04	0.00e+00	3.70e-03	4.27e-04	4.88e-04	4.24e-03	2.30e-03	7.64e-02		5.14e-01
7	814014	811132	140911	141718	8.38e-03	4.52e-02	4.77e-03	3.84e-03	3.80e-04	0.00e+00	3.63e-03	6.71e-04	4.34e-04	4.11e-03	2.31e-03	7.37e-02		5.88e-01
8	807306	814823	139845	142727	8.44e-03	4.96e-02	4.89e-03	2.11e-03	9.19e-04	0.00e+00	3.52e-03	4.79e-04	4.33e-04	4.01e-03	2.35e-03	7.68e-02		6.65e-01
9	819133	810662	147359	139842	8.47e-03	4.63e-02	4.86e-03	2.41e-03	9.48e-04	0.00e+00	3.71e-03	6.07e-04	4.27e-04	3.92e-03	2.31e-03	7.40e-02		7.39e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.3837 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.3837 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.383712  0.000000
];

