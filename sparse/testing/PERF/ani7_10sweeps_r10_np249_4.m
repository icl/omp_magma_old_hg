% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_249 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	823807	818247	141586	141586	1.11e-02	4.44e-02	9.25e-03	1.92e-02	2.93e-03	0.00e+00	2.22e-03	5.31e-04	3.39e-04	1.41e-02	1.72e-03	1.06e-01		1.06e-01
1	802255	801055	124411	129971	1.12e-02	3.75e-02	3.25e-03	5.22e-03	2.90e-03	0.00e+00	2.90e-03	3.86e-04	4.03e-04	7.52e-03	1.83e-03	7.31e-02		1.79e-01
2	808001	804044	146769	147969	1.09e-02	5.09e-02	3.62e-03	3.17e-03	3.10e-04	0.00e+00	3.06e-03	5.52e-04	3.75e-04	4.21e-03	1.99e-03	7.91e-02		2.58e-01
3	808483	802173	141829	145786	1.09e-02	4.99e-02	4.14e-03	2.52e-03	3.16e-04	0.00e+00	3.46e-03	6.50e-04	4.43e-04	4.22e-03	1.98e-03	7.86e-02		3.37e-01
4	810250	813915	142153	148463	1.09e-02	5.46e-02	3.73e-01	2.42e-03	5.59e-04	0.00e+00	4.03e-03	5.92e-04	4.52e-04	4.37e-03	2.13e-03	4.53e-01		7.90e-01
5	808767	811055	141191	137526	1.14e-02	5.00e-02	4.24e-03	2.43e-03	4.16e-04	0.00e+00	3.52e-03	3.73e-04	3.97e-04	5.02e-03	2.06e-03	7.99e-02		8.69e-01
6	814449	810166	143480	141192	1.11e-02	5.52e-02	4.23e-03	2.39e-03	4.05e-04	0.00e+00	3.50e-03	6.86e-04	3.96e-04	4.68e-03	2.07e-03	8.46e-02		9.54e-01
7	812246	810261	138604	142887	1.11e-02	5.07e-02	4.25e-03	2.24e-03	4.13e-04	0.00e+00	3.54e-03	7.93e-04	3.97e-04	4.28e-03	2.07e-03	7.97e-02		1.03e+00
8	809447	812016	141613	143598	1.10e-02	5.53e-02	4.27e-03	2.09e-03	3.76e-04	0.00e+00	4.39e-03	7.56e-04	4.46e-04	4.65e-03	2.08e-03	8.54e-02		1.12e+00
9	810655	812525	145218	142649	1.11e-02	5.18e-02	4.32e-03	2.52e-03	1.56e-03	0.00e+00	3.61e-03	9.19e-04	4.30e-04	4.16e-03	2.10e-03	8.25e-02		1.20e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.8358 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.8358 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.835753  0.000000
];

