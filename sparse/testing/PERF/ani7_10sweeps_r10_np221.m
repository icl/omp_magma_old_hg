% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_221 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	822403	810476	141586	141586	1.20e-02	6.46e-02	1.59e-02	2.35e-02	2.77e-03	0.00e+00	3.20e-03	4.75e-04	3.66e-04	2.25e-02	1.91e-03	1.47e-01		1.47e-01
1	800393	795991	125815	137742	1.20e-02	4.63e-02	3.63e-03	5.61e-03	1.80e-03	0.00e+00	3.68e-03	3.98e-04	3.94e-04	1.23e-02	2.39e-03	8.84e-02		2.36e-01
2	810850	812181	148631	153033	1.18e-02	6.82e-02	4.37e-03	2.99e-03	1.29e-03	0.00e+00	5.04e-03	6.07e-04	4.58e-04	4.56e-03	2.55e-03	1.02e-01		3.38e-01
3	819467	810423	138980	137649	1.21e-02	6.52e-02	5.34e-03	2.77e-03	6.02e-04	0.00e+00	4.50e-03	6.77e-04	4.94e-04	4.57e-03	2.58e-03	9.88e-02		4.36e-01
4	816040	811322	131169	140213	1.21e-02	7.70e-02	5.15e-03	2.42e-03	1.22e-03	0.00e+00	4.79e-03	6.19e-04	5.67e-04	4.48e-03	2.44e-03	1.11e-01		5.47e-01
5	810092	812980	135401	140119	1.21e-02	6.41e-02	5.01e-03	4.78e-03	5.77e-04	0.00e+00	4.88e-03	5.43e-04	5.01e-04	5.67e-03	2.45e-03	1.01e-01		6.48e-01
6	807523	808522	142155	139267	1.21e-02	7.52e-02	5.04e-03	3.19e-03	6.85e-04	0.00e+00	4.81e-03	7.61e-04	5.23e-04	4.61e-03	2.49e-03	1.09e-01		7.57e-01
7	817713	811455	145530	144531	1.20e-02	6.42e-02	5.01e-03	2.21e-03	6.06e-04	0.00e+00	4.83e-03	7.70e-04	4.90e-04	4.63e-03	2.50e-03	9.72e-02		8.55e-01
8	811390	814013	136146	142404	1.21e-02	7.66e-02	5.10e-03	2.77e-03	1.61e-03	0.00e+00	5.14e-03	7.33e-04	5.29e-04	4.91e-03	2.48e-03	1.12e-01		9.67e-01
9	814097	807075	143275	140652	1.22e-02	6.60e-02	5.06e-03	2.95e-03	1.77e-03	0.00e+00	4.89e-03	6.49e-04	4.93e-04	4.46e-03	2.47e-03	1.01e-01		1.07e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.8154 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.8154 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.815417  0.000000
];

