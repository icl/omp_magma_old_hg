% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_173 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	820010	806038	141586	141586	8.34e-03	3.68e-02	7.44e-03	1.53e-02	7.77e-04	0.00e+00	2.26e-03	4.19e-04	3.60e-04	1.11e-02	1.92e-03	8.47e-02		8.47e-02
1	799059	807251	128208	142180	8.27e-03	3.10e-02	3.60e-03	5.49e-03	9.49e-04	0.00e+00	2.75e-03	3.97e-04	3.90e-04	6.67e-03	2.06e-03	6.16e-02		1.46e-01
2	811342	815316	149965	141773	8.23e-03	4.50e-02	4.04e-03	3.53e-03	2.93e-04	0.00e+00	3.02e-03	4.14e-04	3.80e-04	4.02e-03	2.19e-03	7.11e-02		2.17e-01
3	803987	808416	138488	134514	8.33e-03	4.65e-02	4.39e-03	2.19e-03	4.56e-04	0.00e+00	3.36e-03	5.14e-04	4.53e-04	3.97e-03	2.35e-03	7.25e-02		2.90e-01
4	818403	799571	146649	142220	8.25e-03	4.75e-02	4.56e-03	1.99e-03	4.38e-04	0.00e+00	3.76e-03	6.37e-04	4.44e-04	4.53e-03	2.33e-03	7.44e-02		3.64e-01
5	815822	809872	133038	151870	8.14e-03	4.35e-02	4.78e-03	2.35e-03	8.10e-04	0.00e+00	3.63e-03	5.64e-04	4.27e-04	4.31e-03	2.35e-03	7.08e-02		4.35e-01
6	806527	813744	136425	142375	8.27e-03	5.03e-02	4.95e-03	1.78e-03	3.43e-04	0.00e+00	3.63e-03	7.04e-04	4.04e-04	4.39e-03	2.41e-03	7.71e-02		5.12e-01
7	812313	812693	146526	139309	8.31e-03	4.56e-02	4.98e-03	1.61e-03	4.12e-04	0.00e+00	3.57e-03	5.23e-04	4.10e-04	3.98e-03	2.37e-03	7.17e-02		5.84e-01
8	811691	813179	141546	141166	8.27e-03	5.16e-02	4.95e-03	1.62e-03	6.71e-04	0.00e+00	3.57e-03	6.68e-04	3.97e-04	4.39e-03	2.38e-03	7.85e-02		6.63e-01
9	814063	813149	142974	141486	8.35e-03	4.63e-02	5.43e-03	1.75e-03	1.12e-03	0.00e+00	3.65e-03	6.66e-04	4.03e-04	3.95e-03	2.53e-03	7.42e-02		7.37e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.3810 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.3810 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.381022  0.000000
];

