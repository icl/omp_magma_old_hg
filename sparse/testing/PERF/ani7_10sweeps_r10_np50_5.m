% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_50 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	818651	797084	141586	141586	3.22e-03	2.94e-02	5.23e-03	6.72e-03	9.47e-04	0.00e+00	4.74e-03	3.04e-04	5.17e-04	5.98e-03	2.83e-03	5.99e-02		5.99e-02
1	811453	809621	129567	151134	3.19e-03	3.41e-02	4.24e-03	3.29e-03	7.24e-04	0.00e+00	5.89e-03	2.21e-04	5.72e-04	4.06e-03	3.77e-03	6.01e-02		1.20e-01
2	811992	814897	137571	139403	3.30e-03	5.52e-02	5.46e-03	1.77e-03	5.65e-04	0.00e+00	7.18e-03	3.47e-04	6.37e-04	3.87e-03	4.06e-03	8.24e-02		2.02e-01
3	807666	803060	137838	134933	3.30e-03	7.75e-02	6.53e-03	1.62e-03	6.11e-04	0.00e+00	8.31e-03	4.09e-04	7.69e-04	3.48e-03	3.78e-03	1.06e-01		3.09e-01
4	802649	797644	142970	147576	3.26e-03	7.23e-02	6.32e-03	1.57e-03	8.48e-04	0.00e+00	8.85e-03	3.88e-04	7.81e-04	3.47e-03	3.66e-03	1.01e-01		4.10e-01
5	809291	808585	148792	153797	3.24e-03	6.82e-02	6.26e-03	1.62e-03	8.02e-04	0.00e+00	8.63e-03	3.52e-04	7.33e-04	3.97e-03	3.69e-03	9.75e-02		5.08e-01
6	812494	810529	142956	143662	3.32e-03	7.20e-02	6.43e-03	1.72e-03	8.19e-04	0.00e+00	8.95e-03	3.71e-04	7.64e-04	3.50e-03	3.75e-03	1.02e-01		6.09e-01
7	811647	812405	140559	142524	3.27e-03	7.26e-02	6.50e-03	1.53e-03	7.76e-04	0.00e+00	8.90e-03	4.02e-04	7.57e-04	3.82e-03	3.98e-03	1.03e-01		7.12e-01
8	813856	807269	142212	141454	4.85e-03	7.46e-02	8.56e-03	1.77e-03	1.40e-03	0.00e+00	9.10e-03	3.86e-04	7.77e-04	3.66e-03	3.99e-03	1.09e-01		8.21e-01
9	814346	810137	140809	147396	5.02e-03	7.31e-02	8.40e-03	1.68e-03	1.17e-03	0.00e+00	9.22e-03	3.68e-04	7.75e-04	3.51e-03	4.01e-03	1.07e-01		9.28e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.2963 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.2963 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.296316  0.000000
];

