% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_239 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	829514	808199	141586	141586	1.08e-02	4.38e-02	9.00e-03	2.03e-02	8.96e-04	0.00e+00	2.28e-03	6.29e-04	3.48e-04	1.25e-02	1.75e-03	1.02e-01		1.02e-01
1	795355	813636	118704	140019	1.07e-02	3.58e-02	3.41e-03	3.43e-03	1.15e-03	0.00e+00	3.05e-03	6.03e-04	3.79e-04	6.71e-03	1.89e-03	6.71e-02		1.69e-01
2	791965	823557	153669	135388	1.08e-02	5.07e-02	3.60e-03	2.78e-03	2.78e-04	0.00e+00	3.18e-03	7.34e-04	3.79e-04	4.23e-03	2.03e-03	7.87e-02		2.48e-01
3	803687	811081	157865	126273	1.09e-02	5.01e-02	3.98e-03	2.64e-03	4.68e-04	0.00e+00	3.51e-03	5.13e-04	4.19e-04	4.18e-03	2.03e-03	7.87e-02		3.27e-01
4	810530	808158	146949	139555	1.08e-02	5.35e-02	4.24e-03	2.27e-03	4.39e-04	0.00e+00	3.73e-03	7.89e-04	4.54e-04	5.51e-03	2.14e-03	8.38e-02		4.11e-01
5	808646	808783	140911	143283	1.07e-02	4.94e-02	4.37e-03	3.30e-03	1.32e-03	0.00e+00	3.75e-03	6.81e-04	4.48e-04	5.10e-03	2.17e-03	8.12e-02		4.92e-01
6	810831	809095	143601	143464	1.07e-02	5.48e-02	4.42e-03	2.36e-03	3.96e-04	0.00e+00	3.68e-03	7.70e-04	4.41e-04	4.86e-03	2.19e-03	8.46e-02		5.77e-01
7	812092	814656	142222	143958	1.07e-02	4.99e-02	4.45e-03	1.50e-03	4.21e-04	0.00e+00	3.71e-03	5.98e-04	4.76e-04	4.25e-03	2.20e-03	7.83e-02		6.55e-01
8	815770	813255	141767	139203	1.08e-02	5.66e-02	4.52e-03	1.70e-03	1.48e-03	0.00e+00	3.74e-03	7.20e-04	4.07e-04	4.54e-03	2.22e-03	8.67e-02		7.42e-01
9	815240	815544	138895	141410	1.08e-02	5.18e-02	4.54e-03	2.50e-03	1.38e-03	0.00e+00	3.78e-03	8.56e-04	4.17e-04	4.21e-03	2.25e-03	8.25e-02		8.24e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.5887 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.5887 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.588747  0.000000
];

