% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_112 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	818653	804865	141586	141586	6.01e-03	3.87e-02	9.47e-03	1.25e-02	1.06e-03	0.00e+00	3.30e-03	3.95e-04	4.27e-04	1.07e-02	2.00e-03	8.46e-02		8.46e-02
1	805854	801009	129565	143353	6.02e-03	3.48e-02	3.50e-03	3.42e-03	1.16e-03	0.00e+00	4.08e-03	2.62e-04	4.00e-04	6.88e-03	2.74e-03	6.32e-02		1.48e-01
2	818588	797185	143170	148015	5.95e-03	5.59e-02	4.39e-03	1.71e-03	8.62e-04	0.00e+00	4.68e-03	4.16e-04	4.89e-04	3.18e-03	2.88e-03	8.04e-02		2.28e-01
3	813678	806145	131242	152645	5.95e-03	6.15e-02	4.90e-03	2.36e-03	5.07e-04	0.00e+00	5.37e-03	3.10e-04	5.77e-04	3.24e-03	2.80e-03	8.75e-02		3.16e-01
4	807343	801590	136958	144491	6.05e-03	6.41e-02	5.07e-03	2.23e-03	7.59e-04	0.00e+00	5.85e-03	5.15e-04	6.14e-04	3.27e-03	2.55e-03	9.10e-02		4.07e-01
5	814954	809962	144098	149851	5.97e-03	5.53e-02	4.91e-03	2.27e-03	5.56e-04	0.00e+00	5.73e-03	3.87e-04	5.63e-04	3.83e-03	2.68e-03	8.22e-02		4.89e-01
6	807877	809028	137293	142285	6.10e-03	6.11e-02	5.05e-03	1.77e-03	5.87e-04	0.00e+00	5.73e-03	4.10e-04	5.93e-04	3.33e-03	2.66e-03	8.74e-02		5.76e-01
7	810833	810407	145176	144025	6.02e-03	5.74e-02	5.03e-03	2.01e-03	6.11e-04	0.00e+00	5.75e-03	6.82e-04	5.51e-04	3.64e-03	2.67e-03	8.44e-02		6.61e-01
8	814255	810944	143026	143452	6.06e-03	6.11e-02	5.05e-03	2.74e-03	1.15e-03	0.00e+00	5.79e-03	4.88e-04	5.36e-04	3.48e-03	2.72e-03	8.91e-02		7.50e-01
9	812901	812793	140410	143721	6.06e-03	5.84e-02	5.10e-03	1.77e-03	1.10e-03	0.00e+00	5.71e-03	4.52e-04	5.67e-04	3.23e-03	2.72e-03	8.51e-02		8.35e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.3544 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.3544 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.354405  0.000000
];

