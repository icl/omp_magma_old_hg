% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_7 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	817533	819965	141586	141586	4.46e-03	1.46e-01	3.20e-02	1.07e-02	2.90e-03	0.00e+00	3.52e-02	2.76e-04	3.40e-03	1.40e-02	2.06e-02	2.70e-01		2.70e-01
1	802929	795517	130685	128253	4.63e-03	1.86e-01	3.18e-02	8.93e-03	3.44e-03	0.00e+00	4.34e-02	2.80e-04	3.79e-03	1.34e-02	2.55e-02	3.22e-01		5.92e-01
2	802353	800364	146095	153507	4.61e-03	2.99e-01	3.87e-02	6.33e-03	2.94e-03	0.00e+00	5.18e-02	2.30e-04	4.00e-03	1.35e-02	2.78e-02	4.48e-01		1.04e+00
3	815194	809796	147477	149466	4.60e-03	4.20e-01	4.34e-02	7.55e-03	3.85e-03	0.00e+00	5.98e-02	2.82e-04	4.44e-03	1.26e-02	2.68e-02	5.83e-01		1.62e+00
4	817389	809636	135442	140840	4.67e-03	4.15e-01	4.64e-02	8.68e-03	5.96e-03	0.00e+00	6.59e-02	5.23e-04	4.89e-03	1.25e-02	2.63e-02	5.91e-01		2.21e+00
5	805159	806659	134052	141805	4.62e-03	4.03e-01	4.66e-02	8.36e-03	4.91e-03	0.00e+00	6.31e-02	2.64e-04	4.62e-03	1.35e-02	2.60e-02	5.75e-01		2.79e+00
6	815529	809480	147088	145588	4.65e-03	4.03e-01	4.60e-02	7.86e-03	4.80e-03	0.00e+00	6.36e-02	4.11e-04	4.56e-03	1.25e-02	2.65e-02	5.74e-01		3.36e+00
7	808289	814199	137524	143573	4.66e-03	4.20e-01	4.74e-02	8.07e-03	4.95e-03	0.00e+00	6.29e-02	3.42e-04	4.66e-03	1.26e-02	2.63e-02	5.92e-01		3.96e+00
8	811402	810742	145570	139660	4.70e-03	4.16e-01	4.71e-02	7.85e-03	5.06e-03	0.00e+00	6.48e-02	2.56e-04	4.68e-03	1.26e-02	2.64e-02	5.89e-01		4.54e+00
9	810503	812955	143263	143923	4.75e-03	4.21e-01	4.71e-02	6.98e-03	4.93e-03	0.00e+00	6.07e-02	5.13e-04	4.45e-03	1.24e-02	2.64e-02	5.89e-01		5.13e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 5.6328 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 5.6328 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  5.632790  0.000000
];

