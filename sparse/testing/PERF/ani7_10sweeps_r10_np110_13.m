% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_110 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	826614	815528	141586	141586	5.69e-03	2.82e-02	5.78e-03	1.00e-02	1.28e-03	0.00e+00	2.43e-03	3.49e-04	4.29e-04	6.29e-03	2.07e-03	6.26e-02		6.26e-02
1	792026	795767	121604	132690	5.80e-03	2.84e-02	3.68e-03	4.85e-03	1.23e-03	0.00e+00	3.03e-03	3.06e-04	4.49e-04	4.26e-03	2.34e-03	5.44e-02		1.17e-01
2	817461	809586	156998	153257	5.73e-03	4.20e-02	4.23e-03	1.27e-03	8.68e-04	0.00e+00	3.43e-03	4.09e-04	4.14e-04	3.16e-03	2.58e-03	6.41e-02		1.81e-01
3	795563	815770	132369	140244	5.72e-03	4.72e-02	4.70e-03	2.06e-03	3.59e-04	0.00e+00	4.05e-03	3.31e-04	4.78e-04	3.10e-03	2.44e-03	7.04e-02		2.51e-01
4	812185	806503	155073	134866	5.84e-03	4.54e-02	4.66e-03	2.32e-03	4.65e-04	0.00e+00	4.14e-03	4.66e-04	4.40e-04	3.14e-03	2.45e-03	6.93e-02		3.21e-01
5	809754	811997	139256	144938	5.71e-03	4.25e-02	4.86e-03	2.26e-03	4.76e-04	0.00e+00	4.22e-03	5.91e-04	4.41e-04	3.45e-03	2.47e-03	6.70e-02		3.88e-01
6	810558	813019	142493	140250	7.33e-03	4.67e-02	4.99e-03	1.95e-03	4.86e-04	0.00e+00	4.28e-03	5.05e-04	4.78e-04	3.18e-03	2.47e-03	7.23e-02		4.60e-01
7	810753	812974	142495	140034	5.74e-03	4.44e-02	5.05e-03	2.39e-03	4.48e-04	0.00e+00	4.18e-03	3.86e-04	4.82e-04	3.20e-03	2.51e-03	6.88e-02		5.29e-01
8	811708	810525	143106	140885	5.76e-03	4.63e-02	5.03e-03	2.51e-03	1.19e-03	0.00e+00	4.28e-03	4.91e-04	4.38e-04	3.13e-03	2.50e-03	7.16e-02		6.00e-01
9	812175	815098	142957	144140	5.73e-03	4.36e-02	5.00e-03	1.49e-03	8.01e-04	0.00e+00	4.28e-03	4.14e-04	4.46e-04	3.08e-03	2.52e-03	6.74e-02		6.68e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.1458 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.1458 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.145821  0.000000
];

