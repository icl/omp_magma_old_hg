% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_104 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	809345	805791	141586	141586	5.65e-03	2.91e-02	5.91e-03	9.99e-03	4.71e-04	0.00e+00	2.59e-03	3.46e-04	4.07e-04	6.63e-03	2.10e-03	6.32e-02		6.32e-02
1	818334	800247	138873	142427	5.66e-03	2.78e-02	3.72e-03	3.35e-03	4.77e-04	0.00e+00	2.97e-03	3.18e-04	4.53e-04	4.82e-03	2.38e-03	5.20e-02		1.15e-01
2	819792	812035	130690	148777	5.59e-03	4.45e-02	4.38e-03	2.85e-03	3.16e-04	0.00e+00	3.58e-03	2.85e-04	4.37e-04	3.18e-03	2.57e-03	6.77e-02		1.83e-01
3	805483	802245	130038	137795	5.65e-03	5.37e-02	4.72e-03	1.61e-03	5.18e-04	0.00e+00	4.23e-03	3.63e-04	4.89e-04	3.18e-03	2.45e-03	7.69e-02		2.60e-01
4	811126	815715	145153	148391	5.59e-03	4.59e-02	4.86e-03	1.65e-03	5.18e-04	0.00e+00	4.51e-03	3.88e-04	4.70e-04	4.71e-03	2.59e-03	7.12e-02		3.31e-01
5	812799	811283	140315	135726	5.72e-03	4.47e-02	5.20e-03	1.61e-03	7.72e-04	0.00e+00	4.50e-03	3.55e-04	5.15e-04	3.29e-03	2.58e-03	6.92e-02		4.00e-01
6	811979	808989	139448	140964	5.64e-03	4.77e-02	5.21e-03	1.62e-03	4.27e-04	0.00e+00	4.44e-03	4.52e-04	5.05e-04	3.34e-03	2.60e-03	7.20e-02		4.72e-01
7	811597	810073	141074	144064	5.61e-03	4.55e-02	5.21e-03	1.30e-03	4.65e-04	0.00e+00	4.40e-03	4.08e-04	4.83e-04	3.14e-03	2.56e-03	6.91e-02		5.41e-01
8	810879	814796	142262	143786	5.64e-03	4.77e-02	5.26e-03	1.86e-03	9.82e-04	0.00e+00	4.52e-03	4.32e-04	5.19e-04	3.24e-03	2.64e-03	7.28e-02		6.14e-01
9	811918	815775	143786	139869	5.70e-03	4.66e-02	5.40e-03	1.47e-03	9.19e-04	0.00e+00	4.40e-03	8.59e-04	4.98e-04	3.11e-03	2.63e-03	7.16e-02		6.86e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.1625 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.1625 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.162533  0.000000
];

