% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_160 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	822675	813791	141586	141586	7.93e-03	3.58e-02	7.50e-03	1.47e-02	7.18e-04	0.00e+00	2.39e-03	5.13e-04	3.77e-04	9.70e-03	2.09e-03	8.17e-02		8.17e-02
1	819772	778410	125543	134427	7.98e-03	3.01e-02	3.95e-03	5.34e-03	8.07e-04	0.00e+00	3.09e-03	4.19e-04	3.80e-04	5.49e-03	2.18e-03	5.98e-02		1.41e-01
2	810163	796922	129252	170614	7.59e-03	4.61e-02	4.17e-03	4.13e-03	2.74e-04	0.00e+00	3.26e-03	6.11e-04	4.10e-04	3.91e-03	2.32e-03	7.28e-02		2.14e-01
3	815527	811513	139667	152908	7.77e-03	4.65e-02	4.56e-03	2.26e-03	3.54e-04	0.00e+00	3.78e-03	4.05e-04	4.71e-04	3.95e-03	2.43e-03	7.25e-02		2.87e-01
4	798101	804754	135109	139123	7.94e-03	5.20e-02	5.03e-03	1.45e-03	3.90e-04	0.00e+00	3.97e-03	5.93e-04	4.52e-04	4.12e-03	2.43e-03	7.84e-02		3.65e-01
5	808886	812580	153340	146687	7.86e-03	4.23e-02	5.00e-03	2.59e-03	1.21e-03	0.00e+00	4.00e-03	3.82e-04	4.76e-04	4.32e-03	2.50e-03	7.06e-02		4.36e-01
6	811454	809763	143361	139667	7.97e-03	4.98e-02	5.19e-03	3.93e-03	4.52e-04	0.00e+00	4.02e-03	6.44e-04	4.44e-04	4.26e-03	2.58e-03	7.93e-02		5.15e-01
7	813473	811487	141599	143290	7.91e-03	4.46e-02	5.26e-03	3.36e-03	4.59e-04	0.00e+00	4.01e-03	6.11e-04	4.72e-04	4.17e-03	2.54e-03	7.34e-02		5.88e-01
8	813341	812306	140386	142372	7.95e-03	4.96e-02	5.30e-03	1.59e-03	1.13e-03	0.00e+00	4.00e-03	5.65e-04	4.40e-04	3.99e-03	2.55e-03	7.71e-02		6.66e-01
9	814406	811833	141324	142359	7.97e-03	4.51e-02	5.33e-03	2.26e-03	9.98e-04	0.00e+00	3.89e-03	6.23e-04	4.37e-04	3.95e-03	2.55e-03	7.31e-02		7.39e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.3771 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.3771 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.377143  0.000000
];

