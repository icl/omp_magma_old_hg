% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_124 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	822653	807143	141586	141586	5.89e-03	2.83e-02	5.78e-03	1.05e-02	6.50e-04	0.00e+00	2.91e-03	3.33e-04	3.62e-04	7.74e-03	1.82e-03	6.43e-02		6.43e-02
1	796247	808429	125565	141075	5.90e-03	2.78e-02	3.23e-03	3.54e-03	1.04e-03	0.00e+00	3.37e-03	3.14e-04	4.16e-04	4.77e-03	2.14e-03	5.25e-02		1.17e-01
2	803106	816425	152777	140595	5.88e-03	3.79e-02	3.73e-03	1.49e-03	2.90e-04	0.00e+00	4.08e-03	2.34e-04	4.43e-04	3.01e-03	2.34e-03	5.94e-02		1.76e-01
3	805538	785976	146724	133405	5.95e-03	4.55e-02	4.24e-03	1.67e-03	5.80e-04	0.00e+00	4.96e-03	4.02e-04	4.64e-04	3.07e-03	2.99e-03	6.99e-02		2.46e-01
4	807423	810594	145098	164660	5.76e-03	4.47e-02	4.23e-03	2.16e-03	4.21e-04	0.00e+00	4.71e-03	4.30e-04	4.37e-04	3.62e-03	2.27e-03	6.87e-02		3.15e-01
5	810891	810951	144018	140847	5.90e-03	4.50e-02	4.41e-03	1.58e-03	6.28e-04	0.00e+00	5.00e-03	4.62e-04	4.53e-04	3.62e-03	2.29e-03	6.94e-02		3.84e-01
6	813467	807862	141356	141296	5.92e-03	4.82e-02	4.50e-03	1.70e-03	4.08e-04	0.00e+00	4.79e-03	4.22e-04	5.32e-04	3.39e-03	2.39e-03	7.22e-02		4.56e-01
7	815068	812573	139586	145191	5.91e-03	4.61e-02	4.52e-03	1.57e-03	4.10e-04	0.00e+00	5.00e-03	3.65e-04	4.44e-04	3.08e-03	2.40e-03	6.98e-02		5.26e-01
8	809578	811961	138791	141286	5.90e-03	4.98e-02	4.59e-03	1.20e-03	7.07e-04	0.00e+00	4.90e-03	3.91e-04	4.67e-04	3.19e-03	2.34e-03	7.35e-02		6.00e-01
9	811828	811172	145087	142704	5.94e-03	4.74e-02	4.67e-03	1.57e-03	1.06e-03	0.00e+00	4.94e-03	4.14e-04	4.73e-04	3.01e-03	2.37e-03	7.19e-02		6.71e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.1555 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.1555 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.155483  0.000000
];

