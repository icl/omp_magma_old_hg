% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_26 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	808799	816478	141586	141586	3.63e-03	4.88e-02	9.31e-03	7.31e-03	7.91e-04	0.00e+00	9.77e-03	2.61e-04	9.99e-04	7.11e-03	5.70e-03	9.36e-02		9.36e-02
1	801193	787514	139419	131740	4.29e-03	6.04e-02	8.67e-03	4.94e-03	1.08e-03	0.00e+00	1.18e-02	3.13e-04	1.19e-03	5.83e-03	7.21e-03	1.06e-01		1.99e-01
2	820117	810122	147831	161510	3.60e-03	9.29e-02	1.08e-02	2.84e-03	9.38e-04	0.00e+00	1.43e-02	3.16e-04	1.20e-03	5.19e-03	8.14e-03	1.40e-01		3.40e-01
3	812675	802008	129713	139708	3.70e-03	1.42e-01	1.30e-02	2.42e-03	1.11e-03	0.00e+00	1.68e-02	3.33e-04	1.36e-03	5.12e-03	7.45e-03	1.93e-01		5.33e-01
4	802239	808723	137961	148628	3.64e-03	1.32e-01	1.28e-02	2.62e-03	1.57e-03	0.00e+00	1.82e-02	2.87e-04	1.46e-03	5.71e-03	7.40e-03	1.86e-01		7.19e-01
5	809656	812971	149202	142718	3.71e-03	1.30e-01	1.29e-02	2.85e-03	1.93e-03	0.00e+00	1.80e-02	3.72e-04	1.43e-03	5.38e-03	7.57e-03	1.84e-01		9.02e-01
6	809102	811234	142591	139276	3.72e-03	1.34e-01	1.32e-02	3.02e-03	1.44e-03	0.00e+00	1.77e-02	3.44e-04	1.42e-03	5.26e-03	7.42e-03	1.88e-01		1.09e+00
7	813223	812707	143951	141819	3.69e-03	1.34e-01	1.31e-02	2.92e-03	1.50e-03	0.00e+00	1.79e-02	4.67e-04	1.38e-03	5.15e-03	7.55e-03	1.87e-01		1.28e+00
8	808766	810927	140636	141152	3.71e-03	1.37e-01	1.33e-02	2.76e-03	2.15e-03	0.00e+00	1.87e-02	3.77e-04	1.52e-03	5.16e-03	7.49e-03	1.92e-01		1.47e+00
9	816116	810719	145899	143738	3.72e-03	1.34e-01	1.32e-02	2.81e-03	1.94e-03	0.00e+00	1.82e-02	3.13e-04	1.46e-03	5.12e-03	7.55e-03	1.88e-01		1.66e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 2.0547 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 2.0547 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  2.054743  0.000000
];

