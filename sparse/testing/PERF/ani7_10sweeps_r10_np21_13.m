% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_21 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	812515	797494	141586	141586	3.68e-03	5.71e-02	1.13e-02	7.62e-03	1.11e-03	0.00e+00	1.21e-02	3.64e-04	1.23e-03	7.85e-03	7.07e-03	1.09e-01		1.09e-01
1	802274	813280	135703	150724	3.69e-03	7.06e-02	1.05e-02	3.36e-03	1.70e-03	0.00e+00	1.45e-02	2.60e-04	1.40e-03	6.40e-03	9.02e-03	1.21e-01		2.31e-01
2	814411	810975	146750	135744	3.77e-03	1.12e-01	1.38e-02	2.90e-03	1.05e-03	0.00e+00	1.78e-02	2.02e-04	1.45e-03	5.80e-03	9.91e-03	1.68e-01		3.99e-01
3	822354	804787	135419	138855	3.79e-03	1.60e-01	1.57e-02	2.66e-03	1.25e-03	0.00e+00	2.06e-02	3.06e-04	1.61e-03	5.96e-03	9.30e-03	2.21e-01		6.20e-01
4	813706	811668	128282	145849	3.76e-03	1.54e-01	1.59e-02	3.25e-03	1.86e-03	0.00e+00	2.27e-02	4.66e-04	1.69e-03	6.09e-03	9.10e-03	2.19e-01		8.39e-01
5	807487	807964	137735	139773	3.76e-03	1.49e-01	1.60e-02	3.17e-03	1.76e-03	0.00e+00	2.28e-02	3.92e-04	1.67e-03	6.23e-03	9.07e-03	2.14e-01		1.05e+00
6	809896	809231	144760	144283	3.76e-03	1.51e-01	1.60e-02	3.38e-03	2.09e-03	0.00e+00	2.32e-02	3.39e-04	1.75e-03	5.96e-03	9.14e-03	2.16e-01		1.27e+00
7	808090	811925	143157	143822	3.78e-03	1.52e-01	1.61e-02	3.37e-03	1.78e-03	0.00e+00	2.27e-02	4.11e-04	1.69e-03	5.76e-03	9.22e-03	2.17e-01		1.49e+00
8	820202	812022	145769	141934	3.79e-03	1.55e-01	1.62e-02	3.57e-03	1.58e-03	0.00e+00	2.19e-02	2.61e-04	1.58e-03	6.09e-03	9.45e-03	2.20e-01		1.71e+00
9	817133	816074	134463	142643	3.84e-03	1.63e-01	1.65e-02	2.93e-03	2.17e-03	0.00e+00	2.28e-02	4.95e-04	1.70e-03	5.77e-03	9.35e-03	2.28e-01		1.93e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 2.3383 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 2.3383 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  2.338334  0.000000
];

