% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_232 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	816314	828933	141586	141586	1.05e-02	4.42e-02	9.06e-03	2.04e-02	2.67e-03	0.00e+00	2.33e-03	4.77e-04	3.87e-04	1.20e-02	1.83e-03	1.04e-01		1.04e-01
1	800066	821288	131904	119285	1.08e-02	3.77e-02	3.51e-03	2.88e-03	2.26e-03	0.00e+00	3.43e-03	4.57e-04	3.83e-04	7.07e-03	1.95e-03	7.05e-02		1.74e-01
2	804344	796742	148958	127736	1.06e-02	5.11e-02	3.79e-03	2.52e-03	1.24e-03	0.00e+00	3.25e-03	6.77e-04	3.93e-04	4.15e-03	2.00e-03	7.97e-02		2.54e-01
3	799047	805654	145486	153088	1.03e-02	5.10e-02	3.97e-03	2.41e-03	3.17e-04	0.00e+00	3.57e-03	6.60e-04	4.16e-04	4.26e-03	3.40e-03	8.04e-02		3.34e-01
4	813554	800658	151589	144982	1.03e-02	5.54e-02	4.88e-03	2.67e-03	4.10e-04	0.00e+00	3.87e-03	4.58e-04	4.19e-04	4.21e-03	2.24e-03	8.49e-02		4.19e-01
5	805548	809954	137887	150783	1.03e-02	5.08e-02	4.49e-03	2.55e-03	3.92e-04	0.00e+00	3.84e-03	8.86e-04	4.19e-04	5.12e-03	2.22e-03	8.10e-02		5.00e-01
6	811488	804098	146699	142293	1.08e-02	5.66e-02	4.56e-03	3.24e-03	4.14e-04	0.00e+00	3.79e-03	7.23e-04	4.31e-04	4.56e-03	2.19e-03	8.72e-02		5.88e-01
7	809866	811178	141565	148955	1.03e-02	5.08e-02	4.55e-03	1.96e-03	3.93e-04	0.00e+00	3.82e-03	5.70e-04	4.17e-04	4.67e-03	2.25e-03	7.98e-02		6.67e-01
8	813467	814417	143993	142681	1.05e-02	5.69e-02	4.65e-03	1.80e-03	1.46e-03	0.00e+00	3.84e-03	6.85e-04	4.29e-04	4.35e-03	2.29e-03	8.68e-02		7.54e-01
9	813404	815738	141198	140248	1.05e-02	5.29e-02	4.68e-03	2.43e-03	1.81e-03	0.00e+00	3.83e-03	7.16e-04	4.19e-04	4.27e-03	2.28e-03	8.38e-02		8.38e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.5905 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.5905 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.590522  0.000000
];

