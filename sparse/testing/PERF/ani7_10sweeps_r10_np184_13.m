% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_184 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	811057	815846	141586	141586	8.51e-03	3.59e-02	7.53e-03	1.46e-02	1.44e-03	0.00e+00	2.15e-03	7.31e-04	3.47e-04	9.95e-03	1.82e-03	8.30e-02		8.30e-02
1	804504	812878	137161	132372	8.62e-03	3.15e-02	3.39e-03	4.20e-03	1.13e-03	0.00e+00	2.41e-03	4.19e-04	3.77e-04	5.78e-03	2.05e-03	5.99e-02		1.43e-01
2	794202	808603	144520	136146	8.53e-03	4.49e-02	3.93e-03	3.07e-03	6.20e-04	0.00e+00	2.87e-03	5.04e-04	4.37e-04	3.85e-03	2.18e-03	7.09e-02		2.14e-01
3	798971	813575	155628	141227	8.46e-03	4.74e-02	4.27e-03	3.07e-03	3.63e-04	0.00e+00	3.36e-03	3.99e-04	4.89e-04	3.88e-03	2.20e-03	7.39e-02		2.88e-01
4	807641	813374	151665	137061	8.52e-03	5.12e-02	4.43e-03	2.82e-03	5.53e-04	0.00e+00	3.50e-03	5.02e-04	4.88e-04	3.95e-03	2.23e-03	7.82e-02		3.66e-01
5	813603	817166	143800	138067	8.53e-03	4.69e-02	4.52e-03	2.51e-03	4.23e-04	0.00e+00	3.49e-03	3.64e-04	4.59e-04	4.22e-03	2.29e-03	7.37e-02		4.39e-01
6	811690	803069	138644	135081	8.60e-03	5.16e-02	4.71e-03	1.57e-03	4.37e-04	0.00e+00	3.52e-03	7.40e-04	4.81e-04	4.03e-03	2.19e-03	7.79e-02		5.17e-01
7	813647	813163	141363	149984	8.44e-03	4.65e-02	4.61e-03	1.83e-03	4.93e-04	0.00e+00	3.45e-03	6.52e-04	4.90e-04	4.08e-03	2.30e-03	7.28e-02		5.90e-01
8	810651	808941	140212	140696	8.53e-03	5.16e-02	4.73e-03	3.95e-03	1.33e-03	0.00e+00	3.47e-03	8.17e-04	4.60e-04	3.91e-03	2.21e-03	8.11e-02		6.71e-01
9	811496	812162	144014	145724	8.51e-03	4.69e-02	4.66e-03	3.43e-03	1.02e-03	0.00e+00	3.43e-03	5.18e-04	4.54e-04	3.83e-03	2.22e-03	7.49e-02		7.46e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.3950 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.3950 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.394994  0.000000
];

