% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_297 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	818364	799607	141586	141586	1.55e-02	7.23e-02	1.82e-02	2.11e-02	2.32e-03	0.00e+00	3.13e-03	7.02e-04	1.62e-03	3.06e-02	2.38e-03	1.68e-01		1.68e-01
1	811788	809890	129854	148611	1.59e-02	4.42e-02	4.11e-03	5.51e-03	3.31e-03	0.00e+00	3.42e-03	7.69e-04	1.59e-03	1.39e-02	2.54e-03	9.53e-02		2.63e-01
2	804809	797432	137236	139134	1.56e-02	6.09e-02	4.58e-03	5.71e-03	1.23e-03	0.00e+00	3.32e-03	9.01e-04	1.70e-03	3.77e-03	2.75e-03	1.01e-01		3.64e-01
3	806269	807039	145021	152398	1.55e-02	5.15e-02	4.69e-03	4.36e-03	1.09e-03	0.00e+00	3.78e-03	8.70e-04	1.74e-03	3.74e-03	2.66e-03	8.99e-02		4.54e-01
4	815157	813988	144367	143597	1.53e-02	6.51e-02	4.79e-03	6.50e-03	1.16e-03	0.00e+00	3.93e-03	7.73e-04	1.60e-03	5.03e-03	2.69e-03	1.07e-01		5.60e-01
5	807767	804625	136284	137453	1.47e-02	5.70e-02	5.06e-03	5.17e-03	1.15e-03	0.00e+00	3.96e-03	7.87e-04	1.68e-03	4.84e-03	2.57e-03	9.70e-02		6.57e-01
6	809357	809984	144480	147622	1.53e-02	6.85e-02	4.93e-03	4.61e-03	1.16e-03	0.00e+00	3.59e-03	9.32e-04	1.65e-03	4.05e-03	2.69e-03	1.07e-01		7.65e-01
7	814660	810430	143696	143069	1.45e-02	5.30e-02	5.08e-03	5.11e-03	1.23e-03	0.00e+00	4.05e-03	7.52e-04	1.61e-03	3.83e-03	2.68e-03	9.19e-02		8.57e-01
8	810622	810808	139199	143429	1.45e-02	6.84e-02	5.01e-03	5.11e-03	1.17e-03	0.00e+00	3.41e-03	7.87e-04	1.61e-03	4.02e-03	2.63e-03	1.07e-01		9.63e-01
9	811408	814172	144043	143857	1.47e-02	5.32e-02	5.07e-03	4.50e-03	2.18e-03	0.00e+00	4.10e-03	7.59e-04	1.66e-03	3.79e-03	2.72e-03	9.27e-02		1.06e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.5772 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.5772 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.577249  0.000000
];

