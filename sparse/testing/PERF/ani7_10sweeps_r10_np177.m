% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_177 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	821707	799635	141586	141586	9.24e-03	5.58e-02	1.31e-02	2.02e-02	2.75e-03	0.00e+00	3.10e-03	5.68e-04	3.91e-04	1.96e-02	1.92e-03	1.27e-01		1.27e-01
1	809027	819867	126511	148583	9.25e-03	4.28e-02	3.62e-03	3.92e-03	1.94e-03	0.00e+00	3.75e-03	4.05e-04	3.83e-04	1.09e-02	2.56e-03	7.95e-02		2.06e-01
2	795668	783954	139997	129157	9.39e-03	6.62e-02	4.43e-03	3.80e-03	5.14e-04	0.00e+00	4.24e-03	4.80e-04	5.36e-04	4.11e-03	2.45e-03	9.62e-02		3.02e-01
3	822871	791702	154162	165876	9.00e-03	6.01e-02	4.56e-03	1.84e-03	5.84e-04	0.00e+00	4.60e-03	4.39e-04	5.38e-04	4.15e-03	2.49e-03	8.83e-02		3.91e-01
4	789834	807658	127765	158934	9.09e-03	7.13e-02	5.49e-03	3.64e-03	6.53e-04	0.00e+00	4.95e-03	6.56e-04	5.98e-04	4.45e-03	2.40e-03	1.03e-01		4.94e-01
5	810286	815372	161607	143783	9.28e-03	6.03e-02	4.70e-03	3.28e-03	5.96e-04	0.00e+00	4.94e-03	6.70e-04	5.12e-04	4.96e-03	2.56e-03	9.18e-02		5.86e-01
6	811891	814152	141961	136875	9.37e-03	7.17e-02	5.11e-03	2.13e-03	7.77e-04	0.00e+00	5.06e-03	7.18e-04	5.06e-04	4.45e-03	2.54e-03	1.02e-01		6.88e-01
7	811847	811638	141162	138901	9.38e-03	6.53e-02	5.01e-03	2.06e-03	6.22e-04	0.00e+00	5.02e-03	7.50e-04	5.07e-04	4.07e-03	2.51e-03	9.52e-02		7.83e-01
8	811889	811266	142012	142221	9.34e-03	7.13e-02	4.99e-03	2.34e-03	5.44e-04	0.00e+00	4.87e-03	6.92e-04	4.97e-04	4.37e-03	2.49e-03	1.01e-01		8.85e-01
9	812386	813604	142776	143399	9.33e-03	6.41e-02	5.00e-03	1.85e-03	1.58e-03	0.00e+00	5.05e-03	6.96e-04	5.10e-04	4.21e-03	2.49e-03	9.48e-02		9.80e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.6232 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.6232 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.623161  0.000000
];

