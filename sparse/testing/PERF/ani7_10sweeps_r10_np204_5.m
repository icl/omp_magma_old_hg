% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_204 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	814747	806368	141586	141586	8.86e-03	3.84e-02	7.87e-03	1.72e-02	9.11e-04	0.00e+00	2.65e-03	4.57e-04	3.25e-04	1.23e-02	1.69e-03	9.06e-02		9.06e-02
1	814463	804180	133471	141850	8.86e-03	3.40e-02	3.23e-03	4.60e-03	1.68e-03	0.00e+00	3.33e-03	4.15e-04	3.67e-04	6.90e-03	2.23e-03	6.56e-02		1.56e-01
2	811237	809699	134561	144844	8.87e-03	5.44e-02	3.80e-03	3.18e-03	3.72e-04	0.00e+00	3.71e-03	5.96e-04	4.46e-04	4.06e-03	2.25e-03	8.16e-02		2.38e-01
3	801991	812090	138593	140131	8.94e-03	5.39e-02	4.60e-03	2.74e-03	4.63e-04	0.00e+00	4.10e-03	6.51e-04	4.54e-04	4.08e-03	2.17e-03	8.21e-02		3.20e-01
4	820039	814790	148645	138546	8.98e-03	5.47e-02	4.26e-03	2.10e-03	5.31e-04	0.00e+00	4.29e-03	5.24e-04	4.57e-04	4.44e-03	2.16e-03	8.24e-02		4.02e-01
5	806119	812918	131402	136651	8.97e-03	5.12e-02	4.36e-03	3.30e-03	5.93e-04	0.00e+00	4.40e-03	6.14e-04	5.01e-04	4.95e-03	2.15e-03	8.10e-02		4.83e-01
6	815998	811787	146128	139329	8.98e-03	5.50e-02	5.13e-03	2.50e-03	4.47e-04	0.00e+00	4.48e-03	6.22e-04	4.65e-04	4.69e-03	2.61e-03	8.49e-02		5.68e-01
7	814026	810562	137055	141266	1.15e-02	5.35e-02	5.08e-03	2.72e-03	4.73e-04	0.00e+00	4.36e-03	6.01e-04	5.06e-04	4.05e-03	2.37e-03	8.51e-02		6.54e-01
8	810860	815389	139833	143297	1.08e-02	5.69e-02	5.02e-03	2.93e-03	9.65e-04	0.00e+00	4.32e-03	6.16e-04	4.63e-04	4.25e-03	2.38e-03	8.87e-02		7.42e-01
9	813472	813263	143805	139276	1.09e-02	5.29e-02	5.07e-03	2.29e-03	1.13e-03	0.00e+00	4.46e-03	5.79e-04	4.85e-04	4.07e-03	2.37e-03	8.42e-02		8.26e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.4245 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.4245 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.424510  0.000000
];

