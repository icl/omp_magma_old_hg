% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_121 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	816921	816367	141586	141586	5.97e-03	2.76e-02	5.78e-03	1.06e-02	9.41e-04	0.00e+00	2.90e-03	3.32e-04	3.66e-04	7.35e-03	1.86e-03	6.37e-02		6.37e-02
1	792312	800208	131297	131851	5.95e-03	2.86e-02	3.29e-03	2.53e-03	4.13e-04	0.00e+00	3.10e-03	3.09e-04	4.14e-04	4.24e-03	2.14e-03	5.10e-02		1.15e-01
2	792576	819257	156712	148816	5.85e-03	3.84e-02	3.76e-03	1.60e-03	8.39e-04	0.00e+00	4.04e-03	3.01e-04	4.47e-04	3.37e-03	2.36e-03	6.10e-02		1.76e-01
3	799194	806846	157254	130573	5.95e-03	4.57e-02	4.25e-03	2.19e-03	3.69e-04	0.00e+00	4.12e-03	2.45e-04	4.86e-04	3.01e-03	2.31e-03	6.86e-02		2.44e-01
4	814728	812958	151442	143790	5.88e-03	4.52e-02	4.42e-03	1.46e-03	4.98e-04	0.00e+00	4.76e-03	5.76e-04	4.63e-04	3.04e-03	2.40e-03	6.87e-02		3.13e-01
5	809252	814404	136713	138483	5.94e-03	4.74e-02	4.65e-03	1.77e-03	3.75e-04	0.00e+00	4.41e-03	4.23e-04	4.55e-04	3.28e-03	2.41e-03	7.11e-02		3.84e-01
6	814785	809411	142995	137843	5.93e-03	5.00e-02	4.67e-03	1.80e-03	4.12e-04	0.00e+00	4.75e-03	4.72e-04	4.98e-04	3.06e-03	2.41e-03	7.40e-02		4.58e-01
7	810101	809879	138268	143642	5.91e-03	4.92e-02	4.72e-03	1.63e-03	4.03e-04	0.00e+00	4.93e-03	4.45e-04	4.61e-04	3.21e-03	2.42e-03	7.34e-02		5.32e-01
8	812153	811703	143758	143980	5.90e-03	5.02e-02	4.65e-03	1.67e-03	1.22e-03	0.00e+00	4.66e-03	5.32e-04	4.56e-04	3.05e-03	2.42e-03	7.48e-02		6.06e-01
9	812933	814466	142512	142962	5.92e-03	4.90e-02	4.66e-03	1.55e-03	9.16e-04	0.00e+00	5.35e-03	4.39e-04	4.71e-04	3.01e-03	2.42e-03	7.38e-02		6.80e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.1601 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.1601 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.160060  0.000000
];

