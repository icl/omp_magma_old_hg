% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_252 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	820541	805663	141586	141586	1.12e-02	4.38e-02	9.35e-03	2.04e-02	1.84e-03	0.00e+00	2.45e-03	5.15e-04	3.81e-04	1.34e-02	1.66e-03	1.05e-01		1.05e-01
1	814562	790586	127677	142555	1.12e-02	3.56e-02	3.20e-03	7.50e-03	1.61e-03	0.00e+00	2.99e-03	3.99e-04	4.04e-04	6.89e-03	1.87e-03	7.16e-02		1.77e-01
2	802367	793922	134462	158438	1.09e-02	5.11e-02	3.59e-03	2.61e-03	8.25e-04	0.00e+00	3.22e-03	5.19e-04	4.06e-04	4.59e-03	2.04e-03	7.98e-02		2.56e-01
3	823920	813141	147463	155908	1.10e-02	4.88e-02	3.95e-03	2.99e-03	3.60e-04	0.00e+00	3.82e-03	4.73e-04	3.73e-04	4.26e-03	2.14e-03	7.82e-02		3.35e-01
4	806332	809752	126716	137495	1.13e-02	5.99e-02	5.06e-03	2.93e-03	3.51e-04	0.00e+00	3.72e-03	6.39e-04	4.13e-04	4.23e-03	2.02e-03	9.06e-02		4.25e-01
5	810330	809381	145109	141689	2.24e-02	4.90e-02	4.12e-03	3.44e-03	3.81e-04	0.00e+00	3.95e-03	5.07e-04	4.27e-04	4.33e-03	2.06e-03	9.06e-02		5.16e-01
6	811486	811079	141917	142866	1.13e-02	5.52e-02	4.19e-03	1.74e-03	3.56e-04	0.00e+00	3.86e-03	6.73e-04	3.94e-04	4.30e-03	2.04e-03	8.41e-02		6.00e-01
7	809974	811907	141567	141974	1.13e-02	4.99e-02	4.23e-03	2.04e-03	3.29e-04	0.00e+00	3.87e-03	6.42e-04	3.84e-04	4.46e-03	2.06e-03	7.92e-02		6.79e-01
8	811927	813348	143885	141952	1.11e-02	5.56e-02	4.25e-03	1.80e-03	1.78e-03	0.00e+00	3.99e-03	8.78e-04	4.24e-04	4.14e-03	2.09e-03	8.60e-02		7.65e-01
9	814638	814618	142738	141317	1.11e-02	5.02e-02	4.30e-03	2.01e-03	1.15e-03	0.00e+00	3.89e-03	6.20e-04	4.09e-04	4.26e-03	2.09e-03	8.00e-02		8.45e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.5923 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.5923 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.592313  0.000000
];

