% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_149 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	823979	802892	141586	141586	7.69e-03	3.50e-02	7.78e-03	1.44e-02	7.90e-04	0.00e+00	2.52e-03	4.48e-04	4.02e-04	9.55e-03	2.13e-03	8.07e-02		8.07e-02
1	809629	823847	124239	145326	7.63e-03	2.95e-02	4.10e-03	4.93e-03	9.50e-04	0.00e+00	3.01e-03	5.41e-04	3.82e-04	5.08e-03	2.29e-03	5.84e-02		1.39e-01
2	802205	816580	139395	125177	7.80e-03	4.71e-02	4.44e-03	3.46e-03	3.25e-04	0.00e+00	3.60e-03	6.58e-04	4.16e-04	4.00e-03	2.29e-03	7.41e-02		2.13e-01
3	805677	804193	147625	133250	7.71e-03	4.99e-02	4.64e-03	2.94e-03	5.49e-04	0.00e+00	4.07e-03	6.03e-04	5.01e-04	4.01e-03	2.29e-03	7.72e-02		2.90e-01
4	804757	811989	144959	146443	7.63e-03	5.09e-02	4.90e-03	1.58e-03	4.97e-04	0.00e+00	4.25e-03	8.19e-04	4.68e-04	4.77e-03	2.34e-03	7.82e-02		3.69e-01
5	812725	815033	146684	139452	7.69e-03	4.62e-02	5.13e-03	1.69e-03	1.19e-03	0.00e+00	4.38e-03	9.15e-04	4.66e-04	4.57e-03	2.40e-03	7.47e-02		4.43e-01
6	807915	810486	139522	137214	7.74e-03	5.13e-02	6.12e-03	1.62e-03	3.68e-04	0.00e+00	4.11e-03	5.64e-04	4.48e-04	4.31e-03	2.31e-03	7.89e-02		5.22e-01
7	818524	814893	145138	142567	7.68e-03	4.68e-02	5.13e-03	2.05e-03	3.81e-04	0.00e+00	4.17e-03	5.37e-04	4.52e-04	4.12e-03	2.39e-03	7.38e-02		5.96e-01
8	811202	813931	135335	138966	7.72e-03	5.24e-02	5.32e-03	2.45e-03	1.09e-03	0.00e+00	4.20e-03	7.17e-04	4.52e-04	4.28e-03	2.33e-03	8.10e-02		6.77e-01
9	813122	813137	143463	140734	7.73e-03	4.76e-02	5.24e-03	1.68e-03	1.02e-03	0.00e+00	4.33e-03	7.36e-04	4.66e-04	4.01e-03	2.33e-03	7.51e-02		7.52e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.3896 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.3896 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.389558  0.000000
];

