% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_162 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	817915	812927	141586	141586	9.67e-03	5.29e-02	1.27e-02	1.79e-02	4.80e-03	0.00e+00	3.67e-03	4.14e-04	4.18e-04	1.70e-02	2.08e-03	1.22e-01		1.22e-01
1	797329	809863	130303	135291	9.68e-03	4.29e-02	3.96e-03	4.53e-03	3.65e-03	0.00e+00	4.07e-03	4.05e-04	4.09e-04	5.81e-03	2.72e-03	7.81e-02		2.00e-01
2	818430	807635	151695	139161	9.77e-03	6.53e-02	4.73e-03	3.91e-03	1.14e-03	0.00e+00	4.52e-03	4.78e-04	4.98e-04	4.06e-03	2.90e-03	9.73e-02		2.97e-01
3	813259	808055	131400	142195	9.71e-03	6.78e-02	5.79e-03	2.34e-03	5.81e-04	0.00e+00	5.02e-03	5.22e-04	5.76e-04	4.01e-03	2.85e-03	9.92e-02		3.96e-01
4	806344	814705	137377	142581	9.74e-03	7.34e-02	5.67e-03	2.09e-03	6.50e-04	0.00e+00	5.49e-03	5.23e-04	6.04e-04	4.15e-03	2.82e-03	1.05e-01		5.01e-01
5	808663	813979	145097	136736	9.86e-03	6.43e-02	5.37e-03	3.36e-03	7.06e-04	0.00e+00	5.45e-03	3.66e-04	5.87e-04	4.40e-03	2.81e-03	9.72e-02		5.99e-01
6	810427	806923	143584	138268	9.94e-03	7.25e-02	5.63e-03	2.92e-03	6.43e-04	0.00e+00	5.34e-03	5.36e-04	5.53e-04	4.36e-03	2.76e-03	1.05e-01		7.04e-01
7	805828	815431	142626	146130	9.70e-03	6.48e-02	5.45e-03	1.86e-03	6.26e-04	0.00e+00	5.40e-03	5.80e-04	5.80e-04	4.33e-03	2.76e-03	9.61e-02		8.00e-01
8	811272	810215	148031	138428	9.79e-03	7.10e-02	5.55e-03	2.67e-03	1.35e-03	0.00e+00	5.42e-03	8.39e-04	6.14e-04	4.15e-03	2.85e-03	1.04e-01		9.04e-01
9	813138	810143	143393	144450	9.74e-03	6.60e-02	5.51e-03	2.14e-03	1.45e-03	0.00e+00	5.35e-03	6.89e-04	5.75e-04	4.03e-03	2.72e-03	9.82e-02		1.00e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.6305 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.6305 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.630471  0.000000
];

