% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_42 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	808600	808885	141586	141586	3.28e-03	3.29e-02	5.91e-03	6.66e-03	8.89e-04	0.00e+00	5.72e-03	2.50e-04	6.53e-04	5.94e-03	3.35e-03	6.56e-02		6.56e-02
1	798150	807504	139618	139333	3.31e-03	3.88e-02	5.00e-03	2.25e-03	1.03e-03	0.00e+00	6.94e-03	2.10e-04	6.96e-04	4.35e-03	4.31e-03	6.69e-02		1.32e-01
2	808529	809680	150874	141520	3.32e-03	6.03e-02	6.50e-03	2.64e-03	7.62e-04	0.00e+00	8.50e-03	3.18e-04	7.12e-04	3.78e-03	4.72e-03	9.15e-02		2.24e-01
3	812026	809330	141301	140150	3.32e-03	8.72e-02	7.54e-03	1.92e-03	6.81e-04	0.00e+00	9.76e-03	2.36e-04	7.93e-04	3.78e-03	4.46e-03	1.20e-01		3.44e-01
4	803245	817033	138610	141306	3.32e-03	8.45e-02	7.54e-03	1.96e-03	1.10e-03	0.00e+00	1.07e-02	3.30e-04	9.11e-04	3.76e-03	4.35e-03	1.19e-01		4.62e-01
5	813135	810979	148196	134408	3.34e-03	8.19e-02	7.61e-03	1.86e-03	1.16e-03	0.00e+00	1.09e-02	3.37e-04	8.85e-04	4.35e-03	4.43e-03	1.17e-01		5.79e-01
6	815730	808524	139112	141268	3.32e-03	8.58e-02	7.71e-03	2.22e-03	9.64e-04	0.00e+00	1.08e-02	2.81e-04	8.79e-04	3.86e-03	4.43e-03	1.20e-01		6.99e-01
7	811933	810983	137323	144529	3.32e-03	8.67e-02	7.77e-03	3.05e-03	9.16e-04	0.00e+00	1.07e-02	2.50e-04	8.62e-04	4.01e-03	4.45e-03	1.22e-01		8.21e-01
8	813531	813565	141926	142876	3.31e-03	8.71e-02	7.78e-03	2.22e-03	1.61e-03	0.00e+00	1.08e-02	3.03e-04	9.00e-04	3.87e-03	4.43e-03	1.22e-01		9.44e-01
9	813521	810956	141134	141100	3.36e-03	8.64e-02	7.84e-03	1.89e-03	1.29e-03	0.00e+00	1.06e-02	2.86e-04	8.91e-04	3.77e-03	4.41e-03	1.21e-01		1.06e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.4366 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.4366 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.436597  0.000000
];

