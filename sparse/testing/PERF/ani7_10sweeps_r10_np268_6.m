% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_268 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	829858	808258	141586	141586	1.16e-02	4.48e-02	9.48e-03	2.19e-02	1.99e-03	0.00e+00	2.60e-03	4.41e-04	3.28e-04	1.29e-02	1.59e-03	1.08e-01		1.08e-01
1	810026	803272	118360	139960	1.16e-02	3.77e-02	3.05e-03	4.26e-03	1.71e-03	0.00e+00	3.29e-03	4.39e-04	3.49e-04	7.21e-03	1.75e-03	7.13e-02		1.79e-01
2	805192	805576	138998	145752	1.16e-02	5.09e-02	3.60e-03	2.13e-03	1.09e-03	0.00e+00	3.51e-03	5.83e-04	3.73e-04	4.12e-03	2.03e-03	7.99e-02		2.59e-01
3	807342	808387	144638	144254	1.16e-02	5.39e-02	4.06e-03	4.04e-03	3.14e-04	0.00e+00	3.74e-03	5.12e-04	3.98e-04	4.18e-03	2.07e-03	8.48e-02		3.44e-01
4	810706	806588	143294	142249	1.16e-02	5.91e-02	4.25e-03	2.24e-03	3.90e-04	0.00e+00	3.92e-03	6.17e-04	4.22e-04	4.26e-03	2.02e-03	8.89e-02		4.32e-01
5	808789	811747	140735	144853	1.16e-02	5.32e-02	4.16e-03	2.72e-03	4.09e-04	0.00e+00	3.98e-03	5.92e-04	4.02e-04	4.83e-03	2.03e-03	8.39e-02		5.16e-01
6	811427	810696	143458	140500	1.17e-02	5.79e-02	4.20e-03	2.04e-03	4.83e-04	0.00e+00	4.15e-03	4.37e-04	4.53e-04	4.25e-03	2.06e-03	8.77e-02		6.04e-01
7	810995	807011	141626	142357	1.16e-02	5.42e-02	4.24e-03	2.48e-03	4.03e-04	0.00e+00	5.71e-03	5.31e-04	4.49e-04	4.38e-03	2.04e-03	8.60e-02		6.90e-01
8	811331	812111	142864	146848	1.16e-02	5.83e-02	4.23e-03	1.99e-03	1.57e-03	0.00e+00	4.03e-03	5.87e-04	4.19e-04	4.25e-03	2.10e-03	8.91e-02		7.79e-01
9	813219	813701	143334	142554	1.16e-02	5.46e-02	4.29e-03	1.90e-03	1.69e-03	0.00e+00	4.06e-03	8.60e-04	4.12e-04	4.18e-03	2.07e-03	8.57e-02		8.65e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.6020 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.6020 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.601953  0.000000
];

