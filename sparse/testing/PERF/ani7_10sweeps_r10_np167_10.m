% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_167 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	818426	804354	141586	141586	8.17e-03	3.68e-02	7.46e-03	1.49e-02	2.73e-03	0.00e+00	2.38e-03	6.06e-04	3.71e-04	1.06e-02	1.99e-03	8.60e-02		8.60e-02
1	810179	809588	129792	143864	8.14e-03	3.30e-02	3.73e-03	3.42e-03	2.42e-03	0.00e+00	2.69e-03	4.03e-04	4.11e-04	6.19e-03	2.14e-03	6.25e-02		1.49e-01
2	799859	802392	138845	139436	8.16e-03	4.63e-02	4.12e-03	3.05e-03	2.73e-04	0.00e+00	3.25e-03	4.28e-04	4.07e-04	3.98e-03	2.23e-03	7.22e-02		2.21e-01
3	800972	815292	149971	147438	8.09e-03	4.56e-02	4.33e-03	2.06e-03	3.59e-04	0.00e+00	3.58e-03	4.65e-04	4.59e-04	3.98e-03	2.45e-03	7.14e-02		2.92e-01
4	803614	806406	149664	135344	8.17e-03	4.98e-02	4.76e-03	2.28e-03	4.25e-04	0.00e+00	3.79e-03	6.02e-04	4.51e-04	4.28e-03	2.38e-03	7.69e-02		3.69e-01
5	809443	806737	147827	145035	8.11e-03	4.60e-02	4.97e-03	3.66e-03	3.85e-04	0.00e+00	3.82e-03	4.70e-04	4.36e-04	4.33e-03	2.47e-03	7.46e-02		4.44e-01
6	811725	813308	142804	145510	8.13e-03	4.99e-02	5.02e-03	2.23e-03	4.49e-04	0.00e+00	3.86e-03	5.24e-04	4.80e-04	4.46e-03	2.40e-03	7.74e-02		5.21e-01
7	810772	812452	141328	139745	8.21e-03	4.78e-02	5.15e-03	2.53e-03	4.18e-04	0.00e+00	3.72e-03	5.94e-04	4.39e-04	3.88e-03	2.43e-03	7.52e-02		5.96e-01
8	811116	810919	143087	141407	8.17e-03	5.18e-02	5.11e-03	1.92e-03	3.77e-04	0.00e+00	3.76e-03	6.08e-04	4.48e-04	4.00e-03	2.40e-03	7.86e-02		6.75e-01
9	813954	815607	143549	143746	8.17e-03	4.75e-02	5.10e-03	1.96e-03	1.34e-03	0.00e+00	3.91e-03	6.36e-04	4.74e-04	3.96e-03	2.42e-03	7.55e-02		7.50e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.3880 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.3880 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.387980  0.000000
];

