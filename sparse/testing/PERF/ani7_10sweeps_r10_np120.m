% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_120 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	814170	811553	141586	141586	6.60e-03	3.97e-02	1.01e-02	1.31e-02	5.20e-03	0.00e+00	3.22e-03	3.94e-04	4.53e-04	1.40e-02	2.15e-03	9.50e-02		9.50e-02
1	804287	807436	134048	136665	6.65e-03	3.39e-02	3.66e-03	3.23e-03	3.64e-03	0.00e+00	3.78e-03	2.93e-04	3.79e-04	4.50e-03	2.53e-03	6.25e-02		1.58e-01
2	814526	821922	144737	141588	6.67e-03	5.26e-02	4.22e-03	2.19e-03	1.16e-03	0.00e+00	4.42e-03	3.33e-04	4.74e-04	3.14e-03	2.74e-03	7.79e-02		2.35e-01
3	803189	802620	135304	127908	6.81e-03	6.11e-02	5.08e-03	2.27e-03	4.38e-04	0.00e+00	4.95e-03	3.41e-04	5.02e-04	3.20e-03	2.55e-03	8.73e-02		3.23e-01
4	807604	807994	147447	148016	6.31e-03	5.83e-02	4.65e-03	1.98e-03	7.69e-04	0.00e+00	5.58e-03	5.07e-04	5.82e-04	3.26e-03	2.47e-03	8.44e-02		4.07e-01
5	812169	808973	143837	143447	6.66e-03	5.57e-02	4.64e-03	1.86e-03	7.50e-04	0.00e+00	5.41e-03	4.33e-04	5.24e-04	3.86e-03	2.52e-03	8.23e-02		4.89e-01
6	807148	809257	140078	143274	6.38e-03	5.94e-02	4.71e-03	2.35e-03	5.67e-04	0.00e+00	5.40e-03	4.38e-04	5.47e-04	3.47e-03	2.49e-03	8.58e-02		5.75e-01
7	809985	812240	145905	143796	6.56e-03	5.59e-02	4.94e-03	1.46e-03	5.05e-04	0.00e+00	5.23e-03	5.14e-04	5.04e-04	3.21e-03	2.55e-03	8.14e-02		6.57e-01
8	809892	809720	143874	141619	6.68e-03	5.94e-02	4.80e-03	1.34e-03	5.05e-04	0.00e+00	5.16e-03	4.55e-04	5.08e-04	3.38e-03	2.48e-03	8.48e-02		7.41e-01
9	813635	811394	144773	144945	6.46e-03	5.60e-02	4.79e-03	1.90e-03	1.46e-03	0.00e+00	5.49e-03	4.10e-04	5.29e-04	3.13e-03	2.54e-03	8.27e-02		8.24e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.3475 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.3475 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.347452  0.000000
];

