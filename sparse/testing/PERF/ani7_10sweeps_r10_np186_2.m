% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_186 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	818040	807373	141586	141586	8.54e-03	3.64e-02	7.52e-03	1.58e-02	2.09e-03	0.00e+00	2.12e-03	5.83e-04	3.43e-04	9.61e-03	1.80e-03	8.48e-02		8.48e-02
1	796391	816155	130178	140845	8.56e-03	3.28e-02	3.38e-03	3.86e-03	1.75e-03	0.00e+00	2.43e-03	4.40e-04	3.70e-04	6.00e-03	2.02e-03	6.16e-02		1.46e-01
2	804351	802576	152633	132869	8.64e-03	4.48e-02	3.93e-03	3.51e-03	9.58e-04	0.00e+00	2.91e-03	4.68e-04	4.26e-04	3.83e-03	2.18e-03	7.17e-02		2.18e-01
3	815284	806349	145479	147254	8.46e-03	4.83e-02	4.25e-03	3.18e-03	3.81e-04	0.00e+00	3.30e-03	4.43e-04	4.59e-04	3.89e-03	2.19e-03	7.49e-02		2.93e-01
4	820000	813880	135352	144287	8.50e-03	5.09e-02	4.55e-03	3.00e-03	4.55e-04	0.00e+00	3.42e-03	7.34e-04	4.23e-04	3.93e-03	2.19e-03	7.81e-02		3.71e-01
5	812361	807021	131441	137561	8.57e-03	4.74e-02	4.63e-03	2.82e-03	3.71e-04	0.00e+00	3.46e-03	6.62e-04	4.58e-04	4.24e-03	2.20e-03	7.48e-02		4.46e-01
6	810733	810242	139886	145226	8.54e-03	5.11e-02	4.56e-03	2.73e-03	3.48e-04	0.00e+00	3.34e-03	4.92e-04	4.37e-04	4.09e-03	2.20e-03	7.78e-02		5.24e-01
7	812599	810459	142320	142811	8.57e-03	4.70e-02	4.61e-03	2.34e-03	4.00e-04	0.00e+00	3.43e-03	4.18e-04	4.15e-04	4.12e-03	2.23e-03	7.36e-02		5.97e-01
8	812818	815505	141260	143400	8.52e-03	5.03e-02	4.63e-03	1.84e-03	1.25e-03	0.00e+00	3.46e-03	5.24e-04	4.54e-04	3.91e-03	2.26e-03	7.72e-02		6.74e-01
9	811333	815806	141847	139160	8.61e-03	4.84e-02	4.70e-03	2.11e-03	1.14e-03	0.00e+00	3.52e-03	5.46e-04	4.53e-04	3.92e-03	2.30e-03	7.57e-02		7.50e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.3839 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.3839 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.383889  0.000000
];

