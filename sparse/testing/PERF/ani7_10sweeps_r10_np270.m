% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_270 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	820549	798577	141586	141586	1.20e-02	7.41e-02	1.85e-02	2.79e-02	1.94e-03	0.00e+00	2.67e-03	5.24e-04	3.38e-04	2.44e-02	1.62e-03	1.64e-01		1.64e-01
1	803467	804806	127669	149641	1.19e-02	5.28e-02	3.08e-03	3.82e-03	1.56e-03	0.00e+00	3.20e-03	5.30e-04	3.45e-04	1.36e-02	2.03e-03	9.30e-02		2.57e-01
2	802497	802930	145557	144218	1.20e-02	7.26e-02	3.64e-03	2.11e-03	1.30e-03	0.00e+00	3.38e-03	7.70e-04	4.26e-04	4.34e-03	2.09e-03	1.03e-01		3.60e-01
3	810280	809745	147333	146900	1.20e-02	6.77e-02	4.07e-03	2.40e-03	5.08e-04	0.00e+00	3.65e-03	5.13e-04	4.33e-04	4.42e-03	2.13e-03	9.79e-02		4.58e-01
4	804278	806288	140356	140891	1.21e-02	7.87e-02	5.19e-03	2.09e-03	6.44e-04	0.00e+00	3.98e-03	6.20e-04	5.02e-04	4.32e-03	2.03e-03	1.10e-01		5.68e-01
5	811262	809440	147163	145153	1.21e-02	6.50e-02	4.06e-03	3.10e-03	5.75e-04	0.00e+00	4.03e-03	4.39e-04	4.67e-04	5.32e-03	2.04e-03	9.71e-02		6.65e-01
6	810859	811088	140985	142807	1.22e-02	7.83e-02	4.20e-03	2.65e-03	5.01e-04	0.00e+00	4.01e-03	7.11e-04	4.30e-04	4.70e-03	2.09e-03	1.10e-01		7.75e-01
7	812436	809560	142194	141965	1.22e-02	6.73e-02	4.24e-03	3.17e-03	5.59e-04	0.00e+00	4.09e-03	7.55e-04	4.30e-04	5.00e-03	2.12e-03	9.99e-02		8.74e-01
8	809552	812648	141423	144299	1.21e-02	7.96e-02	4.30e-03	2.30e-03	2.14e-03	0.00e+00	4.04e-03	8.56e-04	4.44e-04	4.58e-03	2.10e-03	1.13e-01		9.87e-01
9	813122	816233	145113	142017	1.21e-02	6.69e-02	4.22e-03	2.57e-03	2.01e-03	0.00e+00	5.13e-03	6.20e-04	4.33e-04	4.30e-03	2.09e-03	1.00e-01		1.09e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.8366 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.8366 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.836650  0.000000
];

