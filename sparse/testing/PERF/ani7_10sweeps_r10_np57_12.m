% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_57 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	818025	812255	141586	141586	3.25e-03	2.76e-02	4.89e-03	6.78e-03	7.53e-04	0.00e+00	4.23e-03	2.59e-04	4.62e-04	5.77e-03	2.50e-03	5.65e-02		5.65e-02
1	816596	812491	130193	135963	3.27e-03	3.15e-02	3.75e-03	3.49e-03	8.44e-04	0.00e+00	5.36e-03	1.90e-04	4.90e-04	3.99e-03	3.40e-03	5.63e-02		1.13e-01
2	810402	806525	132428	136533	3.29e-03	5.25e-02	4.89e-03	1.25e-03	5.61e-04	0.00e+00	6.36e-03	3.05e-04	5.80e-04	3.69e-03	3.56e-03	7.70e-02		1.90e-01
3	813182	818831	139428	143305	3.26e-03	6.85e-02	5.57e-03	1.74e-03	5.75e-04	0.00e+00	7.33e-03	2.70e-04	6.80e-04	3.38e-03	3.58e-03	9.49e-02		2.85e-01
4	800229	803244	137454	131805	3.32e-03	6.96e-02	5.73e-03	1.78e-03	9.29e-04	0.00e+00	8.07e-03	4.06e-04	7.73e-04	3.31e-03	3.18e-03	9.71e-02		3.82e-01
5	817935	810103	151212	148197	3.26e-03	6.07e-02	5.53e-03	2.69e-03	8.35e-04	0.00e+00	7.95e-03	3.35e-04	7.16e-04	3.59e-03	3.32e-03	8.90e-02		4.71e-01
6	812249	813885	134312	142144	3.29e-03	6.64e-02	8.36e-03	2.00e-03	7.67e-04	0.00e+00	9.22e-03	3.50e-04	6.84e-04	3.38e-03	4.27e-03	9.87e-02		5.69e-01
7	810848	812594	140804	139168	5.32e-03	6.52e-02	8.38e-03	1.66e-03	7.16e-04	0.00e+00	9.15e-03	3.23e-04	6.76e-04	3.61e-03	4.27e-03	9.93e-02		6.69e-01
8	811456	810964	143011	141265	5.34e-03	6.67e-02	8.34e-03	1.61e-03	1.34e-03	0.00e+00	9.16e-03	3.28e-04	7.10e-04	3.37e-03	3.90e-03	1.01e-01		7.69e-01
9	812503	814741	143209	143701	5.09e-03	6.56e-02	5.77e-03	2.12e-03	8.54e-04	0.00e+00	7.72e-03	4.82e-04	6.86e-04	3.33e-03	3.35e-03	9.50e-02		8.64e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.2451 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.2451 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.245145  0.000000
];

