% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_263 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	817926	808117	141586	141586	1.15e-02	4.47e-02	9.39e-03	2.16e-02	1.29e-03	0.00e+00	2.54e-03	5.48e-04	3.71e-04	1.39e-02	1.58e-03	1.07e-01		1.07e-01
1	811394	815560	130292	140101	1.15e-02	3.71e-02	3.09e-03	5.62e-03	1.30e-03	0.00e+00	3.04e-03	4.83e-04	4.31e-04	6.50e-03	1.85e-03	7.10e-02		1.78e-01
2	809150	810367	137630	133464	1.16e-02	5.12e-02	3.70e-03	1.73e-03	3.00e-04	0.00e+00	3.53e-03	6.10e-04	3.70e-04	4.36e-03	2.08e-03	7.96e-02		2.58e-01
3	803746	812069	140680	139463	1.15e-02	5.30e-02	4.25e-03	3.06e-03	4.61e-04	0.00e+00	3.71e-03	7.75e-04	4.06e-04	4.27e-03	2.15e-03	8.36e-02		3.42e-01
4	812731	806234	146890	138567	1.16e-02	5.67e-02	4.14e-03	2.60e-03	3.97e-04	0.00e+00	4.03e-03	7.51e-04	4.22e-04	5.37e-03	2.06e-03	8.81e-02		4.30e-01
5	810118	811205	138710	145207	1.14e-02	5.18e-02	4.19e-03	2.24e-03	2.03e-03	0.00e+00	3.95e-03	5.43e-04	4.57e-04	4.91e-03	2.13e-03	8.37e-02		5.13e-01
6	809314	807079	142129	141042	1.15e-02	5.66e-02	4.29e-03	1.62e-03	4.05e-04	0.00e+00	4.01e-03	7.52e-04	4.19e-04	4.87e-03	2.06e-03	8.66e-02		6.00e-01
7	812537	812241	143739	145974	1.15e-02	5.23e-02	4.25e-03	1.56e-03	4.49e-04	0.00e+00	3.96e-03	1.00e-03	4.15e-04	4.52e-03	2.13e-03	8.21e-02		6.82e-01
8	811563	812424	141322	141618	1.16e-02	5.80e-02	4.34e-03	1.68e-03	1.49e-03	0.00e+00	4.08e-03	4.96e-04	4.26e-04	4.41e-03	2.12e-03	8.87e-02		7.71e-01
9	812140	811752	143102	142241	1.15e-02	5.37e-02	4.25e-03	3.40e-03	1.50e-03	0.00e+00	3.90e-03	8.16e-04	4.07e-04	4.31e-03	2.10e-03	8.60e-02		8.57e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.5978 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.5978 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.597752  0.000000
];

