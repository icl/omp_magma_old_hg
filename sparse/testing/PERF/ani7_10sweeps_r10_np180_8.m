% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_180 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	816102	808153	141586	141586	8.41e-03	3.59e-02	7.42e-03	1.52e-02	1.79e-03	0.00e+00	2.22e-03	4.54e-04	3.88e-04	9.76e-03	1.84e-03	8.34e-02		8.34e-02
1	810393	795265	132116	140065	8.46e-03	3.16e-02	3.46e-03	4.12e-03	1.89e-03	0.00e+00	2.56e-03	3.55e-04	3.84e-04	5.95e-03	2.00e-03	6.08e-02		1.44e-01
2	817079	807006	138631	153759	8.27e-03	4.47e-02	5.40e-03	2.34e-03	9.36e-04	0.00e+00	2.91e-03	6.12e-04	3.81e-04	3.81e-03	2.24e-03	7.16e-02		2.16e-01
3	807081	817437	132751	142824	8.37e-03	4.54e-02	4.39e-03	1.93e-03	3.55e-04	0.00e+00	3.40e-03	3.99e-04	4.53e-04	3.86e-03	2.28e-03	7.08e-02		2.87e-01
4	819073	807004	143555	133199	8.47e-03	4.91e-02	4.50e-03	2.01e-03	4.04e-04	0.00e+00	3.50e-03	5.01e-04	4.09e-04	3.96e-03	2.24e-03	7.51e-02		3.62e-01
5	813856	810353	132368	144437	8.38e-03	4.61e-02	4.71e-03	2.40e-03	3.68e-04	0.00e+00	3.45e-03	5.53e-04	4.36e-04	4.45e-03	2.24e-03	7.30e-02		4.35e-01
6	810606	808185	138391	141894	8.42e-03	4.98e-02	4.66e-03	1.49e-03	3.65e-04	0.00e+00	3.52e-03	5.79e-04	4.36e-04	4.01e-03	2.23e-03	7.55e-02		5.10e-01
7	811974	812003	142447	144868	8.42e-03	4.52e-02	4.75e-03	2.48e-03	3.91e-04	0.00e+00	3.50e-03	7.37e-04	4.19e-04	4.37e-03	2.32e-03	7.26e-02		5.83e-01
8	813727	812559	141885	141856	8.41e-03	5.02e-02	4.76e-03	2.38e-03	9.93e-04	0.00e+00	3.53e-03	6.16e-04	4.17e-04	3.95e-03	2.36e-03	7.76e-02		6.60e-01
9	812320	811681	140938	142106	8.43e-03	4.71e-02	4.83e-03	1.85e-03	1.20e-03	0.00e+00	3.57e-03	6.68e-04	4.59e-04	3.83e-03	2.30e-03	7.43e-02		7.35e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.3844 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.3844 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.384440  0.000000
];

