% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_94 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	823492	816814	141586	141586	5.48e-03	2.97e-02	6.17e-03	9.77e-03	1.01e-03	0.00e+00	2.86e-03	4.14e-04	4.45e-04	7.28e-03	2.44e-03	6.56e-02		6.56e-02
1	796961	801814	124726	131404	5.52e-03	2.99e-02	4.43e-03	5.45e-03	6.59e-04	0.00e+00	3.35e-03	2.89e-04	4.82e-04	3.93e-03	2.45e-03	5.65e-02		1.22e-01
2	803106	823138	152063	147210	5.41e-03	4.63e-02	4.49e-03	1.66e-03	4.80e-04	0.00e+00	3.93e-03	4.65e-04	5.08e-04	3.61e-03	2.72e-03	6.95e-02		1.92e-01
3	823345	809413	146724	126692	5.57e-03	5.40e-02	5.15e-03	1.41e-03	4.09e-04	0.00e+00	4.46e-03	4.41e-04	4.99e-04	3.22e-03	2.91e-03	7.81e-02		2.70e-01
4	806987	809283	127291	141223	5.50e-03	5.36e-02	5.54e-03	1.25e-03	5.09e-04	0.00e+00	5.63e-03	4.09e-04	5.26e-04	3.22e-03	2.81e-03	7.90e-02		3.49e-01
5	816186	807909	144454	142158	5.51e-03	5.13e-02	6.41e-03	1.90e-03	7.50e-04	0.00e+00	6.44e-03	3.72e-04	5.92e-04	3.54e-03	2.85e-03	7.97e-02		4.28e-01
6	809777	814572	136061	144338	5.48e-03	5.58e-02	5.72e-03	1.81e-03	5.55e-04	0.00e+00	5.71e-03	5.00e-04	5.32e-04	3.27e-03	2.85e-03	8.22e-02		5.11e-01
7	811309	806506	143276	138481	5.51e-03	5.32e-02	5.76e-03	1.87e-03	5.08e-04	0.00e+00	5.82e-03	5.27e-04	5.36e-04	3.38e-03	2.83e-03	8.00e-02		5.91e-01
8	812587	815805	142550	147353	5.46e-03	5.34e-02	5.67e-03	1.73e-03	9.89e-04	0.00e+00	4.95e-03	5.00e-04	5.39e-04	3.33e-03	2.86e-03	7.94e-02		6.70e-01
9	811847	810898	142078	138860	5.55e-03	5.09e-02	5.88e-03	1.58e-03	9.36e-04	0.00e+00	4.64e-03	4.38e-04	5.17e-04	3.22e-03	2.87e-03	7.65e-02		7.46e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.2197 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.2197 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.219742  0.000000
];

