% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_59 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	819286	811581	141586	141586	3.21e-03	2.75e-02	4.81e-03	6.85e-03	4.57e-04	0.00e+00	4.09e-03	2.47e-04	4.89e-04	5.55e-03	2.46e-03	5.57e-02		5.57e-02
1	809328	807619	128932	136637	3.25e-03	3.11e-02	3.62e-03	2.04e-03	4.22e-04	0.00e+00	5.24e-03	2.36e-04	4.80e-04	4.98e-03	3.28e-03	5.46e-02		1.10e-01
2	812438	789682	139696	141405	3.26e-03	5.20e-02	4.68e-03	1.76e-03	4.63e-04	0.00e+00	6.10e-03	2.63e-04	5.74e-04	3.34e-03	3.48e-03	7.59e-02		1.86e-01
3	815479	802641	137392	160148	3.21e-03	6.75e-02	5.13e-03	1.86e-03	6.34e-04	0.00e+00	7.01e-03	2.88e-04	6.64e-04	3.32e-03	3.33e-03	9.30e-02		2.79e-01
4	805139	804234	135157	147995	3.25e-03	6.70e-02	5.71e-03	1.42e-03	7.65e-04	0.00e+00	7.65e-03	3.03e-04	7.24e-04	3.79e-03	3.16e-03	9.37e-02		3.73e-01
5	812588	809301	146302	147207	3.28e-03	5.95e-02	5.38e-03	1.96e-03	1.11e-03	0.00e+00	7.82e-03	3.26e-04	7.08e-04	3.43e-03	3.17e-03	8.67e-02		4.60e-01
6	808917	812733	139659	142946	3.32e-03	6.40e-02	5.54e-03	1.81e-03	7.43e-04	0.00e+00	7.73e-03	3.22e-04	7.18e-04	3.40e-03	3.17e-03	9.07e-02		5.50e-01
7	816115	814197	144136	140320	3.30e-03	6.30e-02	5.53e-03	2.49e-03	6.48e-04	0.00e+00	7.47e-03	2.96e-04	6.95e-04	3.30e-03	3.27e-03	9.00e-02		6.40e-01
8	809679	810029	137744	139662	3.31e-03	6.64e-02	5.62e-03	1.73e-03	1.19e-03	0.00e+00	7.78e-03	3.83e-04	6.82e-04	3.41e-03	3.25e-03	9.38e-02		7.34e-01
9	813047	810183	144986	144636	3.32e-03	6.38e-02	5.55e-03	1.30e-03	1.10e-03	0.00e+00	7.85e-03	2.63e-04	6.99e-04	3.28e-03	3.26e-03	9.05e-02		8.25e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.1987 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.1987 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.198738  0.000000
];

