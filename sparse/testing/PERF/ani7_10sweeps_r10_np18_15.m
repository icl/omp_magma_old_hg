% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_18 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	817582	809766	141586	141586	3.76e-03	6.40e-02	1.31e-02	7.82e-03	1.57e-03	0.00e+00	1.38e-02	2.37e-04	1.44e-03	8.26e-03	8.18e-03	1.22e-01		1.22e-01
1	809967	826325	130636	138452	3.80e-03	7.96e-02	1.24e-02	4.17e-03	1.83e-03	0.00e+00	1.69e-02	2.59e-04	1.61e-03	7.31e-03	1.07e-02	1.39e-01		2.61e-01
2	800110	796520	139057	122699	3.92e-03	1.29e-01	1.62e-02	3.17e-03	1.24e-03	0.00e+00	2.13e-02	3.15e-04	1.72e-03	6.30e-03	1.10e-02	1.94e-01		4.55e-01
3	801785	803816	149720	153310	3.78e-03	1.71e-01	1.71e-02	3.30e-03	1.66e-03	0.00e+00	2.42e-02	3.03e-04	1.90e-03	6.28e-03	1.03e-02	2.40e-01		6.95e-01
4	809889	809782	148851	146820	3.81e-03	1.64e-01	1.82e-02	4.62e-03	2.15e-03	0.00e+00	2.64e-02	3.07e-04	1.98e-03	6.42e-03	1.05e-02	2.38e-01		9.33e-01
5	807480	808960	141552	141659	3.84e-03	1.77e-01	1.97e-02	4.77e-03	2.40e-03	0.00e+00	2.69e-02	3.58e-04	2.01e-03	7.06e-03	1.05e-02	2.55e-01		1.19e+00
6	811598	811231	144767	143287	3.84e-03	1.80e-01	1.84e-02	3.83e-03	1.93e-03	0.00e+00	2.56e-02	3.09e-04	1.89e-03	6.57e-03	1.06e-02	2.53e-01		1.44e+00
7	811967	811076	141455	141822	3.84e-03	1.88e-01	1.87e-02	5.11e-03	2.30e-03	0.00e+00	2.67e-02	2.20e-04	2.02e-03	6.44e-03	1.06e-02	2.64e-01		1.70e+00
8	809712	810070	141892	142783	3.82e-03	1.90e-01	1.88e-02	3.46e-03	1.89e-03	0.00e+00	2.57e-02	2.69e-04	1.90e-03	6.53e-03	1.06e-02	2.63e-01		1.97e+00
9	811463	810531	144953	144595	3.88e-03	1.87e-01	1.87e-02	3.31e-03	2.55e-03	0.00e+00	2.67e-02	2.81e-04	1.98e-03	6.30e-03	1.06e-02	2.62e-01		2.23e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 2.6353 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 2.6353 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  2.635270  0.000000
];

