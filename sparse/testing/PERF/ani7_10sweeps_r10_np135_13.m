% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_135 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	815862	801931	141586	141586	6.00e-03	2.86e-02	5.76e-03	1.09e-02	3.74e-04	0.00e+00	2.68e-03	3.04e-04	3.69e-04	7.25e-03	1.69e-03	6.40e-02		6.40e-02
1	786045	824410	132356	146287	5.96e-03	2.65e-02	2.92e-03	2.84e-03	3.97e-04	0.00e+00	3.30e-03	2.96e-04	3.59e-04	5.10e-03	2.27e-03	5.00e-02		1.14e-01
2	813920	805744	162979	124614	6.13e-03	4.39e-02	3.70e-03	2.84e-03	3.24e-04	0.00e+00	3.95e-03	3.44e-04	4.11e-04	3.02e-03	2.33e-03	6.69e-02		1.81e-01
3	807840	799717	135910	144086	6.03e-03	4.91e-02	4.06e-03	2.63e-03	3.45e-04	0.00e+00	4.48e-03	3.32e-04	4.79e-04	3.00e-03	2.23e-03	7.27e-02		2.54e-01
4	810021	800472	142796	150919	5.97e-03	4.85e-02	4.10e-03	2.07e-03	4.91e-04	0.00e+00	4.55e-03	4.04e-04	4.92e-04	3.74e-03	2.13e-03	7.25e-02		3.26e-01
5	814481	815756	141420	150969	6.00e-03	4.39e-02	4.14e-03	1.80e-03	6.47e-04	0.00e+00	4.64e-03	4.69e-04	4.65e-04	3.36e-03	2.22e-03	6.76e-02		3.94e-01
6	810533	809454	137766	136491	6.10e-03	4.97e-02	4.30e-03	1.49e-03	3.79e-04	0.00e+00	4.46e-03	4.20e-04	4.70e-04	3.41e-03	2.20e-03	7.30e-02		4.67e-01
7	811063	812025	142520	143599	6.03e-03	4.65e-02	4.76e-03	1.35e-03	4.42e-04	0.00e+00	5.55e-03	3.94e-04	4.77e-04	3.10e-03	2.97e-03	7.16e-02		5.38e-01
8	813327	812075	142796	141834	8.99e-03	5.60e-02	6.08e-03	1.34e-03	8.60e-04	0.00e+00	5.52e-03	5.09e-04	4.79e-04	3.41e-03	3.00e-03	8.62e-02		6.24e-01
9	811932	811900	141338	142590	8.98e-03	5.37e-02	6.10e-03	2.66e-03	1.15e-03	0.00e+00	5.51e-03	3.97e-04	5.08e-04	3.09e-03	2.99e-03	8.51e-02		7.09e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.2018 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.2018 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.201821  0.000000
];

