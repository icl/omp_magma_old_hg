% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_36 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	816107	795281	141586	141586	3.31e-03	3.58e-02	6.71e-03	6.77e-03	9.43e-04	0.00e+00	6.61e-03	2.45e-04	7.38e-04	6.16e-03	3.83e-03	7.11e-02		7.11e-02
1	812251	794174	132111	152937	3.29e-03	4.29e-02	5.77e-03	3.23e-03	1.20e-03	0.00e+00	8.04e-03	2.34e-04	8.15e-04	4.64e-03	4.97e-03	7.51e-02		1.46e-01
2	801390	812594	136773	154850	3.31e-03	6.70e-02	7.48e-03	1.97e-03	9.57e-04	0.00e+00	9.74e-03	2.32e-04	8.63e-04	4.05e-03	5.46e-03	1.01e-01		2.47e-01
3	818020	809879	148440	137236	3.35e-03	9.69e-02	8.72e-03	3.19e-03	8.46e-04	0.00e+00	1.13e-02	2.70e-04	9.74e-04	4.07e-03	5.27e-03	1.35e-01		3.82e-01
4	814270	814303	132616	140757	3.35e-03	9.89e-02	8.87e-03	2.15e-03	1.16e-03	0.00e+00	1.23e-02	3.89e-04	1.04e-03	4.05e-03	5.09e-03	1.37e-01		5.19e-01
5	810549	809282	137171	137138	3.35e-03	9.30e-02	8.92e-03	1.98e-03	1.18e-03	0.00e+00	1.25e-02	3.63e-04	1.02e-03	4.49e-03	5.10e-03	1.32e-01		6.51e-01
6	816420	808602	141698	142965	3.35e-03	9.46e-02	8.88e-03	2.27e-03	1.29e-03	0.00e+00	1.30e-02	3.95e-04	1.05e-03	4.12e-03	5.17e-03	1.34e-01		7.85e-01
7	813009	812252	136633	144451	3.36e-03	9.78e-02	9.00e-03	1.89e-03	1.13e-03	0.00e+00	1.23e-02	3.04e-04	1.00e-03	4.42e-03	5.13e-03	1.36e-01		9.22e-01
8	814692	810837	140850	141607	3.37e-03	9.68e-02	9.06e-03	2.25e-03	1.70e-03	0.00e+00	1.23e-02	3.08e-04	1.01e-03	4.13e-03	5.13e-03	1.36e-01		1.06e+00
9	808261	817235	139973	143828	3.36e-03	9.59e-02	9.06e-03	2.33e-03	1.60e-03	0.00e+00	1.24e-02	3.55e-04	1.02e-03	4.09e-03	5.13e-03	1.35e-01		1.19e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.5661 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.5661 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.566072  0.000000
];

