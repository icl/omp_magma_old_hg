% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_30 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	817996	813867	141586	141586	3.34e-03	4.12e-02	7.75e-03	6.79e-03	6.85e-04	0.00e+00	7.87e-03	2.53e-04	8.43e-04	6.56e-03	4.71e-03	8.00e-02		8.00e-02
1	804912	815774	130222	134351	3.38e-03	4.99e-02	7.49e-03	3.13e-03	1.14e-03	0.00e+00	1.02e-02	2.49e-04	1.02e-03	5.29e-03	6.38e-03	8.81e-02		1.68e-01
2	804903	794350	144112	133250	3.65e-03	8.29e-02	9.73e-03	2.80e-03	7.98e-04	0.00e+00	1.27e-02	3.24e-04	1.07e-03	4.77e-03	6.83e-03	1.26e-01		2.94e-01
3	809724	809025	144927	155480	3.58e-03	1.21e-01	1.06e-02	2.60e-03	1.03e-03	0.00e+00	1.45e-02	4.02e-04	1.19e-03	4.81e-03	6.37e-03	1.66e-01		4.60e-01
4	817625	806299	140912	141611	3.60e-03	1.18e-01	1.10e-02	2.67e-03	1.48e-03	0.00e+00	1.51e-02	2.71e-04	1.18e-03	5.15e-03	6.09e-03	1.65e-01		6.25e-01
5	800680	811732	133816	145142	3.62e-03	1.14e-01	1.07e-02	3.01e-03	1.43e-03	0.00e+00	1.48e-02	4.40e-04	1.24e-03	5.24e-03	6.43e-03	1.61e-01		7.86e-01
6	812689	810829	151567	140515	3.63e-03	1.17e-01	1.12e-02	3.03e-03	1.28e-03	0.00e+00	1.52e-02	2.64e-04	1.21e-03	4.93e-03	6.43e-03	1.65e-01		9.51e-01
7	811812	812226	140364	142224	3.62e-03	1.19e-01	1.14e-02	2.43e-03	1.43e-03	0.00e+00	1.56e-02	2.56e-04	1.26e-03	4.81e-03	6.50e-03	1.67e-01		1.12e+00
8	815703	810400	142047	141633	3.63e-03	1.22e-01	1.15e-02	2.90e-03	1.55e-03	0.00e+00	1.56e-02	3.78e-04	1.24e-03	4.90e-03	6.50e-03	1.70e-01		1.29e+00
9	810660	814866	138962	144265	3.67e-03	1.21e-01	1.16e-02	2.12e-03	1.86e-03	0.00e+00	1.55e-02	3.81e-04	1.25e-03	4.72e-03	6.53e-03	1.69e-01		1.46e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.8449 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.8449 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.844946  0.000000
];

