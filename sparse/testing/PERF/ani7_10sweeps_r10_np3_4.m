% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_3 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	822232	805245	141586	141586	5.75e-03	3.24e-01	7.27e-02	1.74e-02	4.75e-03	0.00e+00	6.86e-02	2.40e-04	7.62e-03	2.67e-02	4.70e-02	5.75e-01		5.75e-01
1	821000	810583	125986	142973	6.19e-03	4.32e-01	2.52e-01	5.01e-02	1.68e-02	0.00e+00	8.53e-02	2.54e-04	9.56e-03	3.10e-02	7.77e-02	9.61e-01		1.54e+00
2	814805	810000	128024	138441	1.44e-02	9.78e-01	1.19e-01	1.93e-02	9.98e-03	0.00e+00	1.08e-01	3.16e-04	1.52e-02	5.20e-02	1.64e-01	1.48e+00		3.02e+00
3	804661	814544	135025	139830	2.33e-02	1.12e+00	2.12e-01	2.89e-02	8.16e-03	0.00e+00	1.14e-01	3.54e-04	1.44e-02	3.90e-02	1.10e-01	1.67e+00		4.68e+00
4	808212	803640	145975	136092	2.33e-02	1.09e+00	1.06e-01	1.81e-02	1.09e-02	0.00e+00	1.22e-01	3.70e-04	1.06e-02	2.55e-02	5.92e-02	1.47e+00		6.15e+00
5	810621	813287	143229	147801	6.33e-03	8.36e-01	1.05e-01	1.65e-02	1.05e-02	0.00e+00	1.15e-01	1.79e-04	1.02e-02	2.71e-02	6.08e-02	1.19e+00		7.34e+00
6	807264	810591	141626	138960	6.46e-03	8.75e-01	1.08e-01	1.66e-02	1.01e-02	0.00e+00	1.15e-01	5.56e-04	1.03e-02	2.57e-02	6.02e-02	1.23e+00		8.57e+00
7	814036	812914	145789	142462	6.41e-03	8.66e-01	1.08e-01	1.63e-02	1.04e-02	0.00e+00	1.17e-01	4.69e-04	1.03e-02	2.61e-02	6.12e-02	1.22e+00		9.79e+00
8	810577	812628	139823	140945	6.47e-03	8.98e-01	1.10e-01	1.64e-02	1.13e-02	0.00e+00	1.20e-01	3.53e-04	1.05e-02	2.59e-02	6.09e-02	1.26e+00		1.10e+01
9	814915	813209	144088	142037	6.48e-03	8.89e-01	1.09e-01	1.66e-02	1.09e-02	0.00e+00	1.17e-01	2.91e-04	1.03e-02	2.58e-02	6.12e-02	1.25e+00		1.23e+01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 12.9918 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 12.9918 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  12.991756  0.000000
];

