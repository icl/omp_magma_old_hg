% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_103 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	811058	809358	141586	141586	5.67e-03	2.98e-02	5.97e-03	9.95e-03	1.44e-03	0.00e+00	2.61e-03	3.20e-04	4.45e-04	6.76e-03	2.13e-03	6.51e-02		6.51e-02
1	791933	806786	137160	138860	5.65e-03	2.80e-02	3.81e-03	5.82e-03	1.23e-03	0.00e+00	2.99e-03	3.22e-04	4.40e-04	4.18e-03	2.44e-03	5.49e-02		1.20e-01
2	806318	807122	157091	142238	5.59e-03	4.45e-02	4.32e-03	2.51e-03	7.58e-04	0.00e+00	3.60e-03	4.03e-04	4.51e-04	3.16e-03	2.52e-03	6.78e-02		1.88e-01
3	818147	803380	143512	142708	5.62e-03	4.69e-02	4.63e-03	1.93e-03	3.48e-04	0.00e+00	4.15e-03	3.77e-04	4.42e-04	3.14e-03	2.50e-03	7.00e-02		2.58e-01
4	813803	809309	132489	147256	5.58e-03	4.85e-02	4.88e-03	3.01e-03	6.73e-04	0.00e+00	4.60e-03	3.98e-04	5.43e-04	3.11e-03	2.64e-03	7.39e-02		3.32e-01
5	809032	808591	137638	142132	5.62e-03	4.45e-02	5.27e-03	1.72e-03	4.68e-04	0.00e+00	4.51e-03	3.27e-04	4.96e-04	3.44e-03	2.65e-03	6.90e-02		4.01e-01
6	811935	814001	143215	143656	5.64e-03	4.69e-02	5.22e-03	4.53e-03	6.98e-04	0.00e+00	4.66e-03	5.17e-04	5.23e-04	3.31e-03	2.63e-03	7.46e-02		4.75e-01
7	806630	811904	141118	139052	5.67e-03	4.60e-02	5.27e-03	1.66e-03	4.71e-04	0.00e+00	4.49e-03	5.10e-04	4.57e-04	3.13e-03	2.59e-03	7.03e-02		5.46e-01
8	812457	812672	147229	141955	5.66e-03	4.73e-02	5.22e-03	1.97e-03	5.24e-04	0.00e+00	4.49e-03	5.09e-04	4.79e-04	3.28e-03	2.59e-03	7.20e-02		6.18e-01
9	812370	811406	142208	141993	5.67e-03	4.66e-02	5.34e-03	1.90e-03	7.99e-04	0.00e+00	4.47e-03	3.59e-04	4.81e-04	3.13e-03	2.62e-03	7.14e-02		6.89e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.1643 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.1643 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.164342  0.000000
];

