% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_127 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	818510	802860	141586	141586	5.95e-03	2.78e-02	5.75e-03	1.06e-02	6.10e-04	0.00e+00	2.87e-03	4.27e-04	3.51e-04	7.21e-03	1.78e-03	6.33e-02		6.33e-02
1	818825	810698	129708	145358	5.90e-03	2.73e-02	3.08e-03	3.71e-03	6.95e-04	0.00e+00	3.45e-03	2.87e-04	4.04e-04	3.97e-03	2.12e-03	5.09e-02		1.14e-01
2	810453	814856	130199	138326	5.96e-03	3.77e-02	3.74e-03	3.43e-03	2.79e-04	0.00e+00	4.05e-03	3.58e-04	4.24e-04	3.03e-03	2.35e-03	6.13e-02		1.76e-01
3	801440	796338	139377	134974	5.99e-03	4.54e-02	4.25e-03	1.84e-03	4.31e-04	0.00e+00	4.77e-03	4.07e-04	4.71e-04	2.99e-03	2.22e-03	6.87e-02		2.44e-01
4	817421	813066	149196	154298	5.86e-03	4.47e-02	4.27e-03	2.00e-03	4.87e-04	0.00e+00	4.81e-03	5.87e-04	4.58e-04	3.73e-03	2.25e-03	6.91e-02		3.13e-01
5	808944	813809	134020	138375	5.98e-03	4.55e-02	4.44e-03	1.25e-03	1.07e-03	0.00e+00	4.75e-03	6.15e-04	4.67e-04	3.37e-03	2.28e-03	6.97e-02		3.83e-01
6	811734	812355	143303	138438	6.07e-03	4.81e-02	4.43e-03	2.66e-03	3.90e-04	0.00e+00	4.63e-03	4.35e-04	4.97e-04	3.39e-03	2.32e-03	7.30e-02		4.56e-01
7	811465	808520	141319	140698	5.98e-03	4.68e-02	4.48e-03	1.30e-03	5.73e-04	0.00e+00	4.73e-03	4.11e-04	4.52e-04	3.23e-03	2.30e-03	7.03e-02		5.26e-01
8	811515	811665	142394	145339	5.94e-03	4.94e-02	4.47e-03	1.54e-03	9.36e-04	0.00e+00	4.77e-03	3.39e-04	4.62e-04	3.00e-03	2.30e-03	7.32e-02		5.99e-01
9	811941	811248	143150	143000	6.00e-03	4.75e-02	4.47e-03	2.16e-03	8.91e-04	0.00e+00	4.97e-03	4.28e-04	4.51e-04	3.02e-03	2.27e-03	7.22e-02		6.72e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.1638 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.1638 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.163813  0.000000
];

