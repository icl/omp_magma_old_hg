% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_201 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	813808	816894	141586	141586	9.20e-03	6.07e-02	1.41e-02	2.02e-02	1.02e-02	0.00e+00	2.82e-03	5.24e-04	3.30e-04	1.81e-02	1.74e-03	1.38e-01		1.38e-01
1	807701	819945	134410	131324	9.31e-03	4.40e-02	3.27e-03	4.94e-03	7.63e-03	0.00e+00	3.50e-03	3.76e-04	3.93e-04	6.47e-03	2.23e-03	8.21e-02		2.20e-01
2	810566	800892	141323	129079	9.35e-03	6.74e-02	3.91e-03	2.79e-03	5.39e-04	0.00e+00	3.78e-03	4.72e-04	4.35e-04	4.15e-03	2.24e-03	9.50e-02		3.15e-01
3	805371	809535	139264	148938	9.19e-03	6.16e-02	4.36e-03	2.77e-03	5.05e-04	0.00e+00	4.10e-03	5.48e-04	4.66e-04	4.18e-03	2.27e-03	9.00e-02		4.05e-01
4	810808	810844	145265	141101	9.51e-03	7.18e-02	4.37e-03	2.31e-03	7.73e-04	0.00e+00	4.59e-03	5.83e-04	4.94e-04	4.31e-03	2.18e-03	1.01e-01		5.06e-01
5	812294	806535	140633	140597	9.29e-03	6.11e-02	4.32e-03	4.25e-03	6.43e-04	0.00e+00	4.47e-03	5.00e-04	4.97e-04	4.88e-03	2.17e-03	9.21e-02		5.98e-01
6	810977	809077	139953	145712	9.20e-03	7.02e-02	4.92e-03	2.57e-03	5.50e-04	0.00e+00	4.53e-03	3.66e-04	5.06e-04	4.60e-03	2.23e-03	9.97e-02		6.98e-01
7	809851	808270	142076	143976	9.26e-03	6.17e-02	4.41e-03	1.87e-03	5.58e-04	0.00e+00	4.64e-03	5.64e-04	4.65e-04	4.11e-03	2.19e-03	8.98e-02		7.87e-01
8	811302	809503	144008	145589	9.23e-03	7.00e-02	4.35e-03	2.68e-03	5.17e-04	0.00e+00	4.39e-03	6.54e-04	4.95e-04	4.15e-03	2.20e-03	9.87e-02		8.86e-01
9	810479	811089	143363	145162	9.30e-03	6.26e-02	4.42e-03	2.74e-03	1.61e-03	0.00e+00	4.43e-03	6.43e-04	4.79e-04	4.10e-03	2.21e-03	9.26e-02		9.79e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.6201 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.6201 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.620129  0.000000
];

