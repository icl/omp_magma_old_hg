% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_128 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	813548	815012	141586	141586	5.92e-03	2.74e-02	5.73e-03	1.07e-02	1.35e-03	0.00e+00	2.77e-03	3.96e-04	3.97e-04	8.07e-03	2.32e-03	6.51e-02		6.51e-02
1	801264	802521	134670	133206	8.43e-03	2.76e-02	4.50e-03	2.17e-03	7.62e-04	0.00e+00	3.52e-03	2.56e-04	3.98e-04	4.19e-03	2.47e-03	5.44e-02		1.19e-01
2	816278	806169	147760	146503	8.24e-03	3.80e-02	4.96e-03	1.30e-03	3.49e-04	0.00e+00	3.79e-03	3.49e-04	3.98e-04	3.40e-03	2.50e-03	6.32e-02		1.83e-01
3	813574	803111	133552	143661	8.31e-03	4.58e-02	5.28e-03	2.27e-03	4.73e-04	0.00e+00	4.56e-03	3.23e-04	4.64e-04	3.01e-03	2.52e-03	7.30e-02		2.56e-01
4	800948	808054	137062	147525	8.25e-03	4.88e-02	5.45e-03	1.46e-03	4.04e-04	0.00e+00	4.72e-03	3.55e-04	4.88e-04	2.92e-03	2.59e-03	7.55e-02		3.31e-01
5	807259	810899	150493	143387	8.30e-03	4.30e-02	5.29e-03	2.06e-03	4.16e-04	0.00e+00	4.79e-03	4.32e-04	4.75e-04	3.45e-03	2.26e-03	7.05e-02		4.02e-01
6	807239	810115	144988	141348	5.97e-03	4.67e-02	4.40e-03	1.71e-03	4.59e-04	0.00e+00	4.67e-03	3.66e-04	4.38e-04	3.01e-03	2.25e-03	6.99e-02		4.72e-01
7	811190	812576	145814	142938	5.95e-03	4.56e-02	4.54e-03	2.14e-03	3.86e-04	0.00e+00	4.78e-03	3.92e-04	4.36e-04	3.26e-03	2.32e-03	6.98e-02		5.41e-01
8	813597	812943	142669	141283	5.99e-03	4.84e-02	4.45e-03	1.64e-03	9.08e-04	0.00e+00	5.11e-03	5.30e-04	4.52e-04	3.14e-03	2.32e-03	7.30e-02		6.14e-01
9	812469	811513	141068	141722	6.06e-03	4.66e-02	4.49e-03	1.45e-03	8.88e-04	0.00e+00	4.58e-03	5.03e-04	4.59e-04	2.95e-03	2.30e-03	7.02e-02		6.85e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.1754 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.1754 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.175418  0.000000
];

