% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_153 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	809506	814611	141586	141586	7.81e-03	3.62e-02	7.68e-03	1.46e-02	7.08e-04	0.00e+00	2.50e-03	5.23e-04	4.40e-04	9.94e-03	2.11e-03	8.24e-02		8.24e-02
1	813173	818344	138712	133607	7.86e-03	2.95e-02	3.99e-03	3.69e-03	7.69e-04	0.00e+00	3.13e-03	4.40e-04	4.03e-04	5.41e-03	2.25e-03	5.74e-02		1.40e-01
2	822862	813741	135851	130680	7.88e-03	4.72e-02	4.34e-03	1.93e-03	2.71e-04	0.00e+00	3.62e-03	4.25e-04	4.18e-04	3.99e-03	2.28e-03	7.24e-02		2.12e-01
3	808214	817098	126968	136089	7.80e-03	5.21e-02	4.80e-03	2.12e-03	3.11e-04	0.00e+00	3.84e-03	4.03e-04	4.83e-04	3.98e-03	2.26e-03	7.81e-02		2.90e-01
4	815409	811381	142422	133538	7.85e-03	5.13e-02	4.89e-03	2.47e-03	3.70e-04	0.00e+00	4.11e-03	6.64e-04	4.64e-04	4.22e-03	2.33e-03	7.87e-02		3.69e-01
5	806433	808307	136032	140060	7.79e-03	4.58e-02	5.14e-03	3.31e-03	1.24e-03	0.00e+00	4.20e-03	6.31e-04	4.77e-04	4.44e-03	2.23e-03	7.52e-02		4.44e-01
6	808444	812440	145814	143940	7.79e-03	5.02e-02	4.94e-03	2.62e-03	4.15e-04	0.00e+00	4.26e-03	3.85e-04	4.84e-04	4.24e-03	2.26e-03	7.76e-02		5.22e-01
7	809966	815985	144609	140613	7.77e-03	4.64e-02	4.99e-03	2.85e-03	4.45e-04	0.00e+00	4.19e-03	6.40e-04	4.78e-04	4.21e-03	2.27e-03	7.43e-02		5.96e-01
8	815925	814720	143893	137874	7.83e-03	5.11e-02	4.99e-03	1.73e-03	1.11e-03	0.00e+00	4.41e-03	5.82e-04	4.71e-04	4.10e-03	2.47e-03	7.88e-02		6.75e-01
9	811166	813107	138740	139945	8.86e-03	5.33e-02	5.20e-03	1.96e-03	9.51e-04	0.00e+00	5.11e-03	6.39e-04	4.51e-04	3.96e-03	2.44e-03	8.29e-02		7.58e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.3875 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.3875 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.387500  0.000000
];

