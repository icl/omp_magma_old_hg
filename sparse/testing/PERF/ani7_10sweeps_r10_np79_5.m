% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_79 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	813453	810746	141586	141586	5.22e-03	2.99e-02	6.68e-03	9.45e-03	5.81e-04	0.00e+00	3.35e-03	2.98e-04	5.07e-04	6.66e-03	2.57e-03	6.52e-02		6.52e-02
1	804811	810317	134765	137472	5.20e-03	2.80e-02	4.83e-03	2.56e-03	7.59e-04	0.00e+00	3.82e-03	2.53e-04	4.99e-04	4.35e-03	2.69e-03	5.29e-02		1.18e-01
2	801262	805864	144213	138707	5.23e-03	4.84e-02	5.22e-03	1.94e-03	3.67e-04	0.00e+00	4.57e-03	3.39e-04	4.87e-04	3.35e-03	2.79e-03	7.27e-02		1.91e-01
3	820359	814870	148568	143966	5.19e-03	5.48e-02	5.53e-03	1.99e-03	4.88e-04	0.00e+00	5.34e-03	3.26e-04	5.60e-04	3.42e-03	2.94e-03	8.06e-02		2.71e-01
4	799643	817546	130277	135766	5.25e-03	5.85e-02	6.10e-03	1.75e-03	9.37e-04	0.00e+00	6.01e-03	5.36e-04	6.39e-04	3.44e-03	2.89e-03	8.61e-02		3.58e-01
5	810532	817308	151798	133895	5.26e-03	5.07e-02	6.10e-03	2.13e-03	1.09e-03	0.00e+00	5.79e-03	4.95e-04	5.88e-04	3.60e-03	2.97e-03	7.87e-02		4.36e-01
6	811093	812638	141715	134939	5.28e-03	5.43e-02	6.33e-03	3.65e-03	6.55e-04	0.00e+00	5.84e-03	2.98e-04	5.81e-04	3.52e-03	2.97e-03	8.34e-02		5.20e-01
7	814133	814925	141960	140415	5.20e-03	5.25e-02	6.34e-03	1.38e-03	5.64e-04	0.00e+00	5.54e-03	4.59e-04	5.43e-04	3.51e-03	2.98e-03	7.90e-02		5.99e-01
8	807559	811631	139726	138934	5.21e-03	5.55e-02	6.36e-03	2.14e-03	1.05e-03	0.00e+00	5.68e-03	4.40e-04	5.94e-04	3.44e-03	2.91e-03	8.33e-02		6.82e-01
9	818007	815727	147106	143034	5.23e-03	5.24e-02	6.19e-03	2.34e-03	1.10e-03	0.00e+00	5.89e-03	3.87e-04	5.94e-04	3.46e-03	3.02e-03	8.06e-02		7.63e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.2474 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.2474 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.247406  0.000000
];

