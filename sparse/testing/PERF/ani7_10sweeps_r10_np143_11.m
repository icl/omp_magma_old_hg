% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_143 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	813855	816762	141586	141586	7.49e-03	3.36e-02	6.78e-03	1.40e-02	6.56e-04	0.00e+00	2.73e-03	4.74e-04	4.07e-04	8.87e-03	2.19e-03	7.72e-02		7.72e-02
1	796786	807512	134363	131456	7.59e-03	2.98e-02	4.22e-03	5.23e-03	7.76e-04	0.00e+00	3.53e-03	3.91e-04	3.93e-04	5.53e-03	2.43e-03	5.99e-02		1.37e-01
2	811418	817925	152238	141512	8.80e-03	4.69e-02	4.60e-03	1.80e-03	2.84e-04	0.00e+00	4.53e-03	5.03e-04	4.12e-04	4.09e-03	2.67e-03	7.46e-02		2.12e-01
3	804963	811396	138412	131905	8.87e-03	5.34e-02	5.15e-03	1.49e-03	3.85e-04	0.00e+00	4.59e-03	6.17e-04	4.65e-04	4.05e-03	2.62e-03	8.17e-02		2.93e-01
4	805297	803594	145673	139240	8.73e-03	5.36e-02	5.22e-03	2.47e-03	4.49e-04	0.00e+00	4.67e-03	7.50e-04	4.60e-04	4.91e-03	2.61e-03	8.39e-02		3.77e-01
5	808546	809546	146144	147847	8.75e-03	5.12e-02	1.84e-02	2.21e-03	1.12e-03	0.00e+00	5.47e-03	5.72e-04	4.80e-04	4.34e-03	2.64e-03	9.52e-02		4.72e-01
6	812068	812375	143701	142701	7.95e-03	5.08e-02	5.41e-03	1.90e-03	4.26e-04	0.00e+00	4.37e-03	6.78e-04	4.66e-04	4.40e-03	2.40e-03	7.88e-02		5.51e-01
7	811603	814104	140985	140678	7.50e-03	4.79e-02	5.42e-03	2.09e-03	4.33e-04	0.00e+00	4.28e-03	6.67e-04	4.62e-04	4.18e-03	2.39e-03	7.53e-02		6.27e-01
8	811213	810609	142256	139755	7.51e-03	5.08e-02	5.50e-03	1.72e-03	1.02e-03	0.00e+00	4.40e-03	8.54e-04	4.65e-04	4.15e-03	2.34e-03	7.88e-02		7.05e-01
9	814872	811956	143452	144056	7.50e-03	4.82e-02	5.39e-03	1.63e-03	9.21e-04	0.00e+00	4.39e-03	6.84e-04	4.92e-04	3.98e-03	2.35e-03	7.55e-02		7.81e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.4014 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.4014 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.401413  0.000000
];

