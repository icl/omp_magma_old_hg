% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_196 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	816652	811919	141586	141586	8.76e-03	3.60e-02	7.67e-03	1.60e-02	1.44e-03	0.00e+00	2.77e-03	4.77e-04	3.38e-04	1.01e-02	1.76e-03	8.54e-02		8.54e-02
1	792436	803299	131566	136299	8.85e-03	3.21e-02	3.26e-03	5.20e-03	1.60e-03	0.00e+00	3.41e-03	3.55e-04	3.69e-04	5.81e-03	1.92e-03	6.29e-02		1.48e-01
2	811264	812487	156588	145725	8.71e-03	4.47e-02	3.69e-03	1.89e-03	6.02e-04	0.00e+00	3.62e-03	4.90e-04	3.99e-04	4.29e-03	2.16e-03	7.05e-02		2.19e-01
3	814675	813786	138566	137343	8.77e-03	4.87e-02	4.22e-03	2.01e-03	3.82e-04	0.00e+00	4.26e-03	7.06e-04	4.35e-04	3.88e-03	2.19e-03	7.56e-02		2.94e-01
4	809261	807399	135961	136850	8.82e-03	5.41e-02	4.42e-03	2.75e-03	5.22e-04	0.00e+00	4.42e-03	5.90e-04	4.48e-04	3.94e-03	2.12e-03	8.21e-02		3.76e-01
5	808706	812356	142180	144042	8.73e-03	4.93e-02	4.33e-03	2.52e-03	4.48e-04	0.00e+00	4.39e-03	5.99e-04	4.42e-04	4.40e-03	2.22e-03	7.74e-02		4.54e-01
6	806835	811079	143541	139891	8.83e-03	5.39e-02	4.37e-03	1.80e-03	4.21e-04	0.00e+00	4.43e-03	4.11e-04	4.82e-04	4.02e-03	2.16e-03	8.08e-02		5.35e-01
7	812216	811237	146218	141974	8.80e-03	5.05e-02	4.42e-03	1.92e-03	4.07e-04	0.00e+00	4.30e-03	6.24e-04	4.40e-04	4.05e-03	2.23e-03	7.77e-02		6.12e-01
8	815633	811070	141643	142622	8.81e-03	5.49e-02	4.43e-03	1.70e-03	1.10e-03	0.00e+00	4.41e-03	9.30e-04	4.42e-04	3.96e-03	2.22e-03	8.29e-02		6.95e-01
9	807793	811662	139032	143595	8.79e-03	5.17e-02	4.47e-03	4.17e-03	1.10e-03	0.00e+00	4.43e-03	5.66e-04	4.37e-04	4.00e-03	2.18e-03	8.19e-02		7.77e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.3982 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.3982 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.398163  0.000000
];

