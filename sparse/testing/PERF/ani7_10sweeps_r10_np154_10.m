% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_154 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	809382	811969	141586	141586	7.79e-03	3.68e-02	7.73e-03	1.48e-02	1.20e-03	0.00e+00	2.50e-03	4.38e-04	3.91e-04	9.92e-03	2.09e-03	8.37e-02		8.37e-02
1	818832	803597	138836	136249	7.83e-03	2.93e-02	3.94e-03	2.51e-03	2.63e-03	0.00e+00	3.05e-03	3.53e-04	3.60e-04	6.12e-03	2.23e-03	5.84e-02		1.42e-01
2	817782	814712	130192	145427	7.68e-03	4.72e-02	4.29e-03	3.38e-03	3.56e-04	0.00e+00	3.46e-03	4.84e-04	4.27e-04	4.10e-03	2.25e-03	7.36e-02		2.16e-01
3	794350	800007	132048	135118	7.80e-03	5.16e-02	4.58e-03	3.09e-03	3.69e-04	0.00e+00	3.93e-03	6.00e-04	4.92e-04	4.01e-03	2.15e-03	7.86e-02		2.94e-01
4	814209	803935	156286	150629	7.68e-03	4.74e-02	4.66e-03	2.34e-03	4.85e-04	0.00e+00	4.14e-03	5.93e-04	4.81e-04	4.47e-03	2.30e-03	7.45e-02		3.69e-01
5	807385	809593	137232	147506	7.67e-03	4.53e-02	5.06e-03	2.19e-03	4.43e-04	0.00e+00	4.13e-03	7.65e-04	4.67e-04	4.34e-03	2.24e-03	7.26e-02		4.41e-01
6	814857	809720	144862	142654	7.77e-03	5.00e-02	4.96e-03	2.04e-03	5.00e-04	0.00e+00	4.07e-03	4.43e-04	4.67e-04	4.38e-03	2.32e-03	7.69e-02		5.18e-01
7	812725	810564	138196	143333	7.77e-03	4.67e-02	5.07e-03	2.72e-03	3.65e-04	0.00e+00	4.04e-03	6.45e-04	4.45e-04	3.93e-03	2.26e-03	7.40e-02		5.92e-01
8	814076	812475	141134	143295	7.73e-03	5.03e-02	5.05e-03	1.48e-03	5.06e-04	0.00e+00	4.15e-03	7.38e-04	4.54e-04	4.22e-03	2.26e-03	7.69e-02		6.69e-01
9	815574	815658	140589	142190	7.77e-03	4.68e-02	5.09e-03	2.25e-03	1.12e-03	0.00e+00	4.16e-03	7.10e-04	4.46e-04	4.05e-03	2.28e-03	7.47e-02		7.44e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.3785 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.3785 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.378454  0.000000
];

