% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_169 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	819927	811170	141586	141586	9.22e-03	5.40e-02	1.28e-02	1.83e-02	8.73e-04	0.00e+00	3.28e-03	4.54e-04	3.69e-04	1.24e-02	2.00e-03	1.14e-01		1.14e-01
1	811359	810898	128291	137048	9.22e-03	3.97e-02	3.75e-03	4.57e-03	6.97e-04	0.00e+00	3.99e-03	3.21e-04	4.32e-04	7.42e-03	2.63e-03	7.27e-02		1.86e-01
2	813863	793349	137665	138126	9.31e-03	6.36e-02	4.59e-03	2.31e-03	4.61e-04	0.00e+00	4.41e-03	4.63e-04	4.98e-04	4.36e-03	2.63e-03	9.27e-02		2.79e-01
3	812210	813650	135967	156481	9.13e-03	6.19e-02	5.05e-03	2.95e-03	5.54e-04	0.00e+00	4.82e-03	5.46e-04	5.16e-04	4.09e-03	2.72e-03	9.22e-02		3.71e-01
4	818780	814048	138426	136986	9.34e-03	7.13e-02	5.30e-03	1.98e-03	6.97e-04	0.00e+00	5.30e-03	5.60e-04	5.98e-04	5.03e-03	2.62e-03	1.03e-01		4.74e-01
5	812103	803195	132661	137393	9.34e-03	6.26e-02	5.17e-03	2.64e-03	1.38e-03	0.00e+00	5.22e-03	6.23e-04	5.40e-04	4.42e-03	2.56e-03	9.45e-02		5.69e-01
6	808840	814931	140144	149052	9.27e-03	6.88e-02	5.13e-03	2.74e-03	6.46e-04	0.00e+00	5.14e-03	5.18e-04	5.29e-04	4.61e-03	2.58e-03	9.99e-02		6.68e-01
7	811349	814810	144213	138122	9.37e-03	6.11e-02	5.17e-03	1.78e-03	5.47e-04	0.00e+00	5.20e-03	4.53e-04	5.49e-04	4.15e-03	2.59e-03	9.10e-02		7.59e-01
8	814036	813195	142510	139049	9.34e-03	7.10e-02	5.23e-03	1.83e-03	1.06e-03	0.00e+00	5.29e-03	5.81e-04	5.29e-04	4.48e-03	2.63e-03	1.02e-01		8.61e-01
9	808554	813700	140629	141470	9.35e-03	6.28e-02	5.26e-03	2.59e-03	1.34e-03	0.00e+00	5.31e-03	5.89e-04	5.29e-04	4.23e-03	2.62e-03	9.46e-02		9.56e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.6168 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.6168 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.616816  0.000000
];

