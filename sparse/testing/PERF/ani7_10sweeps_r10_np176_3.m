% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_176 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	813435	798133	141586	141586	9.22e-03	3.67e-02	7.56e-03	1.44e-02	7.80e-04	0.00e+00	2.19e-03	5.65e-04	3.58e-04	9.65e-03	1.87e-03	8.33e-02		8.33e-02
1	811372	804422	134783	150085	8.26e-03	3.02e-02	3.52e-03	1.98e-03	7.89e-04	0.00e+00	2.54e-03	3.33e-04	4.33e-04	5.48e-03	2.07e-03	5.57e-02		1.39e-01
2	803504	811474	137652	144602	8.32e-03	4.55e-02	4.03e-03	2.47e-03	2.99e-04	0.00e+00	3.85e-03	3.93e-04	4.28e-04	3.91e-03	2.24e-03	7.15e-02		2.10e-01
3	817494	809976	146326	138356	8.32e-03	4.51e-02	4.22e-03	2.10e-03	2.94e-04	0.00e+00	3.39e-03	6.02e-04	4.00e-04	3.98e-03	2.30e-03	7.07e-02		2.81e-01
4	797680	808100	133142	140660	8.29e-03	5.02e-02	4.68e-03	1.85e-03	3.48e-04	0.00e+00	3.73e-03	4.95e-04	4.46e-04	4.79e-03	2.21e-03	7.70e-02		3.58e-01
5	806716	810683	153761	143341	8.28e-03	4.41e-02	4.57e-03	2.61e-03	1.16e-03	0.00e+00	3.59e-03	5.79e-04	4.31e-04	4.37e-03	2.31e-03	7.20e-02		4.30e-01
6	814791	811007	145531	141564	8.32e-03	4.98e-02	4.77e-03	1.93e-03	4.28e-04	0.00e+00	3.73e-03	5.26e-04	4.54e-04	4.44e-03	2.35e-03	7.68e-02		5.07e-01
7	806550	812495	138262	142046	8.32e-03	4.60e-02	4.79e-03	1.76e-03	4.07e-04	0.00e+00	3.64e-03	6.32e-04	4.55e-04	4.10e-03	2.30e-03	7.24e-02		5.79e-01
8	810063	812676	147309	141364	8.38e-03	4.98e-02	4.75e-03	2.80e-03	1.18e-03	0.00e+00	3.61e-03	5.11e-04	4.38e-04	4.04e-03	2.30e-03	7.79e-02		6.57e-01
9	811940	811323	144602	141989	8.41e-03	4.67e-02	4.84e-03	1.68e-03	1.24e-03	0.00e+00	3.82e-03	6.08e-04	4.54e-04	3.93e-03	2.30e-03	7.40e-02		7.31e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.3774 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.3774 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.377387  0.000000
];

