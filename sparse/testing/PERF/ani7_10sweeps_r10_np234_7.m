% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_234 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	823246	811728	141586	141586	1.05e-02	4.30e-02	8.89e-03	2.19e-02	2.05e-03	0.00e+00	2.32e-03	6.48e-04	3.54e-04	1.14e-02	1.71e-03	1.03e-01		1.03e-01
1	795464	796515	124972	136490	1.06e-02	3.61e-02	3.44e-03	6.38e-03	2.03e-03	0.00e+00	3.13e-03	4.61e-04	3.93e-04	7.18e-03	1.88e-03	7.16e-02		1.74e-01
2	794092	797768	153560	152509	1.03e-02	5.15e-02	3.63e-03	1.52e-03	1.05e-03	0.00e+00	3.19e-03	6.26e-04	4.00e-04	4.28e-03	1.98e-03	7.85e-02		2.53e-01
3	811715	809834	155738	152062	1.03e-02	4.94e-02	3.87e-03	2.73e-03	3.45e-04	0.00e+00	3.58e-03	5.50e-04	4.07e-04	4.25e-03	2.12e-03	7.75e-02		3.30e-01
4	807375	813139	138921	140802	1.05e-02	5.67e-02	5.14e-03	3.53e-03	4.74e-04	0.00e+00	3.87e-03	5.69e-04	4.71e-04	4.28e-03	2.20e-03	8.77e-02		4.18e-01
5	811882	813428	144066	138302	1.05e-02	5.24e-02	4.46e-03	2.26e-03	4.17e-04	0.00e+00	3.82e-03	5.21e-04	4.29e-04	4.81e-03	2.22e-03	8.19e-02		5.00e-01
6	802868	811329	140365	138819	1.06e-02	5.88e-02	4.55e-03	1.57e-03	3.84e-04	0.00e+00	3.83e-03	5.19e-04	4.34e-04	4.35e-03	2.23e-03	8.72e-02		5.87e-01
7	814376	810756	150185	141724	1.05e-02	5.23e-02	4.49e-03	1.59e-03	4.16e-04	0.00e+00	3.80e-03	7.76e-04	4.44e-04	4.99e-03	2.21e-03	8.16e-02		6.69e-01
8	814357	812391	139483	143103	1.05e-02	5.78e-02	4.61e-03	2.49e-03	1.49e-03	0.00e+00	3.76e-03	7.83e-04	4.19e-04	4.31e-03	2.25e-03	8.85e-02		7.57e-01
9	813960	812571	140308	142274	1.05e-02	5.43e-02	4.62e-03	2.45e-03	1.67e-03	0.00e+00	3.74e-03	7.30e-04	4.19e-04	4.34e-03	2.28e-03	8.50e-02		8.42e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.5904 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.5904 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.590442  0.000000
];

