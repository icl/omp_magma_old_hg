% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_130 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	815629	811180	141586	141586	5.96e-03	2.79e-02	5.74e-03	1.09e-02	6.60e-04	0.00e+00	2.79e-03	3.64e-04	3.44e-04	7.30e-03	1.75e-03	6.36e-02		6.36e-02
1	816468	810234	132589	137038	6.00e-03	2.72e-02	3.13e-03	3.57e-03	8.88e-04	0.00e+00	3.47e-03	2.96e-04	3.71e-04	4.87e-03	2.08e-03	5.19e-02		1.16e-01
2	819694	799924	132556	138790	6.00e-03	3.79e-02	3.76e-03	2.01e-03	2.79e-04	0.00e+00	3.99e-03	3.68e-04	4.68e-04	3.02e-03	2.34e-03	6.01e-02		1.76e-01
3	820911	815297	130136	149906	5.89e-03	4.75e-02	4.89e-03	1.78e-03	3.32e-04	0.00e+00	4.60e-03	4.11e-04	4.58e-04	3.01e-03	2.79e-03	7.17e-02		2.47e-01
4	796828	806008	129725	135339	8.03e-03	5.06e-02	5.74e-03	2.52e-03	4.95e-04	0.00e+00	4.70e-03	4.09e-04	4.62e-04	3.49e-03	2.81e-03	7.93e-02		3.27e-01
5	812612	808633	154613	145433	7.97e-03	4.43e-02	5.77e-03	1.55e-03	6.37e-04	0.00e+00	4.78e-03	3.84e-04	4.91e-04	3.29e-03	2.86e-03	7.20e-02		3.99e-01
6	812142	811042	139635	143614	7.99e-03	4.95e-02	5.92e-03	1.51e-03	4.29e-04	0.00e+00	4.83e-03	4.74e-04	4.57e-04	3.29e-03	2.84e-03	7.72e-02		4.76e-01
7	812777	810897	140911	142011	8.00e-03	4.69e-02	5.86e-03	2.43e-03	4.63e-04	0.00e+00	4.74e-03	4.95e-04	4.83e-04	3.01e-03	2.78e-03	7.52e-02		5.51e-01
8	813114	813426	141082	142962	7.96e-03	5.05e-02	4.42e-03	1.80e-03	6.45e-04	0.00e+00	4.83e-03	5.07e-04	4.72e-04	3.15e-03	2.27e-03	7.66e-02		6.28e-01
9	811549	816667	141551	141239	6.02e-03	4.77e-02	4.43e-03	1.87e-03	8.72e-04	0.00e+00	4.83e-03	3.72e-04	4.89e-04	2.95e-03	2.28e-03	7.18e-02		6.99e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.1856 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.1856 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.185639  0.000000
];

