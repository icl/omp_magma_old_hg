% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_116 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	813989	812761	141586	141586	5.79e-03	2.79e-02	5.77e-03	9.86e-03	1.03e-03	0.00e+00	2.32e-03	4.06e-04	3.69e-04	6.19e-03	1.94e-03	6.15e-02		6.15e-02
1	794301	816086	134229	135457	5.85e-03	2.83e-02	3.39e-03	2.85e-03	1.28e-03	0.00e+00	2.86e-03	3.37e-04	4.35e-04	4.25e-03	2.27e-03	5.18e-02		1.13e-01
2	814821	798028	154723	132938	5.86e-03	3.98e-02	4.01e-03	1.55e-03	5.35e-04	0.00e+00	3.22e-03	4.62e-04	4.45e-04	3.11e-03	2.51e-03	6.15e-02		1.75e-01
3	812193	806275	135009	151802	5.73e-03	4.58e-02	4.47e-03	1.92e-03	3.66e-04	0.00e+00	3.68e-03	4.15e-04	4.95e-04	3.05e-03	2.47e-03	6.84e-02		2.43e-01
4	813611	807763	138443	144361	5.79e-03	4.56e-02	4.65e-03	1.70e-03	4.83e-04	0.00e+00	4.06e-03	5.04e-04	5.13e-04	3.01e-03	2.43e-03	6.87e-02		3.12e-01
5	809591	806194	137830	143678	5.80e-03	4.35e-02	4.74e-03	4.13e-03	4.51e-04	0.00e+00	3.97e-03	3.49e-04	4.97e-04	3.43e-03	2.47e-03	6.93e-02		3.81e-01
6	812157	812176	142656	146053	5.76e-03	4.54e-02	4.71e-03	2.20e-03	3.99e-04	0.00e+00	4.07e-03	4.53e-04	4.62e-04	3.14e-03	2.52e-03	6.91e-02		4.50e-01
7	810641	811860	140896	140877	5.83e-03	4.46e-02	4.87e-03	1.80e-03	3.87e-04	0.00e+00	3.88e-03	3.90e-04	4.67e-04	3.37e-03	2.51e-03	6.81e-02		5.18e-01
8	812685	811987	143218	141999	5.82e-03	4.65e-02	4.86e-03	3.33e-03	1.00e-03	0.00e+00	4.84e-03	7.41e-04	4.94e-04	3.09e-03	2.49e-03	7.32e-02		5.92e-01
9	809805	813047	141980	142678	5.83e-03	4.49e-02	4.89e-03	2.76e-03	8.94e-04	0.00e+00	4.05e-03	4.16e-04	5.01e-04	3.02e-03	2.50e-03	6.98e-02		6.61e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.1413 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.1413 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.141261  0.000000
];

