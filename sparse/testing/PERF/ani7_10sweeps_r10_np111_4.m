% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_111 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	823237	805970	141586	141586	5.78e-03	2.87e-02	5.78e-03	1.02e-02	3.92e-04	0.00e+00	2.42e-03	4.13e-04	3.84e-04	7.61e-03	2.00e-03	6.37e-02		6.37e-02
1	800612	815517	124981	142248	5.75e-03	2.76e-02	3.56e-03	5.60e-03	5.21e-04	0.00e+00	2.87e-03	2.98e-04	4.20e-04	4.87e-03	2.42e-03	5.39e-02		1.18e-01
2	793098	823621	148412	133507	5.78e-03	4.20e-02	4.21e-03	3.78e-03	2.85e-04	0.00e+00	3.39e-03	4.59e-04	4.35e-04	3.23e-03	2.60e-03	6.62e-02		1.84e-01
3	803559	809608	156732	126209	5.87e-03	4.49e-02	4.62e-03	1.75e-03	5.79e-04	0.00e+00	3.85e-03	4.52e-04	4.94e-04	3.08e-03	2.42e-03	6.80e-02		2.52e-01
4	820793	806069	147077	141028	5.75e-03	4.51e-02	4.70e-03	1.48e-03	5.46e-04	0.00e+00	4.35e-03	4.55e-04	4.81e-04	3.59e-03	2.49e-03	6.89e-02		3.21e-01
5	807414	808886	130648	145372	5.76e-03	4.33e-02	4.87e-03	1.57e-03	7.79e-04	0.00e+00	4.22e-03	4.37e-04	5.05e-04	3.38e-03	2.44e-03	6.73e-02		3.88e-01
6	807666	809074	144833	143361	5.75e-03	4.47e-02	4.86e-03	1.36e-03	5.20e-04	0.00e+00	4.41e-03	4.99e-04	4.85e-04	3.50e-03	2.42e-03	6.85e-02		4.56e-01
7	810461	809325	145387	143979	5.76e-03	4.27e-02	4.85e-03	2.50e-03	4.06e-04	0.00e+00	4.17e-03	4.57e-04	5.05e-04	3.13e-03	2.48e-03	6.69e-02		5.23e-01
8	811435	812080	143398	144534	5.76e-03	4.49e-02	4.91e-03	1.52e-03	6.87e-04	0.00e+00	4.10e-03	5.14e-04	4.68e-04	3.24e-03	2.50e-03	6.86e-02		5.92e-01
9	815137	813058	143230	142585	5.80e-03	4.35e-02	4.94e-03	1.54e-03	9.52e-04	0.00e+00	4.28e-03	3.72e-04	5.05e-04	3.07e-03	2.50e-03	6.75e-02		6.59e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.1416 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.1416 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.141627  0.000000
];

