% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_184 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	812808	815793	141586	141586	8.51e-03	3.64e-02	7.60e-03	1.51e-02	6.50e-04	0.00e+00	2.14e-03	6.86e-04	3.79e-04	9.71e-03	1.83e-03	8.31e-02		8.31e-02
1	807431	813213	135410	132425	8.61e-03	3.16e-02	3.41e-03	2.06e-03	6.10e-04	0.00e+00	2.51e-03	4.24e-04	3.68e-04	5.97e-03	2.05e-03	5.76e-02		1.41e-01
2	797830	812681	141593	135811	8.56e-03	4.50e-02	3.91e-03	3.06e-03	2.89e-04	0.00e+00	2.84e-03	5.95e-04	4.31e-04	3.92e-03	2.18e-03	7.08e-02		2.11e-01
3	813610	806448	152000	137149	8.51e-03	4.74e-02	4.27e-03	3.15e-03	4.42e-04	0.00e+00	3.31e-03	5.24e-04	4.51e-04	3.87e-03	2.19e-03	7.41e-02		2.86e-01
4	808956	805879	137026	144188	8.49e-03	5.15e-02	4.98e-03	5.03e-03	4.94e-04	0.00e+00	3.85e-03	6.58e-04	4.85e-04	4.94e-03	2.38e-03	8.28e-02		3.68e-01
5	813723	812569	142485	145562	1.06e-02	4.66e-02	5.02e-03	2.05e-03	1.06e-03	0.00e+00	3.44e-03	7.25e-04	4.53e-04	4.28e-03	2.50e-03	7.67e-02		4.45e-01
6	808820	815494	138524	139678	1.06e-02	5.06e-02	5.15e-03	2.06e-03	4.92e-04	0.00e+00	3.41e-03	5.94e-04	4.64e-04	7.27e-03	2.45e-03	8.31e-02		5.28e-01
7	813845	810203	144233	137559	1.06e-02	4.71e-02	5.18e-03	2.17e-03	4.95e-04	0.00e+00	3.41e-03	6.81e-04	4.54e-04	3.91e-03	2.52e-03	7.65e-02		6.05e-01
8	813051	808461	140014	143656	1.06e-02	5.02e-02	5.17e-03	2.38e-03	1.48e-03	0.00e+00	3.47e-03	6.46e-04	4.59e-04	4.26e-03	2.47e-03	8.11e-02		6.86e-01
9	813532	813084	141614	146204	1.06e-02	5.50e-02	4.67e-03	2.38e-03	1.01e-03	0.00e+00	4.17e-03	7.22e-04	4.53e-04	3.93e-03	2.24e-03	8.51e-02		7.71e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.4048 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.4048 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.404780  0.000000
];

