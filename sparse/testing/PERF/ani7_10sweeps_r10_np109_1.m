% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_109 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	813102	814668	141586	141586	5.70e-03	2.90e-02	5.88e-03	9.96e-03	5.97e-04	0.00e+00	2.44e-03	3.80e-04	3.96e-04	7.14e-03	2.06e-03	6.35e-02		6.35e-02
1	807336	812871	135116	133550	5.78e-03	2.78e-02	3.66e-03	3.02e-03	9.32e-04	0.00e+00	3.11e-03	2.81e-04	4.19e-04	4.56e-03	2.41e-03	5.20e-02		1.16e-01
2	795239	805693	141688	136153	5.74e-03	4.25e-02	4.24e-03	2.11e-03	3.30e-04	0.00e+00	3.45e-03	3.12e-04	4.31e-04	3.22e-03	2.54e-03	6.49e-02		1.80e-01
3	811915	800176	154591	144137	5.71e-03	4.20e-02	4.54e-03	2.62e-03	4.16e-04	0.00e+00	4.05e-03	3.46e-04	4.30e-04	3.12e-03	2.47e-03	6.57e-02		2.46e-01
4	799834	817021	138721	150460	5.66e-03	4.54e-02	4.68e-03	1.74e-03	4.86e-04	0.00e+00	4.28e-03	4.78e-04	4.49e-04	3.32e-03	2.48e-03	6.90e-02		3.15e-01
5	807399	811806	151607	134420	5.78e-03	4.22e-02	4.90e-03	1.37e-03	5.53e-04	0.00e+00	4.19e-03	3.82e-04	4.33e-04	3.50e-03	2.51e-03	6.59e-02		3.81e-01
6	812163	809580	144848	140441	5.76e-03	4.55e-02	4.93e-03	2.88e-03	4.67e-04	0.00e+00	4.22e-03	4.68e-04	4.69e-04	3.34e-03	2.51e-03	7.06e-02		4.51e-01
7	813366	811384	140890	143473	5.73e-03	4.34e-02	4.93e-03	1.71e-03	4.51e-04	0.00e+00	4.17e-03	4.43e-04	4.46e-04	3.09e-03	2.54e-03	6.69e-02		5.18e-01
8	813671	811877	140493	142475	5.74e-03	4.62e-02	5.00e-03	1.83e-03	6.25e-04	0.00e+00	4.17e-03	5.29e-04	4.28e-04	3.30e-03	2.53e-03	7.04e-02		5.89e-01
9	814164	813378	140994	142788	5.76e-03	4.43e-02	5.04e-03	2.25e-03	1.01e-03	0.00e+00	4.30e-03	4.16e-04	4.73e-04	3.13e-03	2.57e-03	6.92e-02		6.58e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.1402 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.1402 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.140210  0.000000
];

