% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_2 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	821969	804844	141586	141586	6.83e-03	4.72e-01	1.08e-01	2.33e-02	6.81e-03	0.00e+00	9.60e-02	2.40e-04	1.12e-02	3.80e-02	6.97e-02	8.33e-01		8.33e-01
1	808967	805304	126249	143374	7.55e-03	5.88e-01	1.09e-01	2.00e-02	8.90e-03	0.00e+00	1.11e-01	2.40e-04	1.17e-02	3.69e-02	8.46e-02	9.79e-01		1.81e+00
2	814290	803741	140057	143720	7.85e-03	8.72e-01	1.32e-01	2.08e-02	9.45e-03	0.00e+00	1.33e-01	2.44e-04	1.28e-02	3.64e-02	9.00e-02	1.31e+00		3.13e+00
3	812992	811910	135540	146089	7.68e-03	1.10e+00	1.44e-01	2.31e-02	1.15e-02	0.00e+00	1.54e-01	2.08e-04	1.39e-02	3.59e-02	8.80e-02	1.58e+00		4.70e+00
4	808406	795543	137644	138726	7.73e-03	1.10e+00	1.52e-01	2.29e-02	1.28e-02	0.00e+00	1.53e-01	4.45e-04	1.39e-02	3.53e-02	8.35e-02	1.59e+00		6.29e+00
5	813447	804577	143035	155898	7.50e-03	1.08e+00	1.49e-01	2.44e-02	1.34e-02	0.00e+00	1.52e-01	3.15e-04	1.38e-02	3.72e-02	8.54e-02	1.56e+00		7.85e+00
6	812911	809529	138800	147670	7.69e-03	1.14e+00	1.54e-01	2.36e-02	1.34e-02	0.00e+00	1.56e-01	2.44e-04	1.40e-02	3.59e-02	8.59e-02	1.63e+00		9.48e+00
7	810230	812933	140142	143524	7.72e-03	1.16e+00	1.56e-01	2.37e-02	1.31e-02	0.00e+00	1.55e-01	3.59e-04	1.40e-02	3.63e-02	8.63e-02	1.65e+00		1.11e+01
8	812005	814596	143629	140926	7.75e-03	1.17e+00	1.56e-01	2.30e-02	1.32e-02	0.00e+00	1.52e-01	3.15e-04	1.38e-02	3.61e-02	8.66e-02	1.66e+00		1.28e+01
9	813642	813883	142660	140069	7.79e-03	1.18e+00	1.58e-01	2.31e-02	1.38e-02	0.00e+00	1.54e-01	3.68e-04	1.39e-02	3.59e-02	8.67e-02	1.67e+00		1.45e+01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 15.3171 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 15.3171 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  15.317102  0.000000
];

