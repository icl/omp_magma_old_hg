% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_129 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	811397	808984	141586	141586	6.82e-03	4.10e-02	1.02e-02	1.36e-02	1.02e-03	0.00e+00	3.15e-03	3.84e-04	4.05e-04	1.62e-02	1.97e-03	9.49e-02		9.49e-02
1	801213	793047	136821	139234	6.72e-03	3.60e-02	3.51e-03	4.95e-03	2.30e-03	0.00e+00	3.67e-03	3.51e-04	3.82e-04	8.03e-03	2.37e-03	6.83e-02		1.63e-01
2	792078	815693	147811	155977	6.93e-03	5.06e-02	3.82e-03	1.86e-03	4.57e-04	0.00e+00	4.12e-03	3.09e-04	4.38e-04	3.22e-03	2.55e-03	7.43e-02		2.37e-01
3	810907	801635	157752	134137	6.46e-03	5.50e-02	4.36e-03	2.27e-03	4.73e-04	0.00e+00	4.81e-03	5.06e-04	5.09e-04	3.18e-03	2.49e-03	8.00e-02		3.18e-01
4	807207	802242	139729	149001	6.29e-03	5.89e-02	4.78e-03	1.70e-03	5.30e-04	0.00e+00	5.20e-03	4.78e-04	5.16e-04	3.51e-03	2.31e-03	8.42e-02		4.02e-01
5	808078	812993	144234	149199	6.67e-03	5.44e-02	4.55e-03	1.89e-03	5.11e-04	0.00e+00	5.20e-03	4.61e-04	4.96e-04	3.58e-03	2.38e-03	8.01e-02		4.82e-01
6	808377	809372	144169	139254	6.59e-03	5.95e-02	4.38e-03	2.22e-03	6.07e-04	0.00e+00	5.19e-03	4.14e-04	5.21e-04	3.69e-03	2.33e-03	8.54e-02		5.67e-01
7	819070	809579	144676	143681	6.99e-03	5.57e-02	4.41e-03	2.58e-03	5.61e-04	0.00e+00	5.29e-03	4.75e-04	5.23e-04	3.16e-03	2.41e-03	8.21e-02		6.49e-01
8	813268	815756	134789	144280	8.19e-03	6.29e-02	4.69e-03	1.76e-03	4.89e-04	0.00e+00	5.08e-03	5.09e-04	4.85e-04	3.52e-03	2.52e-03	9.01e-02		7.39e-01
9	812619	807277	141397	138909	6.87e-03	5.80e-02	4.61e-03	1.56e-03	1.24e-03	0.00e+00	4.97e-03	4.52e-04	5.14e-04	3.18e-03	2.41e-03	8.38e-02		8.23e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.3461 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.3461 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.346101  0.000000
];

