% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_140 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	818184	801743	141586	141586	7.34e-03	3.30e-02	6.79e-03	1.37e-02	7.62e-04	0.00e+00	2.71e-03	4.93e-04	4.51e-04	8.86e-03	2.23e-03	7.64e-02		7.64e-02
1	799722	815887	130034	146475	7.27e-03	2.96e-02	4.21e-03	2.05e-03	9.13e-04	0.00e+00	3.11e-03	3.58e-04	4.27e-04	5.52e-03	2.32e-03	5.58e-02		1.32e-01
2	802436	795333	149302	133137	7.40e-03	4.60e-02	4.31e-03	1.87e-03	3.52e-04	0.00e+00	3.66e-03	7.11e-04	4.27e-04	4.10e-03	2.25e-03	7.11e-02		2.03e-01
3	803621	797481	147394	154497	7.19e-03	4.86e-02	4.38e-03	2.61e-03	3.34e-04	0.00e+00	4.23e-03	5.01e-04	4.75e-04	3.93e-03	2.15e-03	7.44e-02		2.78e-01
4	809892	808891	147015	153155	7.26e-03	4.91e-02	4.81e-03	3.42e-03	5.89e-04	0.00e+00	4.46e-03	4.11e-04	4.99e-04	4.14e-03	2.13e-03	7.68e-02		3.55e-01
5	812074	805866	141549	142550	7.37e-03	4.61e-02	4.99e-03	2.23e-03	1.27e-03	0.00e+00	4.51e-03	5.35e-04	5.17e-04	4.33e-03	2.09e-03	7.40e-02		4.28e-01
6	807382	813542	140173	146381	7.35e-03	5.00e-02	4.96e-03	3.55e-03	4.90e-04	0.00e+00	4.52e-03	6.62e-04	5.19e-04	4.40e-03	2.11e-03	7.86e-02		5.07e-01
7	812331	810890	145671	139511	7.38e-03	4.74e-02	4.93e-03	3.59e-03	4.68e-04	0.00e+00	4.45e-03	5.38e-04	4.79e-04	4.19e-03	2.15e-03	7.55e-02		5.83e-01
8	813037	812386	141528	142969	7.35e-03	5.11e-02	5.01e-03	1.90e-03	9.60e-04	0.00e+00	4.58e-03	4.48e-04	5.11e-04	4.09e-03	2.16e-03	7.82e-02		6.61e-01
9	812716	812694	141628	142279	7.41e-03	4.87e-02	5.54e-03	1.40e-03	1.03e-03	0.00e+00	4.55e-03	6.60e-04	5.03e-04	4.04e-03	2.74e-03	7.65e-02		7.37e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.3805 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.3805 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.380546  0.000000
];

