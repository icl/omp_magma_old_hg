% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_29 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	817230	801062	141586	141586	3.57e-03	4.44e-02	8.47e-03	7.18e-03	1.24e-03	0.00e+00	8.67e-03	2.97e-04	9.50e-04	6.96e-03	5.08e-03	8.69e-02		8.69e-02
1	816060	812818	130988	147156	3.59e-03	5.42e-02	7.65e-03	3.09e-03	1.30e-03	0.00e+00	1.07e-02	2.41e-04	9.87e-04	5.75e-03	6.61e-03	9.41e-02		1.81e-01
2	814101	808731	132964	136206	3.64e-03	8.49e-02	1.01e-02	2.56e-03	7.55e-04	0.00e+00	1.30e-02	3.70e-04	1.11e-03	4.72e-03	7.23e-03	1.28e-01		3.09e-01
3	816749	790046	135729	141099	3.66e-03	1.28e-01	1.14e-02	2.94e-03	1.05e-03	0.00e+00	1.50e-02	2.94e-04	1.29e-03	4.84e-03	6.64e-03	1.75e-01		4.85e-01
4	809902	813291	133887	160590	3.57e-03	1.20e-01	1.14e-02	2.58e-03	1.50e-03	0.00e+00	1.58e-02	3.24e-04	1.37e-03	4.99e-03	6.62e-03	1.68e-01		6.53e-01
5	807548	806848	141539	138150	3.65e-03	1.20e-01	1.18e-02	3.38e-03	1.58e-03	0.00e+00	1.67e-02	4.25e-04	1.31e-03	5.37e-03	6.63e-03	1.71e-01		8.23e-01
6	812020	806489	144699	145399	3.63e-03	1.20e-01	1.17e-02	2.32e-03	1.42e-03	0.00e+00	1.62e-02	4.08e-04	1.33e-03	4.85e-03	6.63e-03	1.69e-01		9.92e-01
7	809951	813913	141033	146564	3.63e-03	1.22e-01	1.18e-02	2.57e-03	1.67e-03	0.00e+00	1.67e-02	3.26e-04	1.33e-03	4.91e-03	6.73e-03	1.71e-01		1.16e+00
8	812668	810819	143908	139946	3.64e-03	1.25e-01	1.19e-02	2.97e-03	1.43e-03	0.00e+00	1.63e-02	3.33e-04	1.29e-03	4.97e-03	6.69e-03	1.74e-01		1.34e+00
9	813110	814061	141997	143846	3.67e-03	1.24e-01	1.19e-02	2.56e-03	1.97e-03	0.00e+00	1.58e-02	4.97e-04	1.24e-03	4.83e-03	6.72e-03	1.73e-01		1.51e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.9030 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.9030 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.902999  0.000000
];

