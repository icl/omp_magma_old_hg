% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_41 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	813274	807792	141586	141586	3.31e-03	3.34e-02	6.04e-03	6.68e-03	6.27e-04	0.00e+00	5.83e-03	2.91e-04	6.19e-04	5.99e-03	3.43e-03	6.62e-02		6.62e-02
1	794254	815355	134944	140426	3.28e-03	3.92e-02	5.13e-03	2.31e-03	7.18e-04	0.00e+00	7.14e-03	2.25e-04	6.90e-04	4.47e-03	4.40e-03	6.75e-02		1.34e-01
2	805076	815990	154770	133669	3.36e-03	6.15e-02	6.61e-03	2.42e-03	5.95e-04	0.00e+00	8.65e-03	2.28e-04	7.73e-04	3.82e-03	4.85e-03	9.28e-02		2.27e-01
3	805594	810962	144754	133840	3.35e-03	8.91e-02	7.81e-03	2.07e-03	6.95e-04	0.00e+00	1.03e-02	2.84e-04	8.55e-04	3.82e-03	4.50e-03	1.23e-01		3.49e-01
4	820358	810300	145042	139674	3.33e-03	8.53e-02	7.72e-03	1.98e-03	9.23e-04	0.00e+00	1.08e-02	3.25e-04	8.68e-04	4.14e-03	4.53e-03	1.20e-01		4.69e-01
5	810056	803191	131083	141141	3.39e-03	8.45e-02	7.92e-03	1.87e-03	1.20e-03	0.00e+00	1.11e-02	3.48e-04	8.94e-04	3.93e-03	4.49e-03	1.20e-01		5.89e-01
6	807299	815294	142191	149056	3.29e-03	8.57e-02	7.81e-03	2.16e-03	1.12e-03	0.00e+00	1.13e-02	3.27e-04	9.21e-04	3.97e-03	4.49e-03	1.21e-01		7.10e-01
7	812657	811163	145754	137759	3.37e-03	8.56e-02	7.92e-03	1.96e-03	9.28e-04	0.00e+00	1.10e-02	3.56e-04	8.74e-04	3.84e-03	4.50e-03	1.20e-01		8.30e-01
8	812791	812460	141202	142696	3.34e-03	8.83e-02	7.99e-03	2.15e-03	1.36e-03	0.00e+00	1.12e-02	2.90e-04	9.51e-04	4.00e-03	4.53e-03	1.24e-01		9.54e-01
9	810355	814571	141874	142205	3.36e-03	8.75e-02	8.03e-03	2.09e-03	1.50e-03	0.00e+00	1.11e-02	3.91e-04	8.96e-04	3.81e-03	4.49e-03	1.23e-01		1.08e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.4494 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.4494 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.449441  0.000000
];

