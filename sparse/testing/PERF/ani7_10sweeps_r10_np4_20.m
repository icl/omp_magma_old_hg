% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_4 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	830286	810681	141586	141586	5.17e-03	2.47e-01	5.50e-02	1.47e-02	3.49e-03	0.00e+00	5.35e-02	2.68e-04	5.74e-03	2.18e-02	3.55e-02	4.43e-01		4.43e-01
1	799976	787902	117932	137537	5.45e-03	3.00e-01	5.52e-02	1.29e-02	5.04e-03	0.00e+00	6.33e-02	2.33e-04	6.10e-03	2.01e-02	4.40e-02	5.13e-01		9.55e-01
2	798180	818165	149048	161122	5.61e-03	5.11e-01	6.63e-02	1.10e-02	5.36e-03	0.00e+00	7.65e-02	3.65e-04	6.71e-03	2.08e-02	4.95e-02	7.53e-01		1.71e+00
3	812987	810373	151650	131665	5.75e-03	7.33e-01	7.75e-02	1.23e-02	6.66e-03	0.00e+00	9.18e-02	3.51e-04	8.11e-03	2.03e-02	4.72e-02	1.00e+00		2.71e+00
4	816714	801539	137649	140263	5.69e-03	6.98e-01	8.08e-02	1.22e-02	7.82e-03	0.00e+00	9.03e-02	4.22e-04	7.90e-03	2.00e-02	4.56e-02	9.69e-01		3.68e+00
5	806413	811952	134727	149902	5.60e-03	6.84e-01	8.09e-02	1.22e-02	8.11e-03	0.00e+00	9.08e-02	4.16e-04	7.88e-03	2.05e-02	4.57e-02	9.56e-01		4.64e+00
6	812609	813333	145834	140295	5.68e-03	6.96e-01	8.12e-02	1.21e-02	7.72e-03	0.00e+00	8.98e-02	3.54e-04	7.74e-03	2.02e-02	4.62e-02	9.67e-01		5.60e+00
7	813550	812978	140444	139720	5.69e-03	7.11e-01	8.25e-02	1.26e-02	8.31e-03	0.00e+00	9.41e-02	3.41e-04	8.01e-03	2.06e-02	4.62e-02	9.89e-01		6.59e+00
8	812754	809404	140309	140881	5.69e-03	7.16e-01	8.27e-02	1.20e-02	8.16e-03	0.00e+00	8.92e-02	3.68e-04	7.81e-03	2.01e-02	4.60e-02	9.88e-01		7.58e+00
9	814744	815195	141911	145261	5.69e-03	7.13e-01	8.24e-02	1.19e-02	8.44e-03	0.00e+00	9.17e-02	3.31e-04	7.90e-03	2.03e-02	4.63e-02	9.87e-01		8.57e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 9.1840 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 9.1840 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  9.183969  0.000000
];

