% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_29 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	817293	801026	141586	141586	3.58e-03	4.44e-02	8.47e-03	7.15e-03	1.17e-03	0.00e+00	8.71e-03	2.98e-04	9.34e-04	6.99e-03	5.05e-03	8.68e-02		8.68e-02
1	815672	795575	130925	147192	3.56e-03	5.43e-02	7.67e-03	5.04e-03	1.34e-03	0.00e+00	1.09e-02	2.10e-04	1.01e-03	5.74e-03	6.89e-03	9.67e-02		1.83e-01
2	812390	813065	133352	153449	4.84e-03	8.50e-02	1.00e-02	2.80e-03	8.91e-04	0.00e+00	1.31e-02	3.72e-04	1.10e-03	4.85e-03	7.24e-03	1.30e-01		3.14e-01
3	806681	815108	137440	136765	3.68e-03	1.29e-01	1.16e-02	2.11e-03	1.02e-03	0.00e+00	1.50e-02	2.74e-04	1.21e-03	4.83e-03	6.84e-03	1.75e-01		4.89e-01
4	807463	809120	143955	135528	3.70e-03	1.23e-01	1.17e-02	2.78e-03	1.43e-03	0.00e+00	1.63e-02	4.58e-04	1.31e-03	4.90e-03	6.59e-03	1.72e-01		6.61e-01
5	810604	807706	143978	142321	3.62e-03	1.18e-01	1.16e-02	2.74e-03	1.61e-03	0.00e+00	1.65e-02	4.52e-04	1.33e-03	5.30e-03	6.61e-03	1.68e-01		8.29e-01
6	812453	809366	141643	144541	3.64e-03	1.20e-01	1.17e-02	2.48e-03	1.50e-03	0.00e+00	1.63e-02	3.53e-04	1.32e-03	4.99e-03	6.62e-03	1.69e-01		9.98e-01
7	812768	809628	140600	143687	3.66e-03	1.21e-01	1.18e-02	3.63e-03	1.52e-03	0.00e+00	1.64e-02	3.81e-04	1.32e-03	4.83e-03	6.66e-03	1.71e-01		1.17e+00
8	811751	811294	141091	144231	3.64e-03	1.23e-01	1.19e-02	2.61e-03	1.43e-03	0.00e+00	1.62e-02	3.76e-04	1.31e-03	5.00e-03	6.67e-03	1.72e-01		1.34e+00
9	817988	810836	142914	143371	3.70e-03	1.23e-01	1.19e-02	2.85e-03	2.01e-03	0.00e+00	1.64e-02	2.93e-04	1.29e-03	4.85e-03	6.71e-03	1.73e-01		1.52e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.9048 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.9048 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.904843  0.000000
];

