% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_188 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	821876	814766	141586	141586	9.19e-03	5.63e-02	1.37e-02	1.99e-02	3.00e-03	0.00e+00	2.95e-03	5.22e-04	3.44e-04	2.14e-02	1.77e-03	1.29e-01		1.29e-01
1	813552	816226	126342	133452	9.34e-03	4.29e-02	3.44e-03	4.15e-03	2.10e-03	0.00e+00	3.72e-03	4.43e-04	3.69e-04	1.12e-02	2.40e-03	8.01e-02		2.09e-01
2	794250	791697	135472	132798	9.40e-03	6.60e-02	4.15e-03	2.08e-03	5.77e-04	0.00e+00	3.98e-03	5.24e-04	5.09e-04	4.17e-03	2.31e-03	9.37e-02		3.03e-01
3	816811	808894	155580	158133	9.09e-03	5.90e-02	4.34e-03	3.01e-03	5.04e-04	0.00e+00	4.35e-03	6.02e-04	5.06e-04	4.12e-03	2.40e-03	8.79e-02		3.91e-01
4	801812	808947	133825	141742	9.85e-03	7.11e-02	5.35e-03	2.10e-03	6.00e-04	0.00e+00	4.60e-03	5.79e-04	5.13e-04	4.23e-03	2.28e-03	1.01e-01		4.92e-01
5	814152	810648	149629	142494	9.27e-03	5.98e-02	4.56e-03	2.31e-03	5.64e-04	0.00e+00	4.73e-03	4.26e-04	4.91e-04	5.04e-03	2.32e-03	8.95e-02		5.82e-01
6	809479	809707	138095	141599	9.32e-03	6.98e-02	4.69e-03	2.35e-03	7.27e-04	0.00e+00	4.82e-03	6.67e-04	5.24e-04	4.38e-03	2.35e-03	9.96e-02		6.81e-01
7	809982	813390	143574	143346	9.31e-03	6.17e-02	4.67e-03	1.66e-03	6.13e-04	0.00e+00	4.72e-03	6.34e-04	4.85e-04	4.02e-03	2.36e-03	9.02e-02		7.71e-01
8	816054	813659	143877	140469	9.34e-03	6.97e-02	4.80e-03	1.86e-03	6.55e-04	0.00e+00	4.73e-03	5.76e-04	5.15e-04	4.29e-03	2.45e-03	9.89e-02		8.70e-01
9	815400	810680	138611	141006	9.36e-03	6.36e-02	5.06e-03	3.24e-03	1.37e-03	0.00e+00	4.80e-03	5.96e-04	4.90e-04	4.15e-03	2.39e-03	9.50e-02		9.65e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.6201 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.6201 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.620112  0.000000
];

