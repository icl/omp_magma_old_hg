% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_254 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	815911	812923	141586	141586	1.12e-02	4.38e-02	9.22e-03	2.14e-02	1.85e-03	0.00e+00	2.21e-03	5.18e-04	3.31e-04	1.33e-02	1.64e-03	1.05e-01		1.05e-01
1	803755	804059	132307	135295	1.12e-02	3.67e-02	3.18e-03	4.82e-03	1.71e-03	0.00e+00	2.73e-03	4.85e-04	3.54e-04	7.08e-03	1.89e-03	7.02e-02		1.76e-01
2	803236	806286	145269	144965	1.11e-02	5.10e-02	3.65e-03	2.39e-03	7.96e-04	0.00e+00	3.01e-03	5.57e-04	3.90e-04	4.15e-03	2.03e-03	7.90e-02		2.55e-01
3	810000	811000	146594	143544	1.11e-02	5.16e-02	4.02e-03	3.97e-03	3.80e-04	0.00e+00	3.35e-03	6.17e-04	4.16e-04	4.18e-03	2.05e-03	8.16e-02		3.36e-01
4	810139	812372	140636	139636	1.11e-02	5.64e-02	4.21e-03	1.92e-03	5.26e-04	0.00e+00	3.60e-03	7.67e-04	4.32e-04	4.32e-03	2.01e-03	8.53e-02		4.22e-01
5	810820	807444	141302	139069	1.11e-02	5.13e-02	4.12e-03	2.32e-03	4.03e-04	0.00e+00	3.49e-03	6.11e-04	4.61e-04	4.45e-03	2.01e-03	8.03e-02		5.02e-01
6	805307	809058	141427	144803	1.11e-02	5.57e-02	4.16e-03	2.70e-03	3.97e-04	0.00e+00	3.45e-03	8.06e-04	4.48e-04	4.32e-03	2.01e-03	8.51e-02		5.87e-01
7	814571	809757	147746	143995	1.12e-02	5.11e-02	4.16e-03	2.02e-03	4.10e-04	0.00e+00	3.56e-03	7.80e-04	4.24e-04	4.48e-03	2.00e-03	8.01e-02		6.67e-01
8	812481	815767	139288	144102	1.11e-02	5.64e-02	4.26e-03	2.02e-03	1.77e-03	0.00e+00	3.53e-03	5.90e-04	4.66e-04	4.16e-03	2.06e-03	8.64e-02		7.53e-01
9	805373	813545	142184	138898	1.12e-02	5.36e-02	4.29e-03	1.98e-03	1.65e-03	0.00e+00	3.53e-03	7.82e-04	4.57e-04	4.15e-03	2.05e-03	8.37e-02		8.37e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.5896 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.5896 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.589631  0.000000
];

