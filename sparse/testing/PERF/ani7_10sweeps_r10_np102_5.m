% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_102 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	808371	808904	141586	141586	5.61e-03	2.94e-02	5.95e-03	1.01e-02	6.75e-04	0.00e+00	2.63e-03	3.96e-04	4.22e-04	6.89e-03	2.15e-03	6.42e-02		6.42e-02
1	806241	802292	139847	139314	5.60e-03	2.77e-02	3.81e-03	2.51e-03	8.96e-04	0.00e+00	3.08e-03	3.06e-04	5.03e-04	4.61e-03	2.38e-03	5.14e-02		1.16e-01
2	808237	817987	142783	146732	5.53e-03	4.53e-02	4.39e-03	1.77e-03	4.20e-04	0.00e+00	3.67e-03	4.44e-04	4.57e-04	3.20e-03	2.56e-03	6.77e-02		1.83e-01
3	790664	807087	141593	131843	5.65e-03	5.20e-02	4.72e-03	1.62e-03	5.12e-04	0.00e+00	4.33e-03	4.70e-04	5.47e-04	3.15e-03	2.52e-03	7.55e-02		2.59e-01
4	804481	807068	159972	143549	5.57e-03	4.51e-02	4.85e-03	1.37e-03	5.39e-04	0.00e+00	4.38e-03	4.23e-04	4.89e-04	3.50e-03	2.59e-03	6.88e-02		3.28e-01
5	813275	810909	146960	144373	5.57e-03	4.61e-02	5.18e-03	2.47e-03	5.35e-04	0.00e+00	4.63e-03	3.76e-04	5.10e-04	3.53e-03	2.64e-03	7.16e-02		3.99e-01
6	810283	811408	138972	141338	5.63e-03	4.96e-02	5.37e-03	2.08e-03	4.43e-04	0.00e+00	4.56e-03	5.63e-04	5.26e-04	3.34e-03	2.65e-03	7.48e-02		4.74e-01
7	812366	813453	142770	141645	5.60e-03	4.71e-02	5.25e-03	1.80e-03	4.59e-04	0.00e+00	4.46e-03	4.97e-04	5.12e-04	3.14e-03	2.65e-03	7.15e-02		5.45e-01
8	809462	814077	141493	140406	5.59e-03	4.99e-02	5.35e-03	1.69e-03	7.75e-04	0.00e+00	4.51e-03	4.31e-04	4.82e-04	3.34e-03	2.64e-03	7.46e-02		6.20e-01
9	813371	814469	145203	140588	5.63e-03	4.72e-02	5.33e-03	1.69e-03	1.00e-03	0.00e+00	4.55e-03	5.44e-04	5.06e-04	3.12e-03	2.70e-03	7.23e-02		6.92e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.1791 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.1791 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.179110  0.000000
];

