% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_9 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	829763	798553	141586	141586	4.26e-03	1.16e-01	2.50e-02	9.68e-03	2.36e-03	0.00e+00	2.75e-02	3.25e-04	2.70e-03	1.14e-02	1.62e-02	2.15e-01		2.15e-01
1	800120	809281	118455	149665	4.23e-03	1.55e-01	2.49e-02	7.61e-03	2.81e-03	0.00e+00	3.41e-02	2.30e-04	3.12e-03	1.10e-02	2.05e-02	2.63e-01		4.79e-01
2	810711	804746	148904	139743	4.40e-03	2.38e-01	3.09e-02	6.04e-03	2.64e-03	0.00e+00	4.04e-02	3.32e-04	3.16e-03	1.04e-02	2.23e-02	3.58e-01		8.37e-01
3	813952	808863	139119	145084	4.37e-03	3.36e-01	3.51e-02	5.76e-03	3.08e-03	0.00e+00	4.66e-02	2.93e-04	3.65e-03	1.04e-02	2.10e-02	4.67e-01		1.30e+00
4	810375	810506	136684	141773	4.37e-03	3.20e-01	3.62e-02	5.97e-03	3.94e-03	0.00e+00	5.03e-02	3.90e-04	3.67e-03	1.03e-02	2.03e-02	4.56e-01		1.76e+00
5	808696	804723	141066	140935	4.34e-03	3.23e-01	3.62e-02	6.71e-03	4.07e-03	0.00e+00	5.05e-02	2.84e-04	3.69e-03	1.11e-02	2.03e-02	4.60e-01		2.22e+00
6	811799	811094	143551	147524	4.32e-03	3.24e-01	3.63e-02	7.83e-03	4.06e-03	0.00e+00	5.10e-02	3.32e-04	3.67e-03	1.05e-02	2.08e-02	4.63e-01		2.68e+00
7	808442	811334	141254	141959	4.38e-03	3.37e-01	3.70e-02	5.84e-03	3.92e-03	0.00e+00	5.01e-02	3.12e-04	3.66e-03	1.07e-02	2.06e-02	4.74e-01		3.16e+00
8	811153	814643	145417	142525	4.39e-03	3.38e-01	3.69e-02	5.97e-03	3.68e-03	0.00e+00	4.82e-02	3.32e-04	3.46e-03	1.05e-02	2.08e-02	4.72e-01		3.63e+00
9	812803	813696	143512	140022	4.42e-03	3.41e-01	3.74e-02	7.31e-03	4.12e-03	0.00e+00	5.01e-02	3.50e-04	3.65e-03	1.04e-02	2.07e-02	4.79e-01		4.11e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 4.5762 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 4.5762 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  4.576163  0.000000
];

