% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_173 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	819882	805904	141586	141586	8.34e-03	3.61e-02	7.42e-03	1.53e-02	9.49e-04	0.00e+00	2.24e-03	4.61e-04	3.60e-04	1.14e-02	1.88e-03	8.45e-02		8.45e-02
1	803974	803194	128336	142314	8.27e-03	3.06e-02	3.61e-03	3.53e-03	4.70e-04	0.00e+00	2.70e-03	3.64e-04	4.32e-04	5.61e-03	2.05e-03	5.77e-02		1.42e-01
2	800916	803513	145050	145830	8.25e-03	4.52e-02	3.94e-03	2.37e-03	4.80e-04	0.00e+00	3.07e-03	5.93e-04	4.19e-04	4.30e-03	2.16e-03	7.08e-02		2.13e-01
3	816902	800818	148914	146317	8.15e-03	4.36e-02	4.21e-03	2.98e-03	3.74e-04	0.00e+00	3.43e-03	4.51e-04	4.00e-04	3.89e-03	2.33e-03	6.99e-02		2.83e-01
4	800029	810818	133734	149818	8.33e-03	4.83e-02	4.66e-03	1.96e-03	3.45e-04	0.00e+00	3.68e-03	6.72e-04	4.72e-04	3.96e-03	2.30e-03	7.47e-02		3.58e-01
5	813813	806675	151412	140623	8.26e-03	4.30e-02	4.78e-03	2.85e-03	3.69e-04	0.00e+00	3.61e-03	6.39e-04	4.14e-04	4.27e-03	2.40e-03	7.06e-02		4.28e-01
6	806168	808154	138434	145572	8.25e-03	5.18e-02	4.90e-03	2.54e-03	3.97e-04	0.00e+00	3.63e-03	5.15e-04	4.27e-04	4.01e-03	2.31e-03	7.88e-02		5.07e-01
7	811931	813188	146885	144899	8.24e-03	4.41e-02	4.82e-03	2.63e-03	4.10e-04	0.00e+00	3.66e-03	6.57e-04	4.40e-04	4.28e-03	2.32e-03	7.15e-02		5.78e-01
8	814709	810863	141928	140671	8.32e-03	4.93e-02	5.73e-03	3.59e-03	1.01e-03	0.00e+00	3.67e-03	6.57e-04	4.14e-04	3.86e-03	2.32e-03	7.89e-02		6.57e-01
9	811871	813072	139956	143802	8.34e-03	4.49e-02	5.01e-03	4.01e-03	7.25e-04	0.00e+00	3.64e-03	6.88e-04	4.08e-04	3.93e-03	2.34e-03	7.40e-02		7.31e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.3729 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.3729 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.372871  0.000000
];

