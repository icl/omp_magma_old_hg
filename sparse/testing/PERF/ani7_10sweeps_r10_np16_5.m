% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_16 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	826731	811725	141586	141586	3.83e-03	7.09e-02	1.45e-02	7.99e-03	1.22e-03	0.00e+00	1.56e-02	2.99e-04	1.59e-03	8.74e-03	9.31e-03	1.34e-01		1.34e-01
1	805122	800398	121487	136493	3.86e-03	8.79e-02	1.41e-02	6.21e-03	2.01e-03	0.00e+00	1.94e-02	2.50e-04	1.69e-03	7.48e-03	1.17e-02	1.55e-01		2.88e-01
2	804577	812780	143902	148626	3.88e-03	1.44e-01	1.78e-02	3.62e-03	1.59e-03	0.00e+00	2.34e-02	3.00e-04	1.92e-03	7.15e-03	1.27e-02	2.16e-01		5.05e-01
3	799666	812009	145253	137050	3.94e-03	2.06e-01	2.02e-02	3.63e-03	1.90e-03	0.00e+00	2.63e-02	3.60e-04	2.17e-03	6.79e-03	1.18e-02	2.83e-01		7.87e-01
4	806349	818959	150970	138627	3.95e-03	1.89e-01	2.03e-02	5.11e-03	2.37e-03	0.00e+00	2.93e-02	2.94e-04	2.20e-03	7.11e-03	1.19e-02	2.72e-01		1.06e+00
5	803966	810885	145092	132482	3.95e-03	1.99e-01	2.08e-02	3.93e-03	2.44e-03	0.00e+00	2.94e-02	3.74e-04	2.22e-03	7.36e-03	1.17e-02	2.81e-01		1.34e+00
6	812083	814355	148281	141362	3.93e-03	2.01e-01	2.05e-02	3.62e-03	2.42e-03	0.00e+00	2.94e-02	3.74e-04	2.19e-03	7.00e-03	1.19e-02	2.83e-01		1.62e+00
7	816750	811343	140970	138698	3.92e-03	2.07e-01	2.10e-02	3.79e-03	2.35e-03	0.00e+00	2.93e-02	3.30e-04	2.18e-03	6.92e-03	1.20e-02	2.89e-01		1.91e+00
8	811141	813628	137109	142516	3.90e-03	2.16e-01	2.12e-02	4.27e-03	2.53e-03	0.00e+00	2.98e-02	2.88e-04	2.19e-03	7.08e-03	1.19e-02	2.99e-01		2.21e+00
9	812959	813256	143524	141037	3.96e-03	2.08e-01	2.11e-02	3.93e-03	2.73e-03	0.00e+00	2.90e-02	3.32e-04	2.11e-03	6.90e-03	1.19e-02	2.90e-01		2.50e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 2.9150 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 2.9150 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  2.915027  0.000000
];

