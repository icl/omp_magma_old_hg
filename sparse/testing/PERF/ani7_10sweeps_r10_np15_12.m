% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_15 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	817290	802382	141586	141586	3.87e-03	7.41e-02	1.54e-02	8.12e-03	1.75e-03	0.00e+00	1.67e-02	2.51e-04	1.72e-03	9.01e-03	9.65e-03	1.41e-01		1.41e-01
1	796307	796186	130928	145836	3.87e-03	9.33e-02	1.47e-02	5.56e-03	1.96e-03	0.00e+00	2.05e-02	2.44e-04	1.87e-03	8.06e-03	1.24e-02	1.62e-01		3.03e-01
2	822587	804757	152717	152838	3.91e-03	1.52e-01	1.87e-02	3.52e-03	1.48e-03	0.00e+00	2.48e-02	2.63e-04	1.97e-03	7.22e-03	1.37e-02	2.28e-01		5.31e-01
3	800565	824185	127243	145073	3.95e-03	2.24e-01	2.17e-02	3.78e-03	1.87e-03	0.00e+00	2.90e-02	2.94e-04	2.20e-03	7.19e-03	1.33e-02	3.08e-01		8.38e-01
4	814506	807281	150071	126451	4.07e-03	2.17e-01	2.22e-02	4.57e-03	2.47e-03	0.00e+00	3.03e-02	3.29e-04	2.35e-03	7.28e-03	1.25e-02	3.03e-01		1.14e+00
5	815910	810561	136935	144160	3.94e-03	2.22e-01	3.32e-02	4.74e-03	2.31e-03	0.00e+00	3.56e-02	3.47e-04	2.62e-03	7.85e-03	1.75e-02	3.31e-01		1.47e+00
6	806068	813117	136337	141686	6.17e-03	2.16e-01	2.24e-02	4.00e-03	2.44e-03	0.00e+00	3.12e-02	3.30e-04	2.27e-03	7.19e-03	1.27e-02	3.05e-01		1.78e+00
7	813136	809795	146985	139936	3.98e-03	2.11e-01	2.22e-02	4.32e-03	2.48e-03	0.00e+00	3.14e-02	2.63e-04	2.29e-03	7.14e-03	1.28e-02	2.98e-01		2.07e+00
8	813269	810473	140723	144064	3.94e-03	2.15e-01	2.25e-02	3.98e-03	2.26e-03	0.00e+00	3.07e-02	3.86e-04	2.28e-03	7.37e-03	1.28e-02	3.01e-01		2.38e+00
9	810542	812328	141396	144192	3.99e-03	2.14e-01	2.25e-02	4.86e-03	2.96e-03	0.00e+00	3.13e-02	3.62e-04	2.28e-03	7.14e-03	1.28e-02	3.02e-01		2.68e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 3.0971 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 3.0971 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  3.097118  0.000000
];

