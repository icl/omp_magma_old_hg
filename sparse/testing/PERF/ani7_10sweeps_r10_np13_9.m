% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_13 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	819969	800365	141586	141586	3.98e-03	8.43e-02	1.77e-02	8.51e-03	1.35e-03	0.00e+00	1.92e-02	2.94e-04	1.97e-03	9.75e-03	1.11e-02	1.58e-01		1.58e-01
1	792330	803797	128249	147853	3.97e-03	1.06e-01	1.69e-02	5.80e-03	1.90e-03	0.00e+00	2.36e-02	2.31e-04	2.27e-03	8.40e-03	1.43e-02	1.83e-01		3.41e-01
2	796815	791197	156694	145227	4.03e-03	1.74e-01	2.16e-02	4.61e-03	1.68e-03	0.00e+00	2.82e-02	2.14e-04	2.33e-03	7.87e-03	1.50e-02	2.60e-01		6.01e-01
3	814354	814632	153015	158633	3.99e-03	2.36e-01	2.35e-02	4.70e-03	2.25e-03	0.00e+00	3.24e-02	4.09e-04	2.54e-03	7.94e-03	1.53e-02	3.29e-01		9.30e-01
4	808423	809286	136282	136004	4.11e-03	2.50e-01	2.62e-02	4.72e-03	2.98e-03	0.00e+00	3.61e-02	4.13e-04	2.79e-03	8.23e-03	1.43e-02	3.50e-01		1.28e+00
5	814092	810007	143018	142155	4.06e-03	2.22e-01	2.52e-02	4.72e-03	2.85e-03	0.00e+00	3.44e-02	4.11e-04	2.54e-03	8.52e-03	1.46e-02	3.19e-01		1.60e+00
6	805062	809721	138155	142240	4.06e-03	2.36e-01	2.58e-02	4.30e-03	2.91e-03	0.00e+00	3.58e-02	4.12e-04	2.67e-03	7.90e-03	1.44e-02	3.35e-01		1.93e+00
7	808768	813154	147991	143332	4.07e-03	2.30e-01	2.55e-02	5.06e-03	2.84e-03	0.00e+00	3.58e-02	2.88e-04	2.63e-03	7.98e-03	1.46e-02	3.29e-01		2.26e+00
8	812018	814357	145091	140705	4.06e-03	2.35e-01	2.59e-02	4.40e-03	3.24e-03	0.00e+00	3.63e-02	4.35e-04	2.68e-03	8.25e-03	1.46e-02	3.35e-01		2.60e+00
9	813631	813811	142647	140308	4.12e-03	2.38e-01	2.62e-02	4.89e-03	3.44e-03	0.00e+00	3.67e-02	3.49e-04	2.69e-03	7.85e-03	1.46e-02	3.39e-01		2.94e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 3.3713 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 3.3713 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  3.371272  0.000000
];

