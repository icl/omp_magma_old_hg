% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_94 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	823664	816974	141586	141586	5.49e-03	2.98e-02	6.18e-03	9.81e-03	1.84e-03	0.00e+00	2.83e-03	4.18e-04	4.42e-04	6.77e-03	2.39e-03	6.60e-02		6.60e-02
1	813865	811889	124554	131244	5.57e-03	2.96e-02	4.24e-03	3.02e-03	1.61e-03	0.00e+00	3.36e-03	2.61e-04	4.97e-04	4.43e-03	2.54e-03	5.52e-02		1.21e-01
2	810618	807576	135159	137135	5.46e-03	4.65e-02	4.69e-03	1.87e-03	4.64e-04	0.00e+00	3.95e-03	2.80e-04	4.60e-04	3.26e-03	2.70e-03	6.96e-02		1.91e-01
3	815472	804465	139212	142254	5.50e-03	5.03e-02	5.08e-03	2.00e-03	4.16e-04	0.00e+00	4.39e-03	4.16e-04	4.84e-04	3.27e-03	2.81e-03	7.47e-02		2.65e-01
4	812579	809146	135164	146171	5.45e-03	5.30e-02	5.40e-03	1.85e-03	7.37e-04	0.00e+00	5.00e-03	4.28e-04	5.69e-04	3.52e-03	2.85e-03	7.88e-02		3.44e-01
5	806973	810103	138862	142295	5.47e-03	4.74e-02	5.62e-03	2.45e-03	5.37e-04	0.00e+00	4.74e-03	5.32e-04	5.04e-04	3.39e-03	2.82e-03	7.34e-02		4.18e-01
6	805565	810200	145274	142144	5.47e-03	5.05e-02	5.70e-03	2.20e-03	5.13e-04	0.00e+00	4.90e-03	3.76e-04	5.30e-04	3.45e-03	2.82e-03	7.65e-02		4.94e-01
7	812338	812843	147488	142853	5.49e-03	4.80e-02	5.72e-03	2.71e-03	5.56e-04	0.00e+00	4.88e-03	4.61e-04	5.36e-04	3.15e-03	2.85e-03	7.43e-02		5.68e-01
8	812944	806933	141521	141016	5.50e-03	5.25e-02	5.83e-03	2.49e-03	5.82e-04	0.00e+00	4.82e-03	4.67e-04	5.11e-04	3.35e-03	2.83e-03	7.89e-02		6.47e-01
9	812820	812254	141721	147732	5.45e-03	4.82e-02	5.74e-03	1.96e-03	1.20e-03	0.00e+00	4.91e-03	3.49e-04	5.13e-04	3.18e-03	2.88e-03	7.44e-02		7.22e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.1938 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.1938 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.193805  0.000000
];

