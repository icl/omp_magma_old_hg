% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_22 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	807877	813240	141586	141586	3.69e-03	5.53e-02	1.08e-02	7.52e-03	1.09e-03	0.00e+00	1.14e-02	2.95e-04	1.18e-03	7.80e-03	6.71e-03	1.06e-01		1.06e-01
1	810012	811408	140341	134978	3.72e-03	6.91e-02	1.01e-02	3.83e-03	1.61e-03	0.00e+00	1.39e-02	2.83e-04	1.33e-03	6.28e-03	8.66e-03	1.19e-01		2.25e-01
2	800469	805767	139012	137616	3.76e-03	1.07e-01	1.32e-02	3.12e-03	1.04e-03	0.00e+00	1.72e-02	3.15e-04	1.42e-03	5.69e-03	9.23e-03	1.62e-01		3.87e-01
3	817766	799194	149361	144063	3.72e-03	1.51e-01	1.44e-02	3.43e-03	1.37e-03	0.00e+00	2.05e-02	3.62e-04	1.59e-03	5.70e-03	8.84e-03	2.11e-01		5.98e-01
4	815837	812789	132870	151442	3.70e-03	1.47e-01	1.53e-02	3.33e-03	1.77e-03	0.00e+00	2.13e-02	4.78e-04	1.64e-03	6.01e-03	8.82e-03	2.10e-01		8.08e-01
5	808355	807927	135604	138652	3.73e-03	1.50e-01	1.56e-02	3.22e-03	1.56e-03	0.00e+00	2.09e-02	3.89e-04	1.56e-03	6.04e-03	8.70e-03	2.12e-01		1.02e+00
6	812046	810292	143892	144320	3.75e-03	1.51e-01	1.53e-02	3.24e-03	1.80e-03	0.00e+00	2.12e-02	3.55e-04	1.65e-03	5.87e-03	8.83e-03	2.13e-01		1.23e+00
7	812174	808922	141007	142761	3.75e-03	1.54e-01	1.56e-02	3.38e-03	1.60e-03	0.00e+00	2.12e-02	3.03e-04	1.57e-03	5.68e-03	8.78e-03	2.16e-01		1.45e+00
8	811303	811691	141685	144937	3.76e-03	1.56e-01	1.56e-02	3.99e-03	1.73e-03	0.00e+00	2.13e-02	3.85e-04	1.63e-03	5.88e-03	8.80e-03	2.19e-01		1.67e+00
9	815406	815627	143362	142974	3.77e-03	1.56e-01	1.56e-02	3.95e-03	2.14e-03	0.00e+00	2.16e-02	5.14e-04	1.62e-03	5.72e-03	8.87e-03	2.20e-01		1.89e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 2.2911 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 2.2911 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  2.291053  0.000000
];

