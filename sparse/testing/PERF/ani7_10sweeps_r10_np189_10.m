% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_189 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	815600	815117	141586	141586	8.68e-03	3.63e-02	7.52e-03	1.60e-02	9.15e-04	0.00e+00	2.30e-03	4.43e-04	3.42e-04	1.02e-02	1.79e-03	8.44e-02		8.44e-02
1	803946	811360	132618	133101	8.76e-03	3.28e-02	3.33e-03	2.93e-03	8.56e-04	0.00e+00	2.62e-03	4.50e-04	3.73e-04	5.62e-03	2.02e-03	5.98e-02		1.44e-01
2	817054	804652	145078	137664	8.72e-03	4.50e-02	3.86e-03	2.19e-03	2.73e-04	0.00e+00	2.91e-03	5.75e-04	3.97e-04	4.08e-03	2.25e-03	7.02e-02		2.14e-01
3	804326	807481	132776	145178	8.59e-03	4.93e-02	4.31e-03	1.84e-03	5.55e-04	0.00e+00	3.13e-03	5.68e-04	4.29e-04	3.90e-03	2.24e-03	7.48e-02		2.89e-01
4	812432	804633	146310	143155	8.67e-03	4.93e-02	4.42e-03	3.02e-03	3.77e-04	0.00e+00	3.65e-03	5.31e-04	4.65e-04	4.83e-03	2.19e-03	7.75e-02		3.67e-01
5	811535	810372	139009	146808	8.60e-03	4.75e-02	4.56e-03	2.86e-03	9.98e-04	0.00e+00	3.58e-03	3.15e-04	4.64e-04	4.49e-03	2.30e-03	7.57e-02		4.42e-01
6	808182	809288	140712	141875	8.69e-03	5.06e-02	4.52e-03	3.01e-03	4.09e-04	0.00e+00	3.42e-03	6.35e-04	4.65e-04	4.27e-03	2.24e-03	7.83e-02		5.21e-01
7	810312	811135	144871	143765	8.68e-03	4.74e-02	4.59e-03	1.63e-03	5.46e-04	0.00e+00	3.37e-03	4.89e-04	4.27e-04	4.10e-03	2.30e-03	7.35e-02		5.94e-01
8	812931	812667	143547	142724	8.69e-03	5.14e-02	4.59e-03	1.98e-03	1.24e-03	0.00e+00	3.36e-03	6.79e-04	4.32e-04	4.11e-03	2.34e-03	7.88e-02		6.73e-01
9	814489	811113	141734	141998	8.75e-03	5.01e-02	5.15e-03	2.26e-03	1.29e-03	0.00e+00	3.38e-03	8.83e-04	4.48e-04	3.99e-03	2.33e-03	7.85e-02		7.52e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.3931 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.3931 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.393073  0.000000
];

