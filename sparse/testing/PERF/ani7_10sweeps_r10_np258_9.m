% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_258 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	808064	818416	141586	141586	1.14e-02	4.39e-02	9.35e-03	2.08e-02	1.39e-03	0.00e+00	2.16e-03	6.03e-04	3.27e-04	1.54e-02	1.58e-03	1.07e-01		1.07e-01
1	804850	818155	140154	129802	1.15e-02	3.59e-02	3.15e-03	2.39e-03	5.32e-04	0.00e+00	2.87e-03	4.14e-04	3.44e-04	6.76e-03	1.90e-03	6.57e-02		1.73e-01
2	809673	810593	144174	130869	1.14e-02	5.17e-02	3.70e-03	4.58e-03	4.82e-04	0.00e+00	3.03e-03	5.19e-04	3.80e-04	4.72e-03	2.06e-03	8.26e-02		2.55e-01
3	826778	790930	140157	139237	1.13e-02	5.11e-02	4.24e-03	1.76e-03	3.69e-04	0.00e+00	3.23e-03	6.07e-04	4.04e-04	4.23e-03	2.04e-03	7.93e-02		3.34e-01
4	802268	805886	123858	159706	1.11e-02	5.58e-02	4.22e-03	2.59e-03	4.86e-04	0.00e+00	3.51e-03	4.13e-04	4.16e-04	4.18e-03	2.06e-03	8.47e-02		4.19e-01
5	811928	807870	149173	145555	1.13e-02	5.05e-02	4.15e-03	3.19e-03	4.55e-04	0.00e+00	3.39e-03	6.51e-04	4.09e-04	4.71e-03	2.05e-03	8.08e-02		5.00e-01
6	815865	813910	140319	144377	1.13e-02	5.66e-02	4.26e-03	2.23e-03	3.86e-04	0.00e+00	3.51e-03	6.06e-04	4.42e-04	4.20e-03	2.13e-03	8.57e-02		5.86e-01
7	813191	806328	137188	139143	1.13e-02	5.42e-02	4.39e-03	1.39e-03	4.51e-04	0.00e+00	3.52e-03	9.09e-04	4.53e-04	4.74e-03	2.16e-03	8.35e-02		6.69e-01
8	812433	815890	140668	147531	1.12e-02	5.68e-02	4.32e-03	2.68e-03	1.59e-03	0.00e+00	3.50e-03	8.11e-04	4.07e-04	4.21e-03	2.10e-03	8.76e-02		7.57e-01
9	811667	812548	142232	138775	1.14e-02	5.32e-02	4.39e-03	1.92e-03	8.61e-04	0.00e+00	3.56e-03	1.04e-03	4.07e-04	4.22e-03	2.13e-03	8.31e-02		8.40e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.5892 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.5892 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.589151  0.000000
];

