% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_96 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	817346	805275	141586	141586	5.52e-03	2.97e-02	6.12e-03	9.84e-03	5.44e-04	0.00e+00	2.78e-03	3.65e-04	4.80e-04	6.58e-03	2.33e-03	6.43e-02		6.43e-02
1	794230	815155	130872	142943	5.48e-03	2.85e-02	4.09e-03	3.68e-03	5.78e-04	0.00e+00	3.19e-03	2.82e-04	5.35e-04	4.74e-03	2.46e-03	5.35e-02		1.18e-01
2	810075	798914	154794	133869	5.55e-03	4.58e-02	4.47e-03	3.51e-03	3.47e-04	0.00e+00	3.93e-03	3.42e-04	5.08e-04	3.31e-03	2.69e-03	7.05e-02		1.88e-01
3	804406	811178	139755	150916	5.43e-03	4.92e-02	4.80e-03	1.61e-03	3.86e-04	0.00e+00	4.64e-03	3.04e-04	5.25e-04	3.13e-03	2.78e-03	7.28e-02		2.61e-01
4	810450	795700	146230	139458	5.51e-03	4.90e-02	5.31e-03	2.21e-03	5.16e-04	0.00e+00	4.80e-03	4.71e-04	5.13e-04	3.59e-03	2.63e-03	7.46e-02		3.36e-01
5	808606	812115	140991	155741	5.46e-03	4.45e-02	5.32e-03	1.49e-03	1.27e-03	0.00e+00	4.85e-03	3.69e-04	5.10e-04	3.41e-03	2.80e-03	7.00e-02		4.06e-01
6	811002	810182	143641	140132	5.53e-03	5.01e-02	5.62e-03	2.87e-03	5.06e-04	0.00e+00	4.85e-03	4.29e-04	5.19e-04	3.35e-03	2.84e-03	7.66e-02		4.82e-01
7	810726	810145	142051	142871	5.52e-03	4.95e-02	5.61e-03	1.56e-03	4.69e-04	0.00e+00	4.76e-03	3.27e-04	5.47e-04	3.16e-03	2.80e-03	7.43e-02		5.57e-01
8	812951	812777	143133	143714	5.51e-03	5.05e-02	5.60e-03	2.22e-03	1.21e-03	0.00e+00	4.79e-03	5.31e-04	5.48e-04	3.30e-03	2.84e-03	7.71e-02		6.34e-01
9	814465	810582	141714	141888	5.54e-03	4.91e-02	5.68e-03	1.92e-03	9.33e-04	0.00e+00	4.88e-03	4.40e-04	5.42e-04	3.20e-03	2.79e-03	7.51e-02		7.09e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.1997 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.1997 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.199653  0.000000
];

