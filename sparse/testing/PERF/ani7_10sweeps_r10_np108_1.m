% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_108 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	818600	809982	141586	141586	5.69e-03	2.86e-02	5.89e-03	9.94e-03	1.35e-03	0.00e+00	2.52e-03	3.29e-04	4.35e-04	6.31e-03	2.08e-03	6.32e-02		6.32e-02
1	814586	801354	129618	138236	5.72e-03	2.87e-02	3.67e-03	4.25e-03	1.44e-03	0.00e+00	2.93e-03	2.84e-04	4.80e-04	4.16e-03	2.44e-03	5.41e-02		1.17e-01
2	809074	814288	134438	147670	5.62e-03	4.31e-02	4.25e-03	1.36e-03	6.81e-04	0.00e+00	3.46e-03	4.09e-04	4.59e-04	3.17e-03	2.65e-03	6.52e-02		1.82e-01
3	813903	819744	140756	135542	6.32e-03	4.88e-02	4.68e-03	2.44e-03	3.27e-04	0.00e+00	3.90e-03	3.25e-04	4.73e-04	3.15e-03	2.65e-03	7.30e-02		2.55e-01
4	811806	802959	136733	130892	5.72e-03	5.05e-02	4.89e-03	1.44e-03	4.60e-04	0.00e+00	4.26e-03	4.19e-04	4.89e-04	3.19e-03	2.47e-03	7.38e-02		3.29e-01
5	809493	813477	139635	148482	5.62e-03	4.30e-02	4.82e-03	1.28e-03	5.04e-04	0.00e+00	4.29e-03	4.43e-04	4.49e-04	3.49e-03	2.51e-03	6.64e-02		3.96e-01
6	808051	807580	142754	138770	5.71e-03	4.65e-02	4.99e-03	1.36e-03	3.91e-04	0.00e+00	4.23e-03	5.06e-04	4.41e-04	3.19e-03	2.54e-03	6.98e-02		4.65e-01
7	810423	809453	145002	145473	5.67e-03	4.38e-02	4.95e-03	1.30e-03	3.94e-04	0.00e+00	4.25e-03	3.29e-04	4.47e-04	3.21e-03	2.51e-03	6.69e-02		5.32e-01
8	816760	810884	143436	144406	5.68e-03	4.63e-02	5.06e-03	2.06e-03	1.08e-03	0.00e+00	4.15e-03	5.44e-04	4.33e-04	3.27e-03	2.52e-03	7.11e-02		6.03e-01
9	812475	815382	137905	143781	5.71e-03	4.49e-02	5.13e-03	1.94e-03	9.73e-04	0.00e+00	4.30e-03	5.20e-04	4.62e-04	3.11e-03	2.55e-03	6.96e-02		6.73e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.1461 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.1461 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.146087  0.000000
];

