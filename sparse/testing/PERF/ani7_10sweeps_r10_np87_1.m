% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_87 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	811082	807728	141586	141586	5.41e-03	2.97e-02	6.41e-03	9.54e-03	4.46e-04	0.00e+00	3.09e-03	4.13e-04	4.77e-04	6.49e-03	2.41e-03	6.44e-02		6.44e-02
1	809756	813573	137136	140490	5.37e-03	2.76e-02	4.50e-03	3.21e-03	4.56e-04	0.00e+00	3.60e-03	3.21e-04	5.01e-04	4.58e-03	2.75e-03	5.29e-02		1.17e-01
2	803350	802710	139268	135451	5.41e-03	4.75e-02	5.01e-03	1.66e-03	3.57e-04	0.00e+00	4.33e-03	4.84e-04	4.64e-04	3.38e-03	2.89e-03	7.15e-02		1.89e-01
3	811148	804291	146480	147120	5.37e-03	5.14e-02	5.32e-03	3.22e-03	5.26e-04	0.00e+00	5.08e-03	4.14e-04	5.36e-04	3.24e-03	2.98e-03	7.81e-02		2.67e-01
4	811021	814090	139488	146345	5.34e-03	5.34e-02	5.75e-03	1.43e-03	5.40e-04	0.00e+00	5.27e-03	3.81e-04	5.85e-04	3.73e-03	3.05e-03	7.94e-02		3.46e-01
5	813652	805430	140420	137351	5.42e-03	4.79e-02	6.07e-03	3.01e-03	1.06e-03	0.00e+00	5.40e-03	4.31e-04	5.49e-04	3.56e-03	3.07e-03	7.65e-02		4.23e-01
6	810997	810650	138595	146817	5.33e-03	5.13e-02	6.07e-03	1.48e-03	5.73e-04	0.00e+00	5.25e-03	3.38e-04	5.63e-04	4.66e-03	3.11e-03	7.87e-02		5.02e-01
7	804824	807895	142056	142403	5.37e-03	4.99e-02	6.09e-03	2.01e-03	5.63e-04	0.00e+00	5.15e-03	4.84e-04	5.37e-04	3.26e-03	3.02e-03	7.64e-02		5.78e-01
8	815741	813574	149035	145964	5.40e-03	5.07e-02	6.04e-03	1.40e-03	9.62e-04	0.00e+00	5.20e-03	2.85e-04	5.52e-04	3.41e-03	3.16e-03	7.72e-02		6.55e-01
9	814984	813422	138924	141091	5.41e-03	5.15e-02	6.33e-03	2.41e-03	9.69e-04	0.00e+00	5.32e-03	3.85e-04	5.63e-04	3.30e-03	3.14e-03	7.93e-02		7.34e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.2198 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.2198 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.219841  0.000000
];

