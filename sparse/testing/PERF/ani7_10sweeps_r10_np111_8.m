% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_111 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	824757	805859	141586	141586	5.76e-03	2.86e-02	5.83e-03	1.01e-02	1.54e-03	0.00e+00	2.43e-03	3.75e-04	4.27e-04	6.59e-03	2.00e-03	6.36e-02		6.36e-02
1	799129	821599	123461	142359	5.72e-03	2.76e-02	3.55e-03	2.71e-03	1.18e-03	0.00e+00	2.96e-03	3.36e-04	4.18e-04	4.30e-03	2.44e-03	5.12e-02		1.15e-01
2	818581	804135	149895	127425	5.84e-03	4.23e-02	4.24e-03	1.35e-03	7.51e-04	0.00e+00	3.38e-03	3.63e-04	4.78e-04	3.24e-03	2.56e-03	6.45e-02		1.79e-01
3	797496	810861	131249	145695	5.74e-03	4.67e-02	4.65e-03	1.67e-03	4.05e-04	0.00e+00	4.04e-03	4.05e-04	5.28e-04	3.09e-03	2.48e-03	6.97e-02		2.49e-01
4	804915	808625	153140	139775	5.75e-03	4.35e-02	4.57e-03	2.02e-03	6.26e-04	0.00e+00	4.27e-03	4.62e-04	4.81e-04	3.15e-03	2.49e-03	6.74e-02		3.16e-01
5	811072	810951	146526	142816	5.73e-03	4.22e-02	4.77e-03	2.99e-03	4.91e-04	0.00e+00	4.21e-03	3.02e-04	5.02e-04	3.42e-03	2.48e-03	6.71e-02		3.83e-01
6	808214	810629	141175	141296	5.78e-03	4.55e-02	4.89e-03	1.99e-03	5.20e-04	0.00e+00	4.26e-03	5.11e-04	5.04e-04	3.19e-03	2.46e-03	6.96e-02		4.53e-01
7	810941	812622	144839	142424	5.75e-03	4.28e-02	4.89e-03	2.38e-03	4.25e-04	0.00e+00	4.12e-03	4.89e-04	4.64e-04	3.07e-03	2.48e-03	6.69e-02		5.20e-01
8	808580	808751	142918	141237	5.75e-03	4.57e-02	4.95e-03	1.98e-03	3.86e-04	0.00e+00	4.13e-03	5.22e-04	4.98e-04	3.20e-03	2.46e-03	6.96e-02		5.90e-01
9	810064	817377	146085	145914	5.75e-03	4.25e-02	4.88e-03	1.63e-03	8.76e-04	0.00e+00	4.21e-03	5.19e-04	4.68e-04	3.10e-03	2.55e-03	6.65e-02		6.56e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.1310 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.1310 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.131003  0.000000
];

