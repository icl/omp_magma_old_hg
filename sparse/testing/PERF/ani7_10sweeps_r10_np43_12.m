% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_43 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	816692	802286	141586	141586	3.26e-03	3.24e-02	5.81e-03	6.66e-03	5.99e-04	0.00e+00	5.56e-03	2.74e-04	5.97e-04	5.57e-03	3.24e-03	6.40e-02		6.40e-02
1	799800	821357	131526	145932	3.24e-03	3.77e-02	5.01e-03	2.16e-03	7.34e-04	0.00e+00	6.87e-03	2.32e-04	6.68e-04	4.23e-03	4.27e-03	6.51e-02		1.29e-01
2	813610	815191	149224	127667	3.40e-03	5.92e-02	6.36e-03	2.96e-03	5.79e-04	0.00e+00	8.25e-03	1.92e-04	7.41e-04	3.78e-03	4.70e-03	9.02e-02		2.19e-01
3	805766	806944	136220	134639	3.37e-03	8.67e-02	7.49e-03	1.95e-03	8.22e-04	0.00e+00	9.77e-03	2.63e-04	8.41e-04	3.75e-03	4.27e-03	1.19e-01		3.38e-01
4	805704	815713	144870	143692	3.29e-03	8.16e-02	7.31e-03	2.55e-03	1.06e-03	0.00e+00	1.05e-02	3.51e-04	8.99e-04	4.12e-03	4.30e-03	1.16e-01		4.54e-01
5	807700	807356	145737	135728	3.34e-03	8.04e-02	7.46e-03	1.91e-03	1.35e-03	0.00e+00	1.05e-02	2.79e-04	8.52e-04	4.10e-03	4.30e-03	1.14e-01		5.69e-01
6	810894	809355	144547	144891	3.32e-03	8.22e-02	7.45e-03	1.62e-03	9.22e-04	0.00e+00	1.03e-02	3.23e-04	8.44e-04	3.86e-03	4.30e-03	1.15e-01		6.84e-01
7	811581	807959	142159	143698	3.30e-03	8.30e-02	7.54e-03	1.57e-03	8.94e-04	0.00e+00	1.02e-02	5.49e-04	8.53e-04	3.94e-03	5.27e-03	1.17e-01		8.01e-01
8	813184	813572	142278	145900	4.98e-03	8.54e-02	1.08e-02	2.35e-03	1.26e-03	0.00e+00	1.01e-02	4.00e-04	8.60e-04	3.84e-03	5.30e-03	1.25e-01		9.26e-01
9	814225	811829	141481	141093	4.98e-03	8.49e-02	1.09e-02	2.05e-03	1.27e-03	0.00e+00	1.05e-02	3.05e-04	8.94e-04	3.74e-03	5.31e-03	1.25e-01		1.05e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.4252 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.4252 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.425209  0.000000
];

