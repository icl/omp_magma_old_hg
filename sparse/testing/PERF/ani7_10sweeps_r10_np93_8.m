% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_93 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	810739	809922	141586	141586	5.47e-03	3.01e-02	6.19e-03	9.76e-03	5.75e-04	0.00e+00	3.23e-03	3.07e-04	4.51e-04	6.58e-03	2.36e-03	6.50e-02		6.50e-02
1	805203	810643	137479	138296	5.48e-03	2.91e-02	4.18e-03	3.06e-03	7.00e-04	0.00e+00	3.27e-03	3.25e-04	4.99e-04	4.10e-03	2.55e-03	5.32e-02		1.18e-01
2	816291	799533	143821	138381	5.49e-03	4.63e-02	4.62e-03	2.43e-03	3.45e-04	0.00e+00	4.11e-03	2.91e-04	5.10e-04	3.32e-03	2.73e-03	7.01e-02		1.88e-01
3	814102	818293	133539	150297	5.41e-03	5.12e-02	5.09e-03	1.79e-03	3.72e-04	0.00e+00	4.61e-03	5.38e-04	5.28e-04	3.22e-03	2.85e-03	7.56e-02		2.64e-01
4	806583	805008	136534	132343	5.53e-03	5.49e-02	5.51e-03	1.65e-03	4.76e-04	0.00e+00	4.83e-03	5.14e-04	5.39e-04	3.83e-03	2.84e-03	8.07e-02		3.45e-01
5	809684	810391	144858	146433	5.46e-03	4.54e-02	5.55e-03	2.61e-03	9.73e-04	0.00e+00	5.10e-03	4.71e-04	5.46e-04	3.51e-03	2.83e-03	7.25e-02		4.17e-01
6	810074	812248	142563	141856	5.50e-03	4.95e-02	5.72e-03	2.04e-03	4.53e-04	0.00e+00	4.74e-03	4.18e-04	5.14e-04	3.52e-03	2.84e-03	7.53e-02		4.92e-01
7	813795	815048	142979	140805	5.48e-03	4.83e-02	5.83e-03	1.89e-03	6.68e-04	0.00e+00	5.05e-03	4.26e-04	5.57e-04	3.30e-03	2.86e-03	7.44e-02		5.67e-01
8	812626	811877	140064	138811	5.54e-03	5.23e-02	5.90e-03	2.88e-03	1.06e-03	0.00e+00	5.07e-03	3.37e-04	5.57e-04	3.30e-03	2.87e-03	7.98e-02		6.46e-01
9	814783	814688	142039	142788	5.50e-03	4.92e-02	5.83e-03	2.24e-03	8.90e-04	0.00e+00	4.99e-03	6.09e-04	5.40e-04	3.19e-03	2.92e-03	7.59e-02		7.22e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.2025 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.2025 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.202464  0.000000
];

