% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_3 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	822139	805282	141586	141586	5.76e-03	3.22e-01	7.27e-02	1.74e-02	4.64e-03	0.00e+00	6.86e-02	2.45e-04	7.64e-03	2.66e-02	4.70e-02	5.72e-01		5.72e-01
1	803098	807710	126079	142936	6.17e-03	4.00e-01	7.32e-02	1.49e-02	6.58e-03	0.00e+00	8.09e-02	2.51e-04	8.13e-03	2.61e-02	5.98e-02	6.76e-01		1.25e+00
2	798844	827894	145926	141314	6.49e-03	6.70e-01	9.06e-02	1.47e-02	6.81e-03	0.00e+00	9.81e-02	2.52e-04	8.88e-03	2.62e-02	6.64e-02	9.88e-01		2.24e+00
3	803331	813640	150986	121936	6.63e-03	9.11e-01	1.04e-01	1.50e-02	7.64e-03	0.00e+00	1.10e-01	3.14e-04	1.00e-02	2.58e-02	6.23e-02	1.25e+00		3.49e+00
4	799372	810119	147305	136996	6.49e-03	8.50e-01	1.06e-01	1.67e-02	1.10e-02	0.00e+00	1.18e-01	3.87e-04	1.05e-02	2.55e-02	5.93e-02	1.20e+00		4.69e+00
5	806849	809504	152069	141322	6.40e-03	8.22e-01	1.05e-01	1.71e-02	1.08e-02	0.00e+00	1.18e-01	3.05e-04	1.03e-02	2.69e-02	6.03e-02	1.18e+00		5.87e+00
6	812022	809488	145398	142743	6.44e-03	8.64e-01	1.07e-01	1.72e-02	1.03e-02	0.00e+00	1.17e-01	3.46e-04	1.02e-02	2.59e-02	6.06e-02	1.22e+00		7.09e+00
7	811887	812047	141031	143565	6.42e-03	8.79e-01	1.08e-01	1.56e-02	9.97e-03	0.00e+00	1.15e-01	3.79e-04	1.01e-02	2.62e-02	6.09e-02	1.23e+00		8.32e+00
8	810569	814942	141972	141812	6.48e-03	8.93e-01	1.09e-01	1.62e-02	1.11e-02	0.00e+00	1.20e-01	2.58e-04	1.04e-02	2.61e-02	6.11e-02	1.25e+00		9.57e+00
9	813215	815334	144096	139723	6.48e-03	8.91e-01	1.10e-01	1.58e-02	1.01e-02	0.00e+00	1.13e-01	4.04e-04	1.00e-02	2.58e-02	6.12e-02	1.24e+00		1.08e+01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 11.5213 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 11.5213 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  11.521308  0.000000
];

