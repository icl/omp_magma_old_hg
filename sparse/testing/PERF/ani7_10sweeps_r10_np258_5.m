% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_258 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	812613	814046	141586	141586	6.28e-02	6.36e-02	9.31e-03	2.08e-02	1.28e-03	0.00e+00	2.16e-03	6.18e-04	3.30e-04	1.44e-02	1.62e-03	1.77e-01		1.77e-01
1	804917	809032	135605	134172	1.14e-02	3.63e-02	3.11e-03	5.50e-03	2.35e-03	0.00e+00	2.69e-03	4.80e-04	3.54e-04	8.19e-03	1.80e-03	7.22e-02		2.49e-01
2	803728	801776	144107	139992	1.12e-02	5.13e-02	3.57e-03	2.31e-03	2.68e-04	0.00e+00	3.01e-03	4.96e-04	3.77e-04	4.25e-03	2.00e-03	7.89e-02		3.28e-01
3	806628	804051	146102	148054	1.12e-02	5.00e-02	4.13e-03	2.49e-03	5.54e-04	0.00e+00	3.43e-03	5.29e-04	4.09e-04	4.28e-03	2.02e-03	7.90e-02		4.07e-01
4	818232	814172	144008	146585	1.12e-02	5.58e-02	4.16e-03	1.95e-03	4.68e-04	0.00e+00	3.59e-03	7.25e-04	4.21e-04	5.19e-03	2.09e-03	8.56e-02		4.93e-01
5	810617	809026	133209	137269	1.14e-02	5.12e-02	4.32e-03	2.24e-03	4.06e-04	0.00e+00	3.38e-03	7.65e-04	4.50e-04	4.98e-03	2.09e-03	8.12e-02		5.74e-01
6	807659	813936	141630	143221	1.13e-02	5.68e-02	4.38e-03	2.52e-03	3.96e-04	0.00e+00	3.54e-03	8.23e-04	2.07e-03	5.10e-03	2.10e-03	8.90e-02		6.63e-01
7	809296	813801	145394	139117	1.14e-02	5.26e-02	4.34e-03	1.73e-03	3.66e-04	0.00e+00	3.38e-03	8.56e-04	4.00e-04	4.25e-03	2.11e-03	8.14e-02		7.44e-01
8	811283	814986	144563	140058	1.14e-02	5.85e-02	4.38e-03	2.17e-03	6.92e-04	0.00e+00	3.45e-03	7.99e-04	4.03e-04	4.54e-03	2.11e-03	8.84e-02		8.33e-01
9	812997	813116	143382	139679	1.14e-02	5.34e-02	4.42e-03	2.76e-03	1.75e-03	0.00e+00	3.48e-03	9.06e-04	4.57e-04	4.21e-03	2.11e-03	8.49e-02		9.18e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.9741 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.9741 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.974114  0.000000
];

