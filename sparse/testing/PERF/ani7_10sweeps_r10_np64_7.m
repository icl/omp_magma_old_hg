% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_64 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	820703	802421	141586	141586	3.23e-03	2.66e-02	5.01e-03	6.96e-03	1.26e-03	0.00e+00	3.77e-03	2.96e-04	4.34e-04	5.91e-03	2.26e-03	5.57e-02		5.57e-02
1	806483	819113	127515	145797	3.24e-03	3.04e-02	3.35e-03	2.39e-03	1.03e-03	0.00e+00	4.62e-03	2.39e-04	4.78e-04	4.15e-03	3.08e-03	5.29e-02		1.09e-01
2	794407	807500	142541	129911	3.33e-03	5.02e-02	4.37e-03	1.94e-03	4.64e-04	0.00e+00	5.65e-03	2.94e-04	5.34e-04	3.19e-03	3.15e-03	7.31e-02		1.82e-01
3	804388	802415	155423	142330	3.28e-03	5.98e-02	4.72e-03	1.66e-03	6.14e-04	0.00e+00	6.52e-03	3.37e-04	6.18e-04	3.24e-03	3.00e-03	8.38e-02		2.66e-01
4	812616	805397	146248	148221	3.23e-03	6.04e-02	5.00e-03	1.62e-03	7.50e-04	0.00e+00	7.07e-03	2.98e-04	6.83e-04	3.23e-03	2.94e-03	8.52e-02		3.51e-01
5	810664	806247	138825	146044	3.29e-03	5.86e-02	5.01e-03	2.43e-03	7.80e-04	0.00e+00	7.14e-03	2.65e-04	6.67e-04	3.58e-03	2.92e-03	8.47e-02		4.36e-01
6	810863	814268	141583	146000	3.25e-03	6.07e-02	5.06e-03	1.90e-03	6.92e-04	0.00e+00	7.22e-03	2.66e-04	6.45e-04	3.35e-03	3.01e-03	8.61e-02		5.22e-01
7	812828	811564	142190	138785	3.29e-03	6.03e-02	5.14e-03	3.01e-03	6.57e-04	0.00e+00	7.04e-03	3.58e-04	6.32e-04	3.22e-03	3.00e-03	8.66e-02		6.08e-01
8	812392	810049	141031	142295	3.26e-03	6.22e-02	5.11e-03	1.25e-03	7.48e-04	0.00e+00	7.25e-03	3.71e-04	6.44e-04	3.50e-03	2.99e-03	8.73e-02		6.96e-01
9	813235	813221	142273	144616	3.28e-03	6.45e-02	7.67e-03	2.08e-03	1.07e-03	0.00e+00	8.73e-03	4.22e-04	8.10e-04	3.35e-03	4.17e-03	9.61e-02		7.92e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.1601 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.1601 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.160084  0.000000
];

