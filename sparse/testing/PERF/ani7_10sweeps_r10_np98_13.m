% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_98 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	812712	806392	141586	141586	5.53e-03	2.96e-02	6.04e-03	9.80e-03	5.05e-04	0.00e+00	2.73e-03	2.92e-04	4.73e-04	6.50e-03	2.26e-03	6.37e-02		6.37e-02
1	810727	802676	135506	141826	5.53e-03	2.81e-02	3.97e-03	5.07e-03	6.18e-04	0.00e+00	3.17e-03	3.05e-04	5.04e-04	4.20e-03	2.40e-03	5.38e-02		1.18e-01
2	805057	795820	138297	146348	5.48e-03	4.54e-02	4.43e-03	1.95e-03	3.34e-04	0.00e+00	3.79e-03	3.54e-04	5.00e-04	3.30e-03	2.55e-03	6.81e-02		1.86e-01
3	809839	812097	144773	154010	5.42e-03	4.78e-02	4.67e-03	2.70e-03	4.94e-04	0.00e+00	4.51e-03	4.48e-04	4.88e-04	3.16e-03	2.72e-03	7.24e-02		2.58e-01
4	803871	808242	140797	138539	5.56e-03	4.98e-02	5.23e-03	2.08e-03	4.35e-04	0.00e+00	4.69e-03	3.95e-04	5.04e-04	3.59e-03	2.76e-03	7.50e-02		3.33e-01
5	817720	807197	147570	143199	5.54e-03	4.49e-02	5.33e-03	1.77e-03	8.57e-04	0.00e+00	4.69e-03	4.34e-04	5.03e-04	3.64e-03	2.71e-03	7.04e-02		4.03e-01
6	805239	815028	134527	145050	5.51e-03	4.98e-02	5.53e-03	1.74e-03	5.44e-04	0.00e+00	4.70e-03	3.91e-04	5.51e-04	3.39e-03	2.73e-03	7.49e-02		4.78e-01
7	814548	807813	147814	138025	5.56e-03	4.70e-02	5.55e-03	1.83e-03	4.94e-04	0.00e+00	4.75e-03	4.33e-04	5.49e-04	3.25e-03	2.70e-03	7.21e-02		5.50e-01
8	815639	811712	139311	146046	5.54e-03	5.04e-02	5.57e-03	1.92e-03	1.04e-03	0.00e+00	4.75e-03	3.77e-04	5.03e-04	3.29e-03	2.76e-03	7.61e-02		6.27e-01
9	810493	812123	139026	142953	5.56e-03	4.89e-02	5.61e-03	1.75e-03	9.33e-04	0.00e+00	4.71e-03	4.22e-04	5.25e-04	3.21e-03	2.74e-03	7.43e-02		7.01e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.1884 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.1884 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.188448  0.000000
];

