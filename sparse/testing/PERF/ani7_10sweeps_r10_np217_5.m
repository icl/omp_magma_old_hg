% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_217 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	816763	813422	141586	141586	1.00e-02	4.11e-02	8.92e-03	1.93e-02	1.50e-03	0.00e+00	2.50e-03	5.77e-04	3.70e-04	1.26e-02	1.84e-03	9.87e-02		9.87e-02
1	812636	798115	131455	134796	1.01e-02	3.59e-02	3.58e-03	5.20e-03	2.00e-03	0.00e+00	3.10e-03	4.96e-04	3.51e-04	6.42e-03	2.04e-03	6.91e-02		1.68e-01
2	796189	809949	136388	150909	9.81e-03	5.23e-02	3.80e-03	2.31e-03	6.85e-04	0.00e+00	3.47e-03	6.83e-04	4.01e-04	4.27e-03	2.04e-03	7.97e-02		2.47e-01
3	814849	815009	153641	139881	9.96e-03	5.20e-02	4.03e-03	3.34e-03	3.45e-04	0.00e+00	3.79e-03	4.58e-04	4.19e-04	4.16e-03	2.10e-03	8.06e-02		3.28e-01
4	816136	805424	135787	135627	1.01e-02	5.70e-02	4.41e-03	2.75e-03	4.05e-04	0.00e+00	4.03e-03	8.59e-04	4.82e-04	4.26e-03	2.11e-03	8.65e-02		4.15e-01
5	813592	813398	135305	146017	9.91e-03	4.97e-02	4.58e-03	2.83e-03	4.43e-04	0.00e+00	4.13e-03	6.45e-04	4.65e-04	4.69e-03	2.14e-03	7.95e-02		4.94e-01
6	810913	808850	138655	138849	1.00e-02	5.52e-02	4.60e-03	2.67e-03	4.37e-04	0.00e+00	4.05e-03	7.40e-04	4.55e-04	4.40e-03	2.09e-03	8.46e-02		5.79e-01
7	806117	812084	142140	144203	1.00e-02	5.25e-02	4.57e-03	3.04e-03	4.35e-04	0.00e+00	4.20e-03	8.65e-04	4.68e-04	4.69e-03	2.10e-03	8.29e-02		6.62e-01
8	808441	809977	147742	141775	9.99e-03	5.46e-02	4.50e-03	3.09e-03	1.30e-03	0.00e+00	3.95e-03	8.68e-04	4.30e-04	4.24e-03	2.09e-03	8.50e-02		7.47e-01
9	819265	812605	146224	144688	9.97e-03	5.11e-02	4.57e-03	2.52e-03	1.05e-03	0.00e+00	4.03e-03	6.68e-04	4.30e-04	4.25e-03	2.12e-03	8.07e-02		8.27e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.5795 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.5795 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.579487  0.000000
];

