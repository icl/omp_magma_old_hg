% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_127 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	818266	802741	141586	141586	5.91e-03	2.80e-02	5.78e-03	1.02e-02	1.79e-03	0.00e+00	2.87e-03	3.99e-04	3.51e-04	7.10e-03	1.77e-03	6.42e-02		6.42e-02
1	803892	809935	129952	145477	5.90e-03	2.75e-02	3.09e-03	3.07e-03	1.51e-03	0.00e+00	3.55e-03	2.54e-04	4.02e-04	4.73e-03	2.09e-03	5.21e-02		1.16e-01
2	805025	810808	145132	139089	5.92e-03	3.77e-02	3.69e-03	1.53e-03	3.46e-04	0.00e+00	4.06e-03	4.29e-04	4.44e-04	3.02e-03	2.36e-03	5.95e-02		1.76e-01
3	813728	800820	144805	139022	5.96e-03	4.48e-02	4.26e-03	1.37e-03	3.43e-04	0.00e+00	4.55e-03	3.06e-04	4.64e-04	3.03e-03	2.35e-03	6.74e-02		2.43e-01
4	804780	809025	136908	149816	5.89e-03	4.66e-02	4.42e-03	1.87e-03	5.31e-04	0.00e+00	4.82e-03	4.53e-04	5.12e-04	3.08e-03	2.24e-03	7.04e-02		3.14e-01
5	811519	819197	146661	142416	5.92e-03	4.37e-02	4.41e-03	1.87e-03	4.81e-04	0.00e+00	4.65e-03	5.27e-04	4.64e-04	3.55e-03	2.32e-03	6.79e-02		3.82e-01
6	809745	809358	140728	133050	6.01e-03	4.91e-02	4.47e-03	1.41e-03	4.84e-04	0.00e+00	4.72e-03	4.92e-04	4.77e-04	3.42e-03	2.27e-03	7.29e-02		4.54e-01
7	810989	812746	143308	143695	5.95e-03	4.72e-02	4.51e-03	1.77e-03	4.75e-04	0.00e+00	4.62e-03	3.99e-04	4.87e-04	3.00e-03	2.30e-03	7.07e-02		5.25e-01
8	809550	813631	142870	141113	6.01e-03	4.94e-02	4.50e-03	2.08e-03	3.89e-04	0.00e+00	4.83e-03	3.78e-04	4.56e-04	3.24e-03	2.32e-03	7.36e-02		5.99e-01
9	814649	811510	145115	141034	6.00e-03	4.75e-02	4.47e-03	1.82e-03	9.22e-04	0.00e+00	4.57e-03	4.72e-04	4.74e-04	2.94e-03	2.29e-03	7.14e-02		6.70e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.1476 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.1476 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.147591  0.000000
];

