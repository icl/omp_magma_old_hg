% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_131 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	815080	811952	141586	141586	5.97e-03	2.79e-02	5.76e-03	1.05e-02	6.96e-04	0.00e+00	2.75e-03	3.05e-04	3.39e-04	7.43e-03	1.70e-03	6.34e-02		6.34e-02
1	795247	792380	133138	136266	6.01e-03	2.71e-02	3.05e-03	3.57e-03	1.06e-03	0.00e+00	3.43e-03	2.87e-04	3.68e-04	4.88e-03	2.02e-03	5.18e-02		1.15e-01
2	802422	817799	153777	156644	5.88e-03	3.73e-02	3.61e-03	1.89e-03	2.94e-04	0.00e+00	3.94e-03	4.10e-04	4.11e-04	3.03e-03	2.33e-03	5.91e-02		1.74e-01
3	806076	804672	147408	132031	6.06e-03	4.83e-02	4.21e-03	2.13e-03	3.99e-04	0.00e+00	4.55e-03	4.78e-04	4.64e-04	3.02e-03	2.31e-03	7.19e-02		2.46e-01
4	811307	814842	144560	145964	5.97e-03	5.12e-02	5.97e-03	1.39e-03	4.73e-04	0.00e+00	4.74e-03	4.06e-04	4.84e-04	3.82e-03	3.05e-03	7.75e-02		3.24e-01
5	811893	810426	140134	136599	8.77e-03	5.19e-02	6.22e-03	1.38e-03	4.86e-04	0.00e+00	4.57e-03	4.90e-04	5.17e-04	3.42e-03	3.06e-03	8.08e-02		4.05e-01
6	812780	811674	140354	141821	8.82e-03	5.47e-02	6.26e-03	2.20e-03	5.99e-04	0.00e+00	4.93e-03	4.50e-04	5.19e-04	3.50e-03	3.14e-03	8.51e-02		4.90e-01
7	809983	814030	140273	141379	8.78e-03	5.33e-02	6.29e-03	1.79e-03	5.22e-04	0.00e+00	4.78e-03	5.30e-04	5.22e-04	3.13e-03	3.08e-03	8.27e-02		5.72e-01
8	813335	812781	143876	139829	8.85e-03	5.64e-02	4.41e-03	2.23e-03	5.60e-04	0.00e+00	4.71e-03	6.68e-04	4.93e-04	3.19e-03	2.25e-03	8.37e-02		6.56e-01
9	810924	813332	141330	141884	6.03e-03	4.75e-02	4.43e-03	1.60e-03	9.90e-04	0.00e+00	4.75e-03	4.61e-04	4.57e-04	2.95e-03	2.27e-03	7.15e-02		7.28e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.2131 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.2131 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.213073  0.000000
];

