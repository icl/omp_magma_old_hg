% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_159 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	814590	814263	141586	141586	7.97e-03	3.51e-02	7.58e-03	1.49e-02	7.63e-04	0.00e+00	2.41e-03	5.15e-04	3.81e-04	9.96e-03	2.06e-03	8.17e-02		8.17e-02
1	803456	812033	133628	133955	8.00e-03	2.96e-02	3.92e-03	2.31e-03	8.03e-04	0.00e+00	2.88e-03	4.03e-04	3.59e-04	5.08e-03	2.21e-03	5.56e-02		1.37e-01
2	795909	819361	145568	136991	7.93e-03	4.60e-02	4.27e-03	1.77e-03	2.73e-04	0.00e+00	3.23e-03	4.83e-04	4.10e-04	4.00e-03	2.44e-03	7.08e-02		2.08e-01
3	807290	801798	153921	130469	8.63e-03	4.88e-02	4.74e-03	2.39e-03	5.15e-04	0.00e+00	3.77e-03	5.96e-04	4.68e-04	3.91e-03	2.50e-03	7.64e-02		2.84e-01
4	810749	804564	143346	148838	8.52e-03	4.88e-02	4.96e-03	1.72e-03	4.42e-04	0.00e+00	3.98e-03	6.80e-04	4.54e-04	4.12e-03	2.47e-03	7.62e-02		3.61e-01
5	806045	811518	140692	146877	8.51e-03	4.51e-02	5.10e-03	2.93e-03	1.09e-03	0.00e+00	4.00e-03	7.80e-04	4.62e-04	4.45e-03	2.49e-03	7.49e-02		4.35e-01
6	812818	809903	146202	140729	8.59e-03	5.23e-02	5.22e-03	2.32e-03	4.82e-04	0.00e+00	4.02e-03	5.25e-04	4.95e-04	4.30e-03	2.50e-03	8.08e-02		5.16e-01
7	813783	811634	140235	143150	8.49e-03	4.63e-02	5.32e-03	4.19e-03	5.47e-04	0.00e+00	4.03e-03	5.66e-04	4.31e-04	4.09e-03	2.51e-03	7.65e-02		5.93e-01
8	810775	808689	140076	142225	7.95e-03	4.98e-02	5.34e-03	3.13e-03	1.36e-03	0.00e+00	4.05e-03	5.70e-04	4.57e-04	4.01e-03	2.48e-03	7.91e-02		6.72e-01
9	812634	812732	143890	145976	7.95e-03	4.60e-02	5.30e-03	2.00e-03	1.08e-03	0.00e+00	3.89e-03	7.01e-04	4.50e-04	3.88e-03	2.55e-03	7.38e-02		7.46e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.3838 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.3838 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.383844  0.000000
];

