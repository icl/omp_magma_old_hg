% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_139 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	813450	805178	141586	141586	7.36e-03	3.40e-02	6.59e-03	1.37e-02	1.13e-03	0.00e+00	2.75e-03	4.78e-04	4.24e-04	9.42e-03	2.26e-03	7.81e-02		7.81e-02
1	799602	800568	134768	143040	7.29e-03	2.95e-02	4.22e-03	7.04e-03	2.23e-03	0.00e+00	3.18e-03	4.01e-04	3.88e-04	5.72e-03	2.26e-03	6.22e-02		1.40e-01
2	808302	808621	149422	148456	7.26e-03	4.68e-02	4.27e-03	2.52e-03	3.36e-04	0.00e+00	3.74e-03	4.96e-04	4.27e-04	4.07e-03	2.32e-03	7.23e-02		2.13e-01
3	828133	811002	141528	141209	7.40e-03	5.12e-02	4.51e-03	2.43e-03	3.55e-04	0.00e+00	4.39e-03	6.46e-04	4.99e-04	4.03e-03	2.32e-03	7.78e-02		2.90e-01
4	818173	804789	122503	139634	7.39e-03	5.53e-02	4.73e-03	2.37e-03	4.60e-04	0.00e+00	4.60e-03	5.95e-04	4.96e-04	4.46e-03	2.17e-03	8.26e-02		3.73e-01
5	815069	808380	133268	146652	7.29e-03	4.76e-02	5.70e-03	2.26e-03	4.09e-04	0.00e+00	4.54e-03	5.99e-04	4.97e-04	4.60e-03	2.74e-03	7.63e-02		4.49e-01
6	806055	813389	137178	143867	8.67e-03	5.25e-02	5.74e-03	2.45e-03	4.12e-04	0.00e+00	4.42e-03	4.60e-04	5.10e-04	4.41e-03	2.78e-03	8.24e-02		5.32e-01
7	814304	807655	146998	139664	8.70e-03	4.91e-02	5.76e-03	2.35e-03	3.90e-04	0.00e+00	4.42e-03	4.54e-04	4.98e-04	4.10e-03	2.77e-03	7.85e-02		6.10e-01
8	814922	811479	139555	146204	8.61e-03	5.26e-02	5.82e-03	1.84e-03	3.93e-04	0.00e+00	4.43e-03	6.87e-04	4.99e-04	4.24e-03	2.79e-03	8.19e-02		6.92e-01
9	810911	817856	139743	143186	8.68e-03	5.83e-02	5.65e-03	1.79e-03	1.18e-03	0.00e+00	5.05e-03	4.73e-04	4.73e-04	4.04e-03	2.21e-03	8.79e-02		7.80e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.3934 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.3934 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.393385  0.000000
];

