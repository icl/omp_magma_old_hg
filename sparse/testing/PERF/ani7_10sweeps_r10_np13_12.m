% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_13 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	819937	800216	141586	141586	3.98e-03	8.39e-02	1.76e-02	9.55e-03	1.51e-03	0.00e+00	1.93e-02	2.94e-04	1.97e-03	9.21e-03	1.11e-02	1.58e-01		1.58e-01
1	826057	824468	128281	148002	3.97e-03	1.06e-01	1.69e-02	4.77e-03	2.03e-03	0.00e+00	2.36e-02	2.09e-04	2.17e-03	8.70e-03	1.48e-02	1.83e-01		3.42e-01
2	811652	821053	122967	124556	4.13e-03	1.76e-01	2.30e-02	4.19e-03	1.82e-03	0.00e+00	2.89e-02	3.28e-04	2.28e-03	7.96e-03	1.59e-02	2.64e-01		6.06e-01
3	819723	810277	138178	128777	4.11e-03	2.58e-01	2.48e-02	4.44e-03	2.08e-03	0.00e+00	3.27e-02	2.96e-04	2.59e-03	7.90e-03	1.49e-02	3.51e-01		9.57e-01
4	808290	804340	130913	140359	4.10e-03	2.42e-01	2.58e-02	4.86e-03	2.73e-03	0.00e+00	3.53e-02	3.99e-04	2.67e-03	7.83e-03	1.44e-02	3.40e-01		1.30e+00
5	811613	814106	143151	147101	4.05e-03	2.28e-01	2.54e-02	4.22e-03	2.65e-03	0.00e+00	3.43e-02	3.38e-04	2.56e-03	8.45e-03	1.46e-02	3.24e-01		1.62e+00
6	810140	812126	140634	138141	4.08e-03	2.36e-01	2.59e-02	4.95e-03	2.77e-03	0.00e+00	3.57e-02	4.01e-04	2.61e-03	7.87e-03	1.45e-02	3.35e-01		1.96e+00
7	812243	811445	142913	140927	4.09e-03	2.35e-01	2.59e-02	4.87e-03	2.85e-03	0.00e+00	3.59e-02	3.30e-04	2.62e-03	8.20e-03	1.46e-02	3.34e-01		2.29e+00
8	812265	814177	141616	142414	4.07e-03	2.37e-01	2.60e-02	4.46e-03	3.10e-03	0.00e+00	3.55e-02	3.93e-04	2.64e-03	7.83e-03	1.46e-02	3.35e-01		2.63e+00
9	811810	812394	142400	140488	4.11e-03	2.38e-01	2.62e-02	5.16e-03	3.14e-03	0.00e+00	3.60e-02	3.45e-04	2.63e-03	7.81e-03	1.46e-02	3.38e-01		2.96e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 3.3967 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 3.3967 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  3.396699  0.000000
];

