% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_21 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	812376	797385	141586	141586	4.51e-03	6.18e-02	1.34e-02	7.67e-03	1.73e-03	0.00e+00	1.18e-02	2.72e-04	1.25e-03	8.17e-03	7.99e-03	1.19e-01		1.19e-01
1	808628	821647	135842	150833	4.24e-03	7.71e-02	1.30e-02	4.62e-03	2.14e-03	0.00e+00	1.42e-02	2.50e-04	1.51e-03	6.62e-03	1.01e-02	1.34e-01		2.52e-01
2	804289	823516	140396	127377	4.29e-03	1.19e-01	1.56e-02	3.71e-03	1.36e-03	0.00e+00	1.76e-02	2.80e-04	1.44e-03	6.09e-03	1.06e-02	1.80e-01		4.32e-01
3	813919	797200	145541	126314	4.80e-03	1.64e-01	1.75e-02	3.14e-03	1.46e-03	0.00e+00	2.02e-02	3.20e-04	1.84e-03	5.80e-03	1.02e-02	2.29e-01		6.61e-01
4	807882	815890	136717	153436	4.56e-03	1.46e-01	1.78e-02	3.30e-03	2.01e-03	0.00e+00	2.22e-02	3.02e-04	1.75e-03	5.81e-03	1.03e-02	2.14e-01		8.76e-01
5	811703	811803	143559	135551	4.51e-03	1.53e-01	1.85e-02	3.18e-03	1.73e-03	0.00e+00	2.22e-02	3.92e-04	1.61e-03	6.76e-03	1.01e-02	2.22e-01		1.10e+00
6	804840	809194	140544	140444	4.75e-03	1.58e-01	1.90e-02	3.08e-03	1.71e-03	0.00e+00	2.23e-02	4.71e-04	1.69e-03	6.00e-03	1.02e-02	2.27e-01		1.32e+00
7	808524	811045	148213	143859	4.48e-03	1.55e-01	1.86e-02	3.87e-03	1.99e-03	0.00e+00	2.25e-02	4.17e-04	1.75e-03	6.00e-03	1.03e-02	2.25e-01		1.55e+00
8	819878	815325	145335	142814	4.61e-03	1.59e-01	1.89e-02	2.85e-03	2.09e-03	0.00e+00	2.18e-02	4.68e-04	1.67e-03	6.02e-03	1.04e-02	2.28e-01		1.78e+00
9	810279	810905	134787	139340	4.79e-03	1.65e-01	1.93e-02	3.32e-03	2.07e-03	0.00e+00	2.23e-02	4.01e-04	1.75e-03	5.83e-03	1.03e-02	2.35e-01		2.01e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 2.4353 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 2.4353 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  2.435347  0.000000
];

