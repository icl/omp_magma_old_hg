% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_44 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	811856	800331	141586	141586	3.26e-03	3.18e-02	5.72e-03	6.72e-03	1.06e-03	0.00e+00	5.42e-03	2.48e-04	6.10e-04	5.90e-03	3.18e-03	6.39e-02		6.39e-02
1	817190	819029	136362	147887	3.24e-03	3.71e-02	4.76e-03	2.29e-03	1.19e-03	0.00e+00	6.57e-03	2.43e-04	6.37e-04	4.34e-03	4.24e-03	6.46e-02		1.29e-01
2	813220	824035	131834	129995	3.33e-03	5.86e-02	6.60e-03	1.84e-03	8.53e-04	0.00e+00	8.13e-03	3.26e-04	6.79e-04	3.71e-03	4.69e-03	8.87e-02		2.17e-01
3	810223	812518	136610	125795	3.37e-03	8.59e-02	7.10e-03	2.11e-03	6.63e-04	0.00e+00	9.34e-03	3.46e-04	7.79e-04	3.73e-03	4.30e-03	1.18e-01		3.35e-01
4	801423	810251	140413	138118	3.36e-03	8.22e-02	7.25e-03	1.60e-03	9.54e-04	0.00e+00	9.97e-03	2.76e-04	8.39e-04	3.69e-03	4.12e-03	1.14e-01		4.49e-01
5	812648	810916	150018	141190	3.31e-03	7.75e-02	7.19e-03	1.92e-03	1.03e-03	0.00e+00	1.03e-02	3.20e-04	8.85e-04	4.13e-03	4.22e-03	1.11e-01		5.60e-01
6	809436	816035	139599	141331	3.29e-03	8.10e-02	7.34e-03	1.83e-03	1.11e-03	0.00e+00	1.05e-02	3.86e-04	8.91e-04	3.84e-03	4.21e-03	1.14e-01		6.74e-01
7	811926	809739	143617	137018	3.37e-03	8.21e-02	7.41e-03	2.11e-03	1.07e-03	0.00e+00	1.05e-02	3.74e-04	8.94e-04	3.87e-03	4.22e-03	1.16e-01		7.90e-01
8	813815	810359	141933	144120	3.31e-03	8.28e-02	7.38e-03	2.06e-03	1.40e-03	0.00e+00	1.03e-02	5.66e-04	8.33e-04	3.80e-03	4.22e-03	1.17e-01		9.07e-01
9	813546	811886	140850	144306	3.32e-03	8.28e-02	7.44e-03	1.88e-03	1.33e-03	0.00e+00	1.03e-02	2.93e-04	8.54e-04	3.74e-03	4.23e-03	1.16e-01		1.02e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.3925 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.3925 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.392526  0.000000
];

