% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_36 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	816135	795301	141586	141586	3.30e-03	3.57e-02	6.73e-03	6.67e-03	8.67e-04	0.00e+00	6.68e-03	2.47e-04	7.39e-04	6.24e-03	3.84e-03	7.10e-02		7.10e-02
1	801738	805915	132083	152917	3.24e-03	4.27e-02	5.77e-03	3.93e-03	6.68e-04	0.00e+00	8.20e-03	2.15e-04	7.94e-04	4.62e-03	4.98e-03	7.51e-02		1.46e-01
2	808112	791531	147286	143109	3.34e-03	6.70e-02	7.51e-03	2.51e-03	7.69e-04	0.00e+00	9.95e-03	3.52e-04	8.55e-04	4.37e-03	5.37e-03	1.02e-01		2.48e-01
3	813623	805639	141718	158299	3.28e-03	9.79e-02	8.23e-03	1.89e-03	7.45e-04	0.00e+00	1.13e-02	3.23e-04	9.49e-04	4.07e-03	5.23e-03	1.34e-01		3.82e-01
4	810650	806916	137013	144997	3.33e-03	9.29e-02	8.79e-03	2.13e-03	1.50e-03	0.00e+00	1.19e-02	2.74e-04	1.04e-03	4.03e-03	5.07e-03	1.31e-01		5.13e-01
5	812053	811949	140791	144525	3.36e-03	9.03e-02	8.82e-03	3.16e-03	1.08e-03	0.00e+00	1.22e-02	2.60e-04	9.79e-04	4.27e-03	5.12e-03	1.30e-01		6.43e-01
6	812939	807699	140194	140298	3.34e-03	9.32e-02	8.97e-03	2.17e-03	1.04e-03	0.00e+00	1.23e-02	2.89e-04	9.85e-04	4.09e-03	5.12e-03	1.31e-01		7.74e-01
7	811117	807340	140114	145354	3.33e-03	9.28e-02	8.95e-03	2.51e-03	1.11e-03	0.00e+00	1.22e-02	3.52e-04	1.01e-03	4.30e-03	5.10e-03	1.32e-01		9.06e-01
8	811613	809529	142742	146519	3.36e-03	9.36e-02	9.01e-03	2.07e-03	1.69e-03	0.00e+00	1.26e-02	4.46e-04	1.01e-03	4.04e-03	5.08e-03	1.33e-01		1.04e+00
9	814540	813007	143052	145136	3.37e-03	9.38e-02	9.02e-03	3.67e-03	1.43e-03	0.00e+00	1.25e-02	3.74e-04	1.02e-03	4.08e-03	5.14e-03	1.34e-01		1.17e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.5473 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.5473 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.547295  0.000000
];

