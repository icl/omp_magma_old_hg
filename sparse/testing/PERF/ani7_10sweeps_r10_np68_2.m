% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_68 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	820491	811327	141586	141586	3.22e-03	2.70e-02	4.55e-03	8.65e-03	9.30e-04	0.00e+00	3.60e-03	2.52e-04	4.03e-04	5.75e-03	2.15e-03	5.65e-02		5.65e-02
1	820658	790645	127727	136891	3.45e-03	3.01e-02	3.67e-03	1.84e-03	1.01e-03	0.00e+00	4.51e-03	2.36e-04	4.58e-04	3.99e-03	2.87e-03	5.21e-02		1.09e-01
2	797743	804185	128366	158379	3.19e-03	5.06e-02	4.05e-03	3.01e-03	7.50e-04	0.00e+00	5.38e-03	3.07e-04	5.03e-04	3.23e-03	2.98e-03	7.40e-02		1.83e-01
3	810156	808934	152087	145645	3.26e-03	5.91e-02	4.41e-03	2.00e-03	5.94e-04	0.00e+00	6.09e-03	3.77e-04	5.89e-04	3.35e-03	2.91e-03	8.27e-02		2.65e-01
4	808360	815955	140480	141702	3.53e-03	6.07e-02	5.09e-03	2.27e-03	8.48e-04	0.00e+00	6.70e-03	3.26e-04	6.84e-04	3.42e-03	2.80e-03	8.64e-02		3.52e-01
5	810703	805418	143081	135486	3.26e-03	5.62e-02	4.75e-03	2.52e-03	6.06e-04	0.00e+00	6.69e-03	5.06e-04	6.34e-04	3.64e-03	2.76e-03	8.16e-02		4.33e-01
6	813250	814843	141544	146829	3.27e-03	5.83e-02	4.75e-03	2.04e-03	6.58e-04	0.00e+00	6.53e-03	3.57e-04	6.19e-04	3.44e-03	2.85e-03	8.28e-02		5.16e-01
7	809329	808154	139803	138210	3.68e-03	5.88e-02	4.84e-03	1.44e-03	6.32e-04	0.00e+00	6.64e-03	2.64e-04	6.28e-04	3.54e-03	2.81e-03	8.33e-02		5.99e-01
8	813116	815029	144530	145705	3.25e-03	6.02e-02	4.78e-03	1.23e-03	1.22e-03	0.00e+00	6.56e-03	3.72e-04	6.30e-04	3.51e-03	2.97e-03	8.47e-02		6.84e-01
9	815264	815472	141549	139636	3.32e-03	5.91e-02	4.88e-03	1.33e-03	9.68e-04	0.00e+00	6.72e-03	3.44e-04	6.55e-04	3.34e-03	2.85e-03	8.35e-02		7.68e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.1652 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.1652 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.165176  0.000000
];

