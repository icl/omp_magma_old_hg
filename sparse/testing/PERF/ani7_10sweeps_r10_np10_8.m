% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_10 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	821503	810501	141586	141586	4.17e-03	1.06e-01	2.26e-02	9.25e-03	1.69e-03	0.00e+00	2.45e-02	2.99e-04	2.46e-03	1.07e-02	1.46e-02	1.97e-01		1.97e-01
1	797271	789791	126715	137717	4.18e-03	1.33e-01	2.22e-02	5.04e-03	2.12e-03	0.00e+00	3.03e-02	2.51e-04	2.74e-03	9.83e-03	1.83e-02	2.28e-01		4.25e-01
2	808819	802897	151753	159233	4.22e-03	2.15e-01	2.74e-02	5.47e-03	2.15e-03	0.00e+00	3.56e-02	3.12e-04	2.92e-03	9.57e-03	2.01e-02	3.23e-01		7.48e-01
3	805020	804362	141011	146933	4.23e-03	3.07e-01	3.15e-02	6.03e-03	2.68e-03	0.00e+00	4.15e-02	3.39e-04	3.19e-03	9.43e-03	1.87e-02	4.24e-01		1.17e+00
4	812248	807207	145616	146274	4.24e-03	2.80e-01	3.20e-02	6.24e-03	3.65e-03	0.00e+00	4.52e-02	3.52e-04	3.40e-03	9.92e-03	1.82e-02	4.04e-01		1.58e+00
5	808252	811290	139193	144234	4.60e-03	2.93e-01	3.26e-02	6.86e-03	4.81e-03	0.00e+00	4.76e-02	3.17e-04	3.50e-03	1.00e-02	1.85e-02	4.22e-01		2.00e+00
6	811765	815468	143995	140957	4.28e-03	2.97e-01	3.30e-02	5.92e-03	3.08e-03	0.00e+00	4.32e-02	4.93e-04	3.13e-03	9.61e-03	1.87e-02	4.19e-01		2.42e+00
7	811053	814532	141288	137585	4.29e-03	3.09e-01	3.35e-02	5.57e-03	3.23e-03	0.00e+00	4.38e-02	4.96e-04	3.22e-03	9.70e-03	1.86e-02	4.32e-01		2.85e+00
8	812612	813294	142806	139327	4.29e-03	3.11e-01	3.35e-02	5.55e-03	3.93e-03	0.00e+00	4.51e-02	2.96e-04	3.32e-03	9.62e-03	1.87e-02	4.35e-01		3.28e+00
9	814337	806716	142053	141371	4.34e-03	3.10e-01	3.37e-02	5.43e-03	4.15e-03	0.00e+00	4.61e-02	2.65e-04	3.36e-03	9.55e-03	1.86e-02	4.35e-01		3.72e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 4.1785 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 4.1785 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  4.178499  0.000000
];

