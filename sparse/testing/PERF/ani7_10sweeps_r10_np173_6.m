% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_173 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	819851	805894	141586	141586	8.36e-03	3.62e-02	7.44e-03	1.48e-02	1.85e-03	0.00e+00	2.24e-03	4.70e-04	3.60e-04	9.33e-03	1.91e-03	8.30e-02		8.30e-02
1	811215	814122	128367	142324	8.29e-03	3.09e-02	3.62e-03	2.55e-03	1.50e-03	0.00e+00	2.66e-03	4.70e-04	3.92e-04	5.87e-03	2.09e-03	5.83e-02		1.41e-01
2	808786	806627	137809	134902	8.31e-03	4.54e-02	4.07e-03	3.32e-03	9.81e-04	0.00e+00	3.07e-03	5.40e-04	3.80e-04	3.87e-03	2.14e-03	7.20e-02		2.13e-01
3	812464	815682	141044	143203	8.24e-03	4.57e-02	4.29e-03	4.14e-03	3.58e-04	0.00e+00	3.51e-03	4.70e-04	4.03e-04	3.94e-03	2.37e-03	7.35e-02		2.87e-01
4	811737	805842	138172	134954	8.30e-03	4.94e-02	6.64e-03	2.36e-03	4.11e-04	0.00e+00	3.60e-03	8.89e-04	4.37e-04	3.92e-03	2.34e-03	7.83e-02		3.65e-01
5	809018	810082	139704	145599	8.19e-03	4.37e-02	4.76e-03	2.63e-03	4.26e-04	0.00e+00	3.68e-03	4.49e-04	4.23e-04	4.37e-03	2.36e-03	7.10e-02		4.36e-01
6	811582	810366	143229	142165	8.28e-03	4.87e-02	4.91e-03	2.92e-03	3.54e-04	0.00e+00	3.63e-03	6.31e-04	4.16e-04	4.08e-03	2.38e-03	7.63e-02		5.12e-01
7	810979	814587	141471	142687	8.29e-03	4.51e-02	4.86e-03	1.55e-03	3.42e-04	0.00e+00	3.64e-03	7.37e-04	4.40e-04	4.15e-03	2.38e-03	7.15e-02		5.84e-01
8	810328	805633	142880	139272	8.36e-03	5.00e-02	5.05e-03	2.90e-03	1.00e-03	0.00e+00	3.72e-03	5.86e-04	4.18e-04	4.11e-03	2.33e-03	7.85e-02		6.62e-01
9	812566	809974	144337	149032	8.23e-03	4.47e-02	4.89e-03	2.14e-03	1.10e-03	0.00e+00	3.66e-03	7.23e-04	4.35e-04	3.90e-03	2.33e-03	7.21e-02		7.35e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.3825 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.3825 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.382485  0.000000
];

