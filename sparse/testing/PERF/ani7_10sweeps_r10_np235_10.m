% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_235 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	822556	798717	141586	141586	1.06e-02	4.29e-02	9.04e-03	1.99e-02	1.05e-03	0.00e+00	2.30e-03	5.80e-04	3.52e-04	1.42e-02	1.75e-03	1.03e-01		1.03e-01
1	815510	807548	125662	149501	1.05e-02	3.59e-02	3.36e-03	3.41e-03	1.28e-03	0.00e+00	2.88e-03	5.08e-04	3.80e-04	6.59e-03	1.94e-03	6.68e-02		1.70e-01
2	814021	788709	133514	141476	1.06e-02	5.20e-02	3.74e-03	1.92e-03	3.29e-04	0.00e+00	3.24e-03	7.41e-04	3.77e-04	4.14e-03	2.00e-03	7.91e-02		2.49e-01
3	814930	807167	135809	161121	1.03e-02	5.01e-02	3.96e-03	2.54e-03	3.91e-04	0.00e+00	3.53e-03	7.19e-04	4.09e-04	4.34e-03	2.16e-03	7.85e-02		3.27e-01
4	810641	808489	135706	143469	1.06e-02	5.67e-02	4.36e-03	2.33e-03	3.85e-04	0.00e+00	3.77e-03	7.13e-04	4.20e-04	4.38e-03	2.19e-03	8.58e-02		4.13e-01
5	810098	809766	140800	142952	1.06e-02	4.98e-02	4.47e-03	2.95e-03	1.46e-03	0.00e+00	3.87e-03	6.07e-04	4.08e-04	5.02e-03	2.17e-03	8.13e-02		4.94e-01
6	808335	809990	142149	142481	1.06e-02	5.54e-02	4.48e-03	2.14e-03	3.92e-04	0.00e+00	3.81e-03	6.85e-04	4.10e-04	4.90e-03	2.17e-03	8.51e-02		5.79e-01
7	811726	815067	144718	143063	1.07e-02	5.03e-02	4.54e-03	1.97e-03	3.78e-04	0.00e+00	3.88e-03	8.28e-04	4.06e-04	4.71e-03	2.24e-03	7.99e-02		6.59e-01
8	811358	812716	142133	138792	1.07e-02	5.63e-02	4.62e-03	1.91e-03	1.68e-03	0.00e+00	5.16e-03	5.36e-04	4.30e-04	4.33e-03	2.21e-03	8.79e-02		7.47e-01
9	812062	812944	143307	141949	1.07e-02	5.10e-02	4.61e-03	2.96e-03	1.33e-03	0.00e+00	3.80e-03	5.17e-04	4.28e-04	4.22e-03	2.46e-03	8.20e-02		8.29e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.5825 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.5825 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.582549  0.000000
];

