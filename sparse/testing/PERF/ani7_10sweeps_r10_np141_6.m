% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_141 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	814121	807196	141586	141586	7.43e-03	3.31e-02	6.72e-03	1.38e-02	1.11e-03	0.00e+00	2.75e-03	4.93e-04	4.23e-04	8.57e-03	2.25e-03	7.67e-02		7.67e-02
1	809480	818704	134097	141022	7.41e-03	3.02e-02	4.18e-03	3.17e-03	1.35e-03	0.00e+00	3.24e-03	4.13e-04	4.03e-04	5.25e-03	2.32e-03	5.79e-02		1.35e-01
2	802146	818427	139544	130320	7.52e-03	4.67e-02	4.45e-03	2.46e-03	6.45e-04	0.00e+00	3.71e-03	6.32e-04	4.22e-04	4.02e-03	2.30e-03	7.29e-02		2.07e-01
3	798892	812191	147684	131403	7.50e-03	5.14e-02	4.74e-03	2.15e-03	3.60e-04	0.00e+00	4.19e-03	7.55e-04	4.69e-04	3.97e-03	2.20e-03	7.78e-02		2.85e-01
4	803270	816643	151744	138445	7.43e-03	5.04e-02	5.06e-03	1.49e-03	4.47e-04	0.00e+00	4.43e-03	4.72e-04	5.12e-04	4.01e-03	2.19e-03	7.65e-02		3.62e-01
5	804638	810549	148171	134798	7.44e-03	4.84e-02	5.23e-03	2.13e-03	4.65e-04	0.00e+00	4.44e-03	4.71e-04	4.92e-04	4.37e-03	2.20e-03	7.56e-02		4.37e-01
6	808331	809932	147609	141698	7.41e-03	5.08e-02	5.11e-03	1.68e-03	4.09e-04	0.00e+00	4.50e-03	8.05e-04	4.93e-04	4.07e-03	2.18e-03	7.75e-02		5.15e-01
7	809891	816069	144722	143121	7.42e-03	4.84e-02	5.20e-03	2.06e-03	4.41e-04	0.00e+00	4.34e-03	5.76e-04	4.63e-04	4.42e-03	2.20e-03	7.55e-02		5.90e-01
8	814184	813390	143968	137790	7.46e-03	5.14e-02	5.36e-03	2.09e-03	1.08e-03	0.00e+00	4.53e-03	8.62e-04	4.71e-04	4.01e-03	2.18e-03	7.95e-02		6.70e-01
9	809546	812855	140481	141275	7.46e-03	4.90e-02	5.36e-03	1.41e-03	1.07e-03	0.00e+00	4.40e-03	6.18e-04	4.68e-04	4.04e-03	2.18e-03	7.60e-02		7.46e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.3838 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.3838 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.383780  0.000000
];

