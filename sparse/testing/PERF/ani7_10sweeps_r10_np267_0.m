% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_267 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	824385	805976	141586	141586	1.16e-02	4.51e-02	9.75e-03	2.17e-02	1.01e-03	0.00e+00	2.62e-03	4.89e-04	3.31e-04	1.34e-02	1.58e-03	1.08e-01		1.08e-01
1	801055	811913	123833	142242	1.16e-02	3.68e-02	3.03e-03	5.66e-03	8.78e-04	0.00e+00	3.15e-03	6.10e-04	3.52e-04	7.11e-03	1.82e-03	7.10e-02		1.79e-01
2	810367	801621	147969	137111	1.17e-02	5.08e-02	3.52e-03	1.74e-03	2.73e-04	0.00e+00	3.42e-03	6.23e-04	4.15e-04	4.23e-03	2.04e-03	7.88e-02		2.57e-01
3	803982	812561	139463	148209	1.16e-02	5.40e-02	4.11e-03	3.17e-03	3.18e-04	0.00e+00	3.80e-03	6.52e-04	4.23e-04	4.22e-03	2.10e-03	8.43e-02		3.42e-01
4	807156	801005	146654	138075	1.18e-02	5.70e-02	4.26e-03	1.57e-03	3.42e-04	0.00e+00	3.87e-03	7.50e-04	4.58e-04	5.55e-03	1.99e-03	8.75e-02		4.29e-01
5	810611	814446	144285	150436	1.15e-02	5.11e-02	4.09e-03	4.03e-03	1.08e-03	0.00e+00	4.00e-03	5.61e-04	4.04e-04	4.83e-03	2.07e-03	8.37e-02		5.13e-01
6	812003	813025	141636	137801	1.17e-02	5.67e-02	4.27e-03	2.25e-03	4.03e-04	0.00e+00	3.98e-03	8.38e-04	4.61e-04	4.94e-03	2.06e-03	8.76e-02		6.01e-01
7	812565	812620	141050	140028	1.18e-02	5.36e-02	4.22e-03	2.22e-03	3.99e-04	0.00e+00	4.04e-03	7.55e-04	4.15e-04	4.17e-03	2.07e-03	8.36e-02		6.84e-01
8	811169	812286	141294	141239	1.17e-02	5.81e-02	4.27e-03	1.88e-03	1.57e-03	0.00e+00	3.99e-03	5.88e-04	4.27e-04	4.47e-03	2.08e-03	8.90e-02		7.73e-01
9	811103	814730	143496	142379	1.17e-02	5.38e-02	4.27e-03	1.49e-03	1.69e-03	0.00e+00	3.93e-03	6.27e-04	4.57e-04	4.20e-03	2.09e-03	8.42e-02		8.57e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.9811 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.9811 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.981087  0.000000
];

