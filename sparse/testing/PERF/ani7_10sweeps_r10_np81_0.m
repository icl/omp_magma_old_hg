% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_81 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	810337	804035	141586	141586	5.26e-03	3.01e-02	6.67e-03	9.99e-03	1.04e-03	0.00e+00	3.30e-03	2.92e-04	5.49e-04	6.76e-03	2.52e-03	6.65e-02		6.65e-02
1	801048	815518	137881	144183	5.22e-03	2.77e-02	4.70e-03	2.74e-03	7.20e-04	0.00e+00	3.76e-03	2.77e-04	4.97e-04	3.86e-03	2.73e-03	5.22e-02		1.19e-01
2	815281	812389	147976	133506	5.30e-03	4.87e-02	5.08e-03	3.22e-03	4.08e-04	0.00e+00	4.63e-03	3.88e-04	5.08e-04	3.71e-03	2.76e-03	7.47e-02		1.93e-01
3	802281	815594	134549	137441	5.26e-03	5.88e-02	6.82e-03	1.67e-03	3.75e-04	0.00e+00	5.14e-03	4.43e-04	5.38e-04	3.31e-03	2.84e-03	8.52e-02		2.79e-01
4	814669	806897	148355	135042	5.27e-03	5.53e-02	5.84e-03	1.64e-03	7.33e-04	0.00e+00	5.63e-03	5.41e-04	5.98e-04	3.33e-03	2.88e-03	8.18e-02		3.60e-01
5	806492	814457	136772	144544	5.23e-03	5.13e-02	6.16e-03	1.34e-03	7.09e-04	0.00e+00	5.60e-03	4.41e-04	5.82e-04	3.75e-03	2.87e-03	7.80e-02		4.38e-01
6	804304	810129	145755	137790	5.29e-03	5.44e-02	5.99e-03	3.01e-03	5.52e-04	0.00e+00	5.45e-03	4.93e-04	5.53e-04	3.29e-03	2.81e-03	8.18e-02		5.20e-01
7	810988	809944	148749	142924	5.24e-03	5.05e-02	5.97e-03	2.03e-03	6.37e-04	0.00e+00	5.52e-03	5.88e-04	5.52e-04	3.51e-03	2.86e-03	7.74e-02		5.98e-01
8	810672	815948	142871	143915	5.26e-03	5.46e-02	6.07e-03	2.13e-03	1.06e-03	0.00e+00	5.56e-03	5.92e-04	5.59e-04	3.46e-03	2.89e-03	8.21e-02		6.80e-01
9	813886	813319	143993	138717	5.29e-03	5.30e-02	6.12e-03	1.59e-03	8.56e-04	0.00e+00	5.26e-03	4.60e-04	5.38e-04	3.37e-03	2.89e-03	7.94e-02		7.59e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.2358 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.2358 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.235802  0.000000
];

