% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_253 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	825815	823340	141586	141586	1.13e-02	4.38e-02	9.19e-03	2.04e-02	2.35e-03	0.00e+00	2.19e-03	4.74e-04	3.76e-04	1.26e-02	1.67e-03	1.04e-01		1.04e-01
1	806043	810331	122403	124878	1.14e-02	3.61e-02	3.25e-03	5.28e-03	1.86e-03	0.00e+00	2.85e-03	4.24e-04	3.93e-04	7.19e-03	1.91e-03	7.07e-02		1.75e-01
2	794357	806528	142981	138693	1.12e-02	5.12e-02	3.69e-03	2.00e-03	1.43e-03	0.00e+00	3.09e-03	4.74e-04	3.87e-04	4.26e-03	2.05e-03	7.98e-02		2.55e-01
3	789723	821422	155473	143302	1.11e-02	4.88e-02	3.93e-03	2.52e-03	3.65e-04	0.00e+00	3.33e-03	6.20e-04	4.15e-04	4.32e-03	2.03e-03	7.74e-02		3.32e-01
4	797977	803790	160913	129214	1.14e-02	5.37e-02	4.77e-03	1.88e-03	3.58e-04	0.00e+00	3.50e-03	6.79e-04	4.34e-04	4.22e-03	1.97e-03	8.28e-02		4.15e-01
5	813055	811656	153464	147651	1.11e-02	4.84e-02	4.01e-03	2.36e-03	4.62e-04	0.00e+00	3.51e-03	8.09e-04	4.20e-04	4.99e-03	2.02e-03	7.80e-02		4.93e-01
6	808138	812477	139192	140591	1.12e-02	5.65e-02	4.17e-03	2.28e-03	4.12e-04	0.00e+00	3.61e-03	6.60e-04	4.55e-04	4.26e-03	2.03e-03	8.55e-02		5.79e-01
7	810317	812507	144915	140576	1.13e-02	5.06e-02	4.17e-03	2.09e-03	4.70e-04	0.00e+00	3.64e-03	5.23e-04	4.70e-04	4.70e-03	2.04e-03	8.00e-02		6.59e-01
8	819540	811068	143542	141352	1.12e-02	5.63e-02	4.23e-03	2.15e-03	1.58e-03	0.00e+00	3.69e-03	8.47e-04	4.58e-04	4.47e-03	2.05e-03	8.70e-02		7.46e-01
9	812488	812080	135125	143597	1.12e-02	5.12e-02	4.28e-03	1.39e-03	1.47e-03	0.00e+00	3.53e-03	7.42e-04	4.54e-04	4.25e-03	2.05e-03	8.05e-02		8.26e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.5817 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.5817 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.581742  0.000000
];

