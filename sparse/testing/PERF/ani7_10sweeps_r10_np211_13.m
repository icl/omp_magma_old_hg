% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_211 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	819560	811835	141586	141586	9.82e-03	4.08e-02	8.34e-03	1.83e-02	1.28e-03	0.00e+00	2.58e-03	4.69e-04	4.15e-04	1.31e-02	1.92e-03	9.70e-02		9.70e-02
1	803226	803135	128658	136383	9.80e-03	3.63e-02	3.72e-03	6.48e-03	2.32e-03	0.00e+00	3.34e-03	4.67e-04	4.00e-04	6.84e-03	2.09e-03	7.17e-02		1.69e-01
2	806045	804592	145798	145889	9.63e-03	5.39e-02	3.82e-03	4.34e-03	2.95e-04	0.00e+00	3.52e-03	5.99e-04	4.02e-04	4.36e-03	2.15e-03	8.30e-02		2.52e-01
3	806425	804120	143785	145238	9.71e-03	5.30e-02	4.12e-03	2.87e-03	3.41e-04	0.00e+00	3.92e-03	6.63e-04	4.39e-04	4.22e-03	2.10e-03	8.14e-02		3.33e-01
4	822801	808199	144211	146516	9.69e-03	5.58e-02	4.33e-03	3.26e-03	7.09e-04	0.00e+00	4.25e-03	6.43e-04	4.91e-04	4.45e-03	2.10e-03	8.57e-02		4.19e-01
5	801865	809672	128640	143242	9.72e-03	5.11e-02	4.72e-03	2.09e-03	4.09e-04	0.00e+00	4.13e-03	6.36e-04	4.77e-04	5.11e-03	2.05e-03	8.05e-02		4.99e-01
6	808762	803253	150382	142575	9.74e-03	5.47e-02	4.44e-03	2.86e-03	4.21e-04	0.00e+00	4.17e-03	7.93e-04	4.68e-04	4.53e-03	2.08e-03	8.42e-02		5.84e-01
7	815623	808568	144291	149800	9.72e-03	5.04e-02	4.47e-03	2.20e-03	4.55e-04	0.00e+00	4.20e-03	6.51e-04	4.61e-04	4.30e-03	2.13e-03	7.90e-02		6.63e-01
8	813685	818008	138236	145291	9.73e-03	5.74e-02	4.61e-03	2.22e-03	4.81e-04	0.00e+00	4.17e-03	5.63e-04	4.45e-04	4.69e-03	2.15e-03	8.64e-02		7.49e-01
9	813152	816249	140980	136657	9.84e-03	5.29e-02	4.70e-03	3.33e-03	1.44e-03	0.00e+00	4.22e-03	4.88e-04	4.39e-04	4.30e-03	2.15e-03	8.38e-02		8.33e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.5748 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.5748 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.574774  0.000000
];

