% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_166 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	815950	811334	141586	141586	8.40e-03	3.67e-02	7.50e-03	1.51e-02	6.95e-04	0.00e+00	2.41e-03	4.79e-04	3.69e-04	9.73e-03	1.99e-03	8.34e-02		8.34e-02
1	827080	807836	132268	136884	8.52e-03	3.13e-02	3.78e-03	4.16e-03	7.93e-04	0.00e+00	2.71e-03	4.55e-04	3.92e-04	5.58e-03	2.21e-03	5.99e-02		1.43e-01
2	807918	810431	121944	141188	8.37e-03	4.63e-02	4.25e-03	1.81e-03	2.84e-04	0.00e+00	3.23e-03	5.99e-04	3.98e-04	4.03e-03	2.30e-03	7.15e-02		2.15e-01
3	820546	809745	141912	139399	8.36e-03	4.61e-02	4.46e-03	2.58e-03	4.32e-04	0.00e+00	3.70e-03	6.88e-04	4.13e-04	3.95e-03	2.43e-03	7.31e-02		2.88e-01
4	810200	804363	130090	140891	8.33e-03	5.23e-02	4.82e-03	2.00e-03	4.38e-04	0.00e+00	3.85e-03	6.27e-04	4.78e-04	4.62e-03	2.36e-03	7.98e-02		3.68e-01
5	811292	810326	141241	147078	8.02e-03	4.49e-02	4.99e-03	2.69e-03	1.10e-03	0.00e+00	3.86e-03	6.05e-04	4.34e-04	4.32e-03	2.45e-03	7.34e-02		4.41e-01
6	813145	809534	140955	141921	8.14e-03	5.11e-02	5.15e-03	2.73e-03	4.28e-04	0.00e+00	3.90e-03	5.06e-04	4.63e-04	4.51e-03	2.43e-03	7.93e-02		5.20e-01
7	807351	813626	139908	143519	8.07e-03	4.67e-02	5.11e-03	3.07e-03	6.25e-04	0.00e+00	3.81e-03	6.77e-04	4.24e-04	3.93e-03	2.49e-03	7.49e-02		5.95e-01
8	810526	814783	146508	140233	8.13e-03	5.16e-02	5.23e-03	2.26e-03	1.02e-03	0.00e+00	3.87e-03	4.89e-04	4.50e-04	4.17e-03	2.46e-03	7.97e-02		6.75e-01
9	813172	814294	144139	139882	8.16e-03	4.77e-02	5.24e-03	1.81e-03	1.06e-03	0.00e+00	3.73e-03	6.12e-04	4.10e-04	3.98e-03	2.49e-03	7.52e-02		7.50e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.3881 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.3881 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.388113  0.000000
];

