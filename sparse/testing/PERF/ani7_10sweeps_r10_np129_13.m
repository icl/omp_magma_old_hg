% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_129 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	812080	809850	141586	141586	5.96e-03	2.77e-02	5.74e-03	1.08e-02	6.39e-04	0.00e+00	2.77e-03	4.23e-04	3.48e-04	7.05e-03	1.75e-03	6.32e-02		6.32e-02
1	787643	819041	136138	138368	5.98e-03	2.73e-02	3.22e-03	4.68e-03	7.22e-04	0.00e+00	3.37e-03	2.43e-04	4.00e-04	3.94e-03	2.08e-03	5.19e-02		1.15e-01
2	819882	800642	161381	129983	6.06e-03	3.75e-02	3.73e-03	2.51e-03	2.62e-04	0.00e+00	3.93e-03	3.47e-04	4.26e-04	3.01e-03	2.33e-03	6.01e-02		1.75e-01
3	799082	803068	129948	149188	5.91e-03	4.70e-02	4.25e-03	1.35e-03	3.06e-04	0.00e+00	4.51e-03	2.81e-04	4.33e-04	2.98e-03	2.23e-03	6.93e-02		2.44e-01
4	807793	818131	151554	147568	5.95e-03	4.59e-02	4.28e-03	1.87e-03	4.81e-04	0.00e+00	4.71e-03	5.17e-04	4.89e-04	3.85e-03	2.29e-03	7.03e-02		3.15e-01
5	811899	811524	143648	133310	6.06e-03	4.55e-02	4.45e-03	4.39e-03	8.47e-04	0.00e+00	4.64e-03	2.75e-04	4.79e-04	3.36e-03	2.27e-03	7.23e-02		3.87e-01
6	810952	811621	140348	140723	6.05e-03	4.85e-02	4.44e-03	1.37e-03	4.02e-04	0.00e+00	4.62e-03	5.59e-04	4.68e-04	3.41e-03	2.28e-03	7.21e-02		4.59e-01
7	811552	811368	142101	141432	6.01e-03	4.72e-02	4.50e-03	2.87e-03	6.05e-04	0.00e+00	4.81e-03	4.92e-04	4.85e-04	4.15e-03	2.31e-03	7.34e-02		5.32e-01
8	812576	809108	142307	142491	6.00e-03	4.90e-02	4.50e-03	1.57e-03	1.05e-03	0.00e+00	4.63e-03	4.17e-04	4.95e-04	3.02e-03	2.31e-03	7.30e-02		6.05e-01
9	813418	812454	142089	145557	5.98e-03	4.70e-02	4.48e-03	1.69e-03	8.99e-04	0.00e+00	4.56e-03	4.40e-04	4.67e-04	2.99e-03	2.30e-03	7.08e-02		6.76e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.1611 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.1611 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.161108  0.000000
];

