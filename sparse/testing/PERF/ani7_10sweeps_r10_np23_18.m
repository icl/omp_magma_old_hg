% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_23 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	809230	805419	141586	141586	3.66e-03	5.31e-02	1.04e-02	7.47e-03	1.02e-03	0.00e+00	1.09e-02	3.06e-04	1.13e-03	7.33e-03	6.34e-03	1.02e-01		1.02e-01
1	818366	805407	138988	142799	3.65e-03	6.49e-02	9.64e-03	3.73e-03	1.25e-03	0.00e+00	1.33e-02	2.54e-04	1.33e-03	5.99e-03	8.29e-03	1.12e-01		2.14e-01
2	788657	806223	130658	143617	3.70e-03	1.03e-01	1.27e-02	2.85e-03	1.02e-03	0.00e+00	1.62e-02	3.04e-04	1.37e-03	5.46e-03	8.65e-03	1.55e-01		3.69e-01
3	811789	794305	161173	143607	3.71e-03	1.42e-01	1.35e-02	3.73e-03	1.46e-03	0.00e+00	1.85e-02	4.06e-04	1.51e-03	5.41e-03	8.26e-03	1.98e-01		5.68e-01
4	809308	794900	138847	156331	3.66e-03	1.39e-01	1.44e-02	4.43e-03	1.76e-03	0.00e+00	2.02e-02	2.96e-04	1.61e-03	5.75e-03	8.23e-03	1.99e-01		7.67e-01
5	808949	806296	142133	156541	3.68e-03	1.39e-01	1.44e-02	3.00e-03	1.89e-03	0.00e+00	2.05e-02	3.20e-04	1.61e-03	5.98e-03	8.30e-03	1.99e-01		9.65e-01
6	806235	813359	143298	145951	3.74e-03	1.43e-01	1.47e-02	2.89e-03	1.72e-03	0.00e+00	2.10e-02	5.31e-04	1.61e-03	5.61e-03	8.35e-03	2.03e-01		1.17e+00
7	809007	811359	146818	139694	3.74e-03	1.48e-01	1.48e-02	3.48e-03	1.71e-03	0.00e+00	2.08e-02	3.05e-04	1.57e-03	5.58e-03	8.37e-03	2.09e-01		1.38e+00
8	810657	811911	144852	142500	3.74e-03	1.50e-01	1.49e-02	3.01e-03	1.93e-03	0.00e+00	2.07e-02	3.58e-04	1.57e-03	5.74e-03	8.38e-03	2.11e-01		1.59e+00
9	816918	813358	144008	142754	3.77e-03	1.51e-01	1.50e-02	3.15e-03	2.06e-03	0.00e+00	2.08e-02	3.71e-04	1.58e-03	5.50e-03	8.45e-03	2.12e-01		1.80e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 2.2045 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 2.2045 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  2.204517  0.000000
];

