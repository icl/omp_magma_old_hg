% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_55 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	822111	802176	141586	141586	3.26e-03	2.80e-02	4.96e-03	6.72e-03	1.01e-03	0.00e+00	4.48e-03	2.49e-04	5.16e-04	5.93e-03	2.60e-03	5.77e-02		5.77e-02
1	803118	802163	126107	146042	3.21e-03	3.23e-02	3.87e-03	2.08e-03	5.62e-04	0.00e+00	5.37e-03	2.40e-04	5.23e-04	3.97e-03	3.47e-03	5.56e-02		1.13e-01
2	809415	807146	145906	146861	3.26e-03	5.32e-02	4.95e-03	3.31e-03	4.83e-04	0.00e+00	6.54e-03	3.48e-04	5.94e-04	3.70e-03	3.69e-03	8.01e-02		1.93e-01
3	810131	816171	140415	142684	3.27e-03	7.03e-02	5.72e-03	2.04e-03	6.62e-04	0.00e+00	7.51e-03	2.56e-04	6.63e-04	3.43e-03	3.55e-03	9.74e-02		2.91e-01
4	812464	815772	140505	134465	3.32e-03	7.03e-02	5.86e-03	1.54e-03	7.34e-04	0.00e+00	8.11e-03	3.65e-04	7.09e-04	3.39e-03	3.44e-03	9.77e-02		3.88e-01
5	804697	808199	138977	135669	3.33e-03	6.53e-02	5.89e-03	1.97e-03	8.29e-04	0.00e+00	8.33e-03	3.58e-04	7.53e-04	4.42e-03	3.40e-03	9.46e-02		4.83e-01
6	809725	814143	147550	144048	3.28e-03	6.66e-02	5.84e-03	2.18e-03	7.21e-04	0.00e+00	8.00e-03	3.49e-04	7.06e-04	3.34e-03	3.42e-03	9.44e-02		5.77e-01
7	812064	814324	143328	138910	3.30e-03	6.69e-02	5.97e-03	2.14e-03	8.09e-04	0.00e+00	8.45e-03	2.65e-04	7.36e-04	3.69e-03	3.42e-03	9.57e-02		6.73e-01
8	812175	809804	141795	139535	3.34e-03	6.87e-02	5.97e-03	1.69e-03	1.02e-03	0.00e+00	8.24e-03	2.80e-04	6.99e-04	3.52e-03	3.41e-03	9.68e-02		7.70e-01
9	819666	814274	142490	144861	3.30e-03	7.05e-02	5.96e-03	1.89e-03	1.25e-03	0.00e+00	8.26e-03	3.36e-04	7.28e-04	3.37e-03	3.47e-03	9.91e-02		8.69e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.2373 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.2373 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.237338  0.000000
];

