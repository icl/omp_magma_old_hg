% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_179 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	821483	818048	141586	141586	8.44e-03	3.67e-02	7.53e-03	1.54e-02	1.28e-03	0.00e+00	2.24e-03	5.51e-04	3.44e-04	1.08e-02	1.91e-03	8.51e-02		8.51e-02
1	819757	809796	126735	130170	8.56e-03	3.24e-02	3.60e-03	4.55e-03	2.41e-03	0.00e+00	2.72e-03	4.68e-04	3.86e-04	6.19e-03	2.08e-03	6.34e-02		1.49e-01
2	791829	807095	129267	139228	8.42e-03	4.54e-02	4.00e-03	5.41e-03	2.52e-04	0.00e+00	3.19e-03	5.04e-04	3.87e-04	3.89e-03	2.15e-03	7.35e-02		2.22e-01
3	798660	810927	158001	142735	8.42e-03	4.41e-02	4.31e-03	2.76e-03	3.41e-04	0.00e+00	3.28e-03	4.68e-04	4.03e-04	3.97e-03	2.32e-03	7.04e-02		2.92e-01
4	804422	810963	151976	139709	8.42e-03	4.79e-02	4.55e-03	2.05e-03	3.73e-04	0.00e+00	3.61e-03	4.93e-04	4.44e-04	4.10e-03	2.31e-03	7.42e-02		3.67e-01
5	811667	811493	147019	140478	8.42e-03	4.51e-02	4.64e-03	2.65e-03	3.72e-04	0.00e+00	3.65e-03	3.17e-04	4.41e-04	4.49e-03	2.31e-03	7.24e-02		4.39e-01
6	812906	811399	140580	140754	8.45e-03	5.08e-02	4.72e-03	2.11e-03	3.97e-04	0.00e+00	3.63e-03	5.56e-04	4.45e-04	4.41e-03	2.24e-03	7.78e-02		5.17e-01
7	812844	810880	140147	141654	8.44e-03	4.69e-02	4.80e-03	2.24e-03	3.44e-04	0.00e+00	3.59e-03	9.02e-04	4.12e-04	3.89e-03	2.28e-03	7.38e-02		5.91e-01
8	811948	811686	141015	142979	8.42e-03	5.11e-02	4.75e-03	1.52e-03	3.78e-04	0.00e+00	3.45e-03	6.77e-04	3.97e-04	4.18e-03	2.32e-03	7.72e-02		6.68e-01
9	814364	811052	142717	142979	8.44e-03	4.67e-02	4.74e-03	2.40e-03	1.16e-03	0.00e+00	3.53e-03	4.93e-04	4.40e-04	3.97e-03	2.31e-03	7.42e-02		7.42e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.3835 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.3835 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.383514  0.000000
];

