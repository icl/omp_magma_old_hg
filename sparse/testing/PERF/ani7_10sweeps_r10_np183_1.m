% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_183 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	834678	804486	141586	141586	8.52e-03	3.68e-02	7.48e-03	1.51e-02	2.65e-03	0.00e+00	2.14e-03	5.82e-04	3.40e-04	1.11e-02	1.86e-03	8.65e-02		8.65e-02
1	798657	794710	113540	143732	8.53e-03	3.21e-02	3.46e-03	3.00e-03	2.55e-03	0.00e+00	2.62e-03	4.22e-04	3.73e-04	6.46e-03	2.03e-03	6.16e-02		1.48e-01
2	794796	802200	150367	154314	8.33e-03	4.49e-02	3.87e-03	2.39e-03	2.91e-04	0.00e+00	2.84e-03	5.12e-04	4.08e-04	3.86e-03	2.23e-03	6.96e-02		2.18e-01
3	799698	811569	155034	147630	8.43e-03	4.35e-02	4.22e-03	2.14e-03	3.10e-04	0.00e+00	3.32e-03	4.04e-04	4.30e-04	3.96e-03	2.19e-03	6.89e-02		2.87e-01
4	810798	810949	150938	139067	8.52e-03	4.63e-02	4.42e-03	2.98e-03	3.51e-04	0.00e+00	3.50e-03	6.76e-04	4.06e-04	4.06e-03	2.22e-03	7.34e-02		3.60e-01
5	807872	811018	140643	140492	8.52e-03	4.54e-02	4.63e-03	2.84e-03	4.11e-04	0.00e+00	3.45e-03	4.51e-04	4.34e-04	4.44e-03	2.21e-03	7.28e-02		4.33e-01
6	811977	809303	144375	141229	8.53e-03	4.90e-02	4.57e-03	3.12e-03	4.21e-04	0.00e+00	3.68e-03	5.44e-04	4.52e-04	4.30e-03	2.25e-03	7.69e-02		5.10e-01
7	811057	816131	141076	143750	8.51e-03	4.55e-02	4.62e-03	1.93e-03	3.24e-04	0.00e+00	3.45e-03	5.44e-04	4.46e-04	3.87e-03	2.30e-03	7.15e-02		5.81e-01
8	807368	810798	142802	137728	8.57e-03	5.07e-02	4.74e-03	1.77e-03	3.79e-04	0.00e+00	3.51e-03	6.09e-04	4.43e-04	4.06e-03	2.31e-03	7.71e-02		6.58e-01
9	814915	814194	147297	143867	8.57e-03	4.65e-02	4.68e-03	1.95e-03	1.17e-03	0.00e+00	3.47e-03	5.84e-04	4.24e-04	3.95e-03	2.26e-03	7.36e-02		7.32e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.3798 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.3798 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.379775  0.000000
];

