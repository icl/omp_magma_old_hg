% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_13 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	820061	800291	141586	141586	3.98e-03	8.41e-02	1.76e-02	8.53e-03	1.42e-03	0.00e+00	1.92e-02	2.95e-04	1.95e-03	9.58e-03	1.11e-02	1.58e-01		1.58e-01
1	790044	794557	128157	147927	3.94e-03	1.06e-01	1.70e-02	4.84e-03	1.94e-03	0.00e+00	2.37e-02	2.28e-04	2.28e-03	8.34e-03	1.42e-02	1.82e-01		3.40e-01
2	804824	818137	158980	154467	4.01e-03	1.74e-01	2.13e-02	4.23e-03	1.76e-03	0.00e+00	2.80e-02	2.94e-04	2.29e-03	8.00e-03	1.58e-02	2.60e-01		5.99e-01
3	812397	792175	145006	131693	4.11e-03	2.56e-01	2.50e-02	5.07e-03	2.08e-03	0.00e+00	3.28e-02	3.59e-04	2.59e-03	7.75e-03	1.44e-02	3.50e-01		9.50e-01
4	810443	812798	138239	158461	3.99e-03	2.29e-01	2.50e-02	5.14e-03	3.29e-03	0.00e+00	3.64e-02	3.62e-04	2.77e-03	8.23e-03	1.44e-02	3.29e-01		1.28e+00
5	811788	810414	140998	138643	4.09e-03	2.33e-01	2.57e-02	5.14e-03	3.10e-03	0.00e+00	3.62e-02	4.20e-04	2.66e-03	8.41e-03	1.45e-02	3.33e-01		1.61e+00
6	812771	810763	140459	141833	4.08e-03	2.37e-01	2.57e-02	4.61e-03	3.20e-03	0.00e+00	3.70e-02	3.05e-04	2.71e-03	7.92e-03	1.45e-02	3.37e-01		1.95e+00
7	809890	815217	140282	142290	4.07e-03	2.37e-01	2.59e-02	4.67e-03	2.77e-03	0.00e+00	3.59e-02	4.68e-04	2.60e-03	7.92e-03	1.46e-02	3.36e-01		2.28e+00
8	811503	813608	143969	138642	4.06e-03	2.39e-01	2.61e-02	4.46e-03	2.79e-03	0.00e+00	3.53e-02	3.73e-04	2.58e-03	8.17e-03	1.46e-02	3.37e-01		2.62e+00
9	817822	813701	143162	141057	4.11e-03	2.38e-01	2.61e-02	4.42e-03	3.05e-03	0.00e+00	3.55e-02	4.20e-04	2.54e-03	7.88e-03	1.47e-02	3.36e-01		2.96e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 3.3960 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 3.3960 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  3.396021  0.000000
];

