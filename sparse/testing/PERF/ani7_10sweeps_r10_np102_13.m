% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_102 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	808251	808814	141586	141586	5.62e-03	2.88e-02	5.96e-03	9.99e-03	1.01e-03	0.00e+00	2.64e-03	4.33e-04	4.14e-04	6.40e-03	2.16e-03	6.34e-02		6.34e-02
1	811385	807448	139967	139404	5.62e-03	2.78e-02	3.81e-03	1.88e-03	9.50e-04	0.00e+00	3.05e-03	2.87e-04	5.02e-04	4.26e-03	2.45e-03	5.07e-02		1.14e-01
2	798434	811784	137639	141576	5.57e-03	4.53e-02	4.42e-03	1.72e-03	6.34e-04	0.00e+00	3.69e-03	4.11e-04	4.86e-04	3.11e-03	2.56e-03	6.79e-02		1.82e-01
3	806657	804664	151396	138046	5.58e-03	4.66e-02	4.69e-03	2.08e-03	3.84e-04	0.00e+00	4.26e-03	3.77e-04	4.71e-04	3.11e-03	2.58e-03	7.02e-02		2.52e-01
4	811905	804622	143979	145972	5.56e-03	4.80e-02	4.95e-03	2.71e-03	5.15e-04	0.00e+00	4.49e-03	5.49e-04	4.90e-04	3.17e-03	2.61e-03	7.30e-02		3.25e-01
5	809647	808519	139536	146819	5.56e-03	4.44e-02	5.22e-03	1.90e-03	5.32e-04	0.00e+00	4.50e-03	5.71e-04	4.81e-04	3.58e-03	2.63e-03	6.94e-02		3.95e-01
6	810553	812144	142600	143728	5.58e-03	4.79e-02	5.28e-03	2.38e-03	4.51e-04	0.00e+00	4.45e-03	5.27e-04	5.18e-04	3.16e-03	2.69e-03	7.29e-02		4.68e-01
7	811843	811272	142500	140909	5.61e-03	4.61e-02	5.30e-03	1.82e-03	4.53e-04	0.00e+00	4.43e-03	4.78e-04	4.86e-04	3.40e-03	2.67e-03	7.07e-02		5.38e-01
8	816391	812794	142016	142587	5.60e-03	4.92e-02	5.34e-03	2.18e-03	1.09e-03	0.00e+00	4.48e-03	4.85e-04	4.80e-04	3.17e-03	2.69e-03	7.47e-02		6.13e-01
9	816647	813111	138274	141871	5.64e-03	4.73e-02	5.41e-03	1.95e-03	9.46e-04	0.00e+00	4.47e-03	4.87e-04	4.86e-04	3.11e-03	2.70e-03	7.25e-02		6.86e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.1644 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.1644 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.164401  0.000000
];

