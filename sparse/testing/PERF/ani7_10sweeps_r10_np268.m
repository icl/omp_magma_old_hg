% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_268 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	828382	811670	141586	141586	1.20e-02	7.31e-02	1.83e-02	2.70e-02	2.78e-03	0.00e+00	2.71e-03	5.22e-04	3.29e-04	2.66e-02	1.59e-03	1.65e-01		1.65e-01
1	792290	818191	119836	136548	1.21e-02	4.90e-02	3.15e-03	3.30e-03	1.78e-03	0.00e+00	3.15e-03	4.74e-04	3.84e-04	1.39e-02	2.01e-03	8.93e-02		2.54e-01
2	805137	798922	156734	130833	1.22e-02	7.10e-02	3.73e-03	2.39e-03	1.27e-03	0.00e+00	3.49e-03	4.50e-04	4.59e-04	4.37e-03	2.06e-03	1.02e-01		3.56e-01
3	799725	797871	144693	150908	1.20e-02	6.06e-02	4.10e-03	4.23e-03	5.10e-04	0.00e+00	3.72e-03	6.75e-04	4.81e-04	4.38e-03	2.06e-03	9.28e-02		4.49e-01
4	811025	813074	150911	152765	1.20e-02	7.31e-02	4.08e-03	3.30e-03	6.94e-04	0.00e+00	3.91e-03	6.43e-04	4.39e-04	4.42e-03	2.07e-03	1.05e-01		5.53e-01
5	811459	810318	140416	138367	1.22e-02	6.61e-02	4.16e-03	2.92e-03	5.86e-04	0.00e+00	4.10e-03	6.70e-04	4.36e-04	5.75e-03	2.08e-03	9.90e-02		6.52e-01
6	808963	811005	140788	141929	1.21e-02	7.82e-02	4.22e-03	2.58e-03	8.51e-04	0.00e+00	4.12e-03	6.16e-04	4.39e-04	4.84e-03	2.08e-03	1.10e-01		7.62e-01
7	811646	811682	144090	142048	1.21e-02	6.41e-02	4.20e-03	2.18e-03	5.66e-04	0.00e+00	4.08e-03	5.55e-04	4.79e-04	5.09e-03	2.09e-03	9.55e-02		8.58e-01
8	813181	811647	142213	142177	1.21e-02	7.85e-02	4.25e-03	2.27e-03	1.94e-03	0.00e+00	4.19e-03	5.44e-04	4.50e-04	4.80e-03	2.09e-03	1.11e-01		9.69e-01
9	809779	812124	141484	143018	1.21e-02	6.52e-02	4.25e-03	2.00e-03	2.06e-03	0.00e+00	4.02e-03	7.45e-04	4.31e-04	4.37e-03	2.13e-03	9.73e-02		1.07e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.8212 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.8212 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.821173  0.000000
];

