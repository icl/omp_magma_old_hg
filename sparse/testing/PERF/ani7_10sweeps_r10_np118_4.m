% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_118 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	819426	816866	141586	141586	5.81e-03	2.82e-02	5.82e-03	1.05e-02	8.82e-04	0.00e+00	2.82e-03	3.98e-04	3.65e-04	7.16e-03	1.94e-03	6.39e-02		6.39e-02
1	796513	811250	128792	131352	5.89e-03	2.88e-02	3.38e-03	2.93e-03	1.94e-03	0.00e+00	2.68e-03	2.74e-04	4.41e-04	4.44e-03	2.25e-03	5.30e-02		1.17e-01
2	809129	809106	152511	137774	5.84e-03	3.95e-02	3.94e-03	2.12e-03	2.94e-04	0.00e+00	3.44e-03	3.52e-04	4.39e-04	3.26e-03	2.46e-03	6.16e-02		1.78e-01
3	817871	806927	140701	140724	5.83e-03	4.66e-02	4.41e-03	1.83e-03	3.69e-04	0.00e+00	4.43e-03	3.15e-04	5.00e-04	3.16e-03	2.42e-03	6.99e-02		2.48e-01
4	810731	812623	132765	143709	5.82e-03	4.91e-02	4.77e-03	2.03e-03	4.94e-04	0.00e+00	4.23e-03	4.34e-04	5.43e-04	3.17e-03	2.39e-03	7.30e-02		3.21e-01
5	805912	808496	140710	138818	5.85e-03	4.58e-02	4.70e-03	2.09e-03	4.33e-04	0.00e+00	3.88e-03	3.52e-04	5.23e-04	3.42e-03	2.43e-03	6.95e-02		3.91e-01
6	809889	810078	146335	143751	5.80e-03	4.79e-02	4.64e-03	1.93e-03	4.69e-04	0.00e+00	3.83e-03	3.62e-04	4.85e-04	3.38e-03	2.47e-03	7.13e-02		4.62e-01
7	813335	818850	143164	142975	5.84e-03	4.65e-02	4.73e-03	1.95e-03	4.85e-04	0.00e+00	4.37e-03	3.95e-04	4.91e-04	3.10e-03	2.50e-03	7.04e-02		5.33e-01
8	812524	810376	140524	135009	5.89e-03	5.01e-02	6.40e-03	2.31e-03	4.52e-04	0.00e+00	3.99e-03	4.17e-04	5.18e-04	3.30e-03	2.87e-03	7.63e-02		6.09e-01
9	808664	814077	142141	144289	8.34e-03	4.83e-02	6.28e-03	1.60e-03	1.06e-03	0.00e+00	3.98e-03	4.60e-04	5.25e-04	3.12e-03	2.88e-03	7.65e-02		6.85e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.1623 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.1623 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.162337  0.000000
];

