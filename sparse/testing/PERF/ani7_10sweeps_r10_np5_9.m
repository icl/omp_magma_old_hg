% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_5 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	820461	808001	141586	141586	4.84e-03	1.99e-01	4.43e-02	1.28e-02	3.43e-03	0.00e+00	4.59e-02	2.43e-04	4.76e-03	1.73e-02	2.81e-02	3.61e-01		3.61e-01
1	800758	801187	127757	140217	5.10e-03	2.42e-01	4.39e-02	1.02e-02	4.39e-03	0.00e+00	5.55e-02	2.13e-04	5.16e-03	1.69e-02	3.59e-02	4.19e-01		7.80e-01
2	802989	800497	148266	147837	5.22e-03	4.20e-01	5.43e-02	9.25e-03	4.39e-03	0.00e+00	6.62e-02	3.21e-04	5.48e-03	1.68e-02	3.91e-02	6.21e-01		1.40e+00
3	810578	822422	146841	149333	5.14e-03	5.90e-01	6.08e-02	9.53e-03	5.08e-03	0.00e+00	7.81e-02	2.64e-04	6.09e-03	1.69e-02	3.94e-02	8.11e-01		2.21e+00
4	814444	813191	140058	128214	5.40e-03	6.13e-01	6.63e-02	1.06e-02	7.42e-03	0.00e+00	8.36e-02	3.50e-04	6.79e-03	1.66e-02	3.68e-02	8.47e-01		3.06e+00
5	817471	804618	136997	138250	5.22e-03	5.70e-01	6.56e-02	1.02e-02	5.58e-03	0.00e+00	7.38e-02	2.07e-04	6.03e-03	1.70e-02	3.72e-02	7.91e-01		3.85e+00
6	809437	807540	134776	147629	5.18e-03	5.91e-01	6.58e-02	9.96e-03	6.49e-03	0.00e+00	7.87e-02	3.48e-04	6.39e-03	1.67e-02	3.68e-02	8.17e-01		4.67e+00
7	811109	814642	143616	145513	5.22e-03	5.85e-01	6.57e-02	1.04e-02	7.19e-03	0.00e+00	8.31e-02	3.96e-04	6.54e-03	1.70e-02	3.74e-02	8.17e-01		5.49e+00
8	809254	811865	142750	139217	5.26e-03	5.99e-01	6.67e-02	1.05e-02	6.75e-03	0.00e+00	7.95e-02	4.05e-04	6.42e-03	1.69e-02	3.71e-02	8.29e-01		6.31e+00
9	810933	811934	145411	142800	5.25e-03	5.93e-01	6.64e-02	9.19e-03	6.14e-03	0.00e+00	7.47e-02	2.95e-04	6.08e-03	1.67e-02	3.73e-02	8.15e-01		7.13e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 7.6886 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 7.6886 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  7.688564  0.000000
];

