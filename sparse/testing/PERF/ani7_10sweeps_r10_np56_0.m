% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_56 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	816981	803042	141586	141586	3.23e-03	2.79e-02	4.91e-03	6.61e-03	7.65e-04	0.00e+00	4.31e-03	2.84e-04	4.79e-04	5.98e-03	2.56e-03	5.71e-02		5.71e-02
1	801617	806963	131237	145176	3.22e-03	3.19e-02	3.77e-03	3.03e-03	1.31e-03	0.00e+00	5.08e-03	1.75e-04	5.67e-04	4.05e-03	3.39e-03	5.64e-02		1.14e-01
2	792043	822463	147407	142061	3.26e-03	5.25e-02	4.88e-03	1.60e-03	4.32e-04	0.00e+00	6.38e-03	3.23e-04	5.74e-04	3.40e-03	3.58e-03	7.69e-02		1.90e-01
3	814347	813947	157787	127367	3.33e-03	6.71e-02	5.76e-03	1.62e-03	6.39e-04	0.00e+00	7.27e-03	2.81e-04	7.02e-04	3.38e-03	3.60e-03	9.37e-02		2.84e-01
4	813337	812479	136289	136689	3.30e-03	6.99e-02	5.90e-03	1.45e-03	6.77e-04	0.00e+00	7.92e-03	3.24e-04	6.79e-04	3.56e-03	3.38e-03	9.71e-02		3.81e-01
5	810763	808640	138104	138962	3.29e-03	6.86e-02	8.82e-03	1.69e-03	8.84e-04	0.00e+00	1.05e-02	2.79e-04	8.48e-04	3.70e-03	4.55e-03	1.03e-01		4.84e-01
6	811150	810820	141484	143607	5.35e-03	7.61e-02	8.49e-03	1.76e-03	8.71e-04	0.00e+00	1.07e-02	3.78e-04	8.42e-04	3.64e-03	4.54e-03	1.13e-01		5.97e-01
7	810339	811633	141903	142233	5.41e-03	7.55e-02	8.75e-03	1.83e-03	8.18e-04	0.00e+00	1.01e-02	3.26e-04	8.36e-04	3.42e-03	4.45e-03	1.11e-01		7.08e-01
8	811528	812090	143520	142226	7.83e-03	6.79e-02	5.86e-03	2.22e-03	7.04e-04	0.00e+00	7.98e-03	3.79e-04	7.27e-04	3.52e-03	3.45e-03	1.01e-01		8.09e-01
9	813538	813593	143137	142575	3.31e-03	6.70e-02	5.88e-03	1.80e-03	1.20e-03	0.00e+00	8.10e-03	3.35e-04	7.36e-04	3.36e-03	3.38e-03	9.51e-02		9.04e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.2862 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.2862 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.286241  0.000000
];

