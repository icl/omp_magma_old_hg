% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_209 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	818817	801327	141586	141586	9.69e-03	3.96e-02	8.30e-03	1.85e-02	1.03e-03	0.00e+00	2.61e-03	4.89e-04	4.31e-04	1.22e-02	1.92e-03	9.47e-02		9.47e-02
1	805131	818120	129401	146891	9.54e-03	3.61e-02	3.68e-03	2.16e-03	9.65e-04	0.00e+00	3.33e-03	5.30e-04	3.57e-04	6.12e-03	2.18e-03	6.50e-02		1.60e-01
2	814996	806522	143893	130904	9.77e-03	5.41e-02	3.98e-03	3.34e-03	3.55e-04	0.00e+00	3.60e-03	8.11e-04	4.09e-04	4.34e-03	2.18e-03	8.29e-02		2.43e-01
3	813962	806183	134834	143308	1.17e-02	5.42e-02	4.59e-03	3.19e-03	6.46e-04	0.00e+00	3.98e-03	5.89e-04	4.43e-04	4.31e-03	2.18e-03	8.59e-02		3.28e-01
4	818187	819818	136674	144453	9.61e-03	5.93e-02	4.26e-03	2.08e-03	4.47e-04	0.00e+00	4.04e-03	7.77e-04	4.60e-04	5.38e-03	2.20e-03	8.85e-02		4.17e-01
5	811846	814635	133254	131623	9.76e-03	5.36e-02	4.54e-03	5.48e-03	1.32e-03	0.00e+00	4.28e-03	5.40e-04	4.88e-04	4.74e-03	2.10e-03	8.68e-02		5.04e-01
6	810518	809785	140401	137612	9.69e-03	5.68e-02	4.44e-03	2.87e-03	4.93e-04	0.00e+00	4.21e-03	7.44e-04	4.50e-04	4.73e-03	2.12e-03	8.66e-02		5.90e-01
7	810581	814372	142535	143268	9.71e-03	5.26e-02	4.39e-03	1.86e-03	4.77e-04	0.00e+00	4.34e-03	5.95e-04	4.77e-04	4.46e-03	2.16e-03	8.11e-02		6.71e-01
8	813716	813496	143278	139487	9.75e-03	5.65e-02	4.46e-03	2.54e-03	1.43e-03	0.00e+00	4.16e-03	6.18e-04	4.45e-04	4.33e-03	2.16e-03	8.63e-02		7.58e-01
9	809212	814023	140949	141169	9.72e-03	5.31e-02	4.53e-03	2.03e-03	1.35e-03	0.00e+00	4.26e-03	5.51e-04	4.42e-04	4.25e-03	2.14e-03	8.24e-02		8.40e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.5838 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.5838 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.583777  0.000000
];

