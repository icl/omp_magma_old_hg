% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_244 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	823454	815290	141586	141586	1.08e-02	4.37e-02	9.18e-03	1.89e-02	1.04e-03	0.00e+00	2.23e-03	4.77e-04	3.74e-04	1.30e-02	1.74e-03	1.01e-01		1.01e-01
1	800171	796550	124764	132928	1.09e-02	3.56e-02	3.34e-03	7.01e-03	1.05e-03	0.00e+00	2.79e-03	5.08e-04	3.64e-04	5.96e-03	1.86e-03	6.94e-02		1.71e-01
2	811220	814767	148853	152474	1.06e-02	5.02e-02	3.58e-03	3.34e-03	2.60e-04	0.00e+00	3.06e-03	6.37e-04	3.62e-04	4.30e-03	1.96e-03	7.84e-02		2.49e-01
3	817265	806929	138610	135063	1.08e-02	5.16e-02	4.47e-03	3.13e-03	4.47e-04	0.00e+00	3.45e-03	6.78e-04	4.35e-04	4.16e-03	2.08e-03	8.12e-02		3.30e-01
4	814428	809256	133371	143707	1.08e-02	5.60e-02	4.16e-03	3.79e-03	4.08e-04	0.00e+00	3.67e-03	6.69e-04	4.21e-04	5.51e-03	2.10e-03	8.75e-02		4.18e-01
5	814206	806935	137013	142185	1.08e-02	4.88e-02	4.33e-03	1.39e-03	1.53e-03	0.00e+00	3.65e-03	7.72e-04	3.90e-04	5.19e-03	2.11e-03	7.90e-02		4.97e-01
6	808139	811910	138041	145312	1.08e-02	5.43e-02	4.31e-03	1.69e-03	3.38e-04	0.00e+00	3.62e-03	8.06e-04	3.90e-04	4.80e-03	2.11e-03	8.32e-02		5.80e-01
7	809609	810985	144914	141143	1.08e-02	4.90e-02	4.33e-03	2.89e-03	4.07e-04	0.00e+00	3.60e-03	7.54e-04	3.87e-04	4.44e-03	2.12e-03	7.88e-02		6.59e-01
8	817035	812729	144250	142874	1.08e-02	5.42e-02	4.35e-03	1.63e-03	1.57e-03	0.00e+00	3.62e-03	7.41e-04	3.98e-04	4.69e-03	2.15e-03	8.41e-02		7.43e-01
9	814640	816331	137630	141936	1.09e-02	5.02e-02	4.46e-03	3.92e-03	1.54e-03	0.00e+00	3.65e-03	7.18e-04	3.93e-04	4.19e-03	2.15e-03	8.21e-02		8.25e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.5843 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.5843 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.584333  0.000000
];

