% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_185 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	815919	810798	141586	141586	8.56e-03	3.71e-02	7.60e-03	1.58e-02	1.19e-03	0.00e+00	2.12e-03	5.18e-04	3.41e-04	1.10e-02	1.82e-03	8.60e-02		8.60e-02
1	802127	817371	132299	137420	8.61e-03	3.20e-02	3.41e-03	5.25e-03	2.62e-03	0.00e+00	2.49e-03	5.46e-04	3.73e-04	6.20e-03	2.06e-03	6.35e-02		1.50e-01
2	800475	813272	146897	131653	8.63e-03	4.50e-02	3.90e-03	3.15e-03	3.67e-04	0.00e+00	2.95e-03	5.70e-04	4.22e-04	3.95e-03	2.21e-03	7.12e-02		2.21e-01
3	819743	800346	149355	136558	8.61e-03	4.78e-02	4.31e-03	2.88e-03	3.40e-04	0.00e+00	3.17e-03	4.44e-04	4.70e-04	3.92e-03	2.18e-03	7.41e-02		2.95e-01
4	805913	814681	130893	150290	8.49e-03	5.12e-02	4.42e-03	2.47e-03	5.75e-04	0.00e+00	3.53e-03	5.77e-04	4.71e-04	4.31e-03	2.22e-03	7.83e-02		3.73e-01
5	807380	815330	145528	136760	8.57e-03	4.76e-02	4.56e-03	3.57e-03	4.37e-04	0.00e+00	3.58e-03	4.21e-04	4.85e-04	4.47e-03	2.28e-03	7.59e-02		4.49e-01
6	808242	813553	144867	136917	8.53e-03	5.15e-02	4.61e-03	2.02e-03	4.90e-04	0.00e+00	3.50e-03	6.31e-04	4.43e-04	4.33e-03	2.22e-03	7.82e-02		5.27e-01
7	809388	809211	144811	139500	8.61e-03	4.87e-02	4.62e-03	1.95e-03	4.41e-04	0.00e+00	3.54e-03	6.47e-04	4.41e-04	3.90e-03	2.29e-03	7.51e-02		6.02e-01
8	813829	812407	144471	144648	8.67e-03	5.18e-02	4.60e-03	2.12e-03	4.61e-04	0.00e+00	3.41e-03	6.10e-04	4.33e-04	4.11e-03	2.29e-03	7.85e-02		6.81e-01
9	813164	814405	140836	142258	8.71e-03	4.88e-02	4.69e-03	2.48e-03	1.14e-03	0.00e+00	3.51e-03	5.93e-04	4.64e-04	3.94e-03	2.27e-03	7.66e-02		7.58e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.3907 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.3907 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.390651  0.000000
];

