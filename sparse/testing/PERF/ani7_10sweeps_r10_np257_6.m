% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_257 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	812116	801055	141586	141586	1.14e-02	4.49e-02	9.37e-03	2.11e-02	2.92e-03	0.00e+00	2.17e-03	5.06e-04	3.37e-04	1.35e-02	1.56e-03	1.08e-01		1.08e-01
1	804080	812477	136102	147163	1.13e-02	3.49e-02	3.12e-03	6.20e-03	2.52e-03	0.00e+00	2.66e-03	4.07e-04	4.22e-04	7.06e-03	1.89e-03	7.05e-02		1.78e-01
2	820640	810698	144944	136547	1.15e-02	5.21e-02	3.65e-03	1.51e-03	1.40e-03	0.00e+00	3.00e-03	5.15e-04	3.88e-04	4.21e-03	2.07e-03	8.03e-02		2.59e-01
3	805149	803313	129190	139132	1.14e-02	5.14e-02	4.73e-03	3.67e-03	4.04e-04	0.00e+00	3.38e-03	5.64e-04	4.36e-04	4.21e-03	1.98e-03	8.21e-02		3.41e-01
4	805385	801649	145487	147323	1.13e-02	5.50e-02	4.10e-03	2.11e-03	8.16e-04	0.00e+00	3.50e-03	6.44e-04	4.63e-04	4.23e-03	2.05e-03	8.42e-02		4.25e-01
5	812952	810913	146056	149792	1.12e-02	4.92e-02	4.11e-03	3.32e-03	4.28e-04	0.00e+00	3.58e-03	6.09e-04	4.56e-04	4.90e-03	2.04e-03	7.99e-02		5.05e-01
6	811949	810472	139295	141334	1.13e-02	5.61e-02	4.25e-03	2.40e-03	4.46e-04	0.00e+00	3.55e-03	6.57e-04	4.38e-04	4.59e-03	2.06e-03	8.58e-02		5.91e-01
7	811583	811761	141104	142581	1.14e-02	5.03e-02	4.37e-03	2.89e-03	4.14e-04	0.00e+00	3.52e-03	7.36e-04	4.30e-04	4.22e-03	2.08e-03	8.04e-02		6.71e-01
8	811684	811989	142276	142098	1.14e-02	5.66e-02	4.35e-03	3.68e-03	4.68e-04	0.00e+00	3.48e-03	8.93e-04	4.71e-04	4.65e-03	3.25e-03	8.92e-02		7.60e-01
9	809869	814494	142981	142676	1.13e-02	5.10e-02	4.38e-03	1.60e-03	1.91e-03	0.00e+00	3.57e-03	7.57e-04	4.70e-04	4.18e-03	2.10e-03	8.13e-02		8.42e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.5877 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.5877 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.587698  0.000000
];

