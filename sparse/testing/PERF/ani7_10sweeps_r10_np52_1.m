% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_52 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	818178	803359	141586	141586	3.24e-03	2.93e-02	5.14e-03	6.77e-03	6.00e-04	0.00e+00	5.48e-03	2.51e-04	5.40e-04	5.80e-03	2.76e-03	5.99e-02		5.99e-02
1	806438	811201	130040	144859	3.22e-03	3.35e-02	4.06e-03	2.67e-03	8.30e-04	0.00e+00	5.71e-03	2.23e-04	5.61e-04	4.21e-03	3.62e-03	5.86e-02		1.19e-01
2	825786	811398	142586	137823	3.29e-03	5.45e-02	5.25e-03	2.46e-03	4.96e-04	0.00e+00	6.87e-03	1.71e-04	6.22e-04	3.55e-03	4.01e-03	8.12e-02		2.00e-01
3	801765	806119	124044	138432	3.30e-03	7.76e-02	6.22e-03	1.52e-03	6.88e-04	0.00e+00	8.05e-03	4.05e-04	7.61e-04	3.47e-03	3.62e-03	1.06e-01		3.05e-01
4	803157	801909	148871	144517	3.28e-03	6.92e-02	6.02e-03	1.97e-03	9.20e-04	0.00e+00	8.65e-03	2.72e-04	7.46e-04	3.73e-03	3.51e-03	9.83e-02		4.04e-01
5	811602	805651	148284	149532	3.27e-03	6.58e-02	6.03e-03	1.86e-03	1.07e-03	0.00e+00	8.70e-03	3.26e-04	7.64e-04	3.81e-03	3.60e-03	9.52e-02		4.99e-01
6	813107	810449	140645	146596	3.27e-03	6.84e-02	6.15e-03	1.74e-03	8.22e-04	0.00e+00	8.72e-03	2.95e-04	7.18e-04	3.67e-03	3.61e-03	9.74e-02		5.96e-01
7	809142	811312	139946	142604	3.29e-03	6.87e-02	6.29e-03	1.91e-03	1.05e-03	0.00e+00	8.89e-03	3.58e-04	7.78e-04	3.44e-03	3.63e-03	9.83e-02		6.95e-01
8	812619	814886	144717	142547	3.27e-03	6.94e-02	6.26e-03	1.80e-03	1.03e-03	0.00e+00	8.53e-03	4.81e-04	7.55e-04	3.61e-03	3.64e-03	9.88e-02		7.93e-01
9	810936	814826	142046	139779	3.34e-03	7.02e-02	6.36e-03	1.79e-03	1.24e-03	0.00e+00	8.64e-03	4.10e-04	7.50e-04	3.42e-03	3.61e-03	9.97e-02		8.93e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.2689 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.2689 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.268853  0.000000
];

