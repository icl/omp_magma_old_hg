% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_197 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	820860	809737	141586	141586	8.83e-03	3.67e-02	7.68e-03	1.63e-02	2.03e-03	0.00e+00	2.68e-03	4.68e-04	3.32e-04	9.96e-03	1.73e-03	8.67e-02		8.67e-02
1	799305	825275	127358	138481	8.86e-03	3.39e-02	3.23e-03	4.25e-03	2.07e-03	0.00e+00	3.47e-03	4.41e-04	3.57e-04	5.98e-03	1.99e-03	6.45e-02		1.51e-01
2	796437	802851	149719	123749	9.03e-03	4.51e-02	3.72e-03	3.23e-03	1.17e-03	0.00e+00	3.68e-03	4.89e-04	4.08e-04	3.77e-03	2.12e-03	7.27e-02		2.24e-01
3	803703	802073	153393	146979	8.79e-03	4.72e-02	4.18e-03	2.42e-03	3.50e-04	0.00e+00	4.09e-03	6.69e-04	4.80e-04	3.95e-03	2.11e-03	7.43e-02		2.98e-01
4	812653	811354	146933	148563	9.35e-03	5.05e-02	4.90e-03	3.54e-03	4.62e-04	0.00e+00	4.31e-03	5.66e-04	4.73e-04	3.95e-03	2.43e-03	8.05e-02		3.79e-01
5	811205	807750	138788	140087	1.09e-02	4.97e-02	5.15e-03	1.91e-03	4.32e-04	0.00e+00	4.47e-03	5.76e-04	4.53e-04	4.46e-03	2.46e-03	8.05e-02		4.59e-01
6	811028	811820	141042	144497	1.09e-02	5.27e-02	5.20e-03	2.70e-03	4.73e-04	0.00e+00	4.34e-03	5.37e-04	4.45e-04	4.12e-03	2.49e-03	8.39e-02		5.43e-01
7	813045	812527	142025	141233	1.09e-02	5.10e-02	5.25e-03	2.70e-03	5.90e-04	0.00e+00	4.54e-03	6.55e-04	4.49e-04	4.19e-03	2.50e-03	8.28e-02		6.26e-01
8	812344	812049	140814	141332	1.09e-02	5.46e-02	8.83e-03	6.24e-03	1.47e-03	0.00e+00	4.50e-03	6.44e-04	4.82e-04	4.18e-03	2.45e-03	9.43e-02		7.20e-01
9	812257	813429	142321	142616	1.11e-02	5.15e-02	4.46e-03	2.71e-03	1.31e-03	0.00e+00	4.51e-03	6.25e-04	4.44e-04	3.82e-03	2.20e-03	8.27e-02		8.03e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.4102 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.4102 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.410220  0.000000
];

