% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_209 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	818691	800700	141586	141586	9.70e-03	4.13e-02	8.34e-03	1.89e-02	2.00e-03	0.00e+00	2.62e-03	4.79e-04	4.18e-04	1.32e-02	1.92e-03	9.88e-02		9.88e-02
1	801584	823483	129527	147518	9.57e-03	3.60e-02	3.66e-03	6.63e-03	1.08e-03	0.00e+00	3.20e-03	4.54e-04	3.66e-04	6.16e-03	2.17e-03	6.93e-02		1.68e-01
2	806386	788572	147440	125541	9.81e-03	5.44e-02	3.96e-03	2.17e-03	3.64e-04	0.00e+00	3.66e-03	5.31e-04	4.13e-04	4.83e-03	2.11e-03	8.23e-02		2.50e-01
3	800843	809753	143444	161258	9.39e-03	5.32e-02	4.00e-03	4.46e-03	3.64e-04	0.00e+00	3.94e-03	6.44e-04	4.46e-04	4.35e-03	2.14e-03	8.30e-02		3.33e-01
4	809247	807709	149793	140883	9.74e-03	5.58e-02	5.22e-03	2.25e-03	5.11e-04	0.00e+00	4.19e-03	4.42e-04	5.00e-04	4.23e-03	2.06e-03	8.50e-02		4.18e-01
5	811984	812011	142194	143732	9.66e-03	5.10e-02	4.49e-03	3.00e-03	4.95e-04	0.00e+00	4.26e-03	6.05e-04	4.47e-04	4.82e-03	2.12e-03	8.09e-02		4.99e-01
6	812605	809587	140263	140236	9.69e-03	5.64e-02	4.45e-03	2.25e-03	4.04e-04	0.00e+00	4.22e-03	6.95e-04	4.69e-04	4.29e-03	2.14e-03	8.50e-02		5.84e-01
7	810024	810102	140448	143466	9.69e-03	5.34e-02	4.46e-03	3.00e-03	5.29e-04	0.00e+00	4.21e-03	6.41e-04	4.96e-04	4.58e-03	2.13e-03	8.31e-02		6.67e-01
8	813702	811782	143835	143757	9.67e-03	5.65e-02	4.36e-03	3.74e-03	1.50e-03	0.00e+00	4.35e-03	7.71e-04	4.72e-04	4.50e-03	2.17e-03	8.80e-02		7.55e-01
9	815307	814618	140963	142883	9.69e-03	5.33e-02	4.44e-03	3.45e-03	1.27e-03	0.00e+00	4.26e-03	1.16e-03	4.70e-04	4.27e-03	2.15e-03	8.45e-02		8.40e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.5807 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.5807 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.580684  0.000000
];

