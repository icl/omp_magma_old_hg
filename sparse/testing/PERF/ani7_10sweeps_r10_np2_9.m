% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_2 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	821686	804844	141586	141586	6.80e-03	4.74e-01	1.08e-01	2.34e-02	6.61e-03	0.00e+00	9.58e-02	2.42e-04	1.12e-02	3.81e-02	6.97e-02	8.34e-01		8.34e-01
1	804454	801492	126532	143374	7.55e-03	5.87e-01	1.09e-01	2.13e-02	8.92e-03	0.00e+00	1.13e-01	2.25e-04	1.19e-02	3.68e-02	8.38e-02	9.80e-01		1.81e+00
2	809753	794051	144570	147532	7.80e-03	8.63e-01	1.30e-01	2.05e-02	9.57e-03	0.00e+00	1.34e-01	4.00e-04	1.31e-02	3.57e-02	8.75e-02	1.30e+00		3.12e+00
3	792573	811500	140077	155779	7.52e-03	1.06e+00	1.41e-01	2.24e-02	1.19e-02	0.00e+00	1.52e-01	3.04e-04	1.40e-02	3.55e-02	8.46e-02	1.53e+00		4.65e+00
4	806698	813623	158063	139136	7.71e-03	1.04e+00	1.47e-01	2.51e-02	1.37e-02	0.00e+00	1.54e-01	2.75e-04	1.39e-02	3.64e-02	8.51e-02	1.53e+00		6.17e+00
5	812071	810258	144743	137818	7.75e-03	1.13e+00	1.53e-01	2.31e-02	1.20e-02	0.00e+00	1.45e-01	3.78e-04	1.33e-02	3.71e-02	8.58e-02	1.60e+00		7.78e+00
6	813277	810728	140176	141989	7.72e-03	1.15e+00	1.54e-01	2.36e-02	1.34e-02	0.00e+00	1.56e-01	3.44e-04	1.41e-02	3.68e-02	8.59e-02	1.64e+00		9.42e+00
7	811295	810073	139776	142325	7.71e-03	1.16e+00	1.56e-01	2.18e-02	1.11e-02	0.00e+00	1.44e-01	4.01e-04	1.32e-02	3.61e-02	8.57e-02	1.64e+00		1.11e+01
8	811856	812188	142564	143786	7.72e-03	1.16e+00	1.56e-01	2.25e-02	1.27e-02	0.00e+00	1.49e-01	3.96e-04	1.36e-02	3.61e-02	8.63e-02	1.64e+00		1.27e+01
9	814376	818039	142809	142477	7.78e-03	1.17e+00	1.57e-01	2.30e-02	1.35e-02	0.00e+00	1.54e-01	4.10e-04	1.39e-02	3.61e-02	8.69e-02	1.66e+00		1.44e+01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 15.2294 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 15.2294 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  15.229407  0.000000
];

