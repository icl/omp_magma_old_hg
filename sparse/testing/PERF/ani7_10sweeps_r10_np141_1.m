% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_141 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	813637	807142	141586	141586	8.60e-03	3.41e-02	7.44e-03	1.36e-02	7.32e-04	0.00e+00	2.67e-03	5.43e-04	4.16e-04	9.64e-03	2.23e-03	8.00e-02		8.00e-02
1	809258	811698	134581	141076	8.61e-03	3.12e-02	4.19e-03	2.15e-03	8.07e-04	0.00e+00	4.29e-03	3.86e-04	4.01e-04	5.63e-03	2.54e-03	6.02e-02		1.40e-01
2	805986	808603	139766	137326	8.12e-03	4.72e-02	4.50e-03	1.62e-03	3.86e-04	0.00e+00	3.67e-03	6.43e-04	4.24e-04	3.97e-03	2.31e-03	7.28e-02		2.13e-01
3	795222	802911	143844	141227	7.37e-03	5.00e-02	4.72e-03	3.27e-03	3.31e-04	0.00e+00	4.31e-03	5.15e-04	4.90e-04	3.95e-03	2.16e-03	7.71e-02		2.90e-01
4	805468	809356	155414	147725	7.31e-03	4.87e-02	4.98e-03	1.69e-03	4.13e-04	0.00e+00	4.29e-03	6.67e-04	4.78e-04	4.23e-03	2.18e-03	7.49e-02		3.65e-01
5	813946	811566	145973	142085	7.39e-03	4.59e-02	5.26e-03	2.50e-03	1.13e-03	0.00e+00	4.50e-03	4.36e-04	5.31e-04	4.38e-03	2.21e-03	7.43e-02		4.39e-01
6	813052	809863	138301	140681	7.42e-03	5.18e-02	5.32e-03	2.97e-03	3.99e-04	0.00e+00	4.35e-03	4.32e-04	5.01e-04	4.22e-03	2.18e-03	7.96e-02		5.19e-01
7	814211	811650	140001	143190	7.38e-03	4.85e-02	5.25e-03	1.79e-03	3.91e-04	0.00e+00	4.40e-03	4.76e-04	4.95e-04	4.22e-03	2.18e-03	7.51e-02		5.94e-01
8	806072	814434	139648	142209	7.44e-03	5.20e-02	5.33e-03	2.95e-03	1.08e-03	0.00e+00	4.39e-03	6.77e-04	5.02e-04	4.10e-03	2.14e-03	8.06e-02		6.75e-01
9	809691	811732	148593	140231	7.47e-03	4.78e-02	5.27e-03	2.89e-03	1.11e-03	0.00e+00	4.49e-03	7.92e-04	4.83e-04	3.95e-03	2.17e-03	7.64e-02		7.51e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.3883 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.3883 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.388313  0.000000
];

