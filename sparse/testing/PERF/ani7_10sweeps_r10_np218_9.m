% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_218 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	821627	804204	141586	141586	9.92e-03	4.23e-02	9.10e-03	1.81e-02	9.55e-04	0.00e+00	2.53e-03	4.54e-04	4.09e-04	1.31e-02	1.85e-03	9.88e-02		9.88e-02
1	809466	810796	126591	144014	9.86e-03	3.53e-02	3.58e-03	6.02e-03	1.62e-03	0.00e+00	3.11e-03	4.64e-04	3.43e-04	7.60e-03	2.07e-03	6.99e-02		1.69e-01
2	790827	815597	139558	138228	9.91e-03	5.24e-02	3.82e-03	2.21e-03	4.83e-04	0.00e+00	3.99e-03	3.93e-04	4.44e-04	4.31e-03	2.04e-03	8.00e-02		2.49e-01
3	806486	810791	159003	134233	1.00e-02	5.20e-02	3.97e-03	3.81e-03	3.34e-04	0.00e+00	3.81e-03	3.66e-04	4.46e-04	4.31e-03	2.08e-03	8.12e-02		3.30e-01
4	805042	811990	144150	139845	9.97e-03	5.53e-02	5.28e-03	3.14e-03	4.81e-04	0.00e+00	4.03e-03	8.29e-04	4.79e-04	4.75e-03	2.10e-03	8.64e-02		4.16e-01
5	808822	809439	146399	139451	9.94e-03	5.00e-02	4.48e-03	3.03e-03	7.11e-04	0.00e+00	4.11e-03	7.13e-04	4.38e-04	5.08e-03	2.05e-03	8.05e-02		4.97e-01
6	810911	809958	143425	142808	9.92e-03	5.49e-02	4.55e-03	4.37e-03	4.87e-04	0.00e+00	4.14e-03	7.88e-04	4.33e-04	4.73e-03	2.07e-03	8.64e-02		5.83e-01
7	811803	810519	142142	143095	9.92e-03	5.10e-02	4.54e-03	2.79e-03	6.42e-04	0.00e+00	4.21e-03	6.84e-04	4.35e-04	4.33e-03	2.08e-03	8.06e-02		6.64e-01
8	812831	813223	142056	143340	9.91e-03	5.53e-02	4.59e-03	3.43e-03	6.93e-04	0.00e+00	4.13e-03	6.79e-04	4.31e-04	4.60e-03	2.09e-03	8.59e-02		7.50e-01
9	814914	812785	141834	141442	1.00e-02	5.14e-02	4.59e-03	4.78e-03	1.51e-03	0.00e+00	4.21e-03	6.70e-04	4.68e-04	4.34e-03	2.11e-03	8.41e-02		8.34e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.5823 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.5823 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.582275  0.000000
];

