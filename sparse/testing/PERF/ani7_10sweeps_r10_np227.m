% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_227 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	821051	806192	141586	141586	1.20e-02	6.44e-02	1.60e-02	2.47e-02	2.07e-03	0.00e+00	3.08e-03	4.48e-04	3.68e-04	2.16e-02	1.82e-03	1.47e-01		1.47e-01
1	823453	814865	127167	142026	1.20e-02	4.86e-02	3.51e-03	7.89e-03	1.53e-03	0.00e+00	3.62e-03	4.34e-04	3.90e-04	1.23e-02	2.39e-03	9.26e-02		2.39e-01
2	819315	809553	125571	134159	1.21e-02	7.11e-02	4.92e-03	2.40e-03	1.32e-03	0.00e+00	4.07e-03	7.01e-04	4.89e-04	4.42e-03	2.50e-03	1.04e-01		3.43e-01
3	811378	810485	130515	140277	1.21e-02	6.70e-02	4.84e-03	2.57e-03	5.34e-04	0.00e+00	4.40e-03	7.93e-04	4.88e-04	4.51e-03	2.48e-03	9.97e-02		4.43e-01
4	808360	812843	139258	140151	1.22e-02	7.70e-02	4.95e-03	2.62e-03	6.43e-04	0.00e+00	4.72e-03	5.79e-04	5.50e-04	4.47e-03	2.41e-03	1.10e-01		5.53e-01
5	806681	809649	143081	138598	1.21e-02	6.61e-02	4.82e-03	4.18e-03	5.56e-04	0.00e+00	4.69e-03	5.40e-04	4.88e-04	5.32e-03	2.43e-03	1.01e-01		6.54e-01
6	811892	813775	145566	142598	1.21e-02	7.55e-02	4.81e-03	3.49e-03	5.54e-04	0.00e+00	4.79e-03	5.20e-04	4.91e-04	4.66e-03	2.46e-03	1.09e-01		7.64e-01
7	810821	811904	141161	139278	1.21e-02	6.72e-02	4.93e-03	2.11e-03	7.47e-04	0.00e+00	4.80e-03	8.71e-04	5.21e-04	4.48e-03	2.43e-03	1.00e-01		8.64e-01
8	811039	810373	143038	141955	1.21e-02	7.58e-02	4.93e-03	2.57e-03	1.64e-03	0.00e+00	4.70e-03	7.48e-04	5.27e-04	4.43e-03	2.46e-03	1.10e-01		9.74e-01
9	812847	812917	143626	144292	1.21e-02	6.84e-02	4.91e-03	2.36e-03	1.56e-03	0.00e+00	4.83e-03	7.71e-04	4.88e-04	4.43e-03	2.43e-03	1.02e-01		1.08e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.8201 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.8201 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.820094  0.000000
];

