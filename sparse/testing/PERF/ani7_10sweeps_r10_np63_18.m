% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_63 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	820747	807290	141586	141586	3.23e-03	2.68e-02	4.66e-03	6.77e-03	6.84e-04	0.00e+00	3.82e-03	2.47e-04	4.60e-04	6.54e-03	2.28e-03	5.55e-02		5.55e-02
1	816425	803820	127471	140928	3.23e-03	3.04e-02	3.42e-03	2.34e-03	1.29e-03	0.00e+00	4.78e-03	2.34e-04	5.10e-04	3.96e-03	3.08e-03	5.32e-02		1.09e-01
2	808632	795577	132599	145204	3.25e-03	5.08e-02	4.39e-03	1.28e-03	4.09e-04	0.00e+00	5.72e-03	3.77e-04	5.56e-04	3.27e-03	3.24e-03	7.33e-02		1.82e-01
3	809448	812069	141198	154253	3.24e-03	6.39e-02	4.81e-03	1.79e-03	5.37e-04	0.00e+00	6.59e-03	3.09e-04	6.43e-04	3.26e-03	3.15e-03	8.82e-02		2.70e-01
4	804843	811852	141188	138567	3.29e-03	6.39e-02	5.34e-03	2.06e-03	8.45e-04	0.00e+00	7.18e-03	3.81e-04	6.84e-04	3.38e-03	2.94e-03	9.00e-02		3.60e-01
5	807802	805562	146598	139589	3.28e-03	5.91e-02	5.10e-03	1.71e-03	7.49e-04	0.00e+00	7.22e-03	3.51e-04	6.85e-04	3.56e-03	2.92e-03	8.47e-02		4.45e-01
6	811437	812337	144445	146685	3.28e-03	6.10e-02	5.10e-03	2.34e-03	7.54e-04	0.00e+00	7.18e-03	3.18e-04	6.80e-04	3.38e-03	3.01e-03	8.70e-02		5.32e-01
7	813948	812989	141616	140716	3.29e-03	6.08e-02	5.18e-03	1.46e-03	7.13e-04	0.00e+00	7.20e-03	3.72e-04	6.48e-04	3.30e-03	3.02e-03	8.60e-02		6.18e-01
8	813190	812053	139911	140870	3.30e-03	6.34e-02	5.24e-03	1.91e-03	5.99e-04	0.00e+00	7.03e-03	4.52e-04	6.33e-04	3.43e-03	3.02e-03	8.90e-02		7.07e-01
9	813285	814081	141475	142612	3.31e-03	6.22e-02	5.23e-03	1.53e-03	1.12e-03	0.00e+00	7.09e-03	3.87e-04	6.25e-04	3.28e-03	3.03e-03	8.79e-02		7.95e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.1599 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.1599 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.159941  0.000000
];

