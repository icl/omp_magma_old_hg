% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_37 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	819265	810581	141586	141586	3.32e-03	3.56e-02	6.54e-03	6.70e-03	6.29e-04	0.00e+00	6.47e-03	2.86e-04	7.02e-04	6.00e-03	3.78e-03	7.00e-02		7.00e-02
1	801746	812659	128953	137637	3.31e-03	4.24e-02	5.71e-03	2.51e-03	9.46e-04	0.00e+00	7.98e-03	2.04e-04	7.52e-04	4.59e-03	4.84e-03	7.33e-02		1.43e-01
2	792218	811419	147278	136365	3.34e-03	6.58e-02	7.35e-03	3.52e-03	6.40e-04	0.00e+00	9.68e-03	2.83e-04	8.00e-04	3.99e-03	5.12e-03	1.01e-01		2.44e-01
3	813709	804967	157612	138411	3.36e-03	8.82e-02	8.03e-03	3.13e-03	9.41e-04	0.00e+00	1.12e-02	1.95e-04	9.39e-04	4.02e-03	5.19e-03	1.25e-01		3.69e-01
4	809705	802801	136927	145669	3.33e-03	9.50e-02	8.76e-03	2.43e-03	1.16e-03	0.00e+00	1.22e-02	4.42e-04	1.02e-03	4.16e-03	4.90e-03	1.33e-01		5.02e-01
5	808719	813672	141736	148640	3.33e-03	8.71e-02	8.57e-03	1.99e-03	1.42e-03	0.00e+00	1.25e-02	4.27e-04	1.00e-03	4.48e-03	4.99e-03	1.26e-01		6.28e-01
6	811961	809660	143528	138575	3.37e-03	9.26e-02	8.67e-03	2.38e-03	9.85e-04	0.00e+00	1.16e-02	3.64e-04	9.35e-04	4.14e-03	5.00e-03	1.30e-01		7.58e-01
7	814022	811286	141092	143393	3.35e-03	9.27e-02	8.72e-03	2.38e-03	1.13e-03	0.00e+00	1.24e-02	3.22e-04	9.90e-04	4.02e-03	5.00e-03	1.31e-01		8.89e-01
8	808329	812003	139837	142573	3.33e-03	9.49e-02	8.81e-03	2.07e-03	1.24e-03	0.00e+00	1.20e-02	3.02e-04	9.65e-04	4.18e-03	4.98e-03	1.33e-01		1.02e+00
9	811179	814966	146336	142662	3.38e-03	9.21e-02	8.72e-03	1.94e-03	1.42e-03	0.00e+00	1.20e-02	4.48e-04	9.05e-04	4.01e-03	5.04e-03	1.30e-01		1.15e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.5269 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.5269 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.526887  0.000000
];

