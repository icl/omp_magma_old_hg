% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_41 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	813263	807826	141586	141586	3.29e-03	3.32e-02	6.02e-03	6.74e-03	1.27e-03	0.00e+00	5.88e-03	2.53e-04	6.77e-04	6.06e-03	3.39e-03	6.67e-02		6.67e-02
1	798197	812161	134955	140392	3.28e-03	3.94e-02	5.12e-03	2.83e-03	1.11e-03	0.00e+00	7.35e-03	2.42e-04	6.99e-04	4.70e-03	4.42e-03	6.92e-02		1.36e-01
2	807200	817523	150827	136863	3.33e-03	6.12e-02	6.64e-03	1.89e-03	5.16e-04	0.00e+00	8.81e-03	2.11e-04	7.40e-04	3.84e-03	4.89e-03	9.21e-02		2.28e-01
3	809240	798652	142630	132307	3.34e-03	8.93e-02	7.84e-03	1.69e-03	7.76e-04	0.00e+00	1.02e-02	3.74e-04	8.57e-04	3.79e-03	4.51e-03	1.23e-01		3.51e-01
4	799370	808249	141396	151984	3.29e-03	8.38e-02	7.65e-03	2.38e-03	1.19e-03	0.00e+00	1.12e-02	3.96e-04	9.27e-04	3.86e-03	4.42e-03	1.19e-01		4.70e-01
5	813219	810407	152071	143192	3.31e-03	8.08e-02	7.71e-03	3.61e-03	1.19e-03	0.00e+00	1.13e-02	3.33e-04	9.34e-04	4.27e-03	4.52e-03	1.18e-01		5.88e-01
6	811266	809127	139028	141840	3.32e-03	8.58e-02	7.91e-03	1.99e-03	9.91e-04	0.00e+00	1.10e-02	2.54e-04	9.00e-04	3.91e-03	4.46e-03	1.21e-01		7.08e-01
7	807819	812222	141787	143926	3.33e-03	8.50e-02	7.90e-03	1.94e-03	1.09e-03	0.00e+00	1.11e-02	2.78e-04	9.05e-04	3.79e-03	4.49e-03	1.20e-01		8.28e-01
8	812748	814015	146040	141637	3.33e-03	8.57e-02	7.92e-03	2.41e-03	1.00e-03	0.00e+00	1.06e-02	3.05e-04	8.94e-04	3.99e-03	4.50e-03	1.21e-01		9.49e-01
9	811077	812734	141917	140650	3.37e-03	8.72e-02	8.01e-03	2.25e-03	1.53e-03	0.00e+00	1.11e-02	3.02e-04	8.89e-04	3.80e-03	4.50e-03	1.23e-01		1.07e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.4379 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.4379 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.437881  0.000000
];

