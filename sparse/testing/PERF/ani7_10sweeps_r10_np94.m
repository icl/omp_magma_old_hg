% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_94 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	820363	816492	141586	141586	6.98e-03	3.96e-02	9.84e-03	1.20e-02	6.76e-04	0.00e+00	3.91e-03	3.68e-04	5.04e-04	8.20e-03	2.44e-03	8.45e-02		8.45e-02
1	791121	804478	127855	131726	7.41e-03	3.48e-02	4.78e-03	2.67e-03	7.57e-04	0.00e+00	4.82e-03	3.27e-04	5.03e-04	5.49e-03	3.19e-03	6.47e-02		1.49e-01
2	790027	811375	157903	144546	7.26e-03	6.25e-02	5.38e-03	1.42e-03	4.86e-04	0.00e+00	5.56e-03	3.65e-04	6.61e-04	3.42e-03	3.41e-03	9.05e-02		2.40e-01
3	803175	804192	159803	138455	7.02e-03	6.28e-02	6.04e-03	2.19e-03	6.82e-04	0.00e+00	6.28e-03	2.86e-04	7.35e-04	3.46e-03	3.28e-03	9.28e-02		3.33e-01
4	812515	808041	147461	146444	6.67e-03	6.67e-02	6.21e-03	1.81e-03	7.92e-04	0.00e+00	6.97e-03	2.51e-04	7.48e-04	4.07e-03	3.29e-03	9.75e-02		4.30e-01
5	810695	810995	138926	143400	6.78e-03	6.19e-02	6.17e-03	1.83e-03	1.29e-03	0.00e+00	6.86e-03	3.54e-04	6.81e-04	3.78e-03	3.77e-03	9.34e-02		5.23e-01
6	810337	813033	141552	141252	6.66e-03	6.94e-02	6.53e-03	2.69e-03	6.85e-04	0.00e+00	7.00e-03	3.59e-04	6.95e-04	3.52e-03	3.25e-03	1.01e-01		6.24e-01
7	811395	809130	142716	140020	7.07e-03	6.35e-02	6.57e-03	1.85e-03	6.51e-04	0.00e+00	6.79e-03	5.02e-04	7.10e-04	3.47e-03	3.80e-03	9.49e-02		7.19e-01
8	813801	813110	142464	144729	7.19e-03	7.04e-02	6.36e-03	1.58e-03	1.16e-03	0.00e+00	6.87e-03	4.51e-04	6.91e-04	3.68e-03	3.48e-03	1.02e-01		8.21e-01
9	812116	813990	140864	141555	7.13e-03	6.48e-02	6.33e-03	2.58e-03	1.27e-03	0.00e+00	6.81e-03	4.93e-04	6.95e-04	3.47e-03	3.58e-03	9.71e-02		9.18e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.4399 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.4399 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.439909  0.000000
];

