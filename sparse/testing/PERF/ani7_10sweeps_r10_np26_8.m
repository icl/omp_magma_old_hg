% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_26 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	808762	816511	141586	141586	3.60e-03	4.82e-02	9.32e-03	7.27e-03	1.11e-03	0.00e+00	9.75e-03	2.61e-04	1.00e-03	7.21e-03	5.75e-03	9.34e-02		9.34e-02
1	792879	811856	139456	131707	3.66e-03	6.07e-02	8.63e-03	3.15e-03	1.47e-03	0.00e+00	1.19e-02	2.29e-04	1.17e-03	5.79e-03	7.28e-03	1.04e-01		1.97e-01
2	816252	803302	156145	137168	3.70e-03	9.34e-02	1.11e-02	3.34e-03	1.11e-03	0.00e+00	1.45e-02	3.59e-04	1.18e-03	5.13e-03	8.00e-03	1.42e-01		3.39e-01
3	807525	812529	133578	146528	3.63e-03	1.41e-01	1.27e-02	3.04e-03	1.20e-03	0.00e+00	1.68e-02	3.01e-04	1.35e-03	5.12e-03	7.58e-03	1.92e-01		5.32e-01
4	814226	807856	143111	138107	3.69e-03	1.34e-01	1.29e-02	3.01e-03	2.03e-03	0.00e+00	1.84e-02	3.49e-04	1.52e-03	5.09e-03	7.46e-03	1.88e-01		7.20e-01
5	814323	808943	137215	143585	3.68e-03	1.31e-01	1.30e-02	3.25e-03	1.59e-03	0.00e+00	1.81e-02	3.01e-04	1.41e-03	5.53e-03	7.49e-03	1.85e-01		9.05e-01
6	811964	810999	137924	143304	3.65e-03	1.36e-01	1.32e-02	3.57e-03	1.67e-03	0.00e+00	1.82e-02	3.67e-04	1.44e-03	5.10e-03	7.50e-03	1.91e-01		1.10e+00
7	812516	813067	141089	142054	3.68e-03	1.35e-01	1.32e-02	2.86e-03	1.41e-03	0.00e+00	1.77e-02	3.39e-04	1.36e-03	5.41e-03	7.53e-03	1.89e-01		1.28e+00
8	812192	811486	141343	140792	3.71e-03	1.37e-01	1.33e-02	2.67e-03	2.16e-03	0.00e+00	1.80e-02	3.71e-04	1.41e-03	5.21e-03	7.50e-03	1.92e-01		1.48e+00
9	812073	811698	142473	143179	3.71e-03	1.37e-01	1.33e-02	2.60e-03	2.04e-03	0.00e+00	1.83e-02	4.17e-04	1.42e-03	5.12e-03	7.48e-03	1.91e-01		1.67e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 2.0639 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 2.0639 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  2.063857  0.000000
];

