% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_269 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	825024	818144	141586	141586	1.21e-02	7.54e-02	1.81e-02	2.75e-02	1.36e-02	0.00e+00	2.79e-03	5.31e-04	3.26e-04	2.68e-02	1.57e-03	1.79e-01		1.79e-01
1	819890	790456	123194	130074	1.22e-02	5.07e-02	3.07e-03	6.21e-03	1.09e-02	0.00e+00	3.17e-03	3.94e-04	3.41e-04	7.87e-03	2.01e-03	9.69e-02		2.76e-01
2	809229	810411	129134	158568	1.18e-02	7.28e-02	3.82e-02	2.49e-03	4.56e-04	0.00e+00	3.55e-03	5.58e-04	4.59e-04	4.48e-03	2.13e-03	1.37e-01		4.13e-01
3	809437	806206	140601	139419	1.24e-02	7.05e-02	4.57e-03	4.85e-03	4.66e-04	0.00e+00	3.76e-03	4.58e-04	4.72e-04	4.50e-03	2.11e-03	1.04e-01		5.17e-01
4	813116	804765	141199	144430	1.20e-02	7.93e-02	4.20e-03	2.17e-03	5.81e-04	0.00e+00	3.98e-03	5.90e-04	4.91e-04	4.68e-03	2.02e-03	1.10e-01		6.27e-01
5	813597	814570	138325	146676	1.20e-02	6.73e-02	4.12e-03	2.80e-03	2.23e-03	0.00e+00	4.05e-03	6.53e-04	4.31e-04	5.76e-03	2.10e-03	1.01e-01		7.28e-01
6	815083	810113	138650	137677	1.22e-02	8.01e-02	4.24e-03	2.56e-03	6.07e-04	0.00e+00	4.17e-03	7.89e-04	4.48e-04	4.82e-03	2.13e-03	1.12e-01		8.40e-01
7	810513	810647	137970	142940	1.22e-02	6.97e-02	4.23e-03	1.65e-03	8.52e-04	0.00e+00	4.04e-03	8.60e-04	4.29e-04	4.56e-03	2.09e-03	1.01e-01		9.41e-01
8	812218	814402	143346	143212	1.21e-02	7.97e-02	4.30e-03	3.50e-03	4.87e-04	0.00e+00	4.01e-03	6.56e-04	4.26e-04	4.52e-03	2.10e-03	1.12e-01		1.05e+00
9	810689	813610	142447	140263	1.22e-02	6.82e-02	4.26e-03	2.61e-03	1.95e-03	0.00e+00	3.97e-03	6.71e-04	4.26e-04	4.41e-03	2.11e-03	1.01e-01		1.15e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.8446 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.8446 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.844629  0.000000
];

