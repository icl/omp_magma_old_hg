% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_50 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	818742	797126	141586	141586	3.24e-03	2.95e-02	5.28e-03	6.71e-03	1.10e-03	0.00e+00	4.79e-03	2.75e-04	5.54e-04	5.82e-03	2.81e-03	6.01e-02		6.01e-02
1	788356	804910	129476	151092	3.22e-03	3.44e-02	4.20e-03	2.07e-03	1.15e-03	0.00e+00	6.05e-03	2.15e-04	6.11e-04	4.12e-03	3.67e-03	5.97e-02		1.20e-01
2	808145	804374	160668	144114	3.26e-03	5.50e-02	5.33e-03	2.73e-03	7.59e-04	0.00e+00	7.09e-03	3.65e-04	6.42e-04	3.52e-03	3.99e-03	8.27e-02		2.03e-01
3	814175	798616	141685	145456	3.32e-03	7.47e-02	6.28e-03	2.06e-03	5.79e-04	0.00e+00	8.36e-03	2.53e-04	7.41e-04	3.47e-03	3.78e-03	1.04e-01		3.06e-01
4	805593	808845	136461	152020	3.24e-03	7.36e-02	6.31e-03	1.86e-03	8.25e-04	0.00e+00	8.68e-03	2.98e-04	7.92e-04	3.48e-03	3.69e-03	1.03e-01		4.09e-01
5	806237	814835	145848	142596	3.28e-03	7.10e-02	6.33e-03	2.42e-03	8.92e-04	0.00e+00	9.11e-03	3.90e-04	7.78e-04	3.96e-03	3.75e-03	1.02e-01		5.11e-01
6	808610	811422	146010	137412	3.30e-03	7.34e-02	6.42e-03	1.75e-03	8.69e-04	0.00e+00	9.01e-03	2.91e-04	7.83e-04	3.65e-03	3.73e-03	1.03e-01		6.14e-01
7	810498	813041	144443	141631	3.35e-03	7.30e-02	6.46e-03	2.36e-03	7.90e-04	0.00e+00	8.89e-03	3.42e-04	7.52e-04	3.50e-03	3.74e-03	1.03e-01		7.17e-01
8	809252	812677	143361	140818	3.32e-03	7.45e-02	6.49e-03	2.58e-03	8.56e-04	0.00e+00	8.93e-03	3.92e-04	7.39e-04	3.72e-03	3.78e-03	1.05e-01		8.22e-01
9	812059	812242	145413	141988	3.31e-03	7.34e-02	6.48e-03	1.59e-03	1.44e-03	0.00e+00	9.20e-03	4.11e-04	8.01e-04	3.47e-03	3.76e-03	1.04e-01		9.26e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.3047 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.3047 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.304666  0.000000
];

