% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_130 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	815548	810978	141586	141586	5.94e-03	2.78e-02	5.72e-03	1.09e-02	5.60e-04	0.00e+00	2.83e-03	3.16e-04	3.46e-04	6.68e-03	1.76e-03	6.28e-02		6.28e-02
1	796395	808745	132670	137240	6.00e-03	2.74e-02	3.04e-03	1.99e-03	6.29e-04	0.00e+00	3.45e-03	2.98e-04	3.80e-04	4.30e-03	2.06e-03	4.95e-02		1.12e-01
2	817197	817812	152629	140279	5.98e-03	3.79e-02	3.67e-03	2.84e-03	2.85e-04	0.00e+00	3.96e-03	3.20e-04	4.14e-04	3.07e-03	2.34e-03	6.07e-02		1.73e-01
3	804042	812847	132633	132018	6.07e-03	4.83e-02	4.27e-03	2.14e-03	3.77e-04	0.00e+00	4.68e-03	4.27e-04	4.59e-04	2.94e-03	2.27e-03	7.19e-02		2.45e-01
4	812516	813083	146594	137789	6.00e-03	4.80e-02	4.29e-03	1.63e-03	4.76e-04	0.00e+00	4.76e-03	3.72e-04	4.88e-04	3.66e-03	2.26e-03	7.19e-02		3.17e-01
5	810981	809175	138925	138358	6.02e-03	4.62e-02	4.37e-03	1.45e-03	8.79e-04	0.00e+00	4.87e-03	5.21e-04	4.63e-04	3.44e-03	3.02e-03	7.12e-02		3.88e-01
6	810654	808650	141266	143072	8.70e-03	5.00e-02	6.22e-03	1.88e-03	4.24e-04	0.00e+00	4.73e-03	4.58e-04	4.97e-04	3.32e-03	3.05e-03	7.92e-02		4.67e-01
7	810326	809770	142399	144403	8.71e-03	4.84e-02	6.24e-03	1.43e-03	4.12e-04	0.00e+00	4.82e-03	3.96e-04	4.69e-04	3.07e-03	2.95e-03	7.69e-02		5.44e-01
8	812047	814007	143533	144089	8.65e-03	5.04e-02	6.28e-03	2.18e-03	8.40e-04	0.00e+00	4.68e-03	4.99e-04	4.64e-04	3.18e-03	3.06e-03	8.02e-02		6.24e-01
9	812078	814504	142618	140658	8.75e-03	4.89e-02	6.36e-03	1.53e-03	1.07e-03	0.00e+00	4.83e-03	5.11e-04	4.58e-04	3.08e-03	3.03e-03	7.85e-02		7.03e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.1871 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.1871 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.187122  0.000000
];

