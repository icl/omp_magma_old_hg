% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_75 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	826076	813799	141586	141586	5.10e-03	3.05e-02	6.71e-03	9.06e-03	5.73e-04	0.00e+00	4.25e-03	4.21e-04	5.68e-04	5.95e-03	3.14e-03	6.63e-02		6.63e-02
1	799437	813449	122142	134419	7.49e-03	3.19e-02	5.16e-03	2.41e-03	5.68e-04	0.00e+00	4.34e-03	2.87e-04	5.17e-04	4.11e-03	2.80e-03	5.96e-02		1.26e-01
2	790860	809914	149587	135575	5.15e-03	4.94e-02	5.48e-03	3.65e-03	3.92e-04	0.00e+00	4.89e-03	3.68e-04	5.37e-04	3.40e-03	2.90e-03	7.61e-02		2.02e-01
3	807853	803570	158970	139916	5.13e-03	5.50e-02	5.76e-03	3.69e-03	5.19e-04	0.00e+00	5.69e-03	4.59e-04	6.01e-04	3.33e-03	3.00e-03	8.31e-02		2.85e-01
4	812995	806905	142783	147066	5.08e-03	5.74e-02	6.25e-03	1.91e-03	7.61e-04	0.00e+00	6.17e-03	6.26e-04	6.34e-04	3.72e-03	3.13e-03	8.57e-02		3.71e-01
5	808330	803884	138446	144536	5.10e-03	5.28e-02	6.57e-03	1.48e-03	1.13e-03	0.00e+00	6.10e-03	5.22e-04	5.95e-04	3.80e-03	3.04e-03	8.11e-02		4.52e-01
6	814280	812106	143917	148363	6.47e-03	5.47e-02	6.39e-03	2.00e-03	6.79e-04	0.00e+00	6.17e-03	3.49e-04	6.25e-04	3.45e-03	3.13e-03	8.40e-02		5.36e-01
7	813724	807863	138773	140947	5.12e-03	5.58e-02	6.65e-03	1.34e-03	5.72e-04	0.00e+00	6.02e-03	4.22e-04	5.98e-04	3.36e-03	3.05e-03	8.29e-02		6.19e-01
8	809388	812009	140135	145996	5.10e-03	5.73e-02	6.58e-03	1.76e-03	1.15e-03	0.00e+00	6.08e-03	5.78e-04	5.99e-04	3.53e-03	3.09e-03	8.57e-02		7.05e-01
9	814151	813253	145277	142656	5.17e-03	5.56e-02	6.47e-03	1.56e-03	8.99e-04	0.00e+00	5.84e-03	6.58e-04	5.82e-04	3.43e-03	3.10e-03	8.33e-02		7.88e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.2765 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.2765 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.276458  0.000000
];

