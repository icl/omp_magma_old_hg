% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_207 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	816140	815466	141586	141586	9.63e-03	3.98e-02	8.21e-03	1.81e-02	2.05e-03	0.00e+00	2.64e-03	4.62e-04	4.05e-04	1.31e-02	1.96e-03	9.64e-02		9.64e-02
1	805347	813404	132078	132752	9.65e-03	3.61e-02	3.75e-03	6.55e-03	1.00e-03	0.00e+00	3.31e-03	4.69e-04	3.62e-04	6.09e-03	2.19e-03	6.95e-02		1.66e-01
2	810784	809197	143677	135620	9.65e-03	5.48e-02	3.98e-03	2.57e-03	4.59e-04	0.00e+00	3.68e-03	7.08e-04	4.68e-04	4.67e-03	2.23e-03	8.32e-02		2.49e-01
3	812748	807685	139046	140633	9.59e-03	5.44e-02	4.49e-03	3.19e-03	4.21e-04	0.00e+00	4.00e-03	6.23e-04	4.76e-04	4.24e-03	2.18e-03	8.36e-02		3.33e-01
4	809354	808635	137888	142951	9.63e-03	5.77e-02	4.27e-03	4.89e-03	7.78e-04	0.00e+00	4.29e-03	6.95e-04	5.21e-04	4.29e-03	2.09e-03	8.92e-02		4.22e-01
5	816153	808983	142087	142806	9.54e-03	5.07e-02	4.20e-03	3.61e-03	4.80e-04	0.00e+00	4.34e-03	6.43e-04	4.86e-04	4.97e-03	2.14e-03	8.11e-02		5.03e-01
6	814495	810226	136094	143264	9.60e-03	5.69e-02	4.30e-03	2.17e-03	5.27e-04	0.00e+00	4.19e-03	6.42e-04	5.01e-04	4.33e-03	2.18e-03	8.54e-02		5.88e-01
7	811664	809850	138558	142827	9.61e-03	5.33e-02	4.29e-03	2.46e-03	4.33e-04	0.00e+00	4.13e-03	7.57e-04	4.48e-04	4.51e-03	2.16e-03	8.21e-02		6.70e-01
8	810274	810706	142195	144009	9.59e-03	5.72e-02	4.31e-03	3.82e-03	1.35e-03	0.00e+00	4.22e-03	8.37e-04	4.76e-04	4.73e-03	2.19e-03	8.87e-02		7.59e-01
9	813368	813847	144391	143959	9.60e-03	5.28e-02	4.29e-03	3.17e-03	1.26e-03	0.00e+00	4.40e-03	5.92e-04	4.48e-04	4.29e-03	2.18e-03	8.31e-02		8.42e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.5857 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.5857 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.585737  0.000000
];

