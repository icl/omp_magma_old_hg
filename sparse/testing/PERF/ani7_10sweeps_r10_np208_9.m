% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_208 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	821517	803381	141586	141586	9.54e-03	4.07e-02	8.07e-03	1.87e-02	9.71e-04	0.00e+00	2.63e-03	4.25e-04	4.07e-04	1.29e-02	1.95e-03	9.63e-02		9.63e-02
1	801376	806401	126701	144837	9.46e-03	3.62e-02	3.68e-03	4.48e-03	1.75e-03	0.00e+00	3.25e-03	4.52e-04	4.14e-04	7.29e-03	2.16e-03	6.92e-02		1.65e-01
2	795729	815305	147648	142623	9.51e-03	5.42e-02	3.88e-03	3.81e-03	3.30e-04	0.00e+00	3.53e-03	6.63e-04	4.36e-04	4.34e-03	2.16e-03	8.29e-02		2.48e-01
3	820767	807265	154101	134525	9.63e-03	5.27e-02	4.16e-03	3.03e-03	3.56e-04	0.00e+00	3.92e-03	6.87e-04	4.30e-04	4.34e-03	2.23e-03	8.15e-02		3.30e-01
4	808832	808194	129869	143371	9.52e-03	5.80e-02	4.28e-03	1.88e-03	8.97e-04	0.00e+00	4.27e-03	5.83e-04	4.81e-04	4.98e-03	2.11e-03	8.70e-02		4.17e-01
5	815084	808924	142609	143247	9.52e-03	5.19e-02	4.20e-03	3.61e-03	5.70e-04	0.00e+00	4.28e-03	6.49e-04	4.43e-04	4.81e-03	2.14e-03	8.21e-02		4.99e-01
6	811817	810788	137163	143323	9.57e-03	5.81e-02	4.23e-03	2.30e-03	4.65e-04	0.00e+00	4.18e-03	7.26e-04	4.37e-04	4.69e-03	2.18e-03	8.68e-02		5.86e-01
7	806756	810502	141236	142265	9.59e-03	5.27e-02	4.26e-03	2.65e-03	4.58e-04	0.00e+00	4.23e-03	6.85e-04	4.39e-04	4.28e-03	2.14e-03	8.15e-02		6.67e-01
8	810192	811737	147103	143357	9.54e-03	5.57e-02	4.23e-03	2.99e-03	5.68e-04	0.00e+00	4.26e-03	6.44e-04	4.37e-04	4.64e-03	2.16e-03	8.52e-02		7.52e-01
9	814200	815950	144473	142928	9.60e-03	5.28e-02	4.30e-03	2.42e-03	1.53e-03	0.00e+00	4.37e-03	8.46e-04	4.39e-04	4.32e-03	2.20e-03	8.29e-02		8.35e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.5828 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.5828 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.582799  0.000000
];

