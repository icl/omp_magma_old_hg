% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_249 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	823733	819977	141586	141586	1.12e-02	4.33e-02	8.96e-03	1.90e-02	2.45e-03	0.00e+00	2.21e-03	4.91e-04	3.78e-04	1.44e-02	1.70e-03	1.04e-01		1.04e-01
1	816067	810449	124485	128241	1.12e-02	3.68e-02	3.26e-03	6.00e-03	1.23e-03	0.00e+00	2.94e-03	4.66e-04	3.67e-04	6.76e-03	1.89e-03	7.09e-02		1.75e-01
2	805506	816148	132957	138575	1.11e-02	5.11e-02	3.70e-03	2.61e-03	3.62e-04	0.00e+00	3.10e-03	9.46e-04	3.82e-04	4.72e-03	2.02e-03	8.00e-02		2.55e-01
3	806654	795599	144324	133682	1.11e-02	5.09e-02	4.17e-03	3.24e-03	3.75e-04	0.00e+00	3.37e-03	4.88e-04	4.09e-04	4.17e-03	1.94e-03	8.02e-02		3.35e-01
4	812035	818805	143982	155037	1.09e-02	5.48e-02	4.02e-03	1.81e-03	3.60e-04	0.00e+00	3.58e-03	4.18e-04	4.32e-04	4.22e-03	2.06e-03	8.27e-02		4.18e-01
5	809853	804854	139406	132636	1.11e-02	5.03e-02	4.27e-03	2.02e-03	4.17e-04	0.00e+00	3.62e-03	6.65e-04	4.11e-04	5.13e-03	2.02e-03	8.00e-02		4.98e-01
6	804272	814490	142394	147393	1.10e-02	5.46e-02	4.20e-03	2.64e-03	3.79e-04	0.00e+00	3.59e-03	7.23e-04	4.42e-04	4.14e-03	2.08e-03	8.38e-02		5.82e-01
7	808290	803872	148781	138563	1.11e-02	5.05e-02	4.27e-03	3.88e-03	4.03e-04	0.00e+00	3.64e-03	6.88e-04	4.18e-04	4.48e-03	2.03e-03	8.15e-02		6.63e-01
8	813585	814280	145569	149987	1.10e-02	5.42e-02	4.19e-03	3.22e-03	2.11e-03	0.00e+00	3.58e-03	5.85e-04	4.12e-04	4.60e-03	2.08e-03	8.59e-02		7.49e-01
9	810523	812980	141080	140385	1.11e-02	5.14e-02	4.36e-03	2.75e-03	1.52e-03	0.00e+00	3.64e-03	4.99e-04	4.18e-04	4.22e-03	2.08e-03	8.20e-02		8.31e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.5808 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.5808 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.580766  0.000000
];

