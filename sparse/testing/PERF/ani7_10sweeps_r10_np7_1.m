% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_7 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	817532	819903	141586	141586	4.44e-03	1.46e-01	3.20e-02	1.07e-02	2.77e-03	0.00e+00	3.53e-02	2.78e-04	3.39e-03	1.42e-02	2.06e-02	2.69e-01		2.69e-01
1	809943	807520	130686	128315	4.59e-03	1.87e-01	3.18e-02	8.32e-03	3.48e-03	0.00e+00	4.36e-02	2.73e-04	3.89e-03	1.33e-02	2.60e-02	3.22e-01		5.91e-01
2	815496	821655	139081	141504	4.67e-03	3.00e-01	3.94e-02	6.54e-03	3.19e-03	0.00e+00	5.19e-02	4.10e-04	4.02e-03	1.28e-02	2.89e-02	4.52e-01		1.04e+00
3	805638	808724	134334	128175	4.72e-03	4.44e-01	4.56e-02	6.76e-03	3.61e-03	0.00e+00	5.95e-02	3.83e-04	4.55e-03	1.26e-02	2.66e-02	6.08e-01		1.65e+00
4	809342	813366	144998	141912	4.64e-03	4.09e-01	4.60e-02	7.67e-03	5.01e-03	0.00e+00	6.43e-02	3.54e-04	4.66e-03	1.26e-02	2.61e-02	5.80e-01		2.23e+00
5	812528	809924	142099	138075	4.69e-03	4.06e-01	4.65e-02	7.73e-03	5.00e-03	0.00e+00	6.20e-02	4.14e-04	4.56e-03	1.37e-02	2.62e-02	5.77e-01		2.81e+00
6	808869	810560	139719	142323	4.66e-03	4.17e-01	4.68e-02	7.52e-03	4.84e-03	0.00e+00	6.47e-02	2.82e-04	4.60e-03	1.26e-02	2.62e-02	5.89e-01		3.40e+00
7	813780	813092	144184	142493	4.67e-03	4.13e-01	4.68e-02	7.18e-03	4.78e-03	0.00e+00	6.38e-02	3.05e-04	4.58e-03	1.26e-02	2.64e-02	5.84e-01		3.98e+00
8	812193	812480	140079	140767	4.68e-03	4.24e-01	4.73e-02	8.06e-03	4.55e-03	0.00e+00	6.32e-02	4.95e-04	4.55e-03	1.29e-02	2.64e-02	5.97e-01		4.58e+00
9	812269	812906	142472	142185	4.69e-03	4.22e-01	4.74e-02	7.82e-03	5.36e-03	0.00e+00	6.44e-02	4.01e-04	4.61e-03	1.26e-02	2.64e-02	5.96e-01		5.17e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 5.6737 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 5.6737 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  5.673704  0.000000
];

