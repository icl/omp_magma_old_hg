% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_135 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	815684	801848	141586	141586	5.98e-03	2.83e-02	5.76e-03	1.10e-02	6.12e-04	0.00e+00	2.68e-03	3.02e-04	3.69e-04	7.26e-03	1.64e-03	6.40e-02		6.40e-02
1	809837	809075	132534	146370	5.96e-03	2.67e-02	2.91e-03	1.97e-03	8.15e-04	0.00e+00	3.23e-03	2.78e-04	3.83e-04	4.43e-03	2.24e-03	4.89e-02		1.13e-01
2	808105	809455	139187	139949	6.05e-03	4.35e-02	3.76e-03	2.07e-03	2.86e-04	0.00e+00	3.84e-03	4.25e-04	4.18e-04	3.05e-03	2.33e-03	6.58e-02		1.79e-01
3	805717	808173	141725	140375	6.04e-03	5.00e-02	4.08e-03	2.16e-03	3.32e-04	0.00e+00	4.23e-03	3.61e-04	4.73e-04	3.00e-03	2.21e-03	7.29e-02		2.51e-01
4	808407	808524	144919	142463	6.05e-03	4.81e-02	4.16e-03	4.14e-03	6.81e-04	0.00e+00	4.69e-03	4.41e-04	4.95e-04	3.75e-03	2.15e-03	7.47e-02		3.26e-01
5	813801	810230	143034	142917	6.05e-03	4.56e-02	4.19e-03	2.15e-03	9.88e-04	0.00e+00	4.60e-03	3.59e-04	4.92e-04	3.34e-03	2.20e-03	6.99e-02		3.96e-01
6	805973	807251	138446	142017	6.06e-03	4.91e-02	4.26e-03	2.23e-03	4.03e-04	0.00e+00	4.58e-03	3.60e-04	5.13e-04	3.29e-03	2.16e-03	7.30e-02		4.69e-01
7	810662	811002	147080	145802	6.05e-03	4.61e-02	4.25e-03	1.84e-03	4.49e-04	0.00e+00	4.55e-03	3.91e-04	5.11e-04	3.17e-03	2.21e-03	6.95e-02		5.39e-01
8	810318	812410	143197	142857	6.06e-03	4.91e-02	4.26e-03	1.94e-03	1.02e-03	0.00e+00	4.64e-03	3.60e-04	5.11e-04	3.08e-03	2.20e-03	7.31e-02		6.12e-01
9	814991	813778	144347	142255	6.07e-03	4.73e-02	4.30e-03	1.78e-03	1.12e-03	0.00e+00	4.63e-03	3.50e-04	4.67e-04	3.05e-03	2.22e-03	7.13e-02		6.83e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.1737 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.1737 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.173650  0.000000
];

