% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_13 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	820028	800296	141586	141586	5.35e-03	1.00e-01	2.78e-02	9.58e-03	1.69e-03	0.00e+00	2.03e-02	2.93e-04	2.58e-03	1.01e-02	1.52e-02	1.93e-01		1.93e-01
1	817646	811304	128190	147922	5.47e-03	1.06e-01	1.70e-02	5.35e-03	1.78e-03	0.00e+00	2.34e-02	2.29e-04	2.15e-03	8.53e-03	1.46e-02	1.84e-01		3.78e-01
2	815635	806754	131378	137720	4.09e-03	1.75e-01	2.22e-02	3.97e-03	1.63e-03	0.00e+00	2.88e-02	3.49e-04	2.24e-03	8.27e-03	1.57e-02	2.62e-01		6.40e-01
3	811150	808516	134195	143076	4.05e-03	2.50e-01	2.47e-02	4.51e-03	2.29e-03	0.00e+00	3.23e-02	2.54e-04	2.61e-03	7.89e-03	1.48e-02	3.43e-01		9.83e-01
4	818980	807063	139486	142120	4.06e-03	2.35e-01	2.52e-02	6.88e-03	3.35e-03	0.00e+00	3.66e-02	3.38e-04	2.75e-03	8.07e-03	1.45e-02	3.37e-01		1.32e+00
5	810160	805146	132461	144378	4.04e-03	2.27e-01	2.56e-02	5.72e-03	3.32e-03	0.00e+00	3.64e-02	2.91e-04	2.73e-03	8.36e-03	1.45e-02	3.28e-01		1.65e+00
6	812178	812944	142087	147101	4.04e-03	2.30e-01	2.55e-02	5.53e-03	3.00e-03	0.00e+00	3.61e-02	3.14e-04	2.66e-03	7.92e-03	1.46e-02	3.30e-01		1.98e+00
7	814705	810620	140875	140109	4.08e-03	2.36e-01	2.60e-02	4.43e-03	2.67e-03	0.00e+00	3.52e-02	2.92e-04	2.58e-03	8.31e-03	1.45e-02	3.34e-01		2.31e+00
8	804133	810609	139154	143239	4.05e-03	2.38e-01	2.60e-02	4.17e-03	2.90e-03	0.00e+00	3.51e-02	3.79e-04	2.62e-03	7.97e-03	1.44e-02	3.36e-01		2.65e+00
9	811810	814983	150532	144056	4.09e-03	2.28e-01	2.57e-02	4.84e-03	3.21e-03	0.00e+00	3.58e-02	2.46e-04	2.64e-03	7.85e-03	1.47e-02	3.27e-01		2.97e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 3.4252 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 3.4252 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  3.425209  0.000000
];

