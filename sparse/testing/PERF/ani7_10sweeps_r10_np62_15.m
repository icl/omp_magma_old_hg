% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_62 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	808042	820263	141586	141586	3.24e-03	2.71e-02	4.67e-03	6.88e-03	5.43e-04	0.00e+00	3.89e-03	2.67e-04	4.36e-04	5.85e-03	2.34e-03	5.52e-02		5.52e-02
1	809862	803822	140176	127955	3.30e-03	3.00e-02	3.44e-03	2.88e-03	7.76e-04	0.00e+00	4.77e-03	2.18e-04	5.27e-04	4.02e-03	3.11e-03	5.30e-02		1.08e-01
2	800068	792824	139162	145202	3.25e-03	5.16e-02	4.44e-03	2.94e-03	3.98e-04	0.00e+00	5.86e-03	3.31e-04	5.25e-04	3.27e-03	3.22e-03	7.58e-02		1.84e-01
3	799890	811006	149762	157006	3.21e-03	6.32e-02	4.80e-03	1.40e-03	5.76e-04	0.00e+00	6.83e-03	3.77e-04	6.58e-04	3.22e-03	3.09e-03	8.74e-02		2.72e-01
4	808536	805757	150746	139630	3.28e-03	6.20e-02	5.21e-03	1.66e-03	8.64e-04	0.00e+00	7.88e-03	4.20e-04	6.73e-04	3.59e-03	2.99e-03	8.86e-02		3.60e-01
5	813435	810894	142905	145684	3.24e-03	5.97e-02	5.18e-03	1.89e-03	7.99e-04	0.00e+00	7.24e-03	2.82e-04	6.66e-04	3.59e-03	3.03e-03	8.56e-02		4.46e-01
6	811453	810254	138812	141353	3.28e-03	6.41e-02	5.25e-03	1.58e-03	7.43e-04	0.00e+00	7.45e-03	4.88e-04	6.63e-04	3.42e-03	3.05e-03	9.00e-02		5.36e-01
7	811584	809032	141600	142799	3.28e-03	6.22e-02	5.24e-03	1.52e-03	8.25e-04	0.00e+00	7.44e-03	3.56e-04	6.94e-04	3.33e-03	3.09e-03	8.79e-02		6.24e-01
8	813029	812684	142275	144827	3.26e-03	6.38e-02	5.25e-03	1.54e-03	8.87e-04	0.00e+00	7.24e-03	4.07e-04	6.85e-04	3.41e-03	3.08e-03	8.96e-02		7.13e-01
9	813961	816028	141636	141981	3.33e-03	6.31e-02	5.31e-03	1.62e-03	1.24e-03	0.00e+00	7.44e-03	3.73e-04	6.85e-04	3.26e-03	3.13e-03	8.95e-02		8.03e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.1739 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.1739 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.173909  0.000000
];

