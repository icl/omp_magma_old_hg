% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_73 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	797224	811446	141586	141586	5.05e-03	2.93e-02	6.38e-03	8.83e-03	6.29e-04	0.00e+00	3.56e-03	4.45e-04	5.51e-04	6.18e-03	2.62e-03	6.35e-02		6.35e-02
1	813606	799467	150994	136772	5.06e-03	2.86e-02	5.06e-03	2.73e-03	6.27e-04	0.00e+00	4.05e-03	3.23e-04	5.01e-04	4.35e-03	2.76e-03	5.41e-02		1.18e-01
2	813460	782449	135418	149557	5.07e-03	5.02e-02	5.53e-03	2.79e-03	4.17e-04	0.00e+00	4.94e-03	4.39e-04	5.08e-04	3.46e-03	2.85e-03	7.62e-02		1.94e-01
3	801021	812289	136370	167381	4.91e-03	5.67e-02	5.76e-03	2.07e-03	6.76e-04	0.00e+00	5.57e-03	3.58e-04	5.78e-04	3.42e-03	3.00e-03	8.31e-02		2.77e-01
4	815033	802794	149615	138347	5.10e-03	5.96e-02	6.37e-03	1.65e-03	6.49e-04	0.00e+00	6.17e-03	3.83e-04	6.21e-04	3.67e-03	3.03e-03	8.72e-02		3.64e-01
5	811136	809327	136408	148647	5.03e-03	5.62e-02	6.75e-03	3.24e-03	1.22e-03	0.00e+00	6.49e-03	4.71e-04	6.17e-04	3.68e-03	3.13e-03	8.69e-02		4.51e-01
6	808739	809761	141111	142920	5.05e-03	5.82e-02	6.77e-03	1.72e-03	6.76e-04	0.00e+00	6.25e-03	4.17e-04	6.34e-04	3.47e-03	3.07e-03	8.63e-02		5.37e-01
7	814550	811816	144314	143292	5.10e-03	5.60e-02	6.56e-03	1.37e-03	5.85e-04	0.00e+00	6.17e-03	5.14e-04	5.91e-04	3.60e-03	3.15e-03	8.37e-02		6.21e-01
8	810134	811438	139309	142043	5.10e-03	5.87e-02	6.81e-03	2.48e-03	1.27e-03	0.00e+00	6.39e-03	3.83e-04	6.28e-04	3.58e-03	3.08e-03	8.84e-02		7.09e-01
9	812332	812429	144531	143227	5.08e-03	5.67e-02	6.70e-03	1.56e-03	1.01e-03	0.00e+00	6.19e-03	3.79e-04	6.22e-04	3.47e-03	3.07e-03	8.48e-02		7.94e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.2677 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.2677 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.267669  0.000000
];

