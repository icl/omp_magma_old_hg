% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_96 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	814016	806223	141586	141586	6.04e-03	3.89e-02	9.11e-03	1.17e-02	6.11e-04	0.00e+00	3.86e-03	3.19e-04	4.75e-04	1.25e-02	2.34e-03	8.59e-02		8.59e-02
1	802285	809328	134202	141995	5.98e-03	3.57e-02	4.00e-03	2.95e-03	8.15e-04	0.00e+00	4.63e-03	3.24e-04	4.91e-04	7.36e-03	3.14e-03	6.53e-02		1.51e-01
2	819770	811937	146739	139696	6.04e-03	6.15e-02	5.14e-03	1.86e-03	5.27e-04	0.00e+00	5.51e-03	4.17e-04	6.13e-04	3.48e-03	3.47e-03	8.86e-02		2.40e-01
3	824625	807443	130060	137893	6.05e-03	7.49e-02	6.14e-03	1.69e-03	6.35e-04	0.00e+00	6.20e-03	3.33e-04	7.78e-04	3.47e-03	3.29e-03	1.04e-01		3.43e-01
4	796806	808466	126011	143193	6.04e-03	7.18e-02	6.10e-03	2.14e-03	6.09e-04	0.00e+00	6.63e-03	3.87e-04	7.30e-04	4.27e-03	3.04e-03	1.02e-01		4.45e-01
5	811071	811570	154635	142975	6.12e-03	5.92e-02	5.67e-03	2.29e-03	1.34e-03	0.00e+00	6.71e-03	4.74e-04	6.98e-04	3.65e-03	3.16e-03	8.93e-02		5.34e-01
6	807401	814069	141176	140677	6.07e-03	6.77e-02	5.83e-03	2.26e-03	6.96e-04	0.00e+00	6.78e-03	4.70e-04	6.82e-04	3.60e-03	3.14e-03	9.73e-02		6.32e-01
7	815883	811448	145652	138984	6.08e-03	6.33e-02	5.88e-03	1.81e-03	7.02e-04	0.00e+00	6.75e-03	4.40e-04	6.92e-04	3.36e-03	3.14e-03	9.22e-02		7.24e-01
8	808663	811469	137976	142411	6.18e-03	6.89e-02	5.95e-03	1.55e-03	1.22e-03	0.00e+00	6.84e-03	5.10e-04	6.72e-04	3.60e-03	3.13e-03	9.86e-02		8.22e-01
9	811215	812462	146002	143196	6.05e-03	6.41e-02	5.89e-03	1.55e-03	1.13e-03	0.00e+00	6.79e-03	3.84e-04	6.92e-04	3.55e-03	3.20e-03	9.33e-02		9.16e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.4328 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.4328 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.432775  0.000000
];

