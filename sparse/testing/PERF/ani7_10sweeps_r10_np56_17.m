% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_56 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	817811	802898	141586	141586	3.24e-03	2.83e-02	4.95e-03	6.76e-03	5.10e-04	0.00e+00	4.29e-03	2.79e-04	4.77e-04	5.64e-03	2.51e-03	5.69e-02		5.69e-02
1	799755	788515	130407	145320	3.22e-03	3.16e-02	3.77e-03	1.78e-03	5.36e-04	0.00e+00	5.20e-03	2.57e-04	5.32e-04	4.24e-03	3.40e-03	5.45e-02		1.11e-01
2	800373	801444	149269	160509	3.20e-03	5.27e-02	4.80e-03	1.47e-03	4.82e-04	0.00e+00	6.32e-03	2.73e-04	6.09e-04	3.42e-03	3.58e-03	7.69e-02		1.88e-01
3	804078	819794	149457	148386	3.25e-03	6.79e-02	5.54e-03	1.69e-03	6.76e-04	0.00e+00	7.44e-03	3.48e-04	6.54e-04	3.40e-03	3.57e-03	9.45e-02		2.83e-01
4	806892	807060	146558	130842	3.34e-03	6.96e-02	8.77e-03	2.70e-03	8.59e-04	0.00e+00	1.02e-02	3.16e-04	8.79e-04	3.91e-03	4.53e-03	1.05e-01		3.88e-01
5	809754	810600	144549	144381	5.30e-03	7.40e-02	8.65e-03	2.85e-03	1.54e-03	0.00e+00	1.06e-02	1.92e-04	8.58e-04	3.54e-03	4.54e-03	1.12e-01		5.00e-01
6	810639	811625	142493	141647	5.44e-03	7.64e-02	8.66e-03	1.78e-03	7.96e-04	0.00e+00	1.05e-02	3.11e-04	7.84e-04	3.59e-03	4.56e-03	1.13e-01		6.13e-01
7	810431	810872	142414	141428	5.37e-03	6.74e-02	5.81e-03	1.82e-03	7.72e-04	0.00e+00	8.14e-03	3.78e-04	7.26e-04	3.42e-03	3.36e-03	9.72e-02		7.10e-01
8	807345	813062	143428	142987	3.28e-03	6.79e-02	5.84e-03	1.97e-03	9.51e-04	0.00e+00	8.06e-03	4.34e-04	6.91e-04	3.53e-03	3.34e-03	9.60e-02		8.06e-01
9	815086	810486	147320	141603	3.30e-03	6.64e-02	5.89e-03	1.51e-03	1.08e-03	0.00e+00	7.74e-03	3.90e-04	7.01e-04	3.39e-03	3.41e-03	9.38e-02		9.00e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.2704 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.2704 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.270433  0.000000
];

