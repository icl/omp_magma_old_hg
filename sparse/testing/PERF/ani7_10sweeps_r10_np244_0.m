% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_244 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	823685	827976	141586	141586	1.09e-02	4.41e-02	9.05e-03	2.02e-02	2.80e-03	0.00e+00	2.26e-03	4.91e-04	3.36e-04	1.24e-02	1.73e-03	1.04e-01		1.04e-01
1	820892	802767	124533	120242	1.11e-02	3.62e-02	4.76e-03	4.60e-03	2.33e-03	0.00e+00	2.95e-03	4.47e-04	3.66e-04	7.12e-03	1.87e-03	7.18e-02		1.76e-01
2	814121	810407	128132	146257	1.07e-02	5.16e-02	3.66e-03	1.80e-03	1.27e-03	0.00e+00	3.25e-03	5.33e-04	3.73e-04	4.21e-03	1.90e-03	7.93e-02		2.55e-01
3	811122	814472	135709	139423	1.08e-02	5.17e-02	4.21e-03	2.78e-03	2.93e-04	0.00e+00	3.45e-03	5.48e-04	4.29e-04	4.20e-03	2.04e-03	8.04e-02		3.36e-01
4	816949	808089	139514	136164	1.09e-02	5.68e-02	4.21e-03	2.35e-03	5.29e-04	0.00e+00	3.70e-03	5.55e-04	4.73e-04	4.30e-03	2.12e-03	8.59e-02		4.22e-01
5	811669	810129	134492	143352	1.08e-02	4.91e-02	4.32e-03	2.76e-03	4.48e-04	0.00e+00	3.63e-03	4.11e-04	4.06e-04	5.07e-03	2.08e-03	7.90e-02		5.01e-01
6	812632	809584	140578	142118	1.08e-02	5.42e-02	4.33e-03	1.55e-03	4.52e-04	0.00e+00	3.68e-03	6.34e-04	3.97e-04	4.57e-03	2.11e-03	8.27e-02		5.83e-01
7	809767	814307	140421	143469	1.08e-02	4.87e-02	4.34e-03	1.98e-03	3.46e-04	0.00e+00	3.66e-03	8.43e-04	4.05e-04	4.56e-03	2.15e-03	7.78e-02		6.61e-01
8	812202	812755	144092	139552	1.09e-02	5.53e-02	4.42e-03	1.72e-03	1.44e-03	0.00e+00	3.53e-03	7.58e-04	4.22e-04	4.69e-03	2.12e-03	8.52e-02		7.46e-01
9	811695	813473	142463	141910	1.08e-02	4.90e-02	4.43e-03	1.58e-03	1.43e-03	0.00e+00	3.70e-03	9.43e-04	3.98e-04	4.19e-03	2.16e-03	7.87e-02		8.25e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.5790 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.5790 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.579044  0.000000
];

