% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_136 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	815648	809950	141586	141586	6.18e-03	3.00e-02	5.80e-03	1.27e-02	4.49e-04	0.00e+00	2.72e-03	3.44e-04	3.67e-04	7.61e-03	1.93e-03	6.81e-02		6.81e-02
1	799856	793863	132570	138268	6.03e-03	2.80e-02	3.33e-03	5.69e-03	5.82e-04	0.00e+00	3.23e-03	3.05e-04	4.05e-04	5.57e-03	2.28e-03	5.54e-02		1.24e-01
2	804429	801727	149168	155161	5.92e-03	4.45e-02	3.64e-03	1.60e-03	3.17e-04	0.00e+00	3.78e-03	4.36e-04	4.14e-04	3.23e-03	2.30e-03	6.61e-02		1.90e-01
3	810510	811785	145401	148103	5.98e-03	4.86e-02	4.09e-03	3.60e-03	3.80e-04	0.00e+00	4.38e-03	2.86e-04	4.91e-04	3.53e-03	2.32e-03	7.36e-02		2.63e-01
4	812977	813667	140126	138851	6.04e-03	5.09e-02	4.16e-03	1.97e-03	4.48e-04	0.00e+00	4.65e-03	3.82e-04	5.23e-04	3.93e-03	2.18e-03	7.51e-02		3.38e-01
5	809444	805969	138464	137774	6.08e-03	4.64e-02	4.20e-03	1.94e-03	5.94e-04	0.00e+00	4.54e-03	4.12e-04	4.91e-04	3.92e-03	2.19e-03	7.07e-02		4.09e-01
6	809442	813066	142803	146278	6.03e-03	4.87e-02	4.17e-03	4.45e-03	5.53e-04	0.00e+00	4.77e-03	4.94e-04	5.25e-04	3.62e-03	2.20e-03	7.55e-02		4.85e-01
7	812597	810328	143611	139987	6.09e-03	4.67e-02	4.27e-03	1.79e-03	4.80e-04	0.00e+00	4.60e-03	6.49e-04	5.16e-04	3.40e-03	2.21e-03	7.07e-02		5.55e-01
8	816569	811146	141262	143531	6.04e-03	4.93e-02	4.25e-03	2.65e-03	1.04e-03	0.00e+00	4.69e-03	5.86e-04	4.81e-04	3.38e-03	2.24e-03	7.47e-02		6.30e-01
9	811494	815057	138096	143519	6.05e-03	4.79e-02	4.31e-03	1.50e-03	1.23e-03	0.00e+00	4.58e-03	5.49e-04	5.04e-04	3.08e-03	2.22e-03	7.20e-02		7.02e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.2331 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.2331 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.233131  0.000000
];

