% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_240 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	812729	813972	141586	141586	1.20e-02	6.60e-02	1.67e-02	2.53e-02	8.96e-04	0.00e+00	2.95e-03	5.00e-04	3.52e-04	1.62e-02	1.65e-03	1.43e-01		1.43e-01
1	799268	786156	135489	134246	1.21e-02	4.79e-02	3.37e-03	9.17e-03	9.83e-04	0.00e+00	3.43e-03	4.80e-04	3.43e-04	9.86e-03	2.19e-03	8.98e-02		2.32e-01
2	804394	800495	149756	162868	1.17e-02	6.90e-02	4.02e-03	4.36e-03	6.69e-04	0.00e+00	3.83e-03	6.30e-04	4.72e-04	4.40e-03	2.31e-03	1.01e-01		3.34e-01
3	807677	812959	145436	149335	1.19e-02	6.75e-02	4.47e-03	3.69e-03	7.18e-04	0.00e+00	4.25e-03	7.02e-04	4.69e-04	4.48e-03	2.34e-03	1.01e-01		4.34e-01
4	799585	807999	142959	137677	1.22e-02	7.76e-02	4.64e-03	3.31e-03	6.26e-04	0.00e+00	4.51e-03	8.47e-04	5.25e-04	5.62e-03	2.21e-03	1.12e-01		5.46e-01
5	808248	809049	151856	143442	1.20e-02	6.48e-02	4.52e-03	4.02e-03	1.53e-03	0.00e+00	4.49e-03	4.77e-04	4.92e-04	5.32e-03	2.32e-03	1.00e-01		6.46e-01
6	812133	812148	143999	143198	1.21e-02	7.60e-02	4.57e-03	3.40e-03	5.20e-04	0.00e+00	4.44e-03	7.12e-04	4.93e-04	4.98e-03	2.29e-03	1.09e-01		7.56e-01
7	810238	813903	140920	140905	1.21e-02	6.74e-02	4.68e-03	2.28e-03	5.33e-04	0.00e+00	4.58e-03	6.42e-04	4.88e-04	4.65e-03	2.32e-03	9.96e-02		8.55e-01
8	813265	814187	143621	139956	1.21e-02	7.75e-02	4.69e-03	2.64e-03	1.67e-03	0.00e+00	4.43e-03	1.04e-03	4.89e-04	4.59e-03	2.35e-03	1.12e-01		9.67e-01
9	810669	812548	141400	140478	1.21e-02	6.90e-02	4.76e-03	2.05e-03	1.78e-03	0.00e+00	4.65e-03	5.95e-04	4.74e-04	4.48e-03	2.29e-03	1.02e-01		1.07e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.8239 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.8239 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.823851  0.000000
];

