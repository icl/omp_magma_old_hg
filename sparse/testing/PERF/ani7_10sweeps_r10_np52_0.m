% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_52 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	818154	803335	141586	141586	3.23e-03	2.87e-02	5.12e-03	6.78e-03	8.78e-04	0.00e+00	4.62e-03	2.76e-04	5.13e-04	5.68e-03	2.74e-03	5.86e-02		5.86e-02
1	791033	805715	130064	144883	3.25e-03	3.40e-02	4.09e-03	2.71e-03	1.00e-03	0.00e+00	5.59e-03	2.13e-04	7.33e-04	4.09e-03	3.59e-03	5.92e-02		1.18e-01
2	801191	814265	157991	143309	3.26e-03	5.44e-02	5.18e-03	1.94e-03	7.85e-04	0.00e+00	6.76e-03	2.81e-04	6.11e-04	3.47e-03	3.86e-03	8.06e-02		1.98e-01
3	809476	792232	148639	135565	3.30e-03	7.37e-02	6.15e-03	2.00e-03	5.68e-04	0.00e+00	7.98e-03	1.90e-04	7.26e-04	3.46e-03	3.54e-03	1.02e-01		3.00e-01
4	810362	807390	141160	158404	3.23e-03	6.84e-02	6.00e-03	2.09e-03	8.85e-04	0.00e+00	8.56e-03	3.99e-04	7.61e-04	3.41e-03	3.56e-03	9.73e-02		3.97e-01
5	801457	812658	141079	144051	3.28e-03	6.76e-02	6.16e-03	1.74e-03	8.43e-04	0.00e+00	8.72e-03	3.13e-04	7.55e-04	3.87e-03	3.55e-03	9.68e-02		4.94e-01
6	814427	814671	150790	139589	3.30e-03	6.82e-02	6.15e-03	1.59e-03	7.72e-04	0.00e+00	8.55e-03	2.39e-04	7.16e-04	3.55e-03	3.61e-03	9.67e-02		5.91e-01
7	812190	808582	138626	138382	3.34e-03	7.01e-02	6.31e-03	2.75e-03	8.14e-04	0.00e+00	8.79e-03	3.95e-04	7.52e-04	3.71e-03	3.61e-03	1.01e-01		6.91e-01
8	814746	813556	141669	145277	3.27e-03	7.02e-02	6.27e-03	1.80e-03	1.26e-03	0.00e+00	8.54e-03	3.21e-04	7.46e-04	3.53e-03	3.67e-03	9.96e-02		7.91e-01
9	811183	815530	139919	141109	3.30e-03	7.05e-02	6.34e-03	1.67e-03	1.26e-03	0.00e+00	8.52e-03	3.88e-04	7.54e-04	3.42e-03	3.63e-03	9.98e-02		8.91e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.2618 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.2618 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.261804  0.000000
];

