% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_300 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	792913	810878	141586	141586	1.56e-02	7.33e-02	1.82e-02	1.97e-02	1.28e-02	0.00e+00	3.03e-03	8.31e-04	1.74e-03	2.85e-02	2.35e-03	1.76e-01		1.76e-01
1	815581	822751	155305	137340	1.56e-02	4.70e-02	4.04e-03	5.18e-03	1.07e-02	0.00e+00	3.00e-03	7.14e-04	1.62e-03	7.02e-03	2.53e-03	9.74e-02		2.73e-01
2	800470	801822	133443	126273	1.56e-02	6.49e-02	4.58e-03	5.89e-03	1.13e-03	0.00e+00	3.21e-03	7.76e-04	1.61e-03	3.86e-03	2.71e-03	1.04e-01		3.78e-01
3	814148	799264	149360	148008	1.55e-02	5.45e-02	4.73e-03	4.66e-03	1.11e-03	0.00e+00	3.55e-03	7.20e-04	1.55e-03	3.86e-03	2.70e-03	9.29e-02		4.70e-01
4	812072	808541	136488	151372	1.53e-02	6.91e-02	4.79e-03	5.11e-03	1.11e-03	0.00e+00	3.79e-03	8.92e-04	1.68e-03	5.17e-03	2.56e-03	1.09e-01		5.80e-01
5	811821	803151	139369	142900	1.47e-02	5.77e-02	4.88e-03	4.83e-03	1.24e-03	0.00e+00	4.06e-03	8.18e-04	1.60e-03	4.64e-03	2.60e-03	9.71e-02		6.77e-01
6	808942	807808	140426	149096	1.44e-02	7.14e-02	4.85e-03	5.97e-03	1.10e-03	0.00e+00	3.64e-03	7.98e-04	1.71e-03	4.05e-03	2.55e-03	1.10e-01		7.87e-01
7	812160	810706	144111	145245	1.55e-02	5.60e-02	5.19e-03	4.77e-03	1.13e-03	0.00e+00	3.78e-03	8.89e-04	1.62e-03	3.88e-03	2.68e-03	9.54e-02		8.83e-01
8	808198	813535	141699	143153	1.54e-02	7.14e-02	5.06e-03	4.61e-03	1.12e-03	0.00e+00	3.56e-03	8.24e-04	1.62e-03	3.77e-03	2.64e-03	1.10e-01		9.93e-01
9	814405	812404	146467	141130	1.47e-02	5.71e-02	5.07e-03	4.37e-03	2.10e-03	0.00e+00	3.60e-03	8.24e-04	1.57e-03	3.75e-03	2.71e-03	9.57e-02		1.09e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.6090 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.6090 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.608960  0.000000
];

