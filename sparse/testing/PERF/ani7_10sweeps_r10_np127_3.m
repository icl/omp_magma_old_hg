% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_127 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	818384	802777	141586	141586	5.93e-03	2.80e-02	5.76e-03	1.06e-02	5.64e-04	0.00e+00	2.80e-03	4.39e-04	3.53e-04	6.94e-03	1.77e-03	6.31e-02		6.31e-02
1	809292	807276	129834	145441	5.91e-03	2.72e-02	3.07e-03	4.33e-03	6.03e-04	0.00e+00	3.40e-03	3.00e-04	3.85e-04	4.31e-03	2.09e-03	5.16e-02		1.15e-01
2	813325	814770	139732	141748	5.97e-03	3.72e-02	3.72e-03	2.18e-03	2.77e-04	0.00e+00	4.09e-03	6.09e-04	4.16e-04	3.07e-03	2.38e-03	5.99e-02		1.75e-01
3	809898	813285	136505	135060	5.99e-03	4.63e-02	4.28e-03	1.40e-03	3.38e-04	0.00e+00	4.73e-03	4.97e-04	4.88e-04	3.00e-03	2.33e-03	6.93e-02		2.44e-01
4	806771	807425	140738	137351	5.96e-03	4.73e-02	4.43e-03	1.43e-03	4.59e-04	0.00e+00	4.66e-03	3.97e-04	4.71e-04	3.67e-03	2.22e-03	7.10e-02		3.15e-01
5	817758	808665	144670	144016	5.92e-03	4.46e-02	4.33e-03	1.78e-03	9.51e-04	0.00e+00	4.67e-03	3.52e-04	4.54e-04	3.47e-03	2.30e-03	6.88e-02		3.84e-01
6	810192	812480	134489	143582	5.96e-03	4.92e-02	4.49e-03	1.41e-03	4.75e-04	0.00e+00	4.93e-03	3.81e-04	4.95e-04	3.39e-03	2.29e-03	7.30e-02		4.57e-01
7	813838	812097	142861	140573	5.96e-03	4.72e-02	4.50e-03	1.35e-03	4.64e-04	0.00e+00	4.76e-03	4.54e-04	4.96e-04	3.19e-03	2.42e-03	7.08e-02		5.28e-01
8	811906	812401	140021	141762	5.97e-03	5.40e-02	4.53e-03	1.91e-03	7.95e-04	0.00e+00	4.71e-03	4.21e-04	4.53e-04	3.15e-03	2.30e-03	7.82e-02		6.06e-01
9	816197	813341	142759	142264	5.99e-03	4.76e-02	4.52e-03	1.71e-03	9.99e-04	0.00e+00	4.91e-03	6.24e-04	4.58e-04	2.98e-03	2.32e-03	7.21e-02		6.78e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.1604 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.1604 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.160362  0.000000
];

