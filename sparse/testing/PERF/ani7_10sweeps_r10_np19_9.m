% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_19 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	809777	809634	141586	141586	3.74e-03	6.17e-02	1.23e-02	7.73e-03	1.02e-03	0.00e+00	1.33e-02	3.14e-04	1.34e-03	7.88e-03	7.74e-03	1.17e-01		1.17e-01
1	810753	797179	138441	138584	3.75e-03	7.62e-02	1.17e-02	4.25e-03	1.35e-03	0.00e+00	1.62e-02	2.36e-04	1.49e-03	6.61e-03	9.94e-03	1.32e-01		2.49e-01
2	809966	812317	138271	151845	3.75e-03	1.22e-01	1.51e-02	3.03e-03	1.73e-03	0.00e+00	1.99e-02	3.80e-04	1.57e-03	6.21e-03	1.09e-02	1.85e-01		4.34e-01
3	806738	813852	139864	137513	3.83e-03	1.74e-01	1.72e-02	3.32e-03	1.40e-03	0.00e+00	2.27e-02	2.35e-04	1.78e-03	6.10e-03	1.02e-02	2.41e-01		6.74e-01
4	803077	804646	143898	136784	3.82e-03	1.66e-01	1.74e-02	3.78e-03	2.32e-03	0.00e+00	2.46e-02	3.84e-04	1.90e-03	6.30e-03	9.78e-03	2.36e-01		9.10e-01
5	808783	811267	148364	146795	3.78e-03	1.60e-01	1.72e-02	3.52e-03	2.31e-03	0.00e+00	2.53e-02	3.87e-04	1.87e-03	6.73e-03	1.00e-02	2.31e-01		1.14e+00
6	810860	811413	143464	140980	3.85e-03	1.67e-01	1.76e-02	3.60e-03	1.93e-03	0.00e+00	2.49e-02	3.83e-04	1.84e-03	6.24e-03	1.01e-02	2.37e-01		1.38e+00
7	807443	809521	142193	141640	3.80e-03	1.73e-01	1.78e-02	3.32e-03	1.76e-03	0.00e+00	2.44e-02	4.28e-04	1.79e-03	6.10e-03	1.00e-02	2.42e-01		1.62e+00
8	812797	812245	146416	144338	3.81e-03	1.71e-01	1.78e-02	3.62e-03	2.33e-03	0.00e+00	2.55e-02	3.39e-04	1.88e-03	6.44e-03	1.02e-02	2.43e-01		1.86e+00
9	813893	813159	141868	142420	3.86e-03	1.75e-01	1.80e-02	3.80e-03	2.33e-03	0.00e+00	2.54e-02	5.14e-04	1.86e-03	6.13e-03	1.02e-02	2.47e-01		2.11e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 2.5197 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 2.5197 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  2.519744  0.000000
];

