% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_1 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	812061	804758	141586	141586	9.91e-03	9.27e-01	2.14e-01	4.17e-02	1.30e-02	0.00e+00	1.81e-01	3.10e-04	2.20e-02	7.13e-02	1.35e-01	1.61e+00		1.61e+00
1	811011	809745	136157	143460	1.10e-02	1.07e+00	2.13e-01	3.87e-02	1.61e-02	0.00e+00	2.04e-01	2.04e-04	2.30e-02	6.77e-02	1.55e-01	1.80e+00		3.41e+00
2	802627	819076	138013	139279	1.12e-02	1.38e+00	2.51e-01	3.88e-02	1.49e-02	0.00e+00	2.24e-01	2.37e-04	2.24e-02	6.40e-02	1.62e-01	2.17e+00		5.58e+00
3	806253	805439	147203	130754	1.11e-02	1.65e+00	2.69e-01	4.14e-02	1.70e-02	0.00e+00	2.42e-01	2.56e-04	2.31e-02	6.36e-02	1.56e-01	2.47e+00		8.05e+00
4	812132	807184	144383	145197	1.08e-02	1.68e+00	2.81e-01	4.24e-02	1.95e-02	0.00e+00	2.46e-01	4.32e-04	2.32e-02	6.38e-02	1.57e-01	2.53e+00		1.06e+01
5	809108	806956	139309	144257	1.09e-02	1.80e+00	2.90e-01	4.32e-02	1.91e-02	0.00e+00	2.48e-01	3.12e-04	2.32e-02	6.54e-02	1.57e-01	2.66e+00		1.32e+01
6	811482	813680	143139	145291	1.09e-02	1.80e+00	2.90e-01	4.19e-02	1.91e-02	0.00e+00	2.47e-01	3.19e-04	2.31e-02	6.47e-02	1.59e-01	2.66e+00		1.59e+01
7	810722	812806	141571	139373	1.10e-02	1.85e+00	2.96e-01	4.38e-02	2.06e-02	0.00e+00	2.56e-01	4.11e-04	2.39e-02	6.38e-02	1.58e-01	2.73e+00		1.86e+01
8	814078	814640	143137	141053	1.09e-02	1.85e+00	2.96e-01	4.18e-02	1.84e-02	0.00e+00	2.44e-01	3.24e-04	2.29e-02	6.49e-02	1.59e-01	2.70e+00		2.13e+01
9	813416	813074	140587	140025	1.10e-02	1.88e+00	2.98e-01	4.17e-02	1.91e-02	0.00e+00	2.46e-01	4.67e-04	2.29e-02	6.35e-02	1.59e-01	2.74e+00		2.41e+01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 25.3991 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 25.3991 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  25.399097  0.000000
];

