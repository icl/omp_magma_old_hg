% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_99 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	826668	806814	141586	141586	5.57e-03	2.94e-02	5.98e-03	9.87e-03	1.48e-03	0.00e+00	2.70e-03	3.88e-04	4.20e-04	6.75e-03	2.24e-03	6.48e-02		6.48e-02
1	809063	806727	121550	141404	5.59e-03	2.92e-02	4.01e-03	3.77e-03	1.22e-03	0.00e+00	3.18e-03	3.18e-04	4.53e-04	4.30e-03	2.41e-03	5.45e-02		1.19e-01
2	807542	818955	139961	142297	5.52e-03	4.59e-02	4.37e-03	3.06e-03	6.40e-04	0.00e+00	3.75e-03	3.65e-04	4.56e-04	3.34e-03	2.84e-03	7.03e-02		1.90e-01
3	813850	808003	142288	130875	5.64e-03	5.31e-02	4.81e-03	1.79e-03	3.56e-04	0.00e+00	4.13e-03	3.12e-04	5.17e-04	3.13e-03	2.80e-03	7.66e-02		2.66e-01
4	802217	809728	136786	142633	5.56e-03	5.13e-02	5.27e-03	1.63e-03	5.05e-04	0.00e+00	4.68e-03	5.47e-04	5.19e-04	3.18e-03	2.69e-03	7.58e-02		3.42e-01
5	809777	808475	149224	141713	5.57e-03	4.69e-02	5.29e-03	1.91e-03	4.69e-04	0.00e+00	4.60e-03	4.86e-04	5.03e-04	3.81e-03	2.71e-03	7.22e-02		4.14e-01
6	809711	808715	142470	143772	5.56e-03	5.05e-02	5.39e-03	1.89e-03	4.75e-04	0.00e+00	4.70e-03	5.06e-04	5.14e-04	3.25e-03	2.70e-03	7.55e-02		4.90e-01
7	808868	813249	143342	144338	5.56e-03	4.81e-02	5.42e-03	1.47e-03	5.08e-04	0.00e+00	4.62e-03	4.37e-04	5.20e-04	3.18e-03	2.68e-03	7.25e-02		5.62e-01
8	813170	814826	144991	140610	5.57e-03	5.13e-02	5.43e-03	1.19e-03	4.21e-04	0.00e+00	4.55e-03	5.20e-04	4.73e-04	3.29e-03	2.72e-03	7.55e-02		6.38e-01
9	813615	813626	141495	139839	5.61e-03	4.96e-02	5.56e-03	1.43e-03	1.09e-03	0.00e+00	4.63e-03	5.18e-04	4.86e-04	3.17e-03	2.73e-03	7.48e-02		7.13e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.1848 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.1848 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.184800  0.000000
];

