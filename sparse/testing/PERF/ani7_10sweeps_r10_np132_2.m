% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_132 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	811056	816477	141586	141586	5.95e-03	2.73e-02	6.13e-03	1.10e-02	1.14e-03	0.00e+00	2.78e-03	4.07e-04	3.39e-04	6.49e-03	1.72e-03	6.33e-02		6.33e-02
1	807184	802298	137162	131741	6.04e-03	2.76e-02	3.00e-03	1.60e-03	1.29e-03	0.00e+00	3.44e-03	2.57e-04	3.59e-04	4.31e-03	2.06e-03	5.00e-02		1.13e-01
2	807926	802052	141840	146726	5.94e-03	3.72e-02	3.67e-03	2.21e-03	6.68e-04	0.00e+00	3.94e-03	4.89e-04	3.96e-04	3.00e-03	2.34e-03	5.99e-02		1.73e-01
3	805269	806564	141904	147778	5.94e-03	4.73e-02	4.13e-03	2.15e-03	3.76e-04	0.00e+00	4.59e-03	4.35e-04	4.50e-04	2.93e-03	2.24e-03	7.05e-02		2.44e-01
4	802795	809922	145367	144072	5.98e-03	4.78e-02	4.24e-03	2.93e-03	4.69e-04	0.00e+00	4.64e-03	5.58e-04	4.52e-04	2.95e-03	2.19e-03	7.22e-02		3.16e-01
5	812172	811713	148646	141519	6.00e-03	4.46e-02	4.28e-03	2.47e-03	4.77e-04	0.00e+00	4.72e-03	2.70e-04	4.43e-04	3.37e-03	2.25e-03	6.89e-02		3.85e-01
6	807144	808446	140075	140534	6.02e-03	4.79e-02	4.34e-03	1.64e-03	4.31e-04	0.00e+00	4.70e-03	4.64e-04	4.70e-04	3.13e-03	2.19e-03	7.13e-02		4.56e-01
7	808721	817512	145909	144607	5.98e-03	4.56e-02	4.32e-03	1.29e-03	4.89e-04	0.00e+00	4.76e-03	4.02e-04	4.43e-04	3.32e-03	2.23e-03	6.89e-02		5.25e-01
8	811579	812386	145138	136347	6.07e-03	4.86e-02	4.39e-03	1.77e-03	1.10e-03	0.00e+00	4.70e-03	4.68e-04	4.75e-04	3.01e-03	2.25e-03	7.29e-02		5.98e-01
9	808815	812623	143086	142279	6.02e-03	4.67e-02	4.36e-03	2.67e-03	1.12e-03	0.00e+00	4.72e-03	4.71e-04	4.72e-04	3.01e-03	2.22e-03	7.18e-02		6.70e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.1525 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.1525 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.152535  0.000000
];

