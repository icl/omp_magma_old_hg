% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_101 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	811063	803046	141586	141586	5.65e-03	2.90e-02	6.01e-03	9.88e-03	1.03e-03	0.00e+00	2.67e-03	3.14e-04	4.20e-04	6.23e-03	2.14e-03	6.34e-02		6.34e-02
1	813083	798645	137155	145172	5.57e-03	2.79e-02	3.83e-03	1.53e-03	8.23e-04	0.00e+00	3.18e-03	2.70e-04	4.56e-04	4.27e-03	2.34e-03	5.02e-02		1.14e-01
2	812300	803435	135941	150379	5.52e-03	4.52e-02	4.40e-03	1.37e-03	5.96e-04	0.00e+00	3.68e-03	3.64e-04	4.79e-04	3.18e-03	2.53e-03	6.74e-02		1.81e-01
3	816570	816338	137530	146395	5.56e-03	4.84e-02	4.71e-03	2.04e-03	3.93e-04	0.00e+00	4.16e-03	4.66e-04	4.90e-04	3.13e-03	2.68e-03	7.21e-02		2.53e-01
4	811696	807749	134066	134298	5.63e-03	5.15e-02	5.09e-03	2.29e-03	7.65e-04	0.00e+00	4.77e-03	4.49e-04	5.95e-04	3.10e-03	2.69e-03	7.69e-02		3.30e-01
5	802888	805503	139745	143692	5.59e-03	4.52e-02	5.31e-03	1.60e-03	5.36e-04	0.00e+00	4.70e-03	4.06e-04	5.24e-04	3.55e-03	2.61e-03	7.00e-02		4.00e-01
6	811909	810841	149359	146744	5.58e-03	4.75e-02	5.19e-03	1.47e-03	4.90e-04	0.00e+00	4.51e-03	3.10e-04	4.71e-04	3.30e-03	2.68e-03	7.15e-02		4.71e-01
7	810802	813348	141144	142212	5.66e-03	4.65e-02	5.37e-03	1.95e-03	4.77e-04	0.00e+00	4.56e-03	4.17e-04	5.20e-04	3.38e-03	2.75e-03	7.15e-02		5.43e-01
8	811511	810936	143057	140511	5.64e-03	4.96e-02	5.45e-03	2.51e-03	9.45e-04	0.00e+00	4.63e-03	3.96e-04	5.21e-04	3.21e-03	2.69e-03	7.56e-02		6.19e-01
9	812046	811474	143154	143729	5.61e-03	4.70e-02	5.39e-03	1.46e-03	1.01e-03	0.00e+00	4.58e-03	3.91e-04	5.11e-04	3.12e-03	3.77e-03	7.28e-02		6.91e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.1725 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.1725 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.172492  0.000000
];

