% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_121 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	815459	816308	141586	141586	5.87e-03	2.85e-02	5.78e-03	1.05e-02	8.49e-04	0.00e+00	3.02e-03	3.46e-04	3.61e-04	7.08e-03	1.86e-03	6.42e-02		6.42e-02
1	799701	815086	132759	131910	6.01e-03	2.76e-02	3.28e-03	3.21e-03	2.27e-03	0.00e+00	3.40e-03	3.56e-04	4.22e-04	4.55e-03	2.18e-03	5.32e-02		1.17e-01
2	806648	808670	149323	133938	5.91e-03	3.87e-02	3.85e-03	2.31e-03	3.41e-04	0.00e+00	4.18e-03	2.86e-04	4.60e-04	3.09e-03	2.38e-03	6.16e-02		1.79e-01
3	810741	801203	143182	141160	5.91e-03	4.55e-02	4.29e-03	1.78e-03	3.38e-04	0.00e+00	4.99e-03	5.16e-04	4.59e-04	3.06e-03	2.33e-03	6.92e-02		2.48e-01
4	807347	811624	139895	149433	5.86e-03	4.64e-02	4.46e-03	1.43e-03	3.82e-04	0.00e+00	4.82e-03	3.83e-04	4.88e-04	3.39e-03	2.38e-03	7.00e-02		3.18e-01
5	811160	809842	144094	139817	5.92e-03	4.58e-02	4.54e-03	1.54e-03	4.83e-04	0.00e+00	4.87e-03	3.30e-04	4.99e-04	3.37e-03	2.34e-03	6.97e-02		3.88e-01
6	811391	810788	141087	142405	5.91e-03	4.87e-02	4.58e-03	2.24e-03	3.81e-04	0.00e+00	4.44e-03	6.26e-04	4.78e-04	3.37e-03	2.43e-03	7.32e-02		4.61e-01
7	813181	809294	141662	142265	5.92e-03	4.79e-02	4.66e-03	1.65e-03	4.03e-04	0.00e+00	4.97e-03	6.54e-04	4.67e-04	3.13e-03	2.41e-03	7.21e-02		5.33e-01
8	805965	813084	140678	144565	5.88e-03	5.02e-02	4.65e-03	2.19e-03	3.87e-04	0.00e+00	5.09e-03	5.59e-04	4.85e-04	3.18e-03	2.41e-03	7.51e-02		6.08e-01
9	815640	814166	148700	141581	5.94e-03	4.77e-02	4.63e-03	1.61e-03	9.80e-04	0.00e+00	5.06e-03	3.22e-04	4.78e-04	3.01e-03	2.43e-03	7.22e-02		6.80e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.1576 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.1576 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.157650  0.000000
];

