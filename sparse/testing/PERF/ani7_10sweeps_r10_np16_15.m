% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_16 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	823098	811683	141586	141586	3.84e-03	7.07e-02	1.45e-02	7.97e-03	1.43e-03	0.00e+00	1.56e-02	2.97e-04	1.59e-03	8.70e-03	9.25e-03	1.34e-01		1.34e-01
1	815315	821979	125120	136535	3.85e-03	8.76e-02	1.40e-02	4.83e-03	1.90e-03	0.00e+00	1.92e-02	2.58e-04	1.73e-03	7.51e-03	1.20e-02	1.53e-01		2.87e-01
2	819049	799466	133709	127045	3.98e-03	1.44e-01	1.86e-02	3.14e-03	1.52e-03	0.00e+00	2.39e-02	2.17e-04	1.86e-03	6.91e-03	1.27e-02	2.17e-01		5.04e-01
3	788040	808825	130781	150364	3.85e-03	2.07e-01	1.98e-02	3.88e-03	1.68e-03	0.00e+00	2.68e-02	2.91e-04	2.08e-03	6.84e-03	1.16e-02	2.84e-01		7.88e-01
4	812829	805231	162596	141811	3.92e-03	1.77e-01	1.97e-02	5.10e-03	2.46e-03	0.00e+00	2.85e-02	3.02e-04	2.14e-03	6.79e-03	1.16e-02	2.58e-01		1.05e+00
5	811663	811391	138612	146210	3.89e-03	1.92e-01	2.03e-02	5.03e-03	2.67e-03	0.00e+00	3.00e-02	3.52e-04	2.21e-03	7.38e-03	1.17e-02	2.76e-01		1.32e+00
6	811399	808678	140584	140856	3.93e-03	1.98e-01	2.07e-02	4.35e-03	2.22e-03	0.00e+00	2.90e-02	4.41e-04	2.16e-03	6.96e-03	1.17e-02	2.80e-01		1.60e+00
7	807916	810870	141654	144375	3.90e-03	1.99e-01	2.08e-02	3.53e-03	2.04e-03	0.00e+00	2.78e-02	3.49e-04	2.10e-03	7.26e-03	1.19e-02	2.78e-01		1.88e+00
8	812489	814438	145943	142989	3.92e-03	2.07e-01	2.09e-02	3.89e-03	2.55e-03	0.00e+00	2.81e-02	3.22e-04	2.07e-03	6.94e-03	1.20e-02	2.87e-01		2.17e+00
9	814385	812669	142176	140227	3.97e-03	2.08e-01	2.11e-02	3.96e-03	2.66e-03	0.00e+00	2.93e-02	3.12e-04	2.17e-03	6.90e-03	1.19e-02	2.90e-01		2.46e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 2.8736 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 2.8736 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  2.873643  0.000000
];

