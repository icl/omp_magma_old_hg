% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_10 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	821535	810516	141586	141586	4.16e-03	1.06e-01	2.26e-02	9.33e-03	2.22e-03	0.00e+00	2.45e-02	2.72e-04	2.43e-03	1.10e-02	1.46e-02	1.97e-01		1.97e-01
1	816757	797816	126683	137702	4.18e-03	1.34e-01	2.22e-02	5.55e-03	2.65e-03	0.00e+00	3.03e-02	2.50e-04	2.75e-03	1.04e-02	1.86e-02	2.31e-01		4.28e-01
2	804826	801642	132267	151208	4.24e-03	2.16e-01	2.82e-02	4.95e-03	2.26e-03	0.00e+00	3.57e-02	2.91e-04	3.01e-03	9.60e-03	2.00e-02	3.24e-01		7.52e-01
3	806836	805988	145004	148188	4.24e-03	3.04e-01	3.10e-02	5.42e-03	2.63e-03	0.00e+00	4.11e-02	3.05e-04	3.17e-03	9.46e-03	1.87e-02	4.20e-01		1.17e+00
4	811128	810382	143800	144648	4.27e-03	2.83e-01	3.25e-02	5.37e-03	3.51e-03	0.00e+00	4.48e-02	3.34e-04	3.32e-03	9.72e-03	1.83e-02	4.05e-01		1.58e+00
5	806358	806938	140313	141059	4.26e-03	2.95e-01	3.27e-02	5.77e-03	3.39e-03	0.00e+00	4.40e-02	2.46e-04	3.27e-03	1.04e-02	1.82e-02	4.17e-01		2.00e+00
6	813217	809963	145889	145309	4.26e-03	2.97e-01	3.25e-02	5.85e-03	3.43e-03	0.00e+00	4.41e-02	3.06e-04	3.27e-03	9.60e-03	1.86e-02	4.19e-01		2.41e+00
7	812767	813423	139836	143090	4.30e-03	3.08e-01	3.33e-02	5.54e-03	3.14e-03	0.00e+00	4.32e-02	3.91e-04	3.20e-03	9.59e-03	1.87e-02	4.29e-01		2.84e+00
8	813265	808310	141092	140436	4.28e-03	3.12e-01	3.36e-02	5.51e-03	3.19e-03	0.00e+00	4.38e-02	3.93e-04	3.23e-03	9.81e-03	1.86e-02	4.35e-01		3.28e+00
9	816423	813441	141400	146355	4.29e-03	3.09e-01	3.36e-02	5.46e-03	3.62e-03	0.00e+00	4.41e-02	5.41e-04	3.21e-03	9.61e-03	1.88e-02	4.32e-01		3.71e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 4.1611 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 4.1611 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  4.161130  0.000000
];

