% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_16 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	822977	811608	141586	141586	3.82e-03	7.07e-02	1.45e-02	7.97e-03	1.57e-03	0.00e+00	1.56e-02	2.98e-04	1.61e-03	8.32e-03	9.24e-03	1.34e-01		1.34e-01
1	810458	794144	125241	136610	3.87e-03	8.78e-02	1.40e-02	7.52e-03	1.99e-03	0.00e+00	1.91e-02	2.37e-04	1.83e-03	7.47e-03	1.17e-02	1.56e-01		2.89e-01
2	809520	803648	138566	154880	3.86e-03	1.43e-01	1.78e-02	3.03e-03	1.56e-03	0.00e+00	2.37e-02	3.86e-04	1.85e-03	6.82e-03	1.25e-02	2.15e-01		5.04e-01
3	801039	809499	140310	146182	3.89e-03	1.99e-01	1.99e-02	3.85e-03	1.69e-03	0.00e+00	2.67e-02	2.95e-04	2.07e-03	6.79e-03	1.18e-02	2.76e-01		7.80e-01
4	806562	809218	149597	141137	3.91e-03	1.84e-01	2.01e-02	4.44e-03	2.63e-03	0.00e+00	2.91e-02	3.84e-04	2.25e-03	6.79e-03	1.16e-02	2.66e-01		1.05e+00
5	810041	805714	144879	142223	3.91e-03	1.90e-01	2.02e-02	4.65e-03	2.43e-03	0.00e+00	2.92e-02	2.81e-04	2.16e-03	7.38e-03	1.16e-02	2.71e-01		1.32e+00
6	811848	808562	142206	146533	3.89e-03	1.97e-01	2.05e-02	4.17e-03	2.33e-03	0.00e+00	2.93e-02	2.83e-04	2.20e-03	6.99e-03	1.17e-02	2.79e-01		1.60e+00
7	812454	812615	141205	144491	3.91e-03	1.99e-01	2.08e-02	4.43e-03	2.27e-03	0.00e+00	2.91e-02	3.65e-04	2.16e-03	7.21e-03	1.20e-02	2.81e-01		1.88e+00
8	816393	809366	141405	141244	3.94e-03	2.08e-01	2.09e-02	3.58e-03	2.52e-03	0.00e+00	2.91e-02	4.03e-04	2.16e-03	6.97e-03	1.18e-02	2.90e-01		2.17e+00
9	814376	811530	138272	145299	3.93e-03	2.07e-01	2.12e-02	3.94e-03	2.63e-03	0.00e+00	2.95e-02	3.41e-04	2.16e-03	6.89e-03	1.19e-02	2.89e-01		2.46e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 2.8762 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 2.8762 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  2.876211  0.000000
];

