% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_131 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	814164	813282	141586	141586	6.87e-03	4.13e-02	1.03e-02	1.37e-02	1.28e-03	0.00e+00	3.08e-03	3.46e-04	3.79e-04	1.22e-02	2.14e-03	9.16e-02		9.16e-02
1	795225	830089	134054	134936	6.97e-03	3.53e-02	3.33e-03	3.19e-03	1.25e-03	0.00e+00	3.95e-03	3.36e-04	4.11e-04	7.62e-03	2.48e-03	6.48e-02		1.56e-01
2	806867	801293	153799	118935	7.32e-03	5.13e-02	4.11e-03	2.39e-03	8.74e-04	0.00e+00	4.46e-03	4.51e-04	4.20e-04	3.16e-03	2.56e-03	7.70e-02		2.33e-01
3	805760	804764	142963	148537	6.93e-03	5.56e-02	4.47e-03	1.93e-03	4.48e-04	0.00e+00	5.37e-03	3.95e-04	5.11e-04	3.25e-03	2.54e-03	8.15e-02		3.15e-01
4	803398	805250	144876	145872	6.75e-03	5.73e-02	4.38e-03	3.66e-03	8.14e-04	0.00e+00	5.39e-03	3.72e-04	5.64e-04	3.18e-03	2.19e-03	8.46e-02		3.99e-01
5	808102	811989	148043	146191	7.16e-03	5.35e-02	4.55e-03	2.05e-03	5.88e-04	0.00e+00	5.22e-03	4.48e-04	4.87e-04	3.81e-03	2.40e-03	8.02e-02		4.80e-01
6	812661	811137	144145	140258	7.14e-03	5.71e-02	4.39e-03	2.48e-03	5.43e-04	0.00e+00	5.47e-03	4.82e-04	5.32e-04	3.33e-03	2.30e-03	8.38e-02		5.63e-01
7	811839	812035	140392	141916	6.99e-03	5.47e-02	4.59e-03	2.36e-03	5.90e-04	0.00e+00	5.39e-03	4.85e-04	4.83e-04	3.18e-03	2.37e-03	8.11e-02		6.45e-01
8	808322	808489	142020	141824	7.17e-03	5.82e-02	4.63e-03	1.84e-03	1.38e-03	0.00e+00	5.53e-03	3.61e-04	4.92e-04	3.27e-03	2.40e-03	8.52e-02		7.30e-01
9	815125	811701	146343	146176	6.80e-03	5.44e-02	4.61e-03	1.48e-03	1.35e-03	0.00e+00	5.47e-03	5.62e-04	5.04e-04	3.14e-03	2.43e-03	8.07e-02		8.11e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.3352 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.3352 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.335224  0.000000
];

