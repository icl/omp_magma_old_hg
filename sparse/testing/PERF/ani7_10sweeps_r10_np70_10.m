% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_70 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	807789	806704	141586	141586	5.06e-03	3.01e-02	6.46e-03	8.62e-03	5.78e-04	0.00e+00	3.71e-03	3.52e-04	6.09e-04	5.79e-03	2.74e-03	6.40e-02		6.40e-02
1	791257	796305	140429	141514	5.06e-03	2.93e-02	5.25e-03	2.97e-03	6.36e-04	0.00e+00	4.18e-03	2.68e-04	5.59e-04	4.20e-03	2.79e-03	5.52e-02		1.19e-01
2	810521	808031	157767	152719	4.99e-03	5.05e-02	5.12e-03	1.76e-03	3.95e-04	0.00e+00	5.14e-03	5.31e-04	5.36e-04	3.52e-03	2.99e-03	7.54e-02		1.95e-01
3	809179	809623	139309	141799	5.08e-03	6.08e-02	5.43e-03	3.08e-03	4.98e-04	0.00e+00	5.98e-03	4.52e-04	6.00e-04	3.47e-03	2.84e-03	8.83e-02		2.83e-01
4	802347	809215	141457	141013	5.09e-03	6.21e-02	5.49e-03	2.82e-03	6.98e-04	0.00e+00	6.49e-03	5.11e-04	6.67e-04	3.94e-03	2.64e-03	9.05e-02		3.73e-01
5	809969	808682	149094	142226	5.07e-03	5.39e-02	5.28e-03	2.35e-03	1.14e-03	0.00e+00	6.57e-03	4.12e-04	6.67e-04	3.67e-03	2.69e-03	8.17e-02		4.55e-01
6	814809	809205	142278	143565	5.07e-03	5.73e-02	5.35e-03	2.48e-03	6.93e-04	0.00e+00	6.57e-03	4.08e-04	6.46e-04	3.54e-03	2.79e-03	8.48e-02		5.40e-01
7	812175	812833	138244	143848	5.06e-03	5.87e-02	5.41e-03	1.73e-03	8.37e-04	0.00e+00	6.44e-03	6.00e-04	6.18e-04	3.58e-03	2.80e-03	8.58e-02		6.26e-01
8	811097	812966	141684	141026	5.10e-03	5.95e-02	5.45e-03	2.16e-03	9.70e-04	0.00e+00	6.41e-03	4.96e-04	6.40e-04	3.56e-03	2.79e-03	8.71e-02		7.13e-01
9	814127	810227	143568	141699	5.09e-03	5.80e-02	5.49e-03	2.07e-03	1.13e-03	0.00e+00	6.56e-03	5.36e-04	6.50e-04	3.44e-03	2.78e-03	8.58e-02		7.99e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.2766 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.2766 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.276593  0.000000
];

