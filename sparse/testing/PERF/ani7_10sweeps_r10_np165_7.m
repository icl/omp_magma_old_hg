% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_165 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	820471	802177	141586	141586	8.12e-03	3.66e-02	7.56e-03	1.51e-02	5.68e-04	0.00e+00	2.32e-03	4.34e-04	3.70e-04	9.49e-03	2.02e-03	8.26e-02		8.26e-02
1	798741	819063	127747	146041	8.07e-03	3.08e-02	3.77e-03	3.89e-03	7.30e-04	0.00e+00	2.75e-03	3.88e-04	4.52e-04	5.57e-03	2.19e-03	5.86e-02		1.41e-01
2	809572	810073	150283	129961	8.22e-03	4.59e-02	4.20e-03	2.42e-03	3.60e-04	0.00e+00	3.27e-03	4.87e-04	4.42e-04	4.00e-03	2.34e-03	7.17e-02		2.13e-01
3	806188	821245	140258	139757	8.08e-03	4.73e-02	4.49e-03	2.29e-03	3.43e-04	0.00e+00	3.48e-03	3.97e-04	4.23e-04	3.94e-03	2.51e-03	7.33e-02		2.86e-01
4	806175	813956	144448	129391	8.21e-03	5.25e-02	4.85e-03	4.59e-03	4.11e-04	0.00e+00	3.85e-03	6.29e-04	4.50e-04	4.74e-03	2.41e-03	8.27e-02		3.69e-01
5	810174	810340	145266	137485	8.21e-03	4.58e-02	5.07e-03	2.72e-03	1.01e-03	0.00e+00	3.79e-03	5.36e-04	4.44e-04	4.52e-03	2.43e-03	7.45e-02		4.43e-01
6	811433	810015	142073	141907	8.06e-03	5.07e-02	5.06e-03	2.14e-03	4.28e-04	0.00e+00	3.79e-03	6.34e-04	4.49e-04	4.38e-03	2.48e-03	7.82e-02		5.21e-01
7	813157	815753	141620	143038	8.15e-03	4.67e-02	5.10e-03	1.66e-03	3.78e-04	0.00e+00	3.79e-03	5.34e-04	4.46e-04	3.97e-03	2.50e-03	7.32e-02		5.95e-01
8	813595	813470	140702	138106	8.17e-03	5.25e-02	5.31e-03	2.41e-03	1.07e-03	0.00e+00	3.87e-03	6.28e-04	4.47e-04	4.17e-03	2.51e-03	8.11e-02		6.76e-01
9	813864	811805	141070	141195	8.16e-03	4.84e-02	5.25e-03	2.17e-03	1.15e-03	0.00e+00	3.95e-03	7.43e-04	4.51e-04	3.97e-03	2.50e-03	7.67e-02		7.53e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.3941 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.3941 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.394050  0.000000
];

