% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_74 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	818867	809942	141586	141586	5.04e-03	2.93e-02	6.32e-03	9.08e-03	7.00e-04	0.00e+00	3.53e-03	4.49e-04	5.40e-04	6.36e-03	2.73e-03	6.40e-02		6.40e-02
1	813173	819367	129351	138276	5.06e-03	2.90e-02	5.12e-03	3.66e-03	6.63e-04	0.00e+00	4.03e-03	3.46e-04	5.28e-04	4.24e-03	2.87e-03	5.55e-02		1.20e-01
2	813888	818424	135851	129657	5.13e-03	4.92e-02	5.56e-03	1.70e-03	4.74e-04	0.00e+00	4.87e-03	4.97e-04	5.05e-04	3.45e-03	2.99e-03	7.44e-02		1.94e-01
3	789303	817108	135942	131406	5.14e-03	6.28e-02	5.93e-03	2.43e-03	5.12e-04	0.00e+00	5.74e-03	3.76e-04	6.41e-04	3.35e-03	3.05e-03	8.99e-02		2.84e-01
4	805829	817766	161333	133528	5.15e-03	5.44e-02	6.24e-03	1.99e-03	7.44e-04	0.00e+00	6.03e-03	5.69e-04	6.10e-04	3.59e-03	3.11e-03	8.24e-02		3.66e-01
5	811152	811210	145612	133675	5.13e-03	5.60e-02	6.64e-03	1.61e-03	9.66e-04	0.00e+00	6.06e-03	4.11e-04	5.66e-04	3.80e-03	3.14e-03	8.43e-02		4.51e-01
6	811850	810076	141095	141037	5.06e-03	5.71e-02	6.69e-03	2.14e-03	5.69e-04	0.00e+00	5.95e-03	5.06e-04	5.92e-04	3.69e-03	3.08e-03	8.54e-02		5.36e-01
7	804831	810894	141203	142977	5.06e-03	5.69e-02	6.69e-03	2.01e-03	5.56e-04	0.00e+00	6.07e-03	4.54e-04	5.87e-04	3.65e-03	3.05e-03	8.51e-02		6.21e-01
8	810152	811482	149028	142965	5.08e-03	5.53e-02	6.50e-03	2.45e-03	9.09e-04	0.00e+00	6.09e-03	4.48e-04	5.81e-04	3.52e-03	3.04e-03	8.40e-02		7.05e-01
9	810439	811866	144513	143183	5.09e-03	5.60e-02	6.58e-03	1.37e-03	1.05e-03	0.00e+00	6.12e-03	4.96e-04	5.87e-04	3.49e-03	3.06e-03	8.38e-02		7.89e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.2651 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.2651 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.265079  0.000000
];

