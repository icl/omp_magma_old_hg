% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_128 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	813349	814825	141586	141586	5.90e-03	2.74e-02	5.76e-03	1.03e-02	9.63e-04	0.00e+00	2.80e-03	3.64e-04	3.47e-04	7.07e-03	1.78e-03	6.27e-02		6.27e-02
1	803110	816107	134869	133393	6.20e-03	2.77e-02	3.08e-03	2.72e-03	1.14e-03	0.00e+00	3.42e-03	3.02e-04	3.89e-04	4.09e-03	2.13e-03	5.12e-02		1.14e-01
2	813718	806698	145914	132917	6.01e-03	3.75e-02	3.72e-03	1.71e-03	5.87e-04	0.00e+00	3.98e-03	4.08e-04	3.96e-04	3.33e-03	2.38e-03	6.01e-02		1.74e-01
3	808219	817558	136112	143132	5.93e-03	4.61e-02	4.24e-03	3.55e-03	3.27e-04	0.00e+00	4.62e-03	3.38e-04	4.44e-04	2.96e-03	2.34e-03	7.09e-02		2.45e-01
4	806197	807926	142417	133078	6.01e-03	4.75e-02	4.37e-03	2.20e-03	4.52e-04	0.00e+00	4.90e-03	4.44e-04	4.59e-04	2.97e-03	2.22e-03	7.16e-02		3.16e-01
5	818400	801689	145244	143515	5.93e-03	4.39e-02	4.34e-03	2.46e-03	5.11e-04	0.00e+00	4.77e-03	5.05e-04	4.51e-04	3.38e-03	2.30e-03	6.85e-02		3.85e-01
6	808608	815502	133847	150558	5.90e-03	4.88e-02	4.42e-03	1.40e-03	3.87e-04	0.00e+00	4.66e-03	5.12e-04	4.57e-04	3.09e-03	2.30e-03	7.19e-02		4.57e-01
7	814438	810843	144445	137551	6.00e-03	4.66e-02	4.44e-03	1.54e-03	4.02e-04	0.00e+00	4.91e-03	4.76e-04	4.47e-04	3.40e-03	2.30e-03	7.06e-02		5.27e-01
8	809583	813262	139421	143016	5.96e-03	4.90e-02	4.52e-03	2.04e-03	1.03e-03	0.00e+00	4.62e-03	3.93e-04	4.57e-04	2.95e-03	2.29e-03	7.33e-02		6.01e-01
9	813493	813202	145082	141403	5.99e-03	4.68e-02	4.55e-03	1.95e-03	7.28e-04	0.00e+00	4.72e-03	3.98e-04	4.68e-04	3.03e-03	2.31e-03	7.09e-02		6.72e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.1506 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.1506 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.150635  0.000000
];

