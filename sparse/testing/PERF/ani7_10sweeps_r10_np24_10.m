% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_24 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	819769	802052	141586	141586	3.64e-03	5.13e-02	9.96e-03	7.43e-03	1.08e-03	0.00e+00	1.04e-02	3.43e-04	1.08e-03	7.34e-03	6.17e-03	9.88e-02		9.88e-02
1	795448	807340	128449	146166	3.64e-03	6.42e-02	9.32e-03	4.02e-03	1.49e-03	0.00e+00	1.28e-02	2.97e-04	1.25e-03	5.99e-03	7.90e-03	1.11e-01		2.10e-01
2	796758	810307	153576	141684	3.72e-03	9.97e-02	1.19e-02	3.05e-03	1.17e-03	0.00e+00	1.56e-02	2.82e-04	1.30e-03	5.32e-03	8.50e-03	1.51e-01		3.60e-01
3	795476	805364	153072	139523	3.72e-03	1.43e-01	1.32e-02	3.36e-03	1.26e-03	0.00e+00	1.83e-02	2.80e-04	1.48e-03	5.29e-03	7.92e-03	1.98e-01		5.58e-01
4	817454	796863	155160	145272	3.69e-03	1.35e-01	1.37e-02	2.84e-03	1.92e-03	0.00e+00	1.97e-02	2.46e-04	1.56e-03	5.32e-03	8.06e-03	1.92e-01		7.50e-01
5	818302	813973	133987	154578	3.67e-03	1.39e-01	1.41e-02	3.39e-03	1.75e-03	0.00e+00	1.97e-02	2.64e-04	1.52e-03	5.73e-03	8.22e-03	1.97e-01		9.47e-01
6	810529	808472	133945	138274	3.74e-03	1.54e-01	1.45e-02	2.73e-03	1.71e-03	0.00e+00	1.99e-02	3.35e-04	1.54e-03	5.38e-03	8.09e-03	2.12e-01		1.16e+00
7	811948	809178	142524	144581	3.72e-03	1.46e-01	1.42e-02	2.60e-03	1.54e-03	0.00e+00	1.95e-02	4.28e-04	1.53e-03	5.55e-03	8.96e-03	2.04e-01		1.36e+00
8	813633	813017	141911	144681	4.96e-03	1.49e-01	1.86e-02	2.94e-03	2.19e-03	0.00e+00	1.97e-02	4.93e-04	1.51e-03	5.36e-03	9.11e-03	2.13e-01		1.58e+00
9	814104	813642	141032	141648	5.03e-03	1.49e-01	1.45e-02	2.77e-03	1.82e-03	0.00e+00	1.99e-02	4.71e-04	1.51e-03	5.34e-03	8.13e-03	2.08e-01		1.78e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 2.1824 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 2.1824 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  2.182353  0.000000
];

