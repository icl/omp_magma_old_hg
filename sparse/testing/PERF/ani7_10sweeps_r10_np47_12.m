% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_47 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	827281	810631	141586	141586	3.24e-03	3.09e-02	5.49e-03	6.79e-03	1.22e-03	0.00e+00	5.12e-03	2.33e-04	5.68e-04	5.98e-03	3.01e-03	6.25e-02		6.25e-02
1	797409	794501	120937	137587	3.27e-03	3.59e-02	4.57e-03	2.57e-03	1.19e-03	0.00e+00	6.47e-03	2.77e-04	6.46e-04	4.27e-03	3.90e-03	6.30e-02		1.26e-01
2	802757	804322	151615	154523	3.23e-03	5.66e-02	5.66e-03	2.22e-03	5.17e-04	0.00e+00	7.61e-03	2.32e-04	6.89e-04	3.60e-03	4.23e-03	8.46e-02		2.10e-01
3	805439	809382	147073	145508	3.31e-03	7.85e-02	6.59e-03	1.56e-03	6.02e-04	0.00e+00	8.75e-03	2.24e-04	7.77e-04	3.60e-03	4.04e-03	1.08e-01		3.18e-01
4	799667	813399	145197	141254	3.30e-03	7.60e-02	6.69e-03	2.14e-03	9.59e-04	0.00e+00	9.72e-03	3.35e-04	8.57e-04	3.69e-03	3.88e-03	1.08e-01		4.26e-01
5	811516	815460	151774	138042	3.34e-03	7.36e-02	6.76e-03	1.74e-03	8.43e-04	0.00e+00	9.39e-03	2.65e-04	7.88e-04	4.02e-03	3.96e-03	1.05e-01		5.30e-01
6	809441	809796	140731	136787	3.34e-03	7.68e-02	6.90e-03	1.82e-03	9.69e-04	0.00e+00	9.79e-03	4.33e-04	8.49e-04	3.77e-03	3.93e-03	1.09e-01		6.39e-01
7	814189	809233	143612	143257	3.30e-03	7.56e-02	6.86e-03	1.60e-03	9.01e-04	0.00e+00	9.56e-03	2.84e-04	8.00e-04	3.59e-03	3.97e-03	1.06e-01		7.46e-01
8	811882	811179	139670	144626	3.30e-03	7.78e-02	6.90e-03	1.64e-03	9.16e-04	0.00e+00	9.71e-03	3.05e-04	8.11e-04	3.80e-03	3.97e-03	1.09e-01		8.55e-01
9	812445	812403	142783	143486	3.33e-03	7.68e-02	6.93e-03	1.76e-03	1.40e-03	0.00e+00	9.61e-03	3.14e-04	8.22e-04	3.58e-03	3.96e-03	1.09e-01		9.63e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.3323 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.3323 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.332303  0.000000
];

