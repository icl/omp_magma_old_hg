% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_254 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	815957	810151	141586	141586	1.11e-02	4.48e-02	9.33e-03	2.15e-02	1.36e-03	0.00e+00	2.14e-03	5.23e-04	3.27e-04	1.46e-02	1.58e-03	1.07e-01		1.07e-01
1	819230	820531	132261	138067	1.12e-02	3.63e-02	3.16e-03	3.92e-03	2.03e-03	0.00e+00	2.68e-03	4.59e-04	3.82e-04	8.22e-03	1.93e-03	7.02e-02		1.78e-01
2	799752	817551	129794	128493	1.13e-02	5.13e-02	3.72e-03	2.23e-03	3.78e-04	0.00e+00	3.20e-03	5.24e-04	3.90e-04	4.26e-03	2.03e-03	7.92e-02		2.57e-01
3	809839	812111	150078	132279	1.13e-02	5.18e-02	4.01e-03	2.68e-03	3.17e-04	0.00e+00	3.53e-03	4.36e-04	4.46e-04	4.26e-03	2.01e-03	8.08e-02		3.38e-01
4	799042	807661	140797	138525	1.12e-02	5.58e-02	4.17e-03	1.97e-03	7.38e-04	0.00e+00	3.62e-03	6.65e-04	4.56e-04	4.86e-03	2.02e-03	8.55e-02		4.23e-01
5	814686	810181	152399	143780	1.11e-02	5.00e-02	4.04e-03	2.66e-03	6.31e-04	0.00e+00	3.62e-03	4.30e-04	4.57e-04	4.93e-03	2.01e-03	7.98e-02		5.03e-01
6	808986	810066	137561	142066	1.11e-02	5.57e-02	4.20e-03	4.06e-03	4.00e-04	0.00e+00	3.67e-03	8.19e-04	4.25e-04	4.72e-03	2.02e-03	8.71e-02		5.90e-01
7	812288	813358	144067	142987	1.11e-02	5.14e-02	4.34e-03	3.00e-03	4.44e-04	0.00e+00	3.71e-03	5.62e-04	4.25e-04	4.21e-03	2.07e-03	8.13e-02		6.71e-01
8	812575	813789	141571	140501	1.12e-02	5.71e-02	4.28e-03	2.57e-03	8.63e-04	0.00e+00	3.50e-03	1.06e-03	4.17e-04	4.68e-03	2.05e-03	8.77e-02		7.59e-01
9	813626	813889	142090	140876	1.12e-02	5.26e-02	4.28e-03	3.15e-03	1.88e-03	0.00e+00	3.48e-03	7.19e-04	4.18e-04	4.30e-03	2.05e-03	8.40e-02		8.43e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.5939 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.5939 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.593916  0.000000
];

