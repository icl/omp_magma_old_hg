% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_186 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	818240	807573	141586	141586	8.57e-03	3.68e-02	7.55e-03	1.59e-02	9.06e-04	0.00e+00	2.11e-03	5.82e-04	3.46e-04	1.08e-02	1.83e-03	8.54e-02		8.54e-02
1	787402	807281	129978	140645	8.54e-03	3.20e-02	3.39e-03	3.03e-03	9.00e-04	0.00e+00	2.47e-03	4.16e-04	3.74e-04	6.74e-03	1.98e-03	5.99e-02		1.45e-01
2	798376	806534	161622	141743	8.50e-03	4.51e-02	3.80e-03	3.32e-03	2.77e-04	0.00e+00	2.81e-03	4.76e-04	3.93e-04	4.00e-03	2.22e-03	7.09e-02		2.16e-01
3	799191	821731	151454	143296	8.49e-03	4.87e-02	4.24e-03	2.77e-03	4.74e-04	0.00e+00	3.32e-03	4.64e-04	4.48e-04	3.94e-03	2.24e-03	7.51e-02		2.91e-01
4	813232	806008	151445	128905	8.65e-03	5.17e-02	4.47e-03	2.49e-03	4.14e-04	0.00e+00	3.45e-03	6.70e-04	4.24e-04	4.42e-03	2.26e-03	7.89e-02		3.70e-01
5	810970	807508	138209	145433	8.49e-03	4.66e-02	4.53e-03	3.75e-03	8.29e-04	0.00e+00	3.50e-03	4.36e-04	4.46e-04	4.12e-03	2.21e-03	7.50e-02		4.45e-01
6	812728	812973	141277	144739	8.55e-03	4.96e-02	4.56e-03	1.71e-03	3.83e-04	0.00e+00	3.40e-03	4.96e-04	4.17e-04	4.24e-03	2.24e-03	7.56e-02		5.21e-01
7	811919	813913	140325	140080	8.57e-03	4.76e-02	4.67e-03	1.94e-03	3.82e-04	0.00e+00	3.40e-03	8.74e-04	4.50e-04	4.00e-03	2.26e-03	7.41e-02		5.95e-01
8	812094	814047	141940	139946	8.59e-03	5.09e-02	4.66e-03	2.12e-03	8.87e-04	0.00e+00	3.46e-03	6.32e-04	4.56e-04	4.14e-03	2.25e-03	7.81e-02		6.73e-01
9	815290	812463	142571	140618	8.61e-03	4.81e-02	4.70e-03	2.17e-03	1.30e-03	0.00e+00	3.33e-03	5.93e-04	4.11e-04	3.93e-03	2.24e-03	7.54e-02		7.48e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.3916 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.3916 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.391640  0.000000
];

