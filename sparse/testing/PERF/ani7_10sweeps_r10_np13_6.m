% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_13 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	819972	800213	141586	141586	3.97e-03	8.38e-02	1.76e-02	8.49e-03	1.47e-03	0.00e+00	1.92e-02	2.94e-04	1.95e-03	9.63e-03	1.11e-02	1.58e-01		1.58e-01
1	809312	809897	128246	148005	3.95e-03	1.06e-01	1.70e-02	5.53e-03	1.90e-03	0.00e+00	2.36e-02	2.36e-04	2.24e-03	8.45e-03	1.45e-02	1.83e-01		3.41e-01
2	816327	815069	139712	139127	4.05e-03	1.74e-01	2.20e-02	4.68e-03	1.71e-03	0.00e+00	2.85e-02	2.24e-04	2.28e-03	8.05e-03	1.60e-02	2.62e-01		6.03e-01
3	813212	814707	133503	134761	4.07e-03	2.60e-01	2.52e-02	4.86e-03	2.14e-03	0.00e+00	3.25e-02	4.79e-04	2.61e-03	7.95e-03	1.49e-02	3.55e-01		9.58e-01
4	806113	808481	137424	135929	4.11e-03	2.41e-01	2.57e-02	4.30e-03	2.73e-03	0.00e+00	3.54e-02	3.99e-04	2.64e-03	8.14e-03	1.43e-02	3.39e-01		1.30e+00
5	808302	804616	145328	142960	4.04e-03	2.28e-01	2.54e-02	4.64e-03	3.00e-03	0.00e+00	3.61e-02	3.71e-04	2.63e-03	8.49e-03	1.43e-02	3.27e-01		1.62e+00
6	811417	807120	143945	147631	4.04e-03	2.31e-01	2.54e-02	4.22e-03	2.63e-03	0.00e+00	3.48e-02	4.16e-04	2.59e-03	7.96e-03	1.43e-02	3.28e-01		1.95e+00
7	806702	813267	141636	145933	4.04e-03	2.33e-01	2.57e-02	5.12e-03	2.56e-03	0.00e+00	3.46e-02	4.49e-04	2.55e-03	7.89e-03	1.46e-02	3.30e-01		2.28e+00
8	813248	812803	147157	140592	4.04e-03	2.33e-01	2.58e-02	4.42e-03	3.01e-03	0.00e+00	3.55e-02	3.10e-04	2.63e-03	8.27e-03	1.46e-02	3.32e-01		2.61e+00
9	814938	813871	141417	141862	4.10e-03	2.36e-01	2.61e-02	4.64e-03	3.37e-03	0.00e+00	3.65e-02	4.00e-04	2.66e-03	7.88e-03	1.46e-02	3.37e-01		2.95e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 3.3825 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 3.3825 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  3.382470  0.000000
];

