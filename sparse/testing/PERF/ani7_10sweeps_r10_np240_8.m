% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_240 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	808420	812335	141586	141586	1.07e-02	4.41e-02	8.92e-03	1.88e-02	2.14e-03	0.00e+00	2.28e-03	5.53e-04	3.54e-04	1.38e-02	1.73e-03	1.03e-01		1.03e-01
1	814881	797842	139798	135883	1.07e-02	3.52e-02	3.31e-03	5.72e-03	2.05e-03	0.00e+00	2.86e-03	4.48e-04	3.70e-04	7.86e-03	1.88e-03	7.04e-02		1.74e-01
2	815949	809351	134143	151182	1.05e-02	5.08e-02	3.59e-03	3.28e-03	3.48e-04	0.00e+00	3.23e-03	4.71e-04	3.80e-04	4.33e-03	1.99e-03	7.90e-02		2.53e-01
3	805338	814120	133881	140479	1.07e-02	5.18e-02	4.49e-03	2.99e-03	3.67e-04	0.00e+00	3.49e-03	6.22e-04	4.11e-04	4.26e-03	2.08e-03	8.12e-02		3.34e-01
4	814344	801321	145298	136516	1.07e-02	5.45e-02	4.20e-03	2.75e-03	5.32e-04	0.00e+00	3.72e-03	6.79e-04	4.21e-04	5.11e-03	2.09e-03	8.47e-02		4.19e-01
5	810694	807387	137097	150120	1.06e-02	4.95e-02	4.31e-03	2.19e-03	5.52e-04	0.00e+00	3.78e-03	7.55e-04	4.61e-04	5.11e-03	2.14e-03	7.94e-02		4.98e-01
6	811820	810880	141553	144860	1.06e-02	5.60e-02	4.38e-03	1.93e-03	3.69e-04	0.00e+00	3.64e-03	7.28e-04	4.09e-04	4.74e-03	2.16e-03	8.50e-02		5.83e-01
7	811292	809804	141233	142173	1.07e-02	5.16e-02	4.45e-03	2.12e-03	4.10e-04	0.00e+00	3.67e-03	8.06e-04	4.09e-04	4.32e-03	2.16e-03	8.07e-02		6.64e-01
8	814294	815151	142567	144055	1.06e-02	5.65e-02	4.44e-03	2.10e-03	8.19e-04	0.00e+00	3.77e-03	6.93e-04	4.07e-04	4.68e-03	2.19e-03	8.62e-02		7.50e-01
9	812877	815073	140371	139514	1.08e-02	5.27e-02	4.52e-03	2.12e-03	1.45e-03	0.00e+00	3.66e-03	7.68e-04	4.30e-04	4.22e-03	2.19e-03	8.29e-02		8.33e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.5872 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.5872 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.587242  0.000000
];

