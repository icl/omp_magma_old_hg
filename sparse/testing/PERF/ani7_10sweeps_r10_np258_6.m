% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_258 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	804406	816457	141586	141586	1.13e-02	4.40e-02	9.34e-03	2.07e-02	1.54e-03	0.00e+00	2.09e-03	6.48e-04	3.35e-04	1.43e-02	1.58e-03	1.06e-01		1.06e-01
1	811708	806882	143812	131761	1.15e-02	3.56e-02	3.10e-03	3.70e-03	1.30e-03	0.00e+00	2.60e-03	5.43e-04	3.55e-04	6.78e-03	1.89e-03	6.74e-02		1.73e-01
2	798900	815802	137316	142142	1.12e-02	5.18e-02	3.63e-03	2.98e-03	3.29e-04	0.00e+00	2.94e-03	5.27e-04	4.27e-04	4.29e-03	2.04e-03	8.02e-02		2.53e-01
3	817307	810165	150930	134028	1.14e-02	5.14e-02	4.07e-03	3.57e-03	3.09e-04	0.00e+00	3.26e-03	8.23e-04	4.52e-04	4.25e-03	2.11e-03	8.17e-02		3.35e-01
4	808567	808249	133329	140471	1.13e-02	5.72e-02	4.40e-03	2.92e-03	4.93e-04	0.00e+00	3.54e-03	9.50e-04	4.30e-04	4.77e-03	2.06e-03	8.80e-02		4.23e-01
5	810932	816050	142874	143192	1.13e-02	5.12e-02	4.20e-03	2.22e-03	1.44e-03	0.00e+00	3.52e-03	7.56e-04	4.13e-04	5.04e-03	2.07e-03	8.21e-02		5.05e-01
6	806082	810469	141315	136197	1.15e-02	5.78e-02	4.32e-03	1.82e-03	3.81e-04	0.00e+00	3.45e-03	7.36e-04	4.39e-04	4.91e-03	2.19e-03	8.76e-02		5.93e-01
7	811785	812645	146971	142584	1.14e-02	5.16e-02	4.29e-03	2.79e-03	4.56e-04	0.00e+00	3.44e-03	7.85e-04	4.15e-04	4.82e-03	2.09e-03	8.21e-02		6.75e-01
8	813099	812536	142074	141214	1.14e-02	5.73e-02	4.36e-03	2.08e-03	1.72e-03	0.00e+00	4.08e-03	9.01e-04	4.72e-04	4.55e-03	2.08e-03	8.89e-02		7.64e-01
9	812043	812873	141566	142129	3.01e-01	1.26e-01	4.44e-03	2.43e-03	1.74e-03	0.00e+00	3.51e-03	6.93e-04	4.57e-04	4.21e-03	2.09e-03	4.47e-01		1.21e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.9772 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.9772 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.977175  0.000000
];

