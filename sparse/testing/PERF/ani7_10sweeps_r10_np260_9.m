% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_260 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	823633	802901	141586	141586	1.14e-02	4.48e-02	9.36e-03	2.15e-02	2.61e-03	0.00e+00	2.57e-03	5.43e-04	3.29e-04	1.40e-02	1.62e-03	1.09e-01		1.09e-01
1	813146	800390	124585	145317	1.13e-02	6.44e-02	3.14e-03	2.44e-03	2.41e-03	0.00e+00	2.73e-03	4.49e-04	3.50e-04	7.12e-03	1.81e-03	9.62e-02		2.05e-01
2	819166	821852	135878	148634	1.12e-02	5.01e-02	3.54e-03	3.92e-03	1.33e-03	0.00e+00	3.19e-03	4.70e-04	3.78e-04	4.81e-03	2.08e-03	8.11e-02		2.86e-01
3	812661	810762	130664	127978	1.16e-02	5.45e-02	4.80e-03	3.70e-03	3.15e-04	0.00e+00	3.86e-03	6.27e-04	4.22e-04	4.20e-03	2.11e-03	8.61e-02		3.72e-01
4	805505	814545	137975	139874	1.13e-02	5.63e-02	4.17e-03	3.50e-03	7.80e-04	0.00e+00	3.95e-03	5.83e-04	4.48e-04	4.36e-03	2.06e-03	8.75e-02		4.60e-01
5	808939	806553	145936	136896	1.14e-02	5.20e-02	4.21e-03	2.88e-03	4.05e-04	0.00e+00	3.83e-03	7.92e-04	4.18e-04	4.89e-03	2.07e-03	8.29e-02		5.43e-01
6	811275	809550	143308	145694	1.14e-02	5.62e-02	4.27e-03	1.60e-03	3.81e-04	0.00e+00	3.78e-03	8.39e-04	4.07e-04	4.45e-03	2.10e-03	8.55e-02		6.28e-01
7	817612	810306	141778	143503	1.14e-02	5.31e-02	4.29e-03	2.95e-03	3.88e-04	0.00e+00	3.68e-03	4.97e-04	4.54e-04	4.14e-03	2.15e-03	8.31e-02		7.11e-01
8	813079	813954	136247	143553	1.14e-02	5.88e-02	4.43e-03	1.90e-03	3.97e-04	0.00e+00	3.92e-03	6.74e-04	4.16e-04	4.40e-03	2.16e-03	8.84e-02		8.00e-01
9	810918	813846	141586	140711	1.14e-02	5.43e-02	4.41e-03	1.74e-03	1.74e-03	0.00e+00	3.79e-03	8.04e-04	4.11e-04	4.26e-03	2.10e-03	8.49e-02		8.84e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.6040 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.6040 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.603985  0.000000
];

