% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_5 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	820362	807999	141586	141586	4.83e-03	1.98e-01	4.44e-02	1.28e-02	2.84e-03	0.00e+00	4.59e-02	2.45e-04	4.79e-03	1.80e-02	2.81e-02	3.60e-01		3.60e-01
1	806249	794409	127856	140219	5.10e-03	2.41e-01	4.40e-02	9.79e-03	4.27e-03	0.00e+00	5.56e-02	2.39e-04	5.14e-03	1.67e-02	3.59e-02	4.18e-01		7.78e-01
2	803897	802696	142775	154615	5.17e-03	4.18e-01	5.43e-02	9.61e-03	4.25e-03	0.00e+00	6.67e-02	3.87e-04	5.44e-03	1.71e-02	3.92e-02	6.20e-01		1.40e+00
3	824094	796730	145933	147134	5.17e-03	5.91e-01	6.10e-02	9.44e-03	4.95e-03	0.00e+00	7.58e-02	3.09e-04	6.18e-03	1.67e-02	3.76e-02	8.09e-01		2.21e+00
4	808415	807421	126542	153906	5.12e-03	5.72e-01	6.44e-02	1.01e-02	6.45e-03	0.00e+00	7.84e-02	4.18e-04	6.41e-03	1.65e-02	3.64e-02	7.96e-01		3.00e+00
5	810590	809665	143026	144020	5.17e-03	5.58e-01	6.46e-02	1.02e-02	6.45e-03	0.00e+00	7.76e-02	2.46e-04	6.33e-03	1.67e-02	3.70e-02	7.83e-01		3.79e+00
6	811953	813547	141657	142582	5.21e-03	5.77e-01	6.54e-02	1.07e-02	7.23e-03	0.00e+00	8.34e-02	4.02e-04	6.63e-03	1.68e-02	3.73e-02	8.10e-01		4.60e+00
7	810839	810790	141100	139506	5.27e-03	5.90e-01	6.66e-02	1.00e-02	6.75e-03	0.00e+00	8.12e-02	3.32e-04	6.48e-03	1.70e-02	3.72e-02	8.21e-01		5.42e+00
8	813074	811272	143020	143069	5.22e-03	5.94e-01	6.65e-02	9.30e-03	6.23e-03	0.00e+00	7.65e-02	3.74e-04	6.16e-03	1.67e-02	3.73e-02	8.18e-01		6.24e+00
9	814796	815396	141591	143393	5.24e-03	5.95e-01	6.69e-02	1.06e-02	7.48e-03	0.00e+00	8.53e-02	3.60e-04	6.60e-03	1.68e-02	3.75e-02	8.32e-01		7.07e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 7.6296 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 7.6296 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  7.629567  0.000000
];

