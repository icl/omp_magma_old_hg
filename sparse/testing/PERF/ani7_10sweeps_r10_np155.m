% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_155 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	823971	805773	141586	141586	9.53e-03	5.13e-02	1.28e-02	1.78e-02	2.68e-03	0.00e+00	3.61e-03	4.85e-04	3.96e-04	1.59e-02	2.15e-03	1.17e-01		1.17e-01
1	789320	806021	124247	142445	9.79e-03	4.15e-02	4.19e-03	2.93e-03	1.45e-03	0.00e+00	4.25e-03	3.97e-04	4.26e-04	5.78e-03	2.80e-03	7.35e-02		1.90e-01
2	806803	809161	159704	143003	9.87e-03	6.48e-02	4.87e-03	2.13e-03	8.36e-04	0.00e+00	4.68e-03	5.87e-04	5.11e-04	4.64e-03	2.97e-03	9.59e-02		2.86e-01
3	804505	802296	143027	140669	9.83e-03	6.60e-02	5.67e-03	2.27e-03	5.97e-04	0.00e+00	5.21e-03	5.43e-04	5.99e-04	4.07e-03	3.00e-03	9.78e-02		3.84e-01
4	813917	812882	146131	148340	9.72e-03	6.70e-02	5.63e-03	1.90e-03	6.53e-04	0.00e+00	5.45e-03	7.06e-04	5.92e-04	4.15e-03	2.87e-03	9.87e-02		4.83e-01
5	811076	811946	137524	138559	9.87e-03	6.52e-02	5.63e-03	1.93e-03	6.15e-04	0.00e+00	5.58e-03	5.55e-04	6.00e-04	4.57e-03	2.90e-03	9.75e-02		5.80e-01
6	811945	809407	141171	140301	9.96e-03	6.85e-02	5.83e-03	3.36e-03	6.72e-04	0.00e+00	5.59e-03	5.17e-04	5.90e-04	4.27e-03	3.00e-03	1.02e-01		6.82e-01
7	810600	813189	141108	143646	1.02e-02	6.54e-02	5.85e-03	1.79e-03	6.12e-04	0.00e+00	5.58e-03	7.08e-04	5.83e-04	4.35e-03	2.96e-03	9.81e-02		7.80e-01
8	813571	813268	143259	140670	9.90e-03	6.96e-02	5.86e-03	1.65e-03	1.47e-03	0.00e+00	5.64e-03	5.59e-04	5.50e-04	4.10e-03	2.97e-03	1.02e-01		8.83e-01
9	813140	812882	141094	141397	9.84e-03	6.65e-02	6.14e-03	2.71e-03	1.24e-03	0.00e+00	5.67e-03	6.22e-04	5.80e-04	4.08e-03	2.93e-03	1.00e-01		9.83e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.6209 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.6209 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.620881  0.000000
];

