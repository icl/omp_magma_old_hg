% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_68 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	820080	811227	141586	141586	3.25e-03	2.66e-02	4.55e-03	8.57e-03	7.87e-04	0.00e+00	3.62e-03	3.12e-04	4.39e-04	5.73e-03	2.18e-03	5.61e-02		5.61e-02
1	822890	804560	128138	136991	3.24e-03	2.96e-02	3.55e-03	3.41e-03	1.02e-03	0.00e+00	4.43e-03	2.65e-04	4.81e-04	3.86e-03	2.95e-03	5.28e-02		1.09e-01
2	814974	796679	126134	144464	3.27e-03	5.04e-02	4.43e-03	1.48e-03	6.01e-04	0.00e+00	5.50e-03	3.46e-04	5.17e-04	3.20e-03	3.03e-03	7.28e-02		1.82e-01
3	798492	806048	134856	153151	3.24e-03	6.18e-02	4.52e-03	2.22e-03	5.07e-04	0.00e+00	6.08e-03	4.95e-04	6.59e-04	3.32e-03	2.85e-03	8.57e-02		2.67e-01
4	814833	809934	152144	144588	3.29e-03	5.83e-02	4.67e-03	2.41e-03	6.26e-04	0.00e+00	6.57e-03	2.30e-04	6.49e-04	3.15e-03	2.76e-03	8.27e-02		3.50e-01
5	813876	812928	136608	141507	3.26e-03	5.61e-02	4.76e-03	1.55e-03	6.53e-04	0.00e+00	6.78e-03	3.51e-04	6.43e-04	3.59e-03	2.79e-03	8.05e-02		4.30e-01
6	810292	812215	138371	139319	3.28e-03	6.01e-02	4.81e-03	1.56e-03	7.22e-04	0.00e+00	6.83e-03	4.03e-04	6.75e-04	3.35e-03	2.80e-03	8.46e-02		5.15e-01
7	810513	809674	142761	140838	3.72e-03	5.81e-02	4.84e-03	1.39e-03	6.42e-04	0.00e+00	6.73e-03	4.03e-04	6.52e-04	3.51e-03	2.89e-03	8.29e-02		5.98e-01
8	813492	811056	143346	144185	3.43e-03	6.06e-02	4.79e-03	1.32e-03	1.18e-03	0.00e+00	6.68e-03	3.79e-04	6.42e-04	3.42e-03	2.80e-03	8.52e-02		6.83e-01
9	813131	815528	141173	143609	3.29e-03	5.93e-02	4.90e-03	1.58e-03	9.16e-04	0.00e+00	6.64e-03	3.46e-04	6.34e-04	3.23e-03	2.84e-03	8.36e-02		7.67e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.1658 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.1658 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.165818  0.000000
];

