% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_48 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	815169	808974	141586	141586	3.25e-03	3.05e-02	5.39e-03	6.71e-03	5.69e-04	0.00e+00	5.03e-03	2.91e-04	5.63e-04	6.02e-03	2.94e-03	6.13e-02		6.13e-02
1	815680	817709	133049	139244	3.25e-03	3.52e-02	4.42e-03	2.45e-03	9.43e-04	0.00e+00	6.13e-03	2.10e-04	6.25e-04	4.23e-03	3.92e-03	6.14e-02		1.23e-01
2	808167	813626	133344	131315	3.32e-03	5.63e-02	6.05e-03	2.07e-03	4.79e-04	0.00e+00	7.76e-03	2.78e-04	6.77e-04	3.62e-03	4.23e-03	8.48e-02		2.07e-01
3	801084	802385	141663	136204	3.29e-03	7.93e-02	6.38e-03	1.64e-03	6.01e-04	0.00e+00	8.62e-03	2.30e-04	7.78e-04	3.56e-03	3.81e-03	1.08e-01		3.16e-01
4	811464	814671	149552	148251	3.29e-03	7.31e-02	6.47e-03	1.98e-03	9.16e-04	0.00e+00	9.33e-03	2.85e-04	8.07e-04	3.86e-03	3.85e-03	1.04e-01		4.20e-01
5	808481	810073	139977	136770	3.31e-03	7.35e-02	6.69e-03	1.80e-03	1.06e-03	0.00e+00	9.46e-03	4.24e-04	7.86e-04	4.08e-03	3.90e-03	1.05e-01		5.25e-01
6	810761	809648	143766	142174	3.31e-03	7.46e-02	9.71e-03	1.91e-03	9.00e-04	0.00e+00	9.51e-03	2.73e-04	8.19e-04	3.73e-03	5.01e-03	1.10e-01		6.34e-01
7	805091	809433	142292	143405	5.12e-03	7.43e-02	9.80e-03	2.42e-03	8.21e-04	0.00e+00	9.28e-03	3.97e-04	7.54e-04	3.63e-03	5.01e-03	1.12e-01		7.46e-01
8	814001	813866	148768	144426	5.17e-03	7.48e-02	9.74e-03	2.32e-03	1.22e-03	0.00e+00	9.44e-03	3.68e-04	8.05e-04	3.77e-03	3.92e-03	1.12e-01		8.57e-01
9	814339	813719	140664	140799	3.33e-03	7.59e-02	6.82e-03	2.01e-03	1.26e-03	0.00e+00	9.44e-03	3.53e-04	7.99e-04	3.54e-03	3.91e-03	1.07e-01		9.65e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.3380 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.3380 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.337969  0.000000
];

