% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_211 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	819591	811796	141586	141586	9.81e-03	4.07e-02	8.47e-03	1.81e-02	1.49e-03	0.00e+00	2.58e-03	4.70e-04	4.31e-04	1.35e-02	1.93e-03	9.74e-02		9.74e-02
1	809332	808897	128627	136422	9.82e-03	3.60e-02	3.68e-03	4.67e-03	2.78e-03	0.00e+00	3.37e-03	4.95e-04	3.91e-04	7.05e-03	2.16e-03	7.04e-02		1.68e-01
2	798542	815267	139692	140127	9.71e-03	5.45e-02	3.92e-03	3.92e-03	3.38e-04	0.00e+00	3.63e-03	6.01e-04	4.29e-04	4.35e-03	2.13e-03	8.35e-02		2.51e-01
3	805247	811745	151288	134563	9.79e-03	5.36e-02	4.14e-03	2.09e-03	4.09e-04	0.00e+00	3.96e-03	3.18e-04	4.32e-04	4.31e-03	2.17e-03	8.12e-02		3.33e-01
4	801851	810555	145389	138891	9.81e-03	5.57e-02	4.40e-03	2.87e-03	4.88e-04	0.00e+00	4.16e-03	5.84e-04	4.53e-04	4.57e-03	2.07e-03	8.52e-02		4.18e-01
5	806616	809548	149590	140886	9.73e-03	5.05e-02	4.53e-03	4.43e-03	4.60e-04	0.00e+00	4.17e-03	5.25e-04	4.39e-04	5.06e-03	2.08e-03	8.19e-02		5.00e-01
6	813867	811981	145631	142699	9.76e-03	5.58e-02	4.49e-03	3.13e-03	4.33e-04	0.00e+00	4.15e-03	3.95e-04	4.68e-04	4.58e-03	2.15e-03	8.54e-02		5.85e-01
7	811950	813202	139186	141072	9.81e-03	5.23e-02	4.62e-03	2.85e-03	4.32e-04	0.00e+00	4.15e-03	6.87e-04	4.41e-04	4.28e-03	2.09e-03	8.17e-02		6.67e-01
8	811145	813772	141909	140657	9.77e-03	5.64e-02	4.59e-03	2.10e-03	4.75e-04	0.00e+00	4.22e-03	6.73e-04	4.37e-04	4.61e-03	2.14e-03	8.54e-02		7.52e-01
9	816031	813871	143520	140893	9.81e-03	5.25e-02	4.65e-03	3.02e-03	1.53e-03	0.00e+00	4.19e-03	7.21e-04	4.44e-04	4.29e-03	2.12e-03	8.33e-02		8.35e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.5793 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.5793 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.579350  0.000000
];

