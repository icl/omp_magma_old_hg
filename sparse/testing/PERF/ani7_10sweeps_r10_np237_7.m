% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_237 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	826621	796812	141586	141586	1.07e-02	4.39e-02	9.04e-03	2.21e-02	2.56e-03	0.00e+00	2.33e-03	5.12e-04	3.46e-04	1.19e-02	1.75e-03	1.05e-01		1.05e-01
1	800578	811678	121597	151406	1.05e-02	3.58e-02	3.36e-03	3.71e-03	2.03e-03	0.00e+00	2.92e-03	4.18e-04	3.70e-04	7.03e-03	1.92e-03	6.81e-02		1.73e-01
2	811502	812868	148446	137346	1.07e-02	3.31e-01	3.68e-03	2.33e-03	1.12e-03	0.00e+00	3.26e-03	6.82e-04	4.07e-04	4.26e-03	2.06e-03	3.60e-01		5.33e-01
3	818864	821084	138328	136962	1.06e-02	5.21e-02	4.34e-03	1.88e-03	3.03e-04	0.00e+00	3.53e-03	3.97e-04	4.40e-04	4.15e-03	2.17e-03	8.00e-02		6.13e-01
4	814210	807486	131772	129552	1.08e-02	5.78e-02	4.42e-03	1.63e-03	6.83e-04	0.00e+00	3.79e-03	7.74e-04	4.59e-04	4.25e-03	2.17e-03	8.68e-02		7.00e-01
5	810080	807654	137231	143955	1.06e-02	4.96e-02	4.46e-03	1.60e-03	4.73e-04	0.00e+00	3.83e-03	6.86e-04	4.47e-04	5.11e-03	2.16e-03	7.90e-02		7.78e-01
6	812102	810914	142167	144593	1.07e-02	5.50e-02	4.48e-03	2.47e-03	4.38e-04	0.00e+00	3.78e-03	6.05e-04	4.05e-04	4.60e-03	2.19e-03	8.47e-02		8.63e-01
7	813687	813394	140951	142139	1.07e-02	5.02e-02	4.51e-03	2.20e-03	6.06e-04	0.00e+00	3.82e-03	7.52e-04	4.12e-04	4.58e-03	2.21e-03	7.99e-02		9.43e-01
8	814850	813469	140172	140465	1.07e-02	5.79e-02	4.55e-03	2.63e-03	1.34e-03	0.00e+00	3.79e-03	8.22e-04	4.08e-04	4.33e-03	2.24e-03	8.88e-02		1.03e+00
9	810389	811497	139815	141196	1.07e-02	5.12e-02	4.70e-03	2.60e-03	1.58e-03	0.00e+00	3.78e-03	8.04e-04	4.43e-04	4.24e-03	2.20e-03	8.23e-02		1.11e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.8104 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.8104 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.810383  0.000000
];

