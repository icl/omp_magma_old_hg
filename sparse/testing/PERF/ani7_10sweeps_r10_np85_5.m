% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_85 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	813942	805730	141586	141586	5.33e-03	3.05e-02	6.48e-03	9.56e-03	6.73e-04	0.00e+00	3.13e-03	3.46e-04	5.23e-04	6.54e-03	2.47e-03	6.56e-02		6.56e-02
1	790416	810815	134276	142488	5.32e-03	2.78e-02	4.51e-03	1.78e-03	6.78e-04	0.00e+00	3.74e-03	3.18e-04	4.60e-04	4.20e-03	2.52e-03	5.13e-02		1.17e-01
2	800600	821547	158608	138209	5.37e-03	4.79e-02	5.07e-03	2.51e-03	3.78e-04	0.00e+00	4.29e-03	3.93e-04	4.95e-04	3.31e-03	2.74e-03	7.25e-02		1.89e-01
3	810894	800881	149230	128283	5.41e-03	5.65e-02	5.55e-03	1.83e-03	4.77e-04	0.00e+00	4.87e-03	3.59e-04	5.45e-04	3.24e-03	2.66e-03	8.15e-02		2.71e-01
4	818427	817388	139742	149755	5.30e-03	5.43e-02	6.04e-03	1.81e-03	5.01e-04	0.00e+00	5.27e-03	4.68e-04	5.54e-04	3.48e-03	2.82e-03	8.05e-02		3.51e-01
5	809541	806790	133014	134053	5.42e-03	5.28e-02	6.37e-03	1.84e-03	1.01e-03	0.00e+00	5.43e-03	5.14e-04	5.64e-04	3.53e-03	2.75e-03	8.02e-02		4.32e-01
6	810440	811099	142706	145457	5.32e-03	5.34e-02	6.19e-03	1.63e-03	5.16e-04	0.00e+00	5.41e-03	3.24e-04	5.55e-04	3.47e-03	2.79e-03	7.96e-02		5.11e-01
7	809932	805809	142613	141954	5.33e-03	5.15e-02	6.29e-03	1.87e-03	5.70e-04	0.00e+00	5.37e-03	5.10e-04	5.39e-04	3.55e-03	2.75e-03	7.83e-02		5.90e-01
8	812416	807818	143927	148050	5.34e-03	5.38e-02	6.22e-03	1.75e-03	1.01e-03	0.00e+00	5.31e-03	5.10e-04	5.62e-04	3.39e-03	2.77e-03	8.06e-02		6.70e-01
9	812285	812337	142249	146847	5.36e-03	5.14e-02	6.28e-03	1.43e-03	9.95e-04	0.00e+00	5.36e-03	4.19e-04	5.34e-04	3.28e-03	2.78e-03	7.79e-02		7.48e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.2204 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.2204 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.220358  0.000000
];

