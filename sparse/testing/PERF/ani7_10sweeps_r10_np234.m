% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_234 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	824976	800403	141586	141586	1.20e-02	6.50e-02	1.65e-02	2.44e-02	1.23e-03	0.00e+00	3.03e-03	5.28e-04	3.62e-04	2.84e-02	1.78e-03	1.53e-01		1.53e-01
1	815292	807350	123242	147815	1.19e-02	4.53e-02	3.38e-03	4.13e-03	1.33e-03	0.00e+00	3.56e-03	4.12e-04	3.79e-04	7.35e-03	2.27e-03	8.00e-02		2.33e-01
2	799882	807874	133732	141674	1.20e-02	6.90e-02	4.22e-03	2.90e-03	4.80e-04	0.00e+00	3.89e-03	7.84e-04	4.60e-04	4.54e-03	2.34e-03	1.01e-01		3.34e-01
3	802375	807901	149948	141956	1.20e-02	6.42e-02	4.60e-03	3.63e-03	4.97e-04	0.00e+00	4.27e-03	6.32e-04	5.07e-04	4.44e-03	2.36e-03	9.72e-02		4.31e-01
4	807442	812348	148261	142735	1.21e-02	7.54e-02	4.72e-03	2.77e-03	5.60e-04	0.00e+00	4.59e-03	5.26e-04	5.41e-04	4.61e-03	2.34e-03	1.08e-01		5.39e-01
5	813369	808028	143999	139093	1.21e-02	6.37e-02	4.67e-03	1.97e-03	1.88e-03	0.00e+00	4.61e-03	6.77e-04	4.89e-04	5.53e-03	2.35e-03	9.80e-02		6.37e-01
6	809936	812897	138878	144219	1.21e-02	7.63e-02	4.77e-03	3.07e-03	6.21e-04	0.00e+00	4.69e-03	4.65e-04	5.41e-04	5.10e-03	2.35e-03	1.10e-01		7.47e-01
7	811858	814619	143117	140156	1.21e-02	6.40e-02	4.77e-03	1.85e-03	8.47e-04	0.00e+00	4.66e-03	8.62e-04	5.15e-04	4.91e-03	2.38e-03	9.69e-02		8.44e-01
8	812048	810131	142001	139240	1.21e-02	7.68e-02	4.85e-03	2.36e-03	1.70e-03	0.00e+00	4.67e-03	7.16e-04	5.04e-04	4.64e-03	2.36e-03	1.11e-01		9.55e-01
9	813254	813680	142617	144534	1.21e-02	6.59e-02	4.80e-03	2.30e-03	1.61e-03	0.00e+00	4.55e-03	9.30e-04	5.06e-04	4.47e-03	2.41e-03	9.95e-02		1.05e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.8142 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.8142 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.814196  0.000000
];

