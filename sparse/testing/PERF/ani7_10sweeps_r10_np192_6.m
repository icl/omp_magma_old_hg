% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_192 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	808165	806265	141586	141586	8.69e-03	3.68e-02	7.63e-03	1.59e-02	5.58e-04	0.00e+00	2.74e-03	4.69e-04	3.72e-04	9.59e-03	1.76e-03	8.45e-02		8.45e-02
1	800050	804363	140053	141953	8.67e-03	3.14e-02	3.24e-03	2.51e-03	5.84e-04	0.00e+00	3.04e-03	4.28e-04	3.63e-04	6.99e-03	1.96e-03	5.92e-02		1.44e-01
2	809995	809355	148974	144661	8.64e-03	4.50e-02	3.80e-03	2.02e-03	2.95e-04	0.00e+00	3.80e-03	3.98e-04	4.35e-04	3.97e-03	2.17e-03	7.06e-02		2.14e-01
3	811204	809443	139835	140475	8.70e-03	4.82e-02	4.20e-03	2.19e-03	4.01e-04	0.00e+00	4.24e-03	5.88e-04	4.46e-04	3.93e-03	2.21e-03	7.51e-02		2.89e-01
4	806195	808048	139432	141193	8.68e-03	5.31e-02	5.28e-03	3.68e-03	3.64e-04	0.00e+00	4.39e-03	6.80e-04	4.69e-04	4.93e-03	2.51e-03	8.41e-02		3.73e-01
5	813598	810799	145246	143393	1.10e-02	5.01e-02	5.30e-03	1.98e-03	1.30e-03	0.00e+00	4.39e-03	4.88e-04	4.89e-04	4.31e-03	2.69e-03	8.21e-02		4.56e-01
6	809805	809201	138649	141448	1.10e-02	5.57e-02	5.42e-03	1.82e-03	4.07e-04	0.00e+00	4.35e-03	8.11e-04	4.74e-04	4.36e-03	2.61e-03	8.70e-02		5.43e-01
7	810748	813452	143248	143852	1.06e-02	5.23e-02	5.42e-03	2.19e-03	7.51e-04	0.00e+00	4.75e-03	4.98e-04	4.78e-04	4.03e-03	2.69e-03	8.38e-02		6.26e-01
8	812140	812492	143111	140407	1.11e-02	5.75e-02	5.60e-03	2.01e-03	9.61e-04	0.00e+00	4.85e-03	1.13e-02	4.51e-04	4.29e-03	2.66e-03	1.01e-01		7.27e-01
9	815127	811396	142525	142173	1.10e-02	5.26e-02	4.64e-03	3.64e-03	1.19e-03	0.00e+00	4.33e-03	5.41e-04	4.76e-04	3.97e-03	2.24e-03	8.46e-02		8.12e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.4134 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.4134 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.413400  0.000000
];

