% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_50 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	818674	797082	141586	141586	3.24e-03	3.00e-02	5.29e-03	6.63e-03	5.71e-04	0.00e+00	4.74e-03	3.05e-04	5.23e-04	5.78e-03	2.82e-03	5.99e-02		5.99e-02
1	816925	787675	129544	151136	3.20e-03	3.40e-02	4.18e-03	1.81e-03	8.01e-04	0.00e+00	5.84e-03	2.19e-04	6.20e-04	4.21e-03	3.73e-03	5.87e-02		1.19e-01
2	807111	796094	132099	161349	3.19e-03	5.52e-02	5.39e-03	2.10e-03	4.68e-04	0.00e+00	7.09e-03	2.88e-04	6.53e-04	3.58e-03	3.97e-03	8.20e-02		2.01e-01
3	809565	806472	142719	153736	3.24e-03	7.48e-02	5.99e-03	1.93e-03	5.78e-04	0.00e+00	8.18e-03	2.67e-04	7.01e-04	3.52e-03	3.80e-03	1.03e-01		3.04e-01
4	807020	807737	141071	144164	3.28e-03	7.23e-02	6.51e-03	1.75e-03	9.18e-04	0.00e+00	9.14e-03	3.23e-04	7.97e-04	3.82e-03	3.66e-03	1.03e-01		4.06e-01
5	809030	802480	144421	143704	3.34e-03	6.94e-02	6.34e-03	1.82e-03	1.02e-03	0.00e+00	8.88e-03	2.88e-04	7.71e-04	3.85e-03	3.66e-03	9.93e-02		5.05e-01
6	808046	814440	143217	149767	3.28e-03	7.08e-02	6.32e-03	1.83e-03	9.16e-04	0.00e+00	9.22e-03	4.05e-04	7.45e-04	3.62e-03	3.75e-03	1.01e-01		6.06e-01
7	815006	813892	145007	138613	3.33e-03	7.36e-02	6.47e-03	2.28e-03	8.17e-04	0.00e+00	9.00e-03	3.63e-04	7.40e-04	3.54e-03	3.78e-03	1.04e-01		7.10e-01
8	803708	811484	138853	139967	3.29e-03	7.55e-02	6.54e-03	2.53e-03	1.06e-03	0.00e+00	8.98e-03	3.68e-04	7.80e-04	3.65e-03	3.70e-03	1.06e-01		8.17e-01
9	811725	814909	150957	143181	3.32e-03	7.21e-02	6.45e-03	1.86e-03	1.22e-03	0.00e+00	8.99e-03	3.03e-04	7.44e-04	3.54e-03	3.76e-03	1.02e-01		9.19e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.2938 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.2938 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.293798  0.000000
];

