% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_247 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	809472	821792	141586	141586	1.11e-02	4.44e-02	9.18e-03	2.03e-02	1.09e-03	0.00e+00	2.27e-03	5.33e-04	3.37e-04	1.45e-02	1.71e-03	1.05e-01		1.05e-01
1	799976	804439	138746	126426	1.12e-02	3.75e-02	3.26e-03	3.82e-03	1.91e-03	0.00e+00	2.86e-03	5.10e-04	3.72e-04	8.00e-03	1.81e-03	7.13e-02		1.77e-01
2	807160	808529	149048	144585	1.09e-02	5.09e-02	3.63e-03	1.96e-03	2.79e-04	0.00e+00	3.06e-03	6.49e-04	3.80e-04	4.27e-03	1.93e-03	7.79e-02		2.55e-01
3	807828	802164	142670	141301	1.10e-02	5.15e-02	4.05e-03	2.59e-03	4.99e-04	0.00e+00	3.39e-03	6.15e-04	3.93e-04	4.25e-03	1.92e-03	8.02e-02		3.35e-01
4	806932	803903	142808	148472	1.09e-02	5.56e-02	4.32e-03	4.16e-03	3.84e-04	0.00e+00	4.03e-03	6.08e-04	4.64e-04	4.91e-03	2.13e-03	8.75e-02		4.22e-01
5	810456	810190	144509	147538	1.15e-02	5.11e-02	4.31e-03	2.51e-03	4.88e-04	0.00e+00	4.01e-03	7.64e-04	4.50e-04	5.25e-03	2.19e-03	8.26e-02		5.05e-01
6	804973	808015	141791	142057	1.17e-02	5.75e-02	4.38e-03	2.19e-03	3.77e-04	0.00e+00	3.89e-03	5.82e-04	4.19e-04	4.81e-03	2.60e-02	1.12e-01		6.17e-01
7	811759	812228	148080	145038	3.05e-02	5.07e-02	4.25e-03	1.94e-03	6.54e-04	0.00e+00	3.80e-03	5.52e-04	4.96e-04	4.28e-03	2.08e-03	9.92e-02		7.16e-01
8	810938	803597	142100	141631	1.09e-02	5.69e-02	4.31e-03	2.87e-03	8.22e-04	0.00e+00	3.67e-03	7.59e-04	4.12e-04	4.46e-03	2.08e-03	8.73e-02		8.03e-01
9	814547	812055	143727	151068	1.08e-02	5.04e-02	4.28e-03	2.14e-03	1.52e-03	0.00e+00	3.55e-03	5.68e-04	4.07e-04	4.34e-03	2.11e-03	8.01e-02		8.83e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.6031 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.6031 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.603131  0.000000
];

