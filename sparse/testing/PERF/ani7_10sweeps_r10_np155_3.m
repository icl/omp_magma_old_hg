% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_155 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	819106	802941	141586	141586	7.86e-03	3.67e-02	7.63e-03	1.49e-02	7.19e-04	0.00e+00	3.17e-03	4.78e-04	3.92e-04	9.55e-03	2.09e-03	8.35e-02		8.35e-02
1	820877	803083	129112	145277	8.68e-03	3.11e-02	3.94e-03	4.55e-03	8.68e-04	0.00e+00	3.50e-03	4.39e-04	3.62e-04	5.40e-03	2.27e-03	6.11e-02		1.45e-01
2	810494	813133	128147	145941	8.70e-03	4.64e-02	4.39e-03	2.59e-03	2.73e-04	0.00e+00	4.25e-03	4.66e-04	4.32e-04	4.03e-03	2.40e-03	7.40e-02		2.19e-01
3	793154	815293	139336	136697	8.69e-03	4.97e-02	4.69e-03	1.93e-03	4.96e-04	0.00e+00	4.72e-03	5.42e-04	4.56e-04	3.96e-03	2.32e-03	7.75e-02		2.96e-01
4	808796	814190	157482	135343	8.81e-03	5.36e-02	4.77e-03	4.68e-03	4.47e-04	0.00e+00	5.04e-03	6.06e-04	4.61e-04	4.98e-03	2.36e-03	8.58e-02		3.82e-01
5	810575	814105	142645	137251	8.73e-03	4.64e-02	5.01e-03	1.73e-03	1.14e-03	0.00e+00	4.07e-03	5.92e-04	4.72e-04	4.33e-03	2.29e-03	7.48e-02		4.57e-01
6	809614	813641	141672	138142	7.81e-03	4.96e-02	5.06e-03	2.17e-03	4.11e-04	0.00e+00	4.04e-03	6.28e-04	4.66e-04	4.31e-03	2.21e-03	7.67e-02		5.33e-01
7	809386	808803	143439	139412	7.88e-03	4.57e-02	5.08e-03	2.21e-03	4.12e-04	0.00e+00	4.06e-03	5.37e-04	4.66e-04	4.19e-03	2.21e-03	7.28e-02		6.06e-01
8	814377	810727	144473	145056	7.84e-03	4.95e-02	5.02e-03	1.53e-03	1.12e-03	0.00e+00	4.02e-03	5.17e-04	4.38e-04	4.02e-03	2.25e-03	7.63e-02		6.82e-01
9	810456	811800	140288	143938	7.87e-03	4.61e-02	5.11e-03	1.70e-03	1.17e-03	0.00e+00	4.10e-03	6.27e-04	4.62e-04	3.96e-03	2.21e-03	7.33e-02		7.56e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.3886 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.3886 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.388570  0.000000
];

