% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_6 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	831190	806593	141586	141586	4.61e-03	1.67e-01	3.73e-02	1.15e-02	3.21e-03	0.00e+00	3.97e-02	3.00e-04	3.99e-03	1.53e-02	2.39e-02	3.07e-01		3.07e-01
1	808294	816854	117028	141625	4.72e-03	2.10e-01	3.69e-02	8.94e-03	4.02e-03	0.00e+00	4.73e-02	2.58e-04	4.26e-03	1.50e-02	3.03e-02	3.62e-01		6.68e-01
2	794874	809664	140730	132170	4.96e-03	3.50e-01	4.59e-02	7.96e-03	3.36e-03	0.00e+00	5.69e-02	3.12e-04	4.71e-03	1.43e-02	3.18e-02	5.21e-01		1.19e+00
3	808039	802431	154956	140166	4.88e-03	4.72e-01	5.01e-02	8.79e-03	4.52e-03	0.00e+00	6.64e-02	3.65e-04	5.20e-03	1.42e-02	3.06e-02	6.57e-01		1.85e+00
4	808166	808438	142597	148205	4.84e-03	4.65e-01	5.28e-02	9.69e-03	5.87e-03	0.00e+00	7.10e-02	2.93e-04	5.49e-03	1.43e-02	3.00e-02	6.60e-01		2.51e+00
5	807513	809498	143275	143003	4.87e-03	4.69e-01	5.33e-02	1.02e-02	5.82e-03	0.00e+00	6.97e-02	3.56e-04	5.43e-03	1.51e-02	3.02e-02	6.64e-01		3.17e+00
6	812301	812397	144734	142749	4.87e-03	4.77e-01	5.37e-02	1.00e-02	6.23e-03	0.00e+00	7.30e-02	3.05e-04	5.52e-03	1.43e-02	3.07e-02	6.75e-01		3.85e+00
7	811752	812915	140752	140656	4.92e-03	4.95e-01	5.46e-02	8.71e-03	5.61e-03	0.00e+00	7.06e-02	3.46e-04	5.40e-03	1.42e-02	3.07e-02	6.90e-01		4.54e+00
8	806754	812433	142107	140944	4.89e-03	4.98e-01	5.47e-02	8.24e-03	5.26e-03	0.00e+00	6.75e-02	5.12e-04	5.29e-03	1.43e-02	3.04e-02	6.89e-01		5.22e+00
9	815373	814323	147911	142232	4.95e-03	4.86e-01	5.44e-02	8.55e-03	6.31e-03	0.00e+00	7.06e-02	4.33e-04	5.39e-03	1.43e-02	3.09e-02	6.81e-01		5.91e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 6.4317 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 6.4317 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  6.431696  0.000000
];

