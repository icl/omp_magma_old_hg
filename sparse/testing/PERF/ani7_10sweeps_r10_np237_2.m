% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_237 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	825700	799548	141586	141586	1.10e-02	4.28e-02	8.96e-03	1.90e-02	1.74e-03	0.00e+00	2.28e-03	4.78e-04	3.75e-04	1.15e-02	1.75e-03	9.99e-02		9.99e-02
1	800005	804533	122518	148670	1.06e-02	3.56e-02	3.38e-03	5.71e-03	2.03e-03	0.00e+00	2.84e-03	4.93e-04	3.89e-04	7.09e-03	1.90e-03	7.01e-02		1.70e-01
2	800963	786154	149019	144491	1.06e-02	5.10e-02	3.68e-03	2.18e-03	9.11e-04	0.00e+00	3.18e-03	7.05e-04	3.99e-04	4.21e-03	1.93e-03	7.88e-02		2.49e-01
3	796112	804648	148867	163676	1.04e-02	4.87e-02	3.85e-03	2.89e-03	3.84e-04	0.00e+00	3.52e-03	5.71e-04	4.11e-04	4.10e-03	2.06e-03	7.69e-02		3.26e-01
4	808216	805072	154524	145988	1.07e-02	5.45e-02	4.57e-03	3.32e-03	5.29e-04	0.00e+00	3.74e-03	6.26e-04	4.50e-04	4.23e-03	2.16e-03	8.48e-02		4.10e-01
5	815722	808175	143225	146369	1.06e-02	4.91e-02	4.36e-03	1.69e-03	4.92e-04	0.00e+00	3.79e-03	6.57e-04	4.54e-04	4.99e-03	2.20e-03	7.84e-02		4.89e-01
6	809767	807681	136525	144072	1.06e-02	5.74e-02	4.47e-03	1.80e-03	4.37e-04	0.00e+00	3.70e-03	7.04e-04	4.12e-04	4.55e-03	2.15e-03	8.62e-02		5.75e-01
7	811345	810313	143286	145372	1.07e-02	5.03e-02	4.46e-03	2.75e-03	4.44e-04	0.00e+00	3.81e-03	5.83e-04	4.50e-04	4.23e-03	2.18e-03	7.99e-02		6.55e-01
8	812888	814866	142514	143546	1.07e-02	5.57e-02	4.52e-03	1.53e-03	1.46e-03	0.00e+00	3.85e-03	7.58e-04	4.09e-04	4.44e-03	2.23e-03	8.56e-02		7.41e-01
9	813679	810373	141777	139799	1.08e-02	5.14e-02	4.61e-03	1.80e-03	1.60e-03	0.00e+00	3.78e-03	7.01e-04	4.05e-04	4.22e-03	2.18e-03	8.15e-02		8.22e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.5790 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.5790 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.578980  0.000000
];

