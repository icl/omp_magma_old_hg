% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_225 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	819921	806819	141586	141586	1.03e-02	4.19e-02	9.02e-03	1.99e-02	1.00e-03	0.00e+00	2.40e-03	4.79e-04	3.72e-04	1.28e-02	1.75e-03	9.99e-02		9.99e-02
1	806659	789725	128297	141399	1.02e-02	3.43e-02	3.50e-03	5.31e-03	1.18e-03	0.00e+00	3.02e-03	4.47e-04	3.50e-04	5.99e-03	1.95e-03	6.63e-02		1.66e-01
2	807693	815370	142365	159299	1.00e-02	5.09e-02	3.68e-03	3.49e-03	3.77e-04	0.00e+00	3.32e-03	3.84e-04	4.23e-04	4.29e-03	2.04e-03	7.89e-02		2.45e-01
3	808969	798685	142137	134460	1.03e-02	5.32e-02	4.45e-03	2.29e-03	6.12e-04	0.00e+00	3.70e-03	4.20e-04	4.58e-04	4.20e-03	1.97e-03	8.16e-02		3.27e-01
4	810752	806698	141667	151951	1.01e-02	5.52e-02	4.12e-03	2.37e-03	4.24e-04	0.00e+00	3.92e-03	6.29e-04	4.30e-04	5.24e-03	2.03e-03	8.45e-02		4.11e-01
5	814006	813152	140689	144743	1.02e-02	4.98e-02	7.16e-03	2.61e-03	1.56e-03	0.00e+00	4.05e-03	9.25e-04	4.26e-04	4.64e-03	2.04e-03	8.34e-02		4.95e-01
6	806977	810793	138241	139095	1.03e-02	5.54e-02	4.48e-03	2.54e-03	3.40e-04	0.00e+00	3.77e-03	7.95e-04	4.11e-04	4.65e-03	2.01e-03	8.47e-02		5.79e-01
7	810565	811763	146076	142260	1.03e-02	5.07e-02	4.60e-03	2.96e-03	4.76e-04	0.00e+00	3.98e-03	8.61e-04	4.24e-04	4.46e-03	2.03e-03	8.07e-02		6.60e-01
8	807260	815504	143294	142096	1.03e-02	5.54e-02	4.64e-03	2.40e-03	1.79e-03	0.00e+00	4.11e-03	7.28e-04	4.52e-04	4.28e-03	2.04e-03	8.61e-02		7.46e-01
9	815652	809168	147405	139161	1.19e-02	5.03e-02	4.63e-03	2.80e-03	1.40e-03	0.00e+00	3.98e-03	5.28e-04	4.15e-04	4.25e-03	2.02e-03	8.23e-02		8.28e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.5808 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.5808 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.580830  0.000000
];

