% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_211 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	819206	811866	141586	141586	9.82e-03	3.98e-02	8.33e-03	1.76e-02	1.94e-03	0.00e+00	2.59e-03	4.75e-04	4.21e-04	1.35e-02	1.97e-03	9.64e-02		9.64e-02
1	811456	801244	129012	136352	1.21e-02	3.57e-02	3.69e-03	3.38e-03	9.06e-04	0.00e+00	3.37e-03	3.94e-04	3.55e-04	6.37e-03	2.15e-03	6.85e-02		1.65e-01
2	809779	804618	137568	147780	9.65e-03	5.41e-02	3.92e-03	2.78e-03	4.00e-04	0.00e+00	3.59e-03	5.71e-04	3.96e-04	6.47e-03	2.15e-03	8.40e-02		2.49e-01
3	817714	799637	140051	145212	9.66e-03	5.40e-02	4.40e-03	2.18e-03	6.40e-04	0.00e+00	3.85e-03	5.53e-04	4.80e-04	4.29e-03	2.14e-03	8.22e-02		3.31e-01
4	813081	808522	132922	150999	9.69e-03	5.65e-02	4.35e-03	2.16e-03	5.18e-04	0.00e+00	4.15e-03	7.12e-04	4.62e-04	4.24e-03	2.07e-03	8.48e-02		4.16e-01
5	809621	809808	138360	142919	9.73e-03	5.09e-02	4.60e-03	3.59e-03	4.86e-04	0.00e+00	4.16e-03	7.90e-04	4.50e-04	4.88e-03	2.08e-03	8.16e-02		4.98e-01
6	811169	809930	142626	142439	9.74e-03	5.50e-02	4.60e-03	2.67e-03	4.41e-04	0.00e+00	4.12e-03	5.75e-04	4.68e-04	4.25e-03	2.12e-03	8.40e-02		5.82e-01
7	813377	815993	141884	143123	9.77e-03	5.18e-02	4.57e-03	2.45e-03	3.99e-04	0.00e+00	4.22e-03	5.05e-04	4.82e-04	4.37e-03	2.15e-03	8.08e-02		6.62e-01
8	814000	814531	140482	137866	9.82e-03	5.64e-02	4.70e-03	3.07e-03	1.72e-03	0.00e+00	4.26e-03	7.80e-04	4.42e-04	4.39e-03	2.14e-03	8.77e-02		7.50e-01
9	812888	814134	140665	140134	9.81e-03	5.28e-02	4.64e-03	2.33e-03	1.38e-03	0.00e+00	4.23e-03	5.40e-04	4.44e-04	4.31e-03	2.14e-03	8.27e-02		8.33e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.5784 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.5784 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.578421  0.000000
];

