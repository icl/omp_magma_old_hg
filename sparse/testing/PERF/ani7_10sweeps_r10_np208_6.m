% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_208 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	821617	803474	141586	141586	9.53e-03	4.07e-02	8.32e-03	1.82e-02	9.25e-04	0.00e+00	2.61e-03	4.27e-04	4.17e-04	1.24e-02	1.95e-03	9.55e-02		9.55e-02
1	804124	803982	126601	144744	9.51e-03	3.53e-02	3.72e-03	6.13e-03	1.70e-03	0.00e+00	3.24e-03	4.17e-04	4.07e-04	7.10e-03	2.16e-03	6.97e-02		1.65e-01
2	816271	810128	144900	145042	9.52e-03	5.48e-02	3.91e-03	2.49e-03	4.54e-04	0.00e+00	3.61e-03	6.12e-04	4.31e-04	4.30e-03	2.21e-03	8.23e-02		2.48e-01
3	807135	803393	133559	139702	9.61e-03	5.51e-02	4.70e-03	2.36e-03	3.92e-04	0.00e+00	4.03e-03	7.60e-04	4.74e-04	4.36e-03	2.14e-03	8.40e-02		3.32e-01
4	817986	810889	143501	147243	9.49e-03	5.66e-02	4.22e-03	3.19e-03	7.25e-04	0.00e+00	4.25e-03	6.80e-04	4.55e-04	4.72e-03	2.11e-03	8.64e-02		4.18e-01
5	811297	812255	133455	140552	9.57e-03	5.13e-02	4.28e-03	3.47e-03	4.69e-04	0.00e+00	4.28e-03	7.92e-04	4.72e-04	5.01e-03	2.14e-03	8.18e-02		5.00e-01
6	813379	804646	140950	139992	9.62e-03	5.86e-02	4.28e-03	2.65e-03	4.75e-04	0.00e+00	4.37e-03	6.61e-04	4.37e-04	4.80e-03	2.15e-03	8.80e-02		5.88e-01
7	810160	810429	139674	148407	9.52e-03	5.24e-02	4.24e-03	2.71e-03	4.54e-04	0.00e+00	4.18e-03	5.71e-04	4.64e-04	4.22e-03	2.15e-03	8.09e-02		6.69e-01
8	814327	814061	143699	143430	9.57e-03	5.67e-02	4.28e-03	2.07e-03	4.03e-04	0.00e+00	4.20e-03	7.68e-04	4.59e-04	4.44e-03	2.16e-03	8.50e-02		7.54e-01
9	813035	810167	140338	140604	9.61e-03	5.33e-02	4.35e-03	2.22e-03	1.31e-03	0.00e+00	4.33e-03	6.90e-04	4.65e-04	4.25e-03	2.14e-03	8.27e-02		8.36e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.5773 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.5773 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.577343  0.000000
];

