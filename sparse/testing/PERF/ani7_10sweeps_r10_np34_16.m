% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_34 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	809206	806719	141586	141586	3.30e-03	3.77e-02	6.99e-03	6.81e-03	6.49e-04	0.00e+00	7.02e-03	3.41e-04	7.36e-04	5.92e-03	4.07e-03	7.35e-02		7.35e-02
1	820573	821939	139012	141499	3.34e-03	4.44e-02	6.13e-03	2.15e-03	7.67e-04	0.00e+00	8.49e-03	2.32e-04	8.53e-04	4.84e-03	5.34e-03	7.65e-02		1.50e-01
2	807712	802169	128451	127085	3.43e-03	7.02e-02	8.49e-03	2.15e-03	6.89e-04	0.00e+00	1.06e-02	2.52e-04	9.15e-04	4.18e-03	5.73e-03	1.07e-01		2.57e-01
3	806884	799940	142118	147661	3.32e-03	1.03e-01	8.76e-03	1.97e-03	8.75e-04	0.00e+00	1.20e-02	3.22e-04	1.01e-03	4.13e-03	5.41e-03	1.40e-01		3.97e-01
4	805549	812484	143752	150696	3.36e-03	9.50e-02	9.07e-03	2.14e-03	1.17e-03	0.00e+00	1.27e-02	2.86e-04	1.08e-03	4.71e-03	5.31e-03	1.35e-01		5.32e-01
5	813418	812217	145892	138957	3.38e-03	9.51e-02	9.27e-03	2.72e-03	1.48e-03	0.00e+00	1.31e-02	3.73e-04	1.05e-03	4.48e-03	5.43e-03	1.36e-01		6.68e-01
6	812311	816064	138829	140030	3.36e-03	9.83e-02	9.44e-03	2.72e-03	1.18e-03	0.00e+00	1.32e-02	3.37e-04	1.05e-03	4.34e-03	5.42e-03	1.39e-01		8.07e-01
7	811470	808641	140742	136989	3.38e-03	9.94e-02	9.52e-03	1.93e-03	1.13e-03	0.00e+00	1.30e-02	3.78e-04	1.02e-03	4.19e-03	5.39e-03	1.39e-01		9.47e-01
8	813064	810129	142389	145218	3.37e-03	1.00e-01	9.48e-03	2.67e-03	1.54e-03	0.00e+00	1.30e-02	3.69e-04	1.04e-03	4.24e-03	5.37e-03	1.41e-01		1.09e+00
9	815857	812262	141601	144536	3.38e-03	9.98e-02	9.53e-03	2.06e-03	1.51e-03	0.00e+00	1.30e-02	4.59e-04	1.04e-03	4.18e-03	5.43e-03	1.40e-01		1.23e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.6059 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.6059 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.605859  0.000000
];

