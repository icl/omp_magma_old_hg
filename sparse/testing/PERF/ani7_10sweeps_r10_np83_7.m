% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_83 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	809404	806782	141586	141586	6.57e-03	3.02e-02	6.59e-03	9.44e-03	6.10e-04	0.00e+00	3.16e-03	3.04e-04	5.25e-04	6.52e-03	2.51e-03	6.64e-02		6.64e-02
1	817154	812226	138814	141436	5.28e-03	2.75e-02	4.60e-03	4.18e-03	7.51e-04	0.00e+00	3.78e-03	2.59e-04	4.96e-04	3.82e-03	2.64e-03	5.33e-02		1.20e-01
2	813580	813854	131870	136798	5.31e-03	4.85e-02	5.07e-03	1.61e-03	3.72e-04	0.00e+00	4.46e-03	4.58e-04	4.62e-04	3.42e-03	2.71e-03	7.23e-02		1.92e-01
3	805341	817394	136250	135976	5.29e-03	5.84e-02	5.34e-03	1.33e-03	6.08e-04	0.00e+00	5.33e-03	2.51e-04	5.99e-04	3.29e-03	2.80e-03	8.33e-02		2.75e-01
4	808675	807537	145295	133242	5.34e-03	5.52e-02	5.74e-03	1.51e-03	5.43e-04	0.00e+00	5.42e-03	3.09e-04	5.70e-04	3.83e-03	2.82e-03	8.13e-02		3.57e-01
5	813505	809856	142766	143904	5.28e-03	5.09e-02	5.89e-03	1.35e-03	1.09e-03	0.00e+00	5.49e-03	5.30e-04	5.71e-04	3.52e-03	2.81e-03	7.75e-02		4.34e-01
6	812867	807834	138742	142391	5.29e-03	5.48e-02	5.99e-03	2.13e-03	5.55e-04	0.00e+00	5.43e-03	4.26e-04	5.41e-04	3.49e-03	2.78e-03	8.14e-02		5.16e-01
7	815565	812482	140186	145219	5.29e-03	5.25e-02	5.92e-03	1.44e-03	4.94e-04	0.00e+00	5.36e-03	4.82e-04	5.36e-04	3.51e-03	2.87e-03	7.84e-02		5.94e-01
8	813362	812241	138294	141377	5.30e-03	5.53e-02	6.05e-03	2.74e-03	1.06e-03	0.00e+00	5.48e-03	4.55e-04	5.78e-04	3.43e-03	2.83e-03	8.32e-02		6.77e-01
9	813586	815578	141303	142424	5.33e-03	5.27e-02	6.00e-03	1.55e-03	1.11e-03	0.00e+00	5.57e-03	5.54e-04	6.04e-04	3.27e-03	2.87e-03	7.96e-02		7.57e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.2297 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.2297 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.229685  0.000000
];

