% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_53 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	809472	803544	141586	141586	3.27e-03	2.90e-02	5.08e-03	6.60e-03	4.64e-04	0.00e+00	4.54e-03	2.61e-04	5.41e-04	5.55e-03	2.71e-03	5.80e-02		5.80e-02
1	819841	801152	138746	144674	3.25e-03	3.29e-02	3.95e-03	1.80e-03	5.11e-04	0.00e+00	5.51e-03	2.27e-04	5.82e-04	4.30e-03	3.56e-03	5.66e-02		1.15e-01
2	806014	809862	129183	147872	3.30e-03	5.39e-02	5.19e-03	2.01e-03	4.45e-04	0.00e+00	6.80e-03	3.17e-04	5.81e-04	3.47e-03	3.80e-03	7.98e-02		1.94e-01
3	809984	804558	143816	139968	3.29e-03	7.24e-02	5.94e-03	1.72e-03	5.63e-04	0.00e+00	7.84e-03	3.28e-04	6.90e-04	3.42e-03	3.61e-03	9.98e-02		2.94e-01
4	807547	813127	140652	146078	3.28e-03	6.83e-02	6.00e-03	1.69e-03	8.64e-04	0.00e+00	8.54e-03	3.40e-04	7.75e-04	3.77e-03	3.47e-03	9.70e-02		3.91e-01
5	810394	809147	143894	138314	3.30e-03	6.67e-02	6.04e-03	1.80e-03	9.17e-04	0.00e+00	8.37e-03	4.49e-04	7.39e-04	3.62e-03	3.48e-03	9.54e-02		4.87e-01
6	810548	810615	141853	143100	3.28e-03	6.84e-02	6.06e-03	1.70e-03	7.86e-04	0.00e+00	8.66e-03	3.02e-04	7.48e-04	3.65e-03	3.50e-03	9.71e-02		5.84e-01
7	812477	813043	142505	142438	3.29e-03	6.81e-02	6.10e-03	1.75e-03	8.18e-04	0.00e+00	8.40e-03	2.31e-04	7.23e-04	3.41e-03	3.56e-03	9.64e-02		6.80e-01
8	812521	811067	141382	140816	3.31e-03	7.06e-02	6.17e-03	2.01e-03	1.13e-03	0.00e+00	8.59e-03	3.69e-04	7.46e-04	3.50e-03	3.53e-03	1.00e-01		7.80e-01
9	815210	815292	142144	143598	3.30e-03	6.94e-02	6.20e-03	2.07e-03	1.25e-03	0.00e+00	8.45e-03	3.39e-04	7.30e-04	3.39e-03	3.59e-03	9.88e-02		8.79e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.2503 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.2503 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.250298  0.000000
];

