% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_69 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	818900	803928	141586	141586	5.11e-03	3.05e-02	6.34e-03	9.65e-03	9.89e-04	0.00e+00	3.71e-03	3.90e-04	5.67e-04	5.85e-03	2.82e-03	6.59e-02		6.59e-02
1	792223	826210	129318	144290	5.06e-03	3.01e-02	5.38e-03	2.25e-03	1.11e-03	0.00e+00	4.28e-03	3.04e-04	5.48e-04	4.14e-03	2.84e-03	5.60e-02		1.22e-01
2	812780	809254	156801	122814	5.18e-03	5.06e-02	5.47e-03	2.29e-03	7.06e-04	0.00e+00	5.29e-03	4.60e-04	5.41e-04	3.55e-03	3.04e-03	7.72e-02		1.99e-01
3	800701	805981	137050	140576	5.11e-03	6.18e-02	5.54e-03	1.26e-03	5.20e-04	0.00e+00	6.05e-03	3.37e-04	6.47e-04	3.46e-03	2.81e-03	8.76e-02		2.87e-01
4	804925	807936	149935	144655	5.06e-03	5.93e-02	5.45e-03	4.03e-03	7.69e-04	0.00e+00	6.56e-03	4.54e-04	6.60e-04	3.46e-03	2.68e-03	8.84e-02		3.75e-01
5	812396	809905	146516	143505	5.05e-03	5.44e-02	5.33e-03	2.61e-03	7.37e-04	0.00e+00	6.57e-03	4.61e-04	6.68e-04	3.71e-03	2.81e-03	8.24e-02		4.57e-01
6	804836	809720	139851	142342	5.12e-03	5.81e-02	5.36e-03	1.64e-03	8.27e-04	0.00e+00	6.57e-03	3.18e-04	6.66e-04	3.58e-03	2.75e-03	8.49e-02		5.42e-01
7	811861	812284	148217	143333	5.09e-03	5.64e-02	5.26e-03	2.61e-03	6.56e-04	0.00e+00	6.54e-03	3.39e-04	6.51e-04	3.56e-03	2.80e-03	8.39e-02		6.26e-01
8	812056	812087	141998	141575	5.12e-03	5.96e-02	5.30e-03	2.00e-03	9.00e-04	0.00e+00	6.51e-03	4.93e-04	6.54e-04	3.57e-03	2.82e-03	8.69e-02		7.13e-01
9	818246	814221	142609	142578	5.09e-03	5.87e-02	5.27e-03	1.50e-03	1.09e-03	0.00e+00	6.59e-03	5.63e-04	6.51e-04	3.51e-03	2.86e-03	8.58e-02		7.99e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.2696 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.2696 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.269640  0.000000
];

