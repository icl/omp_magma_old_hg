% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_69 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	819105	803865	141586	141586	5.05e-03	3.05e-02	6.40e-03	9.61e-03	7.32e-04	0.00e+00	3.79e-03	3.62e-04	5.92e-04	5.92e-03	2.81e-03	6.58e-02		6.58e-02
1	816110	790205	129113	144353	5.01e-03	2.95e-02	5.40e-03	2.09e-03	5.43e-04	0.00e+00	4.57e-03	2.93e-04	5.36e-04	3.99e-03	2.85e-03	5.48e-02		1.21e-01
2	799348	791510	132914	158819	4.98e-03	5.05e-02	5.43e-03	2.18e-03	3.93e-04	0.00e+00	5.24e-03	2.68e-04	5.27e-04	3.55e-03	2.97e-03	7.60e-02		1.97e-01
3	813676	811399	150482	158320	4.97e-03	5.93e-02	5.32e-03	1.53e-03	7.71e-04	0.00e+00	6.14e-03	4.26e-04	6.21e-04	3.54e-03	2.89e-03	8.55e-02		2.82e-01
4	808083	808602	136960	139237	5.12e-03	6.35e-02	5.62e-03	1.78e-03	6.96e-04	0.00e+00	6.67e-03	5.12e-04	6.73e-04	3.96e-03	2.69e-03	9.12e-02		3.73e-01
5	809444	807771	143358	142839	5.08e-03	5.42e-02	5.36e-03	3.41e-03	1.15e-03	0.00e+00	6.56e-03	3.91e-04	6.52e-04	3.71e-03	2.75e-03	8.33e-02		4.57e-01
6	808097	804931	142803	144476	5.12e-03	5.70e-02	5.37e-03	1.37e-03	6.58e-04	0.00e+00	6.44e-03	3.90e-04	6.46e-04	3.65e-03	2.69e-03	8.33e-02		5.40e-01
7	809417	813256	144956	148122	5.06e-03	5.58e-02	5.28e-03	1.51e-03	6.95e-04	0.00e+00	6.41e-03	3.84e-04	6.22e-04	3.73e-03	2.79e-03	8.23e-02		6.22e-01
8	808163	813419	144442	140603	5.12e-03	5.88e-02	5.29e-03	1.87e-03	1.08e-03	0.00e+00	6.63e-03	5.78e-04	6.65e-04	3.66e-03	2.76e-03	8.64e-02		7.09e-01
9	813175	812946	146502	141246	5.17e-03	5.72e-02	5.29e-03	2.59e-03	1.06e-03	0.00e+00	6.62e-03	4.54e-04	6.34e-04	3.44e-03	2.81e-03	8.52e-02		7.94e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.2746 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.2746 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.274609  0.000000
];

