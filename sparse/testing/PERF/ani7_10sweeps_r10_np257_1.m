% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_257 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	811361	807661	141586	141586	1.14e-02	4.52e-02	9.27e-03	2.06e-02	1.89e-03	0.00e+00	2.17e-03	5.26e-04	3.39e-04	1.39e-02	1.63e-03	1.07e-01		1.07e-01
1	805846	801864	136857	140557	1.13e-02	3.49e-02	3.11e-03	3.49e-03	3.41e-03	0.00e+00	2.67e-03	3.85e-04	3.57e-04	7.71e-03	1.87e-03	6.92e-02		1.76e-01
2	811360	804063	143178	147160	1.12e-02	5.17e-02	3.56e-03	2.86e-03	6.20e-04	0.00e+00	2.98e-03	4.01e-04	4.29e-04	4.28e-03	2.03e-03	8.00e-02		2.56e-01
3	795130	819224	138470	145767	1.12e-02	5.19e-02	4.12e-03	3.10e-03	3.82e-04	0.00e+00	3.22e-03	4.12e-04	4.58e-04	4.21e-03	2.02e-03	8.10e-02		3.37e-01
4	813151	813965	155506	131412	1.14e-02	5.46e-02	4.15e-03	3.31e-03	3.58e-04	0.00e+00	3.50e-03	7.03e-04	4.49e-04	4.65e-03	2.07e-03	8.52e-02		4.22e-01
5	808314	815059	138290	137476	1.13e-02	5.03e-02	4.29e-03	4.57e-03	5.38e-04	0.00e+00	3.50e-03	6.53e-04	4.41e-04	4.98e-03	2.06e-03	8.27e-02		5.05e-01
6	810198	809987	143933	137188	1.14e-02	5.68e-02	4.32e-03	1.81e-03	5.02e-04	0.00e+00	3.55e-03	4.89e-04	4.71e-04	4.64e-03	2.12e-03	8.61e-02		5.91e-01
7	810784	809966	142855	143066	1.14e-02	4.98e-02	4.35e-03	2.15e-03	4.50e-04	0.00e+00	3.45e-03	5.35e-04	4.26e-04	4.23e-03	2.12e-03	7.89e-02		6.70e-01
8	811519	811925	143075	143893	1.13e-02	5.61e-02	4.32e-03	1.76e-03	4.57e-04	0.00e+00	3.44e-03	5.81e-04	4.34e-04	4.65e-03	2.08e-03	8.51e-02		7.55e-01
9	811200	813527	143146	142740	1.13e-02	5.08e-02	4.34e-03	1.84e-03	1.55e-03	0.00e+00	3.55e-03	8.61e-04	4.75e-04	4.27e-03	2.10e-03	8.12e-02		8.36e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.5902 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.5902 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.590233  0.000000
];

