% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_247 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	809380	821255	141586	141586	1.11e-02	4.58e-02	9.08e-03	2.01e-02	2.80e-03	0.00e+00	2.26e-03	6.17e-04	3.37e-04	1.24e-02	1.66e-03	1.06e-01		1.06e-01
1	804736	818596	138838	126963	1.12e-02	3.66e-02	3.27e-03	4.06e-03	2.59e-03	0.00e+00	2.85e-03	4.80e-04	3.98e-04	6.96e-03	1.88e-03	7.03e-02		1.76e-01
2	804682	807706	144288	130428	1.11e-02	5.13e-02	3.70e-03	3.11e-03	1.42e-03	0.00e+00	3.07e-03	1.46e-03	3.81e-04	4.16e-03	1.90e-03	8.16e-02		2.58e-01
3	802318	812404	145148	142124	1.09e-02	5.08e-02	3.82e-03	3.05e-03	3.14e-04	0.00e+00	3.39e-03	6.13e-04	4.24e-04	4.19e-03	2.00e-03	7.95e-02		3.38e-01
4	806916	803681	148318	138232	1.11e-02	5.77e-02	4.04e-03	2.64e-03	6.22e-04	0.00e+00	3.65e-03	4.87e-04	4.57e-04	4.20e-03	2.04e-03	8.69e-02		4.25e-01
5	813536	806664	144525	147760	1.09e-02	4.99e-02	4.17e-03	1.83e-03	3.82e-04	0.00e+00	3.59e-03	5.48e-04	4.08e-04	4.84e-03	2.07e-03	7.86e-02		5.03e-01
6	810242	811497	138711	145583	1.09e-02	5.62e-02	4.26e-03	2.50e-03	3.90e-04	0.00e+00	3.66e-03	7.04e-04	4.08e-04	4.51e-03	2.04e-03	8.56e-02		5.89e-01
7	810835	813097	142811	141556	1.11e-02	5.15e-02	4.33e-03	2.24e-03	3.79e-04	0.00e+00	3.60e-03	7.65e-04	4.13e-04	4.55e-03	2.08e-03	8.09e-02		6.70e-01
8	810546	812052	143024	140762	1.10e-02	5.73e-02	4.31e-03	1.87e-03	1.51e-03	0.00e+00	3.61e-03	6.13e-04	4.57e-04	4.33e-03	2.09e-03	8.72e-02		7.57e-01
9	817790	812039	144119	142613	1.10e-02	5.16e-02	4.35e-03	2.04e-03	1.42e-03	0.00e+00	3.66e-03	5.91e-04	4.13e-04	4.22e-03	2.08e-03	8.13e-02		8.38e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.5879 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.5879 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.587926  0.000000
];

