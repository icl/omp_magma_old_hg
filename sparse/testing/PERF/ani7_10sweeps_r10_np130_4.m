% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_130 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	815399	811277	141586	141586	5.93e-03	2.79e-02	5.76e-03	1.10e-02	1.80e-03	0.00e+00	2.78e-03	3.61e-04	3.39e-04	7.11e-03	1.76e-03	6.47e-02		6.47e-02
1	804927	812418	132819	136941	6.03e-03	2.80e-02	3.06e-03	1.44e-03	1.40e-03	0.00e+00	3.45e-03	2.94e-04	3.73e-04	4.66e-03	2.09e-03	5.08e-02		1.16e-01
2	793589	796450	144097	136606	5.99e-03	3.76e-02	3.72e-03	1.23e-03	3.89e-04	0.00e+00	3.98e-03	3.52e-04	4.42e-04	3.00e-03	2.24e-03	5.90e-02		1.74e-01
3	805167	820242	156241	153380	5.91e-03	4.54e-02	4.08e-03	2.17e-03	4.36e-04	0.00e+00	4.63e-03	3.54e-04	4.42e-04	2.97e-03	2.37e-03	6.88e-02		2.43e-01
4	808126	810276	145469	130394	6.06e-03	5.00e-02	4.40e-03	1.91e-03	6.06e-04	0.00e+00	4.88e-03	4.91e-04	5.12e-04	3.10e-03	2.24e-03	7.42e-02		3.17e-01
5	810203	812319	143315	141165	5.99e-03	4.50e-02	4.37e-03	1.66e-03	4.80e-04	0.00e+00	4.62e-03	3.99e-04	4.44e-04	3.45e-03	2.26e-03	6.86e-02		3.86e-01
6	811415	810789	142044	139928	6.00e-03	4.82e-02	4.42e-03	1.53e-03	4.52e-04	0.00e+00	4.58e-03	3.46e-04	4.74e-04	3.26e-03	2.26e-03	7.15e-02		4.58e-01
7	811929	811646	141638	142264	6.00e-03	4.67e-02	4.41e-03	2.46e-03	4.50e-04	0.00e+00	4.73e-03	3.90e-04	4.48e-04	2.96e-03	2.28e-03	7.08e-02		5.28e-01
8	812083	809434	141930	142213	5.99e-03	4.91e-02	4.42e-03	1.69e-03	4.22e-04	0.00e+00	4.87e-03	3.96e-04	4.74e-04	3.32e-03	2.30e-03	7.30e-02		6.01e-01
9	811524	814680	142582	145231	5.99e-03	4.76e-02	4.44e-03	1.93e-03	1.06e-03	0.00e+00	4.80e-03	5.34e-04	4.78e-04	2.95e-03	2.31e-03	7.20e-02		6.73e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.1532 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.1532 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.153174  0.000000
];

