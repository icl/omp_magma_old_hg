% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_7 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	817465	819903	141586	141586	4.47e-03	1.46e-01	3.20e-02	1.07e-02	2.61e-03	0.00e+00	3.53e-02	2.77e-04	3.40e-03	1.35e-02	2.06e-02	2.69e-01		2.69e-01
1	806779	798490	130753	128315	4.58e-03	1.86e-01	3.18e-02	8.69e-03	3.29e-03	0.00e+00	4.36e-02	1.87e-04	3.87e-03	1.31e-02	2.57e-02	3.21e-01		5.90e-01
2	800358	815460	142245	150534	4.65e-03	2.99e-01	3.90e-02	6.82e-03	3.21e-03	0.00e+00	5.15e-02	2.78e-04	4.03e-03	1.26e-02	2.82e-02	4.50e-01		1.04e+00
3	807674	809832	149472	134370	4.66e-03	4.31e-01	4.47e-02	7.62e-03	3.86e-03	0.00e+00	5.97e-02	4.26e-04	4.58e-03	1.25e-02	2.64e-02	5.95e-01		1.63e+00
4	802725	808077	142962	140804	4.67e-03	4.08e-01	4.59e-02	7.13e-03	4.62e-03	0.00e+00	6.44e-02	5.41e-04	4.60e-03	1.24e-02	2.58e-02	5.78e-01		2.21e+00
5	812985	810118	148716	143364	4.65e-03	3.98e-01	4.58e-02	6.95e-03	4.52e-03	0.00e+00	6.19e-02	2.50e-04	4.44e-03	1.33e-02	2.62e-02	5.66e-01		2.78e+00
6	812340	808022	139262	142129	4.66e-03	4.17e-01	4.70e-02	7.75e-03	4.52e-03	0.00e+00	6.27e-02	3.75e-04	4.51e-03	1.25e-02	2.61e-02	5.87e-01		3.37e+00
7	809024	811883	140713	145031	4.66e-03	4.15e-01	4.68e-02	7.55e-03	5.12e-03	0.00e+00	6.54e-02	3.22e-04	4.68e-03	1.29e-02	2.63e-02	5.89e-01		3.95e+00
8	811249	813591	144835	141976	4.69e-03	4.19e-01	4.69e-02	7.31e-03	5.15e-03	0.00e+00	6.43e-02	3.26e-04	4.63e-03	1.27e-02	2.64e-02	5.91e-01		4.55e+00
9	814923	816589	143416	141074	4.73e-03	4.22e-01	4.74e-02	8.59e-03	5.61e-03	0.00e+00	6.62e-02	3.77e-04	4.71e-03	1.25e-02	2.66e-02	5.99e-01		5.15e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 5.6461 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 5.6461 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  5.646070  0.000000
];

