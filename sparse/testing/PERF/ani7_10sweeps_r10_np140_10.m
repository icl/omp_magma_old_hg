% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_140 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	817971	801842	141586	141586	8.36e-03	3.43e-02	7.56e-03	1.38e-02	1.34e-03	0.00e+00	2.69e-03	5.30e-04	4.22e-04	8.49e-03	2.22e-03	7.97e-02		7.97e-02
1	819218	804392	130247	146376	8.70e-03	3.09e-02	4.22e-03	4.53e-03	1.43e-03	0.00e+00	3.17e-03	4.27e-04	3.80e-04	5.33e-03	2.34e-03	6.14e-02		1.41e-01
2	809029	805278	129806	144632	7.28e-03	4.63e-02	4.45e-03	2.05e-03	8.25e-04	0.00e+00	3.81e-03	7.20e-04	4.43e-04	3.98e-03	2.31e-03	7.21e-02		2.13e-01
3	816514	812177	140801	144552	7.29e-03	5.16e-02	4.45e-03	2.26e-03	4.56e-04	0.00e+00	4.30e-03	6.04e-04	4.69e-04	4.01e-03	2.31e-03	7.77e-02		2.91e-01
4	807799	817723	134122	138459	7.36e-03	5.28e-02	4.86e-03	1.96e-03	4.41e-04	0.00e+00	4.47e-03	4.60e-04	4.99e-04	3.99e-03	2.20e-03	7.90e-02		3.70e-01
5	808751	812893	143642	133718	7.38e-03	4.75e-02	5.05e-03	2.87e-03	4.29e-04	0.00e+00	4.58e-03	8.03e-04	4.91e-04	4.35e-03	2.16e-03	7.56e-02		4.46e-01
6	810441	811649	143496	139354	7.37e-03	5.02e-02	4.97e-03	2.30e-03	4.50e-04	0.00e+00	4.58e-03	5.48e-04	5.01e-04	4.12e-03	2.15e-03	7.72e-02		5.23e-01
7	810723	811678	142612	141404	7.38e-03	4.77e-02	5.00e-03	1.62e-03	4.16e-04	0.00e+00	4.49e-03	6.64e-04	5.08e-04	4.29e-03	2.14e-03	7.42e-02		5.97e-01
8	812699	813206	143136	142181	7.38e-03	5.08e-02	4.99e-03	1.81e-03	1.36e-03	0.00e+00	4.58e-03	4.77e-04	4.76e-04	4.03e-03	2.16e-03	7.80e-02		6.75e-01
9	814623	815941	141966	141459	7.37e-03	4.87e-02	5.05e-03	3.58e-03	1.12e-03	0.00e+00	4.51e-03	5.49e-04	5.03e-04	4.00e-03	2.19e-03	7.76e-02		7.53e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.3809 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.3809 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.380935  0.000000
];

