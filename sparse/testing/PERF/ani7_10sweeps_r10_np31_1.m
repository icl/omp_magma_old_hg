% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_31 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	818356	804281	141586	141586	3.35e-03	4.00e-02	7.60e-03	6.74e-03	7.18e-04	0.00e+00	7.69e-03	2.99e-04	8.22e-04	6.02e-03	4.54e-03	7.77e-02		7.77e-02
1	807165	803068	129862	143937	3.32e-03	4.77e-02	6.76e-03	2.53e-03	7.90e-04	0.00e+00	9.36e-03	2.15e-04	9.04e-04	5.02e-03	5.74e-03	8.24e-02		1.60e-01
2	809780	804548	141859	145956	3.42e-03	7.74e-02	9.36e-03	2.09e-03	7.89e-04	0.00e+00	1.22e-02	2.83e-04	1.03e-03	4.66e-03	6.72e-03	1.18e-01		2.78e-01
3	799994	810831	140050	145282	3.61e-03	1.20e-01	1.06e-02	3.12e-03	9.77e-04	0.00e+00	1.41e-02	3.85e-04	1.17e-03	4.65e-03	6.39e-03	1.65e-01		4.43e-01
4	801692	810657	150642	139805	3.64e-03	1.12e-01	1.05e-02	3.31e-03	1.50e-03	0.00e+00	1.50e-02	3.37e-04	1.25e-03	5.08e-03	6.15e-03	1.59e-01		6.02e-01
5	812537	810269	149749	140784	3.62e-03	1.11e-01	1.08e-02	3.22e-03	1.70e-03	0.00e+00	1.53e-02	2.37e-04	1.28e-03	4.85e-03	6.01e-03	1.58e-01		7.60e-01
6	810583	811156	139710	141978	3.63e-03	1.15e-01	1.10e-02	2.93e-03	1.31e-03	0.00e+00	1.52e-02	3.68e-04	1.26e-03	4.80e-03	6.23e-03	1.62e-01		9.22e-01
7	813152	813270	142470	141897	3.62e-03	1.15e-01	1.11e-02	2.17e-03	1.27e-03	0.00e+00	1.47e-02	3.71e-04	1.17e-03	4.64e-03	6.39e-03	1.60e-01		1.08e+00
8	808655	809954	140707	140589	3.66e-03	1.16e-01	1.12e-02	2.50e-03	1.69e-03	0.00e+00	1.47e-02	4.09e-04	1.19e-03	4.76e-03	5.95e-03	1.62e-01		1.24e+00
9	810904	810768	146010	144711	3.64e-03	1.15e-01	1.04e-02	2.66e-03	1.67e-03	0.00e+00	1.52e-02	3.70e-04	1.14e-03	4.55e-03	6.33e-03	1.61e-01		1.40e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.7940 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.7940 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.794036  0.000000
];

