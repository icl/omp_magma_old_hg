% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_32 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	806580	798239	141586	141586	3.32e-03	3.90e-02	7.36e-03	6.81e-03	8.00e-04	0.00e+00	7.49e-03	2.61e-04	8.05e-04	6.01e-03	4.28e-03	7.61e-02		7.61e-02
1	817686	812552	141638	149979	3.28e-03	4.64e-02	6.41e-03	3.49e-03	7.49e-04	0.00e+00	8.96e-03	2.75e-04	8.66e-04	4.93e-03	5.66e-03	8.10e-02		1.57e-01
2	818755	809016	131338	136472	3.43e-03	7.36e-02	8.89e-03	2.05e-03	7.35e-04	0.00e+00	1.11e-02	2.83e-04	9.55e-04	4.31e-03	6.16e-03	1.11e-01		2.69e-01
3	810287	814397	131075	140814	3.37e-03	1.11e-01	9.51e-03	2.40e-03	8.69e-04	0.00e+00	1.29e-02	3.54e-04	1.06e-03	4.31e-03	6.00e-03	1.52e-01		4.21e-01
4	804608	812507	140349	136239	3.40e-03	1.06e-01	9.87e-03	2.05e-03	1.10e-03	0.00e+00	1.32e-02	3.26e-04	1.08e-03	4.63e-03	5.63e-03	1.47e-01		5.68e-01
5	807173	809959	146833	138934	3.40e-03	1.01e-01	9.87e-03	3.93e-03	1.60e-03	0.00e+00	1.40e-02	2.81e-04	1.15e-03	4.56e-03	5.65e-03	1.45e-01		7.13e-01
6	814929	809919	145074	142288	3.40e-03	1.03e-01	9.90e-03	2.23e-03	1.24e-03	0.00e+00	1.37e-02	4.24e-04	1.09e-03	4.41e-03	5.70e-03	1.46e-01		8.59e-01
7	809885	810383	138124	143134	3.37e-03	1.05e-01	1.01e-02	3.50e-03	1.47e-03	0.00e+00	1.43e-02	2.36e-04	1.16e-03	4.37e-03	5.71e-03	1.50e-01		1.01e+00
8	812961	811647	143974	143476	3.39e-03	1.05e-01	1.00e-02	2.63e-03	1.47e-03	0.00e+00	1.37e-02	3.23e-04	1.11e-03	4.38e-03	5.73e-03	1.48e-01		1.16e+00
9	812513	815729	141704	143018	3.43e-03	1.07e-01	1.01e-02	2.06e-03	1.52e-03	0.00e+00	1.39e-02	4.56e-04	1.11e-03	4.28e-03	5.78e-03	1.49e-01		1.31e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.6844 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.6844 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.684355  0.000000
];

