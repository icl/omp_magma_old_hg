% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_12 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	819754	808927	141586	141586	4.04e-03	9.02e-02	1.90e-02	8.75e-03	1.52e-03	0.00e+00	2.06e-02	2.68e-04	2.07e-03	1.01e-02	1.22e-02	1.69e-01		1.69e-01
1	790994	817631	128464	139291	4.04e-03	1.14e-01	1.85e-02	7.98e-03	2.11e-03	0.00e+00	2.57e-02	2.77e-04	2.31e-03	8.80e-03	1.56e-02	2.00e-01		3.68e-01
2	789800	803092	158030	131393	4.18e-03	1.86e-01	2.35e-02	4.21e-03	1.91e-03	0.00e+00	3.05e-02	3.08e-04	2.43e-03	8.61e-03	1.60e-02	2.78e-01		6.46e-01
3	804470	812871	160030	146738	4.07e-03	2.40e-01	2.54e-02	4.94e-03	2.42e-03	0.00e+00	3.51e-02	3.17e-04	2.67e-03	8.35e-03	1.59e-02	3.40e-01		9.86e-01
4	806104	806343	146166	137765	5.05e-03	2.50e-01	2.76e-02	5.05e-03	3.33e-03	0.00e+00	3.84e-02	4.51e-04	2.94e-03	8.27e-03	1.52e-02	3.56e-01		1.34e+00
5	817594	809085	145337	145098	4.09e-03	2.38e-01	2.70e-02	5.61e-03	3.05e-03	0.00e+00	3.79e-02	4.24e-04	2.77e-03	8.73e-03	1.57e-02	3.43e-01		1.68e+00
6	811961	807338	134653	143162	4.12e-03	2.56e-01	2.79e-02	4.48e-03	2.88e-03	0.00e+00	3.75e-02	2.96e-04	2.78e-03	8.28e-03	1.56e-02	3.60e-01		2.04e+00
7	812030	811627	141092	145715	4.13e-03	2.54e-01	2.79e-02	4.92e-03	3.16e-03	0.00e+00	3.86e-02	4.18e-04	2.86e-03	8.67e-03	1.57e-02	3.60e-01		2.40e+00
8	812138	811140	141829	142232	4.14e-03	2.59e-01	2.81e-02	4.63e-03	3.24e-03	0.00e+00	3.80e-02	3.09e-04	2.84e-03	8.29e-03	1.57e-02	3.64e-01		2.77e+00
9	812889	814424	142527	143525	4.16e-03	2.57e-01	2.82e-02	5.23e-03	3.36e-03	0.00e+00	3.83e-02	4.26e-04	2.82e-03	8.37e-03	1.57e-02	3.63e-01		3.13e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 3.5764 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 3.5764 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  3.576400  0.000000
];

