% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_180 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	816100	808301	141586	141586	8.44e-03	3.66e-02	7.53e-03	1.56e-02	1.95e-03	0.00e+00	2.23e-03	4.54e-04	3.92e-04	9.60e-03	1.87e-03	8.46e-02		8.46e-02
1	800422	806180	132118	139917	8.47e-03	3.20e-02	3.47e-03	4.62e-03	2.10e-03	0.00e+00	2.57e-03	4.25e-04	3.83e-04	5.85e-03	2.03e-03	6.19e-02		1.47e-01
2	807898	807938	148602	142844	8.37e-03	4.53e-02	3.97e-03	1.74e-03	1.02e-03	0.00e+00	2.93e-03	4.24e-04	3.78e-04	3.96e-03	2.28e-03	7.03e-02		2.17e-01
3	807453	815851	141932	141892	8.34e-03	4.46e-02	4.39e-03	1.53e-03	3.02e-04	0.00e+00	3.24e-03	4.64e-04	4.00e-04	3.89e-03	2.27e-03	6.94e-02		2.86e-01
4	811978	821476	143183	134785	8.46e-03	4.86e-02	4.53e-03	2.36e-03	3.97e-04	0.00e+00	3.53e-03	4.25e-04	4.12e-04	3.93e-03	2.24e-03	7.49e-02		3.61e-01
5	807702	803866	139463	129965	8.51e-03	4.60e-02	4.76e-03	4.04e-03	4.09e-04	0.00e+00	3.64e-03	5.89e-04	4.62e-04	4.38e-03	2.25e-03	7.50e-02		4.36e-01
6	809022	808518	144545	148381	8.38e-03	4.89e-02	4.67e-03	2.32e-03	3.87e-04	0.00e+00	3.58e-03	5.99e-04	4.47e-04	4.10e-03	2.29e-03	7.57e-02		5.12e-01
7	812628	812013	144031	144535	8.41e-03	4.52e-02	4.71e-03	3.04e-03	3.68e-04	0.00e+00	3.48e-03	7.65e-04	4.35e-04	4.02e-03	2.34e-03	7.28e-02		5.85e-01
8	812788	811650	141231	141846	8.42e-03	5.10e-02	4.79e-03	1.68e-03	1.43e-03	0.00e+00	3.61e-03	6.83e-04	4.36e-04	3.93e-03	2.33e-03	7.83e-02		6.63e-01
9	813664	810739	141877	143015	8.40e-03	4.69e-02	4.81e-03	1.73e-03	1.22e-03	0.00e+00	3.54e-03	5.88e-04	4.24e-04	3.83e-03	2.34e-03	7.38e-02		7.37e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.3851 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.3851 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.385065  0.000000
];

