% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_89 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	823269	802977	141586	141586	5.98e-03	3.82e-02	9.07e-03	1.11e-02	1.13e-03	0.00e+00	4.10e-03	3.52e-04	4.81e-04	1.05e-02	2.46e-03	8.34e-02		8.34e-02
1	822993	784991	124949	145241	5.99e-03	3.45e-02	4.37e-03	2.70e-03	8.42e-04	0.00e+00	5.10e-03	3.15e-04	5.19e-04	5.69e-03	3.40e-03	6.34e-02		1.47e-01
2	804849	809988	126031	164033	5.89e-03	6.35e-02	5.50e-03	2.50e-03	6.07e-04	0.00e+00	5.91e-03	4.49e-04	6.52e-04	3.90e-03	3.57e-03	9.25e-02		2.39e-01
3	804353	816933	144981	139842	6.21e-03	7.11e-02	6.14e-03	1.53e-03	7.32e-04	0.00e+00	6.55e-03	3.44e-04	7.52e-04	3.45e-03	3.45e-03	1.00e-01		3.40e-01
4	804655	807717	146283	133703	6.18e-03	7.04e-02	6.34e-03	2.09e-03	9.52e-04	0.00e+00	7.33e-03	3.31e-04	7.73e-04	3.55e-03	3.29e-03	1.01e-01		4.41e-01
5	811910	809207	146786	143724	6.19e-03	6.15e-02	6.15e-03	1.97e-03	7.52e-04	0.00e+00	7.34e-03	2.20e-04	7.43e-04	3.87e-03	3.36e-03	9.21e-02		5.33e-01
6	808176	810045	140337	143040	6.07e-03	6.93e-02	6.27e-03	1.37e-03	7.22e-04	0.00e+00	7.27e-03	3.46e-04	7.12e-04	3.46e-03	3.36e-03	9.89e-02		6.32e-01
7	812453	811877	144877	143008	6.06e-03	6.49e-02	6.29e-03	1.64e-03	6.92e-04	0.00e+00	7.31e-03	4.39e-04	6.95e-04	3.66e-03	3.42e-03	9.51e-02		7.27e-01
8	813609	809224	141406	141982	6.15e-03	7.19e-02	6.35e-03	2.28e-03	1.35e-03	0.00e+00	7.17e-03	4.89e-04	7.32e-04	3.56e-03	3.38e-03	1.03e-01		8.30e-01
9	813314	817155	141056	145441	6.07e-03	6.59e-02	6.35e-03	1.71e-03	1.61e-03	0.00e+00	7.46e-03	4.65e-04	6.99e-04	3.45e-03	3.41e-03	9.72e-02		9.28e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.4337 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.4337 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.433739  0.000000
];

