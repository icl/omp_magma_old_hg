% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_101 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	811849	803086	141586	141586	5.61e-03	2.91e-02	5.96e-03	9.51e-03	1.01e-03	0.00e+00	2.75e-03	3.11e-04	4.22e-04	7.42e-03	2.15e-03	6.43e-02		6.43e-02
1	808895	809242	136369	145132	5.57e-03	2.75e-02	3.84e-03	2.29e-03	7.08e-04	0.00e+00	3.08e-03	2.56e-04	4.98e-04	4.10e-03	2.37e-03	5.02e-02		1.14e-01
2	802785	818683	140129	139782	5.61e-03	4.50e-02	4.43e-03	2.26e-03	4.02e-04	0.00e+00	3.79e-03	3.00e-04	4.66e-04	3.51e-03	2.55e-03	6.84e-02		1.83e-01
3	808614	821253	147045	131147	5.66e-03	5.18e-02	4.75e-03	1.77e-03	4.65e-04	0.00e+00	4.11e-03	4.83e-04	4.61e-04	3.14e-03	2.75e-03	7.53e-02		2.58e-01
4	806352	810600	142022	129383	5.69e-03	5.07e-02	5.11e-03	1.24e-03	5.66e-04	0.00e+00	4.54e-03	7.46e-04	4.77e-04	3.15e-03	2.60e-03	7.49e-02		3.33e-01
5	808432	805310	145089	140841	5.63e-03	4.65e-02	5.30e-03	1.55e-03	6.09e-04	0.00e+00	4.62e-03	4.47e-04	4.79e-04	3.64e-03	2.66e-03	7.14e-02		4.04e-01
6	808708	812736	143815	146937	5.56e-03	4.85e-02	5.26e-03	1.43e-03	5.27e-04	0.00e+00	4.72e-03	3.67e-04	5.25e-04	3.21e-03	2.71e-03	7.28e-02		4.77e-01
7	812162	814387	144345	140317	5.62e-03	4.74e-02	5.41e-03	2.61e-03	4.11e-04	0.00e+00	4.52e-03	3.97e-04	4.63e-04	3.47e-03	2.75e-03	7.30e-02		5.50e-01
8	810128	812179	141697	139472	5.65e-03	5.04e-02	5.43e-03	2.26e-03	8.54e-04	0.00e+00	4.56e-03	4.60e-04	4.96e-04	3.29e-03	2.70e-03	7.61e-02		6.26e-01
9	813532	812746	144537	142486	5.67e-03	4.73e-02	6.83e-03	2.17e-03	9.29e-04	0.00e+00	4.58e-03	5.37e-04	4.97e-04	3.23e-03	3.01e-03	7.48e-02		7.01e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.1723 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.1723 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.172305  0.000000
];

