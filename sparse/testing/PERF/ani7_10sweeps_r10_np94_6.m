% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_94 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	823699	816792	141586	141586	5.52e-03	2.96e-02	6.18e-03	9.88e-03	6.61e-04	0.00e+00	2.86e-03	4.02e-04	4.46e-04	6.65e-03	2.41e-03	6.46e-02		6.46e-02
1	808749	799360	124519	131426	5.56e-03	2.96e-02	4.23e-03	2.59e-03	7.16e-04	0.00e+00	3.34e-03	2.57e-04	5.10e-04	3.84e-03	2.49e-03	5.31e-02		1.18e-01
2	796914	802581	140275	149664	5.42e-03	4.63e-02	4.55e-03	1.59e-03	3.84e-04	0.00e+00	4.03e-03	4.85e-04	4.90e-04	3.29e-03	2.60e-03	6.92e-02		1.87e-01
3	800462	820057	152916	147249	5.41e-03	4.81e-02	4.97e-03	1.59e-03	4.08e-04	0.00e+00	4.48e-03	4.41e-04	5.28e-04	3.20e-03	2.78e-03	7.19e-02		2.59e-01
4	805881	809994	150174	130579	5.53e-03	5.06e-02	5.42e-03	1.68e-03	5.97e-04	0.00e+00	5.03e-03	4.75e-04	5.60e-04	3.91e-03	2.80e-03	7.66e-02		3.35e-01
5	810769	811788	145560	141447	5.50e-03	4.62e-02	5.54e-03	1.45e-03	1.09e-03	0.00e+00	4.84e-03	2.59e-04	5.34e-04	3.50e-03	2.81e-03	7.17e-02		4.07e-01
6	810858	809327	141478	140459	5.51e-03	5.18e-02	5.69e-03	1.26e-03	5.54e-04	0.00e+00	4.94e-03	4.96e-04	5.36e-04	3.46e-03	2.82e-03	7.71e-02		4.84e-01
7	808295	815097	142195	143726	5.47e-03	4.90e-02	5.74e-03	1.98e-03	6.38e-04	0.00e+00	5.01e-03	5.85e-04	5.21e-04	3.34e-03	2.89e-03	7.52e-02		5.59e-01
8	811795	812093	145564	138762	5.53e-03	5.24e-02	5.78e-03	1.67e-03	1.12e-03	0.00e+00	5.17e-03	3.96e-04	5.41e-04	3.39e-03	2.83e-03	7.88e-02		6.38e-01
9	818426	812544	142870	142572	5.50e-03	4.99e-02	5.83e-03	1.67e-03	9.85e-04	0.00e+00	4.95e-03	3.34e-04	5.12e-04	3.22e-03	2.90e-03	7.58e-02		7.14e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.1883 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.1883 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.188266  0.000000
];

