% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_261 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	824956	801106	141586	141586	1.14e-02	4.37e-02	9.19e-03	2.09e-02	1.60e-03	0.00e+00	2.46e-03	6.58e-04	3.36e-04	1.43e-02	2.56e-02	1.30e-01		1.30e-01
1	816876	811243	123262	147112	3.69e-01	3.57e-02	3.09e-03	8.40e-03	6.09e-04	0.00e+00	2.86e-03	5.01e-04	3.57e-04	6.77e-03	1.84e-03	4.30e-01		5.60e-01
2	803022	803766	132148	137781	1.14e-02	5.08e-02	3.66e-03	2.31e-03	4.03e-04	0.00e+00	3.26e-03	4.51e-04	3.85e-04	4.73e-03	2.01e-03	7.94e-02		6.39e-01
3	801882	808961	146808	146064	1.14e-02	5.20e-02	4.02e-03	3.41e-03	3.14e-04	0.00e+00	3.78e-03	6.03e-04	3.99e-04	4.22e-03	2.12e-03	8.22e-02		7.21e-01
4	819963	813257	148754	141675	1.15e-02	5.70e-02	4.15e-03	1.97e-03	1.23e-03	0.00e+00	3.80e-03	5.99e-04	3.99e-04	4.37e-03	2.10e-03	8.72e-02		8.09e-01
5	813001	809333	131478	138184	1.15e-02	5.23e-02	4.32e-03	2.64e-03	3.88e-04	0.00e+00	3.97e-03	5.24e-04	4.12e-04	6.11e-03	2.08e-03	8.43e-02		8.93e-01
6	810796	810156	139246	142914	1.16e-02	5.76e-02	4.28e-03	3.32e-03	4.55e-04	0.00e+00	3.84e-03	6.85e-04	4.63e-04	4.15e-03	2.10e-03	8.85e-02		9.81e-01
7	811284	814479	142257	142897	1.15e-02	5.30e-02	4.30e-03	1.99e-03	3.76e-04	0.00e+00	3.91e-03	7.10e-04	3.99e-04	4.76e-03	2.11e-03	8.30e-02		1.06e+00
8	817496	811548	142575	139380	1.20e-02	5.87e-02	4.35e-03	1.81e-03	1.51e-03	0.00e+00	3.77e-03	7.75e-04	4.87e-04	4.22e-03	2.15e-03	8.98e-02		1.15e+00
9	815713	814973	137169	143117	1.15e-02	5.47e-02	4.43e-03	1.64e-03	1.05e-03	0.00e+00	3.75e-03	6.58e-04	3.99e-04	4.19e-03	2.17e-03	8.45e-02		1.24e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.9904 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.9904 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.990414  0.000000
];

