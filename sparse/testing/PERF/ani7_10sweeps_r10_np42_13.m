% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_42 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	808670	808894	141586	141586	3.29e-03	3.24e-02	5.93e-03	7.67e-03	8.21e-04	0.00e+00	5.74e-03	3.25e-04	6.99e-04	6.33e-03	3.35e-03	6.66e-02		6.66e-02
1	818543	800685	139548	139324	3.29e-03	3.83e-02	5.00e-03	3.34e-03	8.55e-04	0.00e+00	6.85e-03	2.32e-04	6.96e-04	4.39e-03	4.38e-03	6.73e-02		1.34e-01
2	811096	808224	130481	148339	3.27e-03	5.99e-02	6.49e-03	2.60e-03	6.17e-04	0.00e+00	8.58e-03	2.16e-04	7.31e-04	4.16e-03	4.74e-03	9.13e-02		2.25e-01
3	802083	801256	138734	141606	3.29e-03	8.72e-02	7.42e-03	1.83e-03	6.92e-04	0.00e+00	9.72e-03	3.10e-04	8.09e-04	3.79e-03	4.36e-03	1.19e-01		3.45e-01
4	808924	807101	148553	149380	3.28e-03	8.22e-02	7.43e-03	2.62e-03	9.39e-04	0.00e+00	1.05e-02	4.02e-04	8.62e-04	3.78e-03	4.33e-03	1.16e-01		4.61e-01
5	811021	817709	142517	144340	3.30e-03	8.12e-02	7.60e-03	2.48e-03	1.05e-03	0.00e+00	1.10e-02	2.15e-04	9.01e-04	4.06e-03	4.40e-03	1.16e-01		5.77e-01
6	810709	806892	141226	134538	3.35e-03	8.48e-02	7.75e-03	2.07e-03	1.03e-03	0.00e+00	1.08e-02	2.50e-04	8.88e-04	3.76e-03	4.33e-03	1.19e-01		6.96e-01
7	808067	810174	142344	146161	3.31e-03	8.38e-02	7.69e-03	1.87e-03	1.00e-03	0.00e+00	1.08e-02	3.00e-04	8.82e-04	4.09e-03	4.42e-03	1.18e-01		8.14e-01
8	813536	809306	145792	143685	3.31e-03	8.48e-02	7.71e-03	1.94e-03	1.60e-03	0.00e+00	1.07e-02	3.10e-04	8.85e-04	3.80e-03	4.41e-03	1.19e-01		9.34e-01
9	814285	819520	141129	145359	3.33e-03	8.58e-02	7.77e-03	2.03e-03	1.12e-03	0.00e+00	1.06e-02	2.95e-04	8.77e-04	3.81e-03	4.48e-03	1.20e-01		1.05e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.4287 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.4287 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.428699  0.000000
];

