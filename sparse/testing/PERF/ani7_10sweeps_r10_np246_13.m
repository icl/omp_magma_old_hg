% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_246 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	816888	815952	141586	141586	1.09e-02	4.33e-02	8.97e-03	2.06e-02	2.32e-03	0.00e+00	2.25e-03	5.14e-04	3.47e-04	1.20e-02	1.70e-03	1.03e-01		1.03e-01
1	818154	814002	131330	132266	1.10e-02	3.55e-02	3.29e-03	4.96e-03	1.96e-03	0.00e+00	2.91e-03	5.52e-04	3.66e-04	7.06e-03	1.87e-03	6.95e-02		1.72e-01
2	796957	812632	130870	135022	1.09e-02	5.08e-02	3.73e-03	6.67e-03	1.20e-03	0.00e+00	3.47e-03	5.32e-04	3.76e-04	4.20e-03	1.96e-03	8.39e-02		2.56e-01
3	796638	810472	152873	137198	1.14e-02	5.01e-02	4.10e-03	2.52e-03	3.10e-04	0.00e+00	3.70e-03	5.37e-04	4.47e-04	5.96e-03	2.01e-03	8.11e-02		3.37e-01
4	813627	800735	153998	140164	1.13e-02	5.26e-02	4.11e-03	3.40e-03	4.65e-04	0.00e+00	3.96e-03	5.58e-04	4.66e-04	4.24e-03	2.04e-03	8.32e-02		4.21e-01
5	809328	806739	137814	150706	1.08e-02	4.78e-02	4.22e-03	2.27e-03	4.38e-04	0.00e+00	3.54e-03	6.29e-04	4.00e-04	5.18e-03	2.07e-03	7.73e-02		4.98e-01
6	804180	813607	142919	145508	1.09e-02	5.34e-02	4.26e-03	1.55e-03	4.17e-04	0.00e+00	3.78e-03	5.81e-04	4.41e-04	4.32e-03	2.09e-03	8.17e-02		5.80e-01
7	808862	814094	148873	139446	1.10e-02	4.88e-02	4.29e-03	2.23e-03	3.74e-04	0.00e+00	3.65e-03	7.16e-04	4.33e-04	4.70e-03	2.11e-03	7.83e-02		6.58e-01
8	812200	812964	144997	139765	1.09e-02	5.49e-02	4.35e-03	2.06e-03	1.38e-03	0.00e+00	3.55e-03	6.16e-04	3.90e-04	4.27e-03	2.09e-03	8.45e-02		7.42e-01
9	812583	813066	142465	141701	1.09e-02	5.03e-02	4.39e-03	2.18e-03	1.72e-03	0.00e+00	3.68e-03	7.80e-04	4.13e-04	4.13e-03	2.13e-03	8.07e-02		8.23e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.5869 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.5869 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.586901  0.000000
];

