% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_49 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	809061	810819	141586	141586	3.29e-03	3.48e-02	6.71e-03	7.74e-03	5.03e-04	0.00e+00	4.86e-03	2.93e-04	5.51e-04	6.33e-03	2.91e-03	6.80e-02		6.80e-02
1	803493	800142	139157	137399	3.33e-03	3.60e-02	4.32e-03	3.20e-03	6.37e-04	0.00e+00	6.01e-03	2.09e-04	6.10e-04	4.60e-03	3.81e-03	6.27e-02		1.31e-01
2	808704	795482	145531	148882	3.28e-03	5.84e-02	5.50e-03	1.69e-03	5.07e-04	0.00e+00	7.25e-03	2.32e-04	6.54e-04	3.55e-03	4.05e-03	8.51e-02		2.16e-01
3	808868	809298	141126	154348	3.31e-03	7.71e-02	6.12e-03	2.75e-03	6.39e-04	0.00e+00	8.45e-03	2.32e-04	7.42e-04	3.52e-03	3.86e-03	1.07e-01		3.23e-01
4	806961	802908	141768	141338	3.32e-03	7.65e-02	6.61e-03	1.88e-03	9.25e-04	0.00e+00	9.26e-03	2.68e-04	8.01e-04	4.01e-03	3.76e-03	1.07e-01		4.30e-01
5	810927	810411	144480	148533	3.30e-03	7.17e-02	6.44e-03	1.85e-03	1.21e-03	0.00e+00	9.20e-03	2.50e-04	7.41e-04	3.93e-03	3.83e-03	1.02e-01		5.32e-01
6	810269	811618	141320	141836	3.33e-03	7.63e-02	6.56e-03	2.10e-03	8.23e-04	0.00e+00	9.23e-03	4.03e-04	7.76e-04	3.71e-03	3.80e-03	1.07e-01		6.39e-01
7	811868	810985	142784	141435	3.38e-03	7.40e-02	6.57e-03	2.41e-03	8.65e-04	0.00e+00	9.09e-03	2.95e-04	7.73e-04	3.59e-03	3.82e-03	1.05e-01		7.44e-01
8	814721	811920	141991	142874	3.34e-03	7.75e-02	6.59e-03	3.04e-03	1.04e-03	0.00e+00	9.35e-03	3.29e-04	7.42e-04	3.73e-03	3.87e-03	1.10e-01		8.54e-01
9	814296	814121	139944	142745	3.34e-03	7.55e-02	6.69e-03	1.65e-03	1.19e-03	0.00e+00	9.02e-03	3.47e-04	7.08e-04	3.52e-03	3.86e-03	1.06e-01		9.60e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.3485 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.3485 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.348543  0.000000
];

