% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_96 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	815233	805229	141586	141586	5.50e-03	3.02e-02	6.09e-03	9.92e-03	8.70e-04	0.00e+00	2.81e-03	3.53e-04	4.79e-04	6.90e-03	2.32e-03	6.54e-02		6.54e-02
1	823675	814691	132985	142989	5.51e-03	2.85e-02	4.06e-03	4.71e-03	1.79e-03	0.00e+00	3.16e-03	2.78e-04	5.08e-04	4.51e-03	2.60e-03	5.56e-02		1.21e-01
2	814612	807729	125349	134333	5.53e-03	4.60e-02	4.66e-03	2.31e-03	3.95e-04	0.00e+00	4.00e-03	3.17e-04	5.01e-04	3.29e-03	2.76e-03	6.98e-02		1.91e-01
3	803135	815401	135218	142101	5.49e-03	5.05e-02	5.02e-03	1.69e-03	4.39e-04	0.00e+00	4.38e-03	4.68e-04	5.04e-04	3.19e-03	2.81e-03	7.45e-02		2.65e-01
4	807113	812466	147501	135235	5.56e-03	4.96e-02	5.23e-03	2.76e-03	4.85e-04	0.00e+00	4.68e-03	4.91e-04	5.29e-04	3.71e-03	2.74e-03	7.58e-02		3.41e-01
5	809368	812027	144328	138975	7.32e-03	4.66e-02	5.55e-03	1.65e-03	5.53e-04	0.00e+00	4.67e-03	4.47e-04	5.11e-04	3.54e-03	2.86e-03	7.37e-02		4.15e-01
6	813510	807685	142879	140220	5.52e-03	5.03e-02	5.63e-03	1.66e-03	5.97e-04	0.00e+00	4.75e-03	4.00e-04	5.11e-04	3.47e-03	2.82e-03	7.56e-02		4.90e-01
7	812096	811957	139543	145368	5.50e-03	4.87e-02	5.56e-03	1.34e-03	4.94e-04	0.00e+00	4.77e-03	5.64e-04	5.08e-04	3.17e-03	2.86e-03	7.35e-02		5.64e-01
8	811112	810314	141763	141902	5.54e-03	5.10e-02	5.64e-03	1.98e-03	4.91e-04	0.00e+00	4.59e-03	3.98e-04	5.02e-04	3.54e-03	2.75e-03	7.64e-02		6.40e-01
9	817535	812069	143553	144351	5.51e-03	4.91e-02	5.61e-03	2.33e-03	8.23e-04	0.00e+00	4.84e-03	4.75e-04	5.14e-04	3.31e-03	2.82e-03	7.54e-02		7.16e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.1922 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.1922 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.192154  0.000000
];

