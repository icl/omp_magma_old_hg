% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_148 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	814878	801419	141586	141586	7.61e-03	3.48e-02	7.74e-03	1.41e-02	5.39e-04	0.00e+00	2.52e-03	5.01e-04	4.04e-04	8.77e-03	2.13e-03	7.91e-02		7.91e-02
1	815281	789384	133340	146799	7.55e-03	2.92e-02	4.07e-03	2.28e-03	6.68e-04	0.00e+00	2.98e-03	4.91e-04	3.88e-04	6.55e-03	2.24e-03	5.64e-02		1.35e-01
2	810359	812859	133743	159640	7.38e-03	4.64e-02	4.36e-03	1.76e-03	2.70e-04	0.00e+00	3.49e-03	3.30e-04	4.02e-04	4.06e-03	2.31e-03	7.07e-02		2.06e-01
3	818549	808720	139471	136971	7.61e-03	5.01e-02	4.68e-03	3.36e-03	5.22e-04	0.00e+00	4.20e-03	3.46e-04	4.88e-04	4.06e-03	2.34e-03	7.77e-02		2.84e-01
4	803859	809773	132087	141916	7.60e-03	5.52e-02	5.05e-03	1.66e-03	3.51e-04	0.00e+00	4.14e-03	6.33e-04	4.73e-04	4.66e-03	2.36e-03	8.21e-02		3.66e-01
5	810128	809306	147582	141668	7.56e-03	4.93e-02	5.13e-03	3.13e-03	8.52e-04	0.00e+00	5.32e-03	5.21e-04	4.62e-04	4.33e-03	2.56e-03	7.92e-02		4.45e-01
6	808266	810064	142119	142941	8.77e-03	5.49e-02	5.20e-03	1.97e-03	4.71e-04	0.00e+00	5.34e-03	6.32e-04	4.93e-04	4.21e-03	2.55e-03	8.45e-02		5.30e-01
7	813934	816048	144787	142989	8.72e-03	5.26e-02	5.17e-03	1.92e-03	4.85e-04	0.00e+00	5.26e-03	4.83e-04	4.85e-04	4.03e-03	2.52e-03	8.16e-02		6.11e-01
8	809541	811675	139925	137811	8.83e-03	5.81e-02	5.34e-03	2.11e-03	1.14e-03	0.00e+00	5.20e-03	4.37e-04	4.50e-04	4.10e-03	2.56e-03	8.83e-02		7.00e-01
9	809546	815887	145124	142990	1.22e-02	5.11e-02	5.16e-03	2.31e-03	1.27e-03	0.00e+00	4.18e-03	5.70e-04	4.41e-04	3.98e-03	2.37e-03	8.36e-02		7.83e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.4004 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.4004 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.400433  0.000000
];

