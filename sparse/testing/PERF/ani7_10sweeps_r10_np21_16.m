% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_21 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	812549	797434	141586	141586	3.71e-03	5.65e-02	1.13e-02	7.60e-03	1.13e-03	0.00e+00	1.20e-02	3.62e-04	1.24e-03	7.73e-03	7.01e-03	1.09e-01		1.09e-01
1	813084	802865	135669	150784	3.65e-03	7.06e-02	1.06e-02	3.23e-03	1.39e-03	0.00e+00	1.44e-02	2.24e-04	1.43e-03	6.42e-03	9.06e-03	1.21e-01		2.29e-01
2	807563	824966	135940	146159	3.77e-03	1.11e-01	1.38e-02	2.42e-03	1.19e-03	0.00e+00	1.79e-02	3.38e-04	1.43e-03	6.13e-03	1.01e-02	1.69e-01		3.98e-01
3	802082	806186	142267	124864	3.86e-03	1.66e-01	1.60e-02	3.53e-03	1.30e-03	0.00e+00	2.05e-02	3.81e-04	1.68e-03	5.70e-03	9.34e-03	2.28e-01		6.26e-01
4	808093	810240	148554	144450	3.78e-03	1.49e-01	1.57e-02	3.03e-03	1.77e-03	0.00e+00	2.23e-02	3.96e-04	1.63e-03	5.71e-03	9.05e-03	2.12e-01		8.38e-01
5	808314	805989	143348	141201	3.80e-03	1.47e-01	1.60e-02	3.01e-03	1.85e-03	0.00e+00	2.30e-02	3.28e-04	1.70e-03	6.04e-03	9.04e-03	2.12e-01		1.05e+00
6	808891	807720	143933	146258	3.76e-03	1.50e-01	1.60e-02	3.72e-03	1.83e-03	0.00e+00	2.30e-02	3.08e-04	1.73e-03	5.76e-03	9.10e-03	2.15e-01		1.27e+00
7	812431	811870	144162	145333	3.75e-03	1.52e-01	2.36e-02	3.60e-03	1.72e-03	0.00e+00	2.24e-02	3.40e-04	1.83e-03	6.02e-03	1.21e-02	2.28e-01		1.49e+00
8	811106	807254	141428	141989	5.67e-03	1.58e-01	2.39e-02	3.23e-03	2.23e-03	0.00e+00	2.21e-02	3.99e-04	1.65e-03	5.71e-03	9.23e-03	2.32e-01		1.73e+00
9	812431	812951	143559	147411	3.80e-03	1.55e-01	1.62e-02	3.13e-03	1.94e-03	0.00e+00	2.26e-02	3.37e-04	1.67e-03	5.78e-03	9.32e-03	2.20e-01		1.95e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 2.3492 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 2.3492 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  2.349201  0.000000
];

