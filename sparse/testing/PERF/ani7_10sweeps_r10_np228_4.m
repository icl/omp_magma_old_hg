% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_228 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	812209	818022	141586	141586	1.04e-02	4.29e-02	9.08e-03	2.00e-02	3.70e-03	0.00e+00	2.38e-03	6.02e-04	3.82e-04	1.41e-02	1.74e-03	1.05e-01		1.05e-01
1	798152	806192	136009	130196	1.04e-02	3.49e-02	3.44e-03	5.71e-03	2.83e-03	0.00e+00	3.16e-03	4.95e-04	3.44e-04	7.38e-03	1.95e-03	7.06e-02		1.76e-01
2	801603	806144	150872	142832	1.03e-02	5.19e-02	3.74e-03	3.78e-03	2.75e-04	0.00e+00	3.35e-03	6.32e-04	4.22e-04	4.26e-03	2.08e-03	8.07e-02		2.57e-01
3	794728	798394	148227	143686	1.02e-02	5.15e-02	4.13e-03	2.54e-03	3.15e-04	0.00e+00	3.65e-03	6.25e-04	4.48e-04	4.20e-03	2.10e-03	7.98e-02		3.36e-01
4	812335	805109	155908	152242	1.02e-02	5.33e-02	4.29e-03	7.24e-03	7.43e-04	0.00e+00	3.94e-03	4.68e-04	4.80e-04	4.46e-03	2.21e-03	8.73e-02		4.24e-01
5	811380	808734	139106	146332	1.02e-02	5.07e-02	4.58e-03	2.81e-03	4.16e-04	0.00e+00	3.90e-03	5.39e-04	4.18e-04	5.06e-03	2.28e-03	8.09e-02		5.04e-01
6	812920	811862	140867	143513	1.03e-02	5.57e-02	4.63e-03	1.83e-03	4.38e-04	0.00e+00	3.87e-03	5.38e-04	4.19e-04	4.53e-03	2.23e-03	8.45e-02		5.89e-01
7	811821	811106	140133	141191	1.04e-02	5.13e-02	4.71e-03	1.79e-03	4.12e-04	0.00e+00	3.85e-03	6.18e-04	4.43e-04	4.24e-03	2.23e-03	8.00e-02		6.69e-01
8	808588	813956	142038	142753	1.03e-02	5.60e-02	4.69e-03	1.72e-03	3.74e-04	0.00e+00	3.91e-03	7.39e-04	4.56e-04	4.34e-03	2.28e-03	8.48e-02		7.54e-01
9	811872	812725	146077	140709	1.04e-02	5.11e-02	4.72e-03	2.69e-03	1.52e-03	0.00e+00	4.02e-03	6.57e-04	4.51e-04	4.28e-03	2.25e-03	8.20e-02		8.36e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.5817 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.5817 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.581658  0.000000
];

