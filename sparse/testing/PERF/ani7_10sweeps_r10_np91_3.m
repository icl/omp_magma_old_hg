% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_91 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	812002	811173	141586	141586	5.48e-03	3.01e-02	6.27e-03	9.68e-03	1.41e-03	0.00e+00	2.97e-03	3.69e-04	4.56e-04	6.03e-03	2.43e-03	6.52e-02		6.52e-02
1	797861	806303	136216	137045	5.47e-03	3.04e-02	4.32e-03	2.92e-03	1.13e-03	0.00e+00	3.45e-03	3.53e-04	4.98e-04	4.41e-03	2.53e-03	5.54e-02		1.21e-01
2	814126	809003	151163	142721	5.45e-03	4.69e-02	4.67e-03	1.74e-03	6.74e-04	0.00e+00	4.09e-03	3.53e-04	5.15e-04	3.33e-03	2.87e-03	7.06e-02		1.91e-01
3	804387	804717	135704	140827	5.47e-03	5.19e-02	5.15e-03	1.44e-03	3.76e-04	0.00e+00	4.68e-03	4.12e-04	5.36e-04	3.24e-03	2.91e-03	7.61e-02		2.67e-01
4	810826	807047	146249	145919	5.46e-03	5.25e-02	5.53e-03	2.17e-03	5.21e-04	0.00e+00	5.08e-03	4.39e-04	5.34e-04	3.27e-03	2.92e-03	7.84e-02		3.46e-01
5	805946	807096	140615	144394	5.45e-03	4.94e-02	5.79e-03	2.32e-03	5.32e-04	0.00e+00	5.09e-03	4.23e-04	5.33e-04	3.55e-03	2.91e-03	7.60e-02		4.22e-01
6	813154	815703	146301	145151	5.47e-03	5.15e-02	5.76e-03	2.10e-03	6.24e-04	0.00e+00	5.17e-03	4.37e-04	5.50e-04	3.46e-03	3.00e-03	7.81e-02		5.00e-01
7	811353	807053	139899	137350	5.55e-03	5.18e-02	5.96e-03	2.38e-03	5.02e-04	0.00e+00	5.21e-03	5.83e-04	5.89e-04	3.38e-03	2.91e-03	7.88e-02		5.79e-01
8	809408	809802	142506	146806	5.50e-03	5.22e-02	5.82e-03	2.16e-03	8.45e-04	0.00e+00	5.06e-03	3.27e-04	5.46e-04	3.35e-03	2.94e-03	7.87e-02		6.57e-01
9	813345	810801	145257	144863	5.46e-03	5.06e-02	5.88e-03	1.60e-03	1.15e-03	0.00e+00	5.08e-03	4.26e-04	5.82e-04	3.26e-03	2.97e-03	7.70e-02		7.34e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.2061 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.2061 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.206084  0.000000
];

