% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_79 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	813444	810812	141586	141586	5.20e-03	3.06e-02	6.77e-03	9.39e-03	7.97e-04	0.00e+00	3.33e-03	3.33e-04	5.17e-04	6.61e-03	2.55e-03	6.61e-02		6.61e-02
1	806245	811950	134774	137406	5.24e-03	2.83e-02	4.84e-03	3.47e-03	1.65e-03	0.00e+00	3.81e-03	3.30e-04	5.29e-04	4.43e-03	2.67e-03	5.53e-02		1.21e-01
2	811410	798120	142779	137074	5.23e-03	4.86e-02	5.27e-03	2.62e-03	3.75e-04	0.00e+00	4.67e-03	3.99e-04	4.86e-04	3.48e-03	2.76e-03	7.39e-02		1.95e-01
3	811714	817348	138420	151710	5.14e-03	5.61e-02	5.55e-03	2.08e-03	4.29e-04	0.00e+00	5.43e-03	4.17e-04	5.71e-04	3.43e-03	2.94e-03	8.21e-02		2.77e-01
4	802691	807391	138922	133288	5.26e-03	6.05e-02	6.68e-03	2.25e-03	7.69e-04	0.00e+00	5.79e-03	4.97e-04	6.13e-04	3.58e-03	2.92e-03	8.88e-02		3.66e-01
5	814569	812886	148750	144050	5.20e-03	5.26e-02	6.11e-03	1.91e-03	6.11e-04	0.00e+00	6.78e-03	3.47e-04	6.06e-04	3.60e-03	3.04e-03	8.08e-02		4.47e-01
6	806750	813519	137678	139361	5.22e-03	5.68e-02	6.38e-03	1.88e-03	6.64e-04	0.00e+00	6.91e-03	5.22e-04	6.38e-04	3.47e-03	3.04e-03	8.55e-02		5.32e-01
7	814107	812835	146303	139534	5.26e-03	5.41e-02	6.16e-03	2.41e-03	6.17e-04	0.00e+00	6.64e-03	5.03e-04	5.93e-04	3.31e-03	3.04e-03	8.26e-02		6.15e-01
8	812988	813464	139752	141024	5.22e-03	5.86e-02	6.35e-03	1.39e-03	5.13e-04	0.00e+00	5.66e-03	5.46e-04	5.66e-04	3.55e-03	3.00e-03	8.54e-02		7.01e-01
9	811349	814703	141677	141201	5.24e-03	5.44e-02	6.36e-03	1.57e-03	1.32e-03	0.00e+00	5.78e-03	5.12e-04	5.60e-04	3.38e-03	3.00e-03	8.21e-02		7.83e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.2518 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.2518 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.251779  0.000000
];

