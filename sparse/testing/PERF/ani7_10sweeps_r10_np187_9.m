% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_187 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	827116	818750	141586	141586	8.65e-03	3.59e-02	7.64e-03	1.53e-02	1.42e-03	0.00e+00	2.12e-03	5.34e-04	3.38e-04	9.86e-03	1.86e-03	8.36e-02		8.36e-02
1	829867	815912	121102	129468	9.25e-03	3.29e-02	3.49e-03	3.70e-03	1.21e-03	0.00e+00	2.62e-03	5.61e-04	3.78e-04	5.77e-03	2.21e-03	6.21e-02		1.46e-01
2	805730	821806	119157	133112	1.06e-02	4.55e-02	4.60e-03	3.28e-03	6.32e-04	0.00e+00	3.10e-03	3.98e-04	4.49e-04	4.28e-03	2.30e-03	7.52e-02		2.21e-01
3	805318	810326	144100	128024	1.06e-02	4.99e-02	4.82e-03	2.40e-03	3.93e-04	0.00e+00	3.25e-03	5.69e-04	4.46e-04	3.85e-03	2.26e-03	7.85e-02		2.99e-01
4	807669	805467	145318	140310	1.05e-02	5.00e-02	4.88e-03	2.00e-03	3.61e-04	0.00e+00	3.46e-03	6.66e-04	4.71e-04	3.94e-03	2.30e-03	7.87e-02		3.78e-01
5	809242	804326	143772	145974	1.04e-02	4.54e-02	4.92e-03	1.99e-03	4.01e-04	0.00e+00	3.48e-03	5.87e-04	4.78e-04	4.16e-03	2.30e-03	7.41e-02		4.52e-01
6	805891	808821	143005	147921	1.07e-02	4.89e-02	4.87e-03	1.79e-03	4.17e-04	0.00e+00	3.44e-03	5.17e-04	4.44e-04	3.99e-03	2.34e-03	7.74e-02		5.30e-01
7	810328	813121	147162	144232	9.79e-03	4.66e-02	4.55e-03	1.67e-03	4.79e-04	0.00e+00	3.48e-03	4.97e-04	4.44e-04	4.28e-03	2.25e-03	7.40e-02		6.04e-01
8	808674	810541	143531	140738	8.66e-03	5.00e-02	4.61e-03	2.15e-03	1.25e-03	0.00e+00	3.43e-03	5.62e-04	4.60e-04	3.80e-03	2.24e-03	7.72e-02		6.81e-01
9	812872	817652	145991	144124	8.65e-03	4.65e-02	4.66e-03	1.96e-03	1.22e-03	0.00e+00	3.44e-03	5.83e-04	4.77e-04	3.90e-03	2.25e-03	7.36e-02		7.54e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.3929 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.3929 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.392877  0.000000
];

