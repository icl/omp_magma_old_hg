% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_104 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	807510	805718	141586	141586	5.62e-03	2.90e-02	5.90e-03	9.86e-03	6.66e-04	0.00e+00	2.56e-03	3.79e-04	4.07e-04	6.66e-03	2.11e-03	6.32e-02		6.32e-02
1	812365	801420	140708	142500	5.63e-03	2.76e-02	3.74e-03	2.75e-03	6.83e-04	0.00e+00	3.08e-03	3.67e-04	4.33e-04	4.04e-03	2.45e-03	5.08e-02		1.14e-01
2	807181	803787	136659	147604	5.58e-03	4.45e-02	4.35e-03	1.94e-03	3.54e-04	0.00e+00	3.62e-03	3.64e-04	4.24e-04	3.23e-03	2.48e-03	6.69e-02		1.81e-01
3	805185	810739	142649	146043	5.57e-03	4.65e-02	4.53e-03	2.00e-03	5.77e-04	0.00e+00	4.12e-03	4.61e-04	4.54e-04	3.08e-03	2.57e-03	6.99e-02		2.51e-01
4	809351	805925	145451	139897	5.61e-03	4.53e-02	4.96e-03	1.88e-03	4.38e-04	0.00e+00	4.41e-03	3.76e-04	4.64e-04	3.63e-03	2.51e-03	6.96e-02		3.20e-01
5	797975	812905	142090	145516	5.60e-03	4.34e-02	5.11e-03	4.08e-03	1.07e-03	0.00e+00	4.66e-03	4.19e-04	4.94e-04	3.41e-03	2.57e-03	7.08e-02		3.91e-01
6	812127	811504	154272	139342	5.67e-03	4.68e-02	5.17e-03	2.44e-03	4.70e-04	0.00e+00	4.37e-03	3.32e-04	4.70e-04	3.28e-03	2.60e-03	7.16e-02		4.63e-01
7	818892	811555	140926	141549	5.64e-03	4.60e-02	5.21e-03	2.21e-03	5.97e-04	0.00e+00	4.43e-03	3.44e-04	4.85e-04	3.35e-03	2.64e-03	7.09e-02		5.34e-01
8	811167	815769	134967	142304	5.65e-03	4.92e-02	5.33e-03	1.66e-03	1.05e-03	0.00e+00	4.54e-03	4.72e-04	5.20e-04	3.20e-03	2.59e-03	7.42e-02		6.08e-01
9	813001	814303	143498	138896	5.70e-03	4.70e-02	5.40e-03	1.98e-03	8.71e-04	0.00e+00	4.60e-03	3.53e-04	5.07e-04	3.11e-03	2.64e-03	7.22e-02		6.80e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.1548 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.1548 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.154783  0.000000
];

