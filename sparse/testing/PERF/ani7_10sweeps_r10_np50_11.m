% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_50 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	818664	797083	141586	141586	3.24e-03	2.98e-02	5.27e-03	6.73e-03	4.93e-04	0.00e+00	4.81e-03	2.75e-04	5.57e-04	5.45e-03	2.82e-03	5.94e-02		5.94e-02
1	798286	798676	129554	151135	3.23e-03	3.41e-02	4.20e-03	2.51e-03	6.24e-04	0.00e+00	5.80e-03	2.51e-04	6.18e-04	4.02e-03	3.67e-03	5.90e-02		1.18e-01
2	803699	804320	150738	150348	3.29e-03	5.51e-02	5.35e-03	2.50e-03	4.62e-04	0.00e+00	7.11e-03	2.38e-04	6.20e-04	3.54e-03	3.97e-03	8.22e-02		2.01e-01
3	797560	816672	146131	145510	3.25e-03	7.47e-02	6.19e-03	2.10e-03	6.95e-04	0.00e+00	8.33e-03	2.66e-04	7.45e-04	3.52e-03	3.81e-03	1.04e-01		3.04e-01
4	806524	807415	153076	133964	3.37e-03	7.18e-02	6.33e-03	2.11e-03	1.07e-03	0.00e+00	9.12e-03	2.90e-04	7.80e-04	4.09e-03	3.65e-03	1.03e-01		4.07e-01
5	808614	809718	144917	144026	3.30e-03	6.89e-02	6.40e-03	2.41e-03	1.48e-03	0.00e+00	9.19e-03	2.39e-04	8.06e-04	3.81e-03	3.69e-03	1.00e-01		5.07e-01
6	811187	814402	143633	142529	3.29e-03	7.12e-02	6.42e-03	1.94e-03	9.21e-04	0.00e+00	9.16e-03	2.76e-04	8.03e-04	3.65e-03	3.76e-03	1.01e-01		6.09e-01
7	810825	812226	141866	138651	3.29e-03	7.32e-02	6.46e-03	1.89e-03	1.01e-03	0.00e+00	9.23e-03	3.37e-04	7.75e-04	3.70e-03	3.78e-03	1.04e-01		7.12e-01
8	811449	811234	143034	141633	3.30e-03	7.44e-02	6.47e-03	1.87e-03	1.19e-03	0.00e+00	8.95e-03	3.47e-04	7.66e-04	3.58e-03	3.74e-03	1.05e-01		8.17e-01
9	813031	813819	143216	143431	3.31e-03	7.33e-02	6.51e-03	2.74e-03	1.14e-03	0.00e+00	8.84e-03	3.02e-04	7.29e-04	3.51e-03	3.78e-03	1.04e-01		9.21e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.2946 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.2946 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.294563  0.000000
];

