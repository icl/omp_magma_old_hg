% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_141 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	814551	807095	141586	141586	7.40e-03	3.38e-02	6.67e-03	1.38e-02	4.70e-04	0.00e+00	2.73e-03	5.10e-04	4.53e-04	8.70e-03	2.27e-03	7.68e-02		7.68e-02
1	825487	822829	133667	141123	7.40e-03	3.00e-02	4.19e-03	5.83e-03	5.93e-04	0.00e+00	3.06e-03	4.28e-04	4.05e-04	6.46e-03	2.44e-03	6.08e-02		1.38e-01
2	814579	805787	123537	126195	7.50e-03	4.65e-02	4.56e-03	2.10e-03	3.23e-04	0.00e+00	3.76e-03	4.64e-04	4.20e-04	4.12e-03	2.35e-03	7.21e-02		2.10e-01
3	810121	810619	135251	144043	7.35e-03	5.12e-02	4.72e-03	2.19e-03	3.92e-04	0.00e+00	4.37e-03	6.24e-04	4.76e-04	4.06e-03	2.30e-03	7.77e-02		2.87e-01
4	814946	809439	140515	140017	7.41e-03	5.16e-02	5.10e-03	3.64e-03	4.48e-04	0.00e+00	4.46e-03	6.32e-04	4.74e-04	4.76e-03	2.39e-03	8.09e-02		3.68e-01
5	804794	809563	136495	142002	7.39e-03	4.63e-02	5.29e-03	2.63e-03	8.56e-04	0.00e+00	4.52e-03	6.23e-04	5.08e-04	4.29e-03	2.17e-03	7.46e-02		4.43e-01
6	811192	808721	147453	142684	7.44e-03	4.93e-02	5.14e-03	1.80e-03	4.65e-04	0.00e+00	4.44e-03	6.06e-04	4.75e-04	4.38e-03	2.17e-03	7.62e-02		5.19e-01
7	810467	809525	141861	144332	7.38e-03	4.73e-02	5.27e-03	1.60e-03	4.57e-04	0.00e+00	4.38e-03	5.96e-04	4.70e-04	3.96e-03	2.14e-03	7.36e-02		5.93e-01
8	809725	816456	143392	144334	7.44e-03	5.07e-02	5.26e-03	1.89e-03	1.08e-03	0.00e+00	4.38e-03	6.52e-04	4.81e-04	4.30e-03	2.19e-03	7.84e-02		6.71e-01
9	814225	812126	144940	138209	9.27e-03	5.14e-02	5.67e-03	1.74e-03	9.10e-04	0.00e+00	4.92e-03	5.48e-04	4.97e-04	4.01e-03	2.19e-03	8.12e-02		7.52e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.3865 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.3865 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.386501  0.000000
];

