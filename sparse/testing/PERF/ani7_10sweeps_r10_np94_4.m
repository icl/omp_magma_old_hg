% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_94 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	823545	816845	141586	141586	5.46e-03	2.99e-02	6.19e-03	9.70e-03	1.64e-03	0.00e+00	2.84e-03	4.23e-04	4.34e-04	6.77e-03	2.38e-03	6.57e-02		6.57e-02
1	798343	804092	124673	131373	5.53e-03	3.00e-02	4.23e-03	2.05e-03	1.59e-03	0.00e+00	3.58e-03	3.18e-04	4.77e-04	4.61e-03	2.46e-03	5.49e-02		1.21e-01
2	802717	812224	150681	144932	5.42e-03	4.68e-02	4.48e-03	1.50e-03	3.48e-04	0.00e+00	3.86e-03	2.93e-04	5.02e-04	3.30e-03	2.72e-03	6.92e-02		1.90e-01
3	820994	806933	147113	137606	5.48e-03	5.24e-02	5.06e-03	2.14e-03	4.46e-04	0.00e+00	4.65e-03	4.09e-04	5.16e-04	3.22e-03	2.82e-03	7.72e-02		2.67e-01
4	816342	808082	129642	143703	5.46e-03	5.27e-02	5.53e-03	1.88e-03	6.28e-04	0.00e+00	5.04e-03	2.87e-04	5.19e-04	3.44e-03	2.81e-03	7.83e-02		3.45e-01
5	810274	808677	135099	143359	5.46e-03	4.94e-02	5.65e-03	2.77e-03	5.30e-04	0.00e+00	5.03e-03	5.73e-04	5.03e-04	3.60e-03	2.79e-03	7.63e-02		4.21e-01
6	811046	812875	141973	143570	5.46e-03	5.19e-02	5.65e-03	2.92e-03	4.63e-04	0.00e+00	4.84e-03	4.83e-04	4.99e-04	3.43e-03	2.78e-03	7.85e-02		5.00e-01
7	814507	813457	142007	140178	5.51e-03	5.02e-02	5.76e-03	2.13e-03	5.11e-04	0.00e+00	4.88e-03	3.65e-04	5.08e-04	3.25e-03	2.88e-03	7.60e-02		5.76e-01
8	815767	813752	139352	140402	5.48e-03	5.28e-02	5.84e-03	1.57e-03	6.68e-04	0.00e+00	4.95e-03	4.69e-04	5.25e-04	3.31e-03	2.90e-03	7.85e-02		6.54e-01
9	814667	811156	138898	140913	5.53e-03	5.10e-02	5.90e-03	1.33e-03	9.90e-04	0.00e+00	4.84e-03	4.41e-04	5.26e-04	3.21e-03	2.86e-03	7.66e-02		7.31e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.2158 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.2158 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.215818  0.000000
];

