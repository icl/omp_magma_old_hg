% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_73 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	797187	811479	141586	141586	5.05e-03	2.96e-02	6.83e-03	8.94e-03	4.32e-04	0.00e+00	3.61e-03	4.18e-04	5.90e-04	5.84e-03	2.64e-03	6.40e-02		6.40e-02
1	809723	804977	151031	136739	5.07e-03	2.86e-02	5.05e-03	2.26e-03	5.36e-04	0.00e+00	4.03e-03	3.33e-04	5.30e-04	4.64e-03	2.80e-03	5.39e-02		1.18e-01
2	814997	807808	139301	144047	5.05e-03	4.98e-02	5.56e-03	2.14e-03	4.38e-04	0.00e+00	4.90e-03	4.79e-04	5.03e-04	3.56e-03	3.01e-03	7.54e-02		1.93e-01
3	801720	813353	134833	142022	5.05e-03	6.03e-02	5.94e-03	2.18e-03	4.59e-04	0.00e+00	5.89e-03	4.06e-04	6.25e-04	3.43e-03	3.08e-03	8.74e-02		2.81e-01
4	808886	810366	148916	137283	5.08e-03	5.87e-02	6.47e-03	3.12e-03	7.09e-04	0.00e+00	6.24e-03	4.17e-04	6.46e-04	3.98e-03	3.05e-03	8.84e-02		3.69e-01
5	803482	808176	142555	141075	5.07e-03	5.25e-02	6.72e-03	3.56e-03	1.07e-03	0.00e+00	6.35e-03	5.01e-04	6.51e-04	3.51e-03	3.04e-03	8.30e-02		4.52e-01
6	808831	810425	148765	144071	5.07e-03	5.47e-02	6.52e-03	3.46e-03	6.37e-04	0.00e+00	6.10e-03	3.88e-04	6.06e-04	3.71e-03	3.11e-03	8.43e-02		5.36e-01
7	810849	808561	144222	142628	5.07e-03	5.48e-02	6.70e-03	2.38e-03	6.31e-04	0.00e+00	6.21e-03	4.72e-04	6.23e-04	3.42e-03	3.09e-03	8.34e-02		6.20e-01
8	812214	810720	143010	145298	5.09e-03	6.25e-02	6.68e-03	3.01e-03	9.59e-04	0.00e+00	6.39e-03	4.26e-04	6.19e-04	3.47e-03	3.37e-03	9.25e-02		7.12e-01
9	813564	816547	142451	143945	5.17e-03	6.26e-02	6.82e-03	1.89e-03	1.05e-03	0.00e+00	7.40e-03	3.37e-04	6.54e-04	3.48e-03	3.37e-03	9.28e-02		8.05e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.2820 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.2820 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.282011  0.000000
];

