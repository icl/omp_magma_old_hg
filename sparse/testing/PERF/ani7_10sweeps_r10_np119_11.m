% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_119 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	825392	814375	141586	141586	5.85e-03	2.82e-02	5.80e-03	1.04e-02	1.82e-03	0.00e+00	2.74e-03	3.35e-04	3.61e-04	7.18e-03	1.88e-03	6.46e-02		6.46e-02
1	809815	802154	122826	133843	5.86e-03	2.88e-02	3.49e-03	2.16e-03	1.81e-03	0.00e+00	2.70e-03	3.07e-04	3.98e-04	4.63e-03	2.21e-03	5.23e-02		1.17e-01
2	805665	802897	139209	146870	5.81e-03	3.93e-02	3.90e-03	1.27e-03	3.25e-04	0.00e+00	3.31e-03	4.68e-04	4.46e-04	3.13e-03	2.40e-03	6.03e-02		1.77e-01
3	803805	809810	144165	146933	5.83e-03	4.52e-02	4.35e-03	1.52e-03	3.94e-04	0.00e+00	4.34e-03	3.77e-04	4.96e-04	3.08e-03	2.40e-03	6.79e-02		2.45e-01
4	807209	815025	146831	140826	5.88e-03	4.80e-02	4.59e-03	3.56e-03	4.85e-04	0.00e+00	4.54e-03	4.13e-04	4.71e-04	3.21e-03	2.39e-03	7.36e-02		3.19e-01
5	811491	805609	144232	136416	5.91e-03	4.71e-02	4.66e-03	1.95e-03	4.31e-04	0.00e+00	4.49e-03	4.10e-04	5.11e-04	3.48e-03	2.42e-03	7.14e-02		3.90e-01
6	812389	811781	140756	146638	5.82e-03	4.96e-02	4.66e-03	2.36e-03	3.97e-04	0.00e+00	4.48e-03	3.67e-04	4.82e-04	3.28e-03	2.52e-03	7.40e-02		4.64e-01
7	812456	811420	140664	141272	5.89e-03	4.89e-02	4.76e-03	1.38e-03	3.95e-04	0.00e+00	4.28e-03	6.87e-04	4.92e-04	3.05e-03	2.46e-03	7.23e-02		5.36e-01
8	810794	810473	141403	142439	5.88e-03	5.15e-02	4.74e-03	2.28e-03	4.13e-04	0.00e+00	4.85e-03	6.15e-04	4.88e-04	3.25e-03	2.48e-03	7.65e-02		6.13e-01
9	813458	813851	143871	144192	5.91e-03	4.91e-02	4.79e-03	1.50e-03	9.60e-04	0.00e+00	4.53e-03	4.60e-04	4.65e-04	3.04e-03	2.50e-03	7.32e-02		6.86e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.1704 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.1704 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.170378  0.000000
];

