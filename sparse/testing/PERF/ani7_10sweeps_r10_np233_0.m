% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_233 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	816078	817446	141586	141586	1.06e-02	4.29e-02	9.01e-03	1.86e-02	8.94e-04	0.00e+00	2.29e-03	4.89e-04	3.50e-04	1.28e-02	1.81e-03	9.99e-02		9.99e-02
1	793045	786075	132140	130772	1.07e-02	3.57e-02	3.48e-03	4.25e-03	1.18e-03	0.00e+00	3.04e-03	4.70e-04	3.76e-04	6.75e-03	1.88e-03	6.78e-02		1.68e-01
2	807828	798191	155979	162949	1.03e-02	5.06e-02	3.56e-03	1.42e-03	2.96e-04	0.00e+00	3.18e-03	5.71e-04	3.83e-04	4.31e-03	1.98e-03	7.65e-02		2.44e-01
3	810711	821836	142002	151639	1.04e-02	5.10e-02	3.96e-03	2.88e-03	5.12e-04	0.00e+00	3.56e-03	4.56e-04	4.51e-04	4.18e-03	2.15e-03	7.95e-02		3.24e-01
4	817953	805950	139925	128800	1.08e-02	5.78e-02	4.45e-03	4.19e-03	5.30e-04	0.00e+00	3.95e-03	8.77e-04	4.64e-04	5.32e-03	2.18e-03	9.05e-02		4.14e-01
5	805106	812284	133488	145491	1.05e-02	5.13e-02	4.54e-03	2.80e-03	1.69e-03	0.00e+00	3.83e-03	7.20e-04	4.52e-04	4.85e-03	2.20e-03	8.29e-02		4.97e-01
6	813415	811024	147141	139963	1.06e-02	5.69e-02	4.57e-03	1.68e-03	3.88e-04	0.00e+00	3.87e-03	6.07e-04	4.46e-04	4.81e-03	2.22e-03	8.60e-02		5.83e-01
7	812657	808391	139638	142029	1.06e-02	5.23e-02	4.61e-03	2.14e-03	4.11e-04	0.00e+00	3.89e-03	7.08e-04	4.17e-04	4.26e-03	2.23e-03	8.15e-02		6.65e-01
8	810242	814703	141202	145468	1.05e-02	5.63e-02	4.59e-03	1.56e-03	1.60e-03	0.00e+00	3.87e-03	5.90e-04	4.11e-04	4.38e-03	2.27e-03	8.61e-02		7.51e-01
9	809600	809429	144423	139962	1.06e-02	5.52e-02	4.66e-03	1.74e-03	1.70e-03	0.00e+00	3.87e-03	7.62e-04	4.50e-04	4.30e-03	2.19e-03	8.54e-02		8.36e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.5915 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.5915 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.591491  0.000000
];

