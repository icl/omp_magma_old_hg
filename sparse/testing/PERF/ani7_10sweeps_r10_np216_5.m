% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_216 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	829782	813677	141586	141586	1.08e-02	4.12e-02	9.15e-03	1.92e-02	1.66e-03	0.00e+00	2.54e-03	5.56e-04	3.98e-04	1.18e-02	1.91e-03	9.91e-02		9.91e-02
1	802155	794985	118436	134541	1.09e-02	3.63e-02	3.66e-03	5.58e-03	1.91e-03	0.00e+00	3.42e-03	4.59e-04	3.49e-04	6.43e-03	2.10e-03	7.11e-02		1.70e-01
2	803895	802479	146869	154039	1.02e-02	5.22e-02	3.78e-03	3.00e-03	8.02e-04	0.00e+00	3.46e-03	5.45e-04	4.05e-04	4.24e-03	2.05e-03	8.07e-02		2.51e-01
3	814479	805535	145935	147351	9.73e-03	5.29e-02	4.02e-03	2.96e-03	4.25e-04	0.00e+00	3.86e-03	4.51e-04	4.23e-04	4.28e-03	2.09e-03	8.12e-02		3.32e-01
4	814720	815855	136157	145101	9.81e-03	5.68e-02	4.35e-03	4.07e-03	4.45e-04	0.00e+00	4.08e-03	6.08e-04	4.80e-04	4.28e-03	2.14e-03	8.71e-02		4.19e-01
5	797092	807244	136721	135586	9.92e-03	5.08e-02	4.66e-03	4.06e-03	4.14e-04	0.00e+00	4.10e-03	7.40e-04	4.48e-04	4.85e-03	2.05e-03	8.20e-02		5.01e-01
6	806062	812925	155155	145003	9.82e-03	5.41e-02	4.41e-03	2.28e-03	4.18e-04	0.00e+00	4.13e-03	7.11e-04	4.60e-04	4.30e-03	2.07e-03	8.27e-02		5.84e-01
7	808249	811915	146991	140128	9.93e-03	5.12e-02	4.57e-03	2.89e-03	4.01e-04	0.00e+00	4.09e-03	6.25e-04	4.44e-04	4.59e-03	2.12e-03	8.08e-02		6.65e-01
8	810012	811464	145610	141944	9.88e-03	5.50e-02	4.49e-03	3.39e-03	1.57e-03	0.00e+00	4.20e-03	4.77e-04	4.66e-04	4.38e-03	2.08e-03	8.60e-02		7.51e-01
9	810911	810017	144653	143201	9.88e-03	5.15e-02	4.52e-03	3.57e-03	1.45e-03	0.00e+00	4.08e-03	7.04e-04	4.37e-04	4.29e-03	2.09e-03	8.26e-02		8.33e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.5824 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.5824 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.582431  0.000000
];

