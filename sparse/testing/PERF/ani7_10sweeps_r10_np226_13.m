% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_226 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	816951	805077	141586	141586	1.03e-02	4.32e-02	9.03e-03	4.32e-02	2.98e-01	0.00e+00	4.62e-02	2.85e-02	4.02e-04	1.39e-02	1.68e-03	4.94e-01		4.94e-01
1	800879	791667	131267	143141	1.02e-02	3.48e-02	3.46e-03	4.29e-03	1.80e-03	0.00e+00	3.04e-03	4.75e-04	3.40e-04	7.60e-03	1.87e-03	6.79e-02		5.62e-01
2	805329	801296	148145	157357	9.91e-03	5.11e-02	3.71e-03	2.75e-03	3.27e-04	0.00e+00	3.34e-03	6.80e-04	4.22e-04	4.26e-03	1.98e-03	7.85e-02		6.40e-01
3	810398	803781	144501	148534	1.01e-02	5.15e-02	4.17e-03	3.35e-03	3.78e-04	0.00e+00	3.63e-03	6.58e-04	4.16e-04	4.35e-03	1.98e-03	8.05e-02		7.21e-01
4	815169	803825	140238	146855	1.03e-02	5.60e-02	4.41e-03	3.74e-03	6.95e-04	0.00e+00	3.86e-03	5.92e-04	4.66e-04	4.79e-03	2.01e-03	8.68e-02		8.08e-01
5	814458	814884	136272	147616	1.01e-02	5.00e-02	4.52e-03	2.82e-03	5.95e-04	0.00e+00	3.94e-03	6.72e-04	4.60e-04	5.05e-03	2.05e-03	8.02e-02		8.88e-01
6	811076	812402	137789	137363	1.03e-02	5.62e-02	4.76e-03	2.21e-03	4.59e-04	0.00e+00	3.93e-03	7.72e-04	4.16e-04	4.92e-03	2.02e-03	8.59e-02		9.74e-01
7	806859	813147	141977	140651	1.02e-02	5.09e-02	4.74e-03	2.24e-03	4.41e-04	0.00e+00	3.87e-03	6.93e-04	4.49e-04	4.30e-03	2.01e-03	7.99e-02		1.05e+00
8	812267	812357	147000	140712	1.02e-02	5.54e-02	4.71e-03	2.54e-03	9.47e-04	0.00e+00	3.93e-03	5.79e-04	4.45e-04	4.32e-03	2.02e-03	8.51e-02		1.14e+00
9	813252	812540	142398	142308	1.03e-02	5.09e-02	4.73e-03	2.78e-03	1.62e-03	0.00e+00	3.95e-03	4.06e-04	4.19e-04	4.31e-03	2.02e-03	8.14e-02		1.22e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.9751 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.9751 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.975120  0.000000
];

