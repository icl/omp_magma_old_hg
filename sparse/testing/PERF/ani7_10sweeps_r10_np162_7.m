% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_162 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	818458	814598	141586	141586	7.99e-03	3.65e-02	7.56e-03	1.51e-02	6.89e-04	0.00e+00	2.38e-03	3.81e-04	4.10e-04	9.39e-03	2.05e-03	8.24e-02		8.24e-02
1	781309	804513	129760	133620	8.08e-03	3.15e-02	3.89e-03	3.70e-03	7.44e-04	0.00e+00	2.97e-03	4.22e-04	4.02e-04	5.45e-03	2.09e-03	5.93e-02		1.42e-01
2	812217	803549	167715	144511	7.92e-03	4.63e-02	4.04e-03	2.43e-03	3.42e-04	0.00e+00	3.15e-03	7.88e-04	4.05e-04	3.95e-03	2.33e-03	7.17e-02		2.13e-01
3	819212	812029	137613	146281	7.88e-03	4.81e-02	4.60e-03	1.75e-03	3.37e-04	0.00e+00	3.72e-03	5.20e-04	4.20e-04	3.96e-03	2.52e-03	7.38e-02		2.87e-01
4	805430	808732	131424	138607	7.98e-03	5.38e-02	5.06e-03	2.32e-03	4.26e-04	0.00e+00	4.00e-03	6.20e-04	4.75e-04	4.83e-03	2.45e-03	8.20e-02		3.69e-01
5	812874	810553	146011	142709	7.95e-03	4.59e-02	5.05e-03	3.78e-03	1.14e-03	0.00e+00	4.00e-03	5.52e-04	4.44e-04	4.32e-03	2.50e-03	7.56e-02		4.45e-01
6	812044	809778	139373	141694	7.99e-03	5.18e-02	5.11e-03	2.03e-03	4.76e-04	0.00e+00	3.96e-03	6.07e-04	4.86e-04	4.40e-03	2.49e-03	7.93e-02		5.24e-01
7	811627	812465	141009	143275	7.96e-03	4.68e-02	5.17e-03	1.48e-03	5.15e-04	0.00e+00	3.92e-03	6.25e-04	4.76e-04	3.99e-03	2.43e-03	7.34e-02		5.98e-01
8	813683	812363	142232	141394	7.99e-03	5.15e-02	5.20e-03	1.53e-03	1.04e-03	0.00e+00	4.03e-03	4.24e-04	4.83e-04	4.00e-03	2.43e-03	7.86e-02		6.76e-01
9	812781	810706	140982	142302	8.00e-03	4.77e-02	5.20e-03	1.66e-03	1.02e-03	0.00e+00	3.92e-03	5.75e-04	4.66e-04	4.03e-03	2.51e-03	7.51e-02		7.51e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.3928 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.3928 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.392838  0.000000
];

