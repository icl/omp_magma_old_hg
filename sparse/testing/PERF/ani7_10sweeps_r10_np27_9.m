% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_27 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	815408	803213	141586	141586	3.61e-03	4.68e-02	9.00e-03	7.22e-03	1.26e-03	0.00e+00	9.44e-03	2.71e-04	1.00e-03	6.70e-03	5.45e-03	9.08e-02		9.08e-02
1	803138	784788	132810	145005	3.61e-03	5.73e-02	8.31e-03	3.31e-03	1.39e-03	0.00e+00	1.16e-02	2.54e-04	1.08e-03	5.58e-03	6.95e-03	9.94e-02		1.90e-01
2	807365	800288	145886	164236	3.57e-03	9.00e-02	1.05e-02	3.12e-03	1.09e-03	0.00e+00	1.38e-02	2.70e-04	1.17e-03	5.02e-03	7.63e-03	1.36e-01		3.26e-01
3	803696	802843	142465	149542	3.64e-03	1.36e-01	1.20e-02	2.99e-03	1.07e-03	0.00e+00	1.61e-02	3.59e-04	1.31e-03	4.97e-03	7.09e-03	1.85e-01		5.12e-01
4	807666	814622	146940	147793	3.66e-03	1.26e-01	1.23e-02	2.95e-03	1.42e-03	0.00e+00	1.67e-02	3.36e-04	1.34e-03	4.97e-03	7.19e-03	1.77e-01		6.89e-01
5	808794	811251	143775	136819	3.67e-03	1.27e-01	1.26e-02	3.42e-03	1.47e-03	0.00e+00	1.73e-02	2.95e-04	1.33e-03	5.52e-03	7.24e-03	1.80e-01		8.69e-01
6	808889	810427	143453	140996	3.66e-03	1.30e-01	1.26e-02	3.35e-03	1.50e-03	0.00e+00	1.73e-02	5.10e-04	1.36e-03	5.10e-03	7.22e-03	1.82e-01		1.05e+00
7	809407	809342	144164	142626	3.67e-03	1.29e-01	1.26e-02	3.75e-03	1.59e-03	0.00e+00	1.75e-02	2.89e-04	1.37e-03	5.18e-03	7.25e-03	1.82e-01		1.23e+00
8	813599	812924	144452	144517	3.67e-03	1.31e-01	1.27e-02	2.83e-03	2.03e-03	0.00e+00	1.78e-02	2.82e-04	1.43e-03	5.11e-03	7.29e-03	1.84e-01		1.42e+00
9	810403	813312	141066	141741	3.71e-03	1.32e-01	1.29e-02	2.58e-03	1.78e-03	0.00e+00	1.73e-02	4.35e-04	1.33e-03	4.96e-03	7.28e-03	1.84e-01		1.60e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.9916 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.9916 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.991614  0.000000
];

