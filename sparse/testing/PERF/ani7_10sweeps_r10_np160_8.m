% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_160 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	821319	812992	141586	141586	7.95e-03	3.66e-02	7.58e-03	1.52e-02	1.11e-03	0.00e+00	2.46e-03	4.82e-04	4.33e-04	1.05e-02	2.08e-03	8.44e-02		8.44e-02
1	796947	812143	126899	135226	8.01e-03	2.99e-02	3.95e-03	4.83e-03	1.18e-03	0.00e+00	2.95e-03	3.68e-04	4.08e-04	6.17e-03	2.21e-03	5.99e-02		1.44e-01
2	813413	804249	152077	136881	7.93e-03	4.61e-02	4.25e-03	2.82e-03	3.88e-04	0.00e+00	3.24e-03	6.03e-04	4.01e-04	3.95e-03	2.35e-03	7.21e-02		2.16e-01
3	804046	808416	136417	145581	7.88e-03	4.66e-02	4.67e-03	3.11e-03	2.93e-04	0.00e+00	3.78e-03	5.05e-04	4.70e-04	3.91e-03	2.41e-03	7.36e-02		2.90e-01
4	806414	812288	146590	142220	7.91e-03	4.88e-02	4.96e-03	3.05e-03	3.85e-04	0.00e+00	3.94e-03	4.97e-04	4.50e-04	4.40e-03	2.47e-03	7.69e-02		3.67e-01
5	804982	808741	145027	139153	7.94e-03	4.32e-02	5.11e-03	4.09e-03	3.99e-04	0.00e+00	3.93e-03	6.39e-04	4.54e-04	4.36e-03	2.56e-03	7.27e-02		4.40e-01
6	811066	810738	147265	143506	7.92e-03	4.92e-02	5.18e-03	2.35e-03	4.06e-04	0.00e+00	3.98e-03	4.92e-04	4.48e-04	4.29e-03	2.54e-03	7.68e-02		5.16e-01
7	809475	813767	141987	142315	7.93e-03	4.43e-02	5.23e-03	1.72e-03	4.15e-04	0.00e+00	3.92e-03	4.95e-04	4.46e-04	3.85e-03	2.54e-03	7.09e-02		5.87e-01
8	816243	812691	144384	140092	7.93e-03	4.84e-02	5.37e-03	2.01e-03	3.69e-04	0.00e+00	3.85e-03	5.61e-04	4.59e-04	4.38e-03	2.59e-03	7.59e-02		6.63e-01
9	809410	814262	138422	141974	7.95e-03	4.57e-02	5.37e-03	1.68e-03	1.44e-03	0.00e+00	4.03e-03	1.03e-03	4.67e-04	3.95e-03	2.57e-03	7.42e-02		7.37e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.3756 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.3756 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.375577  0.000000
];

