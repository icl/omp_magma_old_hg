% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_91 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	812091	811141	141586	141586	5.46e-03	2.97e-02	6.26e-03	9.74e-03	7.63e-04	0.00e+00	2.93e-03	3.66e-04	4.63e-04	6.20e-03	2.42e-03	6.44e-02		6.44e-02
1	818269	803173	136127	137077	5.50e-03	2.99e-02	4.28e-03	2.11e-03	8.62e-04	0.00e+00	3.48e-03	2.71e-04	5.14e-04	4.22e-03	2.64e-03	5.38e-02		1.18e-01
2	807624	810230	130755	145851	5.43e-03	4.63e-02	4.77e-03	3.24e-03	8.18e-04	0.00e+00	4.17e-03	3.12e-04	4.83e-04	3.38e-03	2.81e-03	7.17e-02		1.90e-01
3	809985	795266	142206	139600	5.50e-03	5.20e-02	5.11e-03	1.93e-03	3.85e-04	0.00e+00	4.55e-03	4.63e-04	5.45e-04	3.21e-03	2.87e-03	7.65e-02		2.66e-01
4	812488	817097	140651	155370	5.38e-03	5.20e-02	5.50e-03	2.26e-03	6.76e-04	0.00e+00	5.04e-03	3.37e-04	5.70e-04	3.29e-03	2.97e-03	7.80e-02		3.44e-01
5	811161	805333	138953	134344	5.51e-03	5.04e-02	5.86e-03	2.01e-03	5.99e-04	0.00e+00	5.20e-03	3.57e-04	6.52e-04	3.60e-03	2.89e-03	7.71e-02		4.21e-01
6	813051	811323	141086	146914	5.45e-03	5.20e-02	5.76e-03	1.35e-03	5.43e-04	0.00e+00	5.00e-03	4.30e-04	5.74e-04	3.23e-03	2.95e-03	7.73e-02		4.99e-01
7	810071	814604	140002	141730	5.48e-03	5.09e-02	5.85e-03	1.93e-03	5.70e-04	0.00e+00	5.11e-03	3.15e-04	5.90e-04	3.34e-03	2.97e-03	7.70e-02		5.76e-01
8	814911	813620	143788	139255	5.50e-03	5.40e-02	5.98e-03	2.17e-03	8.38e-04	0.00e+00	5.16e-03	4.08e-04	6.00e-04	3.35e-03	2.98e-03	8.10e-02		6.57e-01
9	810031	812467	139754	141045	5.50e-03	5.26e-02	5.98e-03	2.09e-03	1.05e-03	0.00e+00	5.10e-03	4.23e-04	5.78e-04	3.23e-03	2.95e-03	7.95e-02		7.36e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.2241 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.2241 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.224122  0.000000
];

