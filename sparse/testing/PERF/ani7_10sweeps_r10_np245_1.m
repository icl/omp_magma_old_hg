% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_245 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	821327	826678	141586	141586	1.10e-02	4.40e-02	9.27e-03	2.02e-02	2.37e-03	0.00e+00	2.25e-03	7.08e-04	3.73e-04	1.35e-02	1.71e-03	1.05e-01		1.05e-01
1	816439	815520	126891	121540	1.12e-02	3.66e-02	3.33e-03	2.80e-03	2.47e-03	0.00e+00	2.98e-03	3.89e-04	3.61e-04	7.06e-03	1.85e-03	6.91e-02		1.74e-01
2	819496	804166	132585	133504	1.10e-02	5.13e-02	3.70e-03	1.80e-03	1.13e-03	0.00e+00	3.14e-03	4.35e-04	3.72e-04	4.20e-03	1.96e-03	7.90e-02		2.53e-01
3	807208	812451	130334	145664	1.09e-02	5.09e-02	4.41e-03	4.07e-03	3.00e-04	0.00e+00	3.43e-03	7.67e-04	4.36e-04	4.32e-03	2.04e-03	8.15e-02		3.35e-01
4	806755	812756	143428	138185	1.18e-02	5.43e-02	4.13e-03	5.57e-03	5.81e-04	0.00e+00	3.62e-03	5.85e-04	4.15e-04	4.37e-03	2.06e-03	8.74e-02		4.22e-01
5	811081	817458	144686	138685	1.09e-02	4.89e-02	4.49e-03	3.13e-03	4.69e-04	0.00e+00	3.59e-03	7.92e-04	4.18e-04	4.89e-03	2.13e-03	7.97e-02		5.02e-01
6	807001	803978	141166	134789	1.10e-02	5.55e-02	4.35e-03	2.21e-03	3.66e-04	0.00e+00	3.59e-03	8.30e-04	4.48e-04	4.47e-03	2.06e-03	8.48e-02		5.87e-01
7	812024	817315	146052	149075	1.09e-02	4.90e-02	4.29e-03	1.99e-03	3.98e-04	0.00e+00	3.66e-03	5.91e-04	4.65e-04	4.18e-03	2.15e-03	7.76e-02		6.64e-01
8	811621	813502	141835	136544	1.10e-02	5.61e-02	4.44e-03	1.95e-03	3.83e-04	0.00e+00	3.66e-03	5.73e-04	4.54e-04	4.18e-03	2.12e-03	8.49e-02		7.49e-01
9	810444	812821	143044	141163	1.10e-02	5.09e-02	4.41e-03	2.10e-03	2.02e-03	0.00e+00	3.68e-03	8.53e-04	4.71e-04	4.23e-03	2.12e-03	8.18e-02		8.31e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.5800 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.5800 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.580032  0.000000
];

