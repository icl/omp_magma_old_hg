% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_5 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	820440	808217	141586	141586	4.86e-03	1.99e-01	4.42e-02	1.29e-02	2.81e-03	0.00e+00	4.57e-02	2.46e-04	4.75e-03	1.80e-02	2.82e-02	3.60e-01		3.60e-01
1	816824	805552	127778	140001	5.06e-03	2.41e-01	4.39e-02	1.02e-02	4.32e-03	0.00e+00	5.55e-02	2.45e-04	5.15e-03	1.68e-02	3.63e-02	4.19e-01		7.79e-01
2	810480	801794	132200	143472	5.20e-03	4.20e-01	5.53e-02	9.02e-03	4.02e-03	0.00e+00	6.50e-02	3.01e-04	5.31e-03	1.74e-02	3.92e-02	6.21e-01		1.40e+00
3	810681	814749	139350	148036	5.14e-03	5.93e-01	6.13e-02	9.65e-03	5.06e-03	0.00e+00	7.59e-02	2.69e-04	6.19e-03	1.69e-02	3.81e-02	8.11e-01		2.21e+00
4	805556	807664	139955	135887	5.28e-03	5.86e-01	6.49e-02	1.02e-02	6.71e-03	0.00e+00	8.11e-02	3.80e-04	6.48e-03	1.66e-02	3.61e-02	8.13e-01		3.02e+00
5	808642	810419	145885	143777	5.15e-03	5.53e-01	6.42e-02	1.15e-02	7.51e-03	0.00e+00	8.36e-02	2.28e-04	6.63e-03	1.68e-02	3.67e-02	7.85e-01		3.81e+00
6	808243	813913	143605	141828	5.22e-03	5.72e-01	6.49e-02	1.02e-02	6.72e-03	0.00e+00	7.93e-02	2.94e-04	6.41e-03	1.68e-02	3.69e-02	7.98e-01		4.61e+00
7	815316	814072	144810	139140	5.23e-03	5.81e-01	6.58e-02	1.08e-02	6.51e-03	0.00e+00	7.90e-02	4.57e-04	6.40e-03	1.71e-02	3.74e-02	8.10e-01		5.42e+00
8	809536	812358	138543	139787	5.22e-03	5.99e-01	6.68e-02	9.59e-03	6.68e-03	0.00e+00	7.81e-02	3.71e-04	6.36e-03	1.67e-02	3.69e-02	8.25e-01		6.24e+00
9	810395	813466	145129	142307	5.25e-03	5.90e-01	6.63e-02	1.00e-02	6.78e-03	0.00e+00	8.08e-02	2.91e-04	6.36e-03	1.69e-02	3.71e-02	8.20e-01		7.06e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 7.6281 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 7.6281 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  7.628084  0.000000
];

