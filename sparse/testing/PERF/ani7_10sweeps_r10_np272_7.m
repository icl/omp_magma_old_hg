% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_272 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	814367	804425	141586	141586	1.16e-02	4.65e-02	9.56e-03	2.16e-02	2.20e-03	0.00e+00	2.55e-03	5.21e-04	3.24e-04	1.32e-02	1.60e-03	1.10e-01		1.10e-01
1	805602	812680	133851	143793	1.16e-02	3.83e-02	3.01e-03	3.22e-03	2.53e-03	0.00e+00	3.17e-03	4.07e-04	3.45e-04	7.45e-03	2.04e-03	7.21e-02		1.82e-01
2	801931	803450	143422	136344	1.17e-02	5.66e-02	3.70e-03	2.75e-03	1.55e-03	0.00e+00	3.53e-03	6.69e-04	4.32e-04	4.26e-03	2.05e-03	8.72e-02		2.69e-01
3	800718	813210	147899	146380	1.16e-02	5.43e-02	4.00e-03	2.54e-03	3.50e-04	0.00e+00	3.74e-03	5.19e-04	4.49e-04	4.19e-03	2.05e-03	8.38e-02		3.53e-01
4	800062	804653	149918	137426	1.17e-02	5.90e-02	4.15e-03	2.89e-03	5.54e-04	0.00e+00	3.98e-03	8.01e-04	4.55e-04	4.20e-03	1.99e-03	8.98e-02		4.42e-01
5	805248	809421	151379	146788	1.16e-02	5.10e-02	4.05e-03	3.65e-03	4.55e-04	0.00e+00	4.07e-03	5.60e-04	4.39e-04	5.00e-03	2.00e-03	8.28e-02		5.25e-01
6	809529	812731	146999	142826	1.17e-02	5.80e-02	4.11e-03	3.01e-03	4.01e-04	0.00e+00	4.00e-03	7.00e-04	4.72e-04	4.25e-03	2.05e-03	8.87e-02		6.14e-01
7	811155	809830	143524	140322	1.17e-02	5.32e-02	4.22e-03	3.37e-03	4.01e-04	0.00e+00	4.03e-03	8.09e-04	4.25e-04	4.70e-03	2.07e-03	8.50e-02		6.99e-01
8	811658	815676	142704	144029	1.17e-02	5.88e-02	4.16e-03	3.18e-03	1.61e-03	0.00e+00	4.01e-03	6.11e-04	5.01e-04	4.43e-03	2.06e-03	9.11e-02		7.90e-01
9	815761	815705	143007	138989	1.18e-02	5.43e-02	4.26e-03	2.03e-03	2.14e-03	0.00e+00	4.11e-03	9.12e-04	4.22e-04	4.33e-03	2.10e-03	8.64e-02		8.76e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.6057 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.6057 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.605713  0.000000
];

