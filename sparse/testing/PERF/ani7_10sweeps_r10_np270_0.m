% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_270 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	817728	799644	141586	141586	1.16e-02	4.48e-02	9.41e-03	2.17e-02	2.29e-03	0.00e+00	2.56e-03	4.59e-04	3.76e-04	1.32e-02	1.50e-03	1.08e-01		1.08e-01
1	810670	804839	130490	148574	1.16e-02	3.70e-02	2.99e-03	5.37e-03	2.34e-03	0.00e+00	3.20e-03	4.29e-04	3.78e-04	7.31e-03	1.88e-03	7.24e-02		1.80e-01
2	787577	797650	138354	144185	1.16e-02	5.19e-02	3.60e-03	1.94e-03	1.34e-03	0.00e+00	3.57e-03	4.69e-04	4.18e-04	4.07e-03	1.97e-03	8.08e-02		2.61e-01
3	806305	814623	162253	152180	1.15e-02	5.19e-02	3.93e-03	2.29e-03	3.34e-04	0.00e+00	3.81e-03	5.79e-04	4.30e-04	4.15e-03	2.09e-03	8.10e-02		3.42e-01
4	809470	814955	144331	136013	1.17e-02	5.97e-02	4.99e-03	2.60e-03	8.00e-04	0.00e+00	3.98e-03	5.61e-04	4.62e-04	4.28e-03	2.03e-03	9.11e-02		4.33e-01
5	814287	812523	141971	136486	1.17e-02	5.29e-02	4.17e-03	2.43e-03	4.31e-04	0.00e+00	4.05e-03	8.60e-04	3.99e-04	5.00e-03	2.06e-03	8.40e-02		5.17e-01
6	812031	810692	137960	139724	1.17e-02	5.94e-02	4.22e-03	3.11e-03	3.97e-04	0.00e+00	4.15e-03	9.96e-04	4.66e-04	4.50e-03	2.05e-03	9.11e-02		6.08e-01
7	813974	810687	141022	142361	1.17e-02	5.38e-02	4.22e-03	3.57e-03	3.90e-04	0.00e+00	4.04e-03	6.81e-04	3.92e-04	4.36e-03	3.63e-03	8.68e-02		6.95e-01
8	812828	812231	139885	143172	1.16e-02	5.90e-02	4.23e-03	1.77e-03	1.60e-03	0.00e+00	4.06e-03	6.65e-04	4.06e-04	4.71e-03	2.09e-03	9.02e-02		7.85e-01
9	811659	810438	141837	142434	1.17e-02	5.45e-02	4.27e-03	2.57e-03	1.78e-03	0.00e+00	4.11e-03	6.15e-04	4.31e-04	4.17e-03	2.06e-03	8.62e-02		8.71e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.6033 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.6033 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.603269  0.000000
];

