% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_39 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	815392	814046	141586	141586	3.28e-03	3.43e-02	6.70e-03	7.21e-03	6.69e-04	0.00e+00	6.16e-03	3.02e-04	6.39e-04	5.70e-03	3.64e-03	6.86e-02		6.86e-02
1	808826	812321	132826	134172	3.30e-03	4.12e-02	5.44e-03	3.03e-03	6.71e-04	0.00e+00	7.62e-03	2.64e-04	7.46e-04	4.63e-03	4.65e-03	7.15e-02		1.40e-01
2	798900	821461	140198	136703	3.35e-03	6.31e-02	7.03e-03	2.26e-03	6.10e-04	0.00e+00	9.16e-03	3.53e-04	8.08e-04	3.97e-03	5.03e-03	9.57e-02		2.36e-01
3	799796	810741	150930	128369	3.36e-03	9.00e-02	8.18e-03	1.93e-03	7.78e-04	0.00e+00	1.05e-02	3.22e-04	8.94e-04	3.91e-03	4.78e-03	1.25e-01		3.61e-01
4	811469	804975	150840	139895	3.33e-03	8.75e-02	8.09e-03	1.97e-03	1.11e-03	0.00e+00	1.16e-02	3.99e-04	9.91e-04	4.18e-03	4.69e-03	1.24e-01		4.84e-01
5	806106	807223	139972	146466	3.32e-03	8.50e-02	8.14e-03	2.31e-03	1.46e-03	0.00e+00	1.16e-02	4.05e-04	9.50e-04	4.23e-03	4.69e-03	1.22e-01		6.07e-01
6	809693	810072	146141	145024	3.32e-03	8.89e-02	8.19e-03	2.40e-03	1.04e-03	0.00e+00	1.16e-02	3.36e-04	9.27e-04	4.07e-03	4.71e-03	1.26e-01		7.32e-01
7	815311	812318	143360	142981	3.34e-03	8.79e-02	8.31e-03	1.90e-03	1.29e-03	0.00e+00	1.16e-02	3.98e-04	9.35e-04	3.90e-03	4.75e-03	1.24e-01		8.56e-01
8	812328	812717	138548	141541	3.35e-03	9.19e-02	8.42e-03	1.96e-03	1.48e-03	0.00e+00	1.20e-02	2.93e-04	9.53e-04	3.99e-03	4.77e-03	1.29e-01		9.86e-01
9	813227	810470	142337	141948	3.40e-03	9.05e-02	8.41e-03	2.11e-03	1.22e-03	0.00e+00	1.17e-02	3.88e-04	9.48e-04	3.90e-03	4.73e-03	1.27e-01		1.11e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.4878 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.4878 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.487827  0.000000
];

