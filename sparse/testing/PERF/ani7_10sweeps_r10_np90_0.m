% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_90 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	814454	814467	141586	141586	5.43e-03	3.04e-02	6.34e-03	9.60e-03	6.69e-04	0.00e+00	2.96e-03	3.56e-04	4.98e-04	6.79e-03	2.46e-03	6.55e-02		6.55e-02
1	797857	810758	133764	133751	5.48e-03	2.99e-02	4.37e-03	2.91e-03	1.82e-03	0.00e+00	3.59e-03	2.98e-04	4.85e-04	4.45e-03	2.58e-03	5.59e-02		1.21e-01
2	803011	803575	151167	138266	5.41e-03	4.69e-02	4.76e-03	1.59e-03	3.46e-04	0.00e+00	4.14e-03	4.58e-04	4.89e-04	3.35e-03	2.74e-03	7.02e-02		1.92e-01
3	808627	810322	146819	146255	5.35e-03	5.06e-02	5.12e-03	1.81e-03	4.04e-04	0.00e+00	4.54e-03	4.66e-04	4.96e-04	3.24e-03	2.87e-03	7.49e-02		2.67e-01
4	814713	813373	142009	140314	5.39e-03	5.33e-02	5.59e-03	1.65e-03	6.54e-04	0.00e+00	5.13e-03	4.48e-04	5.40e-04	3.56e-03	2.97e-03	7.92e-02		3.46e-01
5	810146	813739	136728	138068	5.41e-03	4.88e-02	5.98e-03	3.36e-03	5.13e-04	0.00e+00	4.89e-03	2.13e-04	5.51e-04	3.51e-03	2.97e-03	7.62e-02		4.22e-01
6	814263	813382	142101	138508	5.45e-03	5.19e-02	5.98e-03	2.13e-03	6.05e-04	0.00e+00	5.19e-03	5.91e-04	5.55e-04	3.51e-03	2.93e-03	7.88e-02		5.01e-01
7	810920	814947	138790	139671	5.43e-03	5.12e-02	6.02e-03	1.35e-03	5.18e-04	0.00e+00	5.05e-03	5.07e-04	5.48e-04	3.25e-03	2.94e-03	7.68e-02		5.78e-01
8	814082	809389	142939	138912	5.43e-03	5.38e-02	6.00e-03	1.77e-03	6.72e-04	0.00e+00	5.14e-03	3.95e-04	5.67e-04	3.41e-03	2.94e-03	8.01e-02		6.58e-01
9	809862	820147	140583	145276	5.42e-03	5.10e-02	5.99e-03	1.98e-03	1.14e-03	0.00e+00	5.23e-03	4.64e-04	5.80e-04	3.25e-03	3.15e-03	7.82e-02		7.36e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.2101 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.2101 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.210137  0.000000
];

