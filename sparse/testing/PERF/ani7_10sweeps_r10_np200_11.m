% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_200 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	816555	817089	141586	141586	8.84e-03	3.64e-02	7.75e-03	1.61e-02	1.39e-03	0.00e+00	2.73e-03	3.78e-04	3.59e-04	1.15e-02	1.74e-03	8.71e-02		8.71e-02
1	797675	806146	131663	131129	8.91e-03	3.24e-02	3.20e-03	3.43e-03	1.75e-03	0.00e+00	3.39e-03	4.18e-04	3.93e-04	5.69e-03	1.91e-03	6.15e-02		1.49e-01
2	810008	816696	151349	142878	8.80e-03	4.53e-02	3.70e-03	2.52e-03	4.98e-04	0.00e+00	3.76e-03	4.44e-04	3.90e-04	4.27e-03	2.20e-03	7.18e-02		2.20e-01
3	804645	801811	139822	133134	1.01e-02	5.20e-02	4.30e-03	2.30e-03	3.86e-04	0.00e+00	4.14e-03	4.94e-04	4.51e-04	3.86e-03	2.20e-03	8.03e-02		3.01e-01
4	804364	804643	145991	148825	8.78e-03	5.42e-02	4.88e-03	2.81e-03	4.43e-04	0.00e+00	4.31e-03	6.91e-04	4.36e-04	3.91e-03	2.37e-03	8.28e-02		3.83e-01
5	810401	804903	147077	146798	1.08e-02	4.85e-02	5.00e-03	1.80e-03	4.58e-04	0.00e+00	4.37e-03	7.60e-04	4.39e-04	4.30e-03	2.44e-03	7.89e-02		4.62e-01
6	811584	810429	141846	147344	1.08e-02	5.35e-02	4.95e-03	2.03e-03	4.69e-04	0.00e+00	4.32e-03	4.72e-04	4.64e-04	3.91e-03	2.42e-03	8.34e-02		5.46e-01
7	810192	819045	141469	142624	1.09e-02	5.09e-02	5.17e-03	2.79e-03	4.96e-04	0.00e+00	4.50e-03	6.53e-04	4.48e-04	4.10e-03	2.49e-03	8.24e-02		6.28e-01
8	808587	812194	143667	134814	1.10e-02	5.54e-02	5.27e-03	1.65e-02	1.29e-03	0.00e+00	4.45e-03	5.60e-04	4.67e-04	3.81e-03	2.41e-03	1.01e-01		7.29e-01
9	813004	812827	146078	142471	1.07e-02	5.10e-02	4.39e-03	3.04e-03	7.72e-04	0.00e+00	4.42e-03	4.45e-04	4.40e-04	3.87e-03	2.23e-03	8.13e-02		8.11e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.4169 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.4169 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.416895  0.000000
];

