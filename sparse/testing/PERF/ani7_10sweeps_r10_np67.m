% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_67 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	810918	803854	141586	141586	3.26e-03	3.40e-02	6.80e-03	8.37e-03	8.65e-04	0.00e+00	3.61e-03	2.78e-04	4.61e-04	7.69e-03	2.19e-03	6.75e-02		6.75e-02
1	806197	821132	137300	144364	3.45e-03	3.09e-02	3.46e-03	1.58e-03	1.04e-03	0.00e+00	4.43e-03	2.35e-04	4.53e-04	5.40e-03	2.92e-03	5.39e-02		1.21e-01
2	805488	823750	142827	127892	3.35e-03	5.40e-02	4.17e-03	1.71e-03	7.69e-04	0.00e+00	5.50e-03	2.26e-04	5.25e-04	3.20e-03	3.15e-03	7.66e-02		1.98e-01
3	813886	804508	144342	126080	3.45e-03	6.76e-02	4.96e-03	1.53e-03	4.79e-04	0.00e+00	6.19e-03	3.06e-04	6.07e-04	3.21e-03	2.98e-03	9.13e-02		2.89e-01
4	803241	809528	136750	146128	3.44e-03	6.63e-02	4.86e-03	1.48e-03	6.38e-04	0.00e+00	6.70e-03	3.16e-04	6.58e-04	3.17e-03	2.84e-03	9.04e-02		3.80e-01
5	813640	813286	148200	141913	3.29e-03	6.01e-02	4.72e-03	1.65e-03	6.74e-04	0.00e+00	6.62e-03	4.61e-04	5.99e-04	3.50e-03	2.91e-03	8.46e-02		4.64e-01
6	812608	814480	138607	138961	3.44e-03	6.54e-02	4.89e-03	1.65e-03	6.83e-04	0.00e+00	6.80e-03	2.98e-04	6.31e-04	3.22e-03	2.91e-03	8.99e-02		5.54e-01
7	809919	812687	140445	138573	3.36e-03	6.13e-02	4.88e-03	1.50e-03	6.42e-04	0.00e+00	6.70e-03	3.49e-04	5.85e-04	3.48e-03	2.92e-03	8.57e-02		6.40e-01
8	806553	814066	143940	141172	3.34e-03	6.47e-02	4.89e-03	1.43e-03	1.07e-03	0.00e+00	6.81e-03	3.05e-04	6.44e-04	3.25e-03	2.92e-03	8.93e-02		7.29e-01
9	812290	813760	148112	140599	3.32e-03	6.11e-02	4.84e-03	1.80e-03	1.12e-03	0.00e+00	6.78e-03	3.50e-04	6.49e-04	3.18e-03	2.91e-03	8.60e-02		8.15e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.2019 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.2019 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.201944  0.000000
];

