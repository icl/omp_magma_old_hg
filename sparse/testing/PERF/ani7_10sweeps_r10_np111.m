% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_111 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	822570	807276	141586	141586	7.10e-03	4.14e-02	9.60e-03	1.33e-02	7.53e-04	0.00e+00	3.34e-03	4.74e-04	4.39e-04	1.34e-02	2.07e-03	9.19e-02		9.19e-02
1	813691	821094	125648	140942	6.86e-03	3.53e-02	4.06e-03	3.86e-03	1.22e-03	0.00e+00	4.10e-03	3.47e-04	4.07e-04	5.95e-03	2.79e-03	6.49e-02		1.57e-01
2	794857	803944	135333	127930	6.69e-03	5.86e-02	4.73e-03	1.97e-03	4.41e-04	0.00e+00	4.88e-03	3.73e-04	5.40e-04	3.62e-03	2.76e-03	8.46e-02		2.41e-01
3	804455	805678	154973	145886	6.47e-03	5.66e-02	4.83e-03	2.63e-03	5.81e-04	0.00e+00	5.25e-03	3.17e-04	5.85e-04	3.71e-03	2.77e-03	8.38e-02		3.25e-01
4	806223	801728	146181	144958	6.64e-03	6.07e-02	5.34e-03	1.73e-03	6.34e-04	0.00e+00	5.68e-03	5.59e-04	6.15e-04	4.07e-03	2.62e-03	8.86e-02		4.14e-01
5	808707	806135	145218	149713	6.78e-03	5.56e-02	4.99e-03	2.28e-03	8.98e-04	0.00e+00	5.59e-03	5.23e-04	6.11e-04	4.21e-03	2.65e-03	8.42e-02		4.98e-01
6	809472	812924	143540	146112	6.56e-03	6.01e-02	5.05e-03	2.73e-03	5.99e-04	0.00e+00	5.66e-03	6.67e-04	6.05e-04	4.17e-03	2.67e-03	8.88e-02		5.87e-01
7	812655	811886	143581	140129	6.70e-03	5.74e-02	5.19e-03	1.85e-03	5.59e-04	0.00e+00	5.81e-03	3.10e-04	5.97e-04	3.61e-03	2.71e-03	8.48e-02		6.72e-01
8	812535	811488	141204	141973	6.57e-03	6.19e-02	5.16e-03	2.62e-03	9.37e-04	0.00e+00	5.79e-03	7.35e-04	5.83e-04	3.79e-03	2.69e-03	9.08e-02		7.62e-01
9	815333	813865	142130	143177	7.00e-03	5.91e-02	5.36e-03	1.76e-03	1.09e-03	0.00e+00	5.76e-03	4.93e-04	5.65e-04	3.85e-03	2.77e-03	8.77e-02		8.50e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.4035 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.4035 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.403524  0.000000
];

