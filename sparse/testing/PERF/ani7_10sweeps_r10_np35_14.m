% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_35 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	820484	811137	141586	141586	3.33e-03	3.71e-02	6.88e-03	6.75e-03	6.91e-04	0.00e+00	6.86e-03	2.36e-04	7.39e-04	6.31e-03	4.00e-03	7.29e-02		7.29e-02
1	789017	810743	127734	137081	3.35e-03	4.43e-02	6.18e-03	3.94e-03	8.65e-04	0.00e+00	8.37e-03	2.61e-04	8.07e-04	4.76e-03	5.06e-03	7.79e-02		1.51e-01
2	795432	800282	160007	138281	3.37e-03	6.80e-02	7.68e-03	1.88e-03	6.81e-04	0.00e+00	1.01e-02	3.21e-04	8.71e-04	4.11e-03	5.44e-03	1.02e-01		2.53e-01
3	810919	796268	154398	149548	3.32e-03	9.33e-02	8.40e-03	2.14e-03	8.94e-04	0.00e+00	1.17e-02	2.44e-04	9.87e-04	4.08e-03	5.16e-03	1.30e-01		3.84e-01
4	808718	815167	139717	154368	3.31e-03	9.33e-02	8.89e-03	2.91e-03	1.44e-03	0.00e+00	1.29e-02	3.33e-04	1.14e-03	4.33e-03	5.22e-03	1.34e-01		5.17e-01
5	810255	815900	142723	136274	3.38e-03	9.48e-02	9.15e-03	2.02e-03	1.29e-03	0.00e+00	1.23e-02	3.43e-04	9.75e-04	4.37e-03	5.29e-03	1.34e-01		6.51e-01
6	810570	812609	141992	136347	3.40e-03	9.72e-02	9.16e-03	2.37e-03	1.25e-03	0.00e+00	1.30e-02	3.71e-04	1.05e-03	4.26e-03	5.25e-03	1.37e-01		7.89e-01
7	808241	811320	142483	140444	3.37e-03	9.68e-02	9.24e-03	2.55e-03	1.16e-03	0.00e+00	1.29e-02	3.09e-04	1.04e-03	4.17e-03	5.23e-03	1.37e-01		9.25e-01
8	811949	814862	145618	142539	3.36e-03	9.76e-02	9.16e-03	2.05e-03	1.36e-03	0.00e+00	1.27e-02	4.06e-04	1.03e-03	4.27e-03	5.28e-03	1.37e-01		1.06e+00
9	808396	812804	142716	139803	3.40e-03	9.76e-02	9.29e-03	2.00e-03	1.78e-03	0.00e+00	1.31e-02	3.15e-04	1.07e-03	4.15e-03	5.23e-03	1.38e-01		1.20e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.5752 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.5752 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.575231  0.000000
];

