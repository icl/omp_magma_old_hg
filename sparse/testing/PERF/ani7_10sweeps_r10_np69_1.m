% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_69 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	819252	803729	141586	141586	5.10e-03	3.07e-02	6.73e-03	9.33e-03	5.37e-04	0.00e+00	3.73e-03	3.69e-04	5.68e-04	5.71e-03	2.80e-03	6.56e-02		6.56e-02
1	824058	809017	128966	144489	5.05e-03	2.96e-02	5.38e-03	2.34e-03	6.08e-04	0.00e+00	4.33e-03	3.39e-04	5.53e-04	4.20e-03	2.90e-03	5.53e-02		1.21e-01
2	812202	795435	124966	140007	5.11e-03	5.07e-02	5.64e-03	1.81e-03	4.44e-04	0.00e+00	5.36e-03	4.05e-04	5.27e-04	3.59e-03	2.98e-03	7.65e-02		1.97e-01
3	793451	813781	137628	154395	5.00e-03	6.12e-02	5.43e-03	1.38e-03	4.62e-04	0.00e+00	5.95e-03	3.94e-04	6.17e-04	3.49e-03	2.80e-03	8.67e-02		2.84e-01
4	811996	803523	157185	136855	5.13e-03	5.75e-02	5.50e-03	2.44e-03	6.14e-04	0.00e+00	6.48e-03	4.41e-04	6.13e-04	3.95e-03	2.69e-03	8.53e-02		3.69e-01
5	810627	811116	139445	147918	5.02e-03	5.45e-02	5.37e-03	2.38e-03	1.09e-03	0.00e+00	6.72e-03	4.01e-04	6.42e-04	3.77e-03	2.79e-03	8.27e-02		4.52e-01
6	811491	807105	141620	141131	5.08e-03	5.84e-02	5.37e-03	1.78e-03	6.18e-04	0.00e+00	6.54e-03	3.68e-04	6.49e-04	3.60e-03	2.79e-03	8.52e-02		5.37e-01
7	809023	809628	141562	145948	5.09e-03	5.76e-02	5.26e-03	2.55e-03	6.11e-04	0.00e+00	6.50e-03	4.41e-04	6.64e-04	3.47e-03	2.77e-03	8.50e-02		6.22e-01
8	811901	812139	144836	144231	5.09e-03	5.93e-02	5.25e-03	1.93e-03	9.76e-04	0.00e+00	6.49e-03	5.36e-04	6.15e-04	4.62e-03	2.79e-03	8.76e-02		7.10e-01
9	814990	813667	142764	142526	5.13e-03	5.83e-02	5.33e-03	1.77e-03	9.40e-04	0.00e+00	6.52e-03	5.77e-04	6.49e-04	3.49e-03	2.81e-03	8.55e-02		7.95e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.2744 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.2744 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.274369  0.000000
];

