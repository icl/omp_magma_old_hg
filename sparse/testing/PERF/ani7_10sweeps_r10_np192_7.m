% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_192 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	808113	806246	141586	141586	8.72e-03	3.67e-02	7.53e-03	1.59e-02	5.23e-04	0.00e+00	2.65e-03	5.05e-04	3.38e-04	9.93e-03	1.76e-03	8.45e-02		8.45e-02
1	800145	804154	140105	141972	8.67e-03	3.15e-02	3.23e-03	3.58e-03	5.69e-04	0.00e+00	3.15e-03	3.72e-04	3.59e-04	7.01e-03	1.96e-03	6.04e-02		1.45e-01
2	804467	817125	148879	144870	8.59e-03	4.50e-02	3.74e-03	3.73e-03	2.99e-04	0.00e+00	3.78e-03	4.88e-04	4.39e-04	4.00e-03	2.19e-03	7.23e-02		2.17e-01
3	816975	803942	145363	132705	8.77e-03	4.93e-02	4.22e-03	1.72e-03	3.36e-04	0.00e+00	4.25e-03	5.52e-04	4.73e-04	3.92e-03	2.19e-03	7.58e-02		2.93e-01
4	801436	805083	133661	146694	8.61e-03	5.26e-02	4.43e-03	3.07e-03	3.94e-04	0.00e+00	4.39e-03	6.42e-04	4.62e-04	4.82e-03	2.14e-03	8.16e-02		3.75e-01
5	813206	810447	150005	146358	8.62e-03	4.77e-02	4.29e-03	2.86e-03	9.32e-04	0.00e+00	4.35e-03	7.89e-04	4.97e-04	4.34e-03	2.25e-03	7.67e-02		4.51e-01
6	811055	811595	139041	141800	8.72e-03	5.30e-02	4.47e-03	2.40e-03	4.17e-04	0.00e+00	4.51e-03	5.51e-04	4.72e-04	4.39e-03	2.25e-03	8.12e-02		5.32e-01
7	812509	812035	141998	141458	8.69e-03	5.06e-02	4.51e-03	2.49e-03	4.15e-04	0.00e+00	4.36e-03	6.39e-04	4.75e-04	3.92e-03	2.24e-03	7.83e-02		6.11e-01
8	807653	813744	141350	141824	9.15e-03	5.48e-02	4.56e-03	1.47e-03	1.13e-03	0.00e+00	4.42e-03	4.79e-04	4.67e-04	4.09e-03	2.22e-03	8.28e-02		6.94e-01
9	812075	810709	147012	140921	8.79e-03	5.05e-02	4.55e-03	2.46e-03	1.12e-03	0.00e+00	4.33e-03	6.03e-04	4.64e-04	3.91e-03	2.25e-03	7.90e-02		7.73e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.4013 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.4013 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.401279  0.000000
];

