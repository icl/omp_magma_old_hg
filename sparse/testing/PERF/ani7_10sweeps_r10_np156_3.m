% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_156 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	814249	815621	141586	141586	7.84e-03	3.60e-02	7.63e-03	1.49e-02	2.27e-03	0.00e+00	2.46e-03	4.83e-04	3.79e-04	9.41e-03	2.09e-03	8.34e-02		8.34e-02
1	803350	824409	133969	132597	7.91e-03	3.02e-02	3.95e-03	3.00e-03	1.38e-03	0.00e+00	3.12e-03	4.25e-04	3.60e-04	5.86e-03	2.22e-03	5.85e-02		1.42e-01
2	816763	813367	145674	124615	7.95e-03	4.66e-02	4.19e-03	1.60e-03	7.76e-04	0.00e+00	3.38e-03	3.98e-04	4.29e-04	3.95e-03	2.26e-03	7.15e-02		2.13e-01
3	811141	802473	133067	136463	7.84e-03	4.91e-02	4.81e-03	2.73e-03	2.89e-04	0.00e+00	3.85e-03	4.77e-04	4.73e-04	4.01e-03	2.21e-03	7.58e-02		2.89e-01
4	812985	812794	139495	148163	9.26e-03	4.96e-02	5.12e-03	1.71e-03	6.00e-04	0.00e+00	4.05e-03	4.91e-04	4.60e-04	4.05e-03	2.31e-03	7.76e-02		3.67e-01
5	809808	811695	138456	138647	7.82e-03	4.42e-02	5.34e-03	2.94e-03	4.47e-04	0.00e+00	4.06e-03	3.95e-04	4.82e-04	4.46e-03	2.27e-03	7.24e-02		4.39e-01
6	810916	812303	142439	140552	7.88e-03	4.94e-02	5.30e-03	2.71e-03	3.50e-04	0.00e+00	3.95e-03	6.33e-04	4.40e-04	4.03e-03	2.22e-03	7.69e-02		5.16e-01
7	812655	810142	142137	140750	7.85e-03	4.61e-02	5.39e-03	2.08e-03	3.45e-04	0.00e+00	3.83e-03	5.51e-04	4.34e-04	3.82e-03	2.28e-03	7.27e-02		5.89e-01
8	809753	813673	141204	143717	7.80e-03	4.99e-02	5.34e-03	1.37e-03	3.63e-04	0.00e+00	4.07e-03	6.81e-04	4.40e-04	4.10e-03	2.28e-03	7.63e-02		6.65e-01
9	814364	809366	144912	140992	7.85e-03	4.59e-02	5.36e-03	2.60e-03	1.23e-03	0.00e+00	4.07e-03	5.36e-04	5.06e-04	3.90e-03	2.28e-03	7.42e-02		7.39e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.3820 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.3820 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.381963  0.000000
];

