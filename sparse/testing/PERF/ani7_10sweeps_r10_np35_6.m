% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_35 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	820445	811042	141586	141586	3.33e-03	3.67e-02	6.86e-03	6.76e-03	1.05e-03	0.00e+00	6.83e-03	2.67e-04	6.98e-04	6.22e-03	3.97e-03	7.27e-02		7.27e-02
1	802458	802846	127773	137176	3.33e-03	4.47e-02	6.05e-03	2.63e-03	1.20e-03	0.00e+00	8.41e-03	1.87e-04	8.33e-04	4.74e-03	5.07e-03	7.71e-02		1.50e-01
2	807518	804318	146566	146178	3.32e-03	6.83e-02	7.71e-03	2.17e-03	9.00e-04	0.00e+00	1.02e-02	3.29e-04	8.90e-04	4.12e-03	5.58e-03	1.04e-01		2.53e-01
3	809150	803985	142312	145512	3.34e-03	1.00e-01	8.73e-03	2.32e-03	8.51e-04	0.00e+00	1.16e-02	2.98e-04	9.96e-04	4.09e-03	5.23e-03	1.38e-01		3.91e-01
4	808973	806421	141486	146651	3.35e-03	9.37e-02	8.88e-03	3.13e-03	1.40e-03	0.00e+00	1.29e-02	3.08e-04	1.09e-03	4.06e-03	5.13e-03	1.34e-01		5.25e-01
5	810466	808874	142468	145020	3.37e-03	9.14e-02	8.96e-03	2.78e-03	1.32e-03	0.00e+00	1.30e-02	2.50e-04	1.07e-03	4.96e-03	5.19e-03	1.32e-01		6.57e-01
6	808989	812844	141781	143373	3.36e-03	9.44e-02	9.07e-03	2.44e-03	1.10e-03	0.00e+00	1.26e-02	4.17e-04	1.01e-03	4.17e-03	5.24e-03	1.34e-01		7.91e-01
7	813781	812007	144064	140209	3.39e-03	9.62e-02	9.17e-03	2.65e-03	1.05e-03	0.00e+00	1.26e-02	2.33e-04	9.96e-04	4.11e-03	5.28e-03	1.36e-01		9.27e-01
8	804636	812343	140078	141852	3.36e-03	9.85e-02	9.27e-03	2.50e-03	1.28e-03	0.00e+00	1.32e-02	3.64e-04	1.07e-03	4.20e-03	5.21e-03	1.39e-01		1.07e+00
9	813049	812865	150029	142322	3.38e-03	9.54e-02	9.15e-03	2.21e-03	1.82e-03	0.00e+00	1.31e-02	2.54e-04	1.07e-03	4.12e-03	5.29e-03	1.36e-01		1.20e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.5745 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.5745 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.574520  0.000000
];

