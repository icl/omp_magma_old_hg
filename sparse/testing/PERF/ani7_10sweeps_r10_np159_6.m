% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_159 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	814542	814135	141586	141586	7.98e-03	3.66e-02	7.61e-03	1.46e-02	1.06e-03	0.00e+00	2.42e-03	4.72e-04	4.28e-04	1.05e-02	2.06e-03	8.37e-02		8.37e-02
1	804483	818526	133676	134083	8.10e-03	3.11e-02	3.92e-03	5.45e-03	2.46e-03	0.00e+00	3.67e-03	3.77e-04	3.76e-04	6.09e-03	2.33e-03	6.39e-02		1.48e-01
2	802884	803429	144541	130498	9.61e-03	4.66e-02	4.40e-03	2.06e-03	3.04e-04	0.00e+00	4.16e-03	5.71e-04	4.30e-04	3.98e-03	2.48e-03	7.46e-02		2.22e-01
3	800983	802153	146946	146401	8.74e-03	4.63e-02	4.54e-03	2.15e-03	3.10e-04	0.00e+00	3.80e-03	4.24e-04	4.74e-04	3.92e-03	2.38e-03	7.31e-02		2.95e-01
4	817898	809399	149653	148483	7.85e-03	5.04e-02	4.83e-03	3.67e-03	5.40e-04	0.00e+00	4.01e-03	5.07e-04	4.83e-04	4.38e-03	2.60e-03	7.92e-02		3.75e-01
5	810515	809196	133543	142042	7.92e-03	4.42e-02	5.34e-03	2.58e-03	4.03e-04	0.00e+00	3.99e-03	3.10e-04	4.79e-04	4.43e-03	2.52e-03	7.21e-02		4.47e-01
6	811453	810753	141732	143051	7.95e-03	4.89e-02	5.26e-03	2.78e-03	3.83e-04	0.00e+00	3.98e-03	7.02e-04	4.46e-04	4.33e-03	2.55e-03	7.73e-02		5.24e-01
7	812204	812228	141600	142300	7.95e-03	4.51e-02	5.33e-03	1.47e-03	4.03e-04	0.00e+00	3.98e-03	5.51e-04	4.62e-04	3.93e-03	2.52e-03	7.17e-02		5.96e-01
8	812901	810510	141655	141631	7.93e-03	4.99e-02	5.32e-03	1.85e-03	4.08e-04	0.00e+00	3.98e-03	9.27e-04	4.43e-04	4.16e-03	2.52e-03	7.75e-02		6.73e-01
9	813552	811647	141764	144155	7.97e-03	4.58e-02	5.36e-03	2.25e-03	1.21e-03	0.00e+00	4.00e-03	6.87e-04	4.66e-04	3.99e-03	2.50e-03	7.42e-02		7.47e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.3841 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.3841 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.384134  0.000000
];

