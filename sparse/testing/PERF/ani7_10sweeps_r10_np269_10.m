% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_269 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	829871	816676	141586	141586	1.17e-02	4.55e-02	9.62e-03	2.20e-02	9.73e-04	0.00e+00	2.59e-03	4.74e-04	3.84e-04	1.36e-02	1.62e-03	1.08e-01		1.08e-01
1	794060	808469	118347	131542	1.18e-02	3.74e-02	3.06e-03	4.37e-03	9.11e-04	0.00e+00	3.34e-03	3.79e-04	3.96e-04	7.19e-03	1.81e-03	7.07e-02		1.79e-01
2	816187	808030	154964	140555	1.17e-02	5.08e-02	3.56e-03	1.65e-03	3.18e-04	0.00e+00	3.42e-03	6.04e-04	3.75e-04	4.26e-03	2.03e-03	7.87e-02		2.58e-01
3	804411	820966	133643	141800	1.16e-02	5.46e-02	5.58e-03	3.21e-03	3.16e-04	0.00e+00	3.82e-03	5.99e-04	4.37e-04	4.32e-03	2.14e-03	8.67e-02		3.44e-01
4	806496	813736	146225	129670	1.19e-02	6.08e-02	4.23e-03	3.78e-03	4.50e-04	0.00e+00	3.82e-03	8.21e-04	4.39e-04	5.52e-03	2.02e-03	9.38e-02		4.38e-01
5	808564	807900	144945	137705	1.17e-02	5.26e-02	4.20e-03	2.34e-03	1.42e-03	0.00e+00	3.85e-03	6.82e-04	4.53e-04	4.93e-03	2.06e-03	8.43e-02		5.22e-01
6	812723	812146	143683	144347	1.17e-02	5.73e-02	4.19e-03	2.34e-03	4.46e-04	0.00e+00	4.02e-03	5.79e-04	4.36e-04	4.97e-03	2.05e-03	8.80e-02		6.11e-01
7	813530	813928	140330	140907	1.18e-02	5.44e-02	4.21e-03	2.60e-03	3.83e-04	0.00e+00	4.01e-03	8.10e-04	4.46e-04	4.27e-03	2.09e-03	8.51e-02		6.96e-01
8	807201	811108	140329	139931	1.18e-02	5.96e-02	4.29e-03	2.39e-03	1.48e-03	0.00e+00	3.97e-03	6.70e-04	4.39e-04	4.63e-03	2.04e-03	9.13e-02		7.87e-01
9	813250	812017	147464	143557	1.17e-02	5.34e-02	4.21e-03	1.85e-03	2.00e-03	0.00e+00	4.12e-03	7.95e-04	4.01e-04	4.21e-03	2.06e-03	8.48e-02		8.72e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.8361 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.8361 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.836107  0.000000
];

