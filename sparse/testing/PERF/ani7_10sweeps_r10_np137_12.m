% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_137 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	815948	813664	141586	141586	7.36e-03	3.40e-02	6.46e-03	1.44e-02	1.76e-03	0.00e+00	2.79e-03	5.26e-04	4.04e-04	9.12e-03	2.30e-03	7.91e-02		7.91e-02
1	804097	809386	132270	134554	7.37e-03	3.13e-02	4.33e-03	3.60e-03	1.42e-03	0.00e+00	3.20e-03	4.64e-04	4.22e-04	5.41e-03	2.33e-03	5.98e-02		1.39e-01
2	805924	815694	144927	139638	7.33e-03	4.69e-02	4.45e-03	2.78e-03	8.32e-04	0.00e+00	3.76e-03	5.90e-04	4.25e-04	3.99e-03	2.35e-03	7.34e-02		2.12e-01
3	819559	801158	143906	134136	7.40e-03	5.26e-02	4.57e-03	1.98e-03	3.32e-04	0.00e+00	4.37e-03	4.40e-04	5.20e-04	3.96e-03	2.28e-03	7.84e-02		2.91e-01
4	815048	805723	131077	149478	7.22e-03	5.30e-02	4.69e-03	3.08e-03	6.69e-04	0.00e+00	4.62e-03	5.48e-04	5.08e-04	4.09e-03	2.18e-03	8.06e-02		3.71e-01
5	810437	811581	136393	145718	7.29e-03	4.89e-02	4.57e-03	1.59e-03	4.95e-04	0.00e+00	4.62e-03	6.33e-04	4.99e-04	4.44e-03	2.19e-03	7.53e-02		4.47e-01
6	810315	812056	141810	140666	7.43e-03	5.23e-02	5.66e-03	1.98e-03	4.43e-04	0.00e+00	4.72e-03	5.44e-04	5.24e-04	4.25e-03	2.73e-03	8.06e-02		5.27e-01
7	811967	810571	142738	140997	8.61e-03	4.95e-02	5.69e-03	3.00e-03	3.82e-04	0.00e+00	4.37e-03	5.02e-04	4.73e-04	4.02e-03	2.74e-03	7.93e-02		6.07e-01
8	812687	812198	141892	143288	8.59e-03	5.23e-02	5.68e-03	2.09e-03	4.06e-04	0.00e+00	4.54e-03	9.59e-04	4.83e-04	4.24e-03	2.74e-03	8.20e-02		6.89e-01
9	813077	814965	141978	142467	8.65e-03	5.02e-02	5.75e-03	2.24e-03	1.21e-03	0.00e+00	4.63e-03	6.76e-04	4.87e-04	4.01e-03	2.77e-03	8.06e-02		7.69e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.3935 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.3935 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.393480  0.000000
];

