% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_63 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	820842	807388	141586	141586	3.23e-03	2.80e-02	5.30e-03	6.93e-03	4.81e-04	0.00e+00	3.84e-03	2.48e-04	4.34e-04	5.64e-03	2.27e-03	5.64e-02		5.64e-02
1	810133	822920	127376	140830	3.25e-03	3.00e-02	3.40e-03	1.67e-03	5.35e-04	0.00e+00	4.66e-03	2.44e-04	5.12e-04	4.25e-03	3.11e-03	5.17e-02		1.08e-01
2	818185	805193	138891	126104	3.33e-03	5.13e-02	6.53e-03	1.44e-03	4.01e-04	0.00e+00	5.94e-03	2.83e-04	6.28e-04	3.32e-03	4.14e-03	7.73e-02		1.85e-01
3	786220	804024	131645	144637	5.19e-03	6.75e-02	7.16e-03	1.81e-03	5.63e-04	0.00e+00	6.90e-03	3.40e-04	7.28e-04	3.24e-03	3.94e-03	9.73e-02		2.83e-01
4	808211	803894	164416	146612	5.18e-03	5.96e-02	7.24e-03	2.71e-03	8.08e-04	0.00e+00	7.27e-03	2.70e-04	7.32e-04	3.83e-03	4.02e-03	9.16e-02		3.74e-01
5	813491	805242	143230	147547	5.23e-03	5.97e-02	5.10e-03	1.95e-03	1.09e-03	0.00e+00	7.28e-03	2.37e-04	7.16e-04	3.35e-03	2.99e-03	8.76e-02		4.62e-01
6	812679	814184	138756	147005	3.27e-03	6.22e-02	5.14e-03	1.97e-03	7.42e-04	0.00e+00	7.26e-03	3.40e-04	6.93e-04	3.30e-03	3.05e-03	8.80e-02		5.50e-01
7	812336	812726	140374	138869	3.29e-03	6.16e-02	5.21e-03	1.71e-03	6.55e-04	0.00e+00	7.04e-03	3.94e-04	6.67e-04	3.24e-03	3.04e-03	8.68e-02		6.37e-01
8	810614	814016	141523	141133	3.32e-03	6.31e-02	5.21e-03	1.56e-03	8.77e-04	0.00e+00	7.10e-03	3.24e-04	6.68e-04	3.33e-03	3.05e-03	8.85e-02		7.25e-01
9	814246	812540	144051	140649	3.30e-03	6.19e-02	5.21e-03	1.72e-03	1.16e-03	0.00e+00	7.35e-03	4.56e-04	6.88e-04	3.25e-03	3.05e-03	8.81e-02		8.13e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.1863 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.1863 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.186287  0.000000
];

