% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_68 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	820147	811297	141586	141586	3.26e-03	2.67e-02	4.50e-03	8.51e-03	1.65e-03	0.00e+00	3.60e-03	2.68e-04	4.11e-04	6.10e-03	2.27e-03	5.73e-02		5.73e-02
1	820707	807991	128071	136921	3.23e-03	2.96e-02	3.75e-03	3.41e-03	6.41e-04	0.00e+00	4.43e-03	2.78e-04	4.82e-04	4.02e-03	2.86e-03	5.27e-02		1.10e-01
2	824814	797424	128317	141033	3.26e-03	5.05e-02	4.41e-03	1.37e-03	5.45e-04	0.00e+00	5.38e-03	1.80e-04	5.39e-04	3.57e-03	3.08e-03	7.29e-02		1.83e-01
3	804686	806598	125016	152406	3.48e-03	6.59e-02	4.52e-03	1.41e-03	4.75e-04	0.00e+00	6.14e-03	3.57e-04	6.40e-04	3.34e-03	2.87e-03	8.91e-02		2.72e-01
4	816977	811733	145950	144038	3.27e-03	5.90e-02	4.66e-03	2.86e-03	7.05e-04	0.00e+00	6.66e-03	3.58e-04	6.39e-04	3.21e-03	2.79e-03	8.41e-02		3.56e-01
5	812590	805884	134464	139708	3.41e-03	5.65e-02	4.82e-03	2.23e-03	6.90e-04	0.00e+00	6.60e-03	2.48e-04	6.53e-04	3.45e-03	2.76e-03	8.13e-02		4.37e-01
6	810531	811250	139657	146363	3.47e-03	5.90e-02	4.78e-03	1.67e-03	6.92e-04	0.00e+00	6.71e-03	2.70e-04	6.55e-04	3.31e-03	2.80e-03	8.33e-02		5.21e-01
7	813554	813412	142522	141803	3.33e-03	5.82e-02	4.83e-03	2.07e-03	6.34e-04	0.00e+00	6.42e-03	3.29e-04	6.23e-04	3.72e-03	2.88e-03	8.31e-02		6.04e-01
8	814225	811560	140305	140447	3.39e-03	6.09e-02	4.84e-03	1.57e-03	1.20e-03	0.00e+00	6.77e-03	4.83e-04	6.73e-04	3.36e-03	2.84e-03	8.60e-02		6.90e-01
9	815285	812016	140440	143105	3.29e-03	5.94e-02	4.86e-03	2.23e-03	1.04e-03	0.00e+00	6.64e-03	3.71e-04	6.39e-04	3.21e-03	3.22e-03	8.49e-02		7.75e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.1750 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.1750 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.175021  0.000000
];

