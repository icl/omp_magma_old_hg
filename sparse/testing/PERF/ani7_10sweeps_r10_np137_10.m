% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_137 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	816156	813754	141586	141586	7.34e-03	3.41e-02	6.19e-03	1.40e-02	1.92e-03	0.00e+00	2.78e-03	5.68e-04	3.93e-04	9.08e-03	2.34e-03	7.88e-02		7.88e-02
1	812714	806822	132062	134464	7.36e-03	3.10e-02	4.36e-03	4.29e-03	1.62e-03	0.00e+00	3.27e-03	3.64e-04	4.07e-04	5.80e-03	2.32e-03	6.08e-02		1.40e-01
2	804032	802814	136310	142202	7.34e-03	4.70e-02	4.45e-03	2.79e-03	3.36e-04	0.00e+00	3.85e-03	5.46e-04	4.28e-04	4.06e-03	2.34e-03	7.32e-02		2.13e-01
3	805763	805791	145798	147016	7.29e-03	5.22e-02	4.84e-03	2.56e-03	3.41e-04	0.00e+00	4.29e-03	4.00e-04	4.77e-04	4.02e-03	2.41e-03	7.88e-02		2.92e-01
4	803570	809901	144873	144845	8.34e-03	5.08e-02	5.00e-03	3.66e-03	7.09e-04	0.00e+00	4.72e-03	3.99e-04	5.53e-04	4.16e-03	2.50e-03	8.08e-02		3.72e-01
5	810804	809811	147871	141540	8.33e-03	4.71e-02	5.18e-03	2.44e-03	4.90e-04	0.00e+00	4.72e-03	4.94e-04	5.07e-04	4.47e-03	2.52e-03	7.63e-02		4.49e-01
6	812432	810307	141443	142436	8.38e-03	5.16e-02	5.21e-03	2.35e-03	4.45e-04	0.00e+00	4.66e-03	4.13e-04	5.27e-04	4.26e-03	2.44e-03	8.03e-02		5.29e-01
7	808757	813456	140621	142746	8.37e-03	4.95e-02	5.71e-03	1.79e-03	5.33e-04	0.00e+00	5.70e-03	6.56e-04	5.29e-04	4.03e-03	2.78e-03	7.96e-02		6.09e-01
8	812242	812991	145102	140403	7.34e-03	5.21e-02	4.45e-03	2.10e-03	5.18e-04	0.00e+00	4.72e-03	5.70e-04	5.23e-04	4.29e-03	2.21e-03	7.89e-02		6.87e-01
9	813814	810241	142423	141674	7.42e-03	4.96e-02	4.52e-03	2.19e-03	1.24e-03	0.00e+00	4.61e-03	7.16e-04	4.85e-04	4.02e-03	2.20e-03	7.70e-02		7.65e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.3899 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.3899 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.389938  0.000000
];

