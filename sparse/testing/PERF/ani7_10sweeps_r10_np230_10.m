% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_230 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	828245	819385	141586	141586	1.04e-02	4.28e-02	9.06e-03	2.01e-02	1.04e-03	0.00e+00	2.37e-03	5.82e-04	3.48e-04	1.37e-02	1.82e-03	1.02e-01		1.02e-01
1	799137	814507	119973	128833	1.05e-02	3.48e-02	3.56e-03	4.46e-03	1.28e-03	0.00e+00	3.14e-03	4.74e-04	3.77e-04	8.22e-03	1.96e-03	6.88e-02		1.71e-01
2	795544	803992	149887	134517	1.04e-02	5.11e-02	3.85e-03	5.19e-03	3.06e-04	0.00e+00	3.28e-03	6.37e-04	3.96e-04	4.37e-03	2.08e-03	8.16e-02		2.53e-01
3	822624	809195	154286	145838	1.03e-02	5.12e-02	4.04e-03	3.60e-03	3.26e-04	0.00e+00	3.66e-03	5.00e-04	4.24e-04	4.32e-03	2.22e-03	8.05e-02		3.33e-01
4	814904	808965	128012	141441	1.03e-02	5.71e-02	5.25e-03	2.99e-03	3.56e-04	0.00e+00	3.77e-03	5.58e-04	4.26e-04	5.22e-03	2.26e-03	8.83e-02		4.21e-01
5	806618	808095	136537	142476	1.03e-02	5.02e-02	4.59e-03	1.87e-03	1.01e-03	0.00e+00	3.91e-03	5.48e-04	4.50e-04	4.69e-03	2.24e-03	7.98e-02		5.01e-01
6	812799	814527	145629	144152	1.03e-02	5.61e-02	4.60e-03	3.47e-03	4.45e-04	0.00e+00	3.89e-03	6.36e-04	4.45e-04	4.84e-03	2.30e-03	8.71e-02		5.88e-01
7	814820	810711	140254	138526	1.04e-02	5.07e-02	4.73e-03	2.43e-03	4.47e-04	0.00e+00	3.81e-03	7.84e-04	5.02e-04	4.30e-03	2.23e-03	8.03e-02		6.69e-01
8	811750	812218	139039	143148	1.03e-02	5.60e-02	4.64e-03	1.75e-03	8.90e-04	0.00e+00	3.93e-03	6.19e-04	4.14e-04	4.84e-03	2.26e-03	8.57e-02		7.54e-01
9	816413	816787	142915	142447	1.04e-02	5.04e-02	4.81e-03	3.00e-03	1.51e-03	0.00e+00	3.89e-03	6.71e-04	4.06e-04	4.36e-03	2.27e-03	8.17e-02		8.36e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.5892 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.5892 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.589238  0.000000
];

