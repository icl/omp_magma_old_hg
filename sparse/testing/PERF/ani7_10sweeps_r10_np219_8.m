% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_219 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	827892	802673	141586	141586	1.01e-02	4.25e-02	9.02e-03	1.91e-02	1.59e-03	0.00e+00	2.49e-03	6.73e-04	3.70e-04	1.22e-02	1.83e-03	9.98e-02		9.98e-02
1	801409	797007	120326	145545	1.08e-02	3.52e-02	3.58e-03	4.04e-03	2.01e-03	0.00e+00	3.08e-03	3.97e-04	4.02e-04	6.49e-03	2.03e-03	6.81e-02		1.68e-01
2	818722	806345	147615	152017	1.07e-02	5.24e-02	3.90e-03	3.26e-03	5.86e-04	0.00e+00	3.53e-03	4.29e-04	4.42e-04	4.23e-03	2.25e-03	8.17e-02		2.50e-01
3	794639	810188	131108	143485	1.08e-02	5.29e-02	4.77e-03	2.93e-03	3.20e-04	0.00e+00	3.80e-03	6.36e-04	4.71e-04	4.23e-03	2.20e-03	8.31e-02		3.33e-01
4	807932	813140	155997	140448	1.08e-02	5.44e-02	4.47e-03	3.02e-03	5.91e-04	0.00e+00	4.05e-03	6.70e-04	4.84e-04	4.24e-03	2.27e-03	8.50e-02		4.18e-01
5	799988	809573	143509	138301	1.07e-02	5.14e-02	4.53e-03	2.60e-03	4.29e-04	0.00e+00	4.06e-03	5.40e-04	4.43e-04	4.87e-03	2.02e-03	8.16e-02		4.99e-01
6	808511	807892	152259	142674	1.01e-02	5.43e-02	4.42e-03	5.75e-03	4.54e-04	0.00e+00	4.14e-03	7.01e-04	4.88e-04	4.34e-03	2.05e-03	8.67e-02		5.86e-01
7	805490	812547	144542	145161	1.01e-02	4.99e-02	4.57e-03	2.72e-03	4.71e-04	0.00e+00	4.08e-03	6.14e-04	4.46e-04	4.92e-03	2.05e-03	7.98e-02		6.66e-01
8	815327	811373	148369	141312	1.01e-02	5.54e-02	4.44e-03	4.39e-03	1.37e-03	0.00e+00	4.03e-03	6.02e-04	4.68e-04	4.25e-03	2.13e-03	8.71e-02		7.53e-01
9	815805	812055	139338	143292	1.01e-02	5.11e-02	4.62e-03	2.22e-03	2.52e-03	0.00e+00	4.11e-03	8.58e-04	4.33e-04	4.40e-03	2.10e-03	8.24e-02		8.35e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.5825 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.5825 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.582469  0.000000
];

