% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_171 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	815238	812299	141586	141586	8.28e-03	3.68e-02	7.48e-03	1.50e-02	5.30e-04	0.00e+00	2.25e-03	4.70e-04	3.64e-04	9.53e-03	1.92e-03	8.26e-02		8.26e-02
1	817164	809186	132980	135919	8.31e-03	3.09e-02	3.68e-03	5.32e-03	5.71e-04	0.00e+00	2.64e-03	4.08e-04	4.45e-04	6.71e-03	2.12e-03	6.11e-02		1.44e-01
2	798817	819611	131860	139838	8.22e-03	4.62e-02	4.09e-03	1.92e-03	3.22e-04	0.00e+00	3.10e-03	5.68e-04	4.18e-04	4.06e-03	2.19e-03	7.11e-02		2.15e-01
3	809542	807994	151013	130219	8.33e-03	4.72e-02	4.40e-03	1.75e-03	2.92e-04	0.00e+00	3.56e-03	4.79e-04	4.11e-04	3.91e-03	2.33e-03	7.27e-02		2.87e-01
4	803831	803421	141094	142642	8.20e-03	4.97e-02	4.69e-03	2.96e-03	4.40e-04	0.00e+00	3.75e-03	7.80e-04	4.48e-04	4.62e-03	2.34e-03	7.79e-02		3.65e-01
5	806453	810296	147610	148020	8.49e-03	4.47e-02	4.85e-03	1.79e-03	7.42e-04	0.00e+00	3.66e-03	6.18e-04	4.16e-04	4.36e-03	2.37e-03	7.19e-02		4.37e-01
6	812380	814817	145794	141951	8.69e-03	5.02e-02	4.89e-03	1.69e-03	4.30e-04	0.00e+00	3.82e-03	4.81e-04	4.45e-04	4.34e-03	2.38e-03	7.73e-02		5.15e-01
7	812050	812160	140673	138236	8.72e-03	4.72e-02	5.10e-03	2.04e-03	3.98e-04	0.00e+00	3.83e-03	5.83e-04	4.48e-04	4.02e-03	2.42e-03	7.47e-02		5.89e-01
8	812096	809515	141809	141699	8.69e-03	5.10e-02	4.97e-03	3.09e-03	1.29e-03	0.00e+00	3.72e-03	7.96e-04	4.13e-04	4.13e-03	2.41e-03	8.06e-02		6.70e-01
9	813305	813372	142569	145150	8.65e-03	5.03e-02	5.53e-03	3.65e-03	9.43e-04	0.00e+00	3.75e-03	4.82e-04	4.39e-04	4.06e-03	2.41e-03	8.02e-02		7.50e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.3918 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.3918 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.391782  0.000000
];

