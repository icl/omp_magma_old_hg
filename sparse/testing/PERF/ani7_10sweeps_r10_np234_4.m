% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_234 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	823425	807679	141586	141586	1.05e-02	4.32e-02	8.91e-03	2.06e-02	9.79e-04	0.00e+00	2.32e-03	7.17e-04	3.57e-04	1.24e-02	1.72e-03	1.02e-01		1.02e-01
1	804649	794583	124793	140539	1.05e-02	3.59e-02	3.42e-03	5.96e-03	1.26e-03	0.00e+00	2.90e-03	4.42e-04	3.75e-04	6.64e-03	1.93e-03	6.93e-02		1.71e-01
2	819855	820871	144375	154441	1.03e-02	5.53e-02	3.65e-03	1.79e-03	2.92e-04	0.00e+00	3.36e-03	4.25e-04	3.77e-04	4.37e-03	2.15e-03	8.20e-02		2.53e-01
3	805423	807916	129975	128959	1.06e-02	5.47e-02	4.98e-03	2.34e-03	3.53e-04	0.00e+00	3.62e-03	6.77e-04	4.65e-04	4.19e-03	2.14e-03	8.41e-02		3.37e-01
4	798168	805129	145213	142720	3.30e-01	1.03e-01	4.29e-03	2.72e-03	4.82e-04	0.00e+00	3.83e-03	7.29e-04	4.52e-04	5.51e-03	2.15e-03	4.53e-01		7.90e-01
5	805629	811847	153273	146312	1.04e-02	5.11e-02	4.32e-03	1.83e-03	1.68e-03	0.00e+00	3.84e-03	8.83e-04	4.36e-04	4.79e-03	2.17e-03	8.15e-02		8.72e-01
6	808099	809743	146618	140400	1.05e-02	5.76e-02	4.53e-03	1.41e-03	4.89e-04	0.00e+00	3.83e-03	7.65e-04	4.58e-04	4.95e-03	2.22e-03	8.68e-02		9.59e-01
7	811471	815067	144954	143310	1.05e-02	5.22e-02	4.52e-03	2.04e-03	4.50e-04	0.00e+00	3.82e-03	4.99e-04	4.71e-04	4.31e-03	2.25e-03	8.10e-02		1.04e+00
8	811359	813086	142388	138792	1.05e-02	5.83e-02	6.19e-03	2.04e-03	1.35e-03	0.00e+00	3.90e-03	9.31e-04	4.34e-04	4.88e-03	2.25e-03	9.08e-02		1.13e+00
9	813742	809150	143306	141579	1.07e-02	5.31e-02	4.65e-03	2.01e-03	1.50e-03	0.00e+00	3.85e-03	6.46e-04	4.16e-04	4.30e-03	2.27e-03	8.35e-02		1.21e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.9774 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.9774 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.977448  0.000000
];

