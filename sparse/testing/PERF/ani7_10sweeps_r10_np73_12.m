% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_73 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	797146	811420	141586	141586	5.09e-03	2.97e-02	6.40e-03	8.96e-03	1.10e-03	0.00e+00	3.59e-03	4.43e-04	5.53e-04	6.38e-03	2.64e-03	6.48e-02		6.48e-02
1	803319	817779	151072	136798	5.12e-03	2.90e-02	5.10e-03	3.17e-03	1.04e-03	0.00e+00	4.06e-03	3.04e-04	5.36e-04	4.20e-03	2.86e-03	5.54e-02		1.20e-01
2	826783	796874	145705	131245	5.09e-03	5.02e-02	5.62e-03	3.21e-03	7.38e-04	0.00e+00	5.03e-03	2.81e-04	5.25e-04	3.57e-03	3.00e-03	7.72e-02		1.97e-01
3	799925	802371	123047	152956	5.00e-03	6.34e-02	5.99e-03	2.12e-03	4.57e-04	0.00e+00	5.91e-03	4.19e-04	6.06e-04	3.45e-03	2.98e-03	9.04e-02		2.88e-01
4	807644	801747	150711	148265	5.04e-03	5.68e-02	6.29e-03	3.11e-03	8.40e-04	0.00e+00	6.26e-03	3.43e-04	6.50e-04	3.44e-03	3.04e-03	8.58e-02		3.74e-01
5	809318	813851	143797	149694	5.02e-03	5.21e-02	6.99e-03	2.26e-03	8.95e-04	0.00e+00	6.43e-03	3.34e-04	6.12e-04	3.85e-03	3.15e-03	8.17e-02		4.55e-01
6	806922	807040	142929	138396	5.09e-03	5.70e-02	6.76e-03	2.12e-03	7.15e-04	0.00e+00	6.41e-03	5.52e-04	6.45e-04	3.52e-03	3.04e-03	8.58e-02		5.41e-01
7	810791	808913	146131	146013	5.12e-03	5.48e-02	6.55e-03	2.11e-03	5.56e-04	0.00e+00	6.10e-03	4.55e-04	6.11e-04	3.41e-03	3.08e-03	8.28e-02		6.24e-01
8	813871	810154	143068	144946	5.07e-03	5.73e-02	6.70e-03	2.53e-03	5.79e-04	0.00e+00	6.18e-03	5.05e-04	6.23e-04	3.60e-03	3.13e-03	8.62e-02		7.10e-01
9	813121	816409	140794	144511	5.09e-03	5.64e-02	6.81e-03	1.90e-03	1.37e-03	0.00e+00	6.56e-03	4.76e-04	6.30e-04	3.43e-03	3.11e-03	8.58e-02		7.96e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.2993 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.2993 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.299270  0.000000
];

