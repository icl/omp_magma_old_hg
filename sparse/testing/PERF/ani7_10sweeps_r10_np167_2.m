% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_167 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	818495	804283	141586	141586	8.17e-03	3.70e-02	7.49e-03	1.52e-02	2.53e-03	0.00e+00	2.34e-03	6.07e-04	3.67e-04	1.07e-02	1.99e-03	8.64e-02		8.64e-02
1	803680	800199	129723	143935	8.12e-03	3.12e-02	3.73e-03	4.07e-03	2.30e-03	0.00e+00	2.70e-03	4.71e-04	4.12e-04	6.40e-03	2.12e-03	6.15e-02		1.48e-01
2	812350	811405	145344	148825	8.04e-03	4.58e-02	4.04e-03	3.43e-03	3.30e-04	0.00e+00	3.08e-03	4.00e-04	4.94e-04	3.93e-03	2.38e-03	7.20e-02		2.20e-01
3	809753	809610	137480	138425	8.18e-03	4.69e-02	4.55e-03	3.63e-03	3.60e-04	0.00e+00	3.55e-03	4.76e-04	4.22e-04	4.05e-03	2.34e-03	7.44e-02		2.94e-01
4	802112	806699	140883	141026	8.14e-03	4.98e-02	4.79e-03	1.85e-03	4.60e-04	0.00e+00	3.80e-03	4.79e-04	4.41e-04	4.21e-03	2.34e-03	7.63e-02		3.71e-01
5	810609	806476	149329	144742	8.10e-03	4.49e-02	4.92e-03	4.10e-03	3.82e-04	0.00e+00	3.84e-03	6.06e-04	4.39e-04	4.42e-03	2.44e-03	7.41e-02		4.45e-01
6	810907	811955	141638	145771	8.11e-03	4.98e-02	5.00e-03	2.19e-03	4.56e-04	0.00e+00	3.79e-03	7.83e-04	4.65e-04	4.30e-03	2.45e-03	7.73e-02		5.22e-01
7	810793	811679	142146	141098	8.18e-03	4.69e-02	5.10e-03	1.71e-03	5.20e-04	0.00e+00	3.81e-03	8.61e-04	4.51e-04	3.91e-03	2.42e-03	7.38e-02		5.96e-01
8	813655	812559	143066	142180	8.14e-03	5.12e-02	5.16e-03	3.02e-03	4.72e-04	0.00e+00	3.77e-03	6.97e-04	4.83e-04	4.08e-03	2.43e-03	7.94e-02		6.75e-01
9	814034	811301	141010	142106	8.15e-03	4.70e-02	5.21e-03	1.68e-03	9.48e-04	0.00e+00	3.75e-03	6.22e-04	4.39e-04	4.13e-03	2.42e-03	7.43e-02		7.50e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.3818 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.3818 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.381844  0.000000
];

