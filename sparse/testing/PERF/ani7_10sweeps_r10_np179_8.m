% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_179 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	821622	818148	141586	141586	8.46e-03	3.57e-02	7.44e-03	1.55e-02	7.66e-04	0.00e+00	2.22e-03	5.48e-04	3.45e-04	1.01e-02	1.90e-03	8.31e-02		8.31e-02
1	806376	824216	126596	130070	8.54e-03	3.17e-02	3.56e-03	4.42e-03	9.60e-04	0.00e+00	2.83e-03	3.39e-04	3.74e-04	5.89e-03	2.09e-03	6.07e-02		1.44e-01
2	809033	811246	142648	124808	8.62e-03	4.51e-02	4.01e-03	2.62e-03	3.09e-04	0.00e+00	3.05e-03	7.05e-04	3.83e-04	3.88e-03	2.25e-03	7.09e-02		2.15e-01
3	809726	810006	140797	138584	9.20e-03	4.49e-02	4.45e-03	2.73e-03	3.82e-04	0.00e+00	3.41e-03	4.77e-04	3.97e-04	3.84e-03	2.17e-03	7.19e-02		2.87e-01
4	819604	814196	140910	140630	8.46e-03	4.89e-02	4.49e-03	2.30e-03	3.67e-04	0.00e+00	3.60e-03	6.45e-04	4.36e-04	4.10e-03	2.29e-03	7.56e-02		3.62e-01
5	811693	809910	131837	137245	8.46e-03	4.64e-02	4.77e-03	2.35e-03	9.15e-04	0.00e+00	3.55e-03	6.62e-04	4.18e-04	4.37e-03	2.24e-03	7.42e-02		4.36e-01
6	811280	811196	140554	142337	8.42e-03	4.98e-02	4.70e-03	1.86e-03	4.07e-04	0.00e+00	3.56e-03	5.52e-04	4.11e-04	4.38e-03	2.31e-03	7.64e-02		5.13e-01
7	809817	811279	141773	141857	8.43e-03	4.59e-02	4.73e-03	1.99e-03	5.81e-04	0.00e+00	3.53e-03	5.35e-04	4.11e-04	4.15e-03	2.30e-03	7.26e-02		5.85e-01
8	812518	813367	144042	142580	8.44e-03	5.06e-02	5.32e-03	2.59e-03	1.24e-03	0.00e+00	3.55e-03	6.80e-04	4.02e-04	4.02e-03	2.56e-03	7.94e-02		6.65e-01
9	812160	813554	142147	141298	1.07e-02	4.76e-02	5.39e-03	1.52e-03	1.06e-03	0.00e+00	3.58e-03	7.52e-04	4.06e-04	3.93e-03	2.60e-03	7.75e-02		7.42e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.3883 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.3883 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.388269  0.000000
];

