% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_102 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	808360	808873	141586	141586	5.60e-03	2.94e-02	5.98e-03	9.84e-03	8.58e-04	0.00e+00	2.62e-03	4.25e-04	4.17e-04	6.68e-03	2.15e-03	6.40e-02		6.40e-02
1	812128	800660	139858	139345	5.63e-03	2.84e-02	3.82e-03	5.79e-03	1.77e-03	0.00e+00	3.13e-03	3.83e-04	4.63e-04	4.47e-03	2.38e-03	5.62e-02		1.20e-01
2	800486	809905	136896	148364	5.54e-03	4.52e-02	4.50e-03	2.06e-03	3.52e-04	0.00e+00	4.62e-03	3.15e-04	4.48e-04	3.19e-03	2.93e-03	6.91e-02		1.89e-01
3	800661	804992	149344	139925	5.75e-03	4.74e-02	5.09e-03	2.40e-03	6.02e-04	0.00e+00	4.97e-03	3.59e-04	5.13e-04	3.12e-03	2.56e-03	7.28e-02		2.62e-01
4	807834	815855	149975	145644	5.61e-03	4.60e-02	4.94e-03	2.10e-03	5.90e-04	0.00e+00	4.61e-03	3.93e-04	4.79e-04	3.59e-03	2.60e-03	7.09e-02		3.33e-01
5	811269	812218	143607	135586	5.67e-03	4.51e-02	5.25e-03	2.24e-03	4.92e-04	0.00e+00	4.56e-03	3.93e-04	5.14e-04	3.51e-03	2.65e-03	7.04e-02		4.03e-01
6	812935	812003	140978	140029	5.64e-03	4.87e-02	5.32e-03	1.93e-03	4.24e-04	0.00e+00	4.55e-03	3.26e-04	4.85e-04	3.37e-03	2.66e-03	7.34e-02		4.77e-01
7	810929	811423	140118	141050	5.61e-03	4.73e-02	5.34e-03	1.84e-03	4.09e-04	0.00e+00	4.42e-03	5.93e-04	5.02e-04	3.11e-03	2.64e-03	7.18e-02		5.49e-01
8	812437	811425	142930	142436	5.60e-03	4.92e-02	5.39e-03	2.31e-03	4.20e-04	0.00e+00	4.46e-03	5.54e-04	4.98e-04	3.40e-03	2.68e-03	7.46e-02		6.23e-01
9	809264	812078	142228	143240	5.63e-03	4.70e-02	5.38e-03	2.07e-03	1.23e-03	0.00e+00	4.34e-03	5.40e-04	5.05e-04	3.18e-03	2.69e-03	7.25e-02		6.96e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.1680 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.1680 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.168006  0.000000
];

