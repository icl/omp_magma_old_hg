% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_164 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	808389	803813	141586	141586	8.04e-03	3.63e-02	7.57e-03	1.50e-02	1.10e-03	0.00e+00	2.37e-03	5.26e-04	3.73e-04	9.73e-03	2.03e-03	8.30e-02		8.30e-02
1	811858	820318	139829	144405	8.00e-03	3.12e-02	3.78e-03	3.40e-03	1.22e-03	0.00e+00	2.66e-03	3.91e-04	3.93e-04	5.80e-03	2.18e-03	5.91e-02		1.42e-01
2	785294	807067	137166	128706	8.14e-03	4.58e-02	4.30e-03	2.61e-03	7.25e-04	0.00e+00	3.22e-03	6.49e-04	4.03e-04	3.89e-03	2.22e-03	7.20e-02		2.14e-01
3	814625	801197	164536	142763	8.00e-03	4.37e-02	4.38e-03	2.06e-03	3.47e-04	0.00e+00	3.68e-03	4.44e-04	4.36e-04	3.96e-03	2.40e-03	6.95e-02		2.84e-01
4	818413	810279	136011	149439	7.93e-03	4.92e-02	4.91e-03	2.28e-03	4.32e-04	0.00e+00	3.82e-03	5.60e-04	1.87e-03	4.05e-03	2.52e-03	7.76e-02		3.61e-01
5	808419	806428	133028	141162	8.03e-03	4.66e-02	5.16e-03	2.67e-03	4.28e-04	0.00e+00	3.79e-03	5.17e-04	4.29e-04	4.29e-03	2.41e-03	7.43e-02		4.36e-01
6	808760	813377	143828	145819	8.00e-03	4.97e-02	5.06e-03	2.07e-03	3.42e-04	0.00e+00	3.76e-03	7.47e-04	4.14e-04	3.99e-03	2.48e-03	7.66e-02		5.12e-01
7	811776	809343	144293	139676	8.07e-03	4.66e-02	5.17e-03	2.15e-03	3.57e-04	0.00e+00	3.86e-03	6.94e-04	4.20e-04	4.11e-03	2.47e-03	7.39e-02		5.86e-01
8	811865	810557	142083	144516	8.03e-03	5.16e-02	5.17e-03	2.07e-03	9.60e-04	0.00e+00	3.75e-03	4.97e-04	4.25e-04	3.91e-03	2.47e-03	7.88e-02		6.65e-01
9	813019	813759	142800	144108	8.06e-03	4.71e-02	5.17e-03	2.32e-03	1.22e-03	0.00e+00	3.90e-03	7.63e-04	4.54e-04	3.91e-03	2.45e-03	7.53e-02		7.40e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.3838 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.3838 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.383797  0.000000
];

