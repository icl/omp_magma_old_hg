% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_102 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	808108	808863	141586	141586	5.62e-03	2.87e-02	5.93e-03	9.87e-03	1.22e-03	0.00e+00	2.66e-03	3.96e-04	4.56e-04	6.13e-03	2.15e-03	6.32e-02		6.32e-02
1	809024	809949	140110	139355	5.64e-03	2.83e-02	3.81e-03	3.79e-03	1.08e-03	0.00e+00	3.05e-03	2.67e-04	4.97e-04	4.24e-03	2.38e-03	5.30e-02		1.16e-01
2	818706	802322	140000	139075	6.25e-03	4.53e-02	4.41e-03	2.19e-03	7.66e-04	0.00e+00	3.67e-03	3.24e-04	4.97e-04	3.14e-03	2.50e-03	6.90e-02		1.85e-01
3	806770	794684	131124	147508	5.52e-03	5.13e-02	4.75e-03	1.42e-03	4.09e-04	0.00e+00	4.37e-03	3.60e-04	5.13e-04	3.10e-03	2.51e-03	7.42e-02		2.59e-01
4	809293	814648	143866	155952	5.51e-03	4.56e-02	4.86e-03	2.89e-03	5.27e-04	0.00e+00	4.57e-03	3.38e-04	5.05e-04	3.10e-03	2.62e-03	7.05e-02		3.30e-01
5	816632	804670	142148	136793	5.62e-03	4.50e-02	5.23e-03	1.84e-03	5.18e-04	0.00e+00	4.49e-03	5.06e-04	4.83e-04	3.41e-03	2.64e-03	6.98e-02		4.00e-01
6	813653	809879	135615	147577	5.58e-03	4.90e-02	5.35e-03	1.55e-03	4.74e-04	0.00e+00	4.52e-03	4.40e-04	5.06e-04	3.28e-03	2.69e-03	7.33e-02		4.73e-01
7	811141	813958	139400	143174	5.58e-03	4.73e-02	5.31e-03	2.47e-03	4.26e-04	0.00e+00	4.43e-03	4.85e-04	4.78e-04	3.32e-03	2.65e-03	7.24e-02		5.45e-01
8	810756	813016	142718	139901	5.65e-03	4.96e-02	5.39e-03	1.62e-03	9.48e-04	0.00e+00	4.44e-03	4.90e-04	4.77e-04	3.41e-03	2.68e-03	7.47e-02		6.20e-01
9	810171	814348	143909	141649	5.64e-03	4.73e-02	5.37e-03	1.94e-03	7.89e-04	0.00e+00	4.46e-03	4.19e-04	5.06e-04	3.15e-03	2.65e-03	7.22e-02		6.92e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.1638 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.1638 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.163789  0.000000
];

