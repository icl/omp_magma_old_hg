% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_177 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	822370	801805	141586	141586	8.37e-03	3.69e-02	7.48e-03	1.69e-02	2.53e-03	0.00e+00	2.19e-03	4.61e-04	3.54e-04	1.09e-02	1.90e-03	8.80e-02		8.80e-02
1	812550	795585	125848	146413	8.34e-03	3.10e-02	3.52e-03	8.59e-03	2.60e-03	0.00e+00	2.65e-03	3.89e-04	4.23e-04	6.21e-03	2.05e-03	6.58e-02		1.54e-01
2	809919	795139	136474	153439	8.19e-03	4.50e-02	3.98e-03	3.96e-03	3.28e-04	0.00e+00	2.95e-03	4.05e-04	3.85e-04	3.96e-03	2.10e-03	7.13e-02		2.25e-01
3	812485	809187	139911	154691	8.23e-03	4.44e-02	4.20e-03	4.20e-03	3.25e-04	0.00e+00	3.42e-03	5.46e-04	4.11e-04	3.98e-03	2.14e-03	7.18e-02		2.97e-01
4	801318	808837	138151	141449	8.35e-03	4.93e-02	4.53e-03	2.03e-03	3.55e-04	0.00e+00	3.58e-03	7.60e-04	4.37e-04	4.12e-03	2.23e-03	7.57e-02		3.73e-01
5	806571	815684	150123	142604	8.39e-03	4.44e-02	4.66e-03	2.40e-03	4.27e-04	0.00e+00	3.49e-03	6.18e-04	4.19e-04	4.51e-03	2.38e-03	7.17e-02		4.44e-01
6	811319	812738	145676	136563	8.46e-03	5.09e-02	4.81e-03	1.92e-03	4.30e-04	0.00e+00	3.57e-03	5.55e-04	4.27e-04	4.32e-03	2.39e-03	7.78e-02		5.22e-01
7	808419	812229	141734	140315	8.43e-03	4.69e-02	4.88e-03	2.00e-03	3.78e-04	0.00e+00	3.56e-03	5.73e-04	4.13e-04	3.86e-03	2.37e-03	7.34e-02		5.96e-01
8	812063	812082	145440	141630	8.37e-03	5.02e-02	4.72e-03	1.90e-03	3.48e-04	0.00e+00	3.53e-03	8.57e-04	4.14e-04	4.04e-03	2.34e-03	7.67e-02		6.72e-01
9	814706	811796	142602	142583	8.39e-03	4.63e-02	4.78e-03	2.18e-03	1.17e-03	0.00e+00	3.60e-03	3.56e-04	4.49e-04	3.92e-03	2.29e-03	7.34e-02		7.46e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.3849 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.3849 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.384880  0.000000
];

