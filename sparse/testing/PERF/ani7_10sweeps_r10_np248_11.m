% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_248 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	807708	797004	141586	141586	1.09e-02	4.42e-02	9.18e-03	2.06e-02	1.73e-03	0.00e+00	2.21e-03	6.20e-04	3.42e-04	1.44e-02	1.66e-03	1.06e-01		1.06e-01
1	800055	802929	140510	151214	1.08e-02	3.51e-02	3.14e-03	3.16e-03	3.06e-03	0.00e+00	2.73e-03	6.43e-04	3.98e-04	7.53e-03	1.80e-03	6.84e-02		1.74e-01
2	798128	797697	148969	146095	1.08e-02	5.08e-02	3.60e-03	3.05e-03	3.20e-04	0.00e+00	3.07e-03	5.98e-04	3.82e-04	5.99e-03	1.87e-03	8.05e-02		2.55e-01
3	814528	815037	151702	152133	1.08e-02	4.93e-02	3.96e-03	2.84e-03	4.96e-04	0.00e+00	3.42e-03	5.36e-04	4.03e-04	4.30e-03	2.05e-03	7.81e-02		3.33e-01
4	808652	804513	136108	135599	1.10e-02	5.66e-02	5.06e-03	3.27e-03	4.91e-04	0.00e+00	3.60e-03	8.18e-04	4.84e-04	4.64e-03	2.04e-03	8.80e-02		4.21e-01
5	810008	808569	142789	146928	1.08e-02	4.82e-02	4.16e-03	3.17e-03	4.07e-04	0.00e+00	3.68e-03	4.02e-04	4.39e-04	5.09e-03	2.05e-03	7.84e-02		4.99e-01
6	811856	811531	142239	143678	1.09e-02	5.37e-02	4.23e-03	1.86e-03	4.07e-04	0.00e+00	3.69e-03	7.45e-04	3.97e-04	4.78e-03	2.11e-03	8.28e-02		5.82e-01
7	806529	813146	141197	141522	1.09e-02	4.98e-02	4.28e-03	1.83e-03	3.56e-04	0.00e+00	3.69e-03	6.53e-04	4.03e-04	4.19e-03	2.11e-03	7.82e-02		6.60e-01
8	812007	812003	147330	140713	1.10e-02	5.47e-02	4.31e-03	2.87e-03	4.02e-04	0.00e+00	3.50e-03	7.86e-04	4.05e-04	4.55e-03	2.10e-03	8.46e-02		7.45e-01
9	815753	814116	142658	142662	1.10e-02	4.98e-02	4.32e-03	3.16e-03	1.43e-03	0.00e+00	3.62e-03	7.15e-04	4.31e-04	4.18e-03	2.11e-03	8.07e-02		8.26e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.5800 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.5800 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.579962  0.000000
];

