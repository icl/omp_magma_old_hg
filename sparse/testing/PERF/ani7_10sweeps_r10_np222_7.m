% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_222 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	817949	812046	141586	141586	1.02e-02	4.18e-02	9.11e-03	1.95e-02	1.94e-03	0.00e+00	2.43e-03	5.39e-04	3.63e-04	1.18e-02	1.78e-03	9.94e-02		9.94e-02
1	778922	805451	130269	136172	1.02e-02	3.50e-02	3.55e-03	4.65e-03	2.07e-03	0.00e+00	3.18e-03	5.09e-04	3.42e-04	6.64e-03	1.95e-03	6.80e-02		1.67e-01
2	797213	811101	170102	143573	9.98e-03	5.26e-02	3.57e-03	1.91e-03	1.08e-03	0.00e+00	3.39e-03	5.61e-04	4.22e-04	4.15e-03	2.00e-03	7.97e-02		2.47e-01
3	802047	805053	152617	138729	1.01e-02	5.09e-02	3.95e-03	3.43e-03	4.79e-04	0.00e+00	3.72e-03	6.10e-04	4.38e-04	4.17e-03	1.99e-03	7.98e-02		3.27e-01
4	805504	809375	148589	145583	1.00e-02	5.43e-02	4.21e-03	2.82e-03	4.92e-04	0.00e+00	3.97e-03	6.76e-04	4.58e-04	4.23e-03	2.02e-03	8.32e-02		4.10e-01
5	815112	810385	145937	142066	1.01e-02	4.87e-02	4.41e-03	2.40e-03	4.22e-04	0.00e+00	4.01e-03	5.88e-04	4.32e-04	4.85e-03	2.07e-03	7.79e-02		4.88e-01
6	813213	810736	137135	141862	1.01e-02	5.64e-02	4.54e-03	2.02e-03	4.31e-04	0.00e+00	3.94e-03	6.83e-04	4.46e-04	4.34e-03	2.05e-03	8.49e-02		5.73e-01
7	813123	813079	139840	142317	1.01e-02	5.07e-02	4.57e-03	3.09e-03	3.98e-04	0.00e+00	4.02e-03	7.44e-04	4.24e-04	4.53e-03	2.06e-03	8.06e-02		6.54e-01
8	813480	809777	140736	140780	1.01e-02	5.60e-02	4.56e-03	3.10e-03	1.41e-03	0.00e+00	3.91e-03	6.38e-04	4.21e-04	4.42e-03	2.03e-03	8.65e-02		7.40e-01
9	813059	812578	141185	144888	1.01e-02	5.09e-02	4.55e-03	1.78e-03	1.58e-03	0.00e+00	3.94e-03	7.22e-04	4.51e-04	4.26e-03	2.06e-03	8.03e-02		8.20e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.5798 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.5798 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.579815  0.000000
];

