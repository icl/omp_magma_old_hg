% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_52 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	817758	803340	141586	141586	3.23e-03	2.91e-02	5.10e-03	6.63e-03	1.19e-03	0.00e+00	4.62e-03	2.65e-04	5.08e-04	6.00e-03	2.78e-03	5.94e-02		5.94e-02
1	812762	802948	130460	144878	3.21e-03	3.39e-02	4.06e-03	2.78e-03	1.09e-03	0.00e+00	5.82e-03	2.24e-04	5.69e-04	4.12e-03	3.62e-03	5.94e-02		1.19e-01
2	795104	786511	136262	146076	3.25e-03	5.45e-02	5.27e-03	1.50e-03	5.35e-04	0.00e+00	6.97e-03	2.80e-04	6.49e-04	3.43e-03	3.72e-03	8.01e-02		1.99e-01
3	810601	804530	154726	163319	3.87e-03	6.81e-02	5.67e-03	2.87e-03	6.63e-04	0.00e+00	7.82e-03	2.46e-04	7.19e-04	3.45e-03	3.76e-03	9.72e-02		2.96e-01
4	811589	817613	140035	146106	3.26e-03	7.15e-02	6.39e-03	1.84e-03	9.29e-04	0.00e+00	8.62e-03	2.68e-04	7.73e-04	3.69e-03	3.71e-03	1.01e-01		3.97e-01
5	809657	809593	139852	133828	3.33e-03	7.06e-02	6.30e-03	1.56e-03	8.54e-04	0.00e+00	8.76e-03	3.43e-04	7.66e-04	3.90e-03	3.59e-03	9.99e-02		4.97e-01
6	810468	810658	142590	142654	3.29e-03	7.01e-02	6.17e-03	1.48e-03	7.48e-04	0.00e+00	8.47e-03	3.22e-04	7.28e-04	3.66e-03	3.62e-03	9.86e-02		5.96e-01
7	811774	809335	142585	142395	3.29e-03	6.98e-02	6.21e-03	2.05e-03	8.09e-04	0.00e+00	8.64e-03	2.70e-04	7.56e-04	3.44e-03	3.60e-03	9.89e-02		6.95e-01
8	813087	814084	142085	144524	3.29e-03	7.09e-02	6.25e-03	1.74e-03	8.15e-04	0.00e+00	8.79e-03	3.94e-04	7.59e-04	3.82e-03	3.58e-03	1.00e-01		7.95e-01
9	813675	810753	141578	140581	3.33e-03	7.05e-02	6.31e-03	1.50e-03	1.24e-03	0.00e+00	8.56e-03	3.07e-04	7.53e-04	3.43e-03	3.64e-03	9.96e-02		8.94e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.2632 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.2632 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.263191  0.000000
];

