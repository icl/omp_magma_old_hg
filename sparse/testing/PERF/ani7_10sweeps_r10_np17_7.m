% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_17 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	818235	819696	141586	141586	3.81e-03	6.75e-02	1.37e-02	7.99e-03	1.57e-03	0.00e+00	1.47e-02	2.91e-04	1.51e-03	8.43e-03	8.73e-03	1.28e-01		1.28e-01
1	801941	818811	129983	128522	3.87e-03	8.47e-02	1.32e-02	3.57e-03	1.75e-03	0.00e+00	1.82e-02	2.75e-04	1.65e-03	7.34e-03	1.12e-02	1.46e-01		2.74e-01
2	809973	792368	147083	130213	3.93e-03	1.36e-01	1.70e-02	3.01e-03	1.63e-03	0.00e+00	2.22e-02	2.87e-04	1.84e-03	6.57e-03	1.18e-02	2.04e-01		4.78e-01
3	808532	803405	139857	157462	3.83e-03	1.85e-01	1.82e-02	3.44e-03	1.73e-03	0.00e+00	2.55e-02	2.69e-04	1.99e-03	6.51e-03	1.11e-02	2.57e-01		7.35e-01
4	804559	816619	142104	147231	3.85e-03	1.77e-01	1.94e-02	4.81e-03	2.71e-03	0.00e+00	2.87e-02	2.95e-04	2.22e-03	6.61e-03	1.10e-02	2.57e-01		9.92e-01
5	811400	806100	146882	134822	3.91e-03	1.93e-01	1.94e-02	3.64e-03	2.22e-03	0.00e+00	2.74e-02	3.33e-04	2.03e-03	7.14e-03	1.10e-02	2.70e-01		1.26e+00
6	812031	810526	140847	146147	3.87e-03	1.95e-01	1.94e-02	4.72e-03	2.28e-03	0.00e+00	2.83e-02	2.42e-04	2.09e-03	6.66e-03	1.11e-02	2.74e-01		1.54e+00
7	811323	808149	141022	142527	3.91e-03	1.99e-01	1.97e-02	3.67e-03	2.12e-03	0.00e+00	2.75e-02	3.41e-04	2.08e-03	6.62e-03	1.10e-02	2.76e-01		1.81e+00
8	811316	823918	142536	145710	3.85e-03	2.00e-01	1.97e-02	4.14e-03	2.17e-03	0.00e+00	2.76e-02	3.28e-04	2.05e-03	6.87e-03	1.12e-02	2.78e-01		2.09e+00
9	812594	816639	143349	130747	3.99e-03	2.07e-01	2.05e-02	3.55e-03	2.57e-03	0.00e+00	2.78e-02	2.90e-04	2.06e-03	6.66e-03	1.12e-02	2.85e-01		2.37e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 2.7872 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 2.7872 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  2.787230  0.000000
];

