% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_28 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	815557	815541	141586	141586	3.59e-03	4.56e-02	8.73e-03	7.23e-03	1.29e-03	0.00e+00	9.06e-03	3.05e-04	9.45e-04	6.96e-03	5.29e-03	8.90e-02		8.90e-02
1	808727	802989	132661	132677	3.64e-03	5.65e-02	8.01e-03	3.14e-03	1.38e-03	0.00e+00	1.13e-02	2.53e-04	1.07e-03	5.54e-03	6.79e-03	9.76e-02		1.87e-01
2	805398	811100	140297	146035	3.64e-03	8.76e-02	1.03e-02	2.57e-03	1.09e-03	0.00e+00	1.34e-02	3.19e-04	1.14e-03	5.01e-03	7.46e-03	1.33e-01		3.19e-01
3	808355	808534	144432	138730	3.65e-03	1.33e-01	1.19e-02	2.90e-03	1.08e-03	0.00e+00	1.55e-02	2.82e-04	1.26e-03	4.91e-03	7.06e-03	1.81e-01		5.00e-01
4	811585	819160	142281	142102	3.65e-03	1.24e-01	1.19e-02	2.82e-03	1.82e-03	0.00e+00	1.71e-02	4.03e-04	1.38e-03	4.90e-03	7.00e-03	1.75e-01		6.75e-01
5	810310	810909	139856	132281	3.70e-03	1.27e-01	1.23e-02	2.62e-03	1.47e-03	0.00e+00	1.67e-02	3.03e-04	1.37e-03	5.40e-03	6.95e-03	1.78e-01		8.53e-01
6	813668	811504	141937	141338	3.65e-03	1.26e-01	1.22e-02	2.82e-03	1.51e-03	0.00e+00	1.71e-02	3.91e-04	1.35e-03	5.07e-03	7.02e-03	1.77e-01		1.03e+00
7	813764	809254	139385	141549	3.67e-03	1.28e-01	1.23e-02	2.31e-03	1.47e-03	0.00e+00	1.67e-02	3.43e-04	1.31e-03	5.17e-03	6.95e-03	1.78e-01		1.21e+00
8	809891	813930	140095	144605	3.66e-03	1.29e-01	1.23e-02	2.52e-03	2.02e-03	0.00e+00	1.68e-02	4.62e-04	1.33e-03	5.03e-03	7.02e-03	1.80e-01		1.39e+00
9	813777	814850	144774	140735	3.68e-03	1.27e-01	1.23e-02	3.10e-03	1.88e-03	0.00e+00	1.67e-02	2.79e-04	1.36e-03	4.91e-03	7.01e-03	1.78e-01		1.57e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.9578 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.9578 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.957775  0.000000
];

