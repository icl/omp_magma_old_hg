% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_55 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	822035	802979	141586	141586	3.23e-03	2.84e-02	4.98e-03	6.81e-03	4.63e-04	0.00e+00	4.45e-03	2.95e-04	4.81e-04	5.59e-03	2.60e-03	5.73e-02		5.73e-02
1	798267	805240	126183	145239	3.25e-03	3.22e-02	3.87e-03	2.54e-03	5.86e-04	0.00e+00	5.33e-03	2.13e-04	5.71e-04	4.42e-03	3.45e-03	5.64e-02		1.14e-01
2	793025	812516	150757	143784	3.25e-03	5.30e-02	4.94e-03	1.63e-03	4.27e-04	0.00e+00	6.48e-03	2.16e-04	5.76e-04	3.41e-03	3.55e-03	7.75e-02		1.91e-01
3	816192	808212	156805	137314	3.30e-03	6.79e-02	5.50e-03	2.57e-03	6.08e-04	0.00e+00	7.45e-03	3.39e-04	7.05e-04	3.37e-03	3.57e-03	9.53e-02		2.87e-01
4	811748	821663	134444	142424	3.28e-03	7.00e-02	6.20e-03	1.81e-03	9.07e-04	0.00e+00	8.43e-03	2.52e-04	7.65e-04	3.80e-03	3.52e-03	9.90e-02		3.86e-01
5	814263	817138	139693	129778	3.35e-03	7.00e-02	6.02e-03	2.41e-03	1.00e-03	0.00e+00	8.20e-03	4.00e-04	7.14e-04	3.58e-03	3.48e-03	9.92e-02		4.85e-01
6	807442	808459	137984	135109	3.35e-03	7.15e-02	6.02e-03	1.68e-03	8.19e-04	0.00e+00	8.48e-03	3.03e-04	7.20e-04	3.58e-03	3.39e-03	9.99e-02		5.85e-01
7	811941	813098	145611	144594	3.30e-03	6.59e-02	5.88e-03	1.99e-03	7.34e-04	0.00e+00	7.90e-03	3.42e-04	7.16e-04	3.38e-03	3.46e-03	9.36e-02		6.78e-01
8	813163	812883	141918	140761	3.32e-03	6.85e-02	5.99e-03	1.86e-03	1.04e-03	0.00e+00	8.24e-03	2.71e-04	7.31e-04	3.51e-03	3.46e-03	9.69e-02		7.75e-01
9	806323	813686	141502	141782	3.33e-03	6.76e-02	6.01e-03	1.63e-03	9.68e-04	0.00e+00	7.96e-03	3.33e-04	6.77e-04	3.39e-03	3.41e-03	9.53e-02		8.70e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.2477 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.2477 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.247693  0.000000
];

