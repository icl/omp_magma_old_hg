% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_91 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	812335	812138	141586	141586	5.44e-03	3.01e-02	6.26e-03	9.60e-03	1.76e-03	0.00e+00	2.90e-03	3.69e-04	4.81e-04	7.04e-03	2.39e-03	6.64e-02		6.64e-02
1	790822	809407	135883	136080	5.52e-03	3.04e-02	4.31e-03	1.66e-03	1.51e-03	0.00e+00	3.45e-03	2.46e-04	5.33e-04	4.59e-03	2.56e-03	5.47e-02		1.21e-01
2	813402	819906	158202	139617	5.44e-03	4.69e-02	4.61e-03	1.57e-03	4.00e-04	0.00e+00	4.05e-03	5.01e-04	4.85e-04	3.25e-03	2.87e-03	7.01e-02		1.91e-01
3	812080	807189	136428	129924	5.54e-03	5.63e-02	5.22e-03	1.78e-03	3.84e-04	0.00e+00	4.69e-03	5.03e-04	5.31e-04	3.24e-03	2.87e-03	8.10e-02		2.72e-01
4	803583	805691	138556	143447	5.42e-03	5.81e-02	5.67e-03	1.29e-03	5.99e-04	0.00e+00	5.93e-03	4.45e-04	5.62e-04	3.49e-03	2.91e-03	8.44e-02		3.57e-01
5	804228	808555	147858	145750	5.43e-03	5.13e-02	5.60e-03	1.64e-03	5.31e-04	0.00e+00	5.85e-03	4.65e-04	5.39e-04	3.64e-03	2.91e-03	7.79e-02		4.35e-01
6	810929	810443	148019	143692	5.45e-03	5.46e-02	5.75e-03	1.60e-03	5.43e-04	0.00e+00	5.82e-03	5.36e-04	5.65e-04	3.53e-03	2.96e-03	8.13e-02		5.16e-01
7	813014	810319	142124	142610	5.49e-03	5.36e-02	5.82e-03	1.33e-03	6.41e-04	0.00e+00	5.01e-03	4.01e-04	5.40e-04	3.28e-03	2.93e-03	7.91e-02		5.95e-01
8	811049	818747	140845	143540	5.44e-03	5.32e-02	5.88e-03	1.62e-03	5.48e-04	0.00e+00	5.17e-03	6.07e-04	5.86e-04	3.43e-03	3.02e-03	7.95e-02		6.74e-01
9	815952	810683	143616	135918	5.54e-03	5.31e-02	6.01e-03	1.51e-03	1.12e-03	0.00e+00	5.10e-03	4.54e-04	5.99e-04	3.27e-03	2.97e-03	7.97e-02		7.54e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.2262 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.2262 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.226185  0.000000
];

