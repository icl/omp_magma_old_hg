% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_72 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	814138	808758	141586	141586	5.06e-03	2.93e-02	6.47e-03	8.82e-03	7.33e-04	0.00e+00	3.67e-03	3.81e-04	5.99e-04	6.04e-03	2.70e-03	6.38e-02		6.38e-02
1	811289	812178	134080	139460	5.07e-03	2.89e-02	5.16e-03	2.94e-03	9.24e-04	0.00e+00	4.11e-03	3.18e-04	5.39e-04	4.14e-03	2.79e-03	5.49e-02		1.19e-01
2	826320	804830	137735	136846	5.07e-03	4.95e-02	5.54e-03	1.58e-03	6.41e-04	0.00e+00	5.06e-03	3.75e-04	5.28e-04	3.81e-03	2.97e-03	7.50e-02		1.94e-01
3	795662	812217	123510	145000	5.05e-03	6.36e-02	5.97e-03	1.75e-03	5.25e-04	0.00e+00	5.78e-03	3.57e-04	6.12e-04	3.40e-03	2.83e-03	8.99e-02		2.84e-01
4	805203	802761	154974	138419	5.11e-03	5.76e-02	6.33e-03	1.72e-03	5.47e-04	0.00e+00	6.10e-03	6.10e-04	5.94e-04	3.37e-03	2.83e-03	8.48e-02		3.68e-01
5	809435	810655	146238	148680	5.04e-03	5.26e-02	6.53e-03	2.43e-03	6.94e-04	0.00e+00	6.25e-03	3.22e-04	6.07e-04	3.63e-03	2.91e-03	8.10e-02		4.49e-01
6	814862	809946	142812	141592	5.09e-03	5.61e-02	6.61e-03	4.01e-03	7.14e-04	0.00e+00	6.53e-03	4.59e-04	6.63e-04	3.45e-03	2.88e-03	8.65e-02		5.36e-01
7	809699	811523	138191	143107	5.08e-03	5.65e-02	6.70e-03	1.42e-03	6.26e-04	0.00e+00	6.20e-03	5.23e-04	6.34e-04	3.65e-03	2.83e-03	8.41e-02		6.20e-01
8	811022	814949	144160	142336	5.09e-03	5.75e-02	6.66e-03	1.70e-03	1.16e-03	0.00e+00	6.27e-03	5.33e-04	6.15e-04	3.45e-03	2.85e-03	8.58e-02		7.06e-01
9	812868	815878	143643	139716	5.11e-03	5.72e-02	6.70e-03	1.49e-03	1.22e-03	0.00e+00	6.41e-03	4.98e-04	6.63e-04	3.45e-03	3.46e-03	8.62e-02		7.92e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.2704 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.2704 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.270412  0.000000
];

