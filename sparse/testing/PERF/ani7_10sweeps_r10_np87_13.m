% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_87 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	811211	807706	141586	141586	5.35e-03	3.02e-02	6.44e-03	9.69e-03	5.56e-04	0.00e+00	3.08e-03	4.11e-04	4.87e-04	6.99e-03	2.40e-03	6.56e-02		6.56e-02
1	820764	812902	137007	140512	5.36e-03	2.76e-02	4.47e-03	3.52e-03	1.80e-03	0.00e+00	3.59e-03	3.41e-04	4.88e-04	4.52e-03	2.73e-03	5.44e-02		1.20e-01
2	800328	806257	128260	136122	5.38e-03	4.74e-02	5.09e-03	3.22e-03	4.14e-04	0.00e+00	4.32e-03	4.50e-04	5.15e-04	3.36e-03	2.93e-03	7.31e-02		1.93e-01
3	817974	816811	149502	143573	5.39e-03	5.14e-02	5.34e-03	2.61e-03	4.41e-04	0.00e+00	4.76e-03	4.30e-04	5.18e-04	3.31e-03	3.14e-03	7.73e-02		2.70e-01
4	814283	822484	132662	133825	5.41e-03	5.47e-02	5.87e-03	1.45e-03	5.21e-04	0.00e+00	5.16e-03	4.88e-04	5.48e-04	3.58e-03	3.11e-03	8.08e-02		3.51e-01
5	807555	813191	137158	128957	5.44e-03	5.35e-02	6.15e-03	2.38e-03	6.36e-04	0.00e+00	5.14e-03	5.45e-04	5.85e-04	3.59e-03	3.07e-03	8.10e-02		4.32e-01
6	815711	814804	144692	139056	5.39e-03	5.22e-02	6.11e-03	1.55e-03	6.43e-04	0.00e+00	5.33e-03	3.55e-04	5.68e-04	3.53e-03	3.14e-03	7.88e-02		5.11e-01
7	815372	813473	137342	138249	5.39e-03	5.31e-02	6.17e-03	1.67e-03	4.95e-04	0.00e+00	5.24e-03	4.32e-04	5.45e-04	3.26e-03	3.14e-03	7.94e-02		5.91e-01
8	814553	812744	138487	140386	5.39e-03	5.53e-02	6.26e-03	1.65e-03	8.31e-04	0.00e+00	5.32e-03	4.67e-04	5.66e-04	3.60e-03	3.11e-03	8.25e-02		6.73e-01
9	813211	811925	140112	141921	6.40e-03	5.37e-02	6.22e-03	1.64e-03	9.85e-04	0.00e+00	4.98e-03	4.74e-04	5.46e-04	3.39e-03	3.07e-03	8.14e-02		7.54e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.2336 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.2336 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.233642  0.000000
];

