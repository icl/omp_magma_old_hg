% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_213 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	826551	802938	141586	141586	9.88e-03	4.14e-02	8.45e-03	1.84e-02	1.45e-03	0.00e+00	2.68e-03	4.94e-04	4.05e-04	1.23e-02	1.93e-03	9.74e-02		9.74e-02
1	815598	809771	121667	145280	9.78e-03	3.57e-02	3.64e-03	3.02e-03	1.84e-03	0.00e+00	3.11e-03	5.37e-04	3.56e-04	6.18e-03	2.14e-03	6.63e-02		1.64e-01
2	816862	812061	133426	139253	9.82e-03	5.31e-02	3.97e-03	3.22e-03	6.51e-04	0.00e+00	3.62e-03	5.83e-04	4.61e-04	4.75e-03	2.15e-03	8.23e-02		2.46e-01
3	812784	817951	132968	137769	9.84e-03	5.49e-02	4.72e-03	2.48e-03	3.49e-04	0.00e+00	3.96e-03	8.50e-04	4.80e-04	4.27e-03	2.18e-03	8.41e-02		3.30e-01
4	810436	807519	137852	132685	9.92e-03	5.91e-02	4.49e-03	2.02e-03	4.07e-04	0.00e+00	4.12e-03	7.52e-04	4.58e-04	4.40e-03	2.08e-03	8.77e-02		4.18e-01
5	811512	810952	141005	143922	9.79e-03	5.04e-02	4.64e-03	3.26e-03	5.41e-04	0.00e+00	4.18e-03	4.82e-04	4.89e-04	4.51e-03	2.09e-03	8.04e-02		4.98e-01
6	807767	812480	140735	141295	9.81e-03	5.51e-02	4.70e-03	1.97e-03	5.07e-04	0.00e+00	4.13e-03	6.80e-04	4.89e-04	4.33e-03	2.11e-03	8.38e-02		5.82e-01
7	810608	811442	145286	140573	9.86e-03	5.15e-02	4.58e-03	3.37e-03	4.08e-04	0.00e+00	4.11e-03	8.26e-04	4.77e-04	4.59e-03	2.12e-03	8.18e-02		6.64e-01
8	813031	814734	143251	142417	9.93e-03	5.55e-02	4.64e-03	2.61e-03	1.46e-03	0.00e+00	4.06e-03	3.75e-04	4.37e-04	4.32e-03	2.18e-03	8.55e-02		7.49e-01
9	812097	812703	141634	139931	9.90e-03	5.21e-02	4.73e-03	2.43e-03	1.44e-03	0.00e+00	4.14e-03	6.75e-04	4.46e-04	4.19e-03	2.14e-03	8.22e-02		8.32e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.5811 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.5811 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.581066  0.000000
];

