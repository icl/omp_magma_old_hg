% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_145 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	807588	821541	141586	141586	7.55e-03	3.35e-02	7.68e-03	1.43e-02	1.21e-03	0.00e+00	2.67e-03	4.33e-04	4.02e-04	8.72e-03	2.20e-03	7.87e-02		7.87e-02
1	807894	807245	140630	126677	7.68e-03	3.03e-02	4.17e-03	3.21e-03	1.34e-03	0.00e+00	3.07e-03	4.62e-04	3.88e-04	5.37e-03	2.25e-03	5.83e-02		1.37e-01
2	802014	809498	141130	141779	7.49e-03	4.65e-02	4.45e-03	2.34e-03	7.62e-04	0.00e+00	3.54e-03	6.05e-04	4.16e-04	3.91e-03	2.24e-03	7.23e-02		2.09e-01
3	811033	810044	147816	140332	8.62e-03	5.17e-02	5.14e-03	3.33e-03	4.22e-04	0.00e+00	4.12e-03	5.25e-04	5.41e-04	4.03e-03	2.57e-03	8.10e-02		2.90e-01
4	801557	812505	139603	140592	8.62e-03	5.38e-02	5.28e-03	2.02e-03	5.77e-04	0.00e+00	4.45e-03	4.37e-04	5.21e-04	4.09e-03	2.57e-03	8.23e-02		3.73e-01
5	807990	809362	149884	138936	8.58e-03	4.81e-02	5.25e-03	2.78e-03	5.03e-04	0.00e+00	4.40e-03	6.94e-04	5.53e-04	4.70e-03	2.59e-03	7.81e-02		4.51e-01
6	810445	809284	144257	142885	8.59e-03	5.18e-02	5.34e-03	2.71e-03	4.66e-04	0.00e+00	4.32e-03	4.13e-04	5.17e-04	4.11e-03	2.61e-03	8.09e-02		5.32e-01
7	810907	809518	142608	143769	8.62e-03	4.76e-02	5.34e-03	1.77e-03	4.42e-04	0.00e+00	4.32e-03	8.45e-04	4.88e-04	4.20e-03	2.40e-03	7.60e-02		6.08e-01
8	810455	812922	142952	144341	7.55e-03	5.03e-02	5.26e-03	1.62e-03	1.34e-03	0.00e+00	4.47e-03	4.12e-04	4.87e-04	4.01e-03	2.39e-03	7.79e-02		6.85e-01
9	813056	813178	144210	141743	7.62e-03	4.78e-02	5.38e-03	2.19e-03	1.21e-03	0.00e+00	4.37e-03	4.88e-04	4.85e-04	3.93e-03	2.43e-03	7.59e-02		7.61e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.3949 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.3949 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.394903  0.000000
];

