% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_109 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	813190	814561	141586	141586	5.71e-03	2.83e-02	5.83e-03	1.03e-02	5.45e-04	0.00e+00	2.51e-03	3.39e-04	4.27e-04	7.16e-03	2.07e-03	6.31e-02		6.31e-02
1	810212	823312	135028	133657	5.77e-03	2.76e-02	3.63e-03	2.43e-03	6.05e-04	0.00e+00	2.96e-03	3.32e-04	4.58e-04	4.38e-03	2.43e-03	5.06e-02		1.14e-01
2	813233	801541	138812	125712	5.81e-03	4.24e-02	4.31e-03	2.07e-03	3.56e-04	0.00e+00	3.59e-03	4.78e-04	4.36e-04	3.12e-03	2.61e-03	6.52e-02		1.79e-01
3	803298	812433	136597	148289	5.67e-03	4.45e-02	4.63e-03	2.29e-03	4.69e-04	0.00e+00	4.07e-03	3.90e-04	4.33e-04	3.04e-03	2.51e-03	6.80e-02		2.47e-01
4	804908	814004	147338	138203	5.75e-03	4.44e-02	4.70e-03	3.05e-03	4.86e-04	0.00e+00	4.31e-03	3.75e-04	4.38e-04	3.31e-03	2.46e-03	6.93e-02		3.16e-01
5	811219	813252	146533	137437	5.76e-03	4.23e-02	4.88e-03	1.96e-03	9.79e-04	0.00e+00	4.23e-03	3.66e-04	4.41e-04	3.42e-03	2.54e-03	6.69e-02		3.83e-01
6	810197	812741	141028	138995	5.75e-03	4.61e-02	4.97e-03	2.29e-03	4.32e-04	0.00e+00	4.14e-03	6.14e-04	4.42e-04	3.31e-03	2.52e-03	7.05e-02		4.54e-01
7	811446	813053	142856	140312	5.74e-03	4.43e-02	5.04e-03	4.34e-03	5.36e-04	0.00e+00	4.19e-03	5.56e-04	4.48e-04	3.27e-03	2.53e-03	7.10e-02		5.24e-01
8	810451	810004	142413	140806	5.76e-03	4.68e-02	5.07e-03	1.55e-03	8.64e-04	0.00e+00	4.20e-03	6.94e-04	4.45e-04	3.19e-03	2.51e-03	7.11e-02		5.96e-01
9	813269	811096	144214	144661	5.75e-03	4.41e-02	5.04e-03	1.57e-03	1.09e-03	0.00e+00	4.23e-03	3.61e-04	4.61e-04	3.13e-03	2.50e-03	6.82e-02		6.64e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.1424 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.1424 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.142408  0.000000
];

