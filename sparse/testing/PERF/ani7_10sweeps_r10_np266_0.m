% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_266 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	814763	838843	141586	141586	1.15e-02	4.55e-02	9.43e-03	2.22e-02	2.43e-03	0.00e+00	2.57e-03	6.62e-04	3.16e-04	1.29e-02	1.63e-03	1.09e-01		1.09e-01
1	830081	811009	133455	109375	1.20e-02	3.85e-02	3.14e-03	3.80e-03	2.63e-03	0.00e+00	3.23e-03	6.22e-04	3.39e-04	7.41e-03	1.76e-03	7.34e-02		1.83e-01
2	813173	814570	118943	138015	1.15e-02	5.15e-02	3.97e-03	2.10e-03	1.51e-03	0.00e+00	3.53e-03	5.49e-04	4.30e-04	4.24e-03	2.06e-03	8.14e-02		2.64e-01
3	821256	817336	136657	135260	1.16e-02	5.34e-02	4.10e-03	4.10e-03	3.69e-04	0.00e+00	3.68e-03	5.25e-04	4.00e-04	4.36e-03	2.13e-03	8.46e-02		3.49e-01
4	801405	812163	129380	133300	1.16e-02	5.94e-02	4.33e-03	2.03e-03	4.00e-04	0.00e+00	3.91e-03	7.85e-04	4.58e-04	4.33e-03	1.99e-03	8.93e-02		4.38e-01
5	809416	809948	150036	139278	1.16e-02	5.12e-02	4.07e-03	2.38e-03	4.36e-04	0.00e+00	5.23e-03	6.52e-04	4.13e-04	4.96e-03	2.03e-03	8.29e-02		5.21e-01
6	808682	810002	142831	142299	1.15e-02	5.73e-02	4.17e-03	1.89e-03	6.89e-04	0.00e+00	4.01e-03	7.14e-04	4.29e-04	4.40e-03	2.03e-03	8.72e-02		6.08e-01
7	816157	813137	144371	143051	1.15e-02	5.33e-02	4.17e-03	1.80e-03	4.40e-04	0.00e+00	4.00e-03	9.63e-04	4.24e-04	4.97e-03	2.09e-03	8.37e-02		6.92e-01
8	813929	811406	137702	140722	1.16e-02	5.91e-02	4.30e-03	1.96e-03	1.73e-03	0.00e+00	4.03e-03	6.58e-04	4.21e-04	4.59e-03	2.09e-03	9.04e-02		7.82e-01
9	814011	815068	140736	143259	1.16e-02	5.39e-02	4.28e-03	2.59e-03	1.36e-03	0.00e+00	3.95e-03	6.32e-04	4.53e-04	4.23e-03	2.09e-03	8.51e-02		8.67e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.5962 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.5962 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.596239  0.000000
];

