% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_143 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	813833	816840	141586	141586	7.49e-03	3.37e-02	6.73e-03	1.37e-02	5.13e-04	0.00e+00	2.73e-03	4.28e-04	4.13e-04	8.59e-03	2.20e-03	7.65e-02		7.65e-02
1	812488	801733	134385	131378	7.59e-03	3.00e-02	4.22e-03	4.57e-03	5.85e-04	0.00e+00	3.17e-03	4.59e-04	3.91e-04	6.52e-03	2.24e-03	5.98e-02		1.36e-01
2	819609	815442	136536	147291	7.41e-03	4.62e-02	4.44e-03	2.80e-03	2.89e-04	0.00e+00	3.65e-03	3.61e-04	4.45e-04	4.06e-03	2.38e-03	7.21e-02		2.08e-01
3	795311	808672	130221	134388	7.54e-03	5.42e-02	4.89e-03	1.96e-03	3.93e-04	0.00e+00	4.23e-03	7.56e-04	5.18e-04	3.97e-03	2.29e-03	8.08e-02		2.89e-01
4	801531	805869	155325	141964	7.48e-03	4.87e-02	5.08e-03	1.54e-03	4.21e-04	0.00e+00	4.84e-03	5.12e-04	5.02e-04	4.55e-03	2.80e-03	7.65e-02		3.66e-01
5	812440	808143	149910	145572	8.89e-03	5.34e-02	5.67e-03	2.48e-03	1.28e-03	0.00e+00	5.50e-03	5.40e-04	1.99e-03	4.34e-03	2.82e-03	8.70e-02		4.53e-01
6	812562	802761	139807	144104	8.96e-03	5.84e-02	5.60e-03	2.13e-03	4.57e-04	0.00e+00	5.37e-03	8.45e-04	4.84e-04	4.45e-03	2.82e-03	8.95e-02		5.42e-01
7	811131	812156	140491	150292	8.82e-03	5.61e-02	5.77e-03	1.81e-03	4.93e-04	0.00e+00	5.31e-03	5.03e-04	5.02e-04	4.06e-03	2.86e-03	8.62e-02		6.28e-01
8	811872	815239	142728	141703	8.99e-03	5.92e-02	5.62e-03	1.46e-03	8.99e-04	0.00e+00	5.05e-03	5.85e-04	4.79e-04	4.26e-03	2.83e-03	8.94e-02		7.18e-01
9	814903	812982	142793	139426	8.16e-03	4.83e-02	5.50e-03	1.51e-03	1.20e-03	0.00e+00	4.32e-03	8.08e-04	5.01e-04	3.99e-03	2.36e-03	7.67e-02		7.94e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.4025 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.4025 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.402522  0.000000
];

