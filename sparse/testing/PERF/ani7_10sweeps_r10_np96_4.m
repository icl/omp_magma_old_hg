% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_96 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	815191	805270	141586	141586	5.52e-03	3.00e-02	6.10e-03	9.85e-03	7.23e-04	0.00e+00	2.76e-03	3.97e-04	4.44e-04	6.67e-03	2.31e-03	6.48e-02		6.48e-02
1	799278	807954	133027	142948	5.52e-03	2.85e-02	4.04e-03	3.18e-03	9.51e-04	0.00e+00	3.19e-03	2.41e-04	4.75e-04	4.52e-03	2.42e-03	5.30e-02		1.18e-01
2	797079	810492	149746	141070	5.50e-03	4.58e-02	4.51e-03	2.40e-03	3.56e-04	0.00e+00	3.78e-03	4.81e-04	4.92e-04	3.22e-03	2.66e-03	6.92e-02		1.87e-01
3	806261	823145	152751	139338	5.51e-03	4.75e-02	4.87e-03	1.93e-03	4.43e-04	0.00e+00	4.68e-03	3.53e-04	5.36e-04	3.20e-03	2.83e-03	7.19e-02		2.59e-01
4	814430	800512	144375	127491	6.40e-03	5.35e-02	5.38e-03	1.28e-03	6.36e-04	0.00e+00	4.79e-03	5.87e-04	5.46e-04	3.53e-03	2.79e-03	7.95e-02		3.38e-01
5	810866	806088	137011	150929	5.46e-03	4.73e-02	5.45e-03	1.91e-03	5.23e-04	0.00e+00	4.76e-03	3.23e-04	5.06e-04	3.53e-03	2.74e-03	7.25e-02		4.11e-01
6	808998	812650	141381	146159	5.50e-03	5.06e-02	5.53e-03	1.83e-03	5.96e-04	0.00e+00	4.81e-03	5.38e-04	5.43e-04	3.43e-03	2.81e-03	7.61e-02		4.87e-01
7	810633	811938	144055	140403	5.55e-03	4.88e-02	5.62e-03	1.71e-03	4.70e-04	0.00e+00	4.87e-03	5.89e-04	5.20e-04	3.17e-03	2.82e-03	7.41e-02		5.61e-01
8	811188	812986	143226	141921	5.58e-03	5.17e-02	5.59e-03	1.75e-03	6.47e-04	0.00e+00	4.83e-03	5.42e-04	5.17e-04	3.33e-03	2.85e-03	7.74e-02		6.38e-01
9	811317	812339	143477	141679	5.54e-03	4.96e-02	5.66e-03	1.61e-03	9.95e-04	0.00e+00	4.90e-03	4.71e-04	5.21e-04	3.21e-03	2.79e-03	7.53e-02		7.14e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.1866 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.1866 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.186570  0.000000
];

