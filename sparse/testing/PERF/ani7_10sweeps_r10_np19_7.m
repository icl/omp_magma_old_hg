% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_19 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	809739	809606	141586	141586	3.74e-03	6.14e-02	1.23e-02	7.71e-03	9.87e-04	0.00e+00	1.33e-02	3.11e-04	1.34e-03	7.68e-03	7.70e-03	1.16e-01		1.16e-01
1	807825	808398	138479	138612	3.73e-03	7.60e-02	1.17e-02	3.25e-03	1.36e-03	0.00e+00	1.62e-02	2.21e-04	1.55e-03	6.92e-03	1.00e-02	1.31e-01		2.47e-01
2	802143	819266	141199	140626	3.81e-03	1.23e-01	1.52e-02	3.75e-03	1.18e-03	0.00e+00	1.97e-02	2.81e-04	1.63e-03	6.08e-03	1.09e-02	1.85e-01		4.33e-01
3	801296	810110	147687	130564	3.86e-03	1.73e-01	1.73e-02	3.93e-03	1.48e-03	0.00e+00	2.27e-02	2.69e-04	1.80e-03	6.05e-03	1.00e-02	2.41e-01		6.73e-01
4	801212	813748	149340	140526	3.83e-03	1.57e-01	1.71e-02	3.15e-03	1.93e-03	0.00e+00	2.48e-02	3.45e-04	2.33e-03	6.33e-03	9.86e-03	2.26e-01		9.00e-01
5	807341	815224	150229	137693	3.84e-03	1.63e-01	1.74e-02	5.13e-03	2.67e-03	0.00e+00	2.60e-02	3.97e-04	1.89e-03	6.67e-03	1.01e-02	2.37e-01		1.14e+00
6	815699	808903	144906	137023	3.83e-03	1.72e-01	1.77e-02	3.35e-03	1.88e-03	0.00e+00	2.49e-02	4.60e-04	1.84e-03	6.20e-03	1.01e-02	2.43e-01		1.38e+00
7	813203	811259	137354	144150	3.80e-03	1.79e-01	1.80e-02	3.19e-03	1.74e-03	0.00e+00	2.44e-02	4.22e-04	1.76e-03	6.39e-03	1.01e-02	2.49e-01		1.63e+00
8	813213	814224	140656	142600	3.83e-03	1.75e-01	1.80e-02	3.15e-03	2.38e-03	0.00e+00	2.52e-02	3.67e-04	1.82e-03	6.24e-03	1.02e-02	2.47e-01		1.87e+00
9	814267	809561	141452	140441	3.87e-03	1.76e-01	1.81e-02	3.95e-03	2.01e-03	0.00e+00	2.46e-02	3.94e-04	1.80e-03	6.09e-03	1.01e-02	2.47e-01		2.12e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 2.5279 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 2.5279 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  2.527939  0.000000
];

