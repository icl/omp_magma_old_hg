% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_265 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	823368	838488	141586	141586	1.16e-02	4.48e-02	9.40e-03	2.16e-02	6.61e-04	0.00e+00	2.64e-03	4.83e-04	3.19e-04	1.32e-02	1.61e-03	1.06e-01		1.06e-01
1	810958	794413	124850	109730	1.20e-02	3.81e-02	3.20e-03	4.21e-03	6.23e-04	0.00e+00	3.43e-03	4.60e-04	4.07e-04	8.64e-03	1.79e-03	7.29e-02		1.79e-01
2	806373	785861	138066	154611	1.14e-02	5.07e-02	3.53e-03	2.20e-03	2.78e-04	0.00e+00	3.45e-03	5.77e-04	4.28e-04	4.22e-03	1.98e-03	7.87e-02		2.58e-01
3	805723	825047	143457	163969	1.12e-02	5.06e-02	3.94e-03	2.76e-03	3.14e-04	0.00e+00	3.66e-03	4.55e-04	3.86e-04	4.32e-03	2.12e-03	7.98e-02		3.38e-01
4	806647	805110	144913	125589	1.18e-02	5.81e-02	4.89e-03	3.23e-03	3.69e-04	0.00e+00	4.06e-03	6.46e-04	4.78e-04	5.71e-03	2.00e-03	9.12e-02		4.29e-01
5	810907	812009	144794	146331	1.15e-02	5.07e-02	4.10e-03	2.19e-03	1.13e-03	0.00e+00	4.17e-03	5.60e-04	4.23e-04	4.84e-03	2.02e-03	8.16e-02		5.10e-01
6	811751	811513	141340	140238	1.16e-02	5.69e-02	4.18e-03	2.20e-03	3.80e-04	0.00e+00	3.87e-03	7.20e-04	4.30e-04	4.98e-03	2.08e-03	8.73e-02		5.98e-01
7	810650	812647	141302	141540	1.16e-02	5.35e-02	4.25e-03	2.02e-03	4.02e-04	0.00e+00	4.27e-03	5.47e-04	4.80e-04	4.25e-03	2.07e-03	8.34e-02		6.81e-01
8	813870	811339	143209	141212	1.16e-02	5.80e-02	4.28e-03	1.72e-03	1.09e-03	0.00e+00	4.03e-03	8.81e-04	4.28e-04	4.79e-03	2.07e-03	8.89e-02		7.70e-01
9	813491	812555	140795	143326	1.16e-02	5.52e-02	4.27e-03	1.54e-03	1.47e-03	0.00e+00	4.11e-03	8.85e-04	4.59e-04	4.18e-03	2.11e-03	8.58e-02		8.56e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.6013 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.6013 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.601295  0.000000
];

