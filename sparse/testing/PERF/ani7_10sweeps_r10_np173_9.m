% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_173 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	819999	805705	141586	141586	8.30e-03	3.67e-02	7.43e-03	1.52e-02	1.11e-03	0.00e+00	2.28e-03	4.23e-04	3.59e-04	1.05e-02	1.94e-03	8.43e-02		8.43e-02
1	821853	794525	128219	142513	8.29e-03	3.24e-02	3.61e-03	4.47e-03	2.50e-03	0.00e+00	2.57e-03	3.96e-04	4.27e-04	6.19e-03	2.08e-03	6.29e-02		1.47e-01
2	799330	806539	127171	154499	8.11e-03	4.57e-02	4.01e-03	2.39e-03	3.24e-04	0.00e+00	3.11e-03	5.26e-04	4.19e-04	3.98e-03	2.17e-03	7.08e-02		2.18e-01
3	816982	794417	150500	143291	8.23e-03	4.43e-02	4.25e-03	3.16e-03	2.98e-04	0.00e+00	3.46e-03	4.74e-04	4.28e-04	5.46e-03	2.27e-03	7.24e-02		2.90e-01
4	809549	818237	133654	156219	8.14e-03	5.02e-02	4.58e-03	1.92e-03	5.11e-04	0.00e+00	3.70e-03	5.64e-04	4.49e-04	4.37e-03	2.39e-03	7.68e-02		3.67e-01
5	809208	800684	141892	133204	8.82e-03	4.48e-02	4.92e-03	2.39e-03	4.52e-04	0.00e+00	3.76e-03	6.16e-04	4.47e-04	4.37e-03	2.29e-03	7.28e-02		4.40e-01
6	808589	809416	143039	151563	8.12e-03	4.84e-02	4.83e-03	1.69e-03	4.11e-04	0.00e+00	3.70e-03	7.53e-04	4.42e-04	4.26e-03	2.34e-03	7.49e-02		5.15e-01
7	812841	815683	144464	143637	8.27e-03	4.50e-02	4.86e-03	1.73e-03	4.29e-04	0.00e+00	3.63e-03	5.31e-04	4.06e-04	3.92e-03	2.38e-03	7.12e-02		5.86e-01
8	810846	816209	141018	138176	8.32e-03	5.03e-02	4.96e-03	1.66e-03	3.30e-04	0.00e+00	3.62e-03	6.56e-04	4.02e-04	4.17e-03	2.38e-03	7.68e-02		6.63e-01
9	811181	808171	143819	138456	8.35e-03	4.65e-02	4.96e-03	1.74e-03	1.28e-03	0.00e+00	3.78e-03	6.21e-04	4.19e-04	3.94e-03	2.39e-03	7.40e-02		7.37e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.3798 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.3798 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.379825  0.000000
];

