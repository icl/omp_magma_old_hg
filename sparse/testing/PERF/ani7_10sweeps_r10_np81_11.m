% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_81 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	810371	803945	141586	141586	5.24e-03	3.01e-02	6.69e-03	9.51e-03	9.98e-04	0.00e+00	3.27e-03	2.91e-04	5.51e-04	6.89e-03	2.48e-03	6.61e-02		6.61e-02
1	810960	790550	137847	144273	5.23e-03	2.76e-02	4.70e-03	2.72e-03	6.13e-04	0.00e+00	3.89e-03	3.03e-04	5.16e-04	4.04e-03	2.62e-03	5.22e-02		1.18e-01
2	803177	811867	138064	158474	5.12e-03	4.82e-02	5.04e-03	2.72e-03	4.66e-04	0.00e+00	4.60e-03	2.75e-04	5.11e-04	3.67e-03	2.73e-03	7.33e-02		1.92e-01
3	809633	808241	146653	137963	5.26e-03	5.78e-02	5.43e-03	1.84e-03	5.75e-04	0.00e+00	5.26e-03	3.31e-04	5.48e-04	3.30e-03	2.80e-03	8.32e-02		2.75e-01
4	809887	800603	141003	142395	5.25e-03	5.58e-02	5.82e-03	1.72e-03	5.54e-04	0.00e+00	5.44e-03	5.12e-04	5.55e-04	3.37e-03	2.80e-03	8.19e-02		3.57e-01
5	809045	808312	141554	150838	5.22e-03	5.14e-02	5.89e-03	1.66e-03	6.29e-04	0.00e+00	5.65e-03	4.64e-04	5.86e-04	3.66e-03	2.87e-03	7.80e-02		4.35e-01
6	812966	811586	143202	143935	5.26e-03	5.39e-02	5.99e-03	1.45e-03	5.94e-04	0.00e+00	5.61e-03	4.98e-04	5.51e-04	3.30e-03	2.86e-03	8.00e-02		5.15e-01
7	813558	807550	140087	141467	5.23e-03	5.30e-02	6.12e-03	2.99e-03	5.31e-04	0.00e+00	5.62e-03	4.16e-04	5.83e-04	3.65e-03	2.88e-03	8.10e-02		5.96e-01
8	813399	811330	140301	146309	5.21e-03	5.46e-02	6.17e-03	1.64e-03	1.03e-03	0.00e+00	5.51e-03	5.18e-04	5.79e-04	3.47e-03	2.90e-03	8.16e-02		6.77e-01
9	810181	813167	141266	143335	5.29e-03	5.30e-02	6.20e-03	1.70e-03	1.11e-03	0.00e+00	5.89e-03	3.24e-04	6.02e-04	3.27e-03	2.90e-03	8.03e-02		7.58e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.2272 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.2272 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.227212  0.000000
];

