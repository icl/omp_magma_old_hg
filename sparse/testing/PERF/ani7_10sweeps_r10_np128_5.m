% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_128 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	814043	815082	141586	141586	5.95e-03	2.74e-02	5.78e-03	1.08e-02	6.42e-04	0.00e+00	2.81e-03	4.00e-04	3.48e-04	7.05e-03	1.80e-03	6.30e-02		6.30e-02
1	805017	793873	134175	133136	6.00e-03	2.78e-02	3.11e-03	2.38e-03	7.23e-04	0.00e+00	3.48e-03	3.01e-04	3.83e-04	4.37e-03	2.10e-03	5.06e-02		1.14e-01
2	817339	812389	144007	155151	5.87e-03	3.68e-02	3.69e-03	2.18e-03	3.23e-04	0.00e+00	3.84e-03	4.32e-04	3.92e-04	2.95e-03	2.41e-03	5.89e-02		1.73e-01
3	802438	817983	132491	137441	5.98e-03	4.65e-02	4.29e-03	2.74e-03	3.63e-04	0.00e+00	4.69e-03	3.37e-04	4.45e-04	2.94e-03	2.28e-03	7.06e-02		2.43e-01
4	808543	806437	148198	132653	6.02e-03	4.63e-02	4.34e-03	1.76e-03	5.29e-04	0.00e+00	4.74e-03	4.08e-04	4.47e-04	3.13e-03	2.25e-03	7.00e-02		3.13e-01
5	808279	811756	142898	145004	5.97e-03	4.40e-02	4.36e-03	2.38e-03	8.92e-04	0.00e+00	4.70e-03	5.05e-04	4.57e-04	3.39e-03	2.26e-03	6.89e-02		3.82e-01
6	809469	809254	143968	140491	5.98e-03	4.74e-02	4.39e-03	2.17e-03	4.31e-04	0.00e+00	4.89e-03	4.74e-04	4.77e-04	3.29e-03	2.27e-03	7.18e-02		4.54e-01
7	811565	811990	143584	143799	5.97e-03	4.60e-02	4.44e-03	1.66e-03	3.55e-04	0.00e+00	4.64e-03	5.22e-04	4.13e-04	3.16e-03	2.29e-03	6.95e-02		5.23e-01
8	811009	811378	142294	141869	5.99e-03	4.84e-02	4.46e-03	1.81e-03	8.40e-04	0.00e+00	4.50e-03	4.60e-04	4.23e-04	3.10e-03	2.30e-03	7.23e-02		5.96e-01
9	811794	812757	143656	143287	5.99e-03	4.69e-02	4.50e-03	2.06e-03	7.93e-04	0.00e+00	4.67e-03	5.12e-04	4.23e-04	3.00e-03	2.34e-03	7.12e-02		6.67e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.1480 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.1480 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.147953  0.000000
];

