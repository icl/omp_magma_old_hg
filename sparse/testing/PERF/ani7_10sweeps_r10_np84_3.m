% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_84 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	811889	813909	141586	141586	5.30e-03	3.01e-02	6.56e-03	9.63e-03	1.17e-03	0.00e+00	3.15e-03	3.34e-04	5.23e-04	6.18e-03	2.51e-03	6.55e-02		6.55e-02
1	804439	806660	136329	134309	5.34e-03	2.86e-02	4.56e-03	3.28e-03	1.03e-03	0.00e+00	3.85e-03	3.04e-04	4.48e-04	4.12e-03	2.54e-03	5.41e-02		1.20e-01
2	809531	795246	144585	142364	5.26e-03	4.72e-02	4.94e-03	1.85e-03	7.14e-04	0.00e+00	4.30e-03	3.87e-04	4.97e-04	3.31e-03	2.70e-03	7.12e-02		1.91e-01
3	818690	801163	140299	154584	5.21e-03	5.34e-02	5.48e-03	1.72e-03	4.16e-04	0.00e+00	5.02e-03	3.47e-04	5.18e-04	3.25e-03	2.74e-03	7.81e-02		2.69e-01
4	801849	808294	131946	149473	5.22e-03	5.94e-02	5.91e-03	1.75e-03	7.59e-04	0.00e+00	5.46e-03	3.90e-04	6.04e-04	3.23e-03	2.77e-03	8.55e-02		3.54e-01
5	812216	810630	149592	143147	5.26e-03	4.82e-02	6.06e-03	2.06e-03	6.19e-04	0.00e+00	5.39e-03	3.91e-04	5.30e-04	3.69e-03	2.86e-03	7.50e-02		4.29e-01
6	809788	811084	140031	141617	5.29e-03	5.20e-02	6.35e-03	1.47e-03	5.82e-04	0.00e+00	5.44e-03	5.37e-04	5.49e-04	3.40e-03	2.76e-03	7.84e-02		5.08e-01
7	811821	812469	143265	141969	5.32e-03	5.04e-02	6.34e-03	2.17e-03	5.24e-04	0.00e+00	5.42e-03	4.35e-04	5.64e-04	3.55e-03	2.81e-03	7.75e-02		5.85e-01
8	813144	814557	142038	141390	5.29e-03	5.35e-02	6.37e-03	2.10e-03	9.69e-04	0.00e+00	5.35e-03	4.35e-04	5.56e-04	3.45e-03	2.84e-03	8.09e-02		6.66e-01
9	818927	812215	141521	140108	5.30e-03	5.20e-02	6.53e-03	1.96e-03	9.44e-04	0.00e+00	5.36e-03	3.41e-04	5.30e-04	3.31e-03	2.85e-03	7.91e-02		7.45e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.2228 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.2228 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.222783  0.000000
];

