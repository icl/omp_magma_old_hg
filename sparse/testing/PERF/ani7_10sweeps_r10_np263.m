% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_263 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	821530	804238	141586	141586	1.21e-02	7.44e-02	1.79e-02	2.68e-02	2.09e-03	0.00e+00	2.74e-03	5.02e-04	3.79e-04	3.28e-02	1.62e-03	1.71e-01		1.71e-01
1	808533	806654	126688	143980	1.20e-02	4.78e-02	3.08e-03	2.67e-03	3.26e-03	0.00e+00	3.15e-03	5.31e-04	3.56e-04	1.47e-02	2.09e-03	8.97e-02		2.61e-01
2	800701	811189	140491	142370	1.20e-02	8.72e-02	1.17e-02	2.82e-03	5.64e-04	0.00e+00	3.55e-03	5.54e-04	4.38e-04	9.13e-03	1.50e-02	1.43e-01		4.04e-01
3	797953	797255	149129	138641	1.21e-02	6.22e-02	4.20e-03	4.47e-03	5.24e-04	0.00e+00	3.79e-03	5.27e-04	4.97e-04	4.43e-03	2.04e-03	9.48e-02		4.99e-01
4	814088	805135	152683	153381	1.20e-02	7.25e-02	4.14e-03	2.89e-03	6.64e-04	0.00e+00	4.12e-03	5.88e-04	5.15e-04	5.14e-03	2.19e-03	1.05e-01		6.03e-01
5	808822	806259	137353	146306	1.21e-02	7.15e-02	4.24e-03	3.38e-03	7.65e-04	0.00e+00	4.18e-03	7.50e-04	4.54e-04	6.08e-03	2.12e-03	1.06e-01		7.09e-01
6	810601	813463	143425	145988	1.23e-02	7.69e-02	4.26e-03	3.94e-03	6.20e-04	0.00e+00	4.19e-03	5.32e-04	4.84e-04	5.18e-03	2.10e-03	1.11e-01		8.20e-01
7	811643	814384	142452	139590	1.23e-02	6.66e-02	4.35e-03	2.78e-03	6.03e-04	0.00e+00	4.02e-03	8.42e-04	4.35e-04	4.58e-03	2.15e-03	9.87e-02		9.18e-01
8	813549	806614	142216	139475	1.23e-02	7.90e-02	4.37e-03	3.11e-03	8.40e-04	0.00e+00	4.14e-03	5.12e-04	4.79e-04	4.90e-03	2.12e-03	1.12e-01		1.03e+00
9	812784	816016	141116	148051	1.21e-02	6.69e-02	4.33e-03	1.96e-03	1.65e-03	0.00e+00	4.19e-03	7.71e-04	4.39e-04	4.46e-03	2.15e-03	9.89e-02		1.13e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.9889 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.9889 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.988914  0.000000
];

