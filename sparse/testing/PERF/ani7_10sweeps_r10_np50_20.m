% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_50 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	818745	797102	141586	141586	3.24e-03	2.99e-02	5.25e-03	6.80e-03	3.99e-04	0.00e+00	4.80e-03	2.95e-04	5.20e-04	6.14e-03	2.82e-03	6.01e-02		6.01e-02
1	792227	804362	129473	151116	3.20e-03	3.40e-02	4.21e-03	2.87e-03	5.57e-04	0.00e+00	5.78e-03	2.15e-04	6.42e-04	4.29e-03	3.71e-03	5.95e-02		1.20e-01
2	810010	813879	156797	144662	3.25e-03	5.51e-02	5.37e-03	1.91e-03	5.07e-04	0.00e+00	7.13e-03	2.07e-04	6.35e-04	3.54e-03	4.09e-03	8.17e-02		2.01e-01
3	807182	798771	139820	135951	3.31e-03	7.73e-02	6.42e-03	1.64e-03	6.78e-04	0.00e+00	8.39e-03	3.51e-04	7.24e-04	3.51e-03	3.73e-03	1.06e-01		3.07e-01
4	801292	814991	143454	151865	3.27e-03	7.25e-02	6.29e-03	1.65e-03	8.06e-04	0.00e+00	8.76e-03	3.77e-04	7.66e-04	3.92e-03	3.72e-03	1.02e-01		4.09e-01
5	809524	815736	150149	136450	3.33e-03	7.00e-02	6.36e-03	1.80e-03	1.05e-03	0.00e+00	9.06e-03	3.33e-04	7.39e-04	3.75e-03	3.75e-03	1.00e-01		5.10e-01
6	812135	811145	142723	136511	3.32e-03	7.24e-02	6.45e-03	1.66e-03	8.50e-04	0.00e+00	9.06e-03	4.21e-04	7.75e-04	3.62e-03	3.74e-03	1.02e-01		6.12e-01
7	813656	810927	140918	141908	3.30e-03	7.25e-02	6.50e-03	2.34e-03	8.42e-04	0.00e+00	9.05e-03	2.24e-04	7.69e-04	3.57e-03	3.75e-03	1.03e-01		7.15e-01
8	811445	811707	140203	142932	3.33e-03	7.41e-02	6.49e-03	1.94e-03	1.03e-03	0.00e+00	9.07e-03	3.81e-04	7.67e-04	3.66e-03	3.73e-03	1.05e-01		8.19e-01
9	813037	811809	143220	142958	3.30e-03	7.33e-02	6.52e-03	2.08e-03	1.08e-03	0.00e+00	8.82e-03	3.02e-04	7.46e-04	3.51e-03	3.73e-03	1.03e-01		9.23e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.2944 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.2944 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.294380  0.000000
];

