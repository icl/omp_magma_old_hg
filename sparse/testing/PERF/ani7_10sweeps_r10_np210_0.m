% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_210 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	805029	804025	141586	141586	9.61e-03	4.09e-02	8.43e-03	1.83e-02	9.91e-04	0.00e+00	2.54e-03	6.12e-04	4.40e-04	1.26e-02	1.88e-03	9.64e-02		9.64e-02
1	819913	804142	143189	144193	9.58e-03	3.55e-02	3.62e-03	3.38e-03	2.89e-03	0.00e+00	3.14e-03	4.95e-04	3.60e-04	7.15e-03	2.16e-03	6.82e-02		1.65e-01
2	797218	795493	129111	144882	9.52e-03	5.47e-02	3.99e-03	4.13e-03	3.08e-04	0.00e+00	3.67e-03	5.57e-04	4.35e-04	4.36e-03	2.10e-03	8.38e-02		2.48e-01
3	802716	813026	152612	154337	9.45e-03	5.13e-02	3.97e-03	3.19e-03	4.35e-04	0.00e+00	3.97e-03	5.07e-04	4.78e-04	4.35e-03	2.14e-03	7.98e-02		3.28e-01
4	807909	802144	147920	137610	9.63e-03	5.64e-02	5.37e-03	3.09e-03	5.75e-04	0.00e+00	4.18e-03	7.80e-04	4.83e-04	4.78e-03	2.06e-03	8.73e-02		4.16e-01
5	809533	808419	143532	149297	9.54e-03	5.11e-02	4.49e-03	3.75e-03	5.18e-04	0.00e+00	4.20e-03	4.08e-04	4.51e-04	5.23e-03	2.08e-03	8.18e-02		4.97e-01
6	811276	808459	142714	143828	9.58e-03	5.53e-02	4.53e-03	2.78e-03	6.24e-04	0.00e+00	4.25e-03	5.81e-04	4.56e-04	4.69e-03	2.11e-03	8.49e-02		5.82e-01
7	811199	812854	141777	144594	9.62e-03	5.66e-02	4.58e-03	1.72e-03	4.67e-04	0.00e+00	4.19e-03	6.15e-04	4.36e-04	4.22e-03	2.14e-03	8.46e-02		6.67e-01
8	811281	808981	142660	141005	9.62e-03	5.64e-02	4.60e-03	3.25e-03	5.90e-04	0.00e+00	4.28e-03	7.77e-04	4.70e-04	4.45e-03	2.12e-03	8.65e-02		7.53e-01
9	813704	814385	143384	145684	9.64e-03	5.18e-02	4.54e-03	3.57e-03	1.29e-03	0.00e+00	4.32e-03	6.74e-04	4.67e-04	4.30e-03	2.13e-03	8.28e-02		8.36e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.5899 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.5899 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.589921  0.000000
];

