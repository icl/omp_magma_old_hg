% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_218 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	818661	809342	141586	141586	9.98e-03	4.23e-02	9.01e-03	1.88e-02	6.02e-04	0.00e+00	2.51e-03	4.58e-04	4.04e-04	1.17e-02	1.81e-03	9.75e-02		9.75e-02
1	799938	804099	129557	138876	9.99e-03	3.48e-02	3.59e-03	3.83e-03	7.43e-04	0.00e+00	3.10e-03	4.73e-04	3.47e-04	8.08e-03	2.00e-03	6.70e-02		1.64e-01
2	811206	803612	149086	144925	9.82e-03	5.26e-02	3.79e-03	3.65e-03	3.49e-04	0.00e+00	3.44e-03	5.15e-04	3.98e-04	4.29e-03	2.05e-03	8.09e-02		2.45e-01
3	810714	799997	138624	146218	9.86e-03	5.27e-02	4.37e-03	2.15e-03	3.13e-04	0.00e+00	3.80e-03	7.00e-04	4.65e-04	4.17e-03	2.04e-03	8.05e-02		3.26e-01
4	816697	793618	139922	150639	9.83e-03	5.68e-02	4.26e-03	2.23e-03	4.36e-04	0.00e+00	3.98e-03	8.41e-04	4.41e-04	5.33e-03	2.00e-03	8.61e-02		4.12e-01
5	810971	807127	134744	157823	9.74e-03	4.85e-02	4.40e-03	3.45e-03	1.17e-03	0.00e+00	4.09e-03	5.41e-04	4.74e-04	4.73e-03	2.06e-03	7.92e-02		4.91e-01
6	812314	810180	141276	145120	9.91e-03	5.54e-02	4.54e-03	2.62e-03	4.62e-04	0.00e+00	4.04e-03	9.26e-04	4.38e-04	4.65e-03	2.08e-03	8.51e-02		5.76e-01
7	808965	810266	140739	142873	9.93e-03	5.08e-02	4.54e-03	2.36e-03	4.01e-04	0.00e+00	4.05e-03	6.43e-04	4.30e-04	4.27e-03	2.10e-03	7.95e-02		6.56e-01
8	811128	812763	144894	143593	9.91e-03	5.56e-02	4.44e-03	1.78e-03	1.01e-03	0.00e+00	3.96e-03	7.71e-04	4.76e-04	4.29e-03	2.09e-03	8.43e-02		7.40e-01
9	808545	813870	143537	141902	9.97e-03	5.09e-02	4.55e-03	2.89e-03	1.45e-03	0.00e+00	4.11e-03	5.63e-04	4.39e-04	4.22e-03	2.11e-03	8.12e-02		8.21e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.5780 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.5780 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.578005  0.000000
];

