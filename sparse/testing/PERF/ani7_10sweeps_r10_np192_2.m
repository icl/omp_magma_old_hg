% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_192 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	808152	806314	141586	141586	8.67e-03	3.65e-02	7.61e-03	1.61e-02	6.83e-04	0.00e+00	2.67e-03	5.00e-04	3.39e-04	9.72e-03	1.75e-03	8.46e-02		8.46e-02
1	803985	804362	140066	141904	8.63e-03	3.24e-02	3.24e-03	2.25e-03	6.35e-04	0.00e+00	2.93e-03	4.51e-04	3.83e-04	6.04e-03	1.97e-03	5.89e-02		1.43e-01
2	819832	798006	145039	144662	8.62e-03	4.52e-02	3.76e-03	3.97e-03	3.46e-04	0.00e+00	3.64e-03	5.17e-04	4.14e-04	3.92e-03	2.16e-03	7.25e-02		2.16e-01
3	802927	801084	129998	151824	8.59e-03	5.13e-02	5.15e-03	2.31e-03	3.38e-04	0.00e+00	3.63e-03	6.08e-04	4.72e-04	3.97e-03	2.48e-03	7.88e-02		2.95e-01
4	817056	814041	147709	149552	1.09e-02	5.14e-02	5.23e-03	2.96e-03	4.07e-04	0.00e+00	4.32e-03	7.52e-04	4.76e-04	4.81e-03	2.63e-03	8.39e-02		3.79e-01
5	804864	810378	134385	137400	1.10e-02	5.16e-02	5.46e-03	1.57e-03	1.33e-03	0.00e+00	3.90e-03	4.72e-04	4.46e-04	4.47e-03	2.63e-03	8.28e-02		4.62e-01
6	809328	806402	147383	141869	1.10e-02	5.37e-02	5.44e-03	2.14e-03	4.23e-04	0.00e+00	4.48e-03	5.67e-04	4.87e-04	4.35e-03	2.64e-03	8.52e-02		5.47e-01
7	815065	809683	143725	146651	1.09e-02	5.14e-02	5.46e-03	3.64e-03	4.83e-04	0.00e+00	4.41e-03	5.56e-04	4.47e-04	4.13e-03	2.68e-03	8.41e-02		6.31e-01
8	810678	814761	138794	144176	1.09e-02	5.84e-02	4.59e-03	3.10e-03	1.33e-03	0.00e+00	4.47e-03	6.44e-04	4.72e-04	3.95e-03	2.22e-03	9.01e-02		7.21e-01
9	812909	813262	143987	139904	8.77e-03	5.19e-02	4.55e-03	3.94e-03	1.06e-03	0.00e+00	4.53e-03	5.19e-04	4.79e-04	3.92e-03	2.25e-03	8.19e-02		8.03e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.4167 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.4167 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.416669  0.000000
];

