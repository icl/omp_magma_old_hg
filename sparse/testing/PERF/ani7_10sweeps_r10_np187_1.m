% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_187 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	824028	818806	141586	141586	8.64e-03	3.68e-02	7.49e-03	1.60e-02	9.36e-04	0.00e+00	2.09e-03	4.84e-04	3.84e-04	1.11e-02	1.85e-03	8.58e-02		8.58e-02
1	802636	820476	124190	129412	8.71e-03	3.29e-02	3.44e-03	3.69e-03	1.65e-03	0.00e+00	2.53e-03	3.55e-04	3.76e-04	6.79e-03	2.05e-03	6.24e-02		1.48e-01
2	800546	819703	146388	128548	8.73e-03	4.51e-02	3.90e-03	1.19e-03	2.80e-04	0.00e+00	2.85e-03	6.24e-04	4.04e-04	4.00e-03	2.24e-03	6.93e-02		2.18e-01
3	828575	796638	149284	130127	8.71e-03	4.93e-02	4.34e-03	1.69e-03	3.72e-04	0.00e+00	3.25e-03	4.56e-04	4.39e-04	3.95e-03	2.19e-03	7.47e-02		2.92e-01
4	802367	814619	122061	153998	8.47e-03	5.09e-02	4.59e-03	2.62e-03	4.25e-04	0.00e+00	3.33e-03	5.21e-04	4.76e-04	4.34e-03	2.18e-03	7.79e-02		3.70e-01
5	814230	807621	149074	136822	8.74e-03	4.57e-02	4.53e-03	1.85e-03	6.26e-04	0.00e+00	3.40e-03	5.98e-04	4.88e-04	4.51e-03	2.26e-03	7.27e-02		4.43e-01
6	812868	806628	138017	144626	8.60e-03	4.95e-02	4.65e-03	1.92e-03	4.59e-04	0.00e+00	3.37e-03	7.74e-04	4.43e-04	4.52e-03	2.23e-03	7.65e-02		5.19e-01
7	809879	808472	140185	146425	8.59e-03	4.65e-02	4.60e-03	1.67e-03	4.71e-04	0.00e+00	3.43e-03	6.19e-04	4.43e-04	3.97e-03	2.19e-03	7.25e-02		5.92e-01
8	814220	819931	143980	145387	8.58e-03	4.92e-02	4.63e-03	2.46e-03	7.48e-04	0.00e+00	3.52e-03	5.69e-04	4.68e-04	4.22e-03	2.26e-03	7.67e-02		6.68e-01
9	811863	816173	140445	134734	8.76e-03	4.86e-02	4.76e-03	2.49e-03	1.14e-03	0.00e+00	3.48e-03	6.09e-04	4.69e-04	3.93e-03	2.27e-03	7.65e-02		7.45e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.3900 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.3900 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.390032  0.000000
];

