% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_207 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	815188	815038	141586	141586	1.12e-02	4.03e-02	8.37e-03	1.81e-02	1.39e-03	0.00e+00	2.92e-03	5.39e-04	3.74e-04	1.28e-02	1.97e-03	9.80e-02		9.80e-02
1	799740	820562	133030	133180	1.15e-02	3.63e-02	3.78e-03	6.28e-03	6.32e-04	0.00e+00	3.58e-03	4.37e-04	3.71e-04	6.32e-03	2.45e-03	7.16e-02		1.70e-01
2	805745	803990	149284	128462	1.15e-02	5.61e-02	4.55e-03	1.91e-03	1.14e-03	0.00e+00	4.07e-03	7.00e-04	4.60e-04	4.88e-03	2.56e-03	8.79e-02		2.57e-01
3	807499	814528	144085	145840	1.13e-02	5.79e-02	4.85e-03	2.73e-03	3.76e-04	0.00e+00	4.47e-03	4.46e-04	5.38e-04	4.29e-03	2.52e-03	8.94e-02		3.47e-01
4	814878	809472	143137	136108	1.14e-02	6.11e-02	4.94e-03	1.98e-03	7.08e-04	0.00e+00	4.57e-03	7.22e-04	4.88e-04	4.30e-03	2.12e-03	9.23e-02		4.39e-01
5	804167	812886	136563	141969	9.59e-03	5.03e-02	4.25e-03	4.13e-03	5.08e-04	0.00e+00	4.18e-03	5.44e-04	5.10e-04	4.70e-03	2.10e-03	8.08e-02		5.20e-01
6	806418	805835	148080	139361	9.64e-03	5.54e-02	4.21e-03	3.53e-03	4.55e-04	0.00e+00	4.25e-03	6.57e-04	5.03e-04	4.31e-03	2.08e-03	8.50e-02		6.05e-01
7	808111	807921	146635	147218	9.57e-03	5.10e-02	4.23e-03	2.97e-03	4.30e-04	0.00e+00	4.20e-03	5.60e-04	4.64e-04	4.90e-03	2.18e-03	8.05e-02		6.86e-01
8	812458	809924	145748	145938	9.56e-03	5.56e-02	4.24e-03	2.68e-03	1.45e-03	0.00e+00	4.19e-03	6.00e-04	4.61e-04	4.20e-03	2.11e-03	8.51e-02		7.71e-01
9	814711	814680	142207	144741	9.60e-03	5.23e-02	4.30e-03	2.05e-03	1.44e-03	0.00e+00	4.31e-03	5.99e-04	4.56e-04	4.25e-03	2.17e-03	8.15e-02		8.52e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.5909 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.5909 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.590886  0.000000
];

