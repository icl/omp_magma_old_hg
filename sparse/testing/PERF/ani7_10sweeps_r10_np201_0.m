% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_201 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	812439	812200	141586	141586	8.87e-03	3.70e-02	7.67e-03	1.66e-02	7.21e-04	0.00e+00	2.69e-03	4.68e-04	3.27e-04	1.04e-02	1.67e-03	8.64e-02		8.64e-02
1	797428	798245	135779	136018	8.95e-03	3.29e-02	3.13e-03	6.66e-03	6.81e-04	0.00e+00	3.40e-03	4.21e-04	3.85e-04	6.11e-03	1.91e-03	6.45e-02		1.51e-01
2	817652	814456	151596	150779	8.82e-03	4.50e-02	3.65e-03	2.26e-03	3.22e-04	0.00e+00	3.65e-03	5.01e-04	3.96e-04	3.88e-03	2.19e-03	7.07e-02		2.22e-01
3	816283	812279	132178	135374	9.00e-03	5.31e-02	4.36e-03	3.83e-03	3.45e-04	0.00e+00	4.14e-03	8.28e-04	4.72e-04	3.83e-03	2.25e-03	8.21e-02		3.04e-01
4	802418	808157	134353	138357	8.97e-03	5.89e-02	4.47e-03	1.89e-03	4.32e-04	0.00e+00	4.34e-03	6.44e-04	4.74e-04	4.91e-03	2.09e-03	8.71e-02		3.91e-01
5	813300	810019	149023	143284	8.91e-03	4.95e-02	4.25e-03	2.25e-03	1.53e-03	0.00e+00	4.33e-03	5.91e-04	4.65e-04	4.27e-03	2.13e-03	7.83e-02		4.69e-01
6	810103	812818	138947	142228	8.98e-03	5.37e-02	4.38e-03	2.37e-03	4.75e-04	0.00e+00	4.50e-03	6.12e-04	4.60e-04	4.44e-03	2.13e-03	8.20e-02		5.51e-01
7	811477	810893	142950	140235	8.97e-03	5.13e-02	4.40e-03	1.86e-03	4.83e-04	0.00e+00	4.29e-03	6.82e-04	4.49e-04	3.96e-03	2.13e-03	7.85e-02		6.30e-01
8	811975	815080	142382	142966	8.96e-03	5.50e-02	4.41e-03	2.59e-03	1.28e-03	0.00e+00	4.43e-03	7.33e-04	4.44e-04	4.15e-03	2.15e-03	8.41e-02		7.14e-01
9	813209	814556	142690	139585	9.05e-03	5.19e-02	4.48e-03	4.18e-03	1.28e-03	0.00e+00	4.45e-03	6.45e-04	4.67e-04	3.90e-03	2.16e-03	8.25e-02		7.96e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.4139 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.4139 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.413883  0.000000
];

