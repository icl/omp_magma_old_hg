% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_66 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	810755	799148	141586	141586	3.22e-03	2.67e-02	4.59e-03	7.02e-03	5.95e-04	0.00e+00	3.70e-03	2.84e-04	4.20e-04	5.91e-03	2.19e-03	5.46e-02		5.46e-02
1	794923	819530	137463	149070	3.25e-03	2.89e-02	3.22e-03	2.81e-03	7.36e-04	0.00e+00	4.46e-03	2.06e-04	4.61e-04	4.14e-03	2.97e-03	5.11e-02		1.06e-01
2	808491	804211	154101	129494	3.28e-03	4.97e-02	4.19e-03	1.53e-03	4.38e-04	0.00e+00	5.42e-03	3.38e-04	5.18e-04	3.21e-03	3.10e-03	7.18e-02		1.78e-01
3	798340	808844	141339	145619	3.25e-03	6.20e-02	4.80e-03	1.59e-03	5.13e-04	0.00e+00	6.29e-03	2.77e-04	6.46e-04	3.15e-03	2.93e-03	8.54e-02		2.63e-01
4	812755	807930	152296	141792	3.26e-03	5.87e-02	4.80e-03	1.91e-03	6.17e-04	0.00e+00	6.82e-03	3.39e-04	6.35e-04	3.55e-03	2.79e-03	8.34e-02		3.46e-01
5	811573	815219	138686	143511	3.26e-03	5.67e-02	4.92e-03	2.09e-03	9.77e-04	0.00e+00	6.93e-03	3.42e-04	6.63e-04	3.39e-03	2.90e-03	8.22e-02		4.29e-01
6	809012	808496	140674	137028	3.30e-03	5.96e-02	4.95e-03	1.25e-03	6.15e-04	0.00e+00	6.55e-03	2.59e-04	6.53e-04	3.24e-03	2.87e-03	8.32e-02		5.12e-01
7	812796	816492	144041	144557	3.28e-03	5.82e-02	4.91e-03	1.66e-03	8.53e-04	0.00e+00	7.02e-03	3.92e-04	6.43e-04	3.18e-03	2.92e-03	8.30e-02		5.95e-01
8	812402	815516	141063	137367	3.31e-03	6.08e-02	4.98e-03	1.41e-03	8.41e-04	0.00e+00	7.01e-03	2.67e-04	6.59e-04	3.37e-03	2.92e-03	8.56e-02		6.80e-01
9	813897	813627	142263	139149	3.30e-03	5.95e-02	5.01e-03	1.67e-03	1.12e-03	0.00e+00	6.87e-03	3.11e-04	6.59e-04	3.24e-03	3.76e-03	8.54e-02		7.66e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.1416 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.1416 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.141568  0.000000
];

