% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_125 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	811645	811884	141586	141586	5.93e-03	2.81e-02	5.78e-03	1.06e-02	4.88e-04	0.00e+00	2.92e-03	3.18e-04	3.57e-04	6.72e-03	1.97e-03	6.32e-02		6.32e-02
1	809487	805900	136573	136334	6.00e-03	2.75e-02	3.15e-03	2.52e-03	5.47e-04	0.00e+00	3.47e-03	2.83e-04	3.81e-04	4.92e-03	2.10e-03	5.09e-02		1.14e-01
2	796045	802792	139537	143124	5.92e-03	3.74e-02	3.73e-03	4.29e-03	2.73e-04	0.00e+00	4.08e-03	2.88e-04	4.45e-04	3.02e-03	2.30e-03	6.18e-02		1.76e-01
3	805913	822928	153785	147038	5.88e-03	4.39e-02	4.14e-03	1.34e-03	3.71e-04	0.00e+00	4.62e-03	3.86e-04	4.35e-04	2.97e-03	2.36e-03	6.64e-02		2.42e-01
4	808577	800189	144723	127708	6.03e-03	4.72e-02	4.43e-03	1.68e-03	3.98e-04	0.00e+00	4.85e-03	4.60e-04	4.57e-04	3.59e-03	2.24e-03	7.13e-02		3.14e-01
5	806290	810225	142864	151252	5.91e-03	4.46e-02	4.34e-03	2.65e-03	7.65e-04	0.00e+00	4.93e-03	2.92e-04	4.61e-04	3.26e-03	2.32e-03	6.95e-02		3.83e-01
6	812860	812641	145957	142022	5.97e-03	4.75e-02	4.43e-03	1.71e-03	4.55e-04	0.00e+00	4.75e-03	4.69e-04	4.54e-04	3.33e-03	2.34e-03	7.14e-02		4.55e-01
7	810359	809955	140193	140412	5.93e-03	4.76e-02	4.53e-03	1.52e-03	3.71e-04	0.00e+00	4.76e-03	4.46e-04	4.68e-04	3.03e-03	2.32e-03	7.10e-02		5.25e-01
8	817248	813158	143500	143904	5.92e-03	4.94e-02	4.52e-03	1.69e-03	7.04e-04	0.00e+00	5.02e-03	5.37e-04	4.52e-04	3.16e-03	2.36e-03	7.37e-02		5.99e-01
9	811680	811527	137417	141507	5.99e-03	4.92e-02	4.74e-03	1.66e-03	7.79e-04	0.00e+00	4.75e-03	2.85e-04	4.74e-04	2.97e-03	2.34e-03	7.32e-02		6.72e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.1524 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.1524 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.152436  0.000000
];

