% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_124 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	822619	807035	141586	141586	5.88e-03	2.80e-02	5.79e-03	1.06e-02	1.98e-03	0.00e+00	2.91e-03	3.36e-04	3.61e-04	7.43e-03	1.79e-03	6.51e-02		6.51e-02
1	819706	807104	125599	141183	5.88e-03	2.83e-02	3.21e-03	2.32e-03	1.72e-03	0.00e+00	3.69e-03	3.26e-04	3.76e-04	4.43e-03	2.18e-03	5.25e-02		1.18e-01
2	804969	816994	129318	141920	5.89e-03	3.81e-02	3.82e-03	1.43e-03	3.93e-04	0.00e+00	4.20e-03	3.01e-04	4.40e-04	3.02e-03	2.37e-03	6.00e-02		1.78e-01
3	814293	805648	144861	132836	6.01e-03	4.63e-02	4.24e-03	2.16e-03	3.36e-04	0.00e+00	4.73e-03	5.57e-04	4.55e-04	2.97e-03	2.29e-03	7.01e-02		2.48e-01
4	810743	808191	136343	144988	5.90e-03	4.79e-02	4.42e-03	1.52e-03	4.52e-04	0.00e+00	5.01e-03	6.45e-04	4.65e-04	3.17e-03	2.27e-03	7.18e-02		3.19e-01
5	810071	810721	140698	143250	5.92e-03	4.52e-02	6.52e-03	1.63e-03	5.02e-04	0.00e+00	4.78e-03	5.83e-04	5.68e-04	3.53e-03	2.28e-03	7.15e-02		3.91e-01
6	815994	810758	142176	141526	5.91e-03	4.74e-02	4.49e-03	2.13e-03	3.96e-04	0.00e+00	4.94e-03	4.30e-04	4.44e-04	3.33e-03	2.45e-03	7.19e-02		4.63e-01
7	810601	811327	137059	142295	5.91e-03	4.79e-02	4.56e-03	1.52e-03	3.60e-04	0.00e+00	4.65e-03	5.15e-04	4.57e-04	3.04e-03	2.36e-03	7.13e-02		5.34e-01
8	811175	811217	143258	142532	5.90e-03	4.96e-02	4.56e-03	1.31e-03	3.68e-04	0.00e+00	4.97e-03	5.22e-04	4.32e-04	3.15e-03	2.37e-03	7.32e-02		6.07e-01
9	811858	814418	143490	143448	5.94e-03	4.78e-02	4.59e-03	1.33e-03	8.64e-04	0.00e+00	5.09e-03	4.20e-04	4.45e-04	3.00e-03	2.36e-03	7.18e-02		6.79e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.1656 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.1656 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.165553  0.000000
];

