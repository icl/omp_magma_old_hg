% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_57 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	818027	812365	141586	141586	3.94e-03	3.13e-02	7.03e-03	6.95e-03	4.49e-04	0.00e+00	4.30e-03	2.24e-04	5.84e-04	6.31e-03	3.41e-03	6.45e-02		6.45e-02
1	813394	810977	130191	135853	5.13e-03	3.41e-02	6.03e-03	1.97e-03	4.99e-04	0.00e+00	5.22e-03	2.81e-04	5.90e-04	4.35e-03	4.05e-03	6.22e-02		1.27e-01
2	800564	810332	135630	138047	5.10e-03	5.25e-02	7.18e-03	2.32e-03	4.19e-04	0.00e+00	6.37e-03	3.18e-04	6.87e-04	3.38e-03	4.44e-03	8.27e-02		2.09e-01
3	803820	804733	149266	139498	5.10e-03	6.69e-02	6.65e-03	2.66e-03	6.59e-04	0.00e+00	7.20e-03	5.52e-04	7.09e-04	3.51e-03	3.34e-03	9.73e-02		3.07e-01
4	799604	808620	146816	145903	3.26e-03	6.47e-02	5.54e-03	1.85e-03	7.90e-04	0.00e+00	7.83e-03	3.50e-04	7.11e-04	3.82e-03	3.19e-03	9.20e-02		3.99e-01
5	808174	809299	151837	142821	3.27e-03	6.15e-02	5.58e-03	2.33e-03	9.54e-04	0.00e+00	7.99e-03	3.32e-04	7.18e-04	3.49e-03	3.34e-03	8.95e-02		4.88e-01
6	811469	809807	144073	142948	3.29e-03	6.38e-02	5.64e-03	1.48e-03	7.52e-04	0.00e+00	7.89e-03	2.53e-04	7.15e-04	3.44e-03	3.56e-03	9.08e-02		5.79e-01
7	810502	812409	141584	143246	3.28e-03	6.37e-02	5.72e-03	1.81e-03	7.66e-04	0.00e+00	7.88e-03	3.51e-04	7.21e-04	3.34e-03	3.33e-03	9.09e-02		6.70e-01
8	810784	813336	143357	141450	3.29e-03	6.61e-02	5.76e-03	1.90e-03	1.19e-03	0.00e+00	8.27e-03	3.70e-04	7.12e-04	3.50e-03	3.29e-03	9.44e-02		7.64e-01
9	810763	814354	143881	141329	3.30e-03	6.54e-02	5.78e-03	1.47e-03	1.06e-03	0.00e+00	7.84e-03	3.74e-04	7.06e-04	3.32e-03	3.32e-03	9.26e-02		8.57e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.2282 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.2282 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.228175  0.000000
];

