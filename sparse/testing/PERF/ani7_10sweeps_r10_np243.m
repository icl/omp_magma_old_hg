% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_243 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	818944	803464	141586	141586	1.20e-02	6.64e-02	1.68e-02	2.38e-02	6.08e-03	0.00e+00	3.10e-03	6.09e-04	3.53e-04	2.34e-02	1.66e-03	1.54e-01		1.54e-01
1	799086	815744	129274	144754	1.20e-02	4.93e-02	3.32e-03	5.20e-03	2.87e-03	0.00e+00	3.40e-03	5.38e-04	3.44e-04	7.09e-03	2.23e-03	8.63e-02		2.40e-01
2	805749	797769	149938	133280	1.21e-02	7.13e-02	4.04e-03	1.97e-03	1.02e-03	0.00e+00	3.85e-03	5.96e-04	4.44e-04	4.51e-03	2.24e-03	1.02e-01		3.42e-01
3	821239	801376	144081	152061	1.19e-02	6.78e-02	4.44e-03	1.81e-03	4.94e-04	0.00e+00	4.03e-03	5.52e-04	4.50e-04	4.44e-03	2.32e-03	9.82e-02		4.41e-01
4	807912	809569	129397	149260	1.19e-02	8.10e-02	4.67e-03	4.75e-03	5.48e-04	0.00e+00	4.37e-03	4.90e-04	5.10e-04	4.45e-03	2.20e-03	1.15e-01		5.56e-01
