% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_223 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	823418	809486	141586	141586	1.03e-02	4.31e-02	9.11e-03	1.97e-02	1.02e-03	0.00e+00	2.46e-03	5.42e-04	4.00e-04	1.35e-02	1.76e-03	1.02e-01		1.02e-01
1	802228	802936	124800	138732	1.02e-02	3.49e-02	3.54e-03	5.67e-03	1.99e-03	0.00e+00	3.07e-03	4.49e-04	3.44e-04	7.70e-03	1.97e-03	6.99e-02		1.72e-01
2	812648	820043	146796	146088	1.01e-02	5.20e-02	3.69e-03	2.46e-03	3.94e-04	0.00e+00	3.37e-03	5.12e-04	3.88e-04	4.30e-03	7.21e-03	8.45e-02		2.56e-01
3	799870	806184	137182	129787	1.03e-02	5.45e-02	4.57e-03	3.66e-03	3.81e-04	0.00e+00	3.83e-03	7.57e-04	4.54e-04	4.35e-03	1.99e-03	8.48e-02		3.41e-01
4	809028	801993	150766	144452	1.02e-02	5.46e-02	4.60e-03	2.88e-03	6.91e-04	0.00e+00	4.32e-03	8.52e-04	4.46e-04	4.68e-03	2.26e-03	8.55e-02		4.27e-01
5	811232	810998	142413	149448	1.01e-02	5.00e-02	4.39e-03	2.42e-03	4.35e-04	0.00e+00	3.87e-03	1.09e-03	4.26e-04	5.12e-03	2.02e-03	8.00e-02		5.07e-01
6	809354	806619	141015	141249	1.02e-02	5.58e-02	4.49e-03	2.67e-03	3.98e-04	0.00e+00	3.98e-03	1.02e-03	4.52e-04	4.81e-03	2.01e-03	8.58e-02		5.92e-01
7	810885	814072	143699	146434	1.02e-02	5.06e-02	4.44e-03	1.87e-03	4.04e-04	0.00e+00	3.99e-03	7.18e-04	4.30e-04	4.27e-03	2.04e-03	7.89e-02		6.71e-01
8	809061	813697	142974	139787	1.02e-02	5.60e-02	4.56e-03	1.91e-03	7.46e-04	0.00e+00	3.89e-03	5.96e-04	4.19e-04	4.45e-03	2.03e-03	8.49e-02		7.56e-01
9	811729	814067	145604	140968	1.03e-02	5.13e-02	4.43e-03	2.56e-03	1.33e-03	0.00e+00	3.92e-03	5.85e-04	4.26e-04	6.11e-03	2.04e-03	8.30e-02		8.39e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.5827 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.5827 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.582659  0.000000
];

