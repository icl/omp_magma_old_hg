% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_99 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	826373	806709	141586	141586	5.60e-03	2.90e-02	6.02e-03	9.92e-03	1.28e-03	0.00e+00	2.72e-03	3.21e-04	4.60e-04	6.35e-03	2.26e-03	6.40e-02		6.40e-02
1	794069	808003	121845	141509	5.56e-03	2.87e-02	4.02e-03	1.70e-03	9.05e-04	0.00e+00	3.32e-03	2.65e-04	4.41e-04	4.33e-03	2.35e-03	5.16e-02		1.16e-01
2	815638	800398	154955	141021	5.56e-03	4.54e-02	4.37e-03	1.40e-03	7.61e-04	0.00e+00	3.71e-03	4.01e-04	4.55e-04	3.21e-03	2.59e-03	6.79e-02		1.83e-01
3	805357	804151	134192	149432	5.50e-03	4.94e-02	4.74e-03	1.70e-03	3.66e-04	0.00e+00	4.33e-03	3.28e-04	4.84e-04	3.14e-03	2.59e-03	7.26e-02		2.56e-01
4	805210	810292	145279	146485	5.51e-03	4.79e-02	5.00e-03	2.82e-03	5.89e-04	0.00e+00	4.64e-03	5.57e-04	5.02e-04	3.19e-03	2.63e-03	7.33e-02		3.29e-01
5	805491	811618	146231	141149	5.57e-03	4.60e-02	5.31e-03	2.58e-03	5.17e-04	0.00e+00	4.55e-03	4.04e-04	5.25e-04	3.67e-03	2.71e-03	7.18e-02		4.01e-01
6	809411	810315	146756	140629	5.58e-03	4.93e-02	5.45e-03	1.91e-03	4.89e-04	0.00e+00	4.66e-03	3.77e-04	5.14e-04	3.31e-03	2.72e-03	7.43e-02		4.75e-01
7	811090	812949	143642	142738	5.54e-03	4.70e-02	5.45e-03	2.43e-03	5.19e-04	0.00e+00	4.53e-03	3.70e-04	4.88e-04	3.38e-03	2.68e-03	7.24e-02		5.48e-01
8	812792	813243	142769	140910	5.61e-03	5.03e-02	5.58e-03	2.20e-03	1.05e-03	0.00e+00	4.72e-03	3.11e-04	5.41e-04	3.20e-03	2.71e-03	7.62e-02		6.24e-01
9	813521	814961	141873	141422	5.59e-03	4.92e-02	5.58e-03	2.25e-03	1.08e-03	0.00e+00	4.76e-03	2.61e-04	5.35e-04	3.18e-03	2.74e-03	7.51e-02		6.99e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.1778 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.1778 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.177779  0.000000
];

