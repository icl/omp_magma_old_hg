% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_110 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	826255	815556	141586	141586	5.70e-03	2.81e-02	5.82e-03	1.03e-02	1.27e-03	0.00e+00	2.71e-03	3.73e-04	3.86e-04	6.10e-03	2.25e-03	6.30e-02		6.30e-02
1	805731	806284	121963	132662	5.79e-03	2.85e-02	3.62e-03	4.36e-03	1.28e-03	0.00e+00	2.95e-03	3.25e-04	4.17e-04	4.28e-03	2.38e-03	5.39e-02		1.17e-01
2	800831	814965	143293	142740	5.72e-03	4.20e-02	4.19e-03	1.66e-03	7.38e-04	0.00e+00	3.62e-03	3.46e-04	4.67e-04	3.06e-03	2.54e-03	6.43e-02		1.81e-01
3	803270	817646	148999	134865	5.75e-03	4.59e-02	4.63e-03	1.37e-03	3.40e-04	0.00e+00	3.95e-03	3.59e-04	4.31e-04	3.12e-03	2.50e-03	6.83e-02		2.50e-01
4	808882	809098	147366	132990	5.74e-03	4.52e-02	4.70e-03	1.57e-03	4.42e-04	0.00e+00	4.16e-03	5.58e-04	4.75e-04	3.06e-03	2.45e-03	6.84e-02		3.18e-01
5	811117	812780	142559	142343	5.75e-03	4.35e-02	4.82e-03	1.43e-03	4.18e-04	0.00e+00	4.21e-03	5.73e-04	4.32e-04	3.39e-03	2.47e-03	6.70e-02		3.85e-01
6	814773	811615	141130	139467	5.75e-03	4.58e-02	4.90e-03	2.12e-03	3.77e-04	0.00e+00	4.13e-03	4.16e-04	4.29e-04	3.20e-03	2.50e-03	6.97e-02		4.55e-01
7	813152	810301	138280	141438	5.74e-03	4.44e-02	4.97e-03	2.45e-03	4.06e-04	0.00e+00	4.24e-03	2.67e-04	4.37e-04	3.36e-03	2.52e-03	6.88e-02		5.23e-01
8	813129	812643	140707	143558	5.72e-03	4.61e-02	4.97e-03	1.97e-03	9.56e-04	0.00e+00	4.15e-03	4.77e-04	4.60e-04	3.17e-03	2.49e-03	7.05e-02		5.94e-01
9	812225	812341	141536	142022	5.73e-03	4.47e-02	5.01e-03	1.74e-03	9.46e-04	0.00e+00	4.11e-03	4.19e-04	4.52e-04	3.04e-03	2.56e-03	6.87e-02		6.63e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.1365 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.1365 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.136521  0.000000
];

