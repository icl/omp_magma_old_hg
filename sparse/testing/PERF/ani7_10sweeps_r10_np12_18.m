% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_12 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	819805	808928	141586	141586	4.02e-03	9.04e-02	1.90e-02	8.79e-03	1.52e-03	0.00e+00	2.05e-02	2.69e-04	2.09e-03	9.69e-03	1.22e-02	1.69e-01		1.69e-01
1	806196	816661	128413	139290	4.04e-03	1.14e-01	1.86e-02	5.34e-03	1.86e-03	0.00e+00	2.56e-02	3.01e-04	2.34e-03	9.02e-03	1.58e-02	1.97e-01		3.66e-01
2	830802	788227	142828	132363	4.18e-03	1.86e-01	2.38e-02	5.40e-03	1.70e-03	0.00e+00	3.07e-02	2.43e-04	2.44e-03	8.41e-03	1.70e-02	2.80e-01		6.46e-01
3	806084	799342	119028	161603	4.01e-03	2.73e-01	2.69e-02	4.22e-03	2.29e-03	0.00e+00	3.55e-02	4.03e-04	2.75e-03	8.25e-03	1.56e-02	3.73e-01		1.02e+00
4	807791	810343	144552	151294	4.07e-03	2.40e-01	2.68e-02	5.81e-03	3.72e-03	0.00e+00	4.00e-02	3.98e-04	3.00e-03	8.65e-03	1.53e-02	3.48e-01		1.37e+00
5	805605	806616	143650	141098	4.12e-03	2.41e-01	2.72e-02	6.27e-03	3.66e-03	0.00e+00	3.87e-02	3.92e-04	2.89e-03	8.66e-03	1.54e-02	3.49e-01		1.72e+00
6	809493	807637	146642	145631	4.10e-03	2.45e-01	2.72e-02	5.52e-03	3.08e-03	0.00e+00	3.85e-02	4.13e-04	2.83e-03	8.28e-03	1.54e-02	3.50e-01		2.07e+00
7	813460	815284	143560	145416	4.12e-03	2.46e-01	2.74e-02	5.25e-03	3.02e-03	0.00e+00	3.79e-02	2.92e-04	2.80e-03	8.63e-03	1.57e-02	3.51e-01		2.42e+00
8	807819	812848	140399	138575	4.15e-03	2.59e-01	2.82e-02	4.92e-03	3.30e-03	0.00e+00	3.80e-02	4.28e-04	2.83e-03	8.36e-03	1.56e-02	3.65e-01		2.78e+00
9	810840	812685	146846	141817	4.17e-03	2.53e-01	2.79e-02	5.30e-03	3.25e-03	0.00e+00	3.76e-02	3.21e-04	2.76e-03	8.28e-03	1.56e-02	3.58e-01		3.14e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 3.5792 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 3.5792 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  3.579191  0.000000
];

