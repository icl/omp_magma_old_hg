% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_58 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	819677	813812	141586	141586	3.21e-03	2.80e-02	4.95e-03	6.86e-03	7.85e-04	0.00e+00	4.18e-03	2.51e-04	4.87e-04	5.81e-03	2.47e-03	5.70e-02		5.70e-02
1	800981	816933	128541	134406	3.28e-03	3.11e-02	3.68e-03	2.98e-03	1.31e-03	0.00e+00	5.07e-03	1.95e-04	5.09e-04	4.04e-03	3.34e-03	5.55e-02		1.13e-01
2	802935	804731	148043	132091	3.28e-03	5.26e-02	4.76e-03	1.27e-03	4.09e-04	0.00e+00	6.21e-03	3.01e-04	5.40e-04	3.38e-03	3.48e-03	7.63e-02		1.89e-01
3	822557	806554	146895	145099	3.28e-03	6.69e-02	5.42e-03	2.44e-03	6.58e-04	0.00e+00	7.31e-03	2.70e-04	6.45e-04	3.36e-03	3.46e-03	9.38e-02		2.83e-01
4	803526	814611	128079	144082	3.26e-03	6.99e-02	5.64e-03	1.64e-03	8.33e-04	0.00e+00	7.81e-03	3.91e-04	7.20e-04	3.66e-03	3.19e-03	9.70e-02		3.80e-01
5	808539	810235	147915	136830	3.30e-03	6.15e-02	5.54e-03	2.33e-03	8.68e-04	0.00e+00	7.90e-03	2.23e-04	7.32e-04	3.59e-03	3.24e-03	8.92e-02		4.69e-01
6	808188	814357	143708	142012	3.28e-03	6.40e-02	5.56e-03	3.54e-03	7.05e-04	0.00e+00	7.67e-03	4.00e-04	6.56e-04	3.49e-03	3.21e-03	9.25e-02		5.61e-01
7	808919	809042	144865	138696	3.34e-03	6.45e-02	5.66e-03	1.74e-03	7.16e-04	0.00e+00	7.71e-03	3.27e-04	7.01e-04	3.34e-03	3.23e-03	9.13e-02		6.53e-01
8	816175	810031	144940	144817	3.33e-03	6.58e-02	5.63e-03	3.46e-03	8.66e-04	0.00e+00	7.89e-03	2.73e-04	7.35e-04	3.61e-03	3.24e-03	9.48e-02		7.47e-01
9	810193	814169	138490	144634	3.30e-03	6.61e-02	5.71e-03	1.87e-03	1.16e-03	0.00e+00	7.85e-03	4.57e-04	7.13e-04	3.31e-03	3.27e-03	9.37e-02		8.41e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.2139 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.2139 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.213904  0.000000
];

