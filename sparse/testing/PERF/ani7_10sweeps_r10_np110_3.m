% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_110 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	826748	815642	141586	141586	5.73e-03	2.87e-02	5.89e-03	9.99e-03	4.15e-04	0.00e+00	2.46e-03	3.51e-04	4.35e-04	6.50e-03	2.05e-03	6.25e-02		6.25e-02
1	810942	807682	121470	132576	5.80e-03	2.78e-02	3.64e-03	3.27e-03	4.77e-04	0.00e+00	3.04e-03	3.16e-04	4.13e-04	4.77e-03	2.38e-03	5.18e-02		1.14e-01
2	803102	788127	138082	141342	5.71e-03	4.21e-02	4.22e-03	2.22e-03	2.92e-04	0.00e+00	3.45e-03	3.77e-04	4.28e-04	3.09e-03	2.49e-03	6.43e-02		1.79e-01
3	806157	806850	146728	161703	5.55e-03	4.30e-02	4.53e-03	1.89e-03	3.92e-04	0.00e+00	3.92e-03	4.48e-04	4.44e-04	3.06e-03	2.52e-03	6.58e-02		2.44e-01
4	813803	804888	144479	143786	5.68e-03	4.42e-02	4.69e-03	2.09e-03	4.93e-04	0.00e+00	4.18e-03	5.21e-04	4.41e-04	3.50e-03	2.48e-03	6.82e-02		3.13e-01
5	810493	807830	137638	146553	5.71e-03	4.23e-02	4.87e-03	1.74e-03	8.99e-04	0.00e+00	4.30e-03	4.32e-04	4.77e-04	3.22e-03	2.45e-03	6.64e-02		3.79e-01
6	809867	808603	141754	144417	5.71e-03	4.54e-02	4.89e-03	1.72e-03	4.80e-04	0.00e+00	4.26e-03	4.17e-04	4.80e-04	3.34e-03	2.46e-03	6.91e-02		4.48e-01
7	814264	816536	143186	144450	5.71e-03	4.29e-02	4.91e-03	2.57e-03	3.90e-04	0.00e+00	4.06e-03	6.48e-04	4.24e-04	3.15e-03	2.50e-03	6.72e-02		5.16e-01
8	812613	808360	139595	137323	5.77e-03	4.72e-02	5.07e-03	1.99e-03	6.73e-04	0.00e+00	3.95e-03	5.94e-04	4.31e-04	3.16e-03	2.50e-03	7.13e-02		5.87e-01
9	814882	811340	142052	146305	5.71e-03	4.34e-02	4.93e-03	2.37e-03	7.73e-04	0.00e+00	4.20e-03	4.44e-04	4.63e-04	3.12e-03	2.53e-03	6.79e-02		6.55e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.1317 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.1317 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.131662  0.000000
];

