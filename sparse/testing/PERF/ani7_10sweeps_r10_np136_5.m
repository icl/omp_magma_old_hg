% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_136 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	815527	810071	141586	141586	5.99e-03	2.99e-02	5.82e-03	1.25e-02	2.10e-03	0.00e+00	2.72e-03	3.40e-04	3.56e-04	8.80e-03	1.69e-03	7.03e-02		7.03e-02
1	820659	815305	132691	138147	6.16e-03	2.83e-02	3.55e-03	4.81e-03	1.79e-03	0.00e+00	3.24e-03	3.40e-04	3.65e-04	5.27e-03	2.30e-03	5.62e-02		1.26e-01
2	810504	798583	128365	133719	6.07e-03	4.53e-02	3.98e-03	1.39e-03	3.44e-04	0.00e+00	3.89e-03	5.52e-04	4.49e-04	3.27e-03	2.29e-03	6.75e-02		1.94e-01
3	806580	824055	139326	151247	5.96e-03	4.87e-02	4.01e-03	2.58e-03	3.36e-04	0.00e+00	4.38e-03	4.07e-04	4.64e-04	3.41e-03	2.29e-03	7.26e-02		2.67e-01
4	807494	803790	144056	126581	6.13e-03	5.07e-02	4.24e-03	1.60e-03	6.66e-04	0.00e+00	4.71e-03	7.41e-04	5.10e-04	3.25e-03	2.13e-03	7.46e-02		3.41e-01
5	811110	809119	143947	147651	6.00e-03	4.49e-02	4.13e-03	2.40e-03	5.17e-04	0.00e+00	4.64e-03	2.75e-04	4.83e-04	3.84e-03	2.17e-03	6.94e-02		4.10e-01
6	811740	811777	141137	143128	6.02e-03	4.94e-02	4.20e-03	3.04e-03	6.23e-04	0.00e+00	4.66e-03	3.34e-04	5.14e-04	3.84e-03	2.23e-03	7.49e-02		4.85e-01
7	811043	811255	141313	141276	6.05e-03	4.74e-02	4.24e-03	2.38e-03	4.75e-04	0.00e+00	4.64e-03	6.96e-04	4.77e-04	3.59e-03	2.22e-03	7.21e-02		5.57e-01
8	813657	812356	142816	142604	6.03e-03	4.94e-02	5.78e-03	2.06e-03	4.16e-04	0.00e+00	4.66e-03	5.62e-04	5.03e-04	3.31e-03	2.52e-03	7.52e-02		6.33e-01
9	811629	816100	141008	142309	8.00e-03	4.76e-02	5.81e-03	1.57e-03	1.12e-03	0.00e+00	4.70e-03	5.06e-04	5.05e-04	3.64e-03	2.58e-03	7.61e-02		7.09e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.2430 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.2430 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.242954  0.000000
];

