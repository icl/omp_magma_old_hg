% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_167 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	816661	804546	141586	141586	8.17e-03	3.65e-02	7.48e-03	1.52e-02	1.13e-03	0.00e+00	2.39e-03	5.67e-04	4.16e-04	1.04e-02	1.97e-03	8.42e-02		8.42e-02
1	787038	815443	131557	143672	8.15e-03	3.12e-02	3.75e-03	4.00e-03	2.80e-03	0.00e+00	2.70e-03	4.47e-04	4.51e-04	6.21e-03	2.10e-03	6.18e-02		1.46e-01
2	805892	817331	161986	133581	8.19e-03	4.62e-02	4.10e-03	3.03e-03	2.86e-04	0.00e+00	3.14e-03	4.00e-04	4.29e-04	4.01e-03	2.29e-03	7.21e-02		2.18e-01
3	813861	809799	143938	132499	8.23e-03	4.79e-02	4.52e-03	2.06e-03	2.98e-04	0.00e+00	3.50e-03	4.70e-04	4.48e-04	3.98e-03	2.44e-03	7.39e-02		2.92e-01
4	804797	808449	136775	140837	8.14e-03	5.11e-02	4.88e-03	2.06e-03	4.25e-04	0.00e+00	3.85e-03	4.87e-04	4.67e-04	4.28e-03	2.36e-03	7.80e-02		3.70e-01
5	809537	805830	146644	142992	8.14e-03	4.54e-02	4.93e-03	2.42e-03	4.63e-04	0.00e+00	3.86e-03	4.36e-04	4.53e-04	4.38e-03	2.39e-03	7.29e-02		4.43e-01
6	812178	808892	142710	146417	8.10e-03	4.95e-02	4.98e-03	1.58e-03	4.20e-04	0.00e+00	3.65e-03	6.70e-04	4.48e-04	4.41e-03	2.39e-03	7.61e-02		5.19e-01
7	814087	809773	140875	144161	8.16e-03	4.59e-02	5.08e-03	2.07e-03	3.82e-04	0.00e+00	3.73e-03	5.06e-04	4.58e-04	4.00e-03	3.65e-03	7.39e-02		5.93e-01
8	808830	813448	139772	144086	8.15e-03	5.11e-02	5.15e-03	3.33e-03	5.54e-04	0.00e+00	3.87e-03	7.69e-04	4.58e-04	4.20e-03	2.44e-03	8.00e-02		6.73e-01
9	811172	811828	145835	141217	8.20e-03	4.70e-02	5.19e-03	2.35e-03	1.14e-03	0.00e+00	3.77e-03	5.35e-04	4.43e-04	3.97e-03	2.42e-03	7.50e-02		7.48e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.3815 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.3815 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.381538  0.000000
];

