% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_87 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	811076	807811	141586	141586	5.39e-03	2.99e-02	6.41e-03	9.52e-03	1.23e-03	0.00e+00	3.12e-03	4.20e-04	4.77e-04	6.14e-03	2.45e-03	6.50e-02		6.50e-02
1	821020	832495	137142	140407	5.38e-03	2.82e-02	4.49e-03	2.91e-03	9.67e-04	0.00e+00	3.59e-03	3.03e-04	4.34e-04	4.30e-03	2.85e-03	5.35e-02		1.18e-01
2	816551	812529	128004	116529	5.52e-03	4.72e-02	5.21e-03	2.58e-03	7.77e-04	0.00e+00	4.41e-03	2.95e-04	4.84e-04	3.38e-03	2.98e-03	7.29e-02		1.91e-01
3	793656	802836	133279	137301	5.42e-03	5.49e-02	5.49e-03	1.72e-03	3.74e-04	0.00e+00	4.88e-03	5.08e-04	5.30e-04	3.21e-03	2.94e-03	8.00e-02		2.71e-01
4	808965	806768	156980	147800	5.33e-03	4.95e-02	5.66e-03	2.73e-03	6.39e-04	0.00e+00	5.36e-03	4.08e-04	5.76e-04	3.25e-03	3.01e-03	7.64e-02		3.48e-01
5	813685	807814	142476	144673	5.34e-03	4.71e-02	6.03e-03	2.52e-03	5.12e-04	0.00e+00	5.00e-03	3.84e-04	5.25e-04	3.58e-03	3.09e-03	7.41e-02		4.22e-01
6	803938	810436	138562	144433	5.37e-03	5.15e-02	6.11e-03	2.02e-03	5.92e-04	0.00e+00	5.35e-03	5.26e-04	5.44e-04	3.42e-03	3.04e-03	7.85e-02		5.00e-01
7	816481	810973	149115	142617	5.37e-03	4.88e-02	6.06e-03	1.61e-03	5.70e-04	0.00e+00	5.29e-03	4.50e-04	5.37e-04	3.55e-03	3.12e-03	7.53e-02		5.76e-01
8	813822	812389	137378	142886	5.40e-03	5.39e-02	6.23e-03	1.86e-03	8.28e-04	0.00e+00	5.24e-03	4.40e-04	5.40e-04	3.37e-03	3.12e-03	8.09e-02		6.57e-01
9	812731	812754	140843	142276	5.37e-03	5.16e-02	6.21e-03	1.42e-03	1.12e-03	0.00e+00	5.25e-03	4.77e-04	5.32e-04	3.27e-03	3.14e-03	7.84e-02		7.35e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.2077 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.2077 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.207739  0.000000
];

