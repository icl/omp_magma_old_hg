% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_41 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	813255	807798	141586	141586	3.29e-03	3.34e-02	6.42e-03	6.77e-03	7.91e-04	0.00e+00	5.86e-03	2.53e-04	6.64e-04	6.16e-03	3.38e-03	6.70e-02		6.70e-02
1	801482	813656	134963	140420	3.28e-03	3.93e-02	5.13e-03	3.78e-03	1.22e-03	0.00e+00	7.38e-03	2.20e-04	7.05e-04	4.46e-03	4.45e-03	7.00e-02		1.37e-01
2	801440	810853	147542	135368	3.33e-03	6.35e-02	9.75e-03	1.89e-03	6.70e-04	0.00e+00	8.73e-03	2.72e-04	8.91e-04	3.88e-03	6.12e-03	9.90e-02		2.36e-01
3	805360	794873	148390	138977	5.03e-03	8.66e-02	1.07e-02	1.75e-03	7.24e-04	0.00e+00	1.02e-02	2.66e-04	9.26e-04	3.82e-03	5.63e-03	1.26e-01		3.62e-01
4	812537	813164	145276	155763	5.00e-03	8.15e-02	7.56e-03	2.93e-03	9.92e-04	0.00e+00	1.08e-02	3.82e-04	9.05e-04	3.91e-03	4.45e-03	1.18e-01		4.80e-01
5	810492	814166	138904	138277	3.33e-03	8.46e-02	7.90e-03	1.88e-03	9.23e-04	0.00e+00	1.09e-02	2.46e-04	8.75e-04	4.20e-03	4.51e-03	1.19e-01		5.99e-01
6	809112	814648	141755	138081	3.34e-03	8.57e-02	7.91e-03	2.14e-03	9.99e-04	0.00e+00	1.10e-02	3.17e-04	9.16e-04	3.94e-03	4.49e-03	1.21e-01		7.20e-01
7	811769	811079	143941	138405	3.36e-03	8.58e-02	7.95e-03	2.12e-03	9.15e-04	0.00e+00	1.10e-02	3.07e-04	8.77e-04	3.87e-03	4.49e-03	1.21e-01		8.41e-01
8	810996	813202	142090	142780	3.33e-03	8.76e-02	7.97e-03	2.26e-03	1.14e-03	0.00e+00	1.11e-02	3.40e-04	9.17e-04	3.92e-03	4.48e-03	1.23e-01		9.64e-01
9	814155	812062	143669	141463	3.37e-03	8.72e-02	8.03e-03	2.01e-03	1.56e-03	0.00e+00	1.11e-02	3.01e-04	9.20e-04	3.81e-03	4.50e-03	1.23e-01		1.09e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.4559 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.4559 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.455920  0.000000
];

