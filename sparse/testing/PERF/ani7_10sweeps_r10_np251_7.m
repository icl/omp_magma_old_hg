% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_251 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	814794	814930	141586	141586	1.12e-02	4.48e-02	9.28e-03	2.09e-02	1.07e-03	0.00e+00	2.16e-03	6.05e-04	3.39e-04	1.39e-02	1.67e-03	1.06e-01		1.06e-01
1	808043	821510	133424	133288	1.12e-02	3.64e-02	3.21e-03	2.70e-03	3.00e-03	0.00e+00	2.93e-03	5.15e-04	3.47e-04	7.87e-03	1.94e-03	7.02e-02		1.76e-01
2	802222	802886	140981	127514	1.12e-02	5.08e-02	3.71e-03	2.38e-03	4.39e-04	0.00e+00	3.02e-03	5.51e-04	4.17e-04	4.20e-03	2.07e-03	7.89e-02		2.55e-01
3	816165	813536	147608	146944	1.10e-02	4.87e-02	4.01e-03	3.63e-03	3.66e-04	0.00e+00	3.27e-03	4.40e-04	4.14e-04	4.23e-03	2.17e-03	7.83e-02		3.33e-01
4	806420	797942	134471	137100	1.12e-02	5.73e-02	4.23e-03	3.19e-03	4.60e-04	0.00e+00	3.53e-03	5.27e-04	4.14e-04	4.74e-03	2.00e-03	8.77e-02		4.21e-01
5	812044	809378	145021	153499	1.10e-02	4.80e-02	4.07e-03	2.75e-03	4.70e-04	0.00e+00	3.59e-03	6.85e-04	3.99e-04	5.16e-03	2.03e-03	7.81e-02		4.99e-01
6	809235	811455	140203	142869	1.11e-02	5.53e-02	4.20e-03	1.33e-03	5.54e-04	0.00e+00	3.55e-03	6.56e-04	3.92e-04	4.90e-03	2.06e-03	8.40e-02		5.83e-01
7	802863	814154	143818	141598	1.12e-02	5.09e-02	4.23e-03	1.92e-03	3.35e-04	0.00e+00	3.50e-03	7.43e-04	4.32e-04	4.21e-03	2.07e-03	7.95e-02		6.62e-01
8	810364	810996	150996	139705	1.11e-02	5.55e-02	4.23e-03	3.85e-03	6.43e-04	0.00e+00	3.52e-03	5.75e-04	4.08e-04	4.75e-03	2.08e-03	8.67e-02		7.49e-01
9	815879	814135	144301	143669	1.11e-02	5.13e-02	4.27e-03	1.84e-03	1.99e-03	0.00e+00	3.61e-03	5.21e-04	3.90e-04	4.19e-03	2.08e-03	8.13e-02		8.31e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.5845 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.5845 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.584468  0.000000
];

