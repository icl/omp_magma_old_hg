% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_170 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	822040	815232	141586	141586	8.19e-03	3.68e-02	7.48e-03	1.54e-02	2.49e-03	0.00e+00	2.28e-03	4.44e-04	4.13e-04	1.02e-02	2.01e-03	8.57e-02		8.57e-02
1	807774	805403	126178	132986	8.29e-03	3.31e-02	3.72e-03	3.40e-03	2.12e-03	0.00e+00	2.72e-03	3.78e-04	3.93e-04	6.26e-03	2.09e-03	6.25e-02		1.48e-01
2	801221	809095	141250	143621	8.10e-03	4.58e-02	4.06e-03	2.24e-03	2.67e-04	0.00e+00	3.13e-03	4.18e-04	4.05e-04	3.92e-03	2.18e-03	7.06e-02		2.19e-01
3	806711	810833	148609	140735	8.20e-03	4.57e-02	4.39e-03	4.72e-03	3.63e-04	0.00e+00	3.51e-03	4.07e-04	4.17e-04	3.84e-03	2.33e-03	7.38e-02		2.93e-01
4	803842	803617	143925	139803	8.16e-03	4.85e-02	4.70e-03	2.35e-03	4.36e-04	0.00e+00	3.77e-03	6.13e-04	4.52e-04	4.03e-03	2.35e-03	7.54e-02		3.68e-01
5	808020	808685	147599	147824	8.08e-03	4.39e-02	4.76e-03	4.02e-03	3.76e-04	0.00e+00	3.75e-03	4.47e-04	4.53e-04	4.40e-03	2.32e-03	7.25e-02		4.40e-01
6	808656	809981	144227	143562	8.18e-03	4.96e-02	4.91e-03	3.14e-03	4.18e-04	0.00e+00	3.77e-03	6.60e-04	4.47e-04	4.19e-03	2.36e-03	7.77e-02		5.18e-01
7	811350	811294	144397	143072	8.18e-03	4.62e-02	4.94e-03	3.22e-03	5.21e-04	0.00e+00	3.80e-03	5.27e-04	4.25e-04	3.94e-03	2.42e-03	7.41e-02		5.92e-01
8	809036	810283	142509	142565	8.16e-03	5.08e-02	4.93e-03	2.75e-03	4.20e-04	0.00e+00	3.70e-03	5.69e-04	4.21e-04	3.94e-03	2.44e-03	7.81e-02		6.70e-01
9	811727	813615	145629	144382	8.16e-03	4.60e-02	4.95e-03	2.90e-03	1.06e-03	0.00e+00	3.80e-03	5.40e-04	4.45e-04	3.99e-03	2.40e-03	7.43e-02		7.45e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.3782 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.3782 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.378164  0.000000
];

