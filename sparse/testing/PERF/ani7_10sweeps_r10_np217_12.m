% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_217 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	811424	820175	141586	141586	9.98e-03	4.09e-02	9.04e-03	1.87e-02	2.08e-03	0.00e+00	2.51e-03	4.82e-04	3.66e-04	1.13e-02	1.86e-03	9.73e-02		9.73e-02
1	810730	792490	136794	128043	1.01e-02	3.59e-02	3.61e-03	4.78e-03	2.17e-03	0.00e+00	3.14e-03	4.50e-04	3.63e-04	6.54e-03	2.04e-03	6.92e-02		1.66e-01
2	802512	823273	138294	156534	9.72e-03	5.28e-02	3.76e-03	3.01e-03	1.30e-03	0.00e+00	3.61e-03	5.28e-04	4.33e-04	4.21e-03	2.15e-03	8.15e-02		2.48e-01
3	811958	811181	147318	126557	1.01e-02	5.47e-02	4.49e-03	2.30e-03	3.24e-04	0.00e+00	3.78e-03	6.74e-04	4.41e-04	4.24e-03	2.11e-03	8.32e-02		3.31e-01
4	811554	816537	138678	139455	9.98e-03	5.92e-02	4.40e-03	2.16e-03	5.10e-04	0.00e+00	4.08e-03	6.28e-04	4.41e-04	4.27e-03	2.10e-03	8.77e-02		4.19e-01
5	808809	812588	139887	134904	1.00e-02	5.12e-02	4.64e-03	3.42e-03	3.68e-04	0.00e+00	4.03e-03	6.17e-04	4.32e-04	4.79e-03	2.11e-03	8.17e-02		5.01e-01
6	810015	810495	143438	139659	9.98e-03	5.59e-02	4.57e-03	2.03e-03	4.12e-04	0.00e+00	4.19e-03	7.33e-04	4.69e-04	4.35e-03	2.11e-03	8.48e-02		5.85e-01
7	812999	807476	143038	142558	1.00e-02	5.10e-02	4.56e-03	2.17e-03	3.83e-04	0.00e+00	4.14e-03	8.00e-04	4.57e-04	4.58e-03	2.12e-03	8.02e-02		6.66e-01
8	812197	808765	140860	146383	9.93e-03	5.56e-02	4.58e-03	2.43e-03	1.43e-03	0.00e+00	4.13e-03	6.05e-04	4.58e-04	4.31e-03	2.09e-03	8.56e-02		7.51e-01
9	814480	810185	142468	145900	9.95e-03	5.07e-02	4.60e-03	3.21e-03	1.46e-03	0.00e+00	3.99e-03	6.94e-04	4.76e-04	4.29e-03	2.10e-03	8.15e-02		8.33e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.5809 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.5809 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.580917  0.000000
];

