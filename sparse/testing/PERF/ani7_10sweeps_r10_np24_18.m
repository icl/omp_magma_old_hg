% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_24 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	819855	802135	141586	141586	3.65e-03	5.16e-02	9.99e-03	7.41e-03	1.02e-03	0.00e+00	1.05e-02	3.15e-04	1.09e-03	7.45e-03	6.19e-03	9.92e-02		9.92e-02
1	795420	808334	128363	146083	3.65e-03	6.40e-02	9.29e-03	3.01e-03	1.16e-03	0.00e+00	1.27e-02	2.60e-04	1.27e-03	6.02e-03	7.89e-03	1.09e-01		2.08e-01
2	794192	827555	153604	140690	3.69e-03	9.97e-02	1.19e-02	2.60e-03	9.82e-04	0.00e+00	1.56e-02	2.72e-04	1.30e-03	5.41e-03	8.81e-03	1.50e-01		3.59e-01
3	808902	807869	155638	122275	3.80e-03	1.49e-01	1.40e-02	3.70e-03	1.69e-03	0.00e+00	1.79e-02	3.58e-04	1.55e-03	5.37e-03	8.53e-03	2.06e-01		5.65e-01
4	798120	816293	141734	142767	3.71e-03	1.44e-01	1.41e-02	2.93e-03	1.94e-03	0.00e+00	2.01e-02	2.76e-04	1.64e-03	5.63e-03	7.99e-03	2.02e-01		7.67e-01
5	808002	811031	153321	135148	3.76e-03	1.38e-01	1.40e-02	3.70e-03	2.10e-03	0.00e+00	1.99e-02	3.72e-04	1.55e-03	5.60e-03	8.04e-03	1.97e-01		9.64e-01
6	811464	808582	144245	141216	3.76e-03	1.44e-01	1.42e-02	2.69e-03	1.73e-03	0.00e+00	1.99e-02	2.82e-04	1.55e-03	5.47e-03	8.01e-03	2.01e-01		1.16e+00
7	812046	812822	141589	144471	3.70e-03	1.44e-01	1.43e-02	3.27e-03	1.53e-03	0.00e+00	1.91e-02	3.76e-04	1.48e-03	5.37e-03	8.10e-03	2.02e-01		1.37e+00
8	813247	812999	141813	141037	3.74e-03	1.49e-01	1.44e-02	3.05e-03	2.07e-03	0.00e+00	2.01e-02	3.91e-04	1.52e-03	5.58e-03	8.08e-03	2.08e-01		1.57e+00
9	811391	813782	141418	141666	3.77e-03	1.48e-01	1.45e-02	2.77e-03	1.97e-03	0.00e+00	1.95e-02	4.70e-04	1.50e-03	5.40e-03	8.11e-03	2.06e-01		1.78e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 2.1852 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 2.1852 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  2.185155  0.000000
];

