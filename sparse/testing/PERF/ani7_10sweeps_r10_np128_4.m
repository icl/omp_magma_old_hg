% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_128 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	814052	815022	141586	141586	5.93e-03	2.74e-02	7.27e-03	1.10e-02	9.38e-04	0.00e+00	2.80e-03	3.97e-04	4.05e-04	8.36e-03	2.30e-03	6.68e-02		6.68e-02
1	818123	801718	134166	133196	8.31e-03	2.87e-02	4.58e-03	1.44e-03	4.11e-04	0.00e+00	3.45e-03	2.85e-04	4.38e-04	4.04e-03	2.27e-03	5.39e-02		1.21e-01
2	818611	811466	130901	147306	8.16e-03	3.76e-02	4.95e-03	1.21e-03	3.44e-04	0.00e+00	3.95e-03	3.09e-04	4.07e-04	3.37e-03	2.54e-03	6.29e-02		1.84e-01
3	812154	807003	131219	138364	8.24e-03	4.63e-02	5.19e-03	1.41e-03	3.21e-04	0.00e+00	4.72e-03	2.64e-04	4.65e-04	3.07e-03	2.34e-03	7.23e-02		2.56e-01
4	803709	809178	138482	143633	8.25e-03	4.71e-02	5.31e-03	2.19e-03	4.33e-04	0.00e+00	4.57e-03	4.71e-04	4.46e-04	2.99e-03	2.32e-03	7.40e-02		3.30e-01
5	808028	810793	147732	142263	8.24e-03	4.39e-02	6.26e-03	1.85e-03	4.17e-04	0.00e+00	5.44e-03	5.16e-04	4.62e-04	3.22e-03	2.27e-03	7.26e-02		4.02e-01
6	809026	810837	144219	141454	5.98e-03	4.66e-02	4.38e-03	1.82e-03	4.55e-04	0.00e+00	4.84e-03	4.47e-04	4.36e-04	3.00e-03	2.29e-03	7.03e-02		4.73e-01
7	810651	813559	144027	142216	5.96e-03	4.52e-02	4.40e-03	1.52e-03	3.85e-04	0.00e+00	4.85e-03	5.97e-04	4.35e-04	3.21e-03	2.33e-03	6.89e-02		5.42e-01
8	812676	811843	143208	140300	5.99e-03	4.83e-02	4.44e-03	1.41e-03	1.11e-03	0.00e+00	4.88e-03	7.34e-04	4.44e-04	2.97e-03	2.28e-03	7.26e-02		6.14e-01
9	810083	817460	141989	142822	5.98e-03	4.68e-02	4.48e-03	1.66e-03	6.17e-04	0.00e+00	4.93e-03	4.06e-04	4.68e-04	2.95e-03	2.30e-03	7.06e-02		6.85e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.1657 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.1657 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.165657  0.000000
];

