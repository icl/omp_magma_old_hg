% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_117 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	813682	820571	141586	141586	5.81e-03	2.81e-02	5.72e-03	1.04e-02	1.74e-03	0.00e+00	2.30e-03	3.60e-04	4.11e-04	7.10e-03	1.99e-03	6.39e-02		6.39e-02
1	802921	799593	134536	127647	5.92e-03	2.84e-02	3.40e-03	2.77e-03	1.55e-03	0.00e+00	2.80e-03	2.40e-04	4.16e-04	4.58e-03	2.23e-03	5.23e-02		1.16e-01
2	806046	795813	146103	149431	5.75e-03	3.97e-02	3.92e-03	1.89e-03	3.95e-04	0.00e+00	3.36e-03	4.81e-04	4.59e-04	3.08e-03	2.47e-03	6.16e-02		1.78e-01
3	806365	807478	143784	154017	5.75e-03	4.50e-02	4.37e-03	2.53e-03	3.71e-04	0.00e+00	3.69e-03	4.35e-04	5.12e-04	3.08e-03	2.43e-03	6.82e-02		2.46e-01
4	809258	809697	144271	143158	5.81e-03	4.59e-02	4.62e-03	1.78e-03	4.76e-04	0.00e+00	3.98e-03	3.27e-04	5.21e-04	3.24e-03	2.41e-03	6.91e-02		3.15e-01
5	810453	810907	142183	141744	5.84e-03	4.33e-02	4.70e-03	1.44e-03	4.18e-04	0.00e+00	4.04e-03	3.69e-04	4.91e-04	3.42e-03	2.45e-03	6.65e-02		3.82e-01
6	811845	809913	141794	141340	5.84e-03	4.60e-02	4.75e-03	1.68e-03	4.99e-04	0.00e+00	4.01e-03	3.72e-04	5.29e-04	3.37e-03	2.44e-03	6.94e-02		4.51e-01
7	811769	808207	141208	143140	5.83e-03	4.46e-02	4.77e-03	1.51e-03	4.68e-04	0.00e+00	3.88e-03	7.69e-04	4.99e-04	3.05e-03	2.44e-03	6.78e-02		5.19e-01
8	813302	809790	142090	145652	5.77e-03	4.61e-02	4.77e-03	2.96e-03	5.00e-04	0.00e+00	4.34e-03	5.63e-04	4.93e-04	3.29e-03	2.49e-03	7.13e-02		5.90e-01
9	808677	812480	141363	144875	5.83e-03	4.46e-02	4.82e-03	1.52e-03	1.03e-03	0.00e+00	3.89e-03	5.01e-04	5.13e-04	3.02e-03	2.50e-03	6.82e-02		6.58e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.1324 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.1324 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.132352  0.000000
];

