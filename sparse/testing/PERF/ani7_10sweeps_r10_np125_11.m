% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_125 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	811757	812068	141586	141586	5.89e-03	2.78e-02	5.74e-03	1.07e-02	1.57e-03	0.00e+00	2.94e-03	3.44e-04	3.58e-04	7.08e-03	1.81e-03	6.42e-02		6.42e-02
1	800712	816644	136461	136150	5.94e-03	2.77e-02	3.16e-03	4.41e-03	1.25e-03	0.00e+00	3.71e-03	3.42e-04	4.10e-04	4.27e-03	2.15e-03	5.33e-02		1.18e-01
2	801062	806087	148312	132380	5.97e-03	3.80e-02	3.76e-03	1.93e-03	7.19e-04	0.00e+00	4.11e-03	3.31e-04	4.09e-04	3.05e-03	2.32e-03	6.05e-02		1.78e-01
3	803867	806703	148768	143743	5.90e-03	4.45e-02	4.19e-03	2.58e-03	3.45e-04	0.00e+00	4.82e-03	3.51e-04	4.44e-04	3.01e-03	2.28e-03	6.84e-02		2.47e-01
4	808475	808638	146769	143933	5.90e-03	4.58e-02	4.27e-03	1.81e-03	6.18e-04	0.00e+00	4.96e-03	3.34e-04	4.88e-04	3.00e-03	2.27e-03	6.94e-02		3.16e-01
5	811669	812237	142966	142803	5.92e-03	4.49e-02	4.39e-03	3.58e-03	4.33e-04	0.00e+00	4.76e-03	4.03e-04	4.87e-04	3.54e-03	2.34e-03	7.07e-02		3.87e-01
6	806380	812431	140578	140010	5.94e-03	4.76e-02	4.48e-03	2.17e-03	3.78e-04	0.00e+00	4.78e-03	7.06e-04	4.42e-04	3.25e-03	2.31e-03	7.21e-02		4.59e-01
7	808320	812951	146673	140622	5.95e-03	4.72e-02	4.49e-03	2.39e-03	3.57e-04	0.00e+00	4.57e-03	3.96e-04	4.53e-04	3.00e-03	2.32e-03	7.11e-02		5.30e-01
8	811460	813184	145539	140908	5.93e-03	4.93e-02	4.51e-03	1.56e-03	4.40e-04	0.00e+00	4.97e-03	4.24e-04	4.78e-04	3.14e-03	2.32e-03	7.30e-02		6.03e-01
9	816335	812694	143205	141481	5.95e-03	4.78e-02	4.59e-03	1.85e-03	1.02e-03	0.00e+00	4.66e-03	4.69e-04	4.46e-04	2.96e-03	2.35e-03	7.22e-02		6.75e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.1537 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.1537 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.153743  0.000000
];

