% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_45 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	814209	803909	141586	141586	3.26e-03	3.18e-02	5.59e-03	6.68e-03	5.67e-04	0.00e+00	5.31e-03	2.65e-04	5.92e-04	5.70e-03	3.14e-03	6.29e-02		6.29e-02
1	810959	822368	134009	144309	3.24e-03	3.60e-02	4.66e-03	2.76e-03	9.06e-04	0.00e+00	6.72e-03	2.48e-04	6.53e-04	4.27e-03	4.12e-03	6.36e-02		1.26e-01
2	806727	808947	138065	126656	3.35e-03	5.79e-02	6.13e-03	1.53e-03	5.73e-04	0.00e+00	8.00e-03	2.86e-04	6.93e-04	3.70e-03	4.42e-03	8.66e-02		2.13e-01
3	798743	811181	143103	140883	3.29e-03	8.21e-02	6.96e-03	2.20e-03	6.76e-04	0.00e+00	9.29e-03	3.10e-04	8.00e-04	3.64e-03	4.15e-03	1.13e-01		3.27e-01
4	802157	817660	151893	139455	3.32e-03	7.83e-02	6.96e-03	3.15e-03	1.21e-03	0.00e+00	1.02e-02	4.64e-04	8.84e-04	3.92e-03	4.11e-03	1.12e-01		4.39e-01
5	809171	813783	149284	133781	3.35e-03	7.68e-02	7.17e-03	2.15e-03	1.12e-03	0.00e+00	1.01e-02	2.24e-04	8.66e-04	4.03e-03	4.13e-03	1.10e-01		5.49e-01
6	807878	809263	143076	138464	3.35e-03	7.97e-02	7.20e-03	2.65e-03	1.08e-03	0.00e+00	1.01e-02	2.50e-04	8.22e-04	3.79e-03	4.11e-03	1.13e-01		6.62e-01
7	811375	811369	145175	143790	3.33e-03	7.90e-02	7.17e-03	3.10e-03	9.27e-04	0.00e+00	9.95e-03	3.19e-04	8.34e-04	3.68e-03	4.13e-03	1.12e-01		7.74e-01
8	813729	810810	142484	142490	3.30e-03	8.16e-02	7.26e-03	1.83e-03	9.99e-04	0.00e+00	9.95e-03	3.21e-04	8.30e-04	3.83e-03	4.15e-03	1.14e-01		8.88e-01
9	811403	814882	140936	143855	3.32e-03	8.07e-02	7.28e-03	1.90e-03	1.37e-03	0.00e+00	1.02e-02	3.48e-04	8.51e-04	3.67e-03	4.16e-03	1.14e-01		1.00e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.3723 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.3723 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.372341  0.000000
];

