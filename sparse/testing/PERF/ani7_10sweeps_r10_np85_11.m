% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_85 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	813812	805642	141586	141586	5.35e-03	3.03e-02	6.51e-03	9.63e-03	6.07e-04	0.00e+00	3.11e-03	3.78e-04	4.88e-04	6.65e-03	2.42e-03	6.54e-02		6.54e-02
1	801352	799821	134406	142576	5.32e-03	2.76e-02	4.54e-03	4.04e-03	7.11e-04	0.00e+00	3.55e-03	3.03e-04	5.07e-04	3.83e-03	2.50e-03	5.29e-02		1.18e-01
2	812626	818040	147672	149203	5.29e-03	4.77e-02	5.05e-03	1.56e-03	4.23e-04	0.00e+00	4.32e-03	2.27e-04	4.53e-04	3.33e-03	2.71e-03	7.10e-02		1.89e-01
3	809237	809164	137204	131790	5.39e-03	5.82e-02	5.67e-03	1.80e-03	4.84e-04	0.00e+00	5.16e-03	3.31e-04	5.44e-04	3.27e-03	2.70e-03	8.36e-02		2.73e-01
4	807605	805276	141399	141472	5.32e-03	5.46e-02	5.89e-03	2.30e-03	5.84e-04	0.00e+00	6.24e-03	6.15e-04	5.41e-04	3.90e-03	2.78e-03	8.28e-02		3.56e-01
5	811233	813675	143836	146165	5.31e-03	5.14e-02	6.16e-03	1.58e-03	8.74e-04	0.00e+00	6.58e-03	2.98e-04	5.73e-04	3.56e-03	2.83e-03	7.92e-02		4.35e-01
6	811450	809996	141014	138572	5.36e-03	5.40e-02	6.30e-03	1.84e-03	5.04e-04	0.00e+00	6.29e-03	6.32e-04	5.60e-04	3.53e-03	2.79e-03	8.18e-02		5.17e-01
7	807161	811406	141603	143057	5.34e-03	5.17e-02	6.40e-03	1.72e-03	6.44e-04	0.00e+00	6.30e-03	4.62e-04	5.62e-04	3.39e-03	2.78e-03	7.93e-02		5.96e-01
8	815727	813063	146698	142453	5.35e-03	5.33e-02	6.27e-03	1.86e-03	9.81e-04	0.00e+00	5.41e-03	5.50e-04	5.62e-04	3.42e-03	2.79e-03	8.05e-02		6.76e-01
9	811037	814071	138938	141602	5.38e-03	5.24e-02	6.39e-03	1.30e-03	9.46e-04	0.00e+00	5.34e-03	4.53e-04	5.56e-04	3.29e-03	2.80e-03	7.89e-02		7.55e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.2303 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.2303 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.230322  0.000000
];

