% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_21 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	812427	797373	141586	141586	3.71e-03	5.71e-02	1.12e-02	7.62e-03	1.01e-03	0.00e+00	1.20e-02	2.74e-04	1.25e-03	7.75e-03	7.02e-03	1.09e-01		1.09e-01
1	786821	793393	135791	150845	3.67e-03	7.05e-02	1.05e-02	4.11e-03	1.25e-03	0.00e+00	1.46e-02	2.55e-04	1.48e-03	6.28e-03	8.85e-03	1.22e-01		2.30e-01
2	812079	821229	162203	155631	3.72e-03	1.11e-01	1.34e-02	2.80e-03	1.09e-03	0.00e+00	1.75e-02	2.66e-04	1.43e-03	5.82e-03	1.01e-02	1.67e-01		3.98e-01
3	808588	807056	137751	128601	3.83e-03	1.65e-01	1.60e-02	2.90e-03	1.35e-03	0.00e+00	2.09e-02	4.23e-04	1.66e-03	5.72e-03	9.34e-03	2.27e-01		6.25e-01
4	804181	810366	142048	143580	3.77e-03	1.50e-01	1.59e-02	3.45e-03	1.92e-03	0.00e+00	2.31e-02	4.23e-04	1.77e-03	6.08e-03	9.02e-03	2.16e-01		8.40e-01
5	809827	806913	147260	141075	3.79e-03	1.46e-01	1.58e-02	3.21e-03	2.01e-03	0.00e+00	2.30e-02	3.44e-04	1.70e-03	5.98e-03	9.07e-03	2.11e-01		1.05e+00
6	811487	814266	142420	145334	3.78e-03	1.51e-01	1.61e-02	3.02e-03	1.85e-03	0.00e+00	2.34e-02	5.99e-04	1.70e-03	5.91e-03	9.29e-03	2.16e-01		1.27e+00
7	807580	815209	141566	138787	3.80e-03	1.57e-01	1.63e-02	3.98e-03	1.67e-03	0.00e+00	2.24e-02	3.66e-04	1.64e-03	5.82e-03	9.29e-03	2.22e-01		1.49e+00
8	812336	814954	146279	138650	3.79e-03	1.57e-01	1.62e-02	3.25e-03	2.10e-03	0.00e+00	2.28e-02	3.27e-04	1.72e-03	6.07e-03	9.37e-03	2.23e-01		1.71e+00
9	810882	813461	142329	139711	3.83e-03	1.58e-01	1.64e-02	3.20e-03	2.05e-03	0.00e+00	2.31e-02	2.79e-04	1.73e-03	5.77e-03	9.28e-03	2.24e-01		1.94e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 2.3406 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 2.3406 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  2.340639  0.000000
];

