% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_141 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	814513	806900	141586	141586	7.40e-03	3.38e-02	6.81e-03	1.38e-02	9.98e-04	0.00e+00	2.75e-03	4.78e-04	4.20e-04	9.34e-03	2.28e-03	7.80e-02		7.80e-02
1	804279	813198	133705	141318	7.38e-03	3.06e-02	4.19e-03	4.11e-03	2.15e-03	0.00e+00	3.15e-03	4.33e-04	3.79e-04	5.54e-03	2.31e-03	6.02e-02		1.38e-01
2	801433	803632	144745	135826	7.43e-03	4.68e-02	4.46e-03	1.79e-03	3.99e-04	0.00e+00	3.67e-03	6.10e-04	4.57e-04	4.01e-03	2.28e-03	7.19e-02		2.10e-01
3	814316	810016	148397	146198	7.34e-03	4.88e-02	4.70e-03	2.77e-03	3.36e-04	0.00e+00	4.18e-03	4.80e-04	4.60e-04	4.03e-03	2.31e-03	7.54e-02		2.86e-01
4	807985	807671	136320	140620	7.40e-03	5.29e-02	5.12e-03	1.97e-03	3.65e-04	0.00e+00	4.32e-03	5.28e-04	4.88e-04	4.19e-03	2.22e-03	7.95e-02		3.65e-01
5	810260	808926	143456	143770	7.38e-03	4.57e-02	5.31e-03	2.21e-03	4.15e-04	0.00e+00	4.34e-03	6.04e-04	5.00e-04	4.40e-03	2.19e-03	7.30e-02		4.38e-01
6	811967	812027	141987	143321	7.42e-03	4.97e-02	5.24e-03	1.55e-03	6.04e-04	0.00e+00	4.43e-03	5.00e-04	4.68e-04	4.31e-03	2.18e-03	7.64e-02		5.14e-01
7	814626	810952	141086	141026	7.43e-03	4.84e-02	5.30e-03	4.03e-03	4.44e-04	0.00e+00	4.49e-03	4.99e-04	5.00e-04	4.09e-03	2.19e-03	7.74e-02		5.92e-01
8	812700	813595	139233	142907	7.41e-03	5.14e-02	5.53e-03	1.88e-03	3.95e-04	0.00e+00	4.42e-03	5.24e-04	4.72e-04	4.12e-03	2.18e-03	7.83e-02		6.70e-01
9	808069	805979	141965	141070	7.43e-03	4.90e-02	5.42e-03	2.90e-03	1.18e-03	0.00e+00	4.49e-03	7.20e-04	4.76e-04	4.06e-03	2.16e-03	7.78e-02		7.48e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.3781 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.3781 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.378067  0.000000
];

