% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_164 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	812806	802473	141586	141586	9.47e-03	5.31e-02	1.27e-02	1.87e-02	7.33e-04	0.00e+00	3.35e-03	4.83e-04	4.18e-04	1.36e-02	2.04e-03	1.15e-01		1.15e-01
1	808312	816817	135412	145745	9.41e-03	4.21e-02	3.88e-03	2.74e-03	8.15e-04	0.00e+00	3.96e-03	5.10e-04	4.00e-04	7.70e-03	2.66e-03	7.41e-02		1.89e-01
2	811035	809154	140712	132207	9.63e-03	6.52e-02	4.73e-03	1.91e-03	4.94e-04	0.00e+00	4.51e-03	5.24e-04	4.84e-04	4.27e-03	2.76e-03	9.45e-02		2.83e-01
3	806613	813263	138795	140676	9.57e-03	6.61e-02	5.25e-03	2.58e-03	5.21e-04	0.00e+00	5.02e-03	5.15e-04	5.36e-04	4.12e-03	2.72e-03	9.69e-02		3.80e-01
4	814064	808557	144023	137373	9.67e-03	7.05e-02	5.46e-03	1.74e-03	6.54e-04	0.00e+00	5.40e-03	4.83e-04	5.96e-04	5.19e-03	2.72e-03	1.02e-01		4.83e-01
5	807894	809642	137377	142884	9.50e-03	6.13e-02	5.30e-03	2.82e-03	1.17e-03	0.00e+00	5.37e-03	5.38e-04	5.48e-04	4.72e-03	2.66e-03	9.39e-02		5.77e-01
6	809588	810405	144353	142605	9.55e-03	6.85e-02	5.40e-03	2.09e-03	6.47e-04	0.00e+00	5.49e-03	6.26e-04	5.73e-04	4.54e-03	2.76e-03	1.00e-01		6.77e-01
7	811949	810777	143465	142648	9.61e-03	6.29e-02	5.36e-03	2.96e-03	5.79e-04	0.00e+00	5.34e-03	6.72e-04	5.60e-04	4.09e-03	2.72e-03	9.47e-02		7.71e-01
8	815781	813373	141910	143082	9.64e-03	7.06e-02	5.49e-03	2.58e-03	1.13e-03	0.00e+00	5.43e-03	6.06e-04	5.77e-04	4.39e-03	2.68e-03	1.03e-01		8.75e-01
9	815834	814475	138884	141292	9.64e-03	6.55e-02	5.80e-03	2.62e-03	1.45e-03	0.00e+00	5.44e-03	7.14e-04	5.50e-04	4.10e-03	2.74e-03	9.85e-02		9.73e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.6311 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.6311 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.631130  0.000000
];

