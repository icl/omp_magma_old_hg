% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_8 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	820000	803741	141586	141586	4.33e-03	1.30e-01	2.82e-02	1.01e-02	2.00e-03	0.00e+00	3.06e-02	2.81e-04	3.01e-03	1.31e-02	1.78e-02	2.39e-01		2.39e-01
1	827224	798713	128218	144477	4.39e-03	1.60e-01	2.75e-02	5.97e-03	2.87e-03	0.00e+00	3.74e-02	2.69e-04	3.32e-03	1.18e-02	2.29e-02	2.76e-01		5.15e-01
2	807469	810776	121800	150311	4.49e-03	2.66e-01	3.53e-02	6.35e-03	2.62e-03	0.00e+00	4.48e-02	2.03e-04	3.58e-03	1.14e-02	2.49e-02	4.00e-01		9.15e-01
3	804377	823207	142361	139054	4.54e-03	3.78e-01	3.89e-02	7.03e-03	3.26e-03	0.00e+00	5.17e-02	3.23e-04	3.95e-03	1.13e-02	2.45e-02	5.24e-01		1.44e+00
4	798114	803386	146259	127429	4.63e-03	3.75e-01	4.10e-02	7.30e-03	4.46e-03	0.00e+00	5.56e-02	2.43e-04	4.25e-03	1.14e-02	2.23e-02	5.26e-01		1.96e+00
5	806120	814520	153327	148055	4.43e-03	3.38e-01	3.95e-02	7.18e-03	5.14e-03	0.00e+00	5.64e-02	3.30e-04	4.18e-03	1.20e-02	2.32e-02	4.90e-01		2.45e+00
6	807718	807621	146127	137727	4.53e-03	3.56e-01	4.08e-02	7.69e-03	4.89e-03	0.00e+00	5.75e-02	5.07e-04	4.20e-03	1.13e-02	2.29e-02	5.11e-01		2.96e+00
7	811145	809033	145335	145432	4.49e-03	3.57e-01	4.08e-02	6.64e-03	4.03e-03	0.00e+00	5.47e-02	2.56e-04	4.02e-03	1.15e-02	2.32e-02	5.06e-01		3.47e+00
8	814235	811552	142714	144826	4.46e-03	3.62e-01	4.12e-02	7.03e-03	4.43e-03	0.00e+00	5.53e-02	3.97e-04	4.02e-03	1.17e-02	2.34e-02	5.14e-01		3.98e+00
9	811811	813880	140430	143113	4.55e-03	3.66e-01	4.15e-02	6.40e-03	4.02e-03	0.00e+00	5.21e-02	4.33e-04	3.79e-03	1.12e-02	2.34e-02	5.14e-01		4.50e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 4.9881 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 4.9881 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  4.988128  0.000000
];

