% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_256 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	818048	799206	141586	141586	1.12e-02	4.52e-02	9.35e-03	2.12e-02	1.89e-03	0.00e+00	2.14e-03	5.47e-04	3.37e-04	1.40e-02	1.61e-03	1.07e-01		1.07e-01
1	802809	807625	130170	149012	1.11e-02	3.65e-02	3.13e-03	2.01e-03	3.08e-03	0.00e+00	2.67e-03	4.54e-04	3.63e-04	7.82e-03	1.83e-03	6.89e-02		1.76e-01
2	802321	807477	146215	141399	1.12e-02	5.16e-02	3.62e-03	1.57e-03	2.92e-04	0.00e+00	2.98e-03	5.82e-04	3.87e-04	4.25e-03	2.03e-03	7.85e-02		2.55e-01
3	807489	809109	147509	142353	1.12e-02	5.15e-02	4.03e-03	3.83e-03	3.28e-04	0.00e+00	3.31e-03	5.23e-04	4.04e-04	4.26e-03	2.03e-03	8.14e-02		3.36e-01
4	813300	809842	143147	141527	1.12e-02	5.58e-02	4.16e-03	3.96e-03	4.60e-04	0.00e+00	3.53e-03	6.69e-04	4.26e-04	4.83e-03	2.03e-03	8.70e-02		4.23e-01
5	813053	809671	138141	141599	1.12e-02	5.35e-02	4.25e-03	1.73e-03	4.12e-04	0.00e+00	3.56e-03	6.18e-04	4.36e-04	5.03e-03	2.05e-03	8.28e-02		5.06e-01
6	811801	810089	139194	142576	1.12e-02	5.67e-02	4.28e-03	2.30e-03	6.04e-04	0.00e+00	3.57e-03	5.97e-04	4.50e-04	4.84e-03	2.02e-03	8.65e-02		5.93e-01
7	810731	811842	141252	142964	1.12e-02	5.30e-02	4.31e-03	1.88e-03	3.76e-04	0.00e+00	3.53e-03	7.39e-04	4.11e-04	4.19e-03	2.04e-03	8.17e-02		6.74e-01
8	812091	812216	143128	142017	1.12e-02	5.71e-02	4.36e-03	2.28e-03	4.11e-04	0.00e+00	3.43e-03	7.00e-04	4.00e-04	4.71e-03	2.07e-03	8.67e-02		7.61e-01
9	814794	813757	142574	142449	1.13e-02	5.37e-02	4.38e-03	2.13e-03	1.84e-03	0.00e+00	3.42e-03	8.13e-04	4.05e-04	4.22e-03	2.09e-03	8.43e-02		8.45e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.8174 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.8174 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.817404  0.000000
];

