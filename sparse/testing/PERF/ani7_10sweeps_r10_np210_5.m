% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_210 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	805810	804275	141586	141586	9.64e-03	3.99e-02	8.43e-03	1.85e-02	1.44e-03	0.00e+00	2.60e-03	5.61e-04	4.14e-04	1.22e-02	1.90e-03	9.56e-02		9.56e-02
1	798097	819542	142408	143943	9.60e-03	3.55e-02	3.63e-03	6.39e-03	1.23e-03	0.00e+00	3.12e-03	4.93e-04	3.58e-04	6.34e-03	2.18e-03	6.88e-02		1.64e-01
2	811691	795901	150927	129482	9.77e-03	5.51e-02	3.92e-03	2.48e-03	6.14e-04	0.00e+00	3.62e-03	6.50e-04	4.57e-04	4.72e-03	2.12e-03	8.35e-02		2.48e-01
3	814600	811925	138139	153929	9.46e-03	5.36e-02	4.10e-03	3.46e-03	4.09e-04	0.00e+00	3.94e-03	5.25e-04	4.39e-04	4.35e-03	2.18e-03	8.24e-02		3.30e-01
4	803381	808385	136036	138711	9.66e-03	5.81e-02	4.43e-03	3.19e-03	7.04e-04	0.00e+00	4.23e-03	7.79e-04	5.11e-04	4.23e-03	2.05e-03	8.79e-02		4.18e-01
5	812082	809792	148060	143056	9.61e-03	4.96e-02	4.49e-03	2.99e-03	4.52e-04	0.00e+00	4.23e-03	5.55e-04	4.84e-04	4.74e-03	2.07e-03	7.92e-02		4.97e-01
6	803276	810818	140165	142455	9.62e-03	5.63e-02	4.59e-03	4.21e-03	4.84e-04	0.00e+00	4.10e-03	4.79e-04	5.06e-04	4.34e-03	2.06e-03	8.67e-02		5.84e-01
7	815140	807543	149777	142235	9.68e-03	5.07e-02	4.51e-03	3.34e-03	4.46e-04	0.00e+00	4.23e-03	7.16e-04	4.52e-04	4.51e-03	2.13e-03	8.07e-02		6.65e-01
8	807688	812006	138719	146316	9.61e-03	5.74e-02	4.59e-03	3.27e-03	1.44e-03	0.00e+00	4.05e-03	9.19e-04	4.66e-04	4.28e-03	2.11e-03	8.81e-02		7.53e-01
9	812300	813268	146977	142659	9.72e-03	5.19e-02	4.54e-03	2.83e-03	9.85e-04	0.00e+00	4.22e-03	7.35e-04	4.69e-04	4.22e-03	2.14e-03	8.18e-02		8.35e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.5835 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.5835 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.583531  0.000000
];

