% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_184 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	811118	815703	141586	141586	1.06e-02	3.66e-02	7.57e-03	1.58e-02	9.02e-04	0.00e+00	2.17e-03	6.94e-04	3.77e-04	1.10e-02	1.79e-03	8.75e-02		8.75e-02
1	814229	799170	137100	132515	8.57e-03	3.14e-02	3.40e-03	2.65e-03	3.79e-04	0.00e+00	2.55e-03	3.77e-04	3.72e-04	5.62e-03	2.05e-03	5.74e-02		1.45e-01
2	817435	820141	134795	149854	8.44e-03	4.51e-02	3.93e-03	2.30e-03	1.17e-03	0.00e+00	2.91e-03	4.39e-04	4.47e-04	4.35e-03	2.28e-03	7.14e-02		2.16e-01
3	820301	813906	132395	129689	8.58e-03	5.13e-02	4.41e-03	2.40e-03	3.98e-04	0.00e+00	3.24e-03	5.67e-04	4.45e-04	3.90e-03	2.26e-03	7.75e-02		2.94e-01
4	809323	807857	130335	136730	8.55e-03	5.38e-02	4.59e-03	2.80e-03	3.51e-04	0.00e+00	3.49e-03	6.33e-04	4.63e-04	3.97e-03	2.20e-03	8.08e-02		3.75e-01
5	802842	814528	142118	143584	8.48e-03	4.57e-02	4.52e-03	1.60e-03	4.67e-04	0.00e+00	3.46e-03	6.76e-04	4.88e-04	4.19e-03	2.27e-03	7.18e-02		4.46e-01
6	807876	806197	149405	137719	8.58e-03	5.04e-02	4.62e-03	2.51e-03	5.22e-04	0.00e+00	3.54e-03	4.53e-04	4.66e-04	3.93e-03	2.23e-03	7.73e-02		5.24e-01
7	812187	810470	145177	146856	8.44e-03	4.55e-02	4.57e-03	2.60e-03	4.32e-04	0.00e+00	3.44e-03	5.94e-04	4.70e-04	4.13e-03	2.24e-03	7.25e-02		5.96e-01
8	812378	813306	141672	143389	8.53e-03	5.06e-02	4.69e-03	2.93e-03	1.57e-03	0.00e+00	3.58e-03	6.25e-04	4.62e-04	3.96e-03	2.24e-03	7.92e-02		6.75e-01
9	814990	815574	142287	141359	8.52e-03	4.73e-02	4.70e-03	2.12e-03	6.16e-04	0.00e+00	3.38e-03	1.08e-03	4.80e-04	3.91e-03	2.29e-03	7.44e-02		7.50e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.3949 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.3949 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.394895  0.000000
];

