% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_9 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	829759	798437	141586	141586	4.24e-03	1.15e-01	2.50e-02	9.60e-03	1.94e-03	0.00e+00	2.75e-02	2.93e-04	2.71e-03	1.18e-02	1.62e-02	2.14e-01		2.14e-01
1	811750	812390	118459	149781	4.25e-03	1.54e-01	2.46e-02	7.91e-03	2.57e-03	0.00e+00	3.47e-02	2.41e-04	3.05e-03	1.10e-02	2.06e-02	2.63e-01		4.78e-01
2	812593	811442	137274	136634	4.42e-03	2.38e-01	3.13e-02	6.06e-03	2.43e-03	0.00e+00	4.19e-02	3.10e-04	3.14e-03	1.07e-02	2.25e-02	3.61e-01		8.39e-01
3	799020	806292	137237	138388	4.37e-03	3.44e-01	3.55e-02	5.58e-03	2.91e-03	0.00e+00	4.66e-02	2.87e-04	3.62e-03	1.02e-02	2.04e-02	4.73e-01		1.31e+00
4	811254	810804	151616	144344	4.36e-03	3.01e-01	3.54e-02	6.36e-03	4.21e-03	0.00e+00	5.03e-02	3.11e-04	3.69e-03	1.03e-02	2.04e-02	4.36e-01		1.75e+00
5	809893	808449	140187	140637	4.37e-03	3.22e-01	3.67e-02	6.49e-03	4.13e-03	0.00e+00	5.15e-02	3.03e-04	3.74e-03	1.08e-02	2.05e-02	4.61e-01		2.21e+00
6	810314	815487	142354	143798	4.40e-03	3.26e-01	3.67e-02	6.45e-03	3.61e-03	0.00e+00	4.96e-02	2.81e-04	3.60e-03	1.04e-02	2.07e-02	4.62e-01		2.67e+00
7	810447	813954	142739	137566	4.41e-03	3.37e-01	3.71e-02	6.50e-03	3.98e-03	0.00e+00	5.11e-02	3.42e-04	3.73e-03	1.06e-02	2.07e-02	4.76e-01		3.15e+00
8	813997	813130	143412	139905	4.40e-03	3.41e-01	3.72e-02	6.16e-03	4.09e-03	0.00e+00	4.98e-02	3.15e-04	3.65e-03	1.03e-02	2.08e-02	4.78e-01		3.62e+00
9	811409	815478	140668	141535	4.40e-03	3.41e-01	3.74e-02	6.38e-03	3.87e-03	0.00e+00	4.96e-02	3.43e-04	3.60e-03	1.04e-02	2.08e-02	4.78e-01		4.10e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 4.5717 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 4.5717 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  4.571681  0.000000
];

