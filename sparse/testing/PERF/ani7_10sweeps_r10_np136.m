% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_136 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	816979	810078	141586	141586	6.11e-03	4.11e-02	1.01e-02	1.43e-02	4.12e-03	0.00e+00	2.90e-03	3.37e-04	3.36e-04	1.42e-02	1.71e-03	9.52e-02		9.52e-02
1	808744	784387	131239	138140	6.18e-03	3.49e-02	3.31e-03	4.30e-03	2.44e-03	0.00e+00	3.39e-03	2.60e-04	3.64e-04	4.74e-03	2.25e-03	6.22e-02		1.57e-01
2	815711	805790	140280	164637	5.89e-03	5.03e-02	3.59e-03	2.64e-03	9.19e-04	0.00e+00	3.92e-03	3.06e-04	4.46e-04	3.09e-03	2.38e-03	7.35e-02		2.31e-01
3	803043	806050	134119	144040	6.62e-03	5.66e-02	4.11e-03	1.66e-03	4.90e-04	0.00e+00	4.48e-03	4.02e-04	5.07e-04	3.02e-03	2.23e-03	8.01e-02		3.11e-01
4	814771	805334	147593	144586	6.34e-03	5.50e-02	4.07e-03	2.94e-03	5.70e-04	0.00e+00	4.81e-03	4.16e-04	5.17e-04	3.06e-03	2.17e-03	7.99e-02		3.91e-01
5	814306	808617	136670	146107	6.43e-03	5.33e-02	4.13e-03	1.70e-03	5.56e-04	0.00e+00	4.96e-03	3.39e-04	4.83e-04	3.71e-03	2.23e-03	7.78e-02		4.69e-01
6	812826	812775	137941	143630	6.01e-03	5.70e-02	4.21e-03	1.94e-03	5.38e-04	0.00e+00	4.77e-03	4.94e-04	4.96e-04	3.23e-03	2.25e-03	8.09e-02		5.50e-01
7	807733	807624	140227	140278	6.07e-03	5.49e-02	4.23e-03	1.71e-03	5.31e-04	0.00e+00	4.84e-03	3.87e-04	4.83e-04	3.28e-03	2.22e-03	7.87e-02		6.28e-01
8	813702	814542	146126	146235	5.99e-03	5.68e-02	4.17e-03	2.03e-03	1.39e-03	0.00e+00	4.92e-03	4.78e-04	4.78e-04	3.22e-03	2.41e-03	8.19e-02		7.10e-01
9	814326	813644	140963	140123	6.22e-03	5.50e-02	4.38e-03	2.31e-03	1.13e-03	0.00e+00	4.85e-03	4.01e-04	5.03e-04	3.04e-03	2.38e-03	8.02e-02		7.90e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.3219 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.3219 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.321945  0.000000
];

