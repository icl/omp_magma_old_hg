% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_162 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	818500	814535	141586	141586	8.48e-03	3.61e-02	7.54e-03	1.50e-02	6.94e-04	0.00e+00	2.37e-03	3.81e-04	4.08e-04	9.84e-03	2.05e-03	8.29e-02		8.29e-02
1	804698	802963	129718	133683	8.05e-03	3.22e-02	3.89e-03	3.90e-03	8.37e-04	0.00e+00	2.88e-03	3.49e-04	4.00e-04	5.58e-03	2.16e-03	6.03e-02		1.43e-01
2	787476	796805	144326	146061	7.91e-03	4.64e-02	4.19e-03	1.70e-03	4.61e-04	0.00e+00	3.19e-03	6.00e-04	4.15e-04	3.95e-03	2.21e-03	7.10e-02		2.14e-01
3	805473	811718	162354	153025	7.86e-03	4.42e-02	4.37e-03	2.63e-03	4.37e-04	0.00e+00	3.59e-03	5.69e-04	4.30e-04	3.92e-03	2.40e-03	7.04e-02		2.85e-01
4	804828	813040	145163	138918	7.98e-03	5.00e-02	4.88e-03	1.68e-03	4.39e-04	0.00e+00	3.91e-03	5.62e-04	4.91e-04	4.10e-03	2.40e-03	7.64e-02		3.61e-01
5	805524	816342	146613	138401	8.01e-03	4.64e-02	5.11e-03	4.20e-03	1.04e-03	0.00e+00	4.06e-03	3.71e-04	4.59e-04	4.34e-03	2.53e-03	7.65e-02		4.37e-01
6	812108	804644	146723	135905	8.04e-03	5.19e-02	5.19e-03	2.80e-03	3.89e-04	0.00e+00	3.84e-03	5.92e-04	4.42e-04	4.29e-03	2.43e-03	7.99e-02		5.17e-01
7	807397	812325	140945	148409	7.91e-03	4.66e-02	5.10e-03	1.97e-03	4.32e-04	0.00e+00	3.92e-03	6.31e-04	4.47e-04	4.06e-03	2.46e-03	7.35e-02		5.91e-01
8	813077	814757	146462	141534	8.01e-03	5.12e-02	5.25e-03	2.91e-03	1.14e-03	0.00e+00	4.00e-03	6.46e-04	4.43e-04	3.94e-03	2.52e-03	8.00e-02		6.71e-01
9	814159	812320	141588	139908	8.04e-03	4.89e-02	5.30e-03	1.76e-03	1.00e-03	0.00e+00	3.89e-03	7.61e-04	4.30e-04	3.89e-03	2.46e-03	7.65e-02		7.47e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.3822 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.3822 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.382199  0.000000
];

