% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_34 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	809277	806678	141586	141586	3.33e-03	3.76e-02	6.99e-03	6.81e-03	1.16e-03	0.00e+00	7.03e-03	3.48e-04	7.78e-04	6.34e-03	4.06e-03	7.44e-02		7.44e-02
1	801055	794964	138941	141540	3.31e-03	4.50e-02	6.16e-03	3.84e-03	1.28e-03	0.00e+00	8.50e-03	2.34e-04	8.62e-04	5.06e-03	5.22e-03	7.94e-02		1.54e-01
2	805308	813574	147969	154060	3.30e-03	6.95e-02	7.84e-03	2.49e-03	6.58e-04	0.00e+00	1.03e-02	2.18e-04	8.97e-04	4.24e-03	5.76e-03	1.05e-01		2.59e-01
3	814966	800908	144522	136256	3.37e-03	1.04e-01	9.22e-03	2.06e-03	8.33e-04	0.00e+00	1.19e-02	3.73e-04	1.00e-03	4.13e-03	5.58e-03	1.42e-01		4.01e-01
4	806033	802795	135670	149728	3.32e-03	1.01e-01	9.18e-03	2.62e-03	1.24e-03	0.00e+00	1.28e-02	3.44e-04	1.09e-03	4.36e-03	5.26e-03	1.41e-01		5.42e-01
5	809347	812268	145408	148646	3.33e-03	9.41e-02	9.17e-03	2.21e-03	1.20e-03	0.00e+00	1.30e-02	3.95e-04	1.04e-03	4.60e-03	5.36e-03	1.34e-01		6.76e-01
6	809380	808871	142900	139979	3.37e-03	9.71e-02	9.41e-03	2.02e-03	1.16e-03	0.00e+00	1.31e-02	4.48e-04	1.05e-03	4.37e-03	5.32e-03	1.37e-01		8.14e-01
7	810215	810252	143673	144182	3.35e-03	9.65e-02	9.38e-03	2.19e-03	1.26e-03	0.00e+00	1.31e-02	3.39e-04	1.06e-03	4.13e-03	5.40e-03	1.37e-01		9.50e-01
8	811633	809368	143644	143607	3.37e-03	9.81e-02	9.46e-03	2.72e-03	1.30e-03	0.00e+00	1.32e-02	3.74e-04	1.07e-03	4.46e-03	5.39e-03	1.39e-01		1.09e+00
9	812612	814451	143032	145297	3.39e-03	9.75e-02	9.50e-03	2.02e-03	1.58e-03	0.00e+00	1.28e-02	3.83e-04	1.02e-03	4.16e-03	5.40e-03	1.38e-01		1.23e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.6014 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.6014 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.601378  0.000000
];

