% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_135 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	815733	801896	141586	141586	8.16e-03	3.00e-02	5.80e-03	1.10e-02	7.02e-04	0.00e+00	2.67e-03	3.04e-04	3.93e-04	8.33e-03	1.70e-03	6.90e-02		6.90e-02
1	800659	802777	132485	146322	5.97e-03	2.64e-02	2.90e-03	2.50e-03	1.23e-03	0.00e+00	3.30e-03	3.09e-04	3.67e-04	4.85e-03	2.21e-03	5.01e-02		1.19e-01
2	813156	796388	148365	146247	5.98e-03	4.63e-02	3.69e-03	1.72e-03	2.71e-04	0.00e+00	3.84e-03	3.67e-04	4.46e-04	3.02e-03	2.29e-03	6.79e-02		1.87e-01
3	806908	807574	136674	153442	5.98e-03	5.01e-02	4.03e-03	2.51e-03	3.37e-04	0.00e+00	4.41e-03	3.84e-04	4.85e-04	2.96e-03	2.22e-03	7.34e-02		2.60e-01
4	811604	815164	143728	143062	6.02e-03	4.82e-02	4.13e-03	1.77e-03	5.44e-04	0.00e+00	4.71e-03	4.05e-04	5.32e-04	3.48e-03	2.22e-03	7.20e-02		3.32e-01
5	810234	806879	139837	136277	6.08e-03	4.49e-02	4.26e-03	2.72e-03	6.56e-04	0.00e+00	4.78e-03	4.11e-04	4.91e-04	3.49e-03	2.18e-03	7.00e-02		4.02e-01
6	808728	814859	142013	145368	6.11e-03	4.84e-02	4.26e-03	1.81e-03	4.79e-04	0.00e+00	4.67e-03	3.73e-04	4.84e-04	3.41e-03	2.23e-03	7.22e-02		4.75e-01
7	811158	811216	144325	138194	6.09e-03	4.59e-02	4.26e-03	1.86e-03	4.56e-04	0.00e+00	4.54e-03	4.16e-04	4.71e-04	3.02e-03	2.19e-03	6.92e-02		5.44e-01
8	813578	814337	142701	142643	6.07e-03	4.94e-02	4.30e-03	2.00e-03	7.30e-04	0.00e+00	4.72e-03	4.64e-04	5.05e-04	3.14e-03	2.21e-03	7.36e-02		6.17e-01
9	812540	812731	141087	140328	6.12e-03	4.71e-02	4.34e-03	1.42e-03	1.16e-03	0.00e+00	4.81e-03	5.44e-04	4.75e-04	3.06e-03	2.21e-03	7.12e-02		6.89e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.1844 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.1844 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.184371  0.000000
];

