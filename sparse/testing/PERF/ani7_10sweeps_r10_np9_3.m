% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_9 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	829761	798283	141586	141586	4.24e-03	1.15e-01	2.50e-02	9.69e-03	2.25e-03	0.00e+00	2.75e-02	2.94e-04	2.74e-03	1.16e-02	1.62e-02	2.15e-01		2.15e-01
1	804127	798782	118457	149935	4.24e-03	1.54e-01	2.46e-02	5.53e-03	2.61e-03	0.00e+00	3.40e-02	2.65e-04	3.11e-03	1.10e-02	2.04e-02	2.60e-01		4.74e-01
2	803127	795302	144897	150242	4.37e-03	2.37e-01	3.07e-02	5.17e-03	2.57e-03	0.00e+00	4.03e-02	2.81e-04	3.20e-03	1.03e-02	2.19e-02	3.56e-01		8.30e-01
3	817868	802354	146703	154528	4.30e-03	3.31e-01	3.42e-02	6.23e-03	3.10e-03	0.00e+00	4.65e-02	2.93e-04	3.58e-03	1.03e-02	2.10e-02	4.60e-01		1.29e+00
4	807102	808833	132768	148282	4.33e-03	3.21e-01	3.65e-02	7.13e-03	4.03e-03	0.00e+00	5.03e-02	4.48e-04	3.73e-03	1.03e-02	2.03e-02	4.58e-01		1.75e+00
5	810389	808983	144339	142608	4.35e-03	3.21e-01	3.60e-02	7.37e-03	3.90e-03	0.00e+00	4.97e-02	2.84e-04	3.63e-03	1.10e-02	2.04e-02	4.58e-01		2.21e+00
6	811282	813181	141858	143264	4.57e-03	3.27e-01	3.64e-02	6.70e-03	3.88e-03	0.00e+00	4.98e-02	4.17e-04	3.67e-03	1.04e-02	2.07e-02	4.64e-01		2.67e+00
7	810717	809489	141771	139872	4.40e-03	3.36e-01	3.71e-02	5.94e-03	3.68e-03	0.00e+00	4.97e-02	3.55e-04	3.66e-03	1.07e-02	2.06e-02	4.72e-01		3.14e+00
8	813394	813349	143142	144370	4.37e-03	3.38e-01	3.70e-02	6.44e-03	4.26e-03	0.00e+00	4.98e-02	3.73e-04	3.66e-03	1.06e-02	2.07e-02	4.76e-01		3.62e+00
9	815672	811665	141271	141316	4.40e-03	3.41e-01	3.74e-02	6.47e-03	4.07e-03	0.00e+00	4.93e-02	4.06e-04	3.59e-03	1.03e-02	2.08e-02	4.78e-01		4.10e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 4.5622 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 4.5622 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  4.562186  0.000000
];

