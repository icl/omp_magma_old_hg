% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_4 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	830119	810759	141586	141586	5.16e-03	2.47e-01	5.49e-02	1.49e-02	3.62e-03	0.00e+00	5.35e-02	2.34e-04	5.75e-03	2.14e-02	3.54e-02	4.42e-01		4.42e-01
1	808763	793892	118099	137459	5.46e-03	3.00e-01	5.52e-02	1.23e-02	4.73e-03	0.00e+00	6.29e-02	2.39e-04	6.05e-03	2.02e-02	4.45e-02	5.12e-01		9.54e-01
2	809384	801759	140261	155132	5.62e-03	5.14e-01	6.73e-02	1.07e-02	4.95e-03	0.00e+00	7.65e-02	3.19e-04	6.64e-03	2.04e-02	4.86e-02	7.55e-01		1.71e+00
3	811031	806745	140446	148071	5.61e-03	7.12e-01	7.61e-02	1.17e-02	6.49e-03	0.00e+00	8.96e-02	2.72e-04	7.73e-03	2.02e-02	4.63e-02	9.76e-01		2.68e+00
4	807372	798646	139605	143891	5.62e-03	6.83e-01	7.95e-02	1.24e-02	8.58e-03	0.00e+00	9.45e-02	2.36e-04	8.14e-03	2.03e-02	4.46e-02	9.57e-01		3.64e+00
5	810120	806909	144069	152795	5.58e-03	6.61e-01	7.89e-02	1.37e-02	9.82e-03	0.00e+00	9.61e-02	3.35e-04	8.17e-03	2.07e-02	4.55e-02	9.40e-01		4.58e+00
6	810852	809009	142127	145338	5.67e-03	6.85e-01	8.06e-02	1.31e-02	8.50e-03	0.00e+00	9.24e-02	4.25e-04	8.00e-03	2.01e-02	4.56e-02	9.60e-01		5.54e+00
7	813340	810328	142201	144044	5.64e-03	6.96e-01	8.16e-02	1.25e-02	8.36e-03	0.00e+00	9.24e-02	3.02e-04	7.99e-03	2.04e-02	4.61e-02	9.72e-01		6.51e+00
8	802052	815020	140519	143531	5.66e-03	7.11e-01	8.24e-02	1.20e-02	7.41e-03	0.00e+00	8.57e-02	3.08e-04	7.55e-03	2.02e-02	4.56e-02	9.78e-01		7.49e+00
9	810991	812692	152613	139645	5.73e-03	6.94e-01	8.15e-02	1.27e-02	8.96e-03	0.00e+00	9.38e-02	2.44e-04	8.01e-03	2.01e-02	4.60e-02	9.71e-01		8.46e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 9.0917 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 9.0917 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  9.091656  0.000000
];

