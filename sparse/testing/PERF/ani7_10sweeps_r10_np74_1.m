% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_74 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	818752	809946	141586	141586	5.08e-03	2.99e-02	6.29e-03	9.06e-03	5.64e-04	0.00e+00	3.52e-03	4.54e-04	5.38e-04	6.47e-03	2.72e-03	6.46e-02		6.46e-02
1	818361	791073	129466	138272	5.07e-03	2.90e-02	5.13e-03	3.96e-03	7.34e-04	0.00e+00	4.02e-03	2.61e-04	5.00e-04	4.43e-03	2.72e-03	5.58e-02		1.20e-01
2	808045	805192	130663	157951	4.93e-03	4.90e-02	5.37e-03	2.20e-03	4.38e-04	0.00e+00	4.85e-03	4.41e-04	4.92e-04	3.49e-03	2.94e-03	7.42e-02		1.95e-01
3	796481	812357	141785	144638	5.11e-03	5.81e-02	5.87e-03	2.50e-03	4.56e-04	0.00e+00	5.74e-03	3.53e-04	6.01e-04	3.38e-03	3.04e-03	8.52e-02		2.80e-01
4	806656	814910	154155	138279	5.08e-03	5.74e-02	6.30e-03	3.00e-03	7.34e-04	0.00e+00	6.25e-03	4.26e-04	6.42e-04	3.77e-03	3.14e-03	8.68e-02		3.67e-01
5	806639	805085	144785	136531	5.08e-03	5.29e-02	6.67e-03	3.26e-03	8.93e-04	0.00e+00	6.28e-03	5.00e-04	6.22e-04	3.77e-03	3.05e-03	8.30e-02		4.50e-01
6	814707	808654	145608	147162	5.04e-03	5.47e-02	6.48e-03	1.96e-03	7.22e-04	0.00e+00	6.25e-03	3.94e-04	6.07e-04	3.54e-03	3.14e-03	8.28e-02		5.32e-01
7	807310	810173	138346	144399	5.05e-03	5.49e-02	6.80e-03	1.23e-03	6.15e-04	0.00e+00	6.01e-03	4.00e-04	6.22e-04	3.39e-03	3.06e-03	8.21e-02		6.15e-01
8	814542	812381	146549	143686	5.06e-03	5.57e-02	6.57e-03	1.92e-03	7.46e-04	0.00e+00	6.16e-03	5.27e-04	6.29e-04	3.67e-03	3.13e-03	8.41e-02		6.99e-01
9	813945	814235	140123	142284	5.09e-03	5.65e-02	6.79e-03	1.95e-03	1.01e-03	0.00e+00	6.03e-03	6.54e-04	5.73e-04	3.46e-03	3.13e-03	8.52e-02		7.84e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.2579 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.2579 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.257934  0.000000
];

