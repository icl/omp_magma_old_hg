% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_219 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	828284	802351	141586	141586	1.01e-02	4.11e-02	9.04e-03	1.93e-02	1.98e-03	0.00e+00	2.48e-03	6.28e-04	3.71e-04	1.14e-02	1.84e-03	9.82e-02		9.82e-02
1	813202	792156	119934	145867	9.97e-03	3.56e-02	3.58e-03	3.95e-03	2.15e-03	0.00e+00	3.16e-03	4.63e-04	3.46e-04	6.50e-03	2.00e-03	6.77e-02		1.66e-01
2	801578	793470	135822	156868	9.78e-03	5.18e-02	3.74e-03	2.05e-03	1.37e-03	0.00e+00	3.51e-03	5.46e-04	4.39e-04	4.28e-03	2.03e-03	7.96e-02		2.45e-01
3	805514	814038	148252	156360	9.84e-03	5.12e-02	3.90e-03	1.76e-03	3.43e-04	0.00e+00	3.82e-03	4.22e-04	4.20e-04	4.22e-03	2.06e-03	7.80e-02		3.23e-01
4	811017	807976	145122	136598	1.02e-02	5.61e-02	5.24e-03	2.87e-03	4.45e-04	0.00e+00	3.96e-03	1.06e-03	4.84e-04	4.39e-03	2.05e-03	8.68e-02		4.10e-01
5	805121	809454	140424	143465	1.00e-02	4.94e-02	4.55e-03	4.32e-03	4.61e-04	0.00e+00	4.13e-03	4.48e-04	4.49e-04	4.75e-03	2.03e-03	8.05e-02		4.91e-01
6	807096	811237	147126	142793	1.00e-02	5.43e-02	4.42e-03	2.54e-03	4.98e-04	0.00e+00	4.05e-03	5.29e-04	4.61e-04	4.45e-03	2.08e-03	8.33e-02		5.74e-01
7	809772	813952	145957	141816	1.01e-02	5.08e-02	4.48e-03	2.75e-03	4.04e-04	0.00e+00	4.02e-03	5.50e-04	4.27e-04	4.77e-03	2.10e-03	8.05e-02		6.55e-01
8	812634	816894	144087	139907	1.01e-02	5.56e-02	4.50e-03	2.94e-03	1.29e-03	0.00e+00	4.03e-03	6.52e-04	4.69e-04	4.37e-03	2.10e-03	8.60e-02		7.41e-01
9	816708	811979	142031	137771	1.01e-02	5.20e-02	4.63e-03	3.63e-03	1.38e-03	0.00e+00	4.09e-03	8.92e-04	4.28e-04	4.35e-03	2.11e-03	8.37e-02		8.24e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.5790 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.5790 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.579011  0.000000
];

