% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_250 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	820174	808558	141586	141586	1.10e-02	4.45e-02	9.27e-03	1.92e-02	3.31e-03	0.00e+00	2.27e-03	5.85e-04	3.35e-04	1.44e-02	1.66e-03	1.06e-01		1.06e-01
1	803811	827641	128044	139660	1.11e-02	3.68e-02	3.20e-03	4.82e-03	2.89e-03	0.00e+00	2.72e-03	5.09e-04	4.07e-04	7.52e-03	1.91e-03	7.18e-02		1.78e-01
2	813259	828538	145213	121383	1.12e-02	5.16e-02	3.84e-03	3.21e-03	2.66e-04	0.00e+00	3.06e-03	5.19e-04	3.66e-04	4.30e-03	2.14e-03	8.06e-02		2.59e-01
3	810853	804873	136571	121292	1.12e-02	5.39e-02	4.86e-03	3.56e-03	3.64e-04	0.00e+00	3.34e-03	6.04e-04	3.98e-04	4.29e-03	2.01e-03	8.45e-02		3.43e-01
4	812005	807643	139783	145763	1.10e-02	5.53e-02	4.08e-03	2.68e-03	3.79e-04	0.00e+00	3.60e-03	8.39e-04	4.40e-04	4.39e-03	2.10e-03	8.47e-02		4.28e-01
5	807038	811593	139436	143798	1.10e-02	5.00e-02	4.19e-03	2.61e-03	4.38e-04	0.00e+00	3.58e-03	9.97e-04	4.29e-04	5.15e-03	2.06e-03	8.05e-02		5.09e-01
6	807952	811142	145209	140654	1.11e-02	5.46e-02	4.23e-03	1.59e-03	3.41e-04	0.00e+00	3.52e-03	7.75e-04	4.19e-04	4.63e-03	2.06e-03	8.33e-02		5.92e-01
7	808480	810893	145101	141911	1.11e-02	5.08e-02	4.21e-03	1.62e-03	3.58e-04	0.00e+00	3.64e-03	7.87e-04	4.00e-04	4.31e-03	2.05e-03	7.92e-02		6.71e-01
8	811263	813124	145379	142966	1.10e-02	5.52e-02	4.24e-03	2.14e-03	4.29e-04	0.00e+00	3.64e-03	5.83e-04	3.99e-04	4.53e-03	2.08e-03	8.42e-02		7.55e-01
9	813271	813758	143402	141541	1.11e-02	5.12e-02	4.33e-03	1.90e-03	1.61e-03	0.00e+00	3.73e-03	5.10e-04	4.64e-04	4.26e-03	2.09e-03	8.12e-02		8.36e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.5866 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.5866 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.586557  0.000000
];

