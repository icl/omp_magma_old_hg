% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_40 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	819455	800781	141586	141586	3.27e-03	3.35e-02	6.14e-03	6.69e-03	9.36e-04	0.00e+00	6.04e-03	2.41e-04	6.72e-04	5.95e-03	3.47e-03	6.69e-02		6.69e-02
1	802817	809584	128763	147437	3.24e-03	3.97e-02	5.24e-03	3.09e-03	1.23e-03	0.00e+00	7.30e-03	2.76e-04	7.22e-04	4.45e-03	4.56e-03	6.99e-02		1.37e-01
2	819134	814251	146207	139440	3.31e-03	6.23e-02	6.77e-03	3.21e-03	8.00e-04	0.00e+00	8.93e-03	2.43e-04	7.20e-04	3.85e-03	5.03e-03	9.51e-02		2.32e-01
3	807352	814122	130696	135579	3.33e-03	9.20e-02	8.06e-03	2.57e-03	8.44e-04	0.00e+00	1.06e-02	4.13e-04	9.05e-04	3.87e-03	4.71e-03	1.27e-01		3.59e-01
4	800762	824423	143284	136514	3.35e-03	8.77e-02	8.00e-03	2.35e-03	1.34e-03	0.00e+00	1.10e-02	4.13e-04	9.40e-04	3.88e-03	4.68e-03	1.24e-01		4.83e-01
5	806529	813495	150679	127018	3.40e-03	8.98e-02	8.14e-03	2.21e-03	9.42e-04	0.00e+00	1.09e-02	3.16e-04	9.06e-04	4.18e-03	4.62e-03	1.25e-01		6.08e-01
6	810574	807867	145718	138752	3.33e-03	8.80e-02	8.10e-03	2.23e-03	9.33e-04	0.00e+00	1.12e-02	2.44e-04	8.98e-04	6.29e-03	4.61e-03	1.26e-01		7.34e-01
7	810656	814063	142479	145186	3.32e-03	8.71e-02	8.08e-03	1.93e-03	1.03e-03	0.00e+00	1.10e-02	4.03e-04	9.15e-04	4.14e-03	4.61e-03	1.23e-01		8.57e-01
8	813224	809629	143203	139796	3.34e-03	8.96e-02	8.15e-03	2.73e-03	1.50e-03	0.00e+00	1.13e-02	3.14e-04	9.34e-04	3.89e-03	4.62e-03	1.26e-01		9.83e-01
9	810191	815789	141441	145036	3.37e-03	8.89e-02	8.19e-03	2.30e-03	1.40e-03	0.00e+00	1.13e-02	3.57e-04	9.03e-04	3.85e-03	4.64e-03	1.25e-01		1.11e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.4785 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.4785 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.478462  0.000000
];

