% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_86 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	813909	811296	141586	141586	5.35e-03	2.92e-02	6.47e-03	9.65e-03	7.06e-04	0.00e+00	3.09e-03	3.26e-04	4.78e-04	6.74e-03	2.47e-03	6.44e-02		6.44e-02
1	795066	821445	134309	136922	5.35e-03	2.88e-02	4.45e-03	2.56e-03	6.66e-04	0.00e+00	3.54e-03	2.81e-04	5.01e-04	4.04e-03	2.77e-03	5.29e-02		1.17e-01
2	808915	806013	153958	127579	5.39e-03	4.74e-02	5.05e-03	2.31e-03	5.78e-04	0.00e+00	4.31e-03	4.23e-04	5.22e-04	3.56e-03	2.94e-03	7.25e-02		1.90e-01
3	806001	805374	140915	143817	5.32e-03	5.30e-02	5.49e-03	2.09e-03	4.52e-04	0.00e+00	4.78e-03	3.96e-04	5.29e-04	3.29e-03	3.09e-03	7.84e-02		2.68e-01
4	811434	814144	144635	145262	5.29e-03	5.38e-02	5.74e-03	2.64e-03	5.48e-04	0.00e+00	5.20e-03	4.69e-04	5.52e-04	3.29e-03	3.13e-03	8.06e-02		3.49e-01
5	806734	810973	140007	137297	5.35e-03	4.82e-02	6.21e-03	1.89e-03	6.36e-04	0.00e+00	5.31e-03	3.73e-04	5.59e-04	3.59e-03	3.14e-03	7.52e-02		4.24e-01
6	813308	809559	145513	141274	5.34e-03	5.07e-02	6.12e-03	1.84e-03	6.06e-04	0.00e+00	5.27e-03	5.01e-04	5.71e-04	3.26e-03	3.15e-03	7.74e-02		5.02e-01
7	810503	811060	139745	143494	5.35e-03	4.95e-02	6.19e-03	3.33e-03	4.93e-04	0.00e+00	5.05e-03	4.33e-04	5.34e-04	3.71e-03	3.13e-03	7.78e-02		5.79e-01
8	812139	815940	143356	142799	5.39e-03	5.41e-02	6.21e-03	1.81e-03	8.85e-04	0.00e+00	5.55e-03	4.58e-04	5.51e-04	3.29e-03	3.18e-03	8.14e-02		6.61e-01
9	810541	814385	142526	138725	5.37e-03	5.13e-02	6.39e-03	1.94e-03	8.96e-04	0.00e+00	5.44e-03	6.06e-04	5.41e-04	3.22e-03	3.18e-03	7.89e-02		7.39e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.2133 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.2133 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.213283  0.000000
];

