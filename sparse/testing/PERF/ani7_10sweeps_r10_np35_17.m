% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_35 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	820477	809664	141586	141586	3.30e-03	3.68e-02	6.83e-03	6.69e-03	8.34e-04	0.00e+00	6.83e-03	2.34e-04	7.31e-04	6.33e-03	3.98e-03	7.26e-02		7.26e-02
1	805313	796344	127741	138554	3.35e-03	4.46e-02	6.02e-03	3.45e-03	1.27e-03	0.00e+00	8.37e-03	2.81e-04	8.04e-04	4.75e-03	5.07e-03	7.80e-02		1.51e-01
2	810042	807542	143711	152680	3.32e-03	6.82e-02	7.68e-03	3.07e-03	6.27e-04	0.00e+00	1.01e-02	2.96e-04	8.78e-04	4.15e-03	5.61e-03	1.04e-01		2.55e-01
3	804874	807682	139788	142288	3.36e-03	1.01e-01	8.78e-03	2.21e-03	8.22e-04	0.00e+00	1.16e-02	3.13e-04	9.72e-04	4.11e-03	5.30e-03	1.38e-01		3.93e-01
4	805284	803884	145762	142954	3.36e-03	9.33e-02	8.87e-03	3.31e-03	1.48e-03	0.00e+00	1.29e-02	4.04e-04	1.09e-03	4.48e-03	5.08e-03	1.34e-01		5.27e-01
5	806359	814409	146157	147557	3.31e-03	9.11e-02	8.90e-03	2.38e-03	1.14e-03	0.00e+00	1.27e-02	3.59e-04	9.97e-04	4.40e-03	5.25e-03	1.31e-01		6.57e-01
6	809177	811818	145888	137838	3.37e-03	9.53e-02	9.08e-03	1.95e-03	1.30e-03	0.00e+00	1.32e-02	2.75e-04	1.07e-03	4.23e-03	5.24e-03	1.35e-01		7.92e-01
7	813149	811829	143876	141235	3.37e-03	9.50e-02	9.14e-03	2.42e-03	1.06e-03	0.00e+00	1.26e-02	2.51e-04	9.96e-04	4.17e-03	5.26e-03	1.34e-01		9.27e-01
8	809950	810212	140710	142030	3.34e-03	9.75e-02	9.28e-03	1.90e-03	1.11e-03	0.00e+00	1.26e-02	3.01e-04	1.03e-03	4.17e-03	5.27e-03	1.36e-01		1.06e+00
9	812760	812576	144715	144453	3.38e-03	9.61e-02	9.20e-03	1.94e-03	1.75e-03	0.00e+00	1.29e-02	3.51e-04	1.03e-03	4.16e-03	5.26e-03	1.36e-01		1.20e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.5706 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.5706 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.570621  0.000000
];

