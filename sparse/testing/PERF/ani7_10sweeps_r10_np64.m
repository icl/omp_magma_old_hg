% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_64 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	821024	802274	141586	141586	3.25e-03	3.42e-02	6.81e-03	8.62e-03	7.11e-04	0.00e+00	3.78e-03	2.62e-04	5.07e-04	9.76e-03	2.32e-03	7.02e-02		7.02e-02
1	808787	825195	127194	145944	3.38e-03	3.34e-02	4.04e-03	1.78e-03	6.35e-04	0.00e+00	4.72e-03	2.28e-04	5.13e-04	3.82e-03	3.08e-03	5.56e-02		1.26e-01
2	812511	810146	140237	123829	3.36e-03	5.43e-02	4.41e-03	2.49e-03	5.48e-04	0.00e+00	5.68e-03	3.05e-04	5.44e-04	3.32e-03	3.24e-03	7.81e-02		2.04e-01
3	812986	811393	137319	139684	3.30e-03	6.72e-02	5.05e-03	3.21e-03	6.44e-04	0.00e+00	6.50e-03	2.65e-04	6.48e-04	3.23e-03	3.16e-03	9.32e-02		2.97e-01
4	815243	806996	137650	139243	3.54e-03	6.82e-02	5.04e-03	1.96e-03	9.74e-04	0.00e+00	7.25e-03	3.21e-04	6.88e-04	3.80e-03	2.95e-03	9.47e-02		3.92e-01
5	811164	809533	136198	144445	3.29e-03	6.16e-02	5.05e-03	2.09e-03	1.07e-03	0.00e+00	7.00e-03	3.82e-04	6.42e-04	3.56e-03	2.97e-03	8.76e-02		4.80e-01
6	806726	810053	141083	142714	3.94e-03	6.52e-02	5.05e-03	1.73e-03	6.72e-04	0.00e+00	7.20e-03	2.61e-04	6.85e-04	3.36e-03	2.95e-03	9.11e-02		5.71e-01
7	817263	811497	146327	143000	3.39e-03	6.29e-02	5.06e-03	2.09e-03	6.91e-04	0.00e+00	7.05e-03	4.31e-04	6.34e-04	3.50e-03	3.03e-03	8.87e-02		6.59e-01
8	812267	811783	136596	142362	3.32e-03	6.92e-02	5.18e-03	1.44e-03	1.17e-03	0.00e+00	7.21e-03	4.70e-04	6.40e-04	3.37e-03	3.00e-03	9.50e-02		7.54e-01
9	812633	813399	142398	142882	3.32e-03	6.45e-02	5.11e-03	1.56e-03	9.38e-04	0.00e+00	7.14e-03	4.11e-04	6.62e-04	3.30e-03	3.04e-03	9.00e-02		8.44e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.2346 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.2346 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.234554  0.000000
];

