% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_134 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	813429	806104	141586	141586	5.97e-03	2.74e-02	5.76e-03	1.11e-02	1.18e-03	0.00e+00	2.73e-03	3.28e-04	3.69e-04	6.81e-03	1.63e-03	6.33e-02		6.33e-02
1	800212	799389	134789	142114	6.01e-03	2.74e-02	2.94e-03	3.21e-03	1.34e-03	0.00e+00	3.33e-03	2.82e-04	3.56e-04	4.35e-03	2.16e-03	5.14e-02		1.15e-01
2	803457	825062	148812	149635	5.95e-03	4.41e-02	3.64e-03	2.52e-03	8.29e-04	0.00e+00	3.85e-03	2.88e-04	4.18e-04	3.10e-03	2.34e-03	6.71e-02		1.82e-01
3	814514	815479	146373	124768	6.13e-03	5.18e-02	4.17e-03	2.42e-03	4.40e-04	0.00e+00	4.30e-03	6.08e-04	5.03e-04	3.05e-03	2.35e-03	7.58e-02		2.58e-01
4	807362	812013	136122	135157	6.08e-03	5.32e-02	4.32e-03	1.64e-03	6.84e-04	0.00e+00	4.74e-03	5.13e-04	5.34e-04	3.07e-03	2.19e-03	7.69e-02		3.34e-01
5	806173	809681	144079	139428	6.05e-03	4.78e-02	4.20e-03	2.02e-03	5.48e-04	0.00e+00	4.78e-03	4.40e-04	4.91e-04	3.51e-03	2.20e-03	7.20e-02		4.06e-01
6	809310	813225	146074	142566	6.04e-03	5.01e-02	4.26e-03	2.19e-03	4.38e-04	0.00e+00	4.73e-03	4.67e-04	5.18e-04	3.17e-03	2.20e-03	7.41e-02		4.81e-01
7	810800	811905	143743	139828	6.08e-03	4.79e-02	4.30e-03	1.66e-03	4.72e-04	0.00e+00	4.66e-03	4.83e-04	4.80e-04	3.29e-03	2.21e-03	7.16e-02		5.52e-01
8	811634	811795	143059	141954	6.07e-03	5.01e-02	4.28e-03	1.73e-03	1.10e-03	0.00e+00	4.79e-03	4.02e-04	5.11e-04	3.07e-03	2.20e-03	7.43e-02		6.26e-01
9	811160	814065	143031	142870	6.07e-03	4.80e-02	4.31e-03	1.50e-03	1.14e-03	0.00e+00	4.67e-03	5.00e-04	4.79e-04	3.01e-03	2.21e-03	7.19e-02		6.98e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.1909 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.1909 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.190934  0.000000
];

