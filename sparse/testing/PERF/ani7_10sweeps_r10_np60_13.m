% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_60 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	815947	811626	141586	141586	3.23e-03	2.70e-02	4.76e-03	6.89e-03	6.96e-04	0.00e+00	4.06e-03	3.28e-04	4.50e-04	5.87e-03	2.37e-03	5.57e-02		5.57e-02
1	818387	810024	132271	136592	3.25e-03	3.02e-02	3.57e-03	1.63e-03	1.00e-03	0.00e+00	5.03e-03	2.82e-04	4.99e-04	3.94e-03	3.24e-03	5.27e-02		1.08e-01
2	799616	806396	130637	139000	3.29e-03	5.15e-02	4.68e-03	1.71e-03	6.00e-04	0.00e+00	5.99e-03	3.55e-04	5.76e-04	3.62e-03	3.32e-03	7.56e-02		1.84e-01
3	818147	806947	150214	143434	3.24e-03	6.46e-02	5.17e-03	1.61e-03	5.15e-04	0.00e+00	6.96e-03	2.38e-04	6.60e-04	3.29e-03	3.29e-03	8.96e-02		2.74e-01
4	813351	800062	132489	143689	3.26e-03	6.79e-02	5.43e-03	2.19e-03	6.24e-04	0.00e+00	7.17e-03	2.72e-04	6.46e-04	3.24e-03	3.05e-03	9.38e-02		3.67e-01
5	818290	811894	138090	151379	3.27e-03	5.99e-02	5.31e-03	1.96e-03	8.35e-04	0.00e+00	7.71e-03	2.84e-04	7.11e-04	3.47e-03	3.18e-03	8.67e-02		4.54e-01
6	812912	805451	133957	140353	3.29e-03	6.49e-02	5.47e-03	1.63e-03	7.75e-04	0.00e+00	7.67e-03	2.11e-04	7.32e-04	3.24e-03	3.13e-03	9.11e-02		5.45e-01
7	812862	808175	140141	147602	3.26e-03	6.29e-02	5.43e-03	2.24e-03	7.31e-04	0.00e+00	7.46e-03	3.30e-04	6.96e-04	3.47e-03	3.16e-03	8.97e-02		6.35e-01
8	814105	816982	140997	145684	3.26e-03	6.48e-02	5.47e-03	1.55e-03	1.50e-03	0.00e+00	7.54e-03	3.59e-04	7.03e-04	3.27e-03	3.18e-03	9.16e-02		7.26e-01
9	813550	809976	140560	137683	3.32e-03	6.42e-02	5.63e-03	1.54e-03	1.00e-03	0.00e+00	7.44e-03	4.43e-04	6.69e-04	3.28e-03	3.18e-03	9.07e-02		8.17e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.1866 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.1866 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.186586  0.000000
];

