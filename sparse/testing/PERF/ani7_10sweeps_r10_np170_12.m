% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_170 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	821975	815292	141586	141586	8.19e-03	3.60e-02	7.51e-03	1.50e-02	1.72e-03	0.00e+00	2.30e-03	4.12e-04	3.62e-04	9.34e-03	2.00e-03	8.29e-02		8.29e-02
1	795009	790961	126243	132926	8.29e-03	3.20e-02	3.73e-03	4.76e-03	1.94e-03	0.00e+00	2.82e-03	4.36e-04	3.99e-04	5.91e-03	2.04e-03	6.23e-02		1.45e-01
2	800825	819972	154015	158063	7.96e-03	4.58e-02	3.86e-03	2.24e-03	9.40e-04	0.00e+00	3.04e-03	5.75e-04	3.97e-04	3.83e-03	2.16e-03	7.08e-02		2.16e-01
3	795677	799366	149005	129858	8.24e-03	4.74e-02	4.41e-03	1.60e-03	3.08e-04	0.00e+00	3.59e-03	4.85e-04	4.56e-04	3.81e-03	2.34e-03	7.26e-02		2.89e-01
4	804021	800717	154959	151270	8.05e-03	4.68e-02	4.56e-03	2.09e-03	5.00e-04	0.00e+00	3.79e-03	4.88e-04	4.59e-04	3.96e-03	2.31e-03	7.30e-02		3.62e-01
5	809987	808983	147420	150724	8.06e-03	4.50e-02	4.82e-03	1.53e-03	4.52e-04	0.00e+00	3.83e-03	3.99e-04	4.49e-04	4.36e-03	2.34e-03	7.12e-02		4.33e-01
6	808519	812616	142260	143264	8.19e-03	5.11e-02	4.96e-03	3.04e-03	3.36e-04	0.00e+00	3.65e-03	8.86e-04	4.12e-04	4.00e-03	2.44e-03	7.91e-02		5.12e-01
7	810641	805928	144534	140437	8.22e-03	4.78e-02	4.97e-03	2.00e-03	3.51e-04	0.00e+00	3.63e-03	6.46e-04	4.46e-04	3.98e-03	2.33e-03	7.43e-02		5.86e-01
8	811769	811981	143218	147931	8.11e-03	4.98e-02	4.95e-03	2.12e-03	1.24e-03	0.00e+00	3.76e-03	6.64e-04	4.13e-04	3.99e-03	2.44e-03	7.75e-02		6.64e-01
9	813451	813274	142896	142684	8.18e-03	4.78e-02	5.05e-03	1.61e-03	1.14e-03	0.00e+00	3.71e-03	5.40e-04	4.14e-04	3.93e-03	2.42e-03	7.48e-02		7.38e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.3837 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.3837 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.383710  0.000000
];

