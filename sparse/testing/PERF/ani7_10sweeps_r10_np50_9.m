% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_50 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	818576	797072	141586	141586	3.25e-03	2.95e-02	5.26e-03	6.74e-03	8.56e-04	0.00e+00	4.83e-03	2.99e-04	5.27e-04	5.82e-03	2.82e-03	5.99e-02		5.99e-02
1	806406	810788	129642	151146	3.23e-03	3.43e-02	4.22e-03	2.85e-03	1.13e-03	0.00e+00	5.87e-03	2.15e-04	5.81e-04	4.10e-03	3.75e-03	6.03e-02		1.20e-01
2	819953	815320	142618	138236	3.30e-03	5.52e-02	5.45e-03	2.24e-03	6.96e-04	0.00e+00	7.14e-03	3.39e-04	6.11e-04	3.55e-03	4.18e-03	8.27e-02		2.03e-01
3	791527	807885	129877	134510	3.30e-03	7.97e-02	6.47e-03	2.04e-03	5.65e-04	0.00e+00	8.29e-03	3.19e-04	7.32e-04	3.50e-03	3.67e-03	1.09e-01		3.12e-01
4	811005	806134	159109	142751	3.27e-03	6.88e-02	6.19e-03	1.83e-03	9.23e-04	0.00e+00	8.97e-03	4.12e-04	7.84e-04	3.48e-03	3.66e-03	9.83e-02		4.10e-01
5	810988	810389	140436	145307	3.26e-03	6.90e-02	6.37e-03	1.93e-03	8.48e-04	0.00e+00	8.90e-03	3.63e-04	7.76e-04	3.81e-03	3.72e-03	9.90e-02		5.09e-01
6	810413	809270	141259	141858	3.28e-03	7.16e-02	6.46e-03	1.87e-03	8.39e-04	0.00e+00	8.96e-03	4.53e-04	7.42e-04	3.56e-03	3.69e-03	1.01e-01		6.10e-01
7	812412	814937	142640	143783	3.29e-03	7.09e-02	6.44e-03	1.98e-03	1.07e-03	0.00e+00	9.29e-03	3.19e-04	8.07e-04	3.83e-03	3.74e-03	1.02e-01		7.12e-01
8	812959	815697	141447	138922	3.32e-03	7.54e-02	6.52e-03	1.96e-03	1.27e-03	0.00e+00	8.87e-03	3.48e-04	7.57e-04	3.59e-03	3.72e-03	1.06e-01		8.18e-01
9	811915	814745	141706	138968	3.33e-03	7.44e-02	6.54e-03	1.50e-03	1.10e-03	0.00e+00	9.17e-03	4.22e-04	7.49e-04	3.47e-03	3.75e-03	1.04e-01		9.22e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.2899 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.2899 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.289937  0.000000
];

