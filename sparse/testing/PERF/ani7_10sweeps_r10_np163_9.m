% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_163 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	820867	812416	141586	141586	8.07e-03	3.61e-02	7.61e-03	1.48e-02	1.50e-03	0.00e+00	2.35e-03	4.66e-04	3.78e-04	8.71e-03	2.07e-03	8.20e-02		8.20e-02
1	811955	815929	127351	135802	8.11e-03	3.17e-02	3.88e-03	2.24e-03	1.30e-03	0.00e+00	3.02e-03	4.17e-04	4.09e-04	5.94e-03	2.20e-03	5.92e-02		1.41e-01
2	802248	806844	137069	133095	8.10e-03	4.60e-02	4.25e-03	1.94e-03	1.13e-03	0.00e+00	3.22e-03	5.67e-04	4.10e-04	3.93e-03	2.29e-03	7.18e-02		2.13e-01
3	805692	818638	147582	142986	8.01e-03	4.55e-02	4.50e-03	2.95e-03	3.97e-04	0.00e+00	3.72e-03	5.07e-04	4.30e-04	3.99e-03	2.39e-03	7.24e-02		2.85e-01
4	817858	801390	144944	131998	8.11e-03	5.07e-02	5.04e-03	2.88e-03	5.80e-04	0.00e+00	3.95e-03	4.11e-04	4.96e-04	4.08e-03	2.40e-03	7.86e-02		3.64e-01
5	810567	806955	133583	150051	7.93e-03	4.45e-02	6.31e-03	1.70e-03	4.18e-04	0.00e+00	3.88e-03	7.42e-04	4.39e-04	4.50e-03	2.41e-03	7.29e-02		4.37e-01
6	813939	816061	141680	145292	8.00e-03	4.97e-02	5.08e-03	2.61e-03	3.99e-04	0.00e+00	3.88e-03	8.71e-04	4.80e-04	4.21e-03	2.46e-03	7.77e-02		5.15e-01
7	810584	811401	139114	136992	8.17e-03	4.74e-02	5.25e-03	1.96e-03	4.41e-04	0.00e+00	3.92e-03	7.68e-04	4.68e-04	4.30e-03	2.48e-03	7.52e-02		5.90e-01
8	812100	810865	143275	142458	8.05e-03	5.08e-02	5.20e-03	1.57e-03	1.21e-03	0.00e+00	3.77e-03	4.22e-04	4.46e-04	4.11e-03	2.49e-03	7.81e-02		6.68e-01
9	813096	812542	142565	143800	8.05e-03	4.67e-02	5.21e-03	2.34e-03	1.01e-03	0.00e+00	3.82e-03	5.02e-04	4.42e-04	4.02e-03	2.54e-03	7.47e-02		7.43e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.3831 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.3831 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.383109  0.000000
];

