% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_99 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	826626	806894	141586	141586	5.61e-03	2.91e-02	6.01e-03	9.83e-03	1.31e-03	0.00e+00	2.74e-03	3.51e-04	4.26e-04	6.18e-03	2.25e-03	6.38e-02		6.38e-02
1	819321	800853	121592	141324	5.57e-03	2.94e-02	4.04e-03	2.26e-03	1.31e-03	0.00e+00	3.17e-03	2.79e-04	4.66e-04	4.28e-03	2.34e-03	5.31e-02		1.17e-01
2	810486	811708	129703	148171	5.51e-03	4.58e-02	4.40e-03	1.70e-03	7.03e-04	0.00e+00	3.73e-03	3.29e-04	4.99e-04	3.27e-03	2.60e-03	6.86e-02		1.85e-01
3	815934	824042	139344	138122	5.56e-03	5.23e-02	4.77e-03	1.94e-03	4.07e-04	0.00e+00	4.31e-03	4.28e-04	4.79e-04	3.16e-03	2.77e-03	7.61e-02		2.62e-01
4	812883	810525	134702	126594	5.66e-03	5.33e-02	5.29e-03	2.07e-03	4.65e-04	0.00e+00	4.50e-03	5.68e-04	4.83e-04	3.14e-03	2.64e-03	7.82e-02		3.40e-01
5	808029	812912	138558	140916	5.57e-03	4.63e-02	5.41e-03	2.09e-03	5.10e-04	0.00e+00	4.72e-03	4.23e-04	5.10e-04	3.46e-03	2.67e-03	7.17e-02		4.11e-01
6	810715	812177	144218	139335	5.62e-03	4.97e-02	5.45e-03	1.59e-03	4.97e-04	0.00e+00	4.64e-03	4.54e-04	5.29e-04	3.29e-03	2.69e-03	7.44e-02		4.86e-01
7	812974	813102	142338	140876	5.56e-03	4.80e-02	5.41e-03	3.03e-03	4.75e-04	0.00e+00	4.66e-03	4.93e-04	5.38e-04	3.49e-03	2.71e-03	7.44e-02		5.60e-01
8	812294	808923	140885	140757	5.59e-03	5.05e-02	5.46e-03	1.91e-03	1.16e-03	0.00e+00	4.63e-03	6.42e-04	4.85e-04	3.29e-03	2.68e-03	7.64e-02		6.37e-01
9	814957	812444	142371	145742	5.55e-03	4.76e-02	5.44e-03	1.89e-03	9.26e-04	0.00e+00	4.64e-03	4.02e-04	4.89e-04	3.28e-03	2.70e-03	7.29e-02		7.09e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.1956 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.1956 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.195609  0.000000
];

