% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_37 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	819292	810525	141586	141586	3.31e-03	3.54e-02	6.57e-03	6.59e-03	1.08e-03	0.00e+00	6.50e-03	2.86e-04	7.02e-04	6.09e-03	3.79e-03	7.03e-02		7.03e-02
1	808116	827024	128926	137693	3.30e-03	4.29e-02	5.70e-03	2.70e-03	1.19e-03	0.00e+00	8.05e-03	2.22e-04	7.75e-04	4.64e-03	5.75e-03	7.53e-02		1.46e-01
2	811880	812766	140908	122000	3.41e-03	6.57e-02	7.49e-03	2.00e-03	7.20e-04	0.00e+00	9.78e-03	4.83e-04	8.23e-04	4.02e-03	5.33e-03	9.97e-02		2.45e-01
3	815040	824168	137950	137064	3.35e-03	9.65e-02	8.54e-03	2.42e-03	7.93e-04	0.00e+00	1.11e-02	2.21e-04	9.14e-04	4.03e-03	5.37e-03	1.33e-01		3.79e-01
4	808775	815200	135596	126468	3.43e-03	9.93e-02	8.81e-03	3.14e-03	1.31e-03	0.00e+00	1.19e-02	4.09e-04	1.05e-03	3.99e-03	4.95e-03	1.38e-01		5.17e-01
5	818125	808638	142666	136241	3.36e-03	8.81e-02	8.65e-03	2.39e-03	1.05e-03	0.00e+00	1.19e-02	4.52e-04	9.75e-04	4.61e-03	5.06e-03	1.27e-01		6.43e-01
6	812405	811781	134122	143609	3.34e-03	9.38e-02	8.76e-03	2.00e-03	1.01e-03	0.00e+00	1.21e-02	2.99e-04	9.57e-04	4.10e-03	5.01e-03	1.31e-01		7.75e-01
7	800589	810989	140648	141272	3.35e-03	9.32e-02	8.75e-03	2.49e-03	9.97e-04	0.00e+00	1.20e-02	3.09e-04	9.58e-04	3.96e-03	4.92e-03	1.31e-01		9.06e-01
8	813968	814097	153270	142870	3.33e-03	9.19e-02	8.63e-03	2.15e-03	1.09e-03	0.00e+00	1.22e-02	3.40e-04	9.83e-04	4.23e-03	5.02e-03	1.30e-01		1.04e+00
9	814119	810644	140697	140568	3.36e-03	9.45e-02	8.85e-03	2.22e-03	1.56e-03	0.00e+00	1.23e-02	2.99e-04	9.89e-04	3.98e-03	4.98e-03	1.33e-01		1.17e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.5395 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.5395 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.539450  0.000000
];

