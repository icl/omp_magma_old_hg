% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_64 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	820747	802430	141586	141586	3.21e-03	2.70e-02	4.64e-03	6.94e-03	5.08e-04	0.00e+00	3.78e-03	2.61e-04	4.57e-04	5.96e-03	2.26e-03	5.50e-02		5.50e-02
1	827093	808802	127471	145788	3.22e-03	2.98e-02	3.36e-03	2.14e-03	8.40e-04	0.00e+00	4.58e-03	2.22e-04	5.24e-04	4.01e-03	3.08e-03	5.18e-02		1.07e-01
2	800808	807241	121931	140222	3.25e-03	5.08e-02	4.67e-03	1.50e-03	3.99e-04	0.00e+00	5.65e-03	2.84e-04	5.64e-04	3.23e-03	3.15e-03	7.35e-02		1.80e-01
3	810749	806809	149022	142589	3.28e-03	6.12e-02	4.75e-03	1.33e-03	6.46e-04	0.00e+00	6.49e-03	3.21e-04	6.48e-04	3.23e-03	3.05e-03	8.49e-02		2.65e-01
4	810640	804682	139887	143827	3.27e-03	6.42e-02	5.01e-03	1.61e-03	7.08e-04	0.00e+00	6.95e-03	3.56e-04	6.96e-04	3.50e-03	2.87e-03	8.92e-02		3.54e-01
5	809094	810139	140801	146759	3.25e-03	5.74e-02	4.99e-03	2.27e-03	1.02e-03	0.00e+00	7.12e-03	3.26e-04	6.77e-04	3.58e-03	2.93e-03	8.36e-02		4.38e-01
6	810346	808645	143153	142108	3.28e-03	6.01e-02	5.05e-03	1.87e-03	6.65e-04	0.00e+00	6.76e-03	4.03e-04	6.85e-04	3.32e-03	2.91e-03	8.50e-02		5.23e-01
7	811234	811073	142707	144408	3.29e-03	5.97e-02	5.10e-03	1.44e-03	6.91e-04	0.00e+00	6.94e-03	3.03e-04	6.74e-04	3.19e-03	2.97e-03	8.43e-02		6.07e-01
8	810339	811790	142625	142786	3.27e-03	6.15e-02	5.14e-03	1.71e-03	8.02e-04	0.00e+00	6.99e-03	4.63e-04	6.34e-04	3.39e-03	2.97e-03	8.68e-02		6.94e-01
9	812777	811860	144326	142875	3.31e-03	6.06e-02	5.13e-03	1.73e-03	1.18e-03	0.00e+00	7.15e-03	4.64e-04	6.75e-04	3.22e-03	2.96e-03	8.64e-02		7.80e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.1502 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.1502 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.150170  0.000000
];

