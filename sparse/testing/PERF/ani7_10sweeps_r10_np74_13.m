% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_74 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	818833	809942	141586	141586	5.05e-03	2.99e-02	6.32e-03	8.97e-03	4.24e-04	0.00e+00	3.58e-03	4.22e-04	5.69e-04	6.76e-03	2.73e-03	6.47e-02		6.47e-02
1	800558	800443	129385	138276	5.05e-03	2.88e-02	5.14e-03	3.58e-03	4.89e-04	0.00e+00	3.99e-03	3.45e-04	5.20e-04	4.67e-03	2.73e-03	5.54e-02		1.20e-01
2	814094	801415	148466	148581	4.99e-03	4.94e-02	5.36e-03	1.62e-03	4.31e-04	0.00e+00	4.81e-03	4.10e-04	4.90e-04	3.47e-03	2.93e-03	7.40e-02		1.94e-01
3	816598	803242	135736	148415	5.04e-03	5.85e-02	5.91e-03	1.62e-03	4.34e-04	0.00e+00	5.81e-03	5.08e-04	5.72e-04	3.46e-03	3.02e-03	8.49e-02		2.79e-01
4	812844	811261	134038	147394	5.02e-03	6.13e-02	6.31e-03	2.26e-03	6.06e-04	0.00e+00	6.11e-03	4.46e-04	6.02e-04	4.03e-03	3.15e-03	8.99e-02		3.69e-01
5	807671	807017	138597	140180	5.11e-03	5.23e-02	6.71e-03	2.64e-03	9.63e-04	0.00e+00	6.20e-03	4.26e-04	6.16e-04	3.57e-03	3.16e-03	8.17e-02		4.50e-01
6	814458	815888	144576	145230	5.10e-03	5.45e-02	6.56e-03	2.51e-03	7.25e-04	0.00e+00	6.36e-03	3.52e-04	5.99e-04	3.59e-03	3.16e-03	8.34e-02		5.34e-01
7	805248	811387	138595	137165	5.10e-03	5.61e-02	6.83e-03	1.58e-03	5.18e-04	0.00e+00	6.02e-03	5.53e-04	5.83e-04	3.38e-03	3.07e-03	8.37e-02		6.18e-01
8	812889	812114	148611	142472	5.08e-03	5.58e-02	6.56e-03	1.82e-03	9.66e-04	0.00e+00	6.23e-03	5.04e-04	6.09e-04	3.54e-03	3.12e-03	8.42e-02		7.02e-01
9	813163	812113	141776	142551	5.09e-03	5.61e-02	6.74e-03	1.64e-03	1.12e-03	0.00e+00	6.12e-03	4.47e-04	6.12e-04	3.41e-03	3.11e-03	8.44e-02		7.86e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.2591 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.2591 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.259100  0.000000
];

