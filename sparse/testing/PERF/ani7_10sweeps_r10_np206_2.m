% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_206 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	819225	813126	141586	141586	9.52e-03	4.06e-02	7.96e-03	1.85e-02	2.22e-03	0.00e+00	2.65e-03	5.74e-04	3.70e-04	1.16e-02	1.92e-03	9.59e-02		9.59e-02
1	808936	805330	128993	135092	9.51e-03	3.74e-02	3.77e-03	3.33e-03	2.06e-03	0.00e+00	3.45e-03	4.17e-04	3.78e-04	6.43e-03	2.20e-03	6.89e-02		1.65e-01
2	811286	795099	140088	143694	9.39e-03	5.52e-02	3.96e-03	2.12e-03	1.31e-03	0.00e+00	3.63e-03	7.80e-04	4.69e-04	4.27e-03	2.19e-03	8.33e-02		2.48e-01
3	812588	814098	138544	154731	9.32e-03	5.42e-02	4.19e-03	4.14e-03	4.00e-04	0.00e+00	4.04e-03	5.04e-04	4.48e-04	4.34e-03	2.23e-03	8.38e-02		3.32e-01
4	813868	799422	138048	136538	9.54e-03	5.89e-02	4.36e-03	2.09e-03	6.50e-04	0.00e+00	4.20e-03	6.50e-04	4.98e-04	6.03e-03	2.07e-03	8.90e-02		4.21e-01
5	801900	806473	137573	152019	9.35e-03	5.09e-02	4.21e-03	2.44e-03	5.37e-04	0.00e+00	4.35e-03	4.56e-04	5.02e-04	4.99e-03	2.09e-03	7.98e-02		5.01e-01
6	812143	810732	150347	145774	9.43e-03	5.50e-02	4.21e-03	2.78e-03	5.32e-04	0.00e+00	4.33e-03	4.35e-04	4.69e-04	4.52e-03	2.13e-03	8.38e-02		5.84e-01
7	812165	812976	140910	142321	9.52e-03	5.26e-02	4.29e-03	2.09e-03	4.70e-04	0.00e+00	4.28e-03	5.14e-04	4.69e-04	4.58e-03	2.16e-03	8.10e-02		6.65e-01
8	812767	812895	141694	140883	9.58e-03	5.66e-02	4.33e-03	1.73e-03	1.76e-03	0.00e+00	4.55e-03	7.00e-04	4.89e-04	4.41e-03	2.18e-03	8.63e-02		7.52e-01
9	813428	813326	141898	141770	9.52e-03	5.35e-02	4.33e-03	2.47e-03	1.57e-03	0.00e+00	4.38e-03	1.01e-03	4.75e-04	4.37e-03	2.18e-03	8.38e-02		8.36e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.5812 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.5812 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.581194  0.000000
];

