% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_77 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	811905	807060	141586	141586	5.18e-03	3.04e-02	6.74e-03	9.15e-03	4.20e-04	0.00e+00	3.41e-03	3.47e-04	5.56e-04	6.93e-03	2.60e-03	6.58e-02		6.58e-02
1	797254	799166	136313	141158	5.16e-03	2.85e-02	4.94e-03	2.34e-03	4.90e-04	0.00e+00	3.90e-03	2.93e-04	4.81e-04	4.69e-03	2.73e-03	5.35e-02		1.19e-01
2	818125	814471	151770	149858	5.09e-03	4.90e-02	5.22e-03	3.13e-03	4.25e-04	0.00e+00	4.59e-03	3.72e-04	4.75e-04	3.50e-03	2.94e-03	7.47e-02		1.94e-01
3	802442	803624	131705	135359	5.20e-03	6.44e-02	5.76e-03	2.28e-03	5.16e-04	0.00e+00	5.56e-03	8.24e-04	6.30e-04	3.41e-03	2.90e-03	9.14e-02		2.85e-01
4	806235	806676	148194	147012	5.12e-03	5.54e-02	6.04e-03	2.42e-03	6.99e-04	0.00e+00	5.95e-03	4.75e-04	6.08e-04	3.77e-03	3.00e-03	8.35e-02		3.69e-01
5	815304	810852	145206	144765	5.18e-03	5.38e-02	6.33e-03	2.75e-03	1.07e-03	0.00e+00	6.02e-03	5.84e-04	5.69e-04	3.61e-03	3.04e-03	8.29e-02		4.52e-01
6	812261	809659	136943	141395	5.20e-03	5.73e-02	6.42e-03	1.87e-03	5.66e-04	0.00e+00	5.93e-03	4.63e-04	5.71e-04	3.66e-03	2.99e-03	8.50e-02		5.37e-01
7	810545	810900	140792	143394	5.16e-03	5.45e-02	6.41e-03	2.13e-03	5.73e-04	0.00e+00	5.81e-03	4.69e-04	5.78e-04	3.37e-03	3.00e-03	8.20e-02		6.19e-01
8	813724	809958	143314	142959	5.20e-03	5.66e-02	6.37e-03	1.73e-03	9.30e-04	0.00e+00	5.95e-03	5.25e-04	5.98e-04	3.62e-03	3.01e-03	8.46e-02		7.03e-01
9	813228	811687	140941	144707	5.18e-03	5.52e-02	6.49e-03	2.95e-03	9.00e-04	0.00e+00	5.93e-03	4.97e-04	5.62e-04	3.41e-03	3.03e-03	8.41e-02		7.88e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.2602 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.2602 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.260172  0.000000
];

