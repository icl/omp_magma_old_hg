% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_38 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	809518	809129	141586	141586	3.29e-03	3.48e-02	6.37e-03	6.61e-03	1.04e-03	0.00e+00	6.34e-03	2.64e-04	6.95e-04	5.91e-03	3.64e-03	6.90e-02		6.90e-02
1	819697	797554	138700	139089	3.31e-03	4.12e-02	5.51e-03	3.23e-03	1.22e-03	0.00e+00	7.65e-03	2.67e-04	7.32e-04	4.54e-03	4.76e-03	7.24e-02		1.41e-01
2	804710	798376	129327	151470	3.28e-03	6.46e-02	7.18e-03	2.59e-03	8.57e-04	0.00e+00	9.40e-03	3.15e-04	8.18e-04	3.96e-03	5.15e-03	9.81e-02		2.40e-01
3	815020	811672	145120	151454	3.29e-03	9.33e-02	7.87e-03	2.14e-03	7.61e-04	0.00e+00	1.07e-02	3.04e-04	9.13e-04	3.94e-03	5.05e-03	1.28e-01		3.68e-01
4	809303	802321	135616	138964	3.35e-03	9.06e-02	8.46e-03	2.30e-03	1.26e-03	0.00e+00	1.19e-02	4.48e-04	1.03e-03	3.89e-03	4.78e-03	1.28e-01		4.96e-01
5	807897	804342	142138	149120	3.32e-03	8.51e-02	8.25e-03	2.96e-03	1.19e-03	0.00e+00	1.18e-02	2.54e-04	9.67e-04	4.45e-03	4.77e-03	1.23e-01		6.19e-01
6	806104	811103	144350	147905	3.31e-03	8.73e-02	8.34e-03	2.60e-03	1.04e-03	0.00e+00	1.18e-02	2.71e-04	9.39e-04	4.01e-03	4.87e-03	1.25e-01		7.43e-01
7	810614	809657	146949	141950	3.35e-03	9.11e-02	8.44e-03	2.30e-03	1.26e-03	0.00e+00	1.21e-02	2.94e-04	9.97e-04	4.24e-03	4.86e-03	1.29e-01		8.72e-01
8	812495	816405	143245	144202	3.34e-03	9.28e-02	8.50e-03	1.96e-03	1.55e-03	0.00e+00	1.19e-02	3.76e-04	9.55e-04	4.06e-03	4.91e-03	1.30e-01		1.00e+00
9	807925	811776	142170	138260	3.37e-03	9.28e-02	8.65e-03	1.77e-03	1.49e-03	0.00e+00	1.16e-02	2.95e-04	9.59e-04	4.76e-03	4.89e-03	1.31e-01		1.13e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.5041 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.5041 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.504066  0.000000
];

