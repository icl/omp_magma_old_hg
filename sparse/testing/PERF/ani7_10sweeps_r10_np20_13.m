% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_20 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	813260	811041	141586	141586	3.71e-03	5.91e-02	1.18e-02	7.55e-03	1.04e-03	0.00e+00	1.27e-02	2.67e-04	1.28e-03	7.48e-03	7.38e-03	1.12e-01		1.12e-01
1	805764	803218	134958	137177	3.76e-03	7.31e-02	1.12e-02	3.81e-03	1.19e-03	0.00e+00	1.54e-02	2.39e-04	1.41e-03	6.62e-03	9.42e-03	1.26e-01		2.38e-01
2	816980	810972	143260	145806	3.76e-03	1.17e-01	1.43e-02	2.95e-03	1.13e-03	0.00e+00	1.86e-02	1.67e-04	1.56e-03	5.98e-03	1.04e-02	1.75e-01		4.14e-01
3	817955	816553	132850	138858	3.79e-03	1.63e-01	1.64e-02	3.51e-03	1.48e-03	0.00e+00	2.20e-02	3.86e-04	1.66e-03	5.90e-03	1.00e-02	2.28e-01		6.42e-01
4	803660	798633	132681	134083	3.84e-03	1.61e-01	1.68e-02	3.69e-03	1.64e-03	0.00e+00	2.33e-02	4.58e-04	1.68e-03	6.35e-03	9.31e-03	2.28e-01		8.69e-01
5	813273	809257	147781	152808	3.74e-03	1.50e-01	1.63e-02	3.36e-03	2.00e-03	0.00e+00	2.30e-02	3.34e-04	1.66e-03	6.20e-03	9.62e-03	2.16e-01		1.09e+00
6	813712	806447	138974	142990	3.78e-03	1.58e-01	1.69e-02	3.24e-03	1.77e-03	0.00e+00	2.39e-02	3.59e-04	1.69e-03	5.98e-03	9.54e-03	2.25e-01		1.31e+00
7	814048	809635	139341	146606	3.78e-03	1.57e-01	1.70e-02	3.59e-03	1.92e-03	0.00e+00	2.38e-02	3.52e-04	1.74e-03	5.94e-03	9.64e-03	2.25e-01		1.53e+00
8	815105	813087	139811	144224	3.77e-03	1.59e-01	1.71e-02	3.64e-03	2.23e-03	0.00e+00	2.38e-02	3.90e-04	1.75e-03	6.05e-03	9.76e-03	2.28e-01		1.76e+00
9	814143	811153	139560	141578	3.82e-03	1.64e-01	1.73e-02	3.04e-03	1.99e-03	0.00e+00	2.32e-02	3.38e-04	1.70e-03	5.89e-03	9.79e-03	2.31e-01		1.99e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 2.4043 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 2.4043 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  2.404340  0.000000
];

