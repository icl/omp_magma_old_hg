% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_127 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	818571	802938	141586	141586	5.91e-03	2.80e-02	5.76e-03	1.08e-02	7.20e-04	0.00e+00	2.83e-03	4.30e-04	3.53e-04	7.65e-03	1.75e-03	6.42e-02		6.42e-02
1	807066	797099	129647	145280	5.88e-03	2.75e-02	3.09e-03	4.00e-03	1.13e-03	0.00e+00	3.49e-03	3.19e-04	4.10e-04	4.69e-03	2.09e-03	5.26e-02		1.17e-01
2	806628	813223	141958	151925	5.85e-03	3.72e-02	3.70e-03	1.90e-03	2.80e-04	0.00e+00	3.93e-03	2.90e-04	4.16e-04	3.05e-03	2.36e-03	5.90e-02		1.76e-01
3	807329	812640	143202	136607	5.92e-03	4.51e-02	4.22e-03	2.05e-03	3.30e-04	0.00e+00	4.77e-03	4.21e-04	4.85e-04	3.00e-03	2.30e-03	6.86e-02		2.44e-01
4	812284	813648	143307	137996	5.97e-03	4.68e-02	4.41e-03	1.86e-03	5.23e-04	0.00e+00	4.77e-03	5.42e-04	4.91e-04	3.42e-03	2.25e-03	7.11e-02		3.15e-01
5	808449	803201	139157	137793	5.99e-03	4.55e-02	4.40e-03	1.48e-03	6.39e-04	0.00e+00	4.63e-03	5.40e-04	4.66e-04	3.59e-03	2.24e-03	6.95e-02		3.85e-01
6	806581	806845	143798	149046	5.89e-03	4.77e-02	4.49e-03	1.74e-03	3.82e-04	0.00e+00	4.58e-03	4.89e-04	4.77e-04	3.29e-03	2.27e-03	7.13e-02		4.56e-01
7	812036	810781	146472	146208	5.95e-03	4.63e-02	4.37e-03	1.59e-03	5.08e-04	0.00e+00	4.90e-03	3.88e-04	5.07e-04	3.08e-03	2.28e-03	6.98e-02		5.26e-01
8	812421	813003	141823	143078	5.92e-03	4.93e-02	4.46e-03	1.97e-03	4.15e-04	0.00e+00	4.72e-03	4.61e-04	4.47e-04	3.12e-03	2.33e-03	7.32e-02		5.99e-01
9	813692	813439	142244	141662	6.01e-03	4.80e-02	4.52e-03	1.48e-03	9.42e-04	0.00e+00	4.96e-03	3.66e-04	4.60e-04	3.01e-03	2.33e-03	7.21e-02		6.71e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.1585 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.1585 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.158497  0.000000
];

