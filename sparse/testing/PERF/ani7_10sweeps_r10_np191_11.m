% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_191 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	831525	806441	141586	141586	8.68e-03	3.64e-02	7.48e-03	1.58e-02	1.77e-03	0.00e+00	2.76e-03	4.22e-04	3.40e-04	9.71e-03	1.81e-03	8.52e-02		8.52e-02
1	825836	813042	116693	141777	8.83e-03	3.28e-02	3.35e-03	4.51e-03	1.98e-03	0.00e+00	2.70e-03	4.08e-04	3.82e-04	6.10e-03	2.02e-03	6.31e-02		1.48e-01
2	793622	803847	123188	135982	8.74e-03	4.51e-02	4.00e-03	2.85e-03	9.56e-04	0.00e+00	3.40e-03	4.86e-04	4.08e-04	3.87e-03	2.18e-03	7.20e-02		2.20e-01
3	807902	801322	156208	145983	8.63e-03	4.70e-02	4.15e-03	2.14e-03	4.67e-04	0.00e+00	4.13e-03	5.22e-04	4.59e-04	3.92e-03	2.16e-03	7.35e-02		2.94e-01
4	800141	817225	142734	149314	8.62e-03	5.12e-02	4.31e-03	2.82e-03	4.15e-04	0.00e+00	4.27e-03	6.67e-04	4.53e-04	3.97e-03	2.23e-03	7.90e-02		3.73e-01
5	811314	807716	151300	134216	8.77e-03	4.85e-02	4.46e-03	4.33e-03	4.63e-04	0.00e+00	4.03e-03	6.35e-04	4.57e-04	4.38e-03	2.23e-03	7.83e-02		4.51e-01
6	812211	811835	140933	144531	8.73e-03	5.38e-02	4.48e-03	1.58e-03	4.11e-04	0.00e+00	4.40e-03	5.35e-04	4.62e-04	5.30e-03	2.21e-03	8.19e-02		5.33e-01
7	804656	811518	140842	141218	8.76e-03	5.08e-02	4.57e-03	1.66e-03	4.58e-04	0.00e+00	4.44e-03	3.74e-04	4.55e-04	4.02e-03	2.24e-03	7.78e-02		6.11e-01
8	816070	814313	149203	142341	8.75e-03	5.70e-02	4.57e-03	2.64e-03	1.27e-03	0.00e+00	4.45e-03	4.83e-04	4.52e-04	4.19e-03	2.25e-03	8.61e-02		6.97e-01
9	815035	814232	138595	140352	8.77e-03	5.19e-02	4.70e-03	3.03e-03	1.23e-03	0.00e+00	4.43e-03	5.54e-04	4.70e-04	3.98e-03	2.26e-03	8.13e-02		7.78e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.4013 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.4013 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.401315  0.000000
];

