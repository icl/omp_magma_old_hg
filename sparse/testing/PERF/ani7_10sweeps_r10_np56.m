% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_56 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	817251	803012	141586	141586	4.14e-03	3.55e-02	7.07e-03	8.00e-03	7.46e-04	0.00e+00	4.28e-03	2.76e-04	5.16e-04	9.38e-03	2.93e-03	7.28e-02		7.28e-02
1	808430	794693	130967	145206	3.84e-03	3.39e-02	3.97e-03	2.36e-03	1.50e-03	0.00e+00	5.21e-03	2.77e-04	5.46e-04	5.53e-03	3.48e-03	6.07e-02		1.33e-01
2	794780	802949	140594	154331	3.70e-03	5.58e-02	5.06e-03	1.82e-03	4.35e-04	0.00e+00	6.37e-03	3.90e-04	5.69e-04	3.37e-03	3.56e-03	8.11e-02		2.15e-01
3	815461	801299	155050	146881	3.98e-03	6.60e-02	5.72e-03	1.63e-03	5.92e-04	0.00e+00	7.31e-03	3.41e-04	6.87e-04	3.34e-03	3.50e-03	9.31e-02		3.08e-01
4	818833	802454	135175	149337	4.17e-03	7.23e-02	6.29e-03	1.60e-03	8.48e-04	0.00e+00	8.05e-03	2.68e-04	7.57e-04	3.78e-03	3.56e-03	1.02e-01		4.09e-01
5	808243	807195	132608	148987	4.02e-03	6.60e-02	5.73e-03	2.27e-03	8.41e-04	0.00e+00	8.00e-03	3.90e-04	6.96e-04	3.82e-03	3.54e-03	9.53e-02		5.05e-01
6	814772	813250	144004	145052	3.35e-03	7.01e-02	6.11e-03	1.65e-03	7.21e-04	0.00e+00	7.86e-03	3.28e-04	7.02e-04	3.55e-03	3.56e-03	9.79e-02		6.03e-01
7	815483	812218	138281	139803	3.47e-03	6.95e-02	6.42e-03	1.68e-03	7.59e-04	0.00e+00	7.97e-03	2.50e-04	6.84e-04	3.40e-03	3.38e-03	9.75e-02		7.00e-01
8	812037	813193	138376	141641	3.83e-03	7.26e-02	6.22e-03	1.60e-03	1.06e-03	0.00e+00	8.19e-03	3.19e-04	7.32e-04	3.51e-03	3.45e-03	1.02e-01		8.02e-01
9	812510	811595	142628	141472	3.72e-03	6.89e-02	6.16e-03	2.88e-03	1.42e-03	0.00e+00	8.21e-03	2.76e-04	6.86e-04	3.40e-03	3.39e-03	9.91e-02		9.01e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.2893 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.2893 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.289310  0.000000
];

