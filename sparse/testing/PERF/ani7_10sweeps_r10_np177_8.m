% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_177 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	822273	801956	141586	141586	1.06e-02	3.70e-02	7.76e-03	1.57e-02	9.33e-04	0.00e+00	2.34e-03	4.64e-04	3.69e-04	1.13e-02	2.11e-03	8.85e-02		8.85e-02
1	797872	811746	125945	146262	1.05e-02	3.10e-02	4.20e-03	5.75e-03	4.70e-04	0.00e+00	2.67e-03	4.43e-04	3.81e-04	5.78e-03	2.38e-03	6.36e-02		1.52e-01
2	804033	824431	151152	137278	1.05e-02	4.60e-02	3.97e-03	3.56e-03	4.20e-04	0.00e+00	2.98e-03	3.92e-04	4.12e-04	4.29e-03	2.25e-03	7.47e-02		2.27e-01
3	799807	815326	145797	125399	8.50e-03	4.86e-02	4.36e-03	4.31e-03	3.09e-04	0.00e+00	3.55e-03	5.12e-04	4.55e-04	3.89e-03	2.26e-03	7.68e-02		3.04e-01
4	809574	813968	150829	135310	8.45e-03	4.84e-02	4.55e-03	2.03e-03	3.92e-04	0.00e+00	3.58e-03	4.51e-04	4.08e-04	3.96e-03	2.35e-03	7.46e-02		3.78e-01
5	811639	810741	141867	137473	8.39e-03	4.61e-02	4.85e-03	1.64e-03	3.29e-04	0.00e+00	3.47e-03	4.22e-04	4.08e-04	4.32e-03	2.37e-03	7.23e-02		4.50e-01
6	805785	810921	140608	141506	8.41e-03	5.01e-02	4.79e-03	1.96e-03	3.28e-04	0.00e+00	3.47e-03	6.97e-04	3.99e-04	3.91e-03	2.29e-03	7.64e-02		5.27e-01
7	812588	812987	147268	142132	8.36e-03	4.52e-02	4.75e-03	2.14e-03	4.49e-04	0.00e+00	3.62e-03	5.19e-04	4.30e-04	3.98e-03	2.35e-03	7.18e-02		5.99e-01
8	810082	807746	141271	140872	8.40e-03	5.02e-02	4.80e-03	2.48e-03	1.05e-03	0.00e+00	3.47e-03	6.04e-04	4.08e-04	3.88e-03	2.29e-03	7.75e-02		6.76e-01
9	819025	813058	144583	146919	8.40e-03	4.47e-02	4.75e-03	2.95e-03	8.20e-04	0.00e+00	3.55e-03	5.79e-04	4.32e-04	3.85e-03	2.36e-03	7.24e-02		7.49e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.3954 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.3954 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.395399  0.000000
];

