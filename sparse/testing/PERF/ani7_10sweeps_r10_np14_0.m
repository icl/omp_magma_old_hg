% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_14 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	815296	806030	141586	141586	3.92e-03	7.88e-02	1.63e-02	8.34e-03	1.29e-03	0.00e+00	1.79e-02	2.91e-04	1.82e-03	8.79e-03	1.04e-02	1.48e-01		1.48e-01
1	814202	813309	132922	142188	3.92e-03	9.79e-02	1.58e-02	5.83e-03	1.53e-03	0.00e+00	2.20e-02	2.72e-04	1.95e-03	8.19e-03	1.36e-02	1.71e-01		3.19e-01
2	808067	805270	134822	135715	4.07e-03	1.63e-01	2.06e-02	4.98e-03	1.54e-03	0.00e+00	2.71e-02	2.35e-04	2.21e-03	7.57e-03	1.44e-02	2.46e-01		5.65e-01
3	817164	809671	141763	144560	3.98e-03	2.29e-01	2.29e-02	5.02e-03	1.95e-03	0.00e+00	3.07e-02	2.90e-04	2.34e-03	7.46e-03	1.39e-02	3.17e-01		8.82e-01
4	804375	813390	133472	140965	4.02e-03	2.22e-01	2.36e-02	4.22e-03	2.50e-03	0.00e+00	3.31e-02	4.14e-04	2.49e-03	7.82e-03	1.36e-02	3.13e-01		1.20e+00
5	803180	813711	147066	138051	4.02e-03	2.09e-01	2.36e-02	4.72e-03	3.43e-03	0.00e+00	3.40e-02	3.13e-04	2.55e-03	7.91e-03	1.36e-02	3.03e-01		1.50e+00
6	808920	809976	149067	138536	4.00e-03	2.14e-01	2.36e-02	4.71e-03	2.47e-03	0.00e+00	3.20e-02	3.07e-04	2.39e-03	7.47e-03	1.36e-02	3.05e-01		1.80e+00
7	809861	817827	144133	143077	4.00e-03	2.17e-01	2.39e-02	4.63e-03	2.80e-03	0.00e+00	3.36e-02	2.93e-04	2.50e-03	7.77e-03	1.38e-02	3.10e-01		2.11e+00
8	812966	809215	143998	136032	4.04e-03	2.21e-01	2.43e-02	4.06e-03	2.72e-03	0.00e+00	3.29e-02	3.36e-04	2.40e-03	7.65e-03	1.36e-02	3.13e-01		2.43e+00
9	812745	814393	141699	145450	4.06e-03	2.21e-01	2.41e-02	3.99e-03	2.73e-03	0.00e+00	3.27e-02	3.34e-04	2.41e-03	7.51e-03	1.38e-02	3.12e-01		2.74e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 3.1807 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 3.1807 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  3.180721  0.000000
];

