% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_49 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	808980	810765	141586	141586	3.27e-03	2.98e-02	5.32e-03	6.73e-03	7.88e-04	0.00e+00	4.95e-03	2.85e-04	5.33e-04	5.96e-03	2.87e-03	6.05e-02		6.05e-02
1	803524	830469	139238	137453	3.25e-03	3.44e-02	4.32e-03	3.89e-03	7.69e-04	0.00e+00	5.96e-03	2.15e-04	5.78e-04	4.11e-03	3.85e-03	6.14e-02		1.22e-01
2	819652	810557	145500	118555	3.38e-03	5.59e-02	5.66e-03	2.04e-03	5.16e-04	0.00e+00	7.35e-03	3.52e-04	6.27e-04	3.91e-03	4.15e-03	8.39e-02		2.06e-01
3	817334	800268	130178	139273	3.31e-03	7.88e-02	6.45e-03	1.88e-03	6.19e-04	0.00e+00	8.51e-03	2.65e-04	7.46e-04	3.56e-03	3.89e-03	1.08e-01		3.14e-01
4	802313	813789	133302	150368	3.26e-03	7.32e-02	6.46e-03	2.71e-03	1.06e-03	0.00e+00	9.32e-03	3.70e-04	8.44e-04	3.55e-03	3.73e-03	1.05e-01		4.18e-01
5	809714	814552	149128	137652	3.32e-03	7.12e-02	6.50e-03	3.49e-03	8.13e-04	0.00e+00	9.02e-03	3.08e-04	7.47e-04	4.00e-03	3.78e-03	1.03e-01		5.22e-01
6	809920	812346	142533	137695	3.32e-03	7.33e-02	6.57e-03	1.77e-03	9.03e-04	0.00e+00	9.35e-03	3.00e-04	7.91e-04	3.57e-03	3.78e-03	1.04e-01		6.25e-01
7	815836	815103	143133	140707	3.35e-03	7.32e-02	6.61e-03	1.91e-03	8.13e-04	0.00e+00	9.13e-03	2.58e-04	7.75e-04	3.67e-03	3.90e-03	1.04e-01		7.29e-01
8	805426	809635	138023	138756	3.34e-03	7.67e-02	6.72e-03	1.86e-03	1.09e-03	0.00e+00	9.22e-03	2.82e-04	7.76e-04	3.63e-03	3.80e-03	1.07e-01		8.36e-01
9	817906	814770	149239	145030	3.31e-03	7.32e-02	6.58e-03	1.91e-03	1.27e-03	0.00e+00	9.35e-03	3.64e-04	7.68e-04	3.55e-03	3.87e-03	1.04e-01		9.40e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.3083 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.3083 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.308287  0.000000
];

