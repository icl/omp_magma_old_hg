% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_218 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	818499	809319	141586	141586	9.93e-03	4.25e-02	9.13e-03	1.92e-02	1.08e-03	0.00e+00	2.47e-03	4.98e-04	3.67e-04	1.30e-02	1.83e-03	9.99e-02		9.99e-02
1	829134	805133	129719	138899	9.93e-03	3.53e-02	3.60e-03	7.16e-03	1.79e-03	0.00e+00	3.17e-03	4.63e-04	3.38e-04	7.46e-03	2.10e-03	7.13e-02		1.71e-01
2	823593	814929	119890	143891	9.87e-03	5.30e-02	4.28e-03	2.07e-03	3.85e-04	0.00e+00	3.59e-03	6.01e-04	4.15e-04	4.39e-03	2.14e-03	8.07e-02		2.52e-01
3	806256	813602	126237	134901	9.99e-03	5.58e-02	4.15e-03	3.97e-03	3.01e-04	0.00e+00	3.84e-03	4.13e-04	4.60e-04	4.28e-03	2.10e-03	8.53e-02		3.37e-01
4	805658	823073	144380	137034	9.98e-03	5.64e-02	4.36e-03	2.81e-03	5.26e-04	0.00e+00	4.05e-03	8.08e-04	4.41e-04	4.73e-03	2.09e-03	8.62e-02		4.24e-01
5	812479	809394	145783	128368	1.01e-02	5.19e-02	4.62e-03	2.99e-03	4.67e-04	0.00e+00	4.15e-03	6.85e-04	4.85e-04	5.10e-03	2.09e-03	8.26e-02		5.06e-01
6	810722	810532	139768	142853	9.92e-03	5.57e-02	4.56e-03	1.97e-03	4.67e-04	0.00e+00	4.05e-03	6.48e-04	4.64e-04	4.81e-03	2.09e-03	8.46e-02		5.91e-01
7	810774	811288	142331	142521	9.95e-03	5.11e-02	4.53e-03	3.17e-03	3.86e-04	0.00e+00	3.97e-03	9.48e-04	4.31e-04	4.25e-03	2.06e-03	8.08e-02		6.72e-01
8	810951	812085	143085	142571	9.92e-03	5.54e-02	4.56e-03	2.19e-03	6.13e-04	0.00e+00	4.09e-03	7.16e-04	4.65e-04	4.63e-03	2.06e-03	8.47e-02		7.56e-01
9	811926	813710	143714	142580	9.95e-03	5.12e-02	4.53e-03	2.35e-03	1.57e-03	0.00e+00	4.22e-03	6.81e-04	4.65e-04	4.24e-03	2.09e-03	8.13e-02		8.37e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.5836 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.5836 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.583580  0.000000
];

