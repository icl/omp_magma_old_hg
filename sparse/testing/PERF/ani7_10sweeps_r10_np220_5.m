% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_220 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	825995	806833	141586	141586	9.99e-03	4.21e-02	9.06e-03	1.91e-02	2.54e-03	0.00e+00	2.44e-03	5.23e-04	3.74e-04	1.23e-02	1.77e-03	1.00e-01		1.00e-01
1	809704	799968	122223	141385	9.97e-03	3.53e-02	3.56e-03	7.35e-03	2.34e-03	0.00e+00	3.10e-03	4.61e-04	3.45e-04	6.83e-03	2.02e-03	7.12e-02		1.72e-01
2	809941	808108	139320	149056	9.87e-03	5.23e-02	3.78e-03	4.43e-03	1.32e-03	0.00e+00	3.39e-03	6.04e-04	3.92e-04	4.22e-03	2.04e-03	8.24e-02		2.54e-01
3	804255	798087	139889	141722	9.97e-03	5.33e-02	4.30e-03	2.96e-03	3.19e-04	0.00e+00	3.86e-03	4.96e-04	4.33e-04	4.16e-03	1.98e-03	8.17e-02		3.36e-01
4	802282	817922	146381	152549	9.85e-03	5.42e-02	4.23e-03	1.98e-03	6.22e-04	0.00e+00	4.00e-03	7.28e-04	4.77e-04	4.27e-03	2.08e-03	8.25e-02		4.18e-01
5	810595	808993	149159	133519	1.01e-02	4.99e-02	4.52e-03	3.09e-03	4.16e-04	0.00e+00	4.02e-03	5.44e-04	4.31e-04	4.75e-03	2.05e-03	7.98e-02		4.98e-01
6	804390	815329	141652	143254	9.97e-03	5.60e-02	4.49e-03	2.29e-03	4.23e-04	0.00e+00	4.01e-03	6.36e-04	4.27e-04	4.44e-03	2.04e-03	8.48e-02		5.83e-01
7	805588	811917	148663	137724	1.00e-02	5.04e-02	4.42e-03	1.65e-03	4.82e-04	0.00e+00	4.11e-03	9.00e-04	4.34e-04	4.32e-03	2.05e-03	7.88e-02		6.62e-01
8	811091	811826	148271	141942	9.99e-03	5.61e-02	4.37e-03	2.80e-03	4.68e-04	0.00e+00	4.10e-03	8.83e-04	4.29e-04	4.37e-03	2.05e-03	8.55e-02		7.47e-01
9	810049	815157	143574	142839	9.98e-03	5.14e-02	4.52e-03	2.27e-03	1.36e-03	0.00e+00	4.08e-03	7.00e-04	4.20e-04	4.31e-03	2.24e-03	8.13e-02		8.28e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.5760 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.5760 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.576006  0.000000
];

