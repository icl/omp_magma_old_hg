% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_132 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	810436	816914	141586	141586	5.99e-03	4.13e-02	1.01e-02	1.36e-02	1.83e-03	0.00e+00	2.86e-03	4.05e-04	3.73e-04	1.38e-02	1.81e-03	9.21e-02		9.21e-02
1	812271	800991	137782	131304	6.07e-03	3.33e-02	3.04e-03	6.32e-03	1.25e-03	0.00e+00	3.49e-03	3.16e-04	4.17e-04	7.79e-03	2.36e-03	6.43e-02		1.56e-01
2	810634	801246	136753	148033	6.05e-03	4.97e-02	3.76e-03	1.87e-03	2.07e-03	0.00e+00	4.07e-03	3.77e-04	4.15e-04	3.19e-03	2.42e-03	7.40e-02		2.30e-01
3	799573	815204	139196	148584	5.96e-03	5.29e-02	4.25e-03	1.72e-03	4.34e-04	0.00e+00	4.61e-03	4.71e-04	5.19e-04	3.22e-03	2.31e-03	7.64e-02		3.07e-01
4	815420	809569	151063	135432	6.07e-03	5.48e-02	4.21e-03	3.71e-03	6.47e-04	0.00e+00	4.88e-03	4.81e-04	5.30e-04	3.05e-03	2.24e-03	8.06e-02		3.87e-01
5	810723	810078	136021	141872	6.01e-03	5.12e-02	4.32e-03	1.66e-03	5.56e-04	0.00e+00	4.86e-03	5.57e-04	5.06e-04	3.81e-03	2.24e-03	7.57e-02		4.63e-01
6	811848	811353	141524	142169	6.14e-03	5.62e-02	4.29e-03	2.65e-03	8.05e-04	0.00e+00	5.01e-03	3.54e-04	5.26e-04	3.50e-03	2.32e-03	8.18e-02		5.45e-01
7	812603	810972	141205	141700	6.06e-03	5.26e-02	4.36e-03	1.73e-03	5.67e-04	0.00e+00	5.00e-03	3.97e-04	5.13e-04	3.02e-03	2.28e-03	7.65e-02		6.21e-01
8	816197	812355	141256	142887	6.05e-03	5.75e-02	4.36e-03	1.57e-03	4.73e-04	0.00e+00	4.82e-03	4.69e-04	4.78e-04	3.47e-03	2.31e-03	8.16e-02		7.03e-01
9	809485	812063	138468	142310	6.06e-03	5.35e-02	4.38e-03	1.89e-03	1.06e-03	0.00e+00	4.91e-03	4.92e-04	4.97e-04	3.08e-03	2.29e-03	7.82e-02		7.81e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.3042 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.3042 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.304215  0.000000
];

