% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_83 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	809417	805400	141586	141586	5.27e-03	3.05e-02	6.56e-03	9.55e-03	7.35e-04	0.00e+00	3.18e-03	3.35e-04	4.93e-04	6.72e-03	2.48e-03	6.58e-02		6.58e-02
1	799056	806147	138801	142818	5.29e-03	2.79e-02	4.57e-03	5.61e-03	1.60e-03	0.00e+00	3.57e-03	3.91e-04	4.79e-04	4.42e-03	2.57e-03	5.64e-02		1.22e-01
2	812050	815508	149968	142877	5.26e-03	4.80e-02	4.91e-03	3.17e-03	3.72e-04	0.00e+00	4.40e-03	3.99e-04	4.62e-04	3.39e-03	2.71e-03	7.31e-02		1.95e-01
3	811570	807907	137780	134322	5.36e-03	5.84e-02	5.35e-03	1.78e-03	4.64e-04	0.00e+00	5.08e-03	2.36e-04	5.79e-04	3.33e-03	2.78e-03	8.34e-02		2.79e-01
4	809656	814047	139066	142729	5.28e-03	5.63e-02	5.73e-03	1.94e-03	6.08e-04	0.00e+00	5.46e-03	5.08e-04	5.52e-04	3.58e-03	2.82e-03	8.28e-02		3.62e-01
5	808561	807254	141785	137394	5.33e-03	5.22e-02	5.98e-03	2.23e-03	5.90e-04	0.00e+00	5.53e-03	3.77e-04	5.77e-04	3.63e-03	2.79e-03	7.92e-02		4.41e-01
6	812274	812098	143686	144993	5.27e-03	5.38e-02	5.84e-03	3.20e-03	8.38e-04	0.00e+00	5.61e-03	4.84e-04	5.78e-04	3.50e-03	2.83e-03	8.19e-02		5.23e-01
7	806438	813713	140779	140955	5.31e-03	5.25e-02	5.99e-03	1.52e-03	5.69e-04	0.00e+00	5.25e-03	5.43e-04	5.50e-04	3.31e-03	2.79e-03	7.83e-02		6.01e-01
8	810860	811860	147421	140146	5.31e-03	5.33e-02	5.91e-03	2.04e-03	5.76e-04	0.00e+00	5.42e-03	4.45e-04	5.45e-04	3.53e-03	2.79e-03	7.98e-02		6.81e-01
9	814030	812393	143805	142805	5.31e-03	5.22e-02	5.96e-03	1.29e-03	1.03e-03	0.00e+00	5.50e-03	3.11e-04	5.65e-04	3.32e-03	2.84e-03	7.84e-02		7.59e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.2445 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.2445 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.244451  0.000000
];

