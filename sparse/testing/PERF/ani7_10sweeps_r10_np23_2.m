% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_23 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	809280	805415	141586	141586	3.66e-03	5.28e-02	1.03e-02	7.41e-03	1.13e-03	0.00e+00	1.09e-02	3.07e-04	1.13e-03	7.43e-03	6.40e-03	1.01e-01		1.01e-01
1	795816	802923	138938	142803	3.70e-03	6.51e-02	9.64e-03	2.95e-03	1.36e-03	0.00e+00	1.33e-02	2.84e-04	1.32e-03	6.10e-03	8.17e-03	1.12e-01		2.13e-01
2	803472	810670	153208	146101	3.72e-03	1.03e-01	1.25e-02	3.09e-03	1.15e-03	0.00e+00	1.64e-02	3.67e-04	1.35e-03	5.80e-03	9.06e-03	1.56e-01		3.70e-01
3	808604	806869	146358	139160	3.73e-03	1.51e-01	1.42e-02	2.51e-03	1.26e-03	0.00e+00	1.89e-02	2.99e-04	1.54e-03	5.43e-03	8.38e-03	2.07e-01		5.77e-01
4	799942	820537	142032	143767	3.72e-03	1.42e-01	1.44e-02	2.97e-03	1.85e-03	0.00e+00	2.13e-02	2.95e-04	1.66e-03	5.44e-03	8.43e-03	2.02e-01		7.79e-01
5	809071	811242	151499	130904	3.78e-03	1.45e-01	1.48e-02	3.12e-03	1.69e-03	0.00e+00	2.04e-02	4.28e-04	1.59e-03	5.87e-03	8.37e-03	2.05e-01		9.83e-01
6	811764	810096	143176	141005	3.73e-03	1.48e-01	1.48e-02	2.90e-03	1.59e-03	0.00e+00	2.06e-02	4.25e-04	1.55e-03	5.53e-03	8.39e-03	2.08e-01		1.19e+00
7	811936	812404	141289	142957	3.75e-03	1.50e-01	1.49e-02	2.99e-03	1.64e-03	0.00e+00	2.08e-02	3.07e-04	1.58e-03	5.76e-03	8.38e-03	2.10e-01		1.40e+00
8	811339	811236	141923	141455	3.75e-03	1.52e-01	1.50e-02	3.43e-03	1.99e-03	0.00e+00	2.08e-02	3.20e-04	1.57e-03	5.48e-03	8.42e-03	2.13e-01		1.61e+00
9	813311	812041	143326	143429	3.77e-03	1.51e-01	1.50e-02	2.76e-03	1.96e-03	0.00e+00	2.06e-02	3.61e-04	1.56e-03	5.49e-03	8.41e-03	2.11e-01		1.82e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 2.2275 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 2.2275 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  2.227470  0.000000
];

