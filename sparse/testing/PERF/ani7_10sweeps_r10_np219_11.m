% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_219 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	828177	802570	141586	141586	1.01e-02	4.11e-02	9.17e-03	1.90e-02	1.03e-03	0.00e+00	2.46e-03	6.75e-04	3.71e-04	1.27e-02	1.83e-03	9.84e-02		9.84e-02
1	809668	809228	120041	145648	9.95e-03	3.49e-02	3.56e-03	6.86e-03	1.11e-03	0.00e+00	3.20e-03	4.73e-04	3.47e-04	6.55e-03	2.04e-03	6.90e-02		1.67e-01
2	812887	798305	139356	139796	1.00e-02	5.23e-02	3.79e-03	3.52e-03	3.29e-04	0.00e+00	3.45e-03	6.45e-04	4.34e-04	4.28e-03	2.05e-03	8.08e-02		2.48e-01
3	800628	805982	136943	151525	9.88e-03	5.17e-02	4.00e-03	2.17e-03	3.63e-04	0.00e+00	3.77e-03	6.22e-04	4.33e-04	4.31e-03	1.99e-03	7.93e-02		3.27e-01
4	815237	805202	150008	144654	1.00e-02	5.42e-02	4.27e-03	2.75e-03	4.63e-04	0.00e+00	4.03e-03	7.04e-04	4.43e-04	4.61e-03	2.08e-03	8.35e-02		4.11e-01
5	811661	805456	136204	146239	9.95e-03	4.92e-02	4.57e-03	2.44e-03	1.40e-03	0.00e+00	4.10e-03	4.66e-04	4.89e-04	4.83e-03	2.06e-03	7.95e-02		4.90e-01
6	807977	810855	140586	146791	9.97e-03	5.50e-02	4.53e-03	2.65e-03	4.57e-04	0.00e+00	4.04e-03	9.68e-04	4.75e-04	4.62e-03	2.05e-03	8.47e-02		5.75e-01
7	810170	812836	145076	142198	1.01e-02	5.03e-02	4.40e-03	3.08e-03	4.39e-04	0.00e+00	3.97e-03	6.33e-04	4.30e-04	4.42e-03	2.07e-03	7.98e-02		6.55e-01
8	812243	812613	143689	141023	1.01e-02	5.61e-02	4.50e-03	2.48e-03	1.47e-03	0.00e+00	4.05e-03	6.10e-04	4.35e-04	4.44e-03	2.08e-03	8.62e-02		7.41e-01
9	813670	812555	142422	142052	1.01e-02	5.11e-02	4.58e-03	2.07e-03	1.36e-03	0.00e+00	4.04e-03	5.99e-04	4.35e-04	4.25e-03	2.06e-03	8.05e-02		8.22e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.5743 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.5743 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.574284  0.000000
];

