% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_101 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	811158	802779	141586	141586	5.63e-03	2.95e-02	5.97e-03	9.97e-03	6.45e-04	0.00e+00	2.66e-03	3.06e-04	4.48e-04	7.11e-03	2.15e-03	6.44e-02		6.44e-02
1	788571	802739	137060	145439	5.60e-03	2.75e-02	3.82e-03	1.93e-03	7.43e-04	0.00e+00	3.11e-03	3.15e-04	4.83e-04	4.58e-03	2.33e-03	5.04e-02		1.15e-01
2	790715	802154	160453	146285	5.52e-03	4.51e-02	4.36e-03	1.35e-03	3.24e-04	0.00e+00	3.60e-03	3.38e-04	4.75e-04	3.22e-03	2.39e-03	6.66e-02		1.81e-01
3	810625	804975	159115	147676	5.56e-03	4.44e-02	4.56e-03	2.37e-03	4.52e-04	0.00e+00	4.16e-03	5.21e-04	4.82e-04	3.21e-03	2.56e-03	6.83e-02		2.50e-01
4	816585	813899	140011	145661	5.55e-03	4.87e-02	4.98e-03	1.47e-03	4.24e-04	0.00e+00	4.25e-03	5.68e-04	4.56e-04	3.51e-03	2.67e-03	7.26e-02		3.22e-01
5	807671	810444	134856	137542	5.68e-03	4.62e-02	5.31e-03	2.42e-03	6.63e-04	0.00e+00	4.57e-03	4.72e-04	4.74e-04	3.45e-03	2.69e-03	7.19e-02		3.94e-01
6	810481	810485	144576	141803	5.61e-03	4.89e-02	5.28e-03	1.55e-03	4.74e-04	0.00e+00	4.57e-03	4.10e-04	5.01e-04	3.27e-03	2.69e-03	7.32e-02		4.67e-01
7	812417	811215	142572	142568	5.63e-03	4.65e-02	5.34e-03	2.36e-03	4.97e-04	0.00e+00	4.61e-03	5.39e-04	5.16e-04	3.15e-03	2.73e-03	7.18e-02		5.39e-01
8	812894	813497	141442	142644	5.60e-03	4.94e-02	5.38e-03	1.49e-03	9.04e-04	0.00e+00	4.54e-03	4.81e-04	4.79e-04	3.31e-03	2.77e-03	7.44e-02		6.14e-01
9	810852	815236	141771	141168	5.65e-03	4.78e-02	5.40e-03	2.18e-03	8.82e-04	0.00e+00	4.37e-03	5.36e-04	4.86e-04	3.13e-03	2.73e-03	7.32e-02		6.87e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.1627 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.1627 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.162701  0.000000
];

