% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_32 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	806577	798298	141586	141586	3.34e-03	3.91e-02	7.38e-03	6.81e-03	7.65e-04	0.00e+00	7.49e-03	2.47e-04	8.08e-04	6.02e-03	4.28e-03	7.62e-02		7.62e-02
1	810486	801889	141641	149920	3.28e-03	4.64e-02	6.44e-03	2.76e-03	7.75e-04	0.00e+00	9.00e-03	2.33e-04	8.71e-04	4.73e-03	5.59e-03	8.00e-02		1.56e-01
2	809083	800450	138538	147135	3.37e-03	7.33e-02	8.45e-03	3.25e-03	6.70e-04	0.00e+00	1.11e-02	2.76e-04	8.96e-04	4.34e-03	6.05e-03	1.12e-01		2.68e-01
3	808022	797689	140747	149380	3.33e-03	1.10e-01	9.52e-03	2.33e-03	1.01e-03	0.00e+00	1.27e-02	2.98e-04	1.07e-03	4.28e-03	5.77e-03	1.50e-01		4.18e-01
4	805711	805847	142614	152947	3.32e-03	1.02e-01	9.61e-03	2.51e-03	1.43e-03	0.00e+00	1.37e-02	4.57e-04	1.14e-03	4.76e-03	5.59e-03	1.44e-01		5.62e-01
5	810729	807417	145730	145594	3.38e-03	1.00e-01	9.79e-03	2.82e-03	1.74e-03	0.00e+00	1.42e-02	2.74e-04	1.14e-03	4.63e-03	5.63e-03	1.44e-01		7.06e-01
6	812923	808423	141518	144830	3.37e-03	1.04e-01	9.89e-03	3.07e-03	1.37e-03	0.00e+00	1.40e-02	3.54e-04	1.16e-03	4.39e-03	5.67e-03	1.47e-01		8.53e-01
7	808150	813428	140130	144630	3.37e-03	1.04e-01	1.00e-02	2.63e-03	1.36e-03	0.00e+00	1.38e-02	3.35e-04	1.12e-03	4.46e-03	5.77e-03	1.47e-01		1.00e+00
8	811088	813259	145709	140431	3.41e-03	1.07e-01	1.00e-02	2.11e-03	1.56e-03	0.00e+00	1.39e-02	2.23e-04	1.13e-03	4.45e-03	5.76e-03	1.49e-01		1.15e+00
9	812090	813673	143577	141406	3.42e-03	1.07e-01	1.01e-02	2.37e-03	1.50e-03	0.00e+00	1.38e-02	3.04e-04	1.11e-03	4.27e-03	5.76e-03	1.49e-01		1.30e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.6753 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.6753 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.675260  0.000000
];

