% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_45 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	814298	803873	141586	141586	3.25e-03	3.14e-02	5.64e-03	6.69e-03	5.30e-04	0.00e+00	5.36e-03	2.61e-04	6.15e-04	5.59e-03	3.09e-03	6.25e-02		6.25e-02
1	812458	812330	133920	144345	3.25e-03	3.61e-02	4.67e-03	4.35e-03	6.21e-04	0.00e+00	6.59e-03	2.41e-04	6.52e-04	4.16e-03	4.11e-03	6.47e-02		1.27e-01
2	800694	812553	136566	136694	3.30e-03	5.80e-02	6.11e-03	1.84e-03	5.51e-04	0.00e+00	7.97e-03	2.47e-04	7.02e-04	3.73e-03	4.43e-03	8.69e-02		2.14e-01
3	811858	798941	149136	137277	3.32e-03	8.07e-02	6.98e-03	2.00e-03	8.77e-04	0.00e+00	9.17e-03	3.12e-04	7.74e-04	3.63e-03	4.16e-03	1.12e-01		3.26e-01
4	808165	813673	138778	151695	3.28e-03	7.91e-02	7.00e-03	2.04e-03	1.02e-03	0.00e+00	9.98e-03	2.73e-04	8.65e-04	4.20e-03	4.11e-03	1.12e-01		4.38e-01
5	807290	814478	143276	137768	3.33e-03	7.69e-02	7.16e-03	1.77e-03	1.36e-03	0.00e+00	1.01e-02	3.53e-04	8.48e-04	3.93e-03	4.11e-03	1.10e-01		5.48e-01
6	812022	811201	144957	137769	3.33e-03	7.92e-02	7.19e-03	1.75e-03	8.70e-04	0.00e+00	1.00e-02	3.86e-04	8.14e-04	3.79e-03	4.12e-03	1.11e-01		6.59e-01
7	812279	812215	141031	141852	3.31e-03	7.91e-02	7.20e-03	2.20e-03	1.07e-03	0.00e+00	1.02e-02	3.31e-04	8.80e-04	3.85e-03	4.16e-03	1.12e-01		7.72e-01
8	813757	809970	141580	141644	3.33e-03	8.13e-02	7.28e-03	1.68e-03	1.21e-03	0.00e+00	9.57e-03	3.14e-04	8.12e-04	3.67e-03	4.17e-03	1.13e-01		8.85e-01
9	814005	813594	140908	144695	3.34e-03	8.04e-02	7.29e-03	1.75e-03	1.28e-03	0.00e+00	1.00e-02	5.17e-04	8.48e-04	3.67e-03	4.15e-03	1.13e-01		9.98e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.3695 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.3695 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.369459  0.000000
];

