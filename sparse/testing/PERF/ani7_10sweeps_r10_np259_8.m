% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_259 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	821111	819052	141586	141586	1.15e-02	4.56e-02	9.25e-03	3.48e-02	1.75e-03	0.00e+00	2.32e-03	5.25e-04	3.74e-04	1.39e-02	1.58e-03	1.22e-01		1.22e-01
1	796946	797702	127107	129166	1.16e-02	3.85e-02	3.21e-03	3.15e-03	2.85e-03	0.00e+00	2.94e-03	4.11e-04	4.12e-04	7.59e-03	1.81e-03	7.25e-02		1.94e-01
2	821566	816051	152078	151322	1.12e-02	5.10e-02	3.53e-03	3.41e-03	3.40e-04	0.00e+00	3.12e-03	5.14e-04	4.15e-04	4.25e-03	2.06e-03	7.99e-02		2.74e-01
3	799120	801265	128264	133779	1.15e-02	5.49e-02	4.86e-03	3.13e-03	4.24e-04	0.00e+00	3.30e-03	5.49e-04	4.15e-04	4.22e-03	1.98e-03	8.53e-02		3.59e-01
4	798913	809126	151516	149371	1.14e-02	5.31e-02	4.10e-03	2.81e-03	4.63e-04	0.00e+00	3.91e-03	8.42e-04	4.11e-04	4.56e-03	2.01e-03	8.35e-02		4.43e-01
5	813019	806554	152528	142315	1.14e-02	5.09e-02	4.12e-03	2.79e-03	4.54e-04	0.00e+00	3.65e-03	5.84e-04	4.18e-04	5.07e-03	2.02e-03	8.13e-02		5.24e-01
6	807496	810882	139228	145693	1.13e-02	5.64e-02	4.23e-03	2.58e-03	4.48e-04	0.00e+00	3.45e-03	5.47e-04	4.12e-04	4.91e-03	2.06e-03	8.64e-02		6.10e-01
7	815542	814584	145557	142171	1.15e-02	5.23e-02	4.31e-03	2.21e-03	5.06e-04	0.00e+00	3.75e-03	8.30e-04	4.16e-04	4.27e-03	2.11e-03	8.22e-02		6.93e-01
8	806407	814866	138317	139275	1.15e-02	5.79e-02	4.39e-03	1.96e-03	4.59e-04	0.00e+00	3.64e-03	8.12e-04	4.67e-04	4.70e-03	2.08e-03	8.79e-02		7.81e-01
9	813454	814553	148258	139799	1.15e-02	5.37e-02	4.35e-03	1.89e-03	1.67e-03	0.00e+00	3.47e-03	6.42e-04	4.14e-04	4.23e-03	2.15e-03	8.40e-02		8.64e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.5964 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.5964 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.596448  0.000000
];

