% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_100 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	822774	804736	141586	141586	5.58e-03	2.90e-02	6.04e-03	1.04e-02	5.92e-04	0.00e+00	2.71e-03	4.94e-04	4.28e-04	6.87e-03	2.20e-03	6.42e-02		6.42e-02
1	803336	799113	125444	143482	5.56e-03	2.77e-02	3.93e-03	2.60e-03	7.36e-04	0.00e+00	3.21e-03	3.33e-04	4.96e-04	4.43e-03	2.35e-03	5.13e-02		1.16e-01
2	820340	820828	145688	149911	5.48e-03	4.55e-02	4.37e-03	3.28e-03	4.17e-04	0.00e+00	3.64e-03	5.99e-04	4.91e-04	3.15e-03	2.64e-03	6.96e-02		1.85e-01
3	818580	807508	129490	129002	5.64e-03	5.77e-02	4.93e-03	1.77e-03	4.44e-04	0.00e+00	4.32e-03	6.19e-04	5.04e-04	3.21e-03	2.62e-03	8.17e-02		2.67e-01
4	816995	811402	132056	143128	5.55e-03	5.11e-02	5.13e-03	1.66e-03	5.37e-04	0.00e+00	4.62e-03	5.93e-04	4.97e-04	3.33e-03	2.66e-03	7.57e-02		3.43e-01
5	813432	809312	134446	140039	5.59e-03	4.71e-02	5.34e-03	1.80e-03	9.18e-04	0.00e+00	4.57e-03	5.48e-04	5.08e-04	3.63e-03	2.69e-03	7.26e-02		4.15e-01
6	813282	810445	138815	142935	5.54e-03	4.99e-02	5.42e-03	4.03e-03	4.20e-04	0.00e+00	5.19e-03	6.53e-04	5.12e-04	3.45e-03	2.70e-03	7.78e-02		4.93e-01
7	809713	809993	139771	142608	5.55e-03	4.74e-02	5.36e-03	2.00e-03	4.90e-04	0.00e+00	5.13e-03	7.96e-04	5.15e-04	3.26e-03	2.64e-03	7.31e-02		5.66e-01
8	814835	811157	144146	143866	5.58e-03	4.93e-02	5.43e-03	2.79e-03	1.10e-03	0.00e+00	5.19e-03	4.28e-04	5.13e-04	3.32e-03	2.66e-03	7.64e-02		6.43e-01
9	812878	813095	139830	143508	5.61e-03	4.77e-02	5.46e-03	2.82e-03	1.00e-03	0.00e+00	5.56e-03	5.38e-04	5.33e-04	3.21e-03	2.90e-03	7.53e-02		7.18e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.1897 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.1897 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.189710  0.000000
];

