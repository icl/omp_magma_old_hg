% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_111 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	824458	805855	141586	141586	5.78e-03	2.81e-02	5.86e-03	1.02e-02	9.92e-04	0.00e+00	2.46e-03	3.75e-04	4.31e-04	6.54e-03	2.00e-03	6.27e-02		6.27e-02
1	800182	797479	123760	142363	5.77e-03	2.76e-02	3.54e-03	3.73e-03	8.62e-04	0.00e+00	2.86e-03	2.92e-04	4.18e-04	4.22e-03	2.34e-03	5.17e-02		1.14e-01
2	806785	801579	148842	151545	6.13e-03	4.17e-02	4.07e-03	3.52e-03	4.87e-04	0.00e+00	3.37e-03	5.16e-04	4.41e-04	3.45e-03	2.51e-03	6.62e-02		1.81e-01
3	804095	807301	143045	148251	5.73e-03	4.42e-02	4.58e-03	2.56e-03	3.87e-04	0.00e+00	3.89e-03	3.94e-04	4.99e-04	3.01e-03	2.44e-03	6.77e-02		2.48e-01
4	813727	800787	146541	143335	5.75e-03	4.53e-02	4.65e-03	2.23e-03	5.95e-04	0.00e+00	4.20e-03	3.15e-04	4.95e-04	3.11e-03	2.41e-03	6.90e-02		3.17e-01
5	806636	811693	137714	150654	5.69e-03	4.17e-02	4.78e-03	1.82e-03	4.62e-04	0.00e+00	4.18e-03	4.96e-04	4.85e-04	3.37e-03	2.48e-03	6.54e-02		3.83e-01
6	815200	809654	145611	140554	5.80e-03	4.73e-02	4.88e-03	2.10e-03	4.34e-04	0.00e+00	4.96e-03	6.72e-04	4.75e-04	3.13e-03	2.47e-03	7.22e-02		4.55e-01
7	812217	805959	137853	143399	5.79e-03	4.72e-02	4.95e-03	2.64e-03	3.76e-04	0.00e+00	4.78e-03	5.73e-04	4.84e-04	3.40e-03	2.47e-03	7.26e-02		5.28e-01
8	811858	812722	141642	147900	5.77e-03	4.91e-02	4.86e-03	2.02e-03	1.07e-03	0.00e+00	4.77e-03	4.05e-04	4.60e-04	3.01e-03	2.53e-03	7.40e-02		6.02e-01
9	813576	814613	142807	141943	5.82e-03	4.72e-02	4.93e-03	1.89e-03	8.05e-04	0.00e+00	4.69e-03	3.99e-04	4.66e-04	3.04e-03	2.56e-03	7.18e-02		6.73e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.1508 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.1508 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.150754  0.000000
];

