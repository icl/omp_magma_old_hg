% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_249 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	823733	819202	141586	141586	1.11e-02	4.41e-02	9.29e-03	2.09e-02	3.06e-03	0.00e+00	2.18e-03	4.69e-04	3.81e-04	1.45e-02	1.71e-03	1.08e-01		1.08e-01
1	783040	810196	124485	129016	1.13e-02	3.73e-02	3.27e-03	4.27e-03	2.62e-03	0.00e+00	2.94e-03	5.09e-04	4.12e-04	7.50e-03	1.86e-03	7.19e-02		1.80e-01
2	805502	805097	165984	138828	1.10e-02	5.13e-02	3.57e-03	2.74e-03	2.83e-04	0.00e+00	2.98e-03	5.37e-04	3.80e-04	4.25e-03	1.97e-03	7.91e-02		2.59e-01
3	810542	815593	144328	144733	1.10e-02	4.91e-02	4.00e-03	3.52e-03	3.09e-04	0.00e+00	3.33e-03	5.10e-04	3.87e-04	4.25e-03	2.02e-03	7.84e-02		3.37e-01
4	801634	812481	140094	135043	1.11e-02	5.59e-02	4.25e-03	2.63e-03	4.10e-04	0.00e+00	3.63e-03	7.25e-04	4.25e-04	4.47e-03	2.03e-03	8.56e-02		4.23e-01
5	811765	809257	149807	138960	1.11e-02	4.87e-02	4.20e-03	2.36e-03	3.78e-04	0.00e+00	3.55e-03	7.06e-04	4.05e-04	5.02e-03	2.05e-03	7.85e-02		5.01e-01
6	805408	807899	140482	142990	1.11e-02	5.54e-02	4.21e-03	2.41e-03	4.90e-04	0.00e+00	3.61e-03	7.32e-04	4.54e-04	4.63e-03	2.02e-03	8.51e-02		5.86e-01
7	811735	813111	147645	145154	1.11e-02	4.95e-02	4.19e-03	1.64e-03	3.80e-04	0.00e+00	3.64e-03	7.35e-04	4.10e-04	4.13e-03	2.08e-03	7.78e-02		6.64e-01
8	813900	806140	142124	140748	1.11e-02	5.58e-02	4.28e-03	1.79e-03	4.23e-04	0.00e+00	3.51e-03	7.03e-04	4.09e-04	4.57e-03	2.07e-03	8.46e-02		7.49e-01
9	813006	817465	140765	148525	1.10e-02	4.99e-02	4.26e-03	1.46e-03	1.43e-03	0.00e+00	3.61e-03	7.84e-04	4.17e-04	4.23e-03	2.11e-03	7.92e-02		8.28e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.5810 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.5810 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.580980  0.000000
];

