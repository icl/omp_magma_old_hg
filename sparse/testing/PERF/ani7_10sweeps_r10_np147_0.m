% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_147 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	828434	807798	141586	141586	7.61e-03	3.56e-02	7.63e-03	1.40e-02	6.24e-04	0.00e+00	2.59e-03	4.90e-04	4.05e-04	9.42e-03	2.22e-03	8.06e-02		8.06e-02
1	799141	810726	119784	140420	7.58e-03	2.97e-02	4.19e-03	5.24e-03	8.29e-04	0.00e+00	3.09e-03	4.57e-04	3.91e-04	5.02e-03	2.22e-03	5.87e-02		1.39e-01
2	801947	803499	149883	138298	7.60e-03	4.63e-02	4.41e-03	2.36e-03	3.21e-04	0.00e+00	3.52e-03	5.48e-04	4.20e-04	4.01e-03	2.22e-03	7.17e-02		2.11e-01
3	800921	813510	147883	146331	7.55e-03	5.01e-02	5.05e-03	2.12e-03	6.08e-04	0.00e+00	4.14e-03	5.95e-04	4.70e-04	3.97e-03	2.61e-03	7.72e-02		2.88e-01
4	812694	808626	149715	137126	8.72e-03	5.19e-02	5.25e-03	1.59e-03	4.39e-04	0.00e+00	4.23e-03	4.48e-04	5.17e-04	4.75e-03	2.56e-03	8.04e-02		3.69e-01
5	811373	809938	138747	142815	8.71e-03	4.74e-02	5.43e-03	2.51e-03	1.01e-03	0.00e+00	4.32e-03	5.26e-04	4.86e-04	4.49e-03	2.67e-03	7.75e-02		4.46e-01
6	811009	811422	140874	142309	8.67e-03	5.20e-02	5.44e-03	2.36e-03	4.40e-04	0.00e+00	4.29e-03	7.13e-04	4.65e-04	4.46e-03	2.59e-03	8.14e-02		5.27e-01
7	810470	810907	142044	141631	8.62e-03	4.88e-02	5.31e-03	2.37e-03	4.18e-04	0.00e+00	5.16e-03	6.34e-04	4.57e-04	4.31e-03	2.46e-03	7.85e-02		6.06e-01
8	814801	810039	143389	142952	7.61e-03	5.07e-02	5.18e-03	2.23e-03	1.00e-03	0.00e+00	4.21e-03	5.45e-04	4.83e-04	4.05e-03	2.39e-03	7.84e-02		6.84e-01
9	813065	812437	139864	144626	7.65e-03	4.77e-02	5.34e-03	2.60e-03	1.10e-03	0.00e+00	4.24e-03	5.34e-04	4.51e-04	3.93e-03	2.39e-03	7.60e-02		7.60e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.3908 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.3908 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.390792  0.000000
];

