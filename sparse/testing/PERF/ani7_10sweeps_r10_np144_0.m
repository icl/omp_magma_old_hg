% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_144 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	813650	804962	141586	141586	7.47e-03	3.49e-02	7.04e-03	1.42e-02	1.10e-03	0.00e+00	2.65e-03	4.39e-04	4.12e-04	8.64e-03	2.17e-03	7.90e-02		7.90e-02
1	813423	806868	134568	143256	7.44e-03	2.98e-02	4.14e-03	2.42e-03	1.44e-03	0.00e+00	3.03e-03	4.08e-04	3.78e-04	5.25e-03	2.30e-03	5.66e-02		1.36e-01
2	830570	811822	135601	142156	7.42e-03	4.60e-02	4.45e-03	1.74e-03	6.06e-04	0.00e+00	3.68e-03	4.57e-04	4.76e-04	3.99e-03	2.39e-03	7.12e-02		2.07e-01
3	803243	803447	119260	138008	7.50e-03	5.35e-02	5.05e-03	2.09e-03	4.13e-04	0.00e+00	4.31e-03	4.80e-04	5.25e-04	3.91e-03	2.33e-03	8.01e-02		2.87e-01
4	810414	810246	147393	147189	7.42e-03	4.93e-02	5.11e-03	2.89e-03	7.08e-04	0.00e+00	4.49e-03	5.81e-04	4.82e-04	3.92e-03	2.45e-03	7.74e-02		3.64e-01
5	812285	813301	141027	141195	7.45e-03	4.65e-02	5.36e-03	3.16e-03	4.83e-04	0.00e+00	4.40e-03	4.78e-04	4.70e-04	4.24e-03	2.44e-03	7.49e-02		4.39e-01
6	809271	809412	139962	138946	7.49e-03	5.09e-02	5.43e-03	2.04e-03	3.81e-04	0.00e+00	4.31e-03	7.53e-04	4.80e-04	4.07e-03	2.41e-03	7.82e-02		5.17e-01
7	809561	812375	143782	143641	7.49e-03	4.74e-02	5.36e-03	1.76e-03	4.30e-04	0.00e+00	4.32e-03	4.81e-04	4.85e-04	4.30e-03	2.40e-03	7.45e-02		5.92e-01
8	815090	807196	144298	141484	7.48e-03	5.07e-02	5.29e-03	2.10e-03	1.39e-03	0.00e+00	4.48e-03	5.71e-04	4.65e-04	3.98e-03	2.42e-03	7.89e-02		6.71e-01
9	813017	816711	139575	147469	7.44e-03	4.89e-02	5.71e-03	1.56e-03	9.94e-04	0.00e+00	4.37e-03	4.78e-04	4.90e-04	3.92e-03	2.72e-03	7.66e-02		7.47e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.3842 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.3842 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.384218  0.000000
];

