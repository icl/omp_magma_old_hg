% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_60 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	816108	809832	141586	141586	3.22e-03	2.72e-02	6.00e-03	6.89e-03	5.56e-04	0.00e+00	4.03e-03	3.00e-04	4.81e-04	5.57e-03	2.40e-03	5.67e-02		5.67e-02
1	805098	793671	132110	138386	3.23e-03	3.01e-02	3.56e-03	2.07e-03	4.82e-04	0.00e+00	5.00e-03	2.55e-04	4.82e-04	4.21e-03	3.19e-03	5.26e-02		1.09e-01
2	803490	801611	143926	155353	3.22e-03	5.13e-02	4.53e-03	1.16e-03	4.06e-04	0.00e+00	5.88e-03	2.36e-04	5.68e-04	3.29e-03	3.41e-03	7.40e-02		1.83e-01
3	807818	806491	146340	148219	3.81e-03	6.52e-02	5.06e-03	1.58e-03	5.49e-04	0.00e+00	6.91e-03	3.11e-04	6.65e-04	3.27e-03	3.21e-03	9.05e-02		2.74e-01
4	809197	810242	142818	144145	3.28e-03	6.36e-02	5.60e-03	2.14e-03	7.20e-04	0.00e+00	7.49e-03	3.45e-04	6.88e-04	3.81e-03	3.10e-03	9.08e-02		3.65e-01
5	810001	805828	142244	141199	3.27e-03	6.66e-02	7.64e-03	1.72e-03	1.17e-03	0.00e+00	8.25e-03	2.29e-04	7.34e-04	3.47e-03	3.97e-03	9.71e-02		4.62e-01
6	812257	809624	142246	146419	5.32e-03	6.81e-02	7.57e-03	1.53e-03	6.97e-04	0.00e+00	8.65e-03	3.12e-04	7.32e-04	3.47e-03	4.00e-03	1.00e-01		5.62e-01
7	812390	810748	140796	143429	5.33e-03	6.86e-02	7.54e-03	1.74e-03	7.52e-04	0.00e+00	9.05e-03	2.70e-04	7.66e-04	3.32e-03	3.98e-03	1.01e-01		6.63e-01
8	813642	811361	141469	143111	5.31e-03	6.44e-02	5.50e-03	1.46e-03	1.15e-03	0.00e+00	7.72e-03	2.95e-04	7.06e-04	3.39e-03	3.16e-03	9.31e-02		7.57e-01
9	813655	813489	141023	143304	3.32e-03	6.39e-02	5.52e-03	1.67e-03	1.11e-03	0.00e+00	7.44e-03	3.98e-04	6.88e-04	3.28e-03	3.14e-03	9.04e-02		8.47e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.2167 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.2167 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.216708  0.000000
];

