% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_67 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	810979	803953	141586	141586	4.96e-03	2.78e-02	4.60e-03	6.96e-03	8.27e-04	0.00e+00	3.62e-03	3.03e-04	4.25e-04	5.73e-03	2.12e-03	5.74e-02		5.74e-02
1	801891	803216	137239	144265	3.22e-03	2.88e-02	3.17e-03	2.04e-03	9.76e-04	0.00e+00	4.48e-03	1.98e-04	4.60e-04	3.83e-03	2.95e-03	5.01e-02		1.07e-01
2	808825	810492	147133	145808	3.22e-03	4.99e-02	4.10e-03	1.85e-03	6.36e-04	0.00e+00	5.43e-03	3.13e-04	5.01e-04	3.17e-03	3.09e-03	7.23e-02		1.80e-01
3	804377	806029	141005	139338	3.27e-03	6.21e-02	4.81e-03	1.55e-03	5.24e-04	0.00e+00	6.07e-03	3.15e-04	6.36e-04	3.14e-03	2.89e-03	8.53e-02		2.65e-01
4	806607	815326	146259	144607	3.27e-03	5.97e-02	4.78e-03	1.88e-03	9.09e-04	0.00e+00	7.04e-03	3.14e-04	6.39e-04	3.20e-03	2.78e-03	8.45e-02		3.50e-01
5	807205	805757	144834	136115	3.30e-03	5.62e-02	4.83e-03	2.04e-03	5.84e-04	0.00e+00	6.61e-03	2.59e-04	6.34e-04	3.49e-03	2.78e-03	8.07e-02		4.30e-01
6	809753	813446	145042	146490	3.25e-03	5.86e-02	4.80e-03	1.33e-03	6.85e-04	0.00e+00	6.67e-03	3.19e-04	6.52e-04	3.19e-03	2.83e-03	8.23e-02		5.13e-01
7	811823	813334	143300	139607	3.29e-03	5.76e-02	4.87e-03	1.43e-03	6.85e-04	0.00e+00	6.75e-03	3.58e-04	6.23e-04	3.28e-03	2.88e-03	8.18e-02		5.94e-01
8	814316	809287	142036	140525	3.30e-03	5.97e-02	4.89e-03	2.02e-03	1.17e-03	0.00e+00	6.79e-03	3.48e-04	6.14e-04	3.25e-03	2.89e-03	8.50e-02		6.79e-01
9	814686	812294	140349	145378	3.27e-03	5.84e-02	4.91e-03	1.89e-03	9.66e-04	0.00e+00	6.66e-03	3.59e-04	6.67e-04	3.16e-03	2.86e-03	8.32e-02		7.62e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.1450 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.1450 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.144954  0.000000
];

