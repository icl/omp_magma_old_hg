% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_229 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	809455	804750	141586	141586	1.05e-02	4.32e-02	8.94e-03	1.96e-02	1.08e-03	0.00e+00	2.41e-03	4.92e-04	3.63e-04	1.47e-02	1.76e-03	1.03e-01		1.03e-01
1	806876	804368	138763	143468	1.03e-02	3.44e-02	3.42e-03	3.75e-03	1.16e-03	0.00e+00	2.94e-03	4.50e-04	3.72e-04	7.93e-03	1.95e-03	6.68e-02		1.70e-01
2	804323	804122	142148	144656	1.03e-02	5.16e-02	3.75e-03	4.70e-03	2.73e-04	0.00e+00	3.34e-03	6.06e-04	4.26e-04	4.31e-03	2.10e-03	8.13e-02		2.51e-01
3	817260	814110	145507	145708	1.03e-02	5.16e-02	4.09e-03	3.25e-03	3.14e-04	0.00e+00	3.69e-03	5.62e-04	4.52e-04	4.27e-03	2.20e-03	8.08e-02		3.32e-01
4	807007	812416	133376	136526	1.05e-02	5.79e-02	4.48e-03	2.73e-03	8.60e-04	0.00e+00	3.92e-03	5.44e-04	4.73e-04	5.13e-03	2.25e-03	8.88e-02		4.21e-01
5	806613	807110	144434	139025	1.04e-02	4.90e-02	4.58e-03	3.15e-03	6.03e-04	0.00e+00	3.92e-03	4.79e-04	4.49e-04	4.63e-03	2.27e-03	7.95e-02		5.00e-01
6	810272	814185	145634	145137	1.03e-02	5.42e-02	4.59e-03	2.55e-03	4.19e-04	0.00e+00	3.84e-03	6.34e-04	4.48e-04	4.67e-03	2.31e-03	8.41e-02		5.84e-01
7	811680	810481	142781	138868	1.04e-02	5.01e-02	4.70e-03	2.18e-03	4.17e-04	0.00e+00	3.90e-03	6.76e-04	4.39e-04	4.28e-03	2.28e-03	7.94e-02		6.64e-01
8	811644	807903	142179	143378	1.04e-02	5.52e-02	4.67e-03	1.90e-03	9.49e-04	0.00e+00	4.00e-03	6.78e-04	4.55e-04	4.41e-03	2.25e-03	8.49e-02		7.49e-01
9	817277	812484	143021	146762	1.04e-02	5.15e-02	4.78e-03	2.47e-03	1.62e-03	0.00e+00	4.30e-03	6.08e-04	4.56e-04	4.29e-03	2.34e-03	8.28e-02		8.31e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.5866 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.5866 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.586612  0.000000
];

