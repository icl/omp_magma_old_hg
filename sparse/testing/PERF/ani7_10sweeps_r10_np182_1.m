% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_182 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	814837	804315	141586	141586	8.45e-03	3.66e-02	7.41e-03	1.53e-02	2.33e-03	0.00e+00	2.64e-03	6.14e-04	3.49e-04	1.00e-02	1.83e-03	8.56e-02		8.56e-02
1	809336	813540	133381	143903	8.72e-03	3.13e-02	3.46e-03	4.71e-03	1.70e-03	0.00e+00	2.50e-03	4.30e-04	4.22e-04	5.99e-03	2.07e-03	6.13e-02		1.47e-01
2	809725	811998	139688	135484	8.45e-03	4.58e-02	4.00e-03	2.07e-03	1.04e-03	0.00e+00	2.98e-03	3.83e-04	4.11e-04	3.81e-03	2.25e-03	7.12e-02		2.18e-01
3	818578	799320	140105	137832	8.47e-03	4.50e-02	4.34e-03	3.80e-03	3.05e-04	0.00e+00	3.34e-03	4.75e-04	4.03e-04	3.86e-03	2.23e-03	7.22e-02		2.90e-01
4	810479	801042	132058	151316	8.37e-03	5.02e-02	4.47e-03	1.47e-03	3.24e-04	0.00e+00	3.56e-03	5.50e-04	4.33e-04	3.93e-03	2.22e-03	7.55e-02		3.66e-01
5	814508	811221	140962	150399	8.32e-03	4.41e-02	4.55e-03	2.10e-03	4.40e-04	0.00e+00	3.57e-03	4.88e-04	4.12e-04	4.36e-03	2.34e-03	7.07e-02		4.37e-01
6	811402	806249	137739	141026	8.46e-03	5.04e-02	4.71e-03	1.42e-03	3.67e-04	0.00e+00	3.52e-03	5.94e-04	4.38e-04	4.10e-03	2.23e-03	7.62e-02		5.13e-01
7	811135	809556	141651	146804	8.43e-03	4.48e-02	4.66e-03	2.05e-03	5.37e-04	0.00e+00	3.52e-03	6.80e-04	4.20e-04	3.92e-03	2.21e-03	7.12e-02		5.84e-01
8	813779	816784	142724	144303	8.42e-03	4.91e-02	4.72e-03	2.90e-03	5.82e-04	0.00e+00	3.56e-03	6.31e-04	4.49e-04	3.99e-03	2.25e-03	7.66e-02		6.61e-01
9	813293	813999	140886	137881	8.52e-03	4.80e-02	4.85e-03	2.32e-03	1.32e-03	0.00e+00	3.52e-03	4.80e-04	4.35e-04	4.03e-03	2.32e-03	7.58e-02		7.36e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.3847 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.3847 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.384732  0.000000
];

