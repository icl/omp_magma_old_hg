% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_211 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	819531	811762	141586	141586	9.79e-03	4.09e-02	8.29e-03	1.85e-02	1.48e-03	0.00e+00	2.54e-03	5.27e-04	3.78e-04	1.25e-02	1.90e-03	9.68e-02		9.68e-02
1	804955	803828	128687	136456	9.80e-03	3.60e-02	3.67e-03	5.59e-03	2.59e-03	0.00e+00	3.24e-03	3.61e-04	3.53e-04	6.86e-03	2.12e-03	7.06e-02		1.67e-01
2	794934	810805	144069	145196	9.63e-03	5.40e-02	3.87e-03	3.34e-03	3.52e-04	0.00e+00	3.51e-03	6.64e-04	4.15e-04	4.35e-03	2.11e-03	8.22e-02		2.50e-01
3	811632	817961	154896	139025	9.75e-03	5.26e-02	4.07e-03	2.18e-03	3.75e-04	0.00e+00	3.87e-03	6.75e-04	4.54e-04	4.26e-03	2.20e-03	8.05e-02		3.30e-01
4	802147	804302	139004	132675	9.83e-03	5.85e-02	4.45e-03	2.08e-03	8.46e-04	0.00e+00	4.17e-03	1.00e-03	5.02e-04	4.45e-03	2.01e-03	8.79e-02		4.18e-01
5	809847	806602	149294	147139	9.64e-03	4.95e-02	4.43e-03	2.42e-03	4.26e-04	0.00e+00	4.18e-03	7.52e-04	4.52e-04	5.18e-03	2.06e-03	7.90e-02		4.97e-01
6	811345	811476	142400	145645	9.68e-03	5.52e-02	4.54e-03	2.07e-03	4.43e-04	0.00e+00	4.10e-03	5.90e-04	4.70e-04	4.67e-03	2.13e-03	8.38e-02		5.81e-01
7	811169	810430	141708	141577	9.76e-03	5.23e-02	4.56e-03	2.53e-03	4.54e-04	0.00e+00	4.29e-03	8.77e-04	4.64e-04	4.32e-03	2.09e-03	8.16e-02		6.62e-01
8	809712	811982	142690	143429	1.18e-02	5.57e-02	4.60e-03	2.85e-03	4.34e-04	0.00e+00	4.15e-03	6.16e-04	4.42e-04	4.46e-03	2.12e-03	8.72e-02		7.50e-01
9	814447	812932	144953	142683	9.76e-03	5.24e-02	4.54e-03	1.94e-03	1.60e-03	0.00e+00	4.12e-03	6.56e-04	4.41e-04	4.35e-03	2.13e-03	8.19e-02		8.32e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.5763 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.5763 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.576319  0.000000
];

