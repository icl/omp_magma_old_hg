% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_11 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	818909	806947	141586	141586	4.16e-03	9.75e-02	2.06e-02	8.99e-03	1.59e-03	0.00e+00	2.24e-02	3.07e-04	2.24e-03	1.06e-02	1.33e-02	1.82e-01		1.82e-01
1	808923	814998	129309	141271	4.11e-03	1.29e-01	2.01e-02	4.83e-03	2.14e-03	0.00e+00	2.76e-02	2.59e-04	2.61e-03	9.50e-03	1.71e-02	2.17e-01		3.99e-01
2	794801	796726	140101	134026	4.25e-03	2.00e-01	2.59e-02	4.45e-03	2.01e-03	0.00e+00	3.32e-02	2.36e-04	2.71e-03	8.80e-03	1.76e-02	2.99e-01		6.98e-01
3	796572	801972	155029	153104	4.16e-03	2.63e-01	2.77e-02	5.02e-03	2.51e-03	0.00e+00	3.83e-02	2.28e-04	2.90e-03	8.71e-03	1.68e-02	3.69e-01		1.07e+00
4	816375	813095	154064	148664	4.16e-03	2.51e-01	2.92e-02	5.41e-03	3.95e-03	0.00e+00	4.25e-02	2.88e-04	3.21e-03	9.23e-03	1.69e-02	3.66e-01		1.43e+00
5	801892	806051	135066	138346	4.17e-03	2.87e-01	3.02e-02	6.78e-03	3.50e-03	0.00e+00	4.06e-02	2.96e-04	3.04e-03	9.58e-03	1.65e-02	4.01e-01		1.83e+00
6	811210	809718	150355	146196	4.18e-03	2.71e-01	2.95e-02	5.50e-03	3.52e-03	0.00e+00	4.18e-02	2.79e-04	3.08e-03	8.90e-03	1.68e-02	3.85e-01		2.22e+00
7	808848	811914	141843	143335	4.20e-03	2.81e-01	3.02e-02	5.87e-03	3.41e-03	0.00e+00	4.11e-02	2.27e-04	3.06e-03	8.89e-03	1.68e-02	3.95e-01		2.61e+00
8	809115	811770	145011	141945	4.20e-03	2.83e-01	3.05e-02	6.03e-03	3.74e-03	0.00e+00	4.11e-02	3.04e-04	3.08e-03	9.23e-03	1.69e-02	3.98e-01		3.01e+00
9	815861	815792	145550	142895	4.21e-03	2.81e-01	3.05e-02	5.22e-03	3.56e-03	0.00e+00	4.13e-02	3.14e-04	3.00e-03	8.90e-03	1.72e-02	3.96e-01		3.41e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 3.8613 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 3.8613 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  3.861251  0.000000
];

