% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_271 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	823557	807724	141586	141586	1.18e-02	4.50e-02	9.74e-03	2.15e-02	1.14e-03	0.00e+00	3.81e-03	5.25e-04	3.23e-04	1.51e-02	1.60e-03	1.11e-01		1.11e-01
1	799431	781605	124661	140494	1.17e-02	3.79e-02	3.13e-03	6.01e-03	1.44e-03	0.00e+00	3.24e-03	4.75e-04	3.54e-04	7.10e-03	1.88e-03	7.32e-02		1.84e-01
2	809636	802913	149593	167419	1.14e-02	5.49e-02	3.52e-03	2.41e-03	3.44e-04	0.00e+00	3.47e-03	5.97e-04	4.18e-04	4.33e-03	2.06e-03	8.35e-02		2.67e-01
3	815030	819889	140194	146917	1.17e-02	5.64e-02	7.35e-03	3.56e-03	4.22e-04	0.00e+00	3.86e-03	5.34e-04	4.60e-04	4.35e-03	2.12e-03	9.07e-02		3.58e-01
4	814372	812035	135606	130747	1.20e-02	6.26e-02	4.32e-03	1.54e-03	1.52e-03	0.00e+00	3.97e-03	5.64e-04	4.78e-04	4.76e-03	1.96e-03	9.37e-02		4.52e-01
5	805058	811058	137069	139406	1.18e-02	5.34e-02	4.34e-03	1.83e-03	1.52e-03	0.00e+00	3.96e-03	9.23e-04	4.70e-04	5.10e-03	2.07e-03	8.55e-02		5.37e-01
6	809528	808624	147189	141189	1.18e-02	6.14e-02	4.16e-03	4.00e-03	4.10e-04	0.00e+00	4.03e-03	6.71e-04	4.35e-04	4.97e-03	2.02e-03	9.40e-02		6.31e-01
7	810972	810501	143525	144429	1.18e-02	5.32e-02	4.20e-03	4.04e-03	4.50e-04	0.00e+00	4.06e-03	6.27e-04	4.33e-04	4.39e-03	2.04e-03	8.53e-02		7.16e-01
8	812884	810946	142887	143358	1.18e-02	5.91e-02	4.20e-03	3.11e-03	1.86e-03	0.00e+00	4.06e-03	6.17e-04	4.44e-04	4.67e-03	2.07e-03	9.20e-02		8.08e-01
9	813490	812727	141781	143719	1.19e-02	5.42e-02	4.24e-03	2.04e-03	1.81e-03	0.00e+00	4.04e-03	7.12e-04	4.31e-04	4.23e-03	2.03e-03	8.56e-02		8.94e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.6136 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.6136 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.613579  0.000000
];

