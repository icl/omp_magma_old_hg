% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_131 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	814990	811970	141586	141586	5.95e-03	2.78e-02	5.75e-03	1.09e-02	3.77e-04	0.00e+00	2.77e-03	3.31e-04	3.80e-04	6.78e-03	1.74e-03	6.27e-02		6.27e-02
1	834121	810904	133228	136248	5.99e-03	2.74e-02	3.05e-03	2.12e-03	5.24e-04	0.00e+00	3.41e-03	3.25e-04	3.78e-04	4.94e-03	2.13e-03	5.03e-02		1.13e-01
2	805392	806285	114903	138120	6.00e-03	3.84e-02	3.80e-03	2.24e-03	2.77e-04	0.00e+00	4.31e-03	3.95e-04	4.17e-04	3.09e-03	2.30e-03	6.12e-02		1.74e-01
3	803854	810147	144438	143545	5.98e-03	4.79e-02	4.15e-03	1.65e-03	3.27e-04	0.00e+00	4.45e-03	4.10e-04	4.58e-04	2.97e-03	2.32e-03	7.06e-02		2.45e-01
4	809209	812558	146782	140489	5.99e-03	4.97e-02	4.32e-03	2.67e-03	4.90e-04	0.00e+00	4.74e-03	4.86e-04	5.01e-04	3.51e-03	2.23e-03	7.46e-02		3.19e-01
5	804090	807497	142232	138883	6.04e-03	4.56e-02	4.35e-03	1.98e-03	8.81e-04	0.00e+00	4.86e-03	4.36e-04	4.74e-04	3.28e-03	2.25e-03	7.02e-02		3.90e-01
6	812012	809111	148157	144750	6.00e-03	4.79e-02	4.30e-03	2.07e-03	4.46e-04	0.00e+00	4.83e-03	3.18e-04	4.54e-04	3.44e-03	2.25e-03	7.20e-02		4.62e-01
7	805656	811364	141041	143942	5.98e-03	4.65e-02	4.37e-03	1.85e-03	4.76e-04	0.00e+00	4.69e-03	5.36e-04	4.95e-04	3.04e-03	2.26e-03	7.02e-02		5.32e-01
8	810436	811017	148203	142495	6.02e-03	4.85e-02	4.35e-03	1.71e-03	9.06e-04	0.00e+00	4.77e-03	4.82e-04	4.66e-04	3.32e-03	2.25e-03	7.28e-02		6.05e-01
9	811994	813849	144229	143648	6.02e-03	4.74e-02	4.40e-03	1.92e-03	1.06e-03	0.00e+00	4.77e-03	5.42e-04	4.62e-04	3.01e-03	2.29e-03	7.18e-02		6.76e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.1572 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.1572 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.157232  0.000000
];

