% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_38 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	809592	808959	141586	141586	3.29e-03	3.47e-02	6.39e-03	6.76e-03	1.01e-03	0.00e+00	6.30e-03	2.58e-04	6.81e-04	6.17e-03	3.68e-03	6.92e-02		6.92e-02
1	807085	812197	138626	139259	3.28e-03	4.08e-02	5.53e-03	3.56e-03	8.74e-04	0.00e+00	7.63e-03	2.17e-04	7.73e-04	4.52e-03	4.74e-03	7.20e-02		1.41e-01
2	803416	801387	141939	136827	3.34e-03	6.47e-02	7.22e-03	2.20e-03	6.74e-04	0.00e+00	9.35e-03	3.32e-04	8.34e-04	4.24e-03	5.13e-03	9.81e-02		2.39e-01
3	813686	807750	146414	148443	3.31e-03	9.29e-02	7.89e-03	2.11e-03	8.08e-04	0.00e+00	1.08e-02	2.45e-04	9.07e-04	3.91e-03	4.97e-03	1.28e-01		3.67e-01
4	805671	801388	136950	142886	3.32e-03	8.91e-02	8.48e-03	1.84e-03	1.09e-03	0.00e+00	1.15e-02	2.77e-04	9.56e-04	3.93e-03	4.69e-03	1.25e-01		4.92e-01
5	811929	810995	145770	150053	3.33e-03	8.46e-02	8.24e-03	2.81e-03	1.27e-03	0.00e+00	1.19e-02	2.52e-04	9.70e-04	4.48e-03	4.85e-03	1.23e-01		6.15e-01
6	812068	811688	140318	141252	3.33e-03	9.07e-02	8.52e-03	1.93e-03	1.08e-03	0.00e+00	1.20e-02	3.37e-04	9.69e-04	3.95e-03	4.85e-03	1.28e-01		7.43e-01
7	811594	810395	140985	141365	3.32e-03	9.14e-02	8.51e-03	1.93e-03	1.05e-03	0.00e+00	1.16e-02	3.93e-04	9.59e-04	4.14e-03	4.88e-03	1.28e-01		8.71e-01
8	811712	812331	142265	143464	3.35e-03	9.29e-02	8.55e-03	2.23e-03	1.58e-03	0.00e+00	1.22e-02	3.37e-04	9.94e-04	4.00e-03	4.86e-03	1.31e-01		1.00e+00
9	814604	812967	142953	142334	3.39e-03	9.22e-02	8.58e-03	2.10e-03	1.44e-03	0.00e+00	1.17e-02	3.81e-04	9.74e-04	3.94e-03	4.88e-03	1.30e-01		1.13e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.4991 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.4991 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.499065  0.000000
];

