% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_182 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	815302	804666	141586	141586	8.50e-03	3.57e-02	7.45e-03	1.55e-02	9.91e-04	0.00e+00	2.15e-03	5.60e-04	3.85e-04	1.03e-02	1.85e-03	8.33e-02		8.33e-02
1	809686	808416	132916	143552	8.41e-03	3.06e-02	3.45e-03	2.19e-03	8.55e-04	0.00e+00	2.52e-03	4.43e-04	4.24e-04	5.97e-03	2.08e-03	5.69e-02		1.40e-01
2	796829	807554	139338	140608	8.44e-03	4.56e-02	4.00e-03	1.62e-03	2.70e-04	0.00e+00	2.94e-03	5.46e-04	4.21e-04	3.84e-03	2.22e-03	6.99e-02		2.10e-01
3	818198	808570	153001	142276	8.40e-03	4.34e-02	4.27e-03	4.28e-03	3.59e-04	0.00e+00	3.25e-03	5.29e-04	4.20e-04	3.91e-03	2.28e-03	7.11e-02		2.81e-01
4	808845	801180	132438	142066	8.44e-03	4.93e-02	4.51e-03	3.10e-03	5.08e-04	0.00e+00	3.63e-03	5.44e-04	4.85e-04	4.18e-03	2.19e-03	7.69e-02		3.58e-01
5	812072	807363	142596	150261	8.36e-03	4.44e-02	4.54e-03	6.14e-03	1.16e-03	0.00e+00	3.51e-03	4.60e-04	4.11e-04	4.52e-03	2.24e-03	7.57e-02		4.34e-01
6	809899	809543	140175	144884	8.44e-03	4.96e-02	4.67e-03	1.70e-03	4.46e-04	0.00e+00	3.53e-03	7.20e-04	4.64e-04	4.35e-03	2.30e-03	7.62e-02		5.10e-01
7	812934	810258	143154	143510	8.46e-03	4.51e-02	4.60e-03	2.15e-03	3.65e-04	0.00e+00	3.42e-03	5.88e-04	4.11e-04	4.18e-03	2.25e-03	7.15e-02		5.82e-01
8	811422	810540	140925	143601	8.43e-03	5.00e-02	4.74e-03	1.81e-03	1.05e-03	0.00e+00	3.46e-03	6.11e-04	4.56e-04	3.92e-03	2.30e-03	7.68e-02		6.58e-01
9	814237	813738	143243	144125	8.45e-03	4.65e-02	4.79e-03	3.24e-03	1.06e-03	0.00e+00	3.44e-03	6.23e-04	4.03e-04	3.96e-03	2.30e-03	7.47e-02		7.33e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.3858 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.3858 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.385826  0.000000
];

