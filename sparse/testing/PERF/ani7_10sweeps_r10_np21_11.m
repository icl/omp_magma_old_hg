% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_21 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	812403	797495	141586	141586	3.71e-03	5.67e-02	1.12e-02	7.57e-03	1.16e-03	0.00e+00	1.20e-02	3.35e-04	1.24e-03	7.74e-03	7.01e-03	1.09e-01		1.09e-01
1	830117	797650	135815	150723	3.69e-03	7.10e-02	1.06e-02	3.09e-03	1.43e-03	0.00e+00	1.46e-02	2.46e-04	1.40e-03	6.46e-03	9.10e-03	1.22e-01		2.30e-01
2	810366	801311	118907	151374	3.74e-03	1.12e-01	1.43e-02	3.41e-03	1.28e-03	0.00e+00	1.79e-02	4.48e-04	1.46e-03	5.77e-03	9.76e-03	1.70e-01		4.01e-01
3	812034	806901	139464	148519	3.72e-03	1.58e-01	1.51e-02	3.38e-03	1.35e-03	0.00e+00	2.07e-02	3.08e-04	1.67e-03	5.73e-03	9.21e-03	2.19e-01		6.20e-01
4	811367	804394	138602	143735	3.75e-03	1.50e-01	1.58e-02	2.99e-03	1.86e-03	0.00e+00	2.27e-02	3.82e-04	1.68e-03	5.75e-03	9.11e-03	2.13e-01		8.33e-01
5	810349	811731	140074	147047	3.76e-03	1.48e-01	1.59e-02	3.90e-03	1.69e-03	0.00e+00	2.20e-02	3.96e-04	1.65e-03	6.13e-03	9.20e-03	2.13e-01		1.05e+00
6	812568	813551	141898	140516	3.79e-03	1.55e-01	1.61e-02	3.14e-03	1.84e-03	0.00e+00	2.29e-02	3.50e-04	1.73e-03	5.78e-03	9.24e-03	2.20e-01		1.27e+00
7	810513	809512	140485	139502	3.81e-03	1.55e-01	1.63e-02	3.32e-03	1.81e-03	0.00e+00	2.30e-02	3.43e-04	1.71e-03	5.97e-03	9.23e-03	2.20e-01		1.49e+00
8	812533	810908	143346	144347	3.79e-03	1.56e-01	1.63e-02	3.00e-03	1.99e-03	0.00e+00	2.23e-02	4.79e-04	1.61e-03	5.79e-03	9.26e-03	2.21e-01		1.71e+00
9	811836	812567	142132	143757	3.81e-03	1.56e-01	1.64e-02	2.99e-03	2.16e-03	0.00e+00	2.29e-02	2.39e-04	1.71e-03	5.76e-03	9.29e-03	2.21e-01		1.93e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 2.3327 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 2.3327 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  2.332732  0.000000
];

