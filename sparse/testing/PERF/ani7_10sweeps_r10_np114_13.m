% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_114 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	805678	812998	141586	141586	5.75e-03	2.79e-02	5.86e-03	1.04e-02	5.84e-04	0.00e+00	2.38e-03	3.09e-04	3.81e-04	7.06e-03	1.94e-03	6.25e-02		6.25e-02
1	799429	813470	142540	135220	5.83e-03	2.72e-02	3.46e-03	2.26e-03	6.83e-04	0.00e+00	2.71e-03	3.31e-04	4.06e-04	4.41e-03	2.31e-03	4.96e-02		1.12e-01
2	787502	802195	149595	135554	5.84e-03	4.10e-02	4.07e-03	2.10e-03	3.28e-04	0.00e+00	3.33e-03	3.90e-04	4.83e-04	3.07e-03	2.40e-03	6.30e-02		1.75e-01
3	825862	800112	162328	147635	5.73e-03	4.50e-02	4.37e-03	1.34e-03	3.83e-04	0.00e+00	3.91e-03	4.40e-04	5.25e-04	3.12e-03	2.43e-03	6.73e-02		2.42e-01
4	808888	806290	124774	150524	5.71e-03	4.87e-02	4.76e-03	1.36e-03	4.56e-04	0.00e+00	4.01e-03	3.92e-04	4.86e-04	3.11e-03	2.39e-03	7.14e-02		3.14e-01
5	809663	811761	142553	145151	5.79e-03	4.30e-02	4.73e-03	2.29e-03	1.12e-03	0.00e+00	4.17e-03	3.61e-04	4.96e-04	3.54e-03	2.42e-03	6.80e-02		3.82e-01
6	811174	813784	142584	140486	5.81e-03	4.65e-02	4.82e-03	1.67e-03	4.36e-04	0.00e+00	4.13e-03	3.70e-04	4.90e-04	3.35e-03	2.44e-03	7.00e-02		4.52e-01
7	812198	813707	141879	139269	5.82e-03	4.56e-02	4.90e-03	2.33e-03	4.26e-04	0.00e+00	3.98e-03	4.56e-04	4.82e-04	3.22e-03	2.49e-03	6.97e-02		5.21e-01
8	810488	813257	141661	140152	5.82e-03	4.74e-02	4.93e-03	1.95e-03	1.11e-03	0.00e+00	4.18e-03	3.70e-04	5.01e-04	3.16e-03	2.45e-03	7.19e-02		5.93e-01
9	811607	814665	144177	141408	5.83e-03	4.57e-02	4.90e-03	1.64e-03	7.67e-04	0.00e+00	3.90e-03	3.17e-04	5.08e-04	3.01e-03	2.44e-03	6.90e-02		6.62e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.1416 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.1416 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.141559  0.000000
];

