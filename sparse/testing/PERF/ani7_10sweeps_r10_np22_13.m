% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_22 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	807854	813212	141586	141586	3.68e-03	5.52e-02	1.08e-02	7.55e-03	1.04e-03	0.00e+00	1.14e-02	2.94e-04	1.19e-03	7.63e-03	6.66e-03	1.05e-01		1.05e-01
1	826451	816564	140364	135006	3.71e-03	6.89e-02	1.01e-02	4.41e-03	1.29e-03	0.00e+00	1.38e-02	2.44e-04	1.37e-03	6.20e-03	8.79e-03	1.19e-01		2.24e-01
2	794381	819113	122573	132460	3.77e-03	1.08e-01	1.37e-02	3.13e-03	1.10e-03	0.00e+00	1.72e-02	2.15e-04	1.39e-03	5.64e-03	9.21e-03	1.63e-01		3.87e-01
3	808579	794803	155449	130717	3.81e-03	1.51e-01	1.46e-02	2.71e-03	1.63e-03	0.00e+00	2.02e-02	4.26e-04	1.63e-03	5.53e-03	8.65e-03	2.10e-01		5.97e-01
4	820349	812986	142057	155833	3.70e-03	1.40e-01	1.49e-02	3.68e-03	2.36e-03	0.00e+00	2.24e-02	3.60e-04	1.69e-03	6.03e-03	8.93e-03	2.04e-01		8.01e-01
5	812421	812187	131092	138455	3.74e-03	1.53e-01	1.58e-02	3.09e-03	1.88e-03	0.00e+00	2.18e-02	3.67e-04	1.65e-03	6.06e-03	8.83e-03	2.16e-01		1.02e+00
6	809589	814694	139826	140060	3.78e-03	1.54e-01	1.79e-02	3.19e-03	1.70e-03	0.00e+00	2.16e-02	4.13e-04	1.65e-03	5.81e-03	8.84e-03	2.19e-01		1.24e+00
7	814088	803464	143464	138359	3.79e-03	1.55e-01	1.56e-02	3.41e-03	1.78e-03	0.00e+00	2.13e-02	2.63e-04	1.62e-03	5.60e-03	8.73e-03	2.17e-01		1.45e+00
8	808999	812756	139771	150395	3.69e-03	1.51e-01	1.55e-02	3.91e-03	2.32e-03	0.00e+00	2.25e-02	3.95e-04	1.67e-03	5.86e-03	8.80e-03	2.16e-01		1.67e+00
9	812652	812581	145666	141909	3.79e-03	1.55e-01	1.55e-02	3.42e-03	2.19e-03	0.00e+00	2.15e-02	4.07e-04	1.65e-03	5.66e-03	8.82e-03	2.18e-01		1.89e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 2.2909 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 2.2909 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  2.290920  0.000000
];

