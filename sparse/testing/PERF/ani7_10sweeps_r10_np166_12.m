% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_166 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	815764	811362	141586	141586	8.15e-03	3.61e-02	7.48e-03	1.46e-02	1.00e-03	0.00e+00	2.40e-03	4.61e-04	3.75e-04	9.57e-03	2.00e-03	8.21e-02		8.21e-02
1	810918	819546	132454	136856	8.17e-03	3.10e-02	3.79e-03	5.26e-03	1.32e-03	0.00e+00	2.75e-03	4.30e-04	4.49e-04	5.76e-03	2.19e-03	6.11e-02		1.43e-01
2	800527	810948	138106	129478	8.69e-03	4.61e-02	4.22e-03	2.50e-03	7.34e-04	0.00e+00	3.17e-03	5.53e-04	4.05e-04	4.40e-03	2.36e-03	7.31e-02		2.16e-01
3	809060	815124	149303	138882	8.59e-03	5.02e-02	4.94e-03	3.30e-03	4.09e-04	0.00e+00	4.87e-03	5.78e-04	4.41e-04	3.96e-03	2.69e-03	8.00e-02		2.96e-01
4	807000	815629	141576	135512	9.23e-03	5.57e-02	5.06e-03	3.79e-03	5.71e-04	0.00e+00	5.10e-03	5.77e-04	5.03e-04	5.60e-03	2.44e-03	8.86e-02		3.85e-01
5	807306	808517	144441	135812	8.64e-03	4.58e-02	5.07e-03	1.43e-03	3.94e-04	0.00e+00	3.88e-03	7.01e-04	4.31e-04	4.34e-03	2.45e-03	7.31e-02		4.58e-01
6	806125	810763	144941	143730	8.10e-03	4.97e-02	5.05e-03	3.34e-03	4.26e-04	0.00e+00	3.87e-03	4.60e-04	4.63e-04	4.06e-03	2.41e-03	7.79e-02		5.36e-01
7	812683	817742	146928	142290	8.12e-03	4.61e-02	5.07e-03	4.23e-03	3.71e-04	0.00e+00	3.78e-03	7.38e-04	4.23e-04	4.19e-03	2.48e-03	7.55e-02		6.11e-01
8	809458	811794	141176	136117	8.17e-03	5.19e-02	5.21e-03	1.75e-03	1.05e-03	0.00e+00	3.90e-03	5.49e-04	4.56e-04	3.89e-03	2.45e-03	7.93e-02		6.91e-01
9	812780	811731	145207	142871	8.11e-03	4.67e-02	5.13e-03	2.35e-03	9.88e-04	0.00e+00	3.80e-03	4.91e-04	4.25e-04	3.91e-03	2.43e-03	7.43e-02		7.65e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.3966 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.3966 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.396614  0.000000
];

