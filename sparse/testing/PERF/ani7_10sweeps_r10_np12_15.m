% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_12 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	819711	808799	141586	141586	4.03e-03	8.99e-02	1.91e-02	8.67e-03	1.52e-03	0.00e+00	2.05e-02	2.70e-04	2.09e-03	1.00e-02	1.22e-02	1.68e-01		1.68e-01
1	802640	812651	128507	139419	4.03e-03	1.15e-01	1.85e-02	5.69e-03	1.89e-03	0.00e+00	2.53e-02	2.85e-04	2.25e-03	8.87e-03	1.57e-02	1.97e-01		3.65e-01
2	802971	806432	146384	136373	4.16e-03	1.87e-01	2.37e-02	4.69e-03	2.36e-03	0.00e+00	3.05e-02	3.30e-04	2.50e-03	8.68e-03	1.68e-02	2.80e-01		6.46e-01
3	801985	807602	146859	143398	4.11e-03	2.63e-01	2.64e-02	4.53e-03	2.27e-03	0.00e+00	3.50e-02	3.52e-04	2.71e-03	8.27e-03	1.56e-02	3.62e-01		1.01e+00
4	802793	806367	148651	143034	4.14e-03	2.41e-01	2.68e-02	4.98e-03	2.47e-03	0.00e+00	3.64e-02	4.55e-04	2.67e-03	8.21e-03	1.52e-02	3.42e-01		1.35e+00
5	812534	808953	148648	145074	4.10e-03	2.37e-01	2.69e-02	5.18e-03	3.04e-03	0.00e+00	3.79e-02	4.34e-04	2.79e-03	8.53e-03	1.55e-02	3.42e-01		1.69e+00
6	811614	810014	139713	143294	4.13e-03	2.51e-01	2.76e-02	5.27e-03	3.26e-03	0.00e+00	3.94e-02	2.64e-04	2.88e-03	8.29e-03	1.56e-02	3.58e-01		2.05e+00
7	810845	813550	141439	143039	4.11e-03	2.52e-01	2.79e-02	4.95e-03	2.75e-03	0.00e+00	3.71e-02	4.36e-04	2.73e-03	8.61e-03	1.56e-02	3.56e-01		2.41e+00
8	811636	812193	143014	140309	4.15e-03	2.58e-01	2.80e-02	6.18e-03	3.28e-03	0.00e+00	3.86e-02	3.00e-04	2.84e-03	8.25e-03	1.57e-02	3.65e-01		2.77e+00
9	809367	810675	143029	142472	4.16e-03	2.56e-01	2.81e-02	4.97e-03	3.21e-03	0.00e+00	3.80e-02	3.45e-04	2.80e-03	8.25e-03	1.55e-02	3.62e-01		3.13e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 3.5699 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 3.5699 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  3.569889  0.000000
];

