% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_129 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	811714	809432	141586	141586	5.96e-03	2.81e-02	5.74e-03	1.09e-02	6.27e-04	0.00e+00	2.80e-03	3.74e-04	3.49e-04	7.24e-03	1.76e-03	6.39e-02		6.39e-02
1	818005	811621	136504	138786	6.25e-03	2.76e-02	3.09e-03	1.88e-03	1.04e-03	0.00e+00	3.69e-03	2.71e-04	3.57e-04	4.61e-03	2.24e-03	5.10e-02		1.15e-01
2	804489	815026	131019	137403	6.05e-03	3.76e-02	3.81e-03	3.14e-03	3.52e-04	0.00e+00	4.02e-03	3.92e-04	4.00e-04	3.03e-03	2.40e-03	6.12e-02		1.76e-01
3	805759	807345	145341	134804	6.10e-03	4.76e-02	4.30e-03	2.05e-03	3.21e-04	0.00e+00	4.52e-03	3.72e-04	4.70e-04	2.94e-03	2.30e-03	7.10e-02		2.47e-01
4	806790	807131	144877	143291	6.05e-03	4.80e-02	4.30e-03	1.47e-03	4.79e-04	0.00e+00	4.68e-03	4.81e-04	4.60e-04	3.22e-03	2.24e-03	7.14e-02		3.19e-01
5	816181	809392	144651	144310	5.96e-03	4.40e-02	4.36e-03	1.39e-03	4.55e-04	0.00e+00	4.63e-03	3.92e-04	4.43e-04	3.55e-03	2.30e-03	6.75e-02		3.86e-01
6	808214	812898	136066	142855	5.99e-03	4.82e-02	4.46e-03	2.27e-03	3.82e-04	0.00e+00	4.64e-03	4.52e-04	4.58e-04	3.29e-03	2.30e-03	7.25e-02		4.58e-01
7	811007	811648	144839	140155	6.01e-03	4.62e-02	4.47e-03	1.69e-03	4.01e-04	0.00e+00	4.66e-03	4.26e-04	4.69e-04	3.01e-03	2.27e-03	6.96e-02		5.28e-01
8	812836	811256	142852	142211	5.98e-03	4.86e-02	4.48e-03	1.80e-03	5.03e-04	0.00e+00	4.58e-03	6.11e-04	4.28e-04	3.17e-03	2.31e-03	7.25e-02		6.01e-01
9	813385	815950	141829	143409	6.01e-03	4.67e-02	4.52e-03	1.92e-03	1.13e-03	0.00e+00	4.82e-03	3.45e-04	4.75e-04	2.95e-03	2.29e-03	7.12e-02		6.72e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.1650 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.1650 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.164976  0.000000
];

