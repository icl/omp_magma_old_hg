% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_58 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	819726	814015	141586	141586	3.27e-03	2.81e-02	4.85e-03	6.90e-03	6.80e-04	0.00e+00	4.19e-03	2.82e-04	4.58e-04	5.85e-03	2.50e-03	5.71e-02		5.71e-02
1	800520	809311	128492	134203	3.26e-03	3.11e-02	3.71e-03	2.16e-03	9.92e-04	0.00e+00	5.12e-03	2.21e-04	5.28e-04	3.89e-03	3.29e-03	5.43e-02		1.11e-01
2	814356	814226	148504	139713	3.26e-03	5.26e-02	4.73e-03	2.07e-03	5.47e-04	0.00e+00	6.13e-03	2.54e-04	5.38e-04	3.67e-03	3.57e-03	7.74e-02		1.89e-01
3	815714	803923	135474	135604	3.32e-03	7.17e-02	5.62e-03	1.33e-03	5.48e-04	0.00e+00	7.16e-03	4.30e-04	6.60e-04	3.36e-03	3.31e-03	9.74e-02		2.86e-01
4	805308	799959	134922	146713	3.25e-03	6.67e-02	5.53e-03	1.47e-03	7.36e-04	0.00e+00	7.75e-03	3.72e-04	7.20e-04	3.30e-03	3.21e-03	9.31e-02		3.79e-01
5	807560	807850	146133	151482	3.25e-03	6.31e-02	5.47e-03	1.61e-03	6.48e-04	0.00e+00	7.58e-03	3.96e-04	6.89e-04	3.58e-03	3.24e-03	8.96e-02		4.69e-01
6	811980	809214	144687	144397	3.29e-03	6.48e-02	5.57e-03	1.90e-03	7.55e-04	0.00e+00	7.87e-03	3.71e-04	7.06e-04	3.31e-03	3.26e-03	9.18e-02		5.61e-01
7	811166	811099	141073	143839	3.27e-03	6.43e-02	5.64e-03	1.65e-03	6.82e-04	0.00e+00	7.72e-03	3.26e-04	7.13e-04	3.58e-03	3.26e-03	9.12e-02		6.52e-01
8	808220	809400	142693	142760	3.28e-03	6.63e-02	5.70e-03	1.65e-03	1.17e-03	0.00e+00	7.75e-03	2.89e-04	7.03e-04	3.31e-03	3.28e-03	9.35e-02		7.45e-01
9	812490	811718	146445	145265	4.11e-03	6.41e-02	5.66e-03	2.39e-03	9.61e-04	0.00e+00	7.80e-03	4.31e-04	6.93e-04	3.31e-03	3.25e-03	9.28e-02		8.38e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.2096 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.2096 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.209568  0.000000
];

