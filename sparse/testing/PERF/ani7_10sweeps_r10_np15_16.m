% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_15 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	817384	802393	141586	141586	3.89e-03	7.45e-02	1.53e-02	8.16e-03	1.32e-03	0.00e+00	1.66e-02	2.50e-04	1.71e-03	9.10e-03	9.66e-03	1.41e-01		1.41e-01
1	799259	799084	130834	145825	3.85e-03	9.27e-02	1.47e-02	5.29e-03	1.86e-03	0.00e+00	2.05e-02	2.63e-04	1.91e-03	7.71e-03	1.25e-02	1.61e-01		3.02e-01
2	810959	805788	149765	149940	3.95e-03	1.52e-01	1.88e-02	3.68e-03	1.61e-03	0.00e+00	2.49e-02	3.29e-04	1.98e-03	7.51e-03	1.35e-02	2.29e-01		5.30e-01
3	799734	816002	138871	144042	3.92e-03	2.15e-01	2.13e-02	3.52e-03	1.81e-03	0.00e+00	2.86e-02	2.93e-04	2.21e-03	7.12e-03	1.27e-02	2.96e-01		8.26e-01
4	808287	803747	150902	134634	4.00e-03	2.00e-01	2.16e-02	6.52e-03	2.73e-03	0.00e+00	3.14e-02	2.84e-04	2.42e-03	7.08e-03	1.24e-02	2.88e-01		1.11e+00
5	812553	806413	143154	147694	3.93e-03	1.97e-01	2.16e-02	5.56e-03	2.74e-03	0.00e+00	3.15e-02	2.09e-04	2.34e-03	7.50e-03	1.26e-02	2.85e-01		1.40e+00
6	809018	811499	139694	145834	3.95e-03	2.07e-01	2.20e-02	4.16e-03	2.59e-03	0.00e+00	3.14e-02	3.34e-04	2.35e-03	7.17e-03	1.27e-02	2.94e-01		1.69e+00
7	814737	806147	144035	141554	3.98e-03	2.09e-01	2.23e-02	4.43e-03	2.71e-03	0.00e+00	3.21e-02	1.94e-04	2.37e-03	7.38e-03	1.27e-02	2.97e-01		1.99e+00
8	812131	810143	139122	147712	3.95e-03	2.10e-01	2.25e-02	3.89e-03	2.75e-03	0.00e+00	3.12e-02	3.87e-04	2.29e-03	7.13e-03	1.27e-02	2.97e-01		2.29e+00
9	811678	819533	142534	144522	3.97e-03	2.11e-01	2.26e-02	3.84e-03	2.71e-03	0.00e+00	3.16e-02	3.87e-04	2.34e-03	7.18e-03	1.29e-02	2.98e-01		2.59e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 3.0106 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 3.0106 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  3.010642  0.000000
];

