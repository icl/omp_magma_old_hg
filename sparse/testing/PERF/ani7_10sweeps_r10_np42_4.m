% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_42 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	808684	810283	141586	141586	3.26e-03	3.24e-02	5.92e-03	6.68e-03	5.83e-04	0.00e+00	5.75e-03	2.57e-04	6.41e-04	5.61e-03	3.36e-03	6.44e-02		6.44e-02
1	813975	784736	139534	137935	3.28e-03	3.86e-02	5.02e-03	2.38e-03	7.70e-04	0.00e+00	6.89e-03	2.06e-04	7.03e-04	4.54e-03	4.36e-03	6.68e-02		1.31e-01
2	816586	804766	135049	164288	3.22e-03	5.98e-02	6.39e-03	2.27e-03	5.48e-04	0.00e+00	8.36e-03	2.33e-04	7.06e-04	3.77e-03	4.79e-03	9.00e-02		2.21e-01
3	801978	822878	133244	145064	3.29e-03	8.75e-02	7.44e-03	1.83e-03	6.73e-04	0.00e+00	9.80e-03	3.26e-04	8.22e-04	3.77e-03	4.65e-03	1.20e-01		3.41e-01
4	801783	804724	148658	127758	3.39e-03	8.77e-02	7.65e-03	2.49e-03	1.15e-03	0.00e+00	1.08e-02	3.47e-04	9.45e-04	3.93e-03	4.28e-03	1.23e-01		4.64e-01
5	805505	810636	149658	146717	3.30e-03	7.96e-02	7.50e-03	2.28e-03	1.31e-03	0.00e+00	1.02e-02	3.17e-04	8.15e-04	4.22e-03	4.38e-03	1.14e-01		5.78e-01
6	816386	809028	146742	141611	3.30e-03	8.39e-02	7.64e-03	2.39e-03	9.43e-04	0.00e+00	1.07e-02	3.03e-04	8.60e-04	3.90e-03	4.45e-03	1.18e-01		6.96e-01
7	808837	813555	136667	144025	3.29e-03	8.69e-02	7.78e-03	1.80e-03	9.80e-04	0.00e+00	1.07e-02	2.86e-04	8.79e-04	3.95e-03	4.42e-03	1.21e-01		8.17e-01
8	814234	813278	145022	140304	3.34e-03	8.62e-02	7.76e-03	2.13e-03	1.43e-03	0.00e+00	1.07e-02	3.39e-04	8.48e-04	3.86e-03	4.44e-03	1.21e-01		9.38e-01
9	814701	811930	140431	141387	3.34e-03	8.63e-02	7.82e-03	2.45e-03	1.37e-03	0.00e+00	1.09e-02	3.61e-04	9.12e-04	3.76e-03	4.43e-03	1.22e-01		1.06e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.4313 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.4313 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.431269  0.000000
];

