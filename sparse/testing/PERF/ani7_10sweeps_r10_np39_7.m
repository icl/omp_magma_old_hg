% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_39 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	815382	814003	141586	141586	3.29e-03	3.41e-02	6.23e-03	6.73e-03	8.44e-04	0.00e+00	6.19e-03	2.73e-04	6.71e-04	5.99e-03	3.64e-03	6.80e-02		6.80e-02
1	812153	804197	132836	134215	3.31e-03	4.11e-02	5.41e-03	3.32e-03	1.10e-03	0.00e+00	7.41e-03	2.36e-04	7.68e-04	4.53e-03	4.65e-03	7.18e-02		1.40e-01
2	808788	811313	136871	144827	3.31e-03	6.30e-02	7.02e-03	1.91e-03	8.21e-04	0.00e+00	9.17e-03	2.90e-04	8.19e-04	4.21e-03	5.08e-03	9.57e-02		2.35e-01
3	813707	801656	141042	138517	3.34e-03	9.16e-02	8.09e-03	2.57e-03	7.75e-04	0.00e+00	1.06e-02	3.39e-04	9.01e-04	3.89e-03	4.78e-03	1.27e-01		3.62e-01
4	799464	811371	136929	148980	3.31e-03	8.78e-02	8.10e-03	2.10e-03	1.11e-03	0.00e+00	1.14e-02	3.36e-04	9.68e-04	3.83e-03	4.67e-03	1.24e-01		4.86e-01
5	811636	807784	151977	140070	3.33e-03	8.53e-02	8.10e-03	1.87e-03	1.11e-03	0.00e+00	1.15e-02	3.15e-04	9.50e-04	4.15e-03	4.70e-03	1.21e-01		6.07e-01
6	810203	808900	140611	144463	3.32e-03	8.87e-02	8.26e-03	2.14e-03	1.06e-03	0.00e+00	1.18e-02	4.19e-04	9.53e-04	3.88e-03	4.69e-03	1.25e-01		7.33e-01
7	810031	813280	142850	144153	3.35e-03	8.80e-02	8.28e-03	2.04e-03	1.17e-03	0.00e+00	1.19e-02	3.08e-04	9.66e-04	4.10e-03	4.76e-03	1.25e-01		8.57e-01
8	811044	812012	143828	140579	3.34e-03	9.08e-02	8.35e-03	1.71e-03	1.31e-03	0.00e+00	1.13e-02	2.21e-04	9.04e-04	3.88e-03	4.71e-03	1.27e-01		9.84e-01
9	813343	814979	143621	142653	3.37e-03	9.03e-02	8.38e-03	1.97e-03	1.36e-03	0.00e+00	1.17e-02	3.34e-04	9.29e-04	3.92e-03	4.77e-03	1.27e-01		1.11e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.4853 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.4853 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.485252  0.000000
];

