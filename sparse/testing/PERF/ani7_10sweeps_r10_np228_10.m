% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_228 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	814579	812302	141586	141586	1.04e-02	4.20e-02	9.00e-03	1.83e-02	1.48e-03	0.00e+00	2.40e-03	6.07e-04	3.89e-04	1.25e-02	1.74e-03	9.88e-02		9.88e-02
1	817498	815761	133639	135916	1.04e-02	3.51e-02	3.41e-03	4.08e-03	1.14e-03	0.00e+00	3.07e-03	4.67e-04	3.30e-04	6.51e-03	1.97e-03	6.64e-02		1.65e-01
2	808629	794203	131526	133263	1.03e-02	5.17e-02	3.93e-03	1.68e-03	1.10e-03	0.00e+00	3.40e-03	4.42e-04	4.24e-04	4.32e-03	2.06e-03	7.94e-02		2.45e-01
3	804902	806215	141201	155627	1.01e-02	5.14e-02	4.09e-03	5.52e-03	3.67e-04	0.00e+00	3.61e-03	6.34e-04	4.55e-04	4.22e-03	2.18e-03	8.26e-02		3.27e-01
4	811732	811585	145734	144421	1.02e-02	5.53e-02	4.36e-03	2.43e-03	5.05e-04	0.00e+00	3.93e-03	5.34e-04	4.68e-04	4.21e-03	2.24e-03	8.42e-02		4.11e-01
5	804258	811335	139709	139856	1.03e-02	5.05e-02	4.62e-03	2.29e-03	4.52e-04	0.00e+00	3.87e-03	7.15e-04	4.69e-04	4.54e-03	2.25e-03	8.00e-02		4.91e-01
6	823000	807862	147989	140912	1.06e-02	5.54e-02	4.60e-03	2.74e-03	4.07e-04	0.00e+00	3.91e-03	6.80e-04	4.14e-04	4.45e-03	2.31e-03	8.55e-02		5.77e-01
7	815799	806684	130053	145191	1.03e-02	5.19e-02	4.78e-03	2.47e-03	3.76e-04	0.00e+00	3.84e-03	6.11e-04	4.44e-04	4.57e-03	2.29e-03	8.15e-02		6.58e-01
8	814156	808708	138060	147175	1.02e-02	5.62e-02	4.70e-03	2.10e-03	1.39e-03	0.00e+00	3.94e-03	6.63e-04	4.51e-04	4.20e-03	2.26e-03	8.61e-02		7.45e-01
9	810565	810970	140509	145957	1.03e-02	5.16e-02	4.75e-03	2.64e-03	1.14e-03	0.00e+00	3.73e-03	8.11e-04	4.57e-04	4.20e-03	2.25e-03	8.19e-02		8.27e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.5815 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.5815 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.581483  0.000000
];

