% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_254 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	815855	812480	141586	141586	1.12e-02	4.45e-02	9.20e-03	2.23e-02	1.21e-03	0.00e+00	2.16e-03	5.45e-04	3.33e-04	1.42e-02	1.58e-03	1.07e-01		1.07e-01
1	819053	793833	132363	135738	1.12e-02	3.54e-02	3.17e-03	3.35e-03	1.50e-03	0.00e+00	2.70e-03	4.69e-04	3.62e-04	8.35e-03	1.86e-03	6.83e-02		1.76e-01
2	804236	809359	129971	155191	1.09e-02	5.10e-02	3.65e-03	1.90e-03	2.83e-04	0.00e+00	3.03e-03	6.01e-04	3.84e-04	4.34e-03	2.06e-03	7.81e-02		2.54e-01
3	815145	803783	145594	140471	1.11e-02	5.09e-02	4.03e-03	3.87e-03	3.22e-04	0.00e+00	3.28e-03	4.37e-04	4.17e-04	4.28e-03	2.00e-03	8.06e-02		3.34e-01
4	799930	817180	135491	146853	1.10e-02	5.55e-02	4.10e-03	2.77e-03	4.24e-04	0.00e+00	3.49e-03	5.72e-04	4.21e-04	5.27e-03	2.02e-03	8.56e-02		4.20e-01
5	809998	814943	151511	134261	1.13e-02	4.90e-02	4.08e-03	3.49e-03	7.21e-04	0.00e+00	3.53e-03	6.44e-04	4.69e-04	4.53e-03	2.04e-03	7.98e-02		5.00e-01
6	807077	809861	142249	137304	1.13e-02	5.72e-02	4.21e-03	2.22e-03	4.61e-04	0.00e+00	3.49e-03	6.86e-04	4.19e-04	4.72e-03	2.01e-03	8.66e-02		5.86e-01
7	811560	809510	145976	143192	1.11e-02	5.00e-02	4.21e-03	1.67e-03	4.57e-04	0.00e+00	3.41e-03	7.43e-04	4.05e-04	4.34e-03	2.00e-03	7.84e-02		6.65e-01
8	813212	814959	142299	144349	1.11e-02	5.63e-02	4.25e-03	1.37e-03	9.88e-04	0.00e+00	3.44e-03	6.23e-04	4.16e-04	4.70e-03	2.05e-03	8.53e-02		7.50e-01
9	808469	812288	141453	139706	1.12e-02	5.22e-02	4.33e-03	1.43e-03	1.60e-03	0.00e+00	3.54e-03	8.09e-04	4.60e-04	4.28e-03	2.04e-03	8.19e-02		8.32e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.5895 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.5895 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.589549  0.000000
];

