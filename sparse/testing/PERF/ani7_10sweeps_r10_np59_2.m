% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_59 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	819244	813513	141586	141586	4.01e-03	2.75e-02	4.82e-03	6.82e-03	5.24e-04	0.00e+00	4.12e-03	2.44e-04	4.84e-04	5.54e-03	2.45e-03	5.65e-02		5.65e-02
1	811523	818296	128974	134705	3.28e-03	3.13e-02	3.79e-03	2.57e-03	4.68e-04	0.00e+00	5.07e-03	2.32e-04	4.92e-04	4.18e-03	3.28e-03	5.46e-02		1.11e-01
2	806791	824465	137501	130728	3.30e-03	5.21e-02	4.74e-03	1.40e-03	4.57e-04	0.00e+00	6.09e-03	3.74e-04	5.69e-04	3.35e-03	3.56e-03	7.59e-02		1.87e-01
3	822073	807783	143039	125365	3.33e-03	7.03e-02	5.60e-03	1.59e-03	6.17e-04	0.00e+00	6.89e-03	3.29e-04	6.73e-04	3.30e-03	3.42e-03	9.60e-02		2.83e-01
4	812592	810848	128563	142853	3.27e-03	7.14e-02	5.59e-03	2.12e-03	7.92e-04	0.00e+00	7.77e-03	3.18e-04	7.19e-04	3.96e-03	3.17e-03	9.91e-02		3.82e-01
5	809959	808763	138849	140593	3.28e-03	6.30e-02	5.50e-03	1.97e-03	9.72e-04	0.00e+00	7.78e-03	3.74e-04	6.79e-04	3.45e-03	3.18e-03	9.02e-02		4.72e-01
6	810656	811822	142288	143484	3.29e-03	6.42e-02	5.49e-03	2.01e-03	7.21e-04	0.00e+00	7.67e-03	4.27e-04	6.86e-04	3.41e-03	3.23e-03	9.11e-02		5.64e-01
7	813781	812061	142397	141231	3.32e-03	6.33e-02	5.54e-03	1.62e-03	7.49e-04	0.00e+00	7.69e-03	3.80e-04	6.98e-04	3.29e-03	3.23e-03	8.99e-02		6.53e-01
8	811957	812487	140078	141798	3.33e-03	6.53e-02	5.61e-03	1.37e-03	1.09e-03	0.00e+00	7.77e-03	3.64e-04	7.02e-04	3.40e-03	3.26e-03	9.22e-02		7.46e-01
9	812967	813635	142708	142178	3.32e-03	6.42e-02	5.58e-03	1.46e-03	1.21e-03	0.00e+00	7.76e-03	3.51e-04	6.82e-04	3.32e-03	3.24e-03	9.12e-02		8.37e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.2186 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.2186 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.218641  0.000000
];

