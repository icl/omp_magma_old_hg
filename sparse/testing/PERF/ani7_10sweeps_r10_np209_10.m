% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_209 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	818961	801417	141586	141586	9.69e-03	4.01e-02	8.31e-03	1.82e-02	1.02e-03	0.00e+00	2.61e-03	4.95e-04	3.80e-04	1.18e-02	1.91e-03	9.46e-02		9.46e-02
1	808091	794218	129257	146801	9.59e-03	3.71e-02	3.67e-03	6.23e-03	9.61e-04	0.00e+00	3.23e-03	5.65e-04	3.76e-04	6.58e-03	2.13e-03	7.04e-02		1.65e-01
2	806996	798461	140933	154806	9.46e-03	5.50e-02	3.85e-03	3.30e-03	3.64e-04	0.00e+00	3.58e-03	5.40e-04	4.13e-04	4.33e-03	2.14e-03	8.29e-02		2.48e-01
3	797968	810239	142834	151369	9.52e-03	5.34e-02	4.14e-03	2.26e-03	4.41e-04	0.00e+00	3.97e-03	5.33e-04	4.73e-04	4.29e-03	2.13e-03	8.12e-02		3.29e-01
4	811473	815607	152668	140397	9.72e-03	5.60e-02	4.23e-03	3.17e-03	4.91e-04	0.00e+00	4.18e-03	7.16e-04	4.52e-04	5.24e-03	2.13e-03	8.63e-02		4.15e-01
5	811729	799540	139968	135834	9.71e-03	5.15e-02	4.47e-03	3.71e-03	1.35e-03	0.00e+00	4.26e-03	7.75e-04	4.85e-04	4.62e-03	2.06e-03	8.29e-02		4.98e-01
6	816254	811112	140518	152707	9.54e-03	5.59e-02	4.29e-03	2.35e-03	4.29e-04	0.00e+00	4.18e-03	6.39e-04	4.92e-04	4.86e-03	2.15e-03	8.49e-02		5.83e-01
7	815014	810092	136799	141941	9.67e-03	5.25e-02	4.42e-03	3.18e-03	4.41e-04	0.00e+00	4.15e-03	5.68e-04	4.46e-04	4.61e-03	2.16e-03	8.21e-02		6.65e-01
8	808602	811127	138845	143767	9.68e-03	5.74e-02	4.42e-03	3.84e-03	1.37e-03	0.00e+00	4.25e-03	7.73e-04	4.48e-04	4.39e-03	2.10e-03	8.87e-02		7.54e-01
9	814774	816180	146063	143538	9.66e-03	5.16e-02	4.43e-03	2.80e-03	1.37e-03	0.00e+00	4.10e-03	7.15e-04	4.77e-04	4.28e-03	2.15e-03	8.16e-02		8.36e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.5856 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.5856 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.585627  0.000000
];

