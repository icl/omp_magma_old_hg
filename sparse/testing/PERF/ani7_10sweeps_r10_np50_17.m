% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_50 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	818661	797095	141586	141586	3.24e-03	2.95e-02	5.33e-03	6.72e-03	8.73e-04	0.00e+00	4.80e-03	2.73e-04	5.50e-04	5.64e-03	2.78e-03	5.97e-02		5.97e-02
1	803091	816140	129557	151123	3.24e-03	3.43e-02	4.18e-03	2.64e-03	9.79e-04	0.00e+00	5.92e-03	2.42e-04	6.07e-04	4.12e-03	3.72e-03	6.00e-02		1.20e-01
2	830388	805395	145933	132884	3.33e-03	5.52e-02	5.48e-03	2.17e-03	6.36e-04	0.00e+00	7.16e-03	3.54e-04	6.14e-04	3.54e-03	4.08e-03	8.25e-02		2.02e-01
3	809173	825584	119442	144435	3.27e-03	7.75e-02	6.35e-03	1.54e-03	5.80e-04	0.00e+00	8.40e-03	3.31e-04	7.29e-04	3.53e-03	4.09e-03	1.06e-01		3.09e-01
4	811237	804730	141463	125052	3.36e-03	7.82e-02	6.51e-03	1.65e-03	8.33e-04	0.00e+00	8.89e-03	3.29e-04	8.07e-04	3.50e-03	3.65e-03	1.08e-01		4.16e-01
5	804225	810749	140204	146711	3.29e-03	6.89e-02	6.34e-03	2.12e-03	9.99e-04	0.00e+00	9.27e-03	4.79e-04	7.99e-04	3.93e-03	3.67e-03	9.98e-02		5.16e-01
6	810186	812763	148022	141498	3.30e-03	7.09e-02	6.39e-03	2.43e-03	9.49e-04	0.00e+00	9.24e-03	2.73e-04	7.96e-04	3.52e-03	3.73e-03	1.01e-01		6.17e-01
7	812792	811432	142867	140290	3.31e-03	7.33e-02	6.48e-03	1.59e-03	9.13e-04	0.00e+00	9.22e-03	3.90e-04	8.04e-04	3.77e-03	3.73e-03	1.04e-01		7.21e-01
8	811424	812219	141067	142427	3.33e-03	7.49e-02	6.57e-03	2.13e-03	1.39e-03	0.00e+00	9.26e-03	3.10e-04	7.88e-04	3.56e-03	3.73e-03	1.06e-01		8.27e-01
9	811766	813098	143241	142446	3.31e-03	7.40e-02	6.52e-03	1.70e-03	1.02e-03	0.00e+00	8.96e-03	3.03e-04	7.55e-04	3.51e-03	3.73e-03	1.04e-01		9.31e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.3054 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.3054 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.305363  0.000000
];

