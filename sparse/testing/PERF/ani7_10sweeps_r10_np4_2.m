% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_4 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	830271	810752	141586	141586	5.76e-03	2.47e-01	5.49e-02	1.46e-02	3.47e-03	0.00e+00	5.36e-02	2.65e-04	5.72e-03	2.16e-02	3.54e-02	4.43e-01		4.43e-01
1	803577	819513	117947	137466	5.47e-03	3.01e-01	5.53e-02	1.13e-02	5.08e-03	0.00e+00	6.33e-02	2.74e-04	5.94e-03	2.04e-02	4.52e-02	5.13e-01		9.55e-01
2	813206	808589	145447	129511	5.77e-03	5.21e-01	8.93e-02	1.05e-02	5.08e-03	0.00e+00	7.53e-02	3.20e-04	6.58e-03	2.08e-02	4.90e-02	7.84e-01		1.74e+00
3	803340	810095	136624	141241	5.66e-03	7.20e-01	7.67e-02	1.21e-02	6.14e-03	0.00e+00	8.70e-02	4.20e-04	7.76e-03	2.01e-02	4.60e-02	9.81e-01		2.72e+00
4	809507	799234	147296	140541	5.67e-03	6.71e-01	7.89e-02	1.38e-02	8.11e-03	0.00e+00	9.13e-02	3.49e-04	7.91e-03	1.99e-02	4.48e-02	9.42e-01		3.66e+00
5	809947	805492	141934	152207	5.55e-03	6.65e-01	7.93e-02	1.31e-02	8.24e-03	0.00e+00	9.09e-02	3.14e-04	7.89e-03	2.05e-02	5.68e-02	9.48e-01		4.61e+00
6	812997	807429	142300	146755	7.30e-03	6.85e-01	8.04e-02	1.24e-02	7.91e-03	0.00e+00	8.99e-02	2.62e-04	7.78e-03	2.01e-02	4.57e-02	9.57e-01		5.57e+00
7	811777	812299	140056	145624	5.63e-03	6.97e-01	8.15e-02	1.33e-02	8.72e-03	0.00e+00	9.56e-02	3.94e-04	8.11e-03	2.06e-02	4.61e-02	9.77e-01		6.54e+00
8	813506	812456	142082	141560	5.68e-03	7.12e-01	8.24e-02	1.22e-02	8.64e-03	0.00e+00	9.28e-02	3.69e-04	7.99e-03	2.02e-02	4.62e-02	9.89e-01		7.53e+00
9	813992	812720	141159	142209	5.72e-03	7.17e-01	8.28e-02	1.25e-02	8.79e-03	0.00e+00	9.49e-02	4.32e-04	8.06e-03	2.01e-02	4.62e-02	9.97e-01		8.53e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 9.1456 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 9.1456 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  9.145588  0.000000
];

