% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_207 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	816247	813079	141586	141586	9.61e-03	4.04e-02	8.20e-03	1.82e-02	9.62e-04	0.00e+00	2.59e-03	5.15e-04	3.71e-04	1.27e-02	1.98e-03	9.55e-02		9.55e-02
1	810805	809427	131971	135139	9.62e-03	3.63e-02	3.76e-03	6.94e-03	1.38e-03	0.00e+00	3.27e-03	4.72e-04	3.74e-04	7.48e-03	2.23e-03	7.18e-02		1.67e-01
2	813055	796782	138219	139597	9.56e-03	5.52e-02	3.94e-03	2.18e-03	3.26e-04	0.00e+00	3.67e-03	6.51e-04	4.66e-04	4.35e-03	2.19e-03	8.25e-02		2.50e-01
3	804563	806385	136775	153048	9.47e-03	5.36e-02	4.14e-03	3.50e-03	4.30e-04	0.00e+00	4.03e-03	4.32e-04	4.66e-04	4.32e-03	2.16e-03	8.25e-02		3.32e-01
4	804041	809456	146073	144251	9.56e-03	5.45e-02	4.19e-03	2.03e-03	4.85e-04	0.00e+00	4.15e-03	7.30e-04	4.80e-04	5.19e-03	2.11e-03	8.35e-02		4.16e-01
5	809042	809413	147400	141985	9.57e-03	5.02e-02	4.20e-03	3.07e-03	9.41e-04	0.00e+00	4.31e-03	4.60e-04	4.96e-04	4.66e-03	2.08e-03	8.00e-02		4.96e-01
6	810213	810508	143205	142834	9.58e-03	5.45e-02	4.98e-03	2.16e-03	5.17e-04	0.00e+00	4.59e-03	6.02e-04	4.74e-04	4.85e-03	2.36e-03	8.46e-02		5.80e-01
7	812018	809764	142840	142545	1.12e-02	5.18e-02	4.97e-03	2.57e-03	4.80e-04	0.00e+00	4.76e-03	6.74e-04	4.61e-04	4.28e-03	2.39e-03	8.35e-02		6.64e-01
8	812135	811807	141841	144095	1.12e-02	5.63e-02	5.01e-03	3.47e-03	1.13e-03	0.00e+00	4.79e-03	7.65e-04	4.48e-04	4.58e-03	2.38e-03	9.00e-02		7.54e-01
9	815112	814289	142530	142858	1.09e-02	5.31e-02	5.04e-03	1.70e-03	1.37e-03	0.00e+00	4.64e-03	7.34e-04	4.46e-04	4.39e-03	2.37e-03	8.47e-02		8.39e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.5852 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.5852 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.585177  0.000000
];

