% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_16 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	823076	811651	141586	141586	3.82e-03	7.08e-02	1.44e-02	8.11e-03	1.70e-03	0.00e+00	1.56e-02	2.98e-04	1.60e-03	8.55e-03	9.23e-03	1.34e-01		1.34e-01
1	804192	820764	125142	136567	4.83e-03	8.76e-02	1.40e-02	5.11e-03	1.88e-03	0.00e+00	1.94e-02	2.86e-04	1.73e-03	7.51e-03	1.19e-02	1.54e-01		2.88e-01
2	793582	806467	144832	128260	3.97e-03	1.44e-01	1.81e-02	4.45e-03	1.56e-03	0.00e+00	2.38e-02	2.79e-04	1.87e-03	6.83e-03	1.22e-02	2.17e-01		5.06e-01
3	816777	820768	156248	143363	3.90e-03	1.86e-01	1.93e-02	4.52e-03	2.18e-03	0.00e+00	2.62e-02	3.09e-04	2.18e-03	6.89e-03	1.24e-02	2.64e-01		7.69e-01
4	806111	808948	133859	129868	3.96e-03	2.05e-01	2.14e-02	4.12e-03	2.48e-03	0.00e+00	2.91e-02	2.82e-04	2.23e-03	6.80e-03	1.17e-02	2.87e-01		1.06e+00
5	810071	813981	145330	142493	3.89e-03	1.91e-01	2.04e-02	3.99e-03	2.28e-03	0.00e+00	2.89e-02	4.77e-04	2.12e-03	7.39e-03	1.19e-02	2.73e-01		1.33e+00
6	811820	811049	142176	138266	3.91e-03	2.05e-01	2.09e-02	3.59e-03	2.15e-03	0.00e+00	2.88e-02	3.57e-04	2.17e-03	6.99e-03	1.19e-02	2.86e-01		1.62e+00
7	807140	814206	141233	142004	3.94e-03	2.06e-01	2.09e-02	3.75e-03	2.20e-03	0.00e+00	2.90e-02	3.99e-04	2.16e-03	6.93e-03	1.19e-02	2.87e-01		1.90e+00
8	815355	806488	146719	139653	3.93e-03	2.07e-01	2.09e-02	3.60e-03	2.30e-03	0.00e+00	2.94e-02	3.94e-04	2.18e-03	6.98e-03	1.19e-02	2.89e-01		2.19e+00
9	808851	813524	139310	148177	3.92e-03	2.06e-01	2.11e-02	3.91e-03	2.58e-03	0.00e+00	2.89e-02	3.33e-04	2.14e-03	6.89e-03	1.19e-02	2.88e-01		2.48e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 2.8950 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 2.8950 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  2.895005  0.000000
];

