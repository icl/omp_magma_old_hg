% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_137 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	815973	813897	141586	141586	7.34e-03	3.41e-02	6.08e-03	1.36e-02	2.11e-03	0.00e+00	2.80e-03	5.24e-04	4.44e-04	9.46e-03	2.30e-03	7.88e-02		7.88e-02
1	805224	818514	132245	134321	7.38e-03	3.09e-02	4.34e-03	2.82e-03	1.70e-03	0.00e+00	3.42e-03	5.06e-04	3.95e-04	5.71e-03	2.34e-03	5.95e-02		1.38e-01
2	811376	808439	143800	130510	7.44e-03	4.73e-02	4.50e-03	3.80e-03	3.25e-04	0.00e+00	3.79e-03	6.04e-04	4.52e-04	4.05e-03	2.38e-03	7.46e-02		2.13e-01
3	801111	795277	138454	141391	7.33e-03	5.17e-02	4.59e-03	1.68e-03	3.73e-04	0.00e+00	4.42e-03	5.38e-04	4.90e-04	4.01e-03	2.19e-03	7.73e-02		2.90e-01
4	804554	801955	149525	155359	7.20e-03	5.01e-02	4.51e-03	2.21e-03	5.46e-04	0.00e+00	4.65e-03	5.45e-04	5.46e-04	4.27e-03	2.12e-03	7.67e-02		3.67e-01
5	818135	813550	146887	149486	7.30e-03	4.68e-02	4.43e-03	3.41e-03	4.80e-04	0.00e+00	4.59e-03	4.42e-04	4.81e-04	4.53e-03	2.22e-03	7.47e-02		4.42e-01
6	812470	809160	134112	138697	7.40e-03	5.31e-02	4.56e-03	2.34e-03	4.12e-04	0.00e+00	4.56e-03	6.25e-04	5.10e-04	4.23e-03	2.21e-03	7.99e-02		5.22e-01
7	814405	812332	140583	143893	7.34e-03	4.96e-02	4.51e-03	2.81e-03	3.88e-04	0.00e+00	4.53e-03	6.19e-04	5.03e-04	4.04e-03	2.21e-03	7.66e-02		5.98e-01
8	812606	811646	139454	141527	7.37e-03	5.32e-02	4.48e-03	1.54e-03	4.13e-04	0.00e+00	4.68e-03	6.82e-04	5.09e-04	4.11e-03	2.20e-03	7.92e-02		6.77e-01
9	806746	813559	142059	143019	7.42e-03	5.01e-02	4.45e-03	2.30e-03	1.18e-03	0.00e+00	4.50e-03	6.21e-04	4.75e-04	4.14e-03	2.18e-03	7.73e-02		7.55e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.3834 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.3834 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.383363  0.000000
];

