% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_197 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	821165	809027	141586	141586	8.79e-03	3.68e-02	7.69e-03	1.64e-02	7.78e-04	0.00e+00	2.70e-03	4.23e-04	3.29e-04	1.06e-02	1.74e-03	8.63e-02		8.63e-02
1	813461	799994	127053	139191	8.84e-03	3.29e-02	3.25e-03	6.00e-03	8.42e-04	0.00e+00	3.34e-03	3.97e-04	3.89e-04	6.08e-03	1.96e-03	6.40e-02		1.50e-01
2	790348	812088	135563	149030	8.75e-03	4.52e-02	3.74e-03	2.35e-03	2.83e-04	0.00e+00	3.80e-03	5.42e-04	4.07e-04	3.95e-03	2.13e-03	7.12e-02		2.21e-01
3	799513	801922	159482	137742	8.89e-03	4.79e-02	4.14e-03	2.12e-03	4.18e-04	0.00e+00	4.25e-03	5.15e-04	4.72e-04	3.87e-03	2.10e-03	7.47e-02		2.96e-01
4	817548	812168	151123	148714	8.77e-03	5.02e-02	4.21e-03	2.12e-03	4.41e-04	0.00e+00	4.31e-03	6.36e-04	4.44e-04	4.94e-03	2.20e-03	7.82e-02		3.74e-01
5	806785	808183	133893	139273	8.86e-03	5.10e-02	4.45e-03	1.78e-03	1.20e-03	0.00e+00	4.44e-03	5.69e-04	4.49e-04	4.46e-03	2.20e-03	7.94e-02		4.54e-01
6	811469	812015	145462	144064	8.85e-03	5.35e-02	4.39e-03	1.81e-03	4.73e-04	0.00e+00	4.33e-03	6.78e-04	4.34e-04	4.51e-03	2.17e-03	8.12e-02		5.35e-01
7	808272	808716	141584	141038	8.88e-03	5.13e-02	4.47e-03	1.73e-03	4.58e-04	0.00e+00	4.41e-03	5.00e-04	4.40e-04	4.03e-03	2.17e-03	7.84e-02		6.13e-01
8	811347	810370	145587	145143	8.82e-03	5.41e-02	4.43e-03	1.57e-03	1.24e-03	0.00e+00	4.33e-03	7.78e-04	4.39e-04	4.15e-03	2.21e-03	8.21e-02		6.95e-01
9	812330	810034	143318	144295	8.87e-03	5.14e-02	4.47e-03	1.34e-03	1.35e-03	0.00e+00	4.32e-03	5.54e-04	4.75e-04	3.94e-03	2.17e-03	7.89e-02		7.74e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.4050 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.4050 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.404966  0.000000
];

