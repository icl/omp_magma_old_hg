% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_189 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	818197	814823	141586	141586	1.09e-02	3.64e-02	8.23e-03	1.56e-02	9.33e-04	0.00e+00	2.75e-03	4.46e-04	3.70e-04	1.04e-02	1.99e-03	8.81e-02		8.81e-02
1	813124	800199	130021	133395	1.10e-02	3.16e-02	4.01e-03	3.78e-03	9.10e-04	0.00e+00	2.91e-03	4.44e-04	4.06e-04	6.08e-03	2.35e-03	6.35e-02		1.52e-01
2	807905	807999	135900	148825	1.08e-02	4.52e-02	3.81e-03	1.39e-03	3.49e-04	0.00e+00	3.02e-03	5.33e-04	4.14e-04	5.31e-03	2.23e-03	7.30e-02		2.25e-01
3	803209	801189	141925	141831	8.66e-03	4.77e-02	4.28e-03	2.05e-03	3.84e-04	0.00e+00	3.38e-03	4.39e-04	4.67e-04	3.92e-03	2.17e-03	7.35e-02		2.98e-01
4	812359	805327	147427	149447	8.53e-03	4.85e-02	4.32e-03	1.70e-03	3.59e-04	0.00e+00	3.68e-03	6.40e-04	4.18e-04	3.99e-03	2.21e-03	7.44e-02		3.72e-01
5	808735	812143	139082	146114	8.63e-03	4.61e-02	4.48e-03	2.58e-03	1.13e-03	0.00e+00	3.97e-03	7.00e-04	4.24e-04	4.50e-03	2.23e-03	7.48e-02		4.47e-01
6	809843	809041	143512	140104	8.71e-03	5.09e-02	4.56e-03	2.20e-03	3.93e-04	0.00e+00	3.44e-03	5.47e-04	4.61e-04	4.46e-03	2.20e-03	7.79e-02		5.25e-01
7	812044	808638	143210	144012	8.65e-03	4.70e-02	4.53e-03	2.64e-03	4.59e-04	0.00e+00	3.35e-03	4.55e-04	4.72e-04	4.20e-03	2.25e-03	7.40e-02		5.99e-01
8	813122	814641	141815	145221	8.73e-03	5.14e-02	4.58e-03	1.90e-03	1.05e-03	0.00e+00	3.90e-03	5.93e-04	4.27e-04	4.11e-03	2.26e-03	7.89e-02		6.78e-01
9	813134	813831	141543	140024	8.72e-03	4.83e-02	4.66e-03	2.32e-03	1.24e-03	0.00e+00	3.36e-03	5.18e-04	4.29e-04	3.88e-03	2.28e-03	7.57e-02		7.54e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.3963 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.3963 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.396294  0.000000
];

