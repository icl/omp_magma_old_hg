% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_48 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	815132	809071	141586	141586	3.28e-03	3.00e-02	5.39e-03	6.78e-03	7.68e-04	0.00e+00	5.05e-03	2.96e-04	5.78e-04	5.81e-03	2.95e-03	6.09e-02		6.09e-02
1	805922	797132	133086	139147	3.29e-03	3.54e-02	4.42e-03	2.92e-03	8.30e-04	0.00e+00	6.15e-03	2.45e-04	5.89e-04	4.14e-03	3.87e-03	6.18e-02		1.23e-01
2	806450	820594	143102	151892	3.29e-03	5.63e-02	5.64e-03	1.70e-03	5.84e-04	0.00e+00	7.40e-03	2.54e-04	6.34e-04	3.90e-03	4.26e-03	8.39e-02		2.07e-01
3	812352	793937	143380	129236	3.36e-03	8.05e-02	6.74e-03	2.33e-03	6.51e-04	0.00e+00	8.58e-03	2.49e-04	7.76e-04	3.51e-03	3.87e-03	1.11e-01		3.17e-01
4	803614	809374	138284	156699	3.23e-03	7.47e-02	6.55e-03	1.71e-03	9.51e-04	0.00e+00	9.22e-03	3.45e-04	8.08e-04	3.53e-03	3.82e-03	1.05e-01		4.22e-01
5	815421	802775	147827	142067	3.30e-03	7.23e-02	6.58e-03	2.04e-03	8.71e-04	0.00e+00	9.40e-03	2.77e-04	8.03e-04	3.81e-03	3.88e-03	1.03e-01		5.25e-01
6	810428	807453	136826	149472	3.28e-03	7.59e-02	6.68e-03	1.86e-03	8.61e-04	0.00e+00	9.32e-03	3.34e-04	7.64e-04	3.57e-03	3.85e-03	1.06e-01		6.32e-01
7	813274	815465	142625	145600	3.28e-03	7.40e-02	6.67e-03	1.48e-03	8.14e-04	0.00e+00	9.26e-03	4.89e-04	7.70e-04	3.83e-03	3.90e-03	1.05e-01		7.36e-01
8	812159	814348	140585	138394	3.32e-03	7.65e-02	6.81e-03	1.85e-03	1.34e-03	0.00e+00	9.44e-03	3.82e-04	8.02e-04	3.58e-03	3.89e-03	1.08e-01		8.44e-01
9	812401	811565	142506	140317	3.34e-03	7.58e-02	6.83e-03	2.00e-03	1.12e-03	0.00e+00	9.32e-03	2.45e-04	8.01e-04	3.58e-03	3.88e-03	1.07e-01		9.51e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.3205 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.3205 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.320540  0.000000
];

