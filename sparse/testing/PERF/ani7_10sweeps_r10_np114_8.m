% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_114 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	805716	813069	141586	141586	5.74e-03	2.83e-02	5.78e-03	9.72e-03	6.62e-04	0.00e+00	2.33e-03	3.54e-04	3.84e-04	7.01e-03	1.94e-03	6.22e-02		6.22e-02
1	798049	802028	142502	135149	5.81e-03	2.72e-02	3.41e-03	2.45e-03	6.74e-04	0.00e+00	3.47e-03	3.07e-04	4.45e-04	4.07e-03	2.26e-03	5.01e-02		1.12e-01
2	808651	810302	150975	146996	6.37e-03	4.06e-02	3.98e-03	3.67e-03	2.96e-04	0.00e+00	3.25e-03	3.74e-04	4.42e-04	3.13e-03	2.52e-03	6.46e-02		1.77e-01
3	806137	802046	141179	139528	5.76e-03	4.69e-02	4.52e-03	2.61e-03	6.59e-04	0.00e+00	3.91e-03	3.99e-04	5.03e-04	3.10e-03	2.40e-03	7.07e-02		2.48e-01
4	814375	812183	144499	148590	5.70e-03	4.64e-02	4.56e-03	2.20e-03	4.66e-04	0.00e+00	4.12e-03	4.46e-04	5.07e-04	3.74e-03	2.40e-03	7.06e-02		3.18e-01
5	808378	808447	137066	139258	5.80e-03	4.46e-02	4.80e-03	2.02e-03	8.44e-04	0.00e+00	4.10e-03	5.65e-04	5.10e-04	3.39e-03	2.46e-03	6.91e-02		3.87e-01
6	816558	812119	143869	143800	5.74e-03	4.59e-02	4.76e-03	1.64e-03	4.25e-04	0.00e+00	3.96e-03	5.20e-04	4.86e-04	3.31e-03	2.48e-03	6.92e-02		4.56e-01
7	808581	812415	136495	140934	5.78e-03	4.62e-02	4.97e-03	1.44e-03	4.61e-04	0.00e+00	3.94e-03	4.93e-04	5.00e-04	3.27e-03	2.45e-03	6.95e-02		5.26e-01
8	810359	811156	145278	141444	5.78e-03	4.72e-02	4.92e-03	3.08e-03	8.45e-04	0.00e+00	4.10e-03	3.39e-04	4.91e-04	3.10e-03	2.46e-03	7.23e-02		5.98e-01
9	812225	814738	144306	143509	5.79e-03	4.52e-02	4.89e-03	1.68e-03	8.30e-04	0.00e+00	4.04e-03	4.94e-04	4.86e-04	3.02e-03	2.47e-03	6.89e-02		6.67e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.1434 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.1434 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.143399  0.000000
];

