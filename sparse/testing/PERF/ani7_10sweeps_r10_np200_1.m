% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_200 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	815978	817536	141586	141586	8.83e-03	3.63e-02	7.58e-03	1.66e-02	1.36e-03	0.00e+00	2.69e-03	3.78e-04	3.62e-04	1.13e-02	1.72e-03	8.71e-02		8.71e-02
1	814058	807521	132240	130682	8.91e-03	3.28e-02	3.20e-03	4.51e-03	8.52e-04	0.00e+00	3.42e-03	3.62e-04	3.66e-04	5.73e-03	1.94e-03	6.21e-02		1.49e-01
2	811055	809801	134966	141503	8.85e-03	4.50e-02	3.79e-03	2.70e-03	4.09e-04	0.00e+00	3.77e-03	4.59e-04	3.94e-04	4.29e-03	2.19e-03	7.18e-02		2.21e-01
3	796960	800808	138775	140029	8.87e-03	5.12e-02	4.27e-03	4.18e-03	3.36e-04	0.00e+00	4.19e-03	3.93e-04	4.45e-04	3.80e-03	2.16e-03	7.99e-02		3.01e-01
4	809874	801778	153676	149828	8.77e-03	5.36e-02	4.27e-03	3.15e-03	4.15e-04	0.00e+00	4.25e-03	7.37e-04	4.55e-04	3.91e-03	2.11e-03	8.17e-02		3.82e-01
5	806704	814378	141567	149663	8.76e-03	4.90e-02	4.31e-03	1.62e-03	4.97e-04	0.00e+00	4.30e-03	6.00e-04	4.23e-04	4.34e-03	2.15e-03	7.60e-02		4.58e-01
6	811138	805821	145543	137869	8.92e-03	5.34e-02	4.40e-03	3.00e-03	4.12e-04	0.00e+00	4.32e-03	6.43e-04	4.43e-04	3.91e-03	2.13e-03	8.16e-02		5.40e-01
7	810029	815064	141915	147232	8.81e-03	5.05e-02	4.36e-03	1.85e-03	4.57e-04	0.00e+00	4.28e-03	6.08e-04	4.26e-04	4.17e-03	2.16e-03	7.77e-02		6.18e-01
8	810693	806822	143830	138795	8.96e-03	5.46e-02	4.42e-03	2.20e-03	1.22e-03	0.00e+00	4.34e-03	7.81e-04	4.37e-04	3.82e-03	2.15e-03	8.30e-02		7.01e-01
9	811939	814190	143972	147843	8.89e-03	5.14e-02	4.40e-03	2.38e-03	6.29e-04	0.00e+00	4.43e-03	5.61e-04	4.68e-04	3.89e-03	2.17e-03	7.92e-02		7.80e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.4039 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.4039 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.403871  0.000000
];

