% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_10 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	821400	810477	141586	141586	4.16e-03	1.06e-01	2.25e-02	9.21e-03	1.72e-03	0.00e+00	2.44e-02	2.94e-04	2.43e-03	1.14e-02	1.46e-02	1.97e-01		1.97e-01
1	812606	809942	126818	137741	4.19e-03	1.33e-01	2.22e-02	4.84e-03	2.25e-03	0.00e+00	3.04e-02	2.35e-04	2.71e-03	1.00e-02	1.87e-02	2.29e-01		4.26e-01
2	800330	811203	136418	139082	4.28e-03	2.16e-01	2.82e-02	5.63e-03	2.06e-03	0.00e+00	3.68e-02	2.91e-04	2.93e-03	9.67e-03	2.01e-02	3.26e-01		7.52e-01
3	800455	809529	149500	138627	4.30e-03	3.07e-01	3.17e-02	6.49e-03	2.82e-03	0.00e+00	4.15e-02	2.43e-04	3.26e-03	9.59e-03	1.86e-02	4.26e-01		1.18e+00
4	809291	809392	150181	141107	4.28e-03	2.79e-01	4.15e-02	5.38e-03	3.30e-03	0.00e+00	4.36e-02	3.09e-04	3.26e-03	9.90e-03	2.11e-02	4.12e-01		1.59e+00
5	812531	808736	142150	142049	5.86e-03	2.93e-01	3.29e-02	5.23e-03	3.02e-03	0.00e+00	4.21e-02	4.56e-04	3.14e-03	1.04e-02	1.86e-02	4.15e-01		2.00e+00
6	813320	813769	139716	143511	4.29e-03	3.01e-01	3.33e-02	5.46e-03	3.43e-03	0.00e+00	4.47e-02	3.00e-04	3.28e-03	9.91e-03	1.88e-02	4.25e-01		2.43e+00
7	812811	811474	139733	139284	4.30e-03	3.08e-01	3.36e-02	5.98e-03	3.23e-03	0.00e+00	4.38e-02	4.16e-04	3.22e-03	9.63e-03	1.87e-02	4.31e-01		2.86e+00
8	813344	810302	141048	142385	4.26e-03	3.10e-01	3.36e-02	5.60e-03	3.49e-03	0.00e+00	4.41e-02	3.71e-04	3.24e-03	9.95e-03	1.87e-02	4.33e-01		3.29e+00
9	816892	814841	141321	144363	4.32e-03	3.09e-01	3.43e-02	5.05e-03	3.48e-03	0.00e+00	4.37e-02	3.77e-04	3.18e-03	9.64e-03	1.88e-02	4.32e-01		3.73e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 4.1815 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 4.1815 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  4.181491  0.000000
];

