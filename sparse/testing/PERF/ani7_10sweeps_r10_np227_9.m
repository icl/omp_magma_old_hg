% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_227 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	825082	808450	141586	141586	1.04e-02	4.28e-02	9.02e-03	1.98e-02	7.41e-04	0.00e+00	2.41e-03	5.01e-04	4.02e-04	1.22e-02	1.77e-03	1.00e-01		1.00e-01
1	795668	801740	123136	139768	1.03e-02	3.43e-02	3.50e-03	4.12e-03	8.52e-04	0.00e+00	3.08e-03	4.63e-04	3.79e-04	6.52e-03	1.94e-03	6.54e-02		1.66e-01
2	807417	814883	153356	147284	1.03e-02	5.14e-02	3.77e-03	1.68e-03	3.16e-04	0.00e+00	3.28e-03	6.65e-04	3.82e-04	4.23e-03	2.11e-03	7.81e-02		2.44e-01
3	820071	806078	142413	134947	1.04e-02	5.31e-02	4.35e-03	1.83e-03	3.90e-04	0.00e+00	3.59e-03	5.46e-04	4.06e-04	4.19e-03	2.05e-03	8.08e-02		3.24e-01
4	816661	807130	130565	144558	1.03e-02	4.16e-01	4.48e-03	3.09e-03	6.22e-04	0.00e+00	3.90e-03	6.53e-04	4.44e-04	5.48e-03	2.02e-03	4.47e-01		7.72e-01
5	805931	811633	134780	144311	1.03e-02	5.05e-02	4.66e-03	3.13e-03	1.47e-03	0.00e+00	3.94e-03	6.18e-04	4.18e-04	4.69e-03	2.05e-03	8.17e-02		8.53e-01
6	813250	808181	146316	140614	1.03e-02	5.51e-02	4.66e-03	4.39e-03	4.39e-04	0.00e+00	3.85e-03	5.34e-04	4.46e-04	4.98e-03	2.00e-03	8.67e-02		9.40e-01
7	809105	812721	139803	144872	1.03e-02	5.11e-02	4.68e-03	2.06e-03	3.83e-04	0.00e+00	3.88e-03	7.97e-04	4.17e-04	4.30e-03	2.04e-03	7.99e-02		1.02e+00
8	812680	816520	144754	141138	1.03e-02	5.56e-02	4.70e-03	3.00e-03	1.33e-03	0.00e+00	3.95e-03	9.25e-04	4.23e-04	4.40e-03	2.09e-03	8.68e-02		1.11e+00
9	816461	812989	141985	138145	1.04e-02	5.13e-02	4.81e-03	1.98e-03	1.52e-03	0.00e+00	4.00e-03	6.16e-04	4.48e-04	4.28e-03	2.20e-03	8.16e-02		1.19e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.8360 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.8360 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.836023  0.000000
];

