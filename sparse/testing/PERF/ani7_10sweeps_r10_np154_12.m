% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_154 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	809503	812049	141586	141586	7.78e-03	3.73e-02	7.68e-03	1.49e-02	7.71e-04	0.00e+00	2.49e-03	4.06e-04	4.26e-04	1.00e-02	2.09e-03	8.38e-02		8.38e-02
1	797615	804235	138715	136169	7.82e-03	2.94e-02	3.94e-03	5.34e-03	1.17e-03	0.00e+00	3.16e-03	4.18e-04	4.05e-04	6.21e-03	2.17e-03	6.00e-02		1.44e-01
2	795398	812773	151409	144789	7.70e-03	4.64e-02	4.17e-03	3.90e-03	3.84e-04	0.00e+00	3.40e-03	4.60e-04	4.32e-04	4.00e-03	2.19e-03	7.30e-02		2.17e-01
3	796744	809865	154432	137057	7.79e-03	4.70e-02	4.44e-03	2.47e-03	4.62e-04	0.00e+00	3.86e-03	4.81e-04	4.61e-04	4.00e-03	2.19e-03	7.32e-02		2.90e-01
4	812503	802239	153892	140771	7.75e-03	4.84e-02	4.73e-03	3.14e-03	3.91e-04	0.00e+00	4.15e-03	7.39e-04	4.51e-04	4.40e-03	2.23e-03	7.64e-02		3.66e-01
5	810097	809345	138938	149202	7.67e-03	4.47e-02	5.00e-03	1.59e-03	4.23e-04	0.00e+00	4.14e-03	4.22e-04	4.37e-04	4.49e-03	2.31e-03	7.12e-02		4.38e-01
6	810360	814876	142150	142902	7.78e-03	5.11e-02	5.48e-03	1.95e-03	5.01e-04	0.00e+00	4.53e-03	8.01e-04	4.76e-04	5.97e-03	2.67e-03	8.12e-02		5.19e-01
7	809217	814263	142693	138177	9.11e-03	4.68e-02	5.53e-03	2.54e-03	3.62e-04	0.00e+00	4.74e-03	7.17e-04	4.61e-04	3.97e-03	2.71e-03	7.69e-02		5.96e-01
8	809895	813376	144642	139596	8.88e-03	5.05e-02	5.00e-03	1.50e-03	6.09e-04	0.00e+00	4.13e-03	5.07e-04	4.41e-04	4.23e-03	2.27e-03	7.80e-02		6.74e-01
9	811991	812508	144770	141289	7.81e-03	4.63e-02	5.03e-03	2.31e-03	1.06e-03	0.00e+00	4.20e-03	5.60e-04	4.68e-04	4.03e-03	2.25e-03	7.40e-02		7.48e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.3852 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.3852 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.385244  0.000000
];

