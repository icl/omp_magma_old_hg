% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_84 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	811840	813747	141586	141586	5.30e-03	2.98e-02	6.56e-03	9.63e-03	8.08e-04	0.00e+00	3.13e-03	3.34e-04	5.27e-04	5.67e-03	2.53e-03	6.43e-02		6.43e-02
1	801791	795766	136378	134471	5.33e-03	2.85e-02	4.57e-03	2.85e-03	9.87e-04	0.00e+00	3.73e-03	2.84e-04	5.04e-04	4.05e-03	2.49e-03	5.33e-02		1.18e-01
2	804812	819449	147233	153258	5.20e-03	4.75e-02	4.97e-03	2.32e-03	7.71e-04	0.00e+00	4.37e-03	3.11e-04	4.64e-04	3.34e-03	2.68e-03	7.19e-02		1.90e-01
3	819990	808722	145018	130381	5.34e-03	5.73e-02	5.69e-03	2.16e-03	4.97e-04	0.00e+00	5.13e-03	3.43e-04	5.74e-04	3.28e-03	2.78e-03	8.31e-02		2.73e-01
4	813164	814276	130646	141914	5.26e-03	5.81e-02	5.99e-03	1.95e-03	6.44e-04	0.00e+00	5.51e-03	6.48e-04	5.81e-04	3.31e-03	3.07e-03	8.51e-02		3.58e-01
5	810575	815473	138277	137165	5.31e-03	5.21e-02	6.19e-03	1.47e-03	5.56e-04	0.00e+00	5.33e-03	4.08e-04	5.64e-04	3.70e-03	3.11e-03	7.87e-02		4.36e-01
6	809811	813026	141672	136774	5.30e-03	5.43e-02	6.39e-03	2.10e-03	5.39e-04	0.00e+00	5.48e-03	3.92e-04	5.66e-04	3.27e-03	3.09e-03	8.15e-02		5.18e-01
7	814391	811303	143242	140027	5.32e-03	5.21e-02	6.39e-03	1.53e-03	5.18e-04	0.00e+00	5.45e-03	5.40e-04	5.81e-04	3.48e-03	3.11e-03	7.91e-02		5.97e-01
8	813886	811374	139468	142556	5.31e-03	5.44e-02	6.44e-03	1.61e-03	1.01e-03	0.00e+00	5.47e-03	4.51e-04	5.71e-04	3.37e-03	2.79e-03	8.15e-02		6.78e-01
9	809854	813519	140779	143291	5.28e-03	5.25e-02	6.42e-03	1.41e-03	1.22e-03	0.00e+00	5.41e-03	4.46e-04	5.39e-04	3.37e-03	2.79e-03	7.93e-02		7.58e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.2434 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.2434 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.243427  0.000000
];

