% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_113 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	808075	808209	141586	141586	5.77e-03	2.85e-02	5.81e-03	1.04e-02	1.69e-03	0.00e+00	2.40e-03	2.89e-04	3.79e-04	6.77e-03	1.98e-03	6.39e-02		6.39e-02
1	797048	808692	140143	140009	5.83e-03	2.81e-02	3.48e-03	2.29e-03	1.35e-03	0.00e+00	2.82e-03	2.82e-04	4.62e-04	4.29e-03	2.32e-03	5.12e-02		1.15e-01
2	803950	796568	151976	140332	5.75e-03	4.07e-02	4.09e-03	2.08e-03	7.55e-04	0.00e+00	3.27e-03	3.51e-04	4.65e-04	3.20e-03	2.51e-03	6.31e-02		1.78e-01
3	817311	807644	145880	153262	5.72e-03	4.50e-02	4.48e-03	2.15e-03	4.22e-04	0.00e+00	3.80e-03	2.83e-04	5.03e-04	3.12e-03	2.50e-03	6.79e-02		2.46e-01
4	813914	807965	133325	142992	5.77e-03	4.81e-02	4.80e-03	2.64e-03	6.20e-04	0.00e+00	4.08e-03	5.15e-04	5.34e-04	3.11e-03	2.45e-03	7.26e-02		3.19e-01
5	811070	813347	137527	143476	5.75e-03	4.47e-02	6.76e-03	1.83e-03	5.26e-04	0.00e+00	4.17e-03	2.46e-04	5.09e-04	3.63e-03	3.29e-03	7.14e-02		3.90e-01
6	812454	812767	141177	138900	8.66e-03	4.75e-02	6.89e-03	2.92e-03	5.26e-04	0.00e+00	4.21e-03	3.60e-04	5.39e-04	3.32e-03	3.30e-03	7.83e-02		4.69e-01
7	814671	810765	140599	140286	8.65e-03	4.56e-02	6.87e-03	2.98e-03	4.63e-04	0.00e+00	4.12e-03	4.61e-04	5.15e-04	3.10e-03	3.30e-03	7.61e-02		5.45e-01
8	814141	811826	139188	143094	8.61e-03	4.82e-02	6.90e-03	1.39e-03	4.57e-04	0.00e+00	4.12e-03	5.57e-04	5.47e-04	3.19e-03	3.30e-03	7.73e-02		6.22e-01
9	813282	812863	140524	142839	8.64e-03	4.58e-02	6.64e-03	2.68e-03	9.47e-04	0.00e+00	4.23e-03	6.69e-04	5.08e-04	3.08e-03	2.44e-03	7.56e-02		6.98e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.1710 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.1710 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.170964  0.000000
];

