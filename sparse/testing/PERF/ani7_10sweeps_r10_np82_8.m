% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_82 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	823573	807283	141586	141586	5.24e-03	3.01e-02	6.66e-03	9.35e-03	8.23e-04	0.00e+00	3.90e-03	3.08e-04	5.18e-04	6.19e-03	2.59e-03	6.57e-02		6.57e-02
1	812301	791200	124645	140935	5.30e-03	2.83e-02	4.70e-03	2.82e-03	1.02e-03	0.00e+00	4.78e-03	2.68e-04	4.66e-04	4.01e-03	2.63e-03	5.43e-02		1.20e-01
2	791466	818802	136723	157824	5.15e-03	4.79e-02	5.02e-03	1.95e-03	5.89e-04	0.00e+00	5.69e-03	3.08e-04	4.74e-04	3.37e-03	2.71e-03	7.31e-02		1.93e-01
3	806044	809041	158364	131028	5.34e-03	5.46e-02	5.30e-03	2.62e-03	5.72e-04	0.00e+00	5.18e-03	4.20e-04	5.92e-04	3.27e-03	2.79e-03	8.06e-02		2.74e-01
4	805813	810666	144592	141595	5.24e-03	5.48e-02	5.72e-03	1.91e-03	5.38e-04	0.00e+00	5.42e-03	4.12e-04	5.58e-04	3.37e-03	2.85e-03	8.09e-02		3.55e-01
5	814810	806542	145628	140775	5.25e-03	5.19e-02	5.92e-03	1.84e-03	5.13e-04	0.00e+00	5.54e-03	5.45e-04	5.56e-04	3.72e-03	2.88e-03	7.87e-02		4.33e-01
6	810170	811708	137437	145705	5.23e-03	5.46e-02	6.05e-03	1.71e-03	5.65e-04	0.00e+00	5.52e-03	3.37e-04	5.38e-04	3.42e-03	2.85e-03	8.08e-02		5.14e-01
7	813721	811890	142883	141345	5.27e-03	5.24e-02	6.02e-03	1.67e-03	5.75e-04	0.00e+00	5.59e-03	5.23e-04	5.42e-04	3.65e-03	2.82e-03	7.90e-02		5.93e-01
8	812884	812271	140138	141969	5.26e-03	5.46e-02	6.11e-03	2.22e-03	9.73e-04	0.00e+00	5.56e-03	4.47e-04	5.67e-04	3.35e-03	2.86e-03	8.19e-02		6.75e-01
9	815583	814314	141781	142394	5.26e-03	5.33e-02	6.14e-03	2.34e-03	1.04e-03	0.00e+00	5.56e-03	5.55e-04	5.82e-04	3.39e-03	2.86e-03	8.10e-02		7.56e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.2290 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.2290 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.228985  0.000000
];

