% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_134 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	812023	806168	141586	141586	5.98e-03	2.81e-02	5.85e-03	1.08e-02	5.28e-04	0.00e+00	2.73e-03	3.29e-04	3.34e-04	7.17e-03	1.70e-03	6.36e-02		6.36e-02
1	792083	797179	136195	142050	6.01e-03	2.68e-02	2.95e-03	4.25e-03	6.20e-04	0.00e+00	3.27e-03	3.46e-04	3.67e-04	5.06e-03	2.15e-03	5.18e-02		1.15e-01
2	813482	810604	156941	151845	5.95e-03	4.44e-02	3.60e-03	2.35e-03	2.81e-04	0.00e+00	3.80e-03	2.18e-04	4.10e-04	3.05e-03	2.43e-03	6.65e-02		1.82e-01
3	815520	803842	136348	139226	8.31e-03	5.13e-02	5.07e-03	1.54e-03	3.42e-04	0.00e+00	4.39e-03	4.26e-04	5.08e-04	3.03e-03	2.44e-03	7.74e-02		2.59e-01
4	813771	823426	135116	146794	8.27e-03	5.07e-02	5.24e-03	1.81e-03	4.96e-04	0.00e+00	4.54e-03	3.38e-04	4.84e-04	3.59e-03	2.51e-03	7.80e-02		3.37e-01
5	809042	817182	137670	128015	8.45e-03	4.95e-02	5.47e-03	1.21e-03	9.64e-04	0.00e+00	4.58e-03	5.88e-04	5.08e-04	3.41e-03	2.51e-03	7.71e-02		4.14e-01
6	813730	810585	143205	135065	8.43e-03	5.59e-02	5.42e-03	2.82e-03	4.57e-04	0.00e+00	4.64e-03	3.79e-04	4.71e-04	3.51e-03	2.52e-03	8.45e-02		4.99e-01
7	812004	810660	139323	142468	8.33e-03	4.85e-02	4.35e-03	1.52e-03	6.04e-04	0.00e+00	4.80e-03	5.11e-04	4.85e-04	3.10e-03	2.22e-03	7.44e-02		5.73e-01
8	814339	811303	141855	143199	6.05e-03	5.04e-02	4.35e-03	2.05e-03	7.82e-04	0.00e+00	5.38e-03	4.86e-04	5.96e-04	3.24e-03	3.45e-03	7.68e-02		6.50e-01
9	813568	813639	140326	143362	9.78e-03	5.02e-02	4.35e-03	1.88e-03	9.37e-04	0.00e+00	4.66e-03	4.18e-04	5.02e-04	3.00e-03	2.23e-03	7.79e-02		7.28e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.2209 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.2209 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.220941  0.000000
];

