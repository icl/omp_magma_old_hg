% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_160 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	821198	813621	141586	141586	7.94e-03	3.61e-02	7.63e-03	1.50e-02	2.35e-03	0.00e+00	2.39e-03	5.22e-04	3.73e-04	9.62e-03	2.09e-03	8.41e-02		8.41e-02
1	809424	819701	127020	134597	8.00e-03	3.05e-02	3.96e-03	2.80e-03	1.63e-03	0.00e+00	2.88e-03	3.89e-04	4.09e-04	5.78e-03	2.27e-03	5.86e-02		1.43e-01
2	814603	802493	139600	129323	8.01e-03	4.61e-02	4.28e-03	2.07e-03	8.76e-04	0.00e+00	3.42e-03	4.60e-04	4.00e-04	3.88e-03	2.37e-03	7.18e-02		2.15e-01
3	809970	806198	135227	147337	7.85e-03	4.66e-02	4.69e-03	1.86e-03	2.91e-04	0.00e+00	3.68e-03	4.82e-04	4.79e-04	3.90e-03	2.40e-03	7.22e-02		2.87e-01
4	815164	803788	140666	144438	7.88e-03	4.92e-02	5.00e-03	2.19e-03	6.87e-04	0.00e+00	4.05e-03	6.94e-04	4.82e-04	4.07e-03	2.49e-03	7.67e-02		3.63e-01
5	801068	814455	136277	147653	7.83e-03	4.32e-02	5.14e-03	1.82e-03	3.85e-04	0.00e+00	3.79e-03	6.86e-04	4.43e-04	4.36e-03	2.48e-03	7.01e-02		4.34e-01
6	810565	805634	151179	137792	7.96e-03	4.88e-02	6.01e-03	3.21e-03	3.88e-04	0.00e+00	3.98e-03	5.08e-04	4.46e-04	4.17e-03	2.45e-03	7.79e-02		5.11e-01
7	811399	808919	142488	147419	7.88e-03	4.44e-02	5.21e-03	2.72e-03	4.35e-04	0.00e+00	3.93e-03	6.66e-04	4.72e-04	3.88e-03	2.49e-03	7.21e-02		5.84e-01
8	807879	811164	142460	144940	7.91e-03	4.88e-02	5.25e-03	6.19e-03	3.73e-04	0.00e+00	3.98e-03	6.55e-04	4.43e-04	4.02e-03	2.53e-03	8.02e-02		6.64e-01
9	817368	811641	146786	143501	7.92e-03	4.47e-02	5.31e-03	2.81e-03	1.17e-03	0.00e+00	3.95e-03	4.53e-04	4.38e-04	3.89e-03	2.56e-03	7.32e-02		7.37e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.3733 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.3733 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.373322  0.000000
];

