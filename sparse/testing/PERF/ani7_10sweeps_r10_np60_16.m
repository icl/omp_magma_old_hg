% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_60 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	817985	810006	141586	141586	3.21e-03	2.73e-02	4.73e-03	6.92e-03	5.55e-04	0.00e+00	4.03e-03	2.95e-04	4.74e-04	5.38e-03	2.37e-03	5.53e-02		5.53e-02
1	797671	807427	130233	138212	3.29e-03	3.01e-02	3.54e-03	2.89e-03	5.50e-04	0.00e+00	4.87e-03	2.14e-04	4.88e-04	3.95e-03	3.24e-03	5.31e-02		1.08e-01
2	792164	806902	151353	141597	3.27e-03	5.15e-02	4.56e-03	1.58e-03	4.08e-04	0.00e+00	5.88e-03	3.96e-04	5.78e-04	3.26e-03	3.27e-03	7.47e-02		1.83e-01
3	807198	803188	157666	142928	3.29e-03	6.34e-02	7.33e-03	2.61e-03	6.25e-04	0.00e+00	6.91e-03	1.79e-04	7.86e-04	3.33e-03	3.98e-03	9.25e-02		2.76e-01
4	810393	806082	143438	147448	5.10e-03	6.31e-02	7.56e-03	2.17e-03	8.03e-04	0.00e+00	7.51e-03	3.18e-04	7.40e-04	3.70e-03	4.09e-03	9.51e-02		3.71e-01
5	815781	806158	141048	145359	5.11e-03	6.16e-02	7.79e-03	2.20e-03	1.14e-03	0.00e+00	7.64e-03	2.63e-04	7.65e-04	3.98e-03	4.08e-03	9.45e-02		4.65e-01
6	807426	814042	136466	146089	5.14e-03	6.72e-02	5.44e-03	1.66e-03	7.75e-04	0.00e+00	7.64e-03	2.75e-04	7.14e-04	3.42e-03	3.12e-03	9.54e-02		5.60e-01
7	813763	812915	145627	139011	3.29e-03	6.26e-02	5.44e-03	1.62e-03	8.17e-04	0.00e+00	7.71e-03	4.40e-04	7.20e-04	3.30e-03	3.17e-03	8.91e-02		6.50e-01
8	813954	812312	140096	140944	3.32e-03	6.48e-02	5.49e-03	1.83e-03	1.10e-03	0.00e+00	7.53e-03	3.05e-04	6.98e-04	3.35e-03	3.21e-03	9.17e-02		7.41e-01
9	814574	812811	140711	142353	3.37e-03	6.40e-02	5.51e-03	1.88e-03	1.17e-03	0.00e+00	7.78e-03	3.10e-04	6.92e-04	3.27e-03	3.19e-03	9.12e-02		8.32e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.2100 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.2100 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.210010  0.000000
];

