% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_272 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	815418	805353	141586	141586	1.16e-02	4.66e-02	9.80e-03	2.22e-02	2.72e-03	0.00e+00	2.58e-03	6.31e-04	3.75e-04	1.50e-02	1.52e-03	1.13e-01		1.13e-01
1	812746	806693	132800	142865	1.17e-02	3.92e-02	2.98e-03	3.04e-03	2.57e-03	0.00e+00	3.14e-03	4.96e-04	3.96e-04	7.11e-03	2.02e-03	7.26e-02		1.86e-01
2	801295	802241	136278	142331	1.16e-02	5.61e-02	3.68e-03	2.69e-03	1.53e-03	0.00e+00	3.47e-03	5.98e-04	4.33e-04	4.36e-03	2.10e-03	8.66e-02		2.72e-01
3	806279	808222	148535	147589	1.16e-02	5.41e-02	3.98e-03	5.08e-03	4.13e-04	0.00e+00	3.69e-03	6.16e-04	4.85e-04	4.19e-03	2.06e-03	8.62e-02		3.58e-01
4	797966	802775	144357	142414	1.17e-02	5.90e-02	4.16e-03	2.94e-03	3.97e-04	0.00e+00	3.85e-03	7.78e-04	4.51e-04	4.39e-03	1.96e-03	8.97e-02		4.48e-01
5	807796	804336	153475	148666	1.15e-02	5.12e-02	4.04e-03	3.07e-03	4.56e-04	0.00e+00	4.04e-03	5.67e-04	4.48e-04	5.16e-03	2.01e-03	8.26e-02		5.31e-01
6	812270	810770	144451	147911	1.16e-02	5.74e-02	4.12e-03	1.78e-03	4.89e-04	0.00e+00	3.96e-03	9.92e-04	4.34e-04	4.64e-03	2.04e-03	8.75e-02		6.18e-01
7	812446	813384	140783	142283	1.17e-02	5.35e-02	4.16e-03	2.67e-03	4.40e-04	0.00e+00	3.87e-03	5.62e-04	4.36e-04	4.23e-03	2.06e-03	8.37e-02		7.02e-01
8	813195	812265	141413	140475	1.17e-02	5.82e-02	4.22e-03	1.47e-03	4.71e-04	0.00e+00	4.01e-03	7.41e-04	4.35e-04	4.32e-03	2.04e-03	8.76e-02		7.89e-01
9	812892	815821	141470	142400	1.17e-02	5.44e-02	4.24e-03	2.44e-03	1.93e-03	0.00e+00	4.01e-03	8.57e-04	4.86e-04	4.25e-03	2.07e-03	8.63e-02		8.76e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.9937 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.9937 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.993732  0.000000
];

