% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_200 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	816731	817063	141586	141586	8.84e-03	3.66e-02	7.73e-03	1.68e-02	6.38e-04	0.00e+00	2.68e-03	3.81e-04	3.24e-04	9.97e-03	1.70e-03	8.56e-02		8.56e-02
1	812185	812157	131487	131155	8.95e-03	3.29e-02	3.20e-03	1.95e-03	4.30e-04	0.00e+00	3.43e-03	3.71e-04	3.77e-04	7.06e-03	1.97e-03	6.06e-02		1.46e-01
2	817402	798060	136839	136867	8.89e-03	4.50e-02	3.80e-03	2.36e-03	2.75e-04	0.00e+00	3.81e-03	5.39e-04	3.95e-04	3.98e-03	2.17e-03	7.12e-02		2.17e-01
3	810294	795345	132428	151770	8.75e-03	5.11e-02	4.26e-03	2.54e-03	4.92e-04	0.00e+00	4.17e-03	3.79e-04	4.39e-04	3.95e-03	2.16e-03	7.83e-02		2.96e-01
4	814333	811795	140342	155291	8.71e-03	5.48e-02	4.31e-03	1.89e-03	4.84e-04	0.00e+00	4.27e-03	5.34e-04	4.31e-04	4.75e-03	2.16e-03	8.24e-02		3.78e-01
5	811119	807449	137108	139646	8.88e-03	5.06e-02	4.40e-03	2.29e-03	1.07e-03	0.00e+00	4.41e-03	7.78e-04	4.36e-04	4.33e-03	2.13e-03	7.93e-02		4.57e-01
6	814832	812465	141128	144798	8.85e-03	5.36e-02	4.36e-03	3.76e-03	4.85e-04	0.00e+00	4.39e-03	5.84e-04	4.33e-04	4.30e-03	2.18e-03	8.30e-02		5.40e-01
7	813181	811421	138221	140588	8.90e-03	5.23e-02	4.44e-03	2.50e-03	4.00e-04	0.00e+00	4.33e-03	5.31e-04	4.69e-04	3.98e-03	2.19e-03	8.00e-02		6.20e-01
8	813232	811690	140678	142438	8.89e-03	5.50e-02	4.40e-03	1.97e-03	9.46e-04	0.00e+00	4.31e-03	8.47e-04	4.58e-04	4.02e-03	2.21e-03	8.30e-02		7.03e-01
9	814727	811449	141433	142975	8.93e-03	5.19e-02	4.43e-03	2.91e-03	1.24e-03	0.00e+00	4.34e-03	4.48e-04	4.69e-04	3.94e-03	2.16e-03	8.08e-02		7.84e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.4085 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.385360e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.4085 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.385360e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.408456  0.000000
];

