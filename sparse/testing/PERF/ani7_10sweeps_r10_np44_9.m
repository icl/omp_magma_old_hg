% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_44 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	811859	800261	141586	141586	3.26e-03	3.15e-02	5.67e-03	6.58e-03	9.94e-04	0.00e+00	5.45e-03	2.48e-04	6.07e-04	5.91e-03	3.18e-03	6.34e-02		6.34e-02
1	819743	806224	136359	147957	3.27e-03	3.70e-02	4.74e-03	2.54e-03	1.12e-03	0.00e+00	6.53e-03	2.76e-04	6.44e-04	4.33e-03	4.22e-03	6.47e-02		1.28e-01
2	804318	814950	129281	142800	3.28e-03	5.84e-02	6.26e-03	2.02e-03	8.03e-04	0.00e+00	8.21e-03	1.98e-04	7.15e-04	3.70e-03	4.53e-03	8.81e-02		2.16e-01
3	807095	799628	145512	134880	3.32e-03	8.37e-02	7.27e-03	2.96e-03	6.95e-04	0.00e+00	9.33e-03	3.25e-04	8.20e-04	3.67e-03	4.17e-03	1.16e-01		3.32e-01
4	805070	810691	143541	151008	3.27e-03	7.97e-02	7.16e-03	1.85e-03	1.02e-03	0.00e+00	1.01e-02	2.71e-04	8.62e-04	3.68e-03	4.12e-03	1.12e-01		4.44e-01
5	813233	808608	146371	140750	3.30e-03	7.80e-02	7.23e-03	2.04e-03	1.11e-03	0.00e+00	1.05e-02	3.37e-04	8.97e-04	4.07e-03	4.19e-03	1.12e-01		5.56e-01
6	810417	807890	139014	143639	3.30e-03	8.12e-02	7.35e-03	1.94e-03	9.42e-04	0.00e+00	1.03e-02	3.77e-04	8.49e-04	3.78e-03	4.15e-03	1.14e-01		6.70e-01
7	811142	810763	142636	145163	3.31e-03	8.04e-02	7.33e-03	1.74e-03	9.61e-04	0.00e+00	1.02e-02	3.47e-04	8.52e-04	4.04e-03	4.21e-03	1.13e-01		7.84e-01
8	810399	812592	142717	143096	3.34e-03	8.33e-02	7.39e-03	1.82e-03	1.59e-03	0.00e+00	1.03e-02	3.64e-04	8.48e-04	3.81e-03	4.21e-03	1.17e-01		9.01e-01
9	810534	811969	144266	142073	3.33e-03	8.24e-02	7.43e-03	2.13e-03	1.31e-03	0.00e+00	1.03e-02	3.15e-04	8.57e-04	3.65e-03	4.24e-03	1.16e-01		1.02e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.3887 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.3887 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.388701  0.000000
];

