% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_176 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	812505	798107	141586	141586	8.33e-03	3.67e-02	7.48e-03	1.51e-02	8.11e-04	0.00e+00	2.20e-03	5.72e-04	3.57e-04	1.05e-02	1.84e-03	8.39e-02		8.39e-02
1	821116	809486	135713	150111	8.22e-03	3.07e-02	3.52e-03	2.90e-03	1.45e-03	0.00e+00	2.61e-03	3.76e-04	4.34e-04	6.34e-03	2.11e-03	5.87e-02		1.43e-01
2	830519	810909	127908	139538	8.30e-03	4.61e-02	4.07e-03	3.56e-03	3.16e-04	0.00e+00	3.15e-03	7.13e-04	3.90e-04	3.95e-03	2.22e-03	7.28e-02		2.15e-01
3	813809	807815	119311	138921	8.33e-03	4.99e-02	4.39e-03	2.31e-03	3.55e-04	0.00e+00	3.48e-03	7.03e-04	4.05e-04	3.98e-03	2.21e-03	7.61e-02		2.91e-01
4	813616	804945	136827	142821	8.26e-03	4.96e-02	4.63e-03	3.12e-03	4.45e-04	0.00e+00	4.82e-03	5.95e-04	4.37e-04	4.41e-03	2.32e-03	7.86e-02		3.70e-01
5	810299	810698	137825	146496	8.98e-03	4.59e-02	4.80e-03	2.64e-03	6.75e-04	0.00e+00	4.78e-03	5.31e-04	4.50e-04	4.33e-03	2.36e-03	7.55e-02		4.46e-01
6	809679	811516	141948	141549	8.82e-03	5.04e-02	4.79e-03	1.90e-03	3.92e-04	0.00e+00	3.66e-03	6.63e-04	4.63e-04	4.23e-03	2.37e-03	7.76e-02		5.23e-01
7	811063	811417	143374	141537	8.33e-03	4.70e-02	4.81e-03	1.81e-03	3.60e-04	0.00e+00	3.42e-03	6.51e-04	4.61e-04	3.84e-03	2.32e-03	7.30e-02		5.96e-01
8	814141	812890	142796	142442	8.28e-03	5.09e-02	4.86e-03	1.85e-03	4.14e-04	0.00e+00	3.55e-03	7.14e-04	4.45e-04	4.15e-03	2.32e-03	7.74e-02		6.74e-01
9	811966	814450	140524	141775	8.37e-03	4.74e-02	5.55e-03	2.03e-03	1.27e-03	0.00e+00	3.65e-03	7.20e-04	4.55e-04	3.97e-03	2.43e-03	7.59e-02		7.50e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.3936 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.3936 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.393590  0.000000
];

