% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_160 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	822627	813663	141586	141586	7.95e-03	3.61e-02	7.60e-03	1.49e-02	7.28e-04	0.00e+00	2.40e-03	4.83e-04	4.17e-04	1.06e-02	2.07e-03	8.32e-02		8.32e-02
1	802649	787203	125591	134555	7.98e-03	2.99e-02	3.97e-03	3.78e-03	1.29e-03	0.00e+00	3.10e-03	4.57e-04	4.09e-04	6.29e-03	2.13e-03	5.93e-02		1.43e-01
2	807815	808423	146375	161821	7.67e-03	4.59e-02	4.12e-03	3.08e-03	2.79e-04	0.00e+00	3.22e-03	5.72e-04	4.09e-04	4.02e-03	2.38e-03	7.16e-02		2.14e-01
3	805484	817213	142015	141407	7.88e-03	4.70e-02	4.60e-03	2.36e-03	2.95e-04	0.00e+00	3.78e-03	5.10e-04	4.59e-04	4.04e-03	2.52e-03	7.34e-02		2.88e-01
4	809152	805599	145152	133423	7.99e-03	5.00e-02	4.97e-03	2.10e-03	4.22e-04	0.00e+00	3.98e-03	5.53e-04	4.84e-04	4.45e-03	2.50e-03	7.75e-02		3.65e-01
5	804818	808541	142289	145842	7.86e-03	4.30e-02	5.12e-03	2.30e-03	8.28e-04	0.00e+00	4.03e-03	5.54e-04	4.54e-04	4.32e-03	2.48e-03	7.09e-02		4.36e-01
6	807027	814789	147429	143706	7.91e-03	4.77e-02	5.18e-03	2.21e-03	5.70e-04	0.00e+00	4.01e-03	4.69e-04	4.77e-04	4.27e-03	2.49e-03	7.52e-02		5.11e-01
7	811006	810283	146026	138264	7.96e-03	4.47e-02	5.26e-03	2.19e-03	4.52e-04	0.00e+00	3.93e-03	5.91e-04	4.41e-04	3.94e-03	2.51e-03	7.20e-02		5.83e-01
8	811457	812047	142853	143576	7.87e-03	4.87e-02	5.31e-03	2.21e-03	5.85e-04	0.00e+00	4.01e-03	4.57e-04	4.36e-04	4.19e-03	2.53e-03	7.63e-02		6.60e-01
9	814533	811465	143208	142618	7.96e-03	4.52e-02	5.41e-03	1.57e-03	1.14e-03	0.00e+00	3.97e-03	5.99e-04	4.36e-04	3.93e-03	2.55e-03	7.27e-02		7.32e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.3756 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.3756 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.375625  0.000000
];

