% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_242 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	821243	821443	141586	141586	1.08e-02	4.39e-02	9.09e-03	1.88e-02	3.02e-03	0.00e+00	2.28e-03	4.81e-04	3.34e-04	1.43e-02	1.73e-03	1.05e-01		1.05e-01
1	827879	816994	126975	126775	1.10e-02	3.64e-02	3.37e-03	3.82e-03	2.84e-03	0.00e+00	3.04e-03	5.30e-04	3.71e-04	7.36e-03	1.91e-03	7.06e-02		1.75e-01
2	825847	803445	121145	132030	1.08e-02	5.14e-02	4.16e-03	2.95e-03	2.57e-04	0.00e+00	3.29e-03	5.44e-04	4.22e-04	4.33e-03	1.98e-03	8.01e-02		2.55e-01
3	801624	814818	123983	146385	1.07e-02	5.25e-02	3.95e-03	4.49e-03	3.14e-04	0.00e+00	3.47e-03	7.26e-04	4.53e-04	4.40e-03	2.04e-03	8.30e-02		3.38e-01
4	809835	814560	149012	135818	1.08e-02	5.46e-02	4.16e-03	2.13e-03	4.15e-04	0.00e+00	3.68e-03	6.38e-04	4.08e-04	4.44e-03	2.16e-03	8.35e-02		4.22e-01
5	811058	815993	141606	136881	1.08e-02	4.14e-01	4.36e-03	2.62e-03	4.06e-04	0.00e+00	3.84e-03	5.75e-04	4.30e-04	5.09e-03	2.15e-03	4.44e-01		8.66e-01
6	811746	811108	141189	136254	1.08e-02	5.74e-02	4.44e-03	2.59e-03	4.56e-04	0.00e+00	3.66e-03	9.73e-04	4.17e-04	4.64e-03	2.15e-03	8.75e-02		9.54e-01
7	810371	810695	141307	141945	1.08e-02	5.15e-02	4.40e-03	1.59e-03	6.52e-04	0.00e+00	3.70e-03	6.39e-04	3.99e-04	4.34e-03	2.15e-03	8.01e-02		1.03e+00
8	814551	813102	143488	143164	1.07e-02	5.61e-02	4.41e-03	2.07e-03	4.03e-04	0.00e+00	3.68e-03	5.30e-04	3.89e-04	4.56e-03	2.16e-03	8.50e-02		1.12e+00
9	812953	818190	140114	141563	1.08e-02	5.16e-02	4.50e-03	1.67e-03	1.67e-03	0.00e+00	3.75e-03	8.36e-04	3.93e-04	4.40e-03	3.41e-03	8.30e-02		1.20e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.8326 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.8326 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.832648  0.000000
];

