% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_193 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	806327	804722	141586	141586	8.75e-03	3.69e-02	7.53e-03	1.59e-02	9.02e-04	0.00e+00	2.72e-03	3.95e-04	3.71e-04	1.11e-02	1.70e-03	8.63e-02		8.63e-02
1	803832	828155	141891	143496	8.71e-03	3.23e-02	3.22e-03	3.59e-03	1.11e-03	0.00e+00	3.20e-03	4.15e-04	3.95e-04	6.88e-03	2.01e-03	6.19e-02		1.48e-01
2	799977	797792	145192	120869	8.95e-03	4.53e-02	3.84e-03	2.69e-03	3.15e-04	0.00e+00	3.89e-03	3.85e-04	3.99e-04	3.98e-03	2.16e-03	7.19e-02		2.20e-01
3	809889	816882	149853	152038	8.61e-03	4.67e-02	4.09e-03	2.89e-03	3.99e-04	0.00e+00	4.30e-03	5.20e-04	4.60e-04	4.02e-03	2.23e-03	7.43e-02		2.94e-01
4	810493	804583	140747	133754	8.82e-03	5.39e-02	4.41e-03	2.13e-03	4.17e-04	0.00e+00	4.49e-03	7.70e-04	4.71e-04	4.53e-03	2.16e-03	8.21e-02		3.77e-01
5	815284	805227	140948	146858	8.72e-03	4.88e-02	4.35e-03	2.45e-03	6.47e-04	0.00e+00	4.53e-03	5.54e-04	4.51e-04	4.33e-03	2.20e-03	7.71e-02		4.54e-01
6	809185	812113	136963	147020	9.08e-03	5.42e-02	4.49e-03	1.66e-03	3.93e-04	0.00e+00	4.33e-03	8.11e-04	4.28e-04	4.33e-03	2.26e-03	8.20e-02		5.36e-01
7	811815	805499	143868	140940	8.77e-03	5.16e-02	4.53e-03	3.27e-03	4.11e-04	0.00e+00	4.23e-03	7.42e-04	4.60e-04	3.93e-03	2.21e-03	8.01e-02		6.16e-01
8	814111	811299	142044	148360	8.70e-03	5.49e-02	4.49e-03	2.17e-03	8.71e-04	0.00e+00	4.32e-03	5.55e-04	4.26e-04	4.13e-03	2.28e-03	8.29e-02		6.99e-01
9	813010	814244	140554	143366	8.80e-03	5.18e-02	4.59e-03	2.21e-03	1.13e-03	0.00e+00	4.39e-03	6.10e-04	4.54e-04	3.89e-03	2.24e-03	8.01e-02		7.79e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.4038 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.4038 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.403756  0.000000
];

