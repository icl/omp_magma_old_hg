% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_116 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	813897	812476	141586	141586	5.78e-03	2.82e-02	5.79e-03	1.04e-02	1.41e-03	0.00e+00	2.34e-03	3.44e-04	3.76e-04	6.50e-03	1.94e-03	6.31e-02		6.31e-02
1	822517	814983	134321	135742	5.85e-03	2.84e-02	3.37e-03	2.18e-03	1.32e-03	0.00e+00	2.72e-03	3.55e-04	4.02e-04	4.35e-03	2.30e-03	5.12e-02		1.14e-01
2	804376	802560	126507	134041	5.84e-03	3.99e-02	4.11e-03	1.67e-03	7.09e-04	0.00e+00	3.36e-03	3.25e-04	4.81e-04	3.03e-03	2.48e-03	6.19e-02		1.76e-01
3	805024	806553	145454	147270	5.76e-03	4.55e-02	4.47e-03	1.94e-03	3.51e-04	0.00e+00	3.78e-03	3.86e-04	4.99e-04	3.06e-03	2.40e-03	6.81e-02		2.44e-01
4	804844	808808	145612	144083	5.81e-03	4.46e-02	4.59e-03	2.43e-03	5.94e-04	0.00e+00	4.12e-03	3.51e-04	4.80e-04	3.02e-03	2.43e-03	6.84e-02		3.13e-01
5	808275	810120	146597	142633	5.81e-03	4.26e-02	4.68e-03	2.53e-03	4.47e-04	0.00e+00	4.06e-03	4.68e-04	4.84e-04	3.38e-03	2.47e-03	6.69e-02		3.80e-01
6	812170	809390	143972	142127	5.79e-03	4.56e-02	4.76e-03	1.71e-03	4.28e-04	0.00e+00	4.11e-03	3.32e-04	4.76e-04	3.29e-03	2.48e-03	6.90e-02		4.49e-01
7	810565	811900	140883	143663	5.79e-03	4.41e-02	4.81e-03	1.94e-03	4.04e-04	0.00e+00	3.85e-03	4.86e-04	5.06e-04	3.55e-03	2.50e-03	6.79e-02		5.17e-01
8	810579	812526	143294	141959	5.83e-03	4.65e-02	6.63e-03	1.37e-03	1.12e-03	0.00e+00	4.20e-03	3.34e-04	5.26e-04	3.23e-03	3.27e-03	7.30e-02		5.90e-01
9	815292	811237	144086	142139	8.60e-03	4.64e-02	6.72e-03	1.72e-03	1.16e-03	0.00e+00	4.13e-03	3.23e-04	5.36e-04	3.12e-03	3.23e-03	7.59e-02		6.66e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.1498 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.1498 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.149799  0.000000
];

