% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_104 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	809705	805704	141586	141586	5.64e-03	2.90e-02	5.90e-03	9.74e-03	5.94e-04	0.00e+00	2.58e-03	3.45e-04	4.54e-04	6.59e-03	2.14e-03	6.30e-02		6.30e-02
1	791590	799713	138513	142514	5.63e-03	2.73e-02	3.73e-03	2.47e-03	7.50e-04	0.00e+00	2.96e-03	2.73e-04	4.51e-04	3.99e-03	2.38e-03	5.00e-02		1.13e-01
2	813299	792881	157434	149311	5.54e-03	4.42e-02	4.26e-03	1.38e-03	3.59e-04	0.00e+00	3.52e-03	4.77e-04	4.42e-04	3.16e-03	2.42e-03	6.58e-02		1.79e-01
3	804223	813086	136531	156949	5.49e-03	4.65e-02	4.48e-03	1.78e-03	3.81e-04	0.00e+00	4.22e-03	3.90e-04	4.65e-04	3.18e-03	2.55e-03	6.94e-02		2.48e-01
4	805856	815738	146413	137550	5.65e-03	4.59e-02	4.98e-03	1.80e-03	4.85e-04	0.00e+00	4.52e-03	4.57e-04	5.14e-04	3.77e-03	2.59e-03	7.07e-02		3.19e-01
5	813599	807318	145585	135703	5.69e-03	4.31e-02	5.11e-03	2.02e-03	8.84e-04	0.00e+00	4.26e-03	5.43e-04	4.71e-04	3.45e-03	2.56e-03	6.81e-02		3.87e-01
6	805828	813511	138648	144929	5.63e-03	4.68e-02	5.16e-03	1.96e-03	4.57e-04	0.00e+00	4.46e-03	4.65e-04	5.14e-04	3.34e-03	2.60e-03	7.14e-02		4.58e-01
7	809182	811366	147225	139542	5.64e-03	4.46e-02	5.17e-03	1.97e-03	4.93e-04	0.00e+00	4.43e-03	6.30e-04	4.80e-04	3.28e-03	2.60e-03	6.93e-02		5.28e-01
8	813842	812588	144677	142493	5.64e-03	4.74e-02	5.28e-03	1.27e-03	1.00e-03	0.00e+00	4.47e-03	5.28e-04	4.84e-04	3.26e-03	2.57e-03	7.20e-02		6.00e-01
9	810986	810568	140823	142077	5.68e-03	4.61e-02	5.28e-03	1.93e-03	8.76e-04	0.00e+00	4.49e-03	4.79e-04	5.09e-04	3.09e-03	2.54e-03	7.09e-02		6.71e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.1525 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.1525 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.152470  0.000000
];

