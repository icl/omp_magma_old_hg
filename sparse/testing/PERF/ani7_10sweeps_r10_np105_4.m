% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_105 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	813355	806092	141586	141586	5.69e-03	2.92e-02	5.94e-03	9.99e-03	5.97e-04	0.00e+00	2.58e-03	3.53e-04	4.45e-04	7.22e-03	2.10e-03	6.41e-02		6.41e-02
1	807277	806870	134863	142126	5.67e-03	2.78e-02	3.71e-03	3.01e-03	9.03e-04	0.00e+00	3.04e-03	3.48e-04	4.58e-04	4.60e-03	2.39e-03	5.20e-02		1.16e-01
2	808093	808615	141747	142154	5.63e-03	4.43e-02	4.35e-03	1.49e-03	3.10e-04	0.00e+00	3.59e-03	5.17e-04	4.44e-04	3.27e-03	2.51e-03	6.64e-02		1.83e-01
3	806844	806147	141737	141215	5.64e-03	4.65e-02	4.70e-03	1.91e-03	4.17e-04	0.00e+00	4.11e-03	3.56e-04	4.84e-04	3.10e-03	2.51e-03	6.97e-02		2.52e-01
4	809795	801122	143792	144489	5.63e-03	4.57e-02	4.79e-03	1.84e-03	5.07e-04	0.00e+00	4.36e-03	3.51e-04	4.69e-04	3.48e-03	2.51e-03	6.96e-02		3.22e-01
5	809587	804775	141646	150319	5.60e-03	4.27e-02	5.01e-03	2.19e-03	6.87e-04	0.00e+00	4.53e-03	4.63e-04	4.86e-04	3.48e-03	2.50e-03	6.76e-02		3.89e-01
6	809799	808835	142660	147472	5.66e-03	4.61e-02	5.11e-03	2.60e-03	5.32e-04	0.00e+00	4.48e-03	3.30e-04	4.81e-04	3.40e-03	2.54e-03	7.12e-02		4.61e-01
7	811891	812194	143254	144218	5.66e-03	4.40e-02	5.16e-03	1.98e-03	6.30e-04	0.00e+00	4.54e-03	5.66e-04	4.68e-04	3.16e-03	2.61e-03	6.87e-02		5.29e-01
8	809327	813818	141968	141665	5.66e-03	4.78e-02	5.27e-03	2.02e-03	6.25e-04	0.00e+00	4.33e-03	7.69e-04	4.52e-04	3.26e-03	2.62e-03	7.28e-02		6.02e-01
9	812523	814714	145338	140847	5.72e-03	4.55e-02	5.28e-03	2.38e-03	9.96e-04	0.00e+00	4.28e-03	4.95e-04	4.66e-04	3.11e-03	2.62e-03	7.08e-02		6.73e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.1512 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.1512 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.151160  0.000000
];

