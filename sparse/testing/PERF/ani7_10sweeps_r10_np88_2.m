% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_88 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	811799	813290	141586	141586	5.41e-03	2.94e-02	6.37e-03	9.67e-03	7.16e-04	0.00e+00	3.04e-03	4.26e-04	5.07e-04	7.06e-03	2.52e-03	6.51e-02		6.51e-02
1	801002	807339	136419	134928	5.42e-03	2.77e-02	4.42e-03	4.68e-03	5.66e-04	0.00e+00	3.63e-03	3.08e-04	5.00e-04	3.92e-03	2.65e-03	5.38e-02		1.19e-01
2	805419	813745	148022	141685	5.36e-03	4.71e-02	5.04e-03	2.15e-03	8.58e-04	0.00e+00	5.27e-03	4.05e-04	4.73e-04	3.69e-03	3.36e-03	7.37e-02		1.93e-01
3	810176	797147	144411	136085	5.70e-03	5.57e-02	5.31e-03	1.65e-03	3.90e-04	0.00e+00	4.92e-03	4.79e-04	5.26e-04	3.24e-03	2.95e-03	8.08e-02		2.73e-01
4	801853	808321	140460	153489	5.31e-03	5.21e-02	5.65e-03	1.38e-03	7.28e-04	0.00e+00	5.18e-03	4.86e-04	5.65e-04	3.31e-03	3.00e-03	7.77e-02		3.51e-01
5	811462	812960	149588	143120	5.38e-03	4.85e-02	5.93e-03	1.38e-03	5.76e-04	0.00e+00	5.13e-03	3.99e-04	5.30e-04	3.53e-03	3.05e-03	7.44e-02		4.26e-01
6	803383	810405	140785	139287	5.40e-03	5.23e-02	6.03e-03	1.78e-03	5.50e-04	0.00e+00	5.22e-03	4.62e-04	5.51e-04	3.24e-03	3.00e-03	7.85e-02		5.04e-01
7	813053	812559	149670	142648	5.39e-03	4.79e-02	5.98e-03	2.72e-03	6.07e-04	0.00e+00	5.05e-03	5.38e-04	5.30e-04	3.40e-03	3.03e-03	7.51e-02		5.79e-01
8	808547	812927	140806	141300	5.39e-03	5.24e-02	6.13e-03	2.81e-03	8.49e-04	0.00e+00	5.19e-03	4.84e-04	5.26e-04	3.30e-03	3.04e-03	8.01e-02		6.59e-01
9	816217	810641	146118	141738	5.39e-03	5.00e-02	6.07e-03	1.65e-03	9.21e-04	0.00e+00	5.62e-03	4.11e-04	5.82e-04	3.24e-03	3.01e-03	7.69e-02		7.36e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.2100 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.2100 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.210006  0.000000
];

