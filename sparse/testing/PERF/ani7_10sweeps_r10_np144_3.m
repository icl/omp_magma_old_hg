% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_144 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	812728	804956	141586	141586	7.50e-03	3.42e-02	6.70e-03	1.41e-02	6.98e-04	0.00e+00	2.70e-03	4.36e-04	4.14e-04	9.61e-03	2.17e-03	7.86e-02		7.86e-02
1	812568	797841	135490	143262	7.44e-03	2.94e-02	4.14e-03	2.22e-03	1.22e-03	0.00e+00	3.13e-03	4.50e-04	3.85e-04	6.16e-03	2.27e-03	5.68e-02		1.35e-01
2	801892	808982	136456	151183	7.32e-03	4.62e-02	4.40e-03	3.09e-03	2.82e-04	0.00e+00	3.59e-03	4.60e-04	4.58e-04	4.04e-03	2.24e-03	7.21e-02		2.07e-01
3	812180	798444	147938	140848	7.44e-03	4.90e-02	4.76e-03	2.45e-03	3.33e-04	0.00e+00	4.08e-03	5.17e-04	4.62e-04	3.99e-03	2.33e-03	7.53e-02		2.83e-01
4	805752	809474	138456	152192	7.39e-03	5.11e-02	5.14e-03	1.95e-03	4.92e-04	0.00e+00	4.32e-03	6.33e-04	4.88e-04	4.42e-03	2.42e-03	7.84e-02		3.61e-01
5	808085	806368	145689	141967	7.43e-03	4.74e-02	5.31e-03	2.39e-03	7.76e-04	0.00e+00	4.51e-03	4.43e-04	5.11e-04	4.52e-03	2.41e-03	7.57e-02		4.37e-01
6	810474	818833	144162	145879	7.43e-03	5.00e-02	5.30e-03	1.67e-03	4.52e-04	0.00e+00	4.34e-03	4.93e-04	4.91e-04	4.38e-03	2.45e-03	7.70e-02		5.14e-01
7	807098	812599	142579	134220	7.54e-03	4.76e-02	5.46e-03	1.96e-03	5.37e-04	0.00e+00	4.44e-03	7.33e-04	4.98e-04	3.93e-03	2.40e-03	7.51e-02		5.89e-01
8	815083	812138	146761	141260	7.48e-03	5.10e-02	5.31e-03	2.78e-03	4.73e-04	0.00e+00	4.26e-03	5.49e-04	4.62e-04	4.25e-03	2.46e-03	7.90e-02		6.68e-01
9	814238	812265	139582	142527	7.51e-03	4.83e-02	5.49e-03	2.56e-03	1.30e-03	0.00e+00	5.83e-03	7.89e-04	4.65e-04	4.03e-03	2.58e-03	7.88e-02		7.47e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.3862 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 1.3862 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.386185  0.000000
];

