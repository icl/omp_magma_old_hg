% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_38 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	809586	809034	141586	141586	3.29e-03	3.49e-02	6.38e-03	6.71e-03	1.18e-03	0.00e+00	6.33e-03	2.63e-04	6.96e-04	6.19e-03	3.64e-03	6.96e-02		6.96e-02
1	808252	800934	138632	139184	3.31e-03	4.11e-02	5.51e-03	3.05e-03	1.26e-03	0.00e+00	7.56e-03	2.28e-04	7.43e-04	4.85e-03	4.71e-03	7.23e-02		1.42e-01
2	815256	798293	140772	148090	3.27e-03	6.49e-02	7.12e-03	2.55e-03	6.34e-04	0.00e+00	9.30e-03	2.25e-04	8.33e-04	4.01e-03	5.18e-03	9.81e-02		2.40e-01
3	812647	812016	134574	151537	3.31e-03	9.46e-02	8.11e-03	2.47e-03	7.71e-04	0.00e+00	1.06e-02	2.64e-04	9.10e-04	3.94e-03	5.03e-03	1.30e-01		3.70e-01
4	809138	807774	137989	138620	3.38e-03	9.06e-02	8.40e-03	2.03e-03	1.04e-03	0.00e+00	1.16e-02	3.21e-04	9.61e-04	4.06e-03	4.77e-03	1.27e-01		4.97e-01
5	810517	809258	142303	143667	3.32e-03	8.60e-02	8.30e-03	2.32e-03	1.07e-03	0.00e+00	1.18e-02	3.11e-04	9.61e-04	4.36e-03	4.82e-03	1.23e-01		6.21e-01
6	807153	809775	141730	142989	3.33e-03	8.91e-02	8.45e-03	2.89e-03	1.00e-03	0.00e+00	1.18e-02	3.19e-04	9.48e-04	4.02e-03	4.82e-03	1.27e-01		7.47e-01
7	811017	810745	145900	143278	3.34e-03	8.85e-02	8.45e-03	1.92e-03	1.06e-03	0.00e+00	1.17e-02	3.77e-04	9.65e-04	3.92e-03	4.86e-03	1.25e-01		8.72e-01
8	814183	814271	142842	143114	3.32e-03	9.20e-02	8.52e-03	2.48e-03	1.01e-03	0.00e+00	1.18e-02	3.19e-04	9.10e-04	4.11e-03	4.87e-03	1.29e-01		1.00e+00
9	814658	813803	140482	140394	3.36e-03	9.28e-02	8.62e-03	2.07e-03	1.59e-03	0.00e+00	1.19e-02	2.91e-04	9.65e-04	3.91e-03	4.92e-03	1.30e-01		1.13e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.5000 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.5000 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.499952  0.000000
];

