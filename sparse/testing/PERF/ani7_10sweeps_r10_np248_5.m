% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_248 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	809083	798186	141586	141586	1.10e-02	4.32e-02	9.22e-03	1.90e-02	1.40e-03	0.00e+00	2.23e-03	5.33e-04	3.86e-04	1.43e-02	1.66e-03	1.03e-01		1.03e-01
1	812633	812994	139135	150032	1.08e-02	3.48e-02	3.16e-03	3.95e-03	5.21e-04	0.00e+00	2.75e-03	4.86e-04	3.86e-04	6.80e-03	1.87e-03	6.56e-02		1.68e-01
2	809334	803468	136391	136030	1.10e-02	5.11e-02	3.66e-03	4.93e-03	5.17e-04	0.00e+00	3.07e-03	5.80e-04	3.73e-04	4.69e-03	1.84e-03	8.18e-02		2.50e-01
3	808806	811940	140496	146362	1.08e-02	5.02e-02	4.12e-03	3.60e-03	3.60e-04	0.00e+00	3.37e-03	7.33e-04	4.04e-04	4.15e-03	1.96e-03	7.97e-02		3.30e-01
4	807264	801393	141830	138696	1.09e-02	5.50e-02	4.26e-03	1.57e-03	1.11e-03	0.00e+00	3.63e-03	7.26e-04	4.19e-04	4.16e-03	2.16e-03	8.40e-02		4.14e-01
5	812073	806306	144177	150048	1.08e-02	4.72e-02	4.12e-03	2.19e-03	3.88e-04	0.00e+00	3.62e-03	4.13e-04	4.44e-04	4.44e-03	2.06e-03	7.57e-02		4.90e-01
6	813663	816029	140174	145941	1.09e-02	5.36e-02	4.24e-03	2.95e-03	3.52e-04	0.00e+00	3.60e-03	5.60e-04	4.00e-04	4.11e-03	2.09e-03	8.28e-02		5.72e-01
7	813021	808183	139390	137024	1.10e-02	5.03e-02	4.39e-03	3.14e-03	4.28e-04	0.00e+00	4.15e-03	6.65e-04	4.32e-04	4.38e-03	2.10e-03	8.10e-02		6.53e-01
8	812824	812357	140838	145676	1.10e-02	5.55e-02	4.27e-03	2.13e-03	1.43e-03	0.00e+00	3.62e-03	8.44e-04	4.37e-04	4.22e-03	2.08e-03	8.56e-02		7.39e-01
9	813855	807393	141841	142308	1.10e-02	4.98e-02	4.31e-03	2.49e-03	1.06e-03	0.00e+00	3.55e-03	8.11e-04	4.51e-04	4.28e-03	2.07e-03	7.98e-02		8.19e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.5790 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.5790 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.579012  0.000000
];

