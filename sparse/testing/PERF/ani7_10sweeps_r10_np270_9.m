% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_270 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	820391	812106	141586	141586	1.16e-02	4.49e-02	9.60e-03	2.20e-02	1.02e-03	0.00e+00	2.52e-03	5.82e-04	3.29e-04	1.36e-02	1.59e-03	1.08e-01		1.08e-01
1	822966	802868	127827	136112	1.16e-02	3.66e-02	3.02e-03	6.82e-03	1.15e-03	0.00e+00	3.20e-03	4.49e-04	3.50e-04	7.38e-03	1.90e-03	7.25e-02		1.80e-01
2	816700	818088	126058	146156	1.16e-02	5.18e-02	3.64e-03	2.12e-03	3.42e-04	0.00e+00	3.48e-03	5.90e-04	3.78e-04	4.36e-03	2.06e-03	8.04e-02		2.61e-01
3	815937	814263	133130	131742	1.18e-02	5.46e-02	4.10e-03	3.57e-03	3.11e-04	0.00e+00	3.84e-03	5.09e-04	3.99e-04	4.22e-03	2.08e-03	8.54e-02		3.46e-01
4	810470	808684	134699	136373	1.18e-02	6.07e-02	4.28e-03	1.52e-03	3.64e-04	0.00e+00	4.02e-03	7.06e-04	4.17e-04	5.51e-03	2.01e-03	9.13e-02		4.37e-01
5	811660	808144	140971	142757	1.16e-02	5.18e-02	4.14e-03	2.42e-03	1.94e-03	0.00e+00	4.10e-03	7.47e-04	4.23e-04	5.06e-03	2.03e-03	8.43e-02		5.22e-01
6	814661	811445	140587	144103	1.17e-02	5.78e-02	4.20e-03	1.97e-03	4.16e-04	0.00e+00	4.07e-03	8.55e-04	4.62e-04	5.03e-03	2.06e-03	8.85e-02		6.10e-01
7	809127	812638	138392	141608	1.16e-02	5.36e-02	4.26e-03	2.15e-03	4.41e-04	0.00e+00	4.02e-03	8.71e-04	4.05e-04	5.07e-03	2.07e-03	8.45e-02		6.95e-01
8	814598	810251	144732	141221	1.17e-02	5.89e-02	4.22e-03	2.33e-03	1.47e-03	0.00e+00	3.98e-03	7.39e-04	4.03e-04	4.41e-03	2.05e-03	9.02e-02		7.85e-01
9	813143	810341	140067	144414	1.17e-02	5.48e-02	4.31e-03	2.26e-03	1.84e-03	0.00e+00	4.11e-03	6.50e-04	4.36e-04	4.21e-03	2.07e-03	8.64e-02		8.71e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.6045 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.818162e-321          0.000000                0          2804
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.249492e-317
%    preconditioner setup: 1.6045 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.249492e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.604547  0.000000
];

