% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_220 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	826367	806818	141586	141586	1.00e-02	4.15e-02	8.98e-03	1.98e-02	1.21e-03	0.00e+00	2.42e-03	5.23e-04	3.75e-04	1.28e-02	1.81e-03	9.95e-02		9.95e-02
1	801058	804226	121851	141400	9.97e-03	3.52e-02	3.58e-03	6.71e-03	1.10e-03	0.00e+00	3.10e-03	4.87e-04	3.94e-04	6.56e-03	2.01e-03	6.91e-02		1.69e-01
2	811711	802634	147966	144798	9.97e-03	5.19e-02	3.74e-03	4.57e-03	2.86e-04	0.00e+00	3.45e-03	7.38e-04	3.93e-04	4.26e-03	2.05e-03	8.14e-02		2.50e-01
3	809680	803970	138119	147196	9.92e-03	5.29e-02	4.24e-03	2.74e-03	3.14e-04	0.00e+00	3.75e-03	5.51e-04	4.55e-04	4.23e-03	2.04e-03	8.11e-02		3.31e-01
4	804857	806441	140956	146666	9.92e-03	5.54e-02	4.32e-03	2.14e-03	3.96e-04	0.00e+00	3.94e-03	6.43e-04	4.38e-04	4.40e-03	2.03e-03	8.36e-02		4.15e-01
5	811251	811812	146584	145000	9.95e-03	4.98e-02	4.48e-03	3.49e-03	1.53e-03	0.00e+00	4.08e-03	6.68e-04	4.45e-04	4.83e-03	2.08e-03	8.13e-02		4.96e-01
6	809119	809631	140996	140435	1.00e-02	5.60e-02	4.52e-03	3.42e-03	4.55e-04	0.00e+00	3.93e-03	7.85e-04	4.47e-04	4.80e-03	2.06e-03	8.64e-02		5.82e-01
7	813086	810922	143934	143422	9.96e-03	5.09e-02	4.52e-03	2.75e-03	4.12e-04	0.00e+00	3.98e-03	5.19e-04	4.21e-04	4.82e-03	2.06e-03	8.03e-02		6.63e-01
8	811771	808021	140773	142937	1.00e-02	5.63e-02	4.56e-03	2.78e-03	1.38e-03	0.00e+00	3.95e-03	9.19e-04	4.60e-04	4.30e-03	2.07e-03	8.68e-02		7.50e-01
9	813784	812190	142894	146644	9.97e-03	5.28e-02	4.49e-03	3.27e-03	1.62e-03	0.00e+00	3.95e-03	6.05e-04	4.26e-04	4.34e-03	2.07e-03	8.36e-02		8.33e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.5793 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.5793 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.579275  0.000000
];

