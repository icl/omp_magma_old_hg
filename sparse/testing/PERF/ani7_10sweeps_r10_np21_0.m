% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_21 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	812475	797496	141586	141586	3.69e-03	5.67e-02	1.12e-02	8.62e-03	1.20e-03	0.00e+00	1.20e-02	2.74e-04	1.27e-03	7.66e-03	7.04e-03	1.10e-01		1.10e-01
1	800444	815377	135743	150722	3.70e-03	7.09e-02	1.06e-02	4.42e-03	1.47e-03	0.00e+00	1.44e-02	2.61e-04	1.44e-03	6.40e-03	9.02e-03	1.23e-01		2.32e-01
2	811608	807562	148580	133647	3.80e-03	1.12e-01	1.38e-02	3.94e-03	1.20e-03	0.00e+00	1.78e-02	2.49e-04	1.48e-03	5.82e-03	9.83e-03	1.70e-01		4.02e-01
3	817847	808457	138222	142268	3.76e-03	1.59e-01	1.55e-02	3.25e-03	1.36e-03	0.00e+00	2.08e-02	3.22e-04	1.61e-03	5.75e-03	9.28e-03	2.20e-01		6.23e-01
4	808322	808403	132789	142179	3.75e-03	1.54e-01	1.59e-02	3.39e-03	1.71e-03	0.00e+00	2.24e-02	4.77e-04	1.71e-03	5.71e-03	9.03e-03	2.18e-01		8.40e-01
5	809125	807961	143119	143038	3.75e-03	1.47e-01	1.59e-02	3.92e-03	1.84e-03	0.00e+00	2.27e-02	2.88e-04	1.70e-03	6.27e-03	9.10e-03	2.13e-01		1.05e+00
6	810654	813742	143122	144286	3.77e-03	1.51e-01	1.60e-02	3.88e-03	1.85e-03	0.00e+00	2.31e-02	4.72e-04	1.66e-03	5.81e-03	9.29e-03	2.17e-01		1.27e+00
7	811207	811878	142399	139311	3.81e-03	1.56e-01	1.62e-02	3.44e-03	1.79e-03	0.00e+00	2.28e-02	3.23e-04	1.72e-03	6.16e-03	9.32e-03	2.22e-01		1.49e+00
8	810505	812568	142652	141981	3.77e-03	1.58e-01	1.63e-02	3.12e-03	2.36e-03	0.00e+00	2.32e-02	2.78e-04	1.75e-03	5.76e-03	9.25e-03	2.24e-01		1.72e+00
9	815742	812808	144160	142097	3.81e-03	1.57e-01	1.63e-02	3.29e-03	2.21e-03	0.00e+00	2.30e-02	3.52e-04	1.72e-03	5.77e-03	9.29e-03	2.23e-01		1.94e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 2.3440 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.249492e-317          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.273197e-313
%    preconditioner setup: 2.3440 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.133613e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  2.344025  0.000000
];

