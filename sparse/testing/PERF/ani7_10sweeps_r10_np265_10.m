% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_265 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	825495	837191	141586	141586	1.16e-02	4.49e-02	9.38e-03	2.14e-02	3.08e-03	0.00e+00	2.57e-03	5.22e-04	3.23e-04	1.47e-02	1.62e-03	1.10e-01		1.10e-01
1	813622	802821	122723	111027	1.20e-02	3.87e-02	3.14e-03	3.25e-03	2.28e-03	0.00e+00	3.44e-03	4.02e-04	3.56e-04	7.65e-03	1.75e-03	7.30e-02		1.83e-01
2	816799	798270	135402	146203	1.14e-02	5.14e-02	3.53e-03	1.45e-03	3.66e-04	0.00e+00	3.39e-03	4.28e-04	3.92e-04	4.32e-03	2.02e-03	7.87e-02		2.62e-01
3	819582	817976	133031	151560	1.15e-02	5.19e-02	4.22e-03	3.58e-03	3.11e-04	0.00e+00	3.76e-03	4.44e-04	4.44e-04	4.19e-03	2.16e-03	8.24e-02		3.44e-01
4	807478	810232	131054	132660	1.18e-02	6.01e-02	4.31e-03	2.49e-03	3.20e-04	0.00e+00	3.91e-03	7.90e-04	4.23e-04	4.76e-03	2.01e-03	9.09e-02		4.35e-01
5	811282	806387	143963	141209	1.16e-02	5.11e-02	4.10e-03	1.59e-03	4.40e-04	0.00e+00	4.05e-03	4.53e-04	4.43e-04	5.18e-03	2.01e-03	8.09e-02		5.16e-01
6	809940	815033	140965	145860	1.15e-02	5.64e-02	4.15e-03	2.65e-03	3.93e-04	0.00e+00	3.98e-03	7.59e-04	4.24e-04	4.84e-03	2.05e-03	8.72e-02		6.03e-01
7	812143	808822	143113	138020	1.18e-02	5.38e-02	4.24e-03	2.15e-03	4.58e-04	0.00e+00	4.17e-03	5.69e-04	4.38e-04	4.15e-03	2.07e-03	8.39e-02		6.87e-01
8	813394	814427	141716	145037	1.16e-02	5.75e-02	4.27e-03	3.09e-03	3.97e-04	0.00e+00	4.06e-03	6.46e-04	4.23e-04	4.58e-03	2.08e-03	8.87e-02		7.76e-01
9	813879	814112	141271	140238	1.17e-02	5.40e-02	4.29e-03	3.17e-03	1.67e-03	0.00e+00	4.16e-03	1.04e-03	4.73e-04	4.28e-03	2.10e-03	8.68e-02		8.63e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.9949 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.9949 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.994860  0.000000
];

