% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_70 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	809881	806921	141586	141586	5.07e-03	3.03e-02	6.39e-03	9.95e-03	5.86e-04	0.00e+00	3.68e-03	3.60e-04	5.68e-04	6.61e-03	2.76e-03	6.63e-02		6.63e-02
1	801903	800650	138337	141297	5.07e-03	2.92e-02	5.27e-03	3.43e-03	8.37e-04	0.00e+00	4.20e-03	3.03e-04	5.77e-04	4.43e-03	2.83e-03	5.62e-02		1.22e-01
2	816550	818200	147121	148374	5.00e-03	5.08e-02	5.23e-03	2.92e-03	3.95e-04	0.00e+00	5.20e-03	4.37e-04	5.22e-04	3.52e-03	3.11e-03	7.72e-02		2.00e-01
3	823832	820241	133280	131630	5.15e-03	6.66e-02	5.51e-03	1.67e-03	5.25e-04	0.00e+00	5.93e-03	3.29e-04	6.49e-04	3.59e-03	3.00e-03	9.30e-02		2.93e-01
4	808353	816988	126804	130395	5.18e-03	6.63e-02	5.67e-03	1.61e-03	8.16e-04	0.00e+00	6.52e-03	5.50e-04	6.57e-04	3.86e-03	2.74e-03	9.39e-02		3.87e-01
5	808892	808929	143088	134453	5.13e-03	5.81e-02	5.35e-03	2.09e-03	9.26e-04	0.00e+00	6.67e-03	4.57e-04	6.51e-04	3.86e-03	2.70e-03	8.59e-02		4.72e-01
6	809126	806633	143355	143318	5.11e-03	5.92e-02	5.33e-03	1.54e-03	5.84e-04	0.00e+00	6.49e-03	3.99e-04	6.21e-04	3.52e-03	2.75e-03	8.55e-02		5.58e-01
7	812079	811231	143927	146420	5.03e-03	5.70e-02	5.33e-03	1.62e-03	8.83e-04	0.00e+00	6.66e-03	5.09e-04	6.64e-04	3.55e-03	3.56e-03	8.48e-02		6.43e-01
8	813216	812569	141780	142628	5.01e-03	6.00e-02	5.42e-03	1.37e-03	7.60e-04	0.00e+00	6.44e-03	4.30e-04	6.28e-04	3.64e-03	2.78e-03	8.65e-02		7.29e-01
9	811209	813209	141449	142096	5.13e-03	5.79e-02	5.51e-03	2.57e-03	1.16e-03	0.00e+00	6.55e-03	6.34e-04	6.44e-04	3.47e-03	2.78e-03	8.63e-02		8.16e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.2931 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.2931 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.293060  0.000000
];

