% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_52 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	818212	803359	141586	141586	3.23e-03	2.92e-02	5.13e-03	6.77e-03	5.90e-04	0.00e+00	4.65e-03	2.44e-04	5.39e-04	5.98e-03	2.79e-03	5.91e-02		5.91e-02
1	814845	810049	130006	144859	3.23e-03	3.39e-02	4.07e-03	2.62e-03	1.28e-03	0.00e+00	5.74e-03	2.16e-04	5.59e-04	4.16e-03	3.61e-03	5.94e-02		1.19e-01
2	795264	782742	134179	138975	3.29e-03	5.52e-02	5.31e-03	2.30e-03	5.25e-04	0.00e+00	6.89e-03	3.26e-04	6.48e-04	3.43e-03	3.65e-03	8.16e-02		2.00e-01
3	814996	801846	154566	167088	3.19e-03	6.81e-02	5.62e-03	2.32e-03	6.95e-04	0.00e+00	7.81e-03	3.13e-04	7.16e-04	3.44e-03	3.75e-03	9.60e-02		2.96e-01
4	803520	811938	135640	148790	3.26e-03	7.45e-02	6.51e-03	1.57e-03	8.74e-04	0.00e+00	8.57e-03	4.18e-04	7.72e-04	3.60e-03	3.59e-03	1.04e-01		4.00e-01
5	807621	807266	147921	139503	3.30e-03	6.72e-02	6.14e-03	2.79e-03	9.42e-04	0.00e+00	8.86e-03	4.50e-04	7.33e-04	3.98e-03	3.63e-03	9.80e-02		4.98e-01
6	815381	810226	144626	144981	3.30e-03	6.86e-02	6.14e-03	1.49e-03	7.85e-04	0.00e+00	8.67e-03	3.74e-04	7.43e-04	3.63e-03	3.60e-03	9.73e-02		5.95e-01
7	807258	811944	137672	142827	3.32e-03	7.04e-02	6.25e-03	2.34e-03	8.31e-04	0.00e+00	8.58e-03	3.97e-04	7.14e-04	3.46e-03	3.58e-03	9.99e-02		6.95e-01
8	812435	813535	146601	141915	3.33e-03	6.98e-02	6.21e-03	1.76e-03	8.40e-04	0.00e+00	8.70e-03	3.80e-04	7.68e-04	3.64e-03	3.65e-03	9.91e-02		7.94e-01
9	815852	809922	142230	141130	3.31e-03	6.99e-02	6.28e-03	1.81e-03	1.48e-03	0.00e+00	8.66e-03	3.01e-04	7.44e-04	3.47e-03	3.63e-03	9.96e-02		8.94e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.2622 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.2622 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.262150  0.000000
];

