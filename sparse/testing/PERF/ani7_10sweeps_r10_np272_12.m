% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_272 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	815566	804748	141586	141586	1.16e-02	4.52e-02	9.82e-03	2.25e-02	2.52e-03	0.00e+00	2.55e-03	5.61e-04	3.23e-04	1.40e-02	1.59e-03	1.11e-01		1.11e-01
1	804639	808066	132652	143470	1.16e-02	3.89e-02	2.93e-03	9.30e-03	2.50e-03	0.00e+00	3.10e-03	4.42e-04	3.48e-04	7.35e-03	1.98e-03	7.85e-02		1.89e-01
2	814914	811679	144385	140958	1.17e-02	5.62e-02	3.67e-03	2.28e-03	1.62e-03	0.00e+00	3.50e-03	5.85e-04	4.65e-04	4.17e-03	2.12e-03	8.63e-02		2.75e-01
3	802451	796811	134916	138151	1.18e-02	5.66e-02	4.77e-03	2.43e-03	3.42e-04	0.00e+00	3.83e-03	5.41e-04	4.94e-04	4.17e-03	2.01e-03	8.70e-02		3.62e-01
4	804770	812473	148185	153825	1.15e-02	5.68e-02	3.98e-03	2.68e-03	5.30e-04	0.00e+00	3.95e-03	5.15e-04	4.87e-04	4.21e-03	1.99e-03	8.66e-02		4.49e-01
5	814716	812469	146671	138968	1.17e-02	5.25e-02	4.10e-03	3.36e-03	4.25e-04	0.00e+00	4.00e-03	7.64e-04	4.31e-04	5.05e-03	2.04e-03	8.44e-02		5.33e-01
6	811341	811188	137531	139778	1.18e-02	5.85e-02	4.24e-03	1.67e-03	6.18e-04	0.00e+00	4.04e-03	6.89e-04	4.39e-04	4.54e-03	2.07e-03	8.86e-02		6.22e-01
7	809167	812899	141712	141865	1.18e-02	5.42e-02	4.17e-03	2.45e-03	3.82e-04	0.00e+00	4.04e-03	7.16e-04	4.30e-04	4.49e-03	2.05e-03	8.47e-02		7.07e-01
8	811551	809741	144692	140960	1.17e-02	5.87e-02	4.19e-03	2.80e-03	1.72e-03	0.00e+00	4.02e-03	6.92e-04	4.36e-04	4.51e-03	2.05e-03	9.08e-02		7.97e-01
9	813790	813277	143114	144924	1.17e-02	5.59e-02	4.21e-03	1.55e-03	1.96e-03	0.00e+00	4.12e-03	9.05e-04	4.68e-04	4.28e-03	2.07e-03	8.71e-02		8.85e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.9995 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.273197e-313          0.000000                0          401424
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 5.442627e-320
%    preconditioner setup: 1.9995 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.036131e-317
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.999483  0.000000
];

