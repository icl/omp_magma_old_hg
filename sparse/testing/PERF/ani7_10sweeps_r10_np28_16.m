% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_28 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	815538	815647	141586	141586	3.59e-03	4.56e-02	8.77e-03	7.25e-03	8.97e-04	0.00e+00	9.04e-03	3.33e-04	9.46e-04	6.60e-03	5.32e-03	8.83e-02		8.83e-02
1	810814	809635	132680	132571	3.62e-03	5.62e-02	8.03e-03	2.40e-03	9.12e-04	0.00e+00	1.11e-02	2.43e-04	1.06e-03	5.59e-03	6.87e-03	9.60e-02		1.84e-01
2	804954	801097	138210	139389	3.65e-03	8.79e-02	1.36e-02	3.30e-03	8.77e-04	0.00e+00	1.34e-02	3.04e-04	1.24e-03	4.97e-03	7.42e-03	1.37e-01		3.21e-01
3	810441	801197	144876	148733	5.26e-03	1.32e-01	1.42e-02	2.72e-03	1.36e-03	0.00e+00	1.55e-02	2.19e-04	1.29e-03	4.93e-03	7.26e-03	1.85e-01		5.06e-01
4	805924	799055	140195	149439	5.29e-03	1.24e-01	1.19e-02	2.55e-03	1.79e-03	0.00e+00	1.71e-02	3.34e-04	1.47e-03	5.19e-03	6.74e-03	1.76e-01		6.82e-01
5	810746	809564	145517	152386	3.62e-03	1.21e-01	1.19e-02	3.38e-03	1.96e-03	0.00e+00	1.66e-02	1.95e-04	1.37e-03	5.27e-03	6.91e-03	1.72e-01		8.54e-01
6	810739	810611	141501	142683	3.67e-03	1.27e-01	1.22e-02	3.32e-03	1.53e-03	0.00e+00	1.70e-02	3.50e-04	1.35e-03	5.10e-03	6.92e-03	1.78e-01		1.03e+00
7	810786	813160	142314	142442	3.64e-03	1.27e-01	1.22e-02	2.59e-03	1.46e-03	0.00e+00	1.68e-02	3.94e-04	1.37e-03	5.16e-03	6.99e-03	1.78e-01		1.21e+00
8	809785	806997	143073	140699	3.70e-03	1.28e-01	1.23e-02	2.46e-03	1.66e-03	0.00e+00	1.64e-02	5.34e-04	1.32e-03	4.98e-03	6.89e-03	1.78e-01		1.39e+00
9	814032	814385	144880	147668	3.66e-03	1.26e-01	1.23e-02	3.17e-03	1.86e-03	0.00e+00	1.72e-02	3.47e-04	1.40e-03	4.94e-03	7.04e-03	1.78e-01		1.57e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.9615 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 1.9615 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.961530  0.000000
];

