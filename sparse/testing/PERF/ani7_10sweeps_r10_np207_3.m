% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_207 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	815900	815308	141586	141586	1.04e-02	4.07e-02	8.36e-03	1.81e-02	1.39e-03	0.00e+00	2.58e-03	5.10e-04	3.73e-04	1.27e-02	1.97e-03	9.71e-02		9.71e-02
1	803400	791279	132318	132910	1.06e-02	3.67e-02	3.76e-03	5.16e-03	2.62e-03	0.00e+00	3.38e-03	4.60e-04	3.64e-04	6.92e-03	2.12e-03	7.21e-02		1.69e-01
2	809602	807998	145624	157745	9.40e-03	5.44e-02	3.82e-03	2.15e-03	3.26e-04	0.00e+00	3.60e-03	5.85e-04	4.34e-04	4.32e-03	2.23e-03	8.13e-02		2.51e-01
3	817239	796905	140228	141832	9.58e-03	5.42e-02	4.50e-03	2.37e-03	4.06e-04	0.00e+00	4.02e-03	5.46e-04	4.93e-04	4.33e-03	2.17e-03	8.27e-02		3.33e-01
4	805013	814886	133397	153731	9.49e-03	5.74e-02	4.25e-03	3.88e-03	5.63e-04	0.00e+00	4.26e-03	6.79e-04	5.30e-04	4.77e-03	2.14e-03	8.80e-02		4.21e-01
5	808167	815333	146428	136555	9.62e-03	5.14e-02	4.22e-03	3.43e-03	4.86e-04	0.00e+00	4.22e-03	7.89e-04	4.60e-04	5.00e-03	2.12e-03	8.17e-02		5.03e-01
6	805815	807802	144080	136914	9.67e-03	5.65e-02	4.24e-03	2.20e-03	4.98e-04	0.00e+00	4.21e-03	7.75e-04	4.55e-04	4.68e-03	2.10e-03	8.53e-02		5.88e-01
7	812594	810284	147238	145251	9.64e-03	5.15e-02	4.25e-03	2.41e-03	3.96e-04	0.00e+00	4.03e-03	8.33e-04	4.60e-04	4.39e-03	2.16e-03	8.00e-02		6.68e-01
8	811611	811725	141265	143575	9.61e-03	5.73e-02	4.30e-03	3.11e-03	5.88e-04	0.00e+00	4.38e-03	5.10e-04	4.53e-04	4.59e-03	2.14e-03	8.70e-02		7.55e-01
9	813541	815709	143054	142940	9.65e-03	5.31e-02	4.34e-03	2.52e-03	1.52e-03	0.00e+00	4.33e-03	5.36e-04	5.04e-04	4.31e-03	2.17e-03	8.30e-02		8.38e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.5848 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 1.5848 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.584792  0.000000
];

