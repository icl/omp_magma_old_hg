% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_155 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	811238	804183	141586	141586	7.86e-03	3.61e-02	7.62e-03	1.46e-02	1.51e-03	0.00e+00	2.52e-03	4.37e-04	4.29e-04	1.02e-02	2.07e-03	8.34e-02		8.34e-02
1	808087	798147	136980	144035	7.82e-03	2.92e-02	3.90e-03	1.96e-03	8.33e-04	0.00e+00	2.98e-03	4.35e-04	3.96e-04	5.15e-03	2.17e-03	5.49e-02		1.38e-01
2	803308	810081	140937	150877	7.72e-03	4.69e-02	4.16e-03	1.91e-03	4.22e-04	0.00e+00	3.37e-03	4.04e-04	4.02e-04	4.36e-03	2.21e-03	7.19e-02		2.10e-01
3	797773	808124	146522	139749	7.83e-03	4.71e-02	4.44e-03	2.72e-03	3.37e-04	0.00e+00	3.89e-03	5.06e-04	4.43e-04	3.94e-03	2.19e-03	7.34e-02		2.84e-01
4	803713	819727	152863	142512	7.80e-03	4.86e-02	4.70e-03	2.67e-03	4.74e-04	0.00e+00	4.04e-03	4.26e-04	4.80e-04	3.98e-03	2.25e-03	7.54e-02		3.59e-01
5	811031	816833	147728	131714	7.91e-03	4.48e-02	4.96e-03	2.79e-03	3.56e-04	0.00e+00	3.97e-03	4.09e-04	4.81e-04	4.49e-03	2.35e-03	7.25e-02		4.31e-01
6	805452	803843	141216	135414	7.91e-03	4.94e-02	5.10e-03	3.87e-03	3.72e-04	0.00e+00	4.02e-03	6.78e-04	4.77e-04	3.92e-03	2.18e-03	7.79e-02		5.09e-01
7	817593	811386	147601	149210	7.74e-03	4.43e-02	4.83e-03	2.31e-03	4.58e-04	0.00e+00	4.19e-03	3.66e-04	4.42e-04	5.11e-03	2.27e-03	7.20e-02		5.81e-01
8	809993	812392	136266	142473	7.85e-03	5.12e-02	5.15e-03	1.82e-03	1.23e-03	0.00e+00	4.06e-03	5.20e-04	4.45e-04	4.01e-03	2.21e-03	7.85e-02		6.60e-01
9	814586	812769	144672	142273	7.90e-03	4.58e-02	4.99e-03	1.95e-03	1.09e-03	0.00e+00	4.08e-03	5.78e-04	4.45e-04	3.95e-03	2.29e-03	7.31e-02		7.33e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.3829 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.983298e-318
%    preconditioner setup: 1.3829 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.983298e-318
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.382860  0.000000
];

