% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_40 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	819477	800864	141586	141586	3.27e-03	3.40e-02	6.14e-03	6.76e-03	6.20e-04	0.00e+00	6.04e-03	2.74e-04	6.31e-04	5.86e-03	3.51e-03	6.71e-02		6.71e-02
1	786473	803088	128741	147354	3.24e-03	3.94e-02	5.25e-03	2.77e-03	8.40e-04	0.00e+00	7.37e-03	2.57e-04	7.14e-04	4.38e-03	4.47e-03	6.87e-02		1.36e-01
2	799027	807034	162551	145936	3.30e-03	6.20e-02	6.64e-03	2.00e-03	6.10e-04	0.00e+00	8.74e-03	2.47e-04	7.62e-04	3.88e-03	4.86e-03	9.30e-02		2.29e-01
3	803992	801095	150803	142796	3.32e-03	8.72e-02	7.49e-03	2.41e-03	7.36e-04	0.00e+00	1.03e-02	2.36e-04	8.68e-04	3.83e-03	4.61e-03	1.21e-01		3.50e-01
4	807669	799620	146644	149541	3.89e-03	8.54e-02	7.86e-03	3.00e-03	1.06e-03	0.00e+00	1.10e-02	3.45e-04	9.22e-04	4.17e-03	4.50e-03	1.22e-01		4.72e-01
5	811499	808731	143772	151821	3.30e-03	8.28e-02	7.91e-03	2.30e-03	1.20e-03	0.00e+00	1.14e-02	4.09e-04	9.41e-04	4.29e-03	4.59e-03	1.19e-01		5.91e-01
6	807647	813067	140748	143516	3.31e-03	8.78e-02	8.08e-03	2.06e-03	1.29e-03	0.00e+00	1.17e-02	2.78e-04	9.70e-04	4.02e-03	4.60e-03	1.24e-01		7.15e-01
7	811961	815052	145406	139986	3.34e-03	8.74e-02	8.08e-03	2.22e-03	9.26e-04	0.00e+00	1.09e-02	3.98e-04	8.87e-04	3.87e-03	5.28e-03	1.23e-01		8.38e-01
8	813570	811479	141898	138807	5.30e-03	8.96e-02	8.18e-03	1.94e-03	1.07e-03	0.00e+00	1.10e-02	3.31e-04	9.13e-04	3.96e-03	4.63e-03	1.27e-01		9.65e-01
9	814301	814906	141095	143186	3.35e-03	8.89e-02	8.19e-03	1.84e-03	1.37e-03	0.00e+00	1.09e-02	4.26e-04	8.98e-04	3.85e-03	4.61e-03	1.24e-01		1.09e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.4624 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.4624 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.462358  0.000000
];

