% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_1 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	812061	804758	141586	141586	9.91e-03	9.23e-01	2.14e-01	4.19e-02	1.25e-02	0.00e+00	1.80e-01	2.76e-04	2.20e-02	7.14e-02	1.34e-01	1.61e+00		1.61e+00
1	811011	809745	136157	143460	1.10e-02	1.06e+00	2.12e-01	3.87e-02	1.52e-02	0.00e+00	2.04e-01	2.03e-04	2.29e-02	6.72e-02	1.55e-01	1.79e+00		3.40e+00
2	802627	819076	138013	139279	1.11e-02	1.38e+00	2.50e-01	3.87e-02	1.49e-02	0.00e+00	2.23e-01	2.41e-04	2.24e-02	6.39e-02	1.62e-01	2.16e+00		5.56e+00
3	806253	805439	147203	130754	1.10e-02	1.64e+00	2.69e-01	4.15e-02	1.71e-02	0.00e+00	2.41e-01	2.51e-04	2.30e-02	6.36e-02	1.56e-01	2.46e+00		8.03e+00
4	812132	807184	144383	145197	1.08e-02	1.68e+00	2.80e-01	4.29e-02	1.94e-02	0.00e+00	2.46e-01	4.31e-04	2.32e-02	6.45e-02	1.57e-01	2.52e+00		1.05e+01
5	809108	806956	139309	144257	1.09e-02	1.80e+00	2.90e-01	4.34e-02	1.98e-02	0.00e+00	2.46e-01	3.57e-04	2.32e-02	6.45e-02	1.56e-01	2.65e+00		1.32e+01
6	811482	813680	143139	145291	1.08e-02	1.80e+00	2.89e-01	4.18e-02	1.90e-02	0.00e+00	2.47e-01	3.19e-04	2.31e-02	6.48e-02	1.58e-01	2.65e+00		1.58e+01
7	810722	812806	141571	139373	1.10e-02	1.85e+00	2.94e-01	4.38e-02	2.08e-02	0.00e+00	2.56e-01	4.07e-04	2.39e-02	6.39e-02	1.58e-01	2.72e+00		1.86e+01
8	814078	814640	143137	141053	1.09e-02	1.84e+00	2.94e-01	4.12e-02	1.89e-02	0.00e+00	2.44e-01	3.21e-04	2.27e-02	6.38e-02	1.59e-01	2.70e+00		2.13e+01
9	813416	813074	140587	140025	1.10e-02	1.87e+00	2.98e-01	4.18e-02	1.87e-02	0.00e+00	2.46e-01	4.69e-04	2.28e-02	6.35e-02	1.58e-01	2.73e+00		2.40e+01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 25.3372 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 25.3372 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  25.337245  0.000000
];

