% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_264 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	811361	818199	141586	141586	1.15e-02	4.51e-02	9.58e-03	2.11e-02	2.85e-03	0.00e+00	2.57e-03	4.66e-04	3.31e-04	1.45e-02	1.53e-03	1.10e-01		1.10e-01
1	809845	800582	136857	130019	1.16e-02	3.75e-02	3.10e-03	2.54e-03	2.82e-03	0.00e+00	3.31e-03	3.80e-04	3.88e-04	7.56e-03	1.80e-03	7.10e-02		1.81e-01
2	806773	807725	139179	148442	1.14e-02	5.14e-02	3.54e-03	4.02e-03	3.04e-04	0.00e+00	3.45e-03	6.31e-04	3.83e-04	4.30e-03	2.02e-03	8.14e-02		2.62e-01
3	808028	822441	143057	142105	1.14e-02	5.18e-02	4.10e-03	2.57e-03	3.05e-04	0.00e+00	3.83e-03	4.75e-04	4.27e-04	4.27e-03	2.08e-03	8.13e-02		3.43e-01
4	814561	808081	142608	128195	1.17e-02	5.80e-02	4.25e-03	1.87e-03	6.34e-04	0.00e+00	4.00e-03	9.46e-04	4.11e-04	4.33e-03	2.03e-03	8.81e-02		4.31e-01
5	804159	805067	136880	143360	1.16e-02	5.20e-02	4.17e-03	2.30e-03	3.54e-04	0.00e+00	4.04e-03	5.87e-04	3.94e-04	5.18e-03	2.02e-03	8.26e-02		5.14e-01
6	810870	810743	148088	147180	1.14e-02	5.72e-02	4.13e-03	2.20e-03	4.32e-04	0.00e+00	4.05e-03	5.14e-04	4.33e-04	4.62e-03	2.05e-03	8.71e-02		6.01e-01
7	812559	810485	142183	142310	1.16e-02	5.40e-02	4.27e-03	1.77e-03	3.51e-04	0.00e+00	3.96e-03	6.77e-04	3.90e-04	4.29e-03	2.08e-03	8.33e-02		6.84e-01
8	811819	813378	141300	143374	1.14e-02	5.92e-02	4.51e-03	2.16e-03	4.12e-04	0.00e+00	4.03e-03	5.11e-04	4.33e-04	4.50e-03	2.11e-03	8.94e-02		7.74e-01
9	811465	813972	142846	141287	1.16e-02	5.44e-02	4.34e-03	4.13e-03	1.92e-03	0.00e+00	4.13e-03	7.61e-04	4.08e-04	4.27e-03	2.07e-03	8.80e-02		8.62e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.5987 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       1.983298e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 3.952525e-323
%    preconditioner setup: 1.5987 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 9.320673e-314
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.598667  0.000000
];

