% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_9 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	829673	798406	141586	141586	4.24e-03	1.16e-01	2.50e-02	9.75e-03	2.36e-03	0.00e+00	2.74e-02	3.12e-04	2.74e-03	1.16e-02	1.62e-02	2.16e-01		2.16e-01
1	819214	809463	118545	149812	4.26e-03	1.54e-01	2.47e-02	6.30e-03	2.78e-03	0.00e+00	3.43e-02	2.62e-04	3.03e-03	1.11e-02	2.06e-02	2.61e-01		4.77e-01
2	816756	809762	129810	139561	4.41e-03	2.38e-01	3.18e-02	5.31e-03	2.67e-03	0.00e+00	4.03e-02	3.18e-04	3.18e-03	1.05e-02	2.25e-02	3.59e-01		8.36e-01
3	796336	807747	133074	140068	4.37e-03	3.39e-01	3.51e-02	5.81e-03	3.05e-03	0.00e+00	4.68e-02	2.73e-04	3.69e-03	1.03e-02	2.05e-02	4.69e-01		1.31e+00
4	813670	814216	154300	142889	4.36e-03	2.98e-01	3.52e-02	6.07e-03	3.84e-03	0.00e+00	4.87e-02	2.84e-04	3.60e-03	1.03e-02	2.07e-02	4.31e-01		1.74e+00
5	810548	809318	137771	137225	4.39e-03	3.35e-01	3.66e-02	8.63e-03	4.69e-03	0.00e+00	5.18e-02	2.71e-04	3.81e-03	1.11e-02	2.07e-02	4.77e-01		2.21e+00
6	809774	812272	141699	142929	4.39e-03	3.35e-01	3.65e-02	6.76e-03	4.20e-03	0.00e+00	5.09e-02	4.35e-04	3.72e-03	1.04e-02	2.07e-02	4.73e-01		2.69e+00
7	813199	809565	143279	140781	4.40e-03	3.37e-01	3.68e-02	6.04e-03	3.75e-03	0.00e+00	4.95e-02	3.19e-04	3.62e-03	1.06e-02	2.07e-02	4.73e-01		3.16e+00
8	809606	812237	140660	144294	4.37e-03	3.40e-01	3.71e-02	5.80e-03	3.94e-03	0.00e+00	4.95e-02	3.19e-04	3.57e-03	1.05e-02	2.07e-02	4.76e-01		3.63e+00
9	811284	816677	145059	142428	4.40e-03	3.38e-01	3.72e-02	6.03e-03	4.34e-03	0.00e+00	5.14e-02	3.97e-04	3.73e-03	1.04e-02	2.08e-02	4.77e-01		4.11e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 4.5895 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 4.5895 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  4.589517  0.000000
];

