% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_16 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	823198	811664	141586	141586	3.85e-03	7.10e-02	1.45e-02	7.92e-03	1.20e-03	0.00e+00	1.56e-02	2.95e-04	1.59e-03	8.84e-03	9.25e-03	1.34e-01		1.34e-01
1	810249	794420	125020	136554	3.86e-03	8.74e-02	1.40e-02	4.42e-03	1.45e-03	0.00e+00	1.94e-02	2.30e-04	1.73e-03	7.34e-03	1.17e-02	1.52e-01		2.86e-01
2	802610	811369	138775	154604	3.83e-03	1.44e-01	1.78e-02	4.21e-03	1.38e-03	0.00e+00	2.36e-02	3.46e-04	1.88e-03	6.89e-03	1.27e-02	2.16e-01		5.02e-01
3	813850	816878	147220	138461	3.93e-03	2.04e-01	2.01e-02	3.68e-03	1.81e-03	0.00e+00	2.65e-02	2.95e-04	2.10e-03	6.84e-03	1.24e-02	2.82e-01		7.84e-01
4	810604	803309	136786	133758	3.99e-03	2.01e-01	2.09e-02	3.82e-03	1.91e-03	0.00e+00	2.79e-02	4.43e-04	2.07e-03	7.19e-03	1.16e-02	2.81e-01		1.06e+00
5	804207	809398	140837	148132	3.88e-03	1.92e-01	2.05e-02	3.73e-03	2.67e-03	0.00e+00	2.93e-02	4.66e-04	2.16e-03	7.10e-03	1.17e-02	2.73e-01		1.34e+00
6	809629	810927	148040	142849	3.94e-03	1.96e-01	2.05e-02	3.68e-03	2.42e-03	0.00e+00	2.90e-02	4.42e-04	2.16e-03	6.92e-03	1.18e-02	2.77e-01		1.61e+00
7	811473	810655	143424	142126	3.92e-03	2.05e-01	2.08e-02	3.75e-03	2.26e-03	0.00e+00	2.91e-02	2.98e-04	2.19e-03	6.92e-03	1.19e-02	2.86e-01		1.90e+00
8	814142	815080	142386	143204	3.91e-03	2.07e-01	2.09e-02	3.68e-03	2.64e-03	0.00e+00	2.95e-02	2.98e-04	2.20e-03	7.23e-03	1.20e-02	2.90e-01		2.19e+00
9	813260	814355	140523	139585	3.98e-03	2.09e-01	2.11e-02	3.64e-03	2.53e-03	0.00e+00	2.89e-02	4.58e-04	2.13e-03	6.89e-03	1.20e-02	2.91e-01		2.48e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 2.9065 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       3.952525e-323          0.000000                0          1685382481
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.818162e-321
%    preconditioner setup: 2.9065 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 1.818162e-321
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  2.906483  0.000000
];

