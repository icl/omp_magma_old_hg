% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (test_matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_19 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	809763	809645	141586	141586	3.74e-03	6.12e-02	1.23e-02	7.62e-03	1.23e-03	0.00e+00	1.33e-02	2.83e-04	1.38e-03	8.11e-03	7.69e-03	1.17e-01		1.17e-01
1	813922	809397	138455	138573	3.74e-03	7.63e-02	1.17e-02	4.58e-03	1.27e-03	0.00e+00	1.61e-02	2.60e-04	1.47e-03	6.68e-03	1.00e-02	1.32e-01		2.49e-01
2	801658	808460	135102	139627	3.83e-03	1.23e-01	1.53e-02	3.41e-03	1.15e-03	0.00e+00	2.02e-02	3.48e-04	1.60e-03	6.43e-03	1.07e-02	1.86e-01		4.35e-01
3	817298	807867	148172	141370	3.84e-03	1.66e-01	1.68e-02	3.44e-03	1.56e-03	0.00e+00	2.27e-02	2.76e-04	1.72e-03	6.09e-03	1.02e-02	2.32e-01		6.67e-01
4	808824	818681	133338	142769	3.80e-03	1.63e-01	1.74e-02	3.64e-03	2.43e-03	0.00e+00	2.56e-02	3.47e-04	1.93e-03	6.08e-03	1.01e-02	2.34e-01		9.01e-01
5	805889	804756	142617	132760	3.85e-03	1.72e-01	1.77e-02	3.17e-03	1.73e-03	0.00e+00	2.39e-02	3.30e-04	1.75e-03	6.83e-03	9.98e-03	2.41e-01		1.14e+00
6	810733	811999	146358	147491	3.78e-03	1.69e-01	1.74e-02	4.02e-03	2.04e-03	0.00e+00	2.50e-02	2.58e-04	1.82e-03	6.15e-03	1.01e-02	2.39e-01		1.38e+00
7	813440	812559	142320	141054	3.81e-03	1.74e-01	1.79e-02	3.92e-03	1.86e-03	0.00e+00	2.44e-02	4.00e-04	1.81e-03	6.36e-03	1.02e-02	2.45e-01		1.63e+00
8	814362	808023	140419	141300	3.86e-03	1.77e-01	1.80e-02	3.14e-03	2.40e-03	0.00e+00	2.54e-02	3.82e-04	1.86e-03	6.20e-03	1.01e-02	2.48e-01		1.88e+00
9	815132	813023	140303	146642	3.84e-03	1.75e-01	1.79e-02	3.72e-03	2.03e-03	0.00e+00	2.42e-02	3.26e-04	1.77e-03	6.11e-03	1.02e-02	2.45e-01		2.12e+00
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 2.5282 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.442627e-320          0.000000                0            8
%=================================================================================%

%=================================================================================%
% PCGS solver summary:
%    initial residual: 1.248788e-317
%    preconditioner setup: 2.5282 sec
%    iterations:    0
%    SpMV-count:    0
%    exact final residual: 2.984157e-320
%    runtime: 0.0000 sec
%    preconditioner runtime: 0.0000 sec
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  2.528205  0.000000
];

