% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (/home/hanzt/matrices/G3_circuit.mtx): done. Converting to CSR:
% Detected symmetric case. done.
performance_68 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	4728683	4759318	843134	843134	4.37e-02	1.52e-01	2.18e-02	1.19e-02	4.74e-03	0.00e+00	2.30e-02	2.64e-04	1.92e-03	3.24e-02	1.46e-02	3.06e-01		3.06e-01
1	4775680	4739337	814656	784021	4.45e-02	1.55e-01	1.96e-02	5.86e-03	4.93e-03	0.00e+00	2.22e-02	3.30e-04	2.07e-03	2.91e-02	1.39e-02	2.98e-01		6.04e-01
