% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (/home/hanzt/matrices/ani7_crop.mtx): done. Converting to CSR: done.
performance_68 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	830375	820714	154211	154211	7.50e-03	2.67e-02	4.62e-03	9.68e-03	7.31e-04	0.00e+00	3.58e-03	2.80e-04	4.11e-04	6.01e-03	2.26e-03	6.17e-02		6.17e-02
1	841916	833160	143092	152753	7.53e-03	3.03e-02	3.65e-03	4.67e-03	8.78e-04	0.00e+00	4.44e-03	2.56e-04	4.83e-04	4.34e-03	2.97e-03	5.95e-02		1.21e-01
2	850232	844847	144982	153738	7.66e-03	5.12e-02	4.69e-03	2.88e-03	5.31e-04	0.00e+00	5.48e-03	2.15e-04	4.99e-04	3.48e-03	3.40e-03	8.01e-02		2.01e-01
];

% matrix info: 203841-by-203841 with 1407811 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    203841    203841         1407811                6           1407811
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.869836e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
%   Solver info not supported.
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       5.869836e-318          0.000000                0            0
%=================================================================================%

%=================================================================================%
%   Solver info not supported.
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  0.621576  0.000000
];

