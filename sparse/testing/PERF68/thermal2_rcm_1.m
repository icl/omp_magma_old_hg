% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (/home/hanzt/matrices/thermal2_rcm.mtx): done. Converting to CSR:
% Detected symmetric case. done.
performance_68 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	4967125	4932677	939767	939767	1.03e+00	4.32e+00	8.73e-01	6.58e-01	4.15e-01	0.00e+00	7.28e-01	3.25e-02	6.48e-02	1.35e+00	8.91e-01	1.04e+01		1.04e+01
1	5087363	5041881	958557	993005	1.07e+00	4.33e+00	2.94e-01	1.24e-01	4.00e-02	0.00e+00	7.80e-02	1.71e-02	5.99e-02	6.28e-02	4.42e-02	6.12e+00		1.65e+01
2	5138372	5142163	920055	965537	9.49e-02	3.54e-01	5.49e-02	1.19e-01	3.50e-02	0.00e+00	7.40e-02	2.11e-02	6.89e-02	5.79e-02	4.60e-02	9.26e-01		1.74e+01
];

% matrix info: 1228045-by-1228045 with 8580313 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
   1228045   1228045         8580313                6           8580313
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       0.000000e+00          0.000000                0            1
%=================================================================================%

%=================================================================================%
%   Solver info not supported.
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       0.000000e+00          0.000000                0            1
%=================================================================================%

%=================================================================================%
%   Solver info not supported.
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  25.900088  0.000000
];

