% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (/home/hanzt/matrices/apache2.mtx): done. Converting to CSR:
% Detected symmetric case. done.
performance_68 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	2856222	2943276	527895	527895	6.98e-01	3.02e+00	7.06e-01	5.65e-01	1.45e-01	0.00e+00	4.49e-01	6.66e-02	7.11e-02	8.00e-01	4.94e-01	7.02e+00		7.02e+00
1	2873216	2963357	484305	397251	7.64e-01	3.18e+00	8.50e-01	2.42e-01	1.43e-01	0.00e+00	7.73e-01	5.20e-04	2.95e-01	1.88e-01	3.22e-02	6.46e+00		1.35e+01
2	2911600	2911506	513420	423279	5.99e-02	2.76e-01	7.87e-02	1.41e-01	3.00e-02	0.00e+00	3.50e-02	1.51e-02	5.28e-02	4.59e-02	2.81e-02	7.63e-01		1.42e+01
];

% matrix info: 715176-by-715176 with 4817870 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    715176    715176         4817870                6           4817870
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       0.000000e+00          0.000000                0            1
%=================================================================================%

%=================================================================================%
%   Solver info not supported.
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       0.000000e+00          0.000000                0            1
%=================================================================================%

%=================================================================================%
%   Solver info not supported.
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  19.583627  0.000000
];

