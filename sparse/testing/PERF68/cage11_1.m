% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (/home/hanzt/matrices/cage11.mtx): done. Converting to CSR: done.
performance_68 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	303576	304950	60962	60962	3.16e-02	3.44e-01	3.47e-02	1.61e-01	3.30e-02	0.00e+00	2.80e-02	2.22e-02	6.88e-02	4.20e-02	2.80e-02	7.94e-01		7.94e-01
1	309662	309061	61778	60404	2.09e-02	4.07e-01	3.39e-02	1.19e-01	3.00e-02	0.00e+00	2.50e-02	1.61e-02	4.69e-02	3.70e-02	1.90e-02	7.55e-01		1.55e+00
