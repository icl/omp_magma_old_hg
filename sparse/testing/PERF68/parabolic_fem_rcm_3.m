% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (/home/hanzt/matrices/parabolic_fem_rcm.mtx): done. Converting to CSR:
% Detected symmetric case. done.
performance_68 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	2148923	2131773	402465	402465	1.84e-02	5.74e-02	9.78e-03	1.04e-02	1.24e-03	0.00e+00	8.32e-03	2.10e-04	1.02e-03	1.11e-02	6.43e-03	1.24e-01		1.24e-01
1	2174351	2182616	388771	405921	1.86e-02	7.27e-02	9.86e-03	2.30e-03	2.86e-03	0.00e+00	8.54e-03	3.76e-04	1.05e-03	9.95e-03	6.01e-03	1.32e-01		2.57e-01
2	2205694	2200381	398347	390082	1.86e-02	7.09e-02	9.76e-03	2.34e-03	1.17e-03	0.00e+00	8.65e-03	3.80e-04	9.58e-04	8.20e-03	6.16e-03	1.27e-01		3.84e-01
];

% matrix info: 525825-by-525825 with 3674625 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    525825    525825         3674625                6           3674625
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       0.000000e+00          0.000000                0          3299048
%=================================================================================%

%=================================================================================%
%   Solver info not supported.
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       0.000000e+00          0.000000                0          3299048
%=================================================================================%

%=================================================================================%
%   Solver info not supported.
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  1.328163  0.000000
];

