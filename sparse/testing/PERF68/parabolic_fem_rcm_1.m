% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (/home/hanzt/matrices/parabolic_fem_rcm.mtx): done. Converting to CSR:
% Detected symmetric case. done.
performance_68 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	2152469	2136084	402465	402465	4.39e-02	1.88e-01	2.96e-02	1.17e-01	2.10e-02	0.00e+00	2.80e-02	1.42e-02	5.58e-02	4.10e-02	1.40e-02	5.53e-01		5.53e-01
1	2159389	2160393	385225	401610	4.19e-02	2.07e-01	2.45e-02	1.09e-01	3.50e-02	0.00e+00	2.41e-02	1.91e-02	5.28e-02	4.60e-02	2.20e-02	5.82e-01		1.13e+00
2	2202769	2205916	413309	412305	4.09e-02	2.21e-01	2.69e-02	1.54e-01	3.70e-02	0.00e+00	2.60e-02	1.72e-02	6.28e-02	4.90e-02	2.80e-02	6.63e-01		1.80e+00
];

% matrix info: 525825-by-525825 with 3674625 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
    525825    525825         3674625                6           3674625
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       0.000000e+00          0.000000                0            1
%=================================================================================%

%=================================================================================%
%   Solver info not supported.
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       0.000000e+00          0.000000                0            1
%=================================================================================%

%=================================================================================%
%   Solver info not supported.
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  3.693284  0.000000
];

