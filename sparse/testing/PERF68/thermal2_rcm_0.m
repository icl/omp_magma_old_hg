% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (/home/hanzt/matrices/thermal2_rcm.mtx): done. Converting to CSR:
% Detected symmetric case. done.
performance_68 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	4967117	4932426	939767	939767	5.25e-02	1.60e-01	3.40e-02	1.28e-02	6.03e-03	0.00e+00	3.70e-02	4.50e-04	3.33e-03	3.01e-02	1.98e-02	3.56e-01		3.56e-01
1	5017706	5086014	958565	993256	6.51e-02	1.64e-01	3.32e-02	7.52e-03	6.77e-03	0.00e+00	3.93e-02	3.08e-04	3.42e-03	2.53e-02	2.05e-02	3.65e-01		7.22e-01
2	5113385	5153733	989712	921404	6.72e-02	1.70e-01	3.46e-02	5.71e-03	5.92e-03	0.00e+00	4.02e-02	3.03e-04	3.39e-03	2.35e-02	2.11e-02	3.72e-01		1.09e+00
];

% matrix info: 1228045-by-1228045 with 8580313 nonzeros

matrixinfo = [
%   size   (m x n)     ||   nonzeros (nnz)   ||   nnz/m   ||   stored nnz
%============================================================================%
   1228045   1228045         8580313                6           8580313
%============================================================================%
];
convergence = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       0.000000e+00          0.000000                0          641304
%=================================================================================%

%=================================================================================%
%   Solver info not supported.
%=================================================================================%
];

solverinfo = [
%   iter   ||   residual-nrm2    ||   runtime    ||   SpMV-count   ||   info
%=================================================================================%
        0       0.000000e+00          0.000000                0          641304
%=================================================================================%

%=================================================================================%
%   Solver info not supported.
%=================================================================================%
];

precondinfo = [
%   setup  runtime
  3.451595  0.000000
];

