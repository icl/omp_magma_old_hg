% Usage: ./testing_dsolver [options] [-h|--help]  matrices

% Reading sparse matrix from file (/home/hanzt/matrices/G3_circuit.mtx): done. Converting to CSR:
% Detected symmetric case. done.
performance_68 = [
%iter	L.nnz	U.nnz	rm L	rm U	rowmajor	candidates	residuals	select		insert		reorder		sweep		threshold	remove		reorder		sweeep		total			accum
0	4728856	4758864	843134	843134	5.36e-02	1.55e-01	2.68e-02	1.39e-02	6.05e-03	0.00e+00	4.33e-02	2.21e-04	2.44e-03	3.37e-02	1.50e-02	3.50e-01		3.50e-01
1	4843902	4734104	814483	784475	5.52e-02	1.59e-01	2.41e-02	1.05e-02	5.86e-03	0.00e+00	4.99e-02	3.48e-04	2.67e-03	2.86e-02	1.49e-02	3.52e-01		7.02e-01
