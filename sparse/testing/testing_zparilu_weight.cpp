/*
    -- MAGMA (version 1.1) --
       Univ. of Tennessee, Knoxville
       Univ. of California, Berkeley
       Univ. of Colorado, Denver
       @date

       @precisions normal z -> c d s
       @author Hartwig Anzt
       @author Stephen Wood
*/

// includes, system
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <libgen.h>
#include <math.h>

// includes, project
#include "magma_v2.h"
#include "magmasparse.h"
#include "testings.h"

#ifdef MAGMA_WITH_MKL
	#define MKL_Complex8  magmaFloatComplex
	#define MKL_Complex16 magmaDoubleComplex
    #include <mkl_spblas.h>
    #include <mkl_rci.h>
#endif

#define PRECISION_z


int main( int argc, char** argv)
{
    // generates the exact preconditioner for system IC
    // and uses then update sweeps for all others
    TESTING_CHECK( magma_init() );
    magma_print_environment();

    magma_queue_t queue=NULL;
    magma_queue_create( 0, &queue );
    
    real_Double_t start, end;
    real_Double_t t_cusparse, t_chow;

    magma_z_matrix hA={Magma_CSR}, hAL={Magma_CSR},
    hAU={Magma_CSR},  hAUt={Magma_CSR}, hLCU={Magma_CSR}, hUCU={Magma_CSR},
    hAcusparse={Magma_CSR}, hAtmp={Magma_CSR}, dA={Magma_CSR}, hLU={Magma_CSR}, 
    dL={Magma_CSR}, dU={Magma_CSR}, hL={Magma_CSR}, hU={Magma_CSR}, 
    hUT={Magma_CSR};

    int inp = 1;
    int dwitermax = 1;
    magmaDoubleComplex diagweight = MAGMA_Z_ZERO;
    magma_scale_t scaling = Magma_UNITDIAG;
    int writecsr = 0;
    char filename[80];
    char matrixfilename[80];
    
    for (inp=1; inp < argc; ++inp) {
    	if ( strcmp("--dwitermax", argv[inp]) == 0 && inp+1 < argc ) {
        	dwitermax = atoi( argv[++inp] );
        }
        else if ( strcmp("--dweight", argv[inp]) == 0 && inp+1 < argc ) {
        	double dwtmp = 0.0;
            sscanf( argv[++inp], "%lf", &dwtmp );
            diagweight = MAGMA_Z_MAKE( dwtmp, 0.0 );
        }
        else if ( strcmp("--mscale", argv[inp]) == 0 && inp+1 < argc ) {
            inp++;
            if ( strcmp("NOSCALE", argv[inp]) == 0 ) {
                scaling = Magma_NOSCALE;
            }
            else if ( strcmp("UNITDIAG", argv[inp]) == 0 ) {
                scaling = Magma_UNITDIAG;
            }
            else if ( strcmp("UNITROW", argv[inp]) == 0 ) {
                scaling = Magma_UNITROW;
            }
            else {
                printf( "%%error: invalid scaling, use default.\n" );
            }
        }
        else if ( strcmp("--writecsr", argv[inp]) == 0 && inp+1 < argc ) {
        	writecsr = atoi( argv[++inp] );
        }
        else {
        	break;	
        }
    }
    printf("dwitermax=%d dweight=%e mscale=%d writecsr=%d\n", 
    	dwitermax, MAGMA_Z_REAL(diagweight), scaling, writecsr );

        //################################################################//
        //                      read matrix from file                     //
        //################################################################//
    while( inp < argc ) {
        if ( strcmp("LAPLACE2D", argv[inp]) == 0 && inp+1 < argc ) {   // Laplace test
            inp++;
            magma_int_t laplace_size = atoi( argv[inp] );
            sprintf(matrixfilename, "LAPLACE2D_%d", laplace_size);
        	printf("generating %s\n", matrixfilename);
            TESTING_CHECK( magma_zm_5stencil(  laplace_size, &hA, queue ));
            inp++;
        } else {                        // file-matrix test
        	sprintf( matrixfilename, basename(argv[inp]) );
        	char *mftmp;
        	mftmp = strtok( matrixfilename, "." );
        	strcpy( matrixfilename, mftmp );
        	printf("reading file %s\n", matrixfilename);
            TESTING_CHECK( magma_z_csr_mtx( &hA,  argv[inp], queue ));
            inp++;
        }
        
        
    // optional scaling
    if (scaling != Magma_NOSCALE) {
    	TESTING_CHECK( magma_zmscale( &hA, scaling, queue ) );
    }

        //################################################################//
        //                  MKL SPARSE reference ILU                        //
        //################################################################//
        
        real_Double_t cunonlinres = 0.0;
        real_Double_t cuilures = 0.0;

        magma_z_mtransfer( hA, &dA, Magma_CPU, Magma_DEV, queue );
        
        magma_int_t info = 0;
#if defined PRECISION_d  
        magmaDoubleComplex zero = MAGMA_Z_MAKE(0.0, 0.0);
    	int n = dA.num_rows;
    	int ipar[128];
    	magmaDoubleComplex dpar[128];

    	#pragma omp parallel for
		for (magma_int_t i=0; i<128; i++ ) {
			ipar[i] = 0;
			dpar[i] = zero;
		}
		ipar[0] = n;			// problem size (number of rows)
		ipar[1] = 6;			// error log control 6: to screan
		ipar[2] = 1;			// RCI stage
		ipar[3] = 0;			// iteration count
		ipar[4] = (int) min(150, n); // maximum number of iterations
		ipar[5] = 1; 			// error output switch
		ipar[6] = 1;			// warning output switch
		ipar[7] = 0; 			// dfgmres iteration stopping test switch
		ipar[8] = 1;			// dfgmres residual stopping test switch
		ipar[9] = 1;			// user defined stopping test switch
		ipar[10] = 0; 			// preconditioned dfgmres switch
		ipar[11] = 0;			// zero norm check switch
    	ipar[15] = ipar[4];	    // default non-restart GMRES
		ipar[30] = 0;  			// 0: stop if diagonal entry < dpar[31], 1: replace with dpar[32]
    	
		dpar[30] = 1.0e-16;		// small value
		dpar[31] = 1.0e-10;		// value that replaces diagonal elements < dpar[31]
		
		#pragma omp parallel for
		for (magma_int_t i=0; i<dA.nnz; i++) {
			dA.dcol[i] += 1;
		}
		#pragma omp parallel for
		for (magma_int_t i=0; i<dA.num_rows+1; i++) {
			dA.drow[i] += 1;	
		}
		start = magma_sync_wtime( queue );
		//call dcsrilu0(n, a, ia, ja, bilu0, ipar, dpar, ierr)
		dcsrilu0( (int*) &n, dA.dval, dA.drow, dA.dcol, dA.dval, 
			ipar, dpar, (int*) &info );
		end = magma_sync_wtime( queue );
        t_cusparse = end-start;
		
		// decrement to create zero-based indexing of the array parameters
		#pragma omp parallel for
		for (magma_int_t i=0; i<dA.nnz; i++) {
			dA.dcol[i] -= 1;
		}
		#pragma omp parallel for
		for (magma_int_t i=0; i<dA.num_rows+1; i++) {
			dA.drow[i] -= 1;
		}
		
#else
	info = MAGMA_ERR_NOT_SUPPORTED;
	printf("MAGMA_ERR_NOT_SUPPORTED\nMKL ONLY SUPPORTS DOUBLE PRECISION iLU0\n");
	return info;
#endif  

        magma_z_mtransfer( dA, &hAcusparse, Magma_DEV, Magma_CPU, queue );
        //hLCU.diagorder_type = Magma_UNITY;
        magma_z_mconvert( hAcusparse, &hLCU, Magma_CSR, Magma_CSRL, queue );
        //hUCU.diagorder_type = Magma_VALUE;
        magma_z_mconvert( hAcusparse, &hUCU, Magma_CSR, Magma_CSRU, queue );
               
        magma_zilures(   hA, hLCU, hUCU, &hLU, &cuilures, &cunonlinres, queue ); 
        magma_z_mfree( &hLCU, queue );
        magma_z_mfree( &hUCU, queue );
        magma_z_mfree( &hLU, queue );

        if (writecsr > 0) {
    	    //magma_zprint_matrix(hAcusparse, queue); //getchar();
        	sprintf( filename, "mkl_iLU_%s.mtx", matrixfilename );
        	// write to file
        	printf("hAcusparse.num_cols=%d\n", hAcusparse.num_cols);
        	TESTING_CHECK( magma_zwrite_csrtomtx( hAcusparse, filename, queue ));
        }
        
        //################################################################//
        //                  end MKL SPARSE reference ILU                    //
        //################################################################//
    // reorder the matrix determining the update processing order
    magma_z_sparse_matrix hAcopy, hACSRCOO, dAinitguess;
    magma_z_mtransfer( hA, &hAcopy, Magma_CPU, Magma_CPU, queue );
    // --------------- initial error and residual -----------//
    magma_zsymbilu( &hAcopy, 0, &hAL, &hAU, queue ); 
    real_Double_t initres = 0.0;
    real_Double_t initilures = 0.0;
    real_Double_t initnonlinres = 0.0;
    magma_zmlumerge( hAL, hAU, &hAtmp, queue );
    // frobenius norm of error
    magma_zfrobenius( hAcusparse, hAtmp, &initres, queue );
    // ilu residual
    magma_zilures(   hA, hAL, hAU, &hLU, &initilures, &initnonlinres, queue ); 
    // free what we don't need any more
    magma_z_mfree( &hAtmp, queue );
    magma_z_mfree( &hAL, queue );
    magma_z_mfree( &hAU, queue );
    for( int localiters = 1; localiters < 2; localiters++){ //local iterations

//    printf("\n\nLaplace3D_%d = [\n", 32*(matrix-7));
    // ---------------- iteration matrices ------------------- //
    // possibility to increase fill-in in ILU-(m)
    //ILU-m levels
    for( int levels = 0; levels < 1; levels++){ //ILU-m levels
    //{int levels = atoi( argv[1]);
    magma_z_mtransfer( hA, &hAcopy, Magma_CPU, Magma_CPU, queue );
    magma_zsymbilu( &hAcopy, levels, &hAL, &hAUt, queue ); 
    printf("ILU%d = [", levels);
    printf("\n%%#=======================================================================================================#\n");
    printf("%%#\t#nnz\titers\tbs\tILU-time\tParILU-time\tILU-ILUres\tParILU-ILUres\tParILU-nonlinres\t\tscaled \n");
    // add a unit diagonal to L for the algorithm
    magma_zmLdiagadd( &hAL, queue ); 

    // transpose U for the algorithm
    //magma_z_cucsrtranspose(  hAUt, &hAU, queue );
    magma_zmtranspose(  hAUt, &hAU, queue );
    magma_z_mfree( &hAUt, queue );
    // scale to unit diagonal
    //magma_zmscale( &hAU, Magma_UNITDIAG, queue );


/*
    // need only lower triangular
    magma_z_mfree(&hAL);
    hAL.diagorder_type == Magma_UNITY;
    magma_z_mconvert( hA, &hAL, Magma_CSR, Magma_CSRL, queue );
*/

    // ---------------- initial guess ------------------- //
    magma_z_mconvert( hAcopy, &hACSRCOO, Magma_CSR, Magma_CSRCOO, queue );
    int blocksize = 1;
    //magma_zmreorder( hACSRCOO, n, blocksize, blocksize, blocksize, &hAinitguess, queue );
    //magma_z_mfree(&hAinitguess);
    magma_z_mtransfer( hACSRCOO, &dAinitguess, Magma_CPU, Magma_DEV, queue );
    magma_z_mfree(&hACSRCOO, queue );

    
        //################################################################//
        //                        iterative ILU                           //
        //################################################################//
        
    // weight diagonal    
    magma_zmdiagadd( &hAU, diagweight, queue);
    // number of AILU sweeps
    for(int iters=0; iters<101; iters+=1){
    	// reduce diagonal weight
    	if (iters>0 && iters<dwitermax) {
          magmaDoubleComplex dwincr = MAGMA_Z_MAKE( -MAGMA_Z_REAL(diagweight) / (double)(dwitermax) , 0.0 ) ;
          magma_zmdiagadd( &hAU, dwincr, queue); 
        }
    	// take average results for residuals
    	real_Double_t resavg = 0.0;
    	real_Double_t iluresavg = 0.0;
    	real_Double_t nonlinresavg = 0.0;
    	int nnz, numavg = 1;
    	//multiple runs
    	for(int z=0; z<numavg; z++){
    		real_Double_t res = 0.0;
    	    real_Double_t ilures = 0.0;
    	    real_Double_t nonlinres = 0.0;
    	
    	    // transfer the factor L and U
    	    magma_z_mtransfer( hAL, &dL, Magma_CPU, Magma_DEV, queue );
    	    magma_z_mtransfer( hAU, &dU, Magma_CPU, Magma_DEV, queue );
    	    
    	    // iterative ILU embedded in timing
    	    start = magma_sync_wtime( queue );
    	    for(int i=0; i<iters; i++){
    	    	//cudaProfilerStart();
    	        magma_zparilu_csr( dAinitguess, dL, dU, queue );
    	        //cudaProfilerStop();
    	    }
    	    end = magma_sync_wtime( queue );
    	    t_chow = end-start;
    	
    	    // check the residuals
    	    magma_z_mtransfer( dL, &hL, Magma_DEV, Magma_CPU, queue );
    	    magma_z_mtransfer( dU, &hU, Magma_DEV, Magma_CPU, queue );
    	    //magma_z_cucsrtranspose(  hU, &hUT, queue );
    	    magma_zmtranspose(  hU, &hUT, queue );
    	
    	    magma_z_mfree(&dL, queue );
    	    magma_z_mfree(&dU, queue );
    	
    	    magma_zmlumerge( hL, hUT, &hAtmp, queue );
    	    // frobenius norm of error
    	    magma_zfrobenius( hAcusparse, hAtmp, &res, queue );
    	    
    	    if (writecsr > 0 && iters % writecsr == 0) {
    	    	//magma_zprint_matrix(hAtmp, queue); //getchar();
    	    	sprintf( filename, "PariLU0_%d_%e_%d_%s_iter_%05d.mtx", 
    	    		dwitermax, MAGMA_Z_REAL(diagweight), scaling, matrixfilename, iters );
    	    	// write to file
    	    	TESTING_CHECK( magma_zwrite_csrtomtx( hAtmp, filename, queue ));
    	    }
    	    
    	    // ilu residual
    	    magma_zilures(   hA, hL, hUT, &hLU, &ilures, &nonlinres, queue ); 
    	
    	    iluresavg += ilures;
    	    resavg += res;
    	    nonlinresavg += nonlinres;
    	    nnz = hAtmp.nnz;
    	
    	    magma_z_mfree( &hL, queue );
    	    magma_z_mfree( &hU, queue );
    	    magma_z_mfree( &hUT, queue );
    	    magma_z_mfree( &hAtmp, queue );
    	
    	}//multiple runs
    	
    	iluresavg = iluresavg/numavg;
    	resavg = resavg/numavg;
    	nonlinresavg = nonlinresavg/numavg;
    	
    	printf(" %d\t%d\t%d\t%d\t%.2e\t",
    	                          levels, nnz, 1* iters, blocksize, t_cusparse);
    	printf(" %.2e\t%.4e\t%.4e\t%.4e\t%.4e\t%.4e\t\n", 
    	t_chow, cuilures, iluresavg, nonlinresavg, iluresavg/initilures, nonlinresavg/initnonlinres);


    	//printf(" %.2e  &  ", iluresavg);    
    }// iters
    magma_z_mfree( &hAcopy, queue );
    printf("\n%%#=======================================================================================================#\n];\n");
    }// levels

    }// localiters

    // free all memory
    magma_z_mfree( &hAL, queue );
    magma_z_mfree( &hAU, queue );
    magma_z_mfree( &hAcusparse, queue );
    magma_z_mfree( &dA, queue );
    magma_z_mfree( &dAinitguess, queue );
    magma_z_mfree( &hA, queue );


    }// multiple matrices

    magma_queue_destroy( queue );
    TESTING_CHECK( magma_finalize() );
    return 0;
}
