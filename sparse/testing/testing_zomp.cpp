/*
    -- MAGMA (version 2.0) --
       Univ. of Tennessee, Knoxville
       Univ. of California, Berkeley
       Univ. of Colorado, Denver
       @date

       @precisions normal z -> c d s
       @author Hartwig Anzt
*/
//#define PAPI				 /* PAPI test variable */

// includes, system
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

// includes, project
#include "magma_v2.h"
#include "magmasparse.h"
#include "testings.h"

// papi
#ifdef PAPI
#include "/opt/include/papi.h"
#endif

#define NUM_EVENTS 8		 /* # of PAPI events */

#define NAME_TIME
#define SIZE 16384


/* ////////////////////////////////////////////////////////////////////////////
   -- testing any solver
*/
int main(  int argc, char** argv )
{
    magma_int_t info = 0;
    TESTING_CHECK( magma_init() );
    magma_print_environment();

    //magma_zopts zopts;
    magma_queue_t queue;
    magma_queue_create( 0, &queue );
    
    magmaDoubleComplex one = MAGMA_Z_MAKE(1.0, 0.0);
    //magmaDoubleComplex zero = MAGMA_Z_MAKE(0.0, 0.0);
    magma_z_matrix A={Magma_CSR}, B={Magma_CSR}, C={Magma_CSR};
    //magma_z_matrix x={Magma_CSR}, b={Magma_CSR};
    
    int num_threads, q=1;
    real_Double_t start, end;
    int size = 16384;   
    if ( strcmp("SIZE", argv[q]) == 0 && q+1 < argc ) {   // Laplace test
            q++;
            size = 256*atoi( argv[q] );
            
    }



#ifdef PAPI
	int retval, i;
	int EventSet = PAPI_NULL;
	long long values[NUM_EVENTS];
#ifdef NAME_TIME
	char *EventName[] = { "PAPI_TOT_CYC", "PAPI_REF_CYC", "PAPI_L1_TCM", "PAPI_L2_TCM", "PAPI_L1_TCH", "PAPI_L2_TCH", "PAPI_L3_TCH", "PAPI_RES_STL" };
	int events[NUM_EVENTS];
#else	
	int events[NUM_EVENTS] = { PAPI_TOT_CYC, PAPI_REF_CYC, PAPI_L1_TCM, PAPI_L2_TCM, PAPI_L1_TCH, PAPI_L2_TCH, PAPI_L3_TCH, PAPI_RES_STL };
	char event_name[PAPI_MAX_STR_LEN];
#endif
#endif	
        
    TESTING_CHECK( magma_zvinit( &A, Magma_CPU, size, size, one, queue ));
    TESTING_CHECK( magma_zvinit( &B, Magma_CPU, size, 1, one, queue ));
    TESTING_CHECK( magma_zvinit( &C, Magma_CPU, size, 1, one, queue ));

    
#ifdef _OPENMP
//for(int threads=1; threads<273; threads++){

    #pragma omp parallel
    {
        num_threads = omp_get_max_threads();   
    }
    
    // warmup
    
    
    magma_zgemvomp( A, B, &C, num_threads, queue );
    
/**************************** START PAPI **********************************/
	
#ifdef PAPI
	/* PAPI Initialization */
	retval = PAPI_library_init( PAPI_VER_CURRENT );
	if( retval != PAPI_VER_CURRENT )
		fprintf( stderr, "PAPI_library_init failed\n" );
	
	// printf( "%% PAPI_VERSION     : %4d %6d %7d\n",
	// 		PAPI_VERSION_MAJOR( PAPI_VERSION ),
	// 		PAPI_VERSION_MINOR( PAPI_VERSION ),
	// 		PAPI_VERSION_REVISION( PAPI_VERSION ) );
	
	
#ifdef NAME_TIME        
	/* convert PAPI native events to PAPI code */
	for( i = 0; i < NUM_EVENTS; i++ )
	{
		retval = PAPI_event_name_to_code( EventName[i], &events[i] );
		if( retval != PAPI_OK )
			fprintf( stderr, "PAPI_event_name_to_code failed\n" );
		else
		    ;//	printf( "%% Name %s --- Code: %x\n", EventName[i], events[i] );
	}
#endif
	
	retval = PAPI_create_eventset( &EventSet );
	if( retval != PAPI_OK )
		fprintf( stderr, "PAPI_create_eventset failed\n" );
	
	retval = PAPI_add_events( EventSet, events, NUM_EVENTS );
	if( retval != PAPI_OK )
		fprintf( stderr, "PAPI_add_events failed\n" );
	
	retval = PAPI_start( EventSet );
	if( retval != PAPI_OK )
		fprintf( stderr, "PAPI_start failed\n" );
#endif
	
	/**************************** END PAPI **********************************/

    num_threads = 68;

    start = magma_sync_wtime( queue );
    
    magma_zgemvomp( A, B, &C, num_threads, queue );
  
/*
  #pragma omp parallel for num_threads(68)
    for( int i=0; i<size; i++){
        magmaDoubleComplex tmp = zero;
        for( int j=0; j<size; j++){
            tmp += A.val[size*i+j] * B.val[j];           
        }
        C.val[i] = tmp;
    }
*/
/*	// mkl
	magma_trans_t transtype = MagmaNoTrans;
        const magma_int_t ss = size;
	const magma_int_t ione = 1;
	const magmaDoubleComplex z_one = MAGMA_Z_ONE;
	const magmaDoubleComplex z_zero = MAGMA_Z_ZERO;
    
    blasf77_zgemv( lapack_trans_const(transtype), &ss, &ss, &z_one, A.val, &ss, B.val, &ione, &z_zero, B.val, &ione );  
    
*/
    end = magma_sync_wtime( queue );
    //printf("\t%.4e", end-start);
    // printf("runtime_%d = %.4e;\n", num_threads, end-start);
    
    	/**************************** START PAPI **********************************/
	
#ifdef PAPI
    printf("%%num_threads\ttime\tGFLOPs\t"); 
	for( i = 0; i < NUM_EVENTS; i++ )
	{
		printf( "%s\t", EventName[i] );
	}
    printf( "\n" );
    printf(" %d\t%d\t", num_threads, size ); 
    printf("\t%.4e\t%.4e\t", end-start,(double)size/1e5*(double)size/1e5*10*2/(end-start));    
	retval = PAPI_stop( EventSet, values );
	if( retval != PAPI_OK )
		fprintf( stderr, "PAPI_stop failed\n" );
	
	for( i = 0; i < NUM_EVENTS; i++ )
	{
#ifdef NAME_TIME
		// printf( "%% %12lld \t\t --> %s \n", values[i],
		//	   EventName[i] );
#else
		retval = PAPI_event_code_to_name( events[i], event_name );
		if( retval != PAPI_OK )
			fprintf( stderr, "PAPI_event_code_to_name failed\n" );
		
		// printf( "%% %12lld \t\t --> %s \n", values[i],
		//	   event_name );
#endif
        printf( "%12lld \t", values[i]);

	}
#endif
        printf( "\n" );
	
	/**************************** END PAPI **********************************/
    
    
//}
#endif


        magma_zmfree(&A, queue );
        magma_zmfree(&B, queue );
        magma_zmfree(&C, queue );

    magma_queue_destroy( queue );
    TESTING_CHECK( magma_finalize() );
    return info;
}
