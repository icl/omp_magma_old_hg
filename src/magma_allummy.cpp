/*
    -- MAGMA (version 2.0) --
       Univ. of Tennessee, Knoxville
       Univ. of California, Berkeley
       Univ. of Colorado, Denver
       @date

       @author Hartwig Anzt

*/

// just a dummy file containing undefined functions
#include "magma_internal.h"

#define COMPLEX

#ifdef __cplusplus
extern "C" {
#endif

/*
void magma_setvector_q_internal(
    magma_int_t n, magma_int_t elemSize,
    const void *hx_src, magma_int_t incx,
    void       *dy_dst, magma_int_t incy,
    magma_queue_t queue,
    const char* func, const char* file, int line ){  ; }

void magma_getvector_q_internal(
    magma_int_t n, magma_int_t elemSize,
    const void *dx_src, magma_int_t incx,
    void       *hy_dst, magma_int_t incy,
    magma_queue_t queue,
    const char* func, const char* file, int line ){  ; }

void magma_copyvector_q_internal(
    magma_int_t n, magma_int_t elemSize,
    const void *dx_src, magma_int_t incx,
    void       *dy_dst, magma_int_t incy,
    magma_queue_t queue,
    const char* func, const char* file, int line ){  ; }

void magma_setvector_async_internal(
    magma_int_t n, magma_int_t elemSize,
    const void *hx_src, magma_int_t incx,
    void       *dy_dst, magma_int_t incy,
    magma_queue_t queue,
    const char* func, const char* file, int line ){  ; }

void magma_getvector_async_internal(
    magma_int_t n, magma_int_t elemSize,
    const void *dx_src, magma_int_t incx,
    void       *hy_dst, magma_int_t incy,
    magma_queue_t queue,
    const char* func, const char* file, int line ){  ; }

void magma_copyvector_async_internal(
    magma_int_t n, magma_int_t elemSize,
    const void *dx_src, magma_int_t incx,
    void       *dy_dst, magma_int_t incy,
    magma_queue_t queue,
    const char* func, const char* file, int line ){  ; }

void magma_setmatrix_q_internal(
    magma_int_t m, magma_int_t n, magma_int_t elemSize,
    const void *hA_src, magma_int_t lda,
    void       *dB_dst, magma_int_t lddb,
    magma_queue_t queue,
    const char* func, const char* file, int line ){  ; }

void magma_getmatrix_q_internal(
    magma_int_t m, magma_int_t n, magma_int_t elemSize,
    const void *dA_src, magma_int_t ldda,
    void       *hB_dst, magma_int_t ldb,
    magma_queue_t queue,
    const char* func, const char* file, int line ){  ; }

void magma_copymatrix_q_internal(
    magma_int_t m, magma_int_t n, magma_int_t elemSize,
    const void *dA_src, magma_int_t ldda,
    void       *dB_dst, magma_int_t lddb,
    magma_queue_t queue,
    const char* func, const char* file, int line ){  ; }

void magma_setmatrix_async_internal(
    magma_int_t m, magma_int_t n, magma_int_t elemSize,
    const void *hA_src, magma_int_t lda,
    void       *dB_dst, magma_int_t lddb,
    magma_queue_t queue,
    const char* func, const char* file, int line ){  ; }

void magma_getmatrix_async_internal(
    magma_int_t m, magma_int_t n, magma_int_t elemSize,
    const void *dA_src, magma_int_t ldda,
    void       *hB_dst, magma_int_t ldb,
    magma_queue_t queue,
    const char* func, const char* file, int line ){  ; }

void magma_copymatrix_async_internal(
    magma_int_t m, magma_int_t n, magma_int_t elemSize,
    const void *dA_src, magma_int_t ldda,
    void       *dB_dst, magma_int_t lddb,
    magma_queue_t queue,
    const char* func, const char* file, int line ){  ; }
*/




const char* magma_strerror( magma_int_t error )
{
    return MAGMA_SUCCESS;
}

#ifdef __cplusplus
}
#endif


